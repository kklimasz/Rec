/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddSpdInfo.h
 *
 * Header file for algorithm ChargedProtoParticleAddSpdInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_ChargedProtoParticleAddSpdInfo_H
#define GLOBALRECO_ChargedProtoParticleAddSpdInfo_H 1

// from Gaudi
#include "ChargedProtoParticleCALOBaseAlg.h"

/** @class ChargedProtoParticleAddSpdInfo ChargedProtoParticleAddSpdInfo.h
 *
 *  Updates the CALO SPD information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class ChargedProtoParticleAddSpdInfo final : public ChargedProtoParticleCALOBaseAlg {

public:
  /// Standard constructor
  ChargedProtoParticleAddSpdInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Load the Calo Spd tables
  bool getSpdData();

  /// Add Calo Spd information to the given ProtoParticle
  bool addSpd( LHCb::ProtoParticle* proto ) const;

private:
  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inSpdPath;
  std::string m_spdEPath;

  const LHCb::Calo2Track::ITrAccTable*  m_InSpdTable = nullptr;
  const LHCb::Calo2Track::ITrEvalTable* m_SpdETable  = nullptr;
};

#endif // GLOBALRECO_ChargedProtoParticleAddSpdInfo_H

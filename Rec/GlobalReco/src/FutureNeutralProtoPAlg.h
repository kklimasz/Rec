/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef GLOBALRECO_FUTURENEUTRALPROTOPALG_H
#define GLOBALRECO_FUTURENEUTRALPROTOPALG_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
// Calo
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
/** @class FutureNeutralProtoPAlg FutureNeutralProtoPAlg.h
 *
 *  Creator of the neutral ProtoParticles from CaloHypos
 *
 *  The current version fills the following estimators for ProtoParticle
 *
 *  <ul>
 *  <li>  <i>CaloTrMatch</i>     as <b>minimal</b> of this estimator for all
 *        linked <i>CaloHypo</i> objects. The value is extracted from
 *        the relation table/associator as a relation weigth between
 *        <i>CaloCluster</i> and <i>TrStoredTrack</i> objects </li>
 *  <li>  <i>CaloDepositID</i>   as <b>maximal</b> of this estimator for all
 *        linked <i>CaloHypo</i> objects using Spd/Prs estimator tool
 *        written by Frederic Machefert </li>
 *  <li>  <i>CaloShowerShape</i> as <b>maximal</b> of the estimator for
 *        all linked <i>CaloHypo</i> objects. Estimator is equal to the
 *        sum of diagonal elements of cluster spread matrix (2nd order
 *        moments of the cluster) </li>
 *  <li>  <i>ClusterMass</i>     as <b>maximal</b> of the estimator of
 *        cluster mass using smart algorithm by Olivier Deschamp </li>
 *  <li>  <i>PhotonID</i>        as the estimator of PhotonID
 *        using nice identifiaction tool
 *        CaloPhotonEstimatorTool by Frederic Machefert *
 *  </ul>
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-06-09
 *  Adapted from NeutralPPsFromCPsAlg class (Vanya Belyaev Ivan.Belyaev@itep.ru)
 */
class FutureNeutralProtoPAlg final : public GaudiAlgorithm {
  // ==========================================================================
public:
  // ==========================================================================
  /// Standard constructor
  FutureNeutralProtoPAlg( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// use"light" mode? ( suitable fo recalibration purposes)
  inline bool lightMode() const noexcept { return m_light_mode; }

  void pushData( LHCb::ProtoParticle*, LHCb::ProtoParticle::additionalInfo, const LHCb::CaloHypo&,
                 LHCb::Calo::Enum::DataType, double def = LHCb::Calo::Enum::Default, const bool force = false ) const;

  double getMass( const int cellCode ) const;

private: // data
  IFutureCounterLevel* counterStat = nullptr;

  std::string              m_protoLocation;
  std::vector<std::string> m_hyposLocations;
  /// flag to indicate "light/calibration" mode
  bool                                    m_light_mode{false};
  LHCb::Calo::Interfaces::IHypoEstimator* m_estimator = nullptr;
  mutable std::map<const int, double>     m_mass      = {{}};
  mutable bool                            m_setMass{false};
};

inline void FutureNeutralProtoPAlg::pushData( LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo pflag,
                                              const LHCb::CaloHypo& hypo, LHCb::Calo::Enum::DataType hflag,
                                              const double def, const bool force ) const {
  const auto data = m_estimator->data( hypo, hflag ).value_or( def );
  if ( data != def || force ) {
    proto->addInfo( pflag, data ); // only store when different from default
    std::ostringstream mess;
    mess << pflag << " for " << hypo.hypothesis();
    counter( mess.str() ) += data;
  }
}

#endif // GLOBALRECO_FUTURENEUTRALPROTOPALG_H

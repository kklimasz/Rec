/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H
#define FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H 1

// Include files
// from Gaudi
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FutureNeutralProtoParticleAddNeutralID FutureNeutralProtoParticleAddNeutralID.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-05-27
 */

class FutureNeutralProtoParticleAddNeutralID final : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::string>                            m_input{this, "Input", LHCb::ProtoParticleLocation::Neutrals};
  Gaudi::Property<bool>                                   m_isNotE{this, "UpdateIsNotE", true};
  Gaudi::Property<bool>                                   m_isNotH{this, "UpdateIsNotH", true};
  Gaudi::Property<bool>                                   m_isPhoton{this, "UpdateIsPhoton", true};
  Gaudi::Property<double>                                 m_isNotE_Pt{this, "MinPtIsNotE", 75.0};
  Gaudi::Property<double>                                 m_isNotH_Pt{this, "MinPtIsNotH", 75.0};
  Gaudi::Property<double>                                 m_isPhoton_Pt{this, "MinPtIsPhoton", 2000.0};
  ToolHandle<LHCb::Calo::Interfaces::IGammaPi0Separation> m_gammaPi0{this, "FutureGammaPi0SeparationTool",
                                                                     "FutureGammaPi0SeparationTool"};
  ToolHandle<LHCb::Calo::Interfaces::INeutralID> m_neutralID{this, "FutureNeutralIDTool", "FutureNeutralIDTool"};
};

#endif // FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H

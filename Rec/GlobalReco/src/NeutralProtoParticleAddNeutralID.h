/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NEUTRALPROTOPARTICLEADDNEUTRALID_H
#define NEUTRALPROTOPARTICLEADDNEUTRALID_H 1

// Include files
// from Gaudi
#include "CaloInterfaces/IGammaPi0SeparationTool.h"
#include "CaloInterfaces/INeutralIDTool.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class NeutralProtoParticleAddNeutralID NeutralProtoParticleAddNeutralID.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-05-27
 */

class NeutralProtoParticleAddNeutralID final : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::string>        m_input{this, "Input", LHCb::ProtoParticleLocation::Neutrals};
  Gaudi::Property<bool>               m_isNotE{this, "UpdateIsNotE", true};
  Gaudi::Property<bool>               m_isNotH{this, "UpdateIsNotH", true};
  Gaudi::Property<bool>               m_isPhoton{this, "UpdateIsPhoton", true};
  Gaudi::Property<double>             m_isNotE_Pt{this, "MinPtIsNotE", 75.0};
  Gaudi::Property<double>             m_isNotH_Pt{this, "MinPtIsNotH", 75.0};
  Gaudi::Property<double>             m_isPhoton_Pt{this, "MinPtIsPhoton", 2000.0};
  ToolHandle<IGammaPi0SeparationTool> m_gammaPi0{this, "GammaPi0SeparationTool", "GammaPi0SeparationTool"};
  ToolHandle<INeutralIDTool>          m_neutralID{this, "NeutralIDTool", "NeutralIDTool"};
};

#endif // NEUTRALPROTOPARTICLEADDNEUTRALID_H

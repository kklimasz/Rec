#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import ApplicationMgr, EventSelector, FileCatalog, DEBUG
from GaudiConf import IOHelper
from Configurables import createODIN, HltLumiSummaryDecoder
from Configurables import DumpLumiEvents

FileCatalog().Catalogs = ["xmlcatalog_file:MyCatalog.xml"]

inputs = [
    "lfn:/lhcb/data/2012/RAW/FULL/LHCb/COLLISION12/114753/114753_0000000298.raw"
]

IOHelper(None, None).setupServices()

dumpLumiEvents = DumpLumiEvents()
dumpLumiEvents.OutputLevel = DEBUG
dumpLumiEvents.OutputFileName = "testDumpLumiEvents.bz2"
dumpLumiEvents.NEventsHint = 100

ApplicationMgr().TopAlg = [
    createODIN(), HltLumiSummaryDecoder(), dumpLumiEvents
]
ApplicationMgr().HistogramPersistency = 'NONE'
ApplicationMgr().EvtMax = 100
IOHelper(Input='MDF').inputFiles(inputs, clear=True)
EventSelector().PrintFreq = 1

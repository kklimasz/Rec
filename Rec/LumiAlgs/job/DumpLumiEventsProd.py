#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import ApplicationMgr, EventSelector
from GaudiConf import IOHelper
from Configurables import createODIN, HltLumiSummaryDecoder
from Configurables import DumpLumiEvents

IOHelper(None, None).setupServices()
#IOHelper(Input='MDF').inputFiles(inputs, clear=True)

dumpLumiEvents = DumpLumiEvents()
dumpLumiEvents.OutputFileName = "output.bz2"

ApplicationMgr().TopAlg = [
    createODIN(), HltLumiSummaryDecoder(), dumpLumiEvents
]
ApplicationMgr().HistogramPersistency = 'NONE'
EventSelector().PrintFreq = 100000

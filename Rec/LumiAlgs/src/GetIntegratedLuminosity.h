/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GETINTEGRATEDLUMINOSITY_H
#define GETINTEGRATEDLUMINOSITY_H 1

// Include files
// from Gaudi

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IDataManagerSvc.h"

// event model
#include "Event/EventCountFSR.h"
#include "Event/HltLumiSummary.h"
#include "Event/LumiFSR.h"
#include "Event/LumiIntegral.h"
#include "Event/TimeSpanFSR.h"

// local
#include "LumiIntegrator.h"

/** @class GetIntegratedLuminosity GetIntegratedLuminosity.h
 *
 *  Returns integrated luminosity
 *
 *  @author Yasmine Amhis
 *  @date   2010-05-26
 */
class GetIntegratedLuminosity : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

  // ==========================================================================
protected:
  virtual int check();

  IDataProviderSvc*            m_fileRecordSvc = nullptr;
  Gaudi::Property<std::string> m_FileRecordName{this, "FileRecordLocation", "/FileRecords"}; // location of FileRecords
  Gaudi::Property<std::string> m_FSRName{this, "FSRName", "/LumiFSR"}; // specific tag of summary data in FSR
  std::string m_defaultStatusStr = "UNCHECKED"; /// status to start with if nothing else is known, Set by DefaultStatus
  LHCb::EventCountFSR::StatusFlag m_defaultStatus =
      LHCb::EventCountFSR::StatusFlag::UNCHECKED; /// status to start with if nothing else is known, cast from
                                                  /// DefaultStatus
  int                   m_events_in_file = 0;     // events after OpenFileIncident
  Gaudi::Property<bool> m_countersDetails{this, "WriteCountersDetails", false};
  int                   m_count_input  = 0; // number of events seen
  int                   m_count_output = 0; // number of incidents seen

  Gaudi::Property<std::string> m_ToolName{this, "IntegratorToolName",
                                          "LumiIntegrator"}; // name of tool for normalization
  Gaudi::Property<std::string> m_RawToolName{this, "RawIntegratorToolName",
                                             "RawLumiIntegrator"}; // name of tool for raw mu
  Gaudi::Property<std::string> m_EventCountFSRName{this, "EventCountFSRName",
                                                   "/EventCountFSR"}; // specific tag of event summary data in FSR

private:
  ILumiIntegrator* m_integratorTool    = nullptr; // tool to integrate luminosity
  ILumiIntegrator* m_rawIntegratorTool = nullptr; // tool to integrate raw mu
};
#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/LumiFSR.h"
#include "Event/LumiIntegral.h"

// local
#include "LumiReadBackFSR.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LumiReadBackFSR
//
// The purpose of this algorithm is to check the integrator tool
//
// 2009-02-27 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiReadBackFSR )

//=============================================================================
// Initialization
//=============================================================================
StatusCode LumiReadBackFSR::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by Algorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // prepare tool
  m_integratorTool = tool<ILumiIntegrator>( "LumiIntegrator", m_ToolName );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution -- maybe a Service is more appropriate than an algorithm?
//=============================================================================
StatusCode LumiReadBackFSR::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LumiReadBackFSR::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  // use tool to count events for this file
  info() << "number of events seen: " << m_integratorTool->events() << endmsg;

  // use tool to get summay for this file
  info() << "integrated normalization: " << m_integratorTool->integral() << endmsg;

  // final results
  info() << "Integrated luminosity: " << m_integratorTool->lumiValue() << " +/- " << m_integratorTool->lumiError()
         << " [pb-1]" << endmsg;

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DUMPLUMIEVENTS_H
#define DUMPLUMIEVENTS_H 1

#include "Event/HltLumiSummary.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <fstream>
#include <iostream>
#include <vector>

/** @class DumpLumiEvents DumpLumiEvents.h
 *
 * Algorithm that dumps random events from RAW files.
 * Output is ASCII file with name specified by OutputFileName option.
 * For each event with nonzero random counter a line is written:
 *   odin_1,...,odin_N,;key_1,value_1,...key_M,value_M,[vtx_1,...,vtx_K,];
 * where odin_i are ODIN fields, (key_j,value_j) are lumi counter pairs,
 * vtx_k are "x,y,z,n_tr,chi2" and items in [] are optional. Output can be
 * optionally compressed and time-ordered (default). Intermediate non-ordered
 * data can be kept in memory (default) or in file. See description of class
 * members for more details.
 * Typical memory footprint of the simplest job with default settings
 * (in-memory intermediate data and without output of vertices) for 1h run
 * is just below 1GB.
 *
 *  @author Rosen Matev
 *  @date   2012-03-16
 */
class DumpLumiEvents : public GaudiAlgorithm {
public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;
  StatusCode execute() override;
  StatusCode finalize() override;

private:
  struct KeyLocationPair final {
    KeyLocationPair( ulonglong k, ulonglong l ) : key{k}, location{l} {}
    ulonglong   key;
    ulonglong   location;
    friend bool operator<( const KeyLocationPair& lhs, const KeyLocationPair& rhs ) { return lhs.key < rhs.key; }
  };

  std::ostream& outputOdin( std::ostream&     s,
                            const LHCb::ODIN& odin ) const; ///<  Output ODIN values in format "odin_1,...,odin_N,;"
                                                            ///<  (see implementation).
  std::ostream& outputLumiSummary( std::ostream& s, const LHCb::HltLumiSummary& hltLumiSummary )
      const; ///<  Output lumi summary in format "key_1,value_1,...,key_M,value_M,;".
  std::ostream& outputVertices( std::ostream&            s,
                                const LHCb::RecVertices& recVertices ) const; ///< Output vertices in format
                                                                              ///< "vtx_1,...,vtx_K,;", where vtx_i is
                                                                              ///< "x,y,z,n_tr,chi2".

  Gaudi::Property<bool>        m_sort{this, "Sort", true};               ///< Whether to order events
  Gaudi::Property<std::string> m_outputFileName{this, "OutputFileName"}; ///< If filename end with .bz2, output will be
                                                                         ///< compress, otherwise output is plain ASCII
  Gaudi::Property<std::string> m_recVerticesLocation{this, "RecVerticesLocation"}; ///< If non-empty, vertices in this
                                                                                   ///< location will also be dumped
  Gaudi::Property<std::string> m_intermediateStreamFileName{
      this, "IntermediateStreamFileName"}; ///< If non-empty, the intermediate non-sorted dump should be stored in file
                                           ///< instead of memory
  Gaudi::Property<unsigned int> m_nEventsHint{this, "NEventsHint",
                                              4096000}; ///< Hint for the expected number of events, used to reserve
                                                        ///< memory for m_keyLocationPairs

  std::ostream*                m_intermediateStream = nullptr;
  std::ostream*                m_outputStream       = nullptr;
  std::ofstream                m_outputFile;
  std::vector<KeyLocationPair> m_keyLocationPairs;
};
#endif

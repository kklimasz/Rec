/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERONLUMISUMMARY_H
#define FILTERONLUMISUMMARY_H 1

#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/LumiMethods.h"

#include "GaudiAlg/FilterPredicate.h"

/** @class FilterOnLumiSummary FilterOnLumiSummary.h
 *
 *
 *  @author Jaap Panman
 *  @date   2010-01-29
 */
class FilterOnLumiSummary : public Gaudi::Functional::FilterPredicate<bool( const LHCb::HltLumiSummary& )> {
public:
  /// Standard constructor
  FilterOnLumiSummary( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  bool       operator()( const LHCb::HltLumiSummary& ) const override;

private:
  int m_Counter = 0; // int value required
  int m_Value   = 0; // int counter looked at

  Gaudi::Property<std::string> m_ValueName{this, "ValueName", "RandomMethod",
                                           [=]( Property& ) {
                                             m_Value = LHCb::LumiMethods::methodKeyToType( m_ValueName );
                                             if ( m_Value == LHCb::LumiMethods::methodKey::Unknown ) {
                                               throw GaudiException( "LumiMethod not found with name: " + m_ValueName,
                                                                     name(), StatusCode::SUCCESS );
                                             }
                                           },
                                           Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; // value required
  Gaudi::Property<std::string> m_CounterName{
      this, "CounterName", "Method",
      [=]( Property& ) {
        m_Counter = LHCb::LumiCounters::counterKeyToType( m_CounterName );
        if ( m_Counter == LHCb::LumiCounters::counterKey::Unknown ) {
          throw GaudiException( "LumiCounter not found with name: " + m_CounterName, name(), StatusCode::SUCCESS );
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{true}}; // counter looked at
};
#endif // FILTERONLUMISUMMARY_H

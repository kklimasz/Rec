/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LOKI_VELOHITPATTERNFUNCTIONS_H
#define LOKI_VELOHITPATTERNFUNCTIONS_H 1

#include "LoKi/BasicFunctors.h"

#include "Event/Track.h"

/** @file LoKi/VeloHitPatternFunctions.h
 *  Collection of Velo hit-pattern functions (that are not just counting the
 *  LHCbIDs passing a certain test)
 *
 *  @author Pieter David pieter.david@cern.ch
 *  @date   2012-03-12
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Track {
    // ========================================================================
    /** @class MaxNumConsecutiveVeloSpacePoints
     *  Get the maximal number of consecutive Velo R-Phi space points on a
     *  track
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    struct GAUDI_API MaxNumConsecutiveVeloSpacePoints final : LoKi::BasicFunctors<const LHCb::Track*>::Function {
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      MaxNumConsecutiveVeloSpacePoints* clone() const override { return new MaxNumConsecutiveVeloSpacePoints( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "TrNCONSECVELOSPACEPOINTS"; }
      // ======================================================================
    };
    // ========================================================================
    /** @class NumVeloSpacePoints
     *  Count the Velo R-Phi space points on a track
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    struct GAUDI_API NumVeloSpacePoints final : LoKi::BasicFunctors<const LHCb::Track*>::Function {
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      NumVeloSpacePoints* clone() const override { return new NumVeloSpacePoints( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "TrNVELOSPACEPOINTS"; }
      // ======================================================================
    };
    // ========================================================================
    /** @class NumVeloACOverlapHits
     *  Count the A/C side overlap hits on a track, after taking the OR of R
     *  and Phi hits.
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    struct GAUDI_API NumVeloACOverlapHits final : LoKi::BasicFunctors<const LHCb::Track*>::Function {
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      NumVeloACOverlapHits* clone() const override { return new NumVeloACOverlapHits( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "TrNVELOOVERLAPS"; }
      // ======================================================================
    };
    // ========================================================================
    /** @class NumVeloACOverlapHitsR
     *  Count the A/C side overlap R hits on a track
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    struct GAUDI_API NumVeloACOverlapHitsR final : LoKi::BasicFunctors<const LHCb::Track*>::Function {
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      NumVeloACOverlapHitsR* clone() const override { return new NumVeloACOverlapHitsR( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "TrNVELOOVERLAPSR"; }
      // ======================================================================
    };
    // ========================================================================
    /** @class NumVeloACOverlapHitsPhi
     *  Count the A/C side overlap Phi hits on a track
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    struct GAUDI_API NumVeloACOverlapHitsPhi final : LoKi::BasicFunctors<const LHCb::Track*>::Function {
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      NumVeloACOverlapHitsPhi* clone() const override { return new NumVeloACOverlapHitsPhi( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "TrNVELOOVERLAPSPHI"; }
      // ======================================================================
    };
    // ========================================================================
  } // namespace Track
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef  TrNCONSECVELOSPACEPOINTS
     *  Get the maximal number of consecutive Velo R-Phi space points on a
     *  track
     *  @see LoKi::Track::MaxNumConsecutiveVeloSpacePoints
     *  @see LoKi::Cuts::TrNVELOSPACEPOINTS
     *  @see LoKi::Track::NumVeloSpacePoints
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    typedef LoKi::Track::MaxNumConsecutiveVeloSpacePoints TrNCONSECVELOSPACEPOINTS;
    // ========================================================================
    /** @typedef  TrNVELOSPACEPOINTS
     *  Count the Velo R-Phi space points on a track
     *  @see LoKi::Track::NumVeloSpacePoints
     *  @see LoKi::Cuts::TrNCONSECVELOSPACEPOINTS
     *  @see LoKi::Track::MaxNumConsecutiveVeloSpacePoints
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    typedef LoKi::Track::NumVeloSpacePoints TrNVELOSPACEPOINTS;
    // ========================================================================
    /** @typedef TrNVELOOVERLAPS
     *  Count the A/C side overlap hits on a track, after taking the OR of R
     *  and Phi hits.
     *  @see LoKi::Track::NumVeloACOverlapHits
     *  @see LoKi::Cuts::TrNVELOOVERLAPSR
     *  @see LoKi::Track::NumVeloACOverlapHitsR
     *  @see LoKi::Cuts::TrNVELOOVERLAPSPHI
     *  @see LoKi::Track::NumVeloACOverlapHitsPhi
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    typedef LoKi::Track::NumVeloACOverlapHits TrNVELOOVERLAPS;
    // ========================================================================
    /** @typedef TrNVELOOVERLAPSR
     *  Count the A/C side overlap R hits on a track
     *  @see LoKi::Track::NumVeloACOverlapHitsR
     *  @see LoKi::Cuts::TrNVELOOVERLAPS
     *  @see LoKi::Track::NumVeloACOverlapHits
     *  @see LoKi::Cuts::TrNVELOOVERLAPSPHI
     *  @see LoKi::Track::NumVeloACOverlapHitsPhi
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    typedef LoKi::Track::NumVeloACOverlapHitsR TrNVELOOVERLAPSR;
    // ========================================================================
    /** @typedef TrNVELOOVERLAPSPHI
     *  Count the A/C side overlap R hits on a track
     *  @see LoKi::Track::NumVeloACOverlapHitsPhi
     *  @see LoKi::Cuts::TrNVELOOVERLAPS
     *  @see LoKi::Track::NumVeloACOverlapHits
     *  @see LoKi::Cuts::TrNVELOOVERLAPSR
     *  @see LoKi::Track::NumVeloACOverlapHitsR
     *  @author Wouter Hulsbergen wouterh@nikhef.nl
     *  @author Pieter David      pieter.david@cern.ch
     *  @date 2012-03-12
     */
    typedef LoKi::Track::NumVeloACOverlapHitsPhi TrNVELOOVERLAPSPHI;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_VELOHITPATTERNFUNCTIONS_H

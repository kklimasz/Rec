/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRACKFUNCTIONS_H
#define LOKI_TRACKFUNCTIONS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/State.h"
#include "Event/Track.h"
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/ValueWithError.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Track {
    // ========================================================================
    /** get the transverse momentum (and uncertainty) from the state
     *  @param state (INPUT) the  state
     *  @return transverse momentum (and uncertainty) from the given state
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-03-05
     */
    GAUDI_API
    Gaudi::Math::ValueWithError state_pt( const LHCb::State& state );
    // ========================================================================
    /** get the phi angle (and uncertainty) from the state
     *  @param state (INPUT) the  state
     *  @return phi angle  (and uncertainty) from the given state
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-03-05
     */
    GAUDI_API
    Gaudi::Math::ValueWithError state_phi( const LHCb::State& state );
    // ========================================================================
    /** get the theta angle (and uncertainty) from the state
     *  @param state (INPUT) the  state
     *  @return theta angle (and uncertainty) from the given state
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-03-05
     */
    GAUDI_API
    Gaudi::Math::ValueWithError state_theta( const LHCb::State& state );
    // ========================================================================
    /** get the pseudorapidity (and uncertainty) from the state
     *  @param state (INPUT) the  state
     *  @return pseudorapidity (and uncertainty) from the given state
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2016-03-05
     */
    GAUDI_API
    Gaudi::Math::ValueWithError state_eta( const LHCb::State& state );
    // ========================================================================
  } // namespace Track
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_TRACKFUNCTIONS_H

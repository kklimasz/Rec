/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackEngineActor.h"
#include "LoKi/Report.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hybrid::TrackEngineActor
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2004-06-29
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 */
// ============================================================================
// helper method to descrease number of lines
// ============================================================================
template <class TYPE>
inline StatusCode LoKi::Hybrid::TrackEngineActor::_add( const std::string& name, const TYPE& cut ) const {
  if ( m_stack.empty() ) { return LoKi::Report::Error( "LoKi::Hybrid::CoreEngineActor::addCut/Fun(): empty stack!" ); }
  //
  const Entry& entry = m_stack.top();
  // check the tool
  if ( !entry.first ) {
    return LoKi::Report::Error(
        "LoKi::Hybrid::TrackEngineActor::addCut/Fun(): LoKi::ITrackFunctorAntiFactory* is invalid!" );
  }
  // one more check
  if ( name != entry.first->name() ) {
    return LoKi::Report::Error(
        "LoKi::Hybrid::TrackEngineActor::addCut/Fun() : mismatch in LoKi::ITrackFunctorAntiFactory name!" );
  }
  // set the cut for the tool
  entry.first->set( cut );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
// accessor to the static instance
// ============================================================================
LoKi::Hybrid::TrackEngineActor& LoKi::Hybrid::TrackEngineActor::instance() {
  static LoKi::Hybrid::TrackEngineActor s_holder;
  return s_holder;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Hybrid::TrackEngineActor::TrackEngineActor() : m_stack() {}
// ============================================================================
// disconnect the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::disconnect( const LoKi::ITrackFunctorAntiFactory* factory ) {
  if ( m_stack.empty() ) { return LoKi::Report::Error( "LoKi::Hybrid::TrackEngineActor::disconnect: empty stack!" ); }
  //
  const Entry& entry = m_stack.top();
  //
  if ( entry.first == factory ) {
    m_stack.pop();
  } /// remove the last entry
  else {
    return LoKi::Report::Error( "LoKi::Hybrid::TrackEngineActor::disconnect: mismatch in tools " );
  }
  ///
  return StatusCode::SUCCESS;
}
// ============================================================================
// connect the hybrid tool for code translation
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::connect( const LoKi::ITrackFunctorAntiFactory* factory,
                                                    const LoKi::Context&                  context ) {
  //
  if ( !factory ) { return LoKi::Report::Error( "LoKi::Hybrid::TrackEngineActor::connect: Invalid factory" ); }
  m_stack.emplace( factory, context );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  get the current context
 *  contex is valid only inbetween <code>connect/disconnect</code>
 *  @return the current active context
 */
// ============================================================================
const LoKi::Context* LoKi::Hybrid::TrackEngineActor::context() const {
  if ( m_stack.empty() ) {
    LoKi::Report::Error( "LoKi::Hybrid::TrackEngineActor::context: empty stack" );
    return nullptr;
  }
  const Entry& last = m_stack.top();
  return &last.second;
}
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string& name, const LoKi::Types::TrCuts& cut ) const {
  return _add( name, cut );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string& name, const LoKi::Types::TrFunc& func ) const {
  return _add( name, func );
}
// ============================================================================
// functional part
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string& name, const LoKi::Types::TrMaps& func ) const {
  return _add( name, func );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string& name, const LoKi::Types::TrPipes& func ) const {
  return _add( name, func );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string&            name,
                                                    const LoKi::Types::TrFunVals& func ) const {
  return _add( name, func );
}
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrackEngineActor::process( const std::string&            name,
                                                    const LoKi::Types::TrSources& func ) const {
  return _add( name, func );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================

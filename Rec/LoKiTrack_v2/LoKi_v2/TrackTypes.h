/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"
#include "LoKi/BiFunctions.h"
#include "LoKi/Functions.h"

// ============================================================================
namespace LoKi::Pr {
  // ==========================================================================
  /** @namespace LoKi::Pr::TrackTypes
   *  helper namespace  to collect the types needed for "Track"-functions
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELAYEV, Sascha Stahl
   */
  using TrackType       = LHCb::Event::v2::Track;
  using VertexContainer = std::vector<LHCb::Event::v2::RecVertex>;
  namespace TrackTypes {
    // ========================================================================
    // the regular functors for Tracks
    // ========================================================================
    /// type of 'cuts' for Tracks       (interface)
    using TrCuts = LoKi::BasicFunctors<TrackType const*>::Predicate;
    /// type of 'functions' for Tracks  (interface)
    using TrFunc = LoKi::BasicFunctors<TrackType const*>::Function;
    /// type of 'cuts' for Tracks       (assignable)
    // using TrCut = LoKi::FunctorFromFunctor<TrackType const *, bool>;
    using TrCut = LoKi::Assignable_t<TrCuts>;
    // typedef LoKi::Assignable<TrCuts>::Type                     TrCut  ;
    /// type of 'functions' for Tracks  (assignable)
    // using TrFun = LoKi::FunctorFromFunctor<TrackType const *, double>;
    using TrFun = LoKi::Assignable_t<TrFunc>;
    // typedef LoKi::Assignable<TrFunc>::Type                     TrFun  ;
    // ========================================================================
  } // namespace TrackTypes
  // ==========================================================================

  namespace Types {
    // ========================================================================
    // regular functions/predicates for tracks: "Tr"
    // ========================================================================
    /// type of 'cuts' for Tracks       (interface)
    using TrCuts = LoKi::Pr::TrackTypes::TrCuts;
    /// type of 'functions' for Tracks  (interface)
    using TrFunc = LoKi::Pr::TrackTypes::TrFunc;
    /// type of 'cuts' for Tracks       (assignable)
    using TrCut = LoKi::Pr::TrackTypes::TrCut;
    /// type of 'functions' for Tracks  (assignable)
    using TrFun = LoKi::Pr::TrackTypes::TrFun;

  } // namespace Types
  // ==========================================================================
} // namespace LoKi::Pr
// ============================================================================
namespace LoKi {
  // ==========================================================================
  // the specialized printout
  // ==========================================================================
  template <>
  std::ostream& Constant<LoKi::Pr::TrackType const*, bool>::fillStream( std::ostream& s ) const;
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "GaudiKernel/Kernel.h"
#include "LoKi/Interface.h"
#include "LoKi/TES.h"
#include "LoKi_v2/TrackTypes.h"
#include <boost/callable_traits.hpp>

// ============================================================================
namespace LoKi::Pr::Track {
  // ==========================================================================
  /** @namespace LoKi::Pr::Track Track.h LoKi_v2/Track.h
   *  Namespace with few basic "track"-functors
   *  @see LHCb::Event::v2::Track
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  // ========================================================================
  namespace details {
    template <size_t N, typename meta>
    using arg_t = std::tuple_element_t<N, boost::callable_traits::args_t<decltype( meta::fun )>>;

    template <size_t N, typename meta>
    using parg_t = std::add_pointer_t<arg_t<N, meta>>;

    /** @class details::UnaryFun
     *  type erasure wrapper to convert a small 'meta' struct which defines an
     *  unary callable 'fun' and a const char* 'symbol()' into a LoKi functor
     *
     *  @author Gerhard Raven
     */
    template <typename meta>
    struct GAUDI_API UnaryFun final : LoKi::BasicFunctors<parg_t<0, meta>>::Function {
      // ======================================================================
      /// Default Constructor
      UnaryFun() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("virtual constructor")
      UnaryFun* clone() const override { return new UnaryFun( *this ); }
      /// mandatory: the only one essential method
      double operator()( parg_t<0, meta> t ) const override {
        assert( t != nullptr );
        return std::invoke( meta::fun, *t );
      }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << meta::symbol; }
      // ======================================================================
    };
  } // namespace details
  // ========================================================================
  /** @class Momentum
   *  @see LoKi::Pr::Cuts::TrP
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  namespace details {
    struct Momentum {
      static constexpr auto       fun      = &LoKi::Pr::TrackType::p;
      static constexpr const char symbol[] = "TrP";
    };
  } // namespace details
  using Momentum = details::UnaryFun<details::Momentum>;

  // ========================================================================
  /** @class Momentum
   *  @see LoKi::Pr::Cuts::TrP
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  namespace details {
    struct TransverseMomentum {
      static constexpr auto       fun      = &LoKi::Pr::TrackType::pt;
      static constexpr const char symbol[] = "TrPT";
    };
  } // namespace details
  using TransverseMomentum = details::UnaryFun<details::TransverseMomentum>;

  // ========================================================================
  namespace details {
    struct Chi2 {
      static constexpr auto       fun      = &LoKi::Pr::TrackType::chi2;
      static constexpr const char symbol[] = "TrCHI2";
    };
  } // namespace details
  using Chi2 = details::UnaryFun<details::Chi2>;

  // ========================================================================
  /** @class Chi2PerDoF
   *  simple evaluator of the LHcb::Track::chi2PerDoF
   *  @see LoKi::Pr::Cuts::TrCHI2PDOF
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  namespace details {
    struct Chi2PerDoF {
      static constexpr auto       fun      = &LoKi::Pr::TrackType::chi2PerDoF;
      static constexpr const char symbol[] = "TrCHI2PDOF";
    };
  } // namespace details
  using Chi2PerDoF = details::UnaryFun<details::Chi2PerDoF>;

  // ========================================================================
  /** @class CheckFlag
   *  @see LoKi::Pr::Cuts::TrISFLAG
   *  @see LoKi::Pr::Cuts::TrBACKWARD
   *  @see LoKi::Pr::Cuts::TrINVALID
   *  @simple predicate to check the flag
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  class GAUDI_API CheckFlag final : public Types::TrCuts {
  public:
    // ======================================================================
    /// constructor from the flag ;
    CheckFlag( TrackType::Flag flag );
    /// MANDATORY: clone method ("virtual constructor")
    CheckFlag* clone() const override { return new CheckFlag( *this ); }
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override { return t->checkFlag( m_flag ); };
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  private:
    // ======================================================================
    // the flag to be checked:
    TrackType::Flag m_flag; ///< the flag to be checked:
    // ======================================================================
  };

  // ========================================================================
  /** @class MinimalImpactParameter
   *  @see LoKi::Pr::Cuts::TrMinIP
   *  simple evaluator of minimal impact parameter to and PV of the track
   *  @author Sascha Stahl
   */
  class GAUDI_API MinimalImpactParameter final : public Types::TrFunc, public LoKi::TES::DataHandle<VertexContainer> {
  public:
    MinimalImpactParameter( const GaudiAlgorithm* algorithm, const std::string& location );
    MinimalImpactParameter( const GaudiAlgorithm* algorithm );
    MinimalImpactParameter* clone() const override { return new MinimalImpactParameter( *this ); };
    result_type             operator()( argument track ) const override;
    std::ostream&           fillStream( std::ostream& s ) const override;
    // ======================================================================
  };
  // ========================================================================
  /** @class MinimalImpactParameterGreaterThan
   *  @see LoKi::Pr::Cuts::TrMinIPGreaterThan
   *  simple evaluator of minimal impact parameter to and PV of the track
   *  @author Sascha Stahl
   */
  class GAUDI_API MinimalImpactParameterGreaterThan final : public Types::TrCuts,
                                                            public LoKi::TES::DataHandle<VertexContainer> {
  public:
    MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm, double cut_value, const std::string& location );
    MinimalImpactParameterGreaterThan( const GaudiAlgorithm* algorithm, double cut_value );
    MinimalImpactParameterGreaterThan* clone() const override {
      return new MinimalImpactParameterGreaterThan( *this );
    };
    result_type   operator()( argument track ) const override;
    std::ostream& fillStream( std::ostream& s ) const override;

  private:
    double m_cut_value = 0.0;
    // ======================================================================
  };
  // ========================================================================

  // ========================================================================
  /** @class MinimalImpactParameterChi2
   *  @see LoKi::Tracks::TrMINIPCHI2
   *  Evaluate the minimum impact parameter chi2 of the track to any vertex in the given location.
   *  @author Olli Lupton
   */
  class GAUDI_API MinimalImpactParameterChi2 final : public Types::TrFunc,
                                                     public LoKi::TES::DataHandle<VertexContainer> {
  public:
    MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location );
    MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm );
    MinimalImpactParameterChi2* clone() const override { return new MinimalImpactParameterChi2( *this ); };
    result_type                 operator()( argument track ) const override;
    std::ostream&               fillStream( std::ostream& s ) const override;
  };

  // ========================================================================
  /** @class MinimalImpactParameterChi2Cut
   *  @see LoKi::Tracks::TrMINIPCHI2CUT
   *  Check whether the minimum impact parameter chi2 of the track to any vertex in the given location exceeds the
   *  given cut.
   *  @author Olli Lupton
   */
  class GAUDI_API MinimalImpactParameterChi2Cut final : public Types::TrCuts,
                                                        public LoKi::TES::DataHandle<VertexContainer> {
    double m_ipchi2_cut{0.0};

  public:
    MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut, const std::string& location );
    MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut );
    MinimalImpactParameterChi2Cut* clone() const override { return new MinimalImpactParameterChi2Cut( *this ); };
    result_type                    operator()( argument track ) const override;
    std::ostream&                  fillStream( std::ostream& s ) const override;
  };

} //                                            end of namespace LoKi::Pr::Track

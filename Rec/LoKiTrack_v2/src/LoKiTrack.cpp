/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LoKi_v2/LoKiTrack.h"
#include "LoKi_v2/ITrackFunctorAntiFactory.h"
#include "LoKi_v2/ITrackFunctorFactory.h"
#include "LoKi_v2/Track.h"
#include "LoKi_v2/TrackEngine.h"
#include "LoKi_v2/TrackEngineActor.h"
#include "LoKi_v2/TrackFactoryLock.h"
#include "LoKi_v2/TrackTypes.h"
// =======================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV, Sascha Stahl
 */
// ============================================================================
// the specific printout
// ============================================================================
template <>
std::ostream& LoKi::Functors::Empty<LoKi::Pr::TrackType const*>::fillStream( std::ostream& s ) const {
  return s << "TrEMPTY";
}
// ============================================================================
// the specific printpout
// ============================================================================
template <>
std::ostream& LoKi::Functors::Size<LoKi::Pr::TrackType const*>::fillStream( std::ostream& s ) const {
  return s << "TrSIZE";
}
// ============================================================================
// the specific printpout
// ============================================================================
template <>
std::ostream& LoKi::Constant<LoKi::Pr::TrackType const*, bool>::fillStream( std::ostream& s ) const {
  return s << ( this->m_value ? "TrALL" : "TrNONE" );
}
// ============================================================================
// The END
// ============================================================================

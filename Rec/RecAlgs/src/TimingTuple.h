/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TIMINGTUPLE_H
#define TIMINGTUPLE_H 1

#include <algorithm>
#include <atomic>

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiKernel/Memory.h"

#include "Event/ODIN.h"
#include "Event/RecSummary.h"

class ISequencerTimerTool;

/** @class TimingTuple TimingTuple.h
 *
 *  Fill a Tuple with timing, memory and some event variables
 *
 *  @author Patrick Koppenburg
 *  @date   2010-08-18
 */
class TimingTuple final : public Gaudi::Functional::Consumer<void( const LHCb::ODIN&, const LHCb::RecSummary& ),
                                                             Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  /// Standard constructor
  TimingTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                                                   ///< Algorithm initialization
  void       operator()( const LHCb::ODIN&, const LHCb::RecSummary& ) const override; ///< Algorithm execution

private:
  ISequencerTimerTool*                    m_timerTool  = nullptr; ///< timer tool
  int                                     m_timer      = 0;       ///< timer index
  mutable std::atomic<unsigned long long> m_evtCounter = {0};

  template <class TYPE>
  void fillTuple( Tuple& tuple, const std::string& var, const TYPE number ) const {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Filling " << var << " with " << number << endmsg;
    tuple->column( var, number );
  }
};
#endif // TIMINGTUPLE_H

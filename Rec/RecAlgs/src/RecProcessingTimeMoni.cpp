/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "RecProcessingTimeMoni.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RecProcessingTimeMoni
//
// 2010-07-15 : Chris Jones
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RecProcessingTimeMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RecProcessingTimeMoni::RecProcessingTimeMoni( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator ) {
  setProperty( "HistoTopDir", "Timing/" );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode RecProcessingTimeMoni::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;

  // are we properly configured
  if ( m_algNames.empty() ) { sc = Warning( "No algorithms to time !" ); }

  // book the histogram at initialization time
  m_hist = book( "overallTime", "log10(Event Processing Time / ms)", m_logMinTime, m_logMaxTime, 100 );

  // return
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RecProcessingTimeMoni::execute() {

  // Loop over algorithms to include in the timing and add them up
  double time = 0;
  for ( const auto& name : m_algNames ) {
    const auto alg_time = chronoSvc()->chronoDelta( name + ":Execute", IChronoStatSvc::ELAPSED ) / 1000;
    time += alg_time;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << name << " " << alg_time << endmsg;
  }

  // only fill if algorithm(s) ran (time>0)
  if ( time > 0 ) {
    // Take the base 10 log of the time (helps show the large tails)
    fill( m_hist, std::log10( time ), 1.0 );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECALGS_RECPROCESSINGTIMEMONI_H
#define RECALGS_RECPROCESSINGTIMEMONI_H 1

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// from Aida
#include "AIDA/IHistogram1D.h"

/** @class RecProcessingTimeMoni RecProcessingTimeMoni.h
 *
 *  Simple monitor making basic processing time plots for the Reconstruction
 *
 *  @author Chris Jones
 *  @date   2010-07-15
 */

class RecProcessingTimeMoni final : public GaudiHistoAlg {

public:
  /// Standard constructor
  RecProcessingTimeMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// Definition of algorithm name list
  typedef std::vector<std::string> AlgorithmNames;

  AIDA::IHistogram1D* m_hist = nullptr; ///< Pointer to processing time histogram

  Gaudi::Property<AlgorithmNames> m_algNames{this, "Algorithms"}; ///< List of algorithm(s) to include in timing
  Gaudi::Property<double>         m_logMaxTime{this, "LogMaxEventTime",
                                       8.0}; ///< Job Option for log10(maximum overall processing time) for plots
  Gaudi::Property<double>         m_logMinTime{this, "LogMinEventTime",
                                       -2.0}; ///< Job Option for log10(minimum overall processing time) for plots
};

#endif // RECALGS_RECPROCESSINGTIMEMONI_H

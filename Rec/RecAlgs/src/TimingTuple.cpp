/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TimingTuple.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TimingTuple
//
// 2010-08-18 : Patrick Koppenburg
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TimingTuple )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TimingTuple::TimingTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"RecSummaryLocation", LHCb::RecSummaryLocation::Default}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TimingTuple::initialize() {
  const StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) return sc;

  m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" ); // global tool
  m_timer     = m_timerTool->addTimer( name() );
  m_timerTool->start( m_timer ); // start it now

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
void TimingTuple::operator()( const LHCb::ODIN& odin, const LHCb::RecSummary& summary ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_evtCounter;

  const double t = m_timerTool->stop( m_timer ); // stop
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Time is " << t << endmsg;
  m_timerTool->start( m_timer ); // start again

  // Fill tuple
  Tuple tuple = nTuple( "TimingTuple", "TimingTuple" );

  fillTuple( tuple, "EventInSequence", m_evtCounter.load() );
  fillTuple( tuple, "RunNumber", odin.runNumber() );
  fillTuple( tuple, "EvtNumber", (int)odin.eventNumber() );
  fillTuple( tuple, "Memory", (double)System::virtualMemory() );
  fillTuple( tuple, "Time", t );

  fillTuple( tuple, "nPVs", summary.info( LHCb::RecSummary::DataTypes::nPVs, -999 ) );
  fillTuple( tuple, "VeloClusters", summary.info( LHCb::RecSummary::DataTypes::nVeloClusters, -999 ) );
  fillTuple( tuple, "TTClusters", summary.info( LHCb::RecSummary::DataTypes::nTTClusters, -999 ) );
  fillTuple( tuple, "ITClusters", summary.info( LHCb::RecSummary::DataTypes::nITClusters, -999 ) );
  fillTuple( tuple, "OTClusters", summary.info( LHCb::RecSummary::DataTypes::nOTClusters, -999 ) );
  fillTuple( tuple, "spdMult", summary.info( LHCb::RecSummary::DataTypes::nSPDhits, -999 ) );

  fillTuple( tuple, "MuonCoordsS0", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS0, -999 ) );
  fillTuple( tuple, "MuonCoordsS1", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS1, -999 ) );
  fillTuple( tuple, "MuonCoordsS2", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS2, -999 ) );
  fillTuple( tuple, "MuonCoordsS3", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS3, -999 ) );
  fillTuple( tuple, "MuonCoordsS4", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS4, -999 ) );
  fillTuple( tuple, "MuonTracks", summary.info( LHCb::RecSummary::DataTypes::nMuonTracks, -999 ) );

  fillTuple( tuple, "BestTracks", summary.info( LHCb::RecSummary::DataTypes::nTracks, -999 ) );
  fillTuple( tuple, "BackwardTracks", summary.info( LHCb::RecSummary::DataTypes::nBackTracks, -999 ) );
  fillTuple( tuple, "VeloTracks", summary.info( LHCb::RecSummary::DataTypes::nVeloTracks, -999 ) );
  fillTuple( tuple, "LongTracks", summary.info( LHCb::RecSummary::DataTypes::nLongTracks, -999 ) );
  fillTuple( tuple, "DownstreamTracks", summary.info( LHCb::RecSummary::DataTypes::nDownstreamTracks, -999 ) );
  fillTuple( tuple, "UpstreamTracks", summary.info( LHCb::RecSummary::DataTypes::nUpstreamTracks, -999 ) );

  fillTuple( tuple, "Rich1Hits", summary.info( LHCb::RecSummary::DataTypes::nRich1Hits, -999 ) );
  fillTuple( tuple, "Rich2Hits", summary.info( LHCb::RecSummary::DataTypes::nRich2Hits, -999 ) );

  tuple->write();
}

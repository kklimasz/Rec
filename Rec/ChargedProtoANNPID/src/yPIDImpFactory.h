/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// gaudi
#include "GaudiKernel/GaudiException.h"

// STL
#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

// Include VDT here so it gets into all yPID compilations
// TODO: check whether yPID needs this
//#include "vdt/exp.h"

// IClassifierReader interface
#include "vdt/exp.h"

// yPID IClassifierReader interface
#include "yPIDIClassifierReader.h"

namespace ANNGlobalPID {

  /** @class yPIDImpFactory yPIDImpFactory.h
   *
   *  Factory to create instances of standalone C++ yPID objects for
   *  a given track, hypothesis and network tuning.
   *
   *  @author Chris Jones
   *  @date   2013-03-12
   */
  class yPIDImpFactory final {

  public:
    // vector of input names
    typedef std::vector<std::string> InputNames;

  private:
    /** @class yPIDFactoryBase yPIDImpFactory.h
     *
     *  Base class for factories
     *  from:
     *  @author Chris Jones
     *  @date   2013-03-12
     *  by
     *  @author Denis Derkach
     *  @date 2017-05-28
     */
    class yPIDFactoryBase {
    public:
      /// Create an instance of the yPID classifier for this factory
      virtual std::unique_ptr<yPIDIClassifierReader> create( const InputNames& inputs ) = 0;
      /// Destructor
      virtual ~yPIDFactoryBase() = default;
    };

    /** @class yPIDFactory yPIDImpFactory.h
     *
     *  Templated class for specific yPID factories
     *
     *  @author Chris Jones
     *  @date   2013-03-12
     *  @author Denis Derkach
     *  @date 2017-05-28
     */
    template <class YPIDTYPE>
    class yPIDFactory final : public yPIDFactoryBase {
    public:
      /// Create an instance of the yPID classifier for this factory
      std::unique_ptr<yPIDIClassifierReader> create( const InputNames& inputs ) override {
        return std::make_unique<YPIDTYPE>( inputs );
      }
    };

  public:
    /// Standard constructor
    yPIDImpFactory();

  private:
    // methods to declare new MVAs to the factory during construction
    // Seperated out to allow seperate compilation units (as the MVAs
    // themselves can be heavy so compiling all at once does not scale well).

    // NB. Remember any new methods added here for new tunes needs to
    //     be called from the constructor in yPIDImpFactory.cpp

    void addMC15TuneFLAT4dV1();
    void addMC15TuneDNNV1();
    void addMC15TuneBDNNV1();
    void addMC15TuneCatBoostV1();

  private:
    /// Returns the id string for a given configuration
    inline std::string id( const std::string& config, const std::string& particle, const std::string& track ) const {
      return config + "-" + particle + "-" + track;
    }

    /// Register a new yPID instance with the factory
    template <class YPIDTYPE>
    inline void add( const std::string& config, const std::string& particle, const std::string& track ) {
      const auto _id = id( config, particle, track );
      const auto i   = m_map.find( _id );
      if ( i != m_map.end() ) {
        throw GaudiException( _id + " already registered", "ANNGlobalPID::yPIDImpFactory", StatusCode::FAILURE );
      }
      m_map[_id] = std::make_unique<yPIDFactory<YPIDTYPE>>();
    }

  public:
    /// Get an instance for a given set of parameters
    inline std::unique_ptr<yPIDIClassifierReader> create( const std::string& config, const std::string& particle,
                                                          const std::string&              track,
                                                          const std::vector<std::string>& inputs ) const {
      const auto i = m_map.find( id( config, particle, track ) );
      return ( i != m_map.end() ? i->second->create( inputs ) : nullptr );
    }

  private:
    /// Type for internal map
    typedef std::unordered_map<std::string, std::unique_ptr<yPIDFactoryBase>> Map;

  private:
    /// The map of known yPID instance generators linked to a given nickname
    Map m_map;
  };

  /// Method to get a static instance of the factory
  const yPIDImpFactory& ypidFactory();

} // namespace ANNGlobalPID

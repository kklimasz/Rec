/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "yPIDImpFactory.h"

// Long tracks FLAT4d
#include "yPID/MC15TuneV1/GlobalPID_Electron_Long_FLAT4d.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Ghost_Long_FLAT4d.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Kaon_Long_FLAT4d.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Muon_Long_FLAT4d.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Pion_Long_FLAT4d.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Proton_Long_FLAT4d.class.C"
// Long tracks DNN
#include "yPID/MC15TuneV1/GlobalPID_Electron_Long_DNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Ghost_Long_DNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Kaon_Long_DNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Muon_Long_DNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Pion_Long_DNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Proton_Long_DNN.class.C"
// Long tracks BDNN
#include "yPID/MC15TuneV1/GlobalPID_Electron_Long_BDNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Ghost_Long_BDNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Kaon_Long_BDNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Muon_Long_BDNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Pion_Long_BDNN.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Proton_Long_BDNN.class.C"
// Long tracks CatBoost
#include "yPID/MC15TuneV1/GlobalPID_Electron_Long_CatBoost.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Ghost_Long_CatBoost.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Kaon_Long_CatBoost.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Muon_Long_CatBoost.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Pion_Long_CatBoost.class.C"
#include "yPID/MC15TuneV1/GlobalPID_Proton_Long_CatBoost.class.C"

namespace ANNGlobalPID {

  void yPIDImpFactory::addMC15TuneFLAT4dV1() {
    const std::string tune = "MC15TuneFLAT4dV1";
    // Long
    {
      const std::string tkType = "Long";
      add<ReadElectron_Long_FLAT4d>( tune, "electron", tkType );
      add<ReadMuon_Long_FLAT4d>( tune, "muon", tkType );
      add<ReadPion_Long_FLAT4d>( tune, "pion", tkType );
      add<ReadKaon_Long_FLAT4d>( tune, "kaon", tkType );
      add<ReadProton_Long_FLAT4d>( tune, "proton", tkType );
      add<ReadGhost_Long_FLAT4d>( tune, "ghost", tkType );
    }
  }

  void yPIDImpFactory::addMC15TuneDNNV1() {
    const std::string tune = "MC15TuneDNNV1";
    // Long
    {
      const std::string tkType = "Long";
      add<ReadElectron_Long_DNN>( tune, "electron", tkType );
      add<ReadMuon_Long_DNN>( tune, "muon", tkType );
      add<ReadPion_Long_DNN>( tune, "pion", tkType );
      add<ReadKaon_Long_DNN>( tune, "kaon", tkType );
      add<ReadProton_Long_DNN>( tune, "proton", tkType );
      add<ReadGhost_Long_DNN>( tune, "ghost", tkType );
    }
  }

  void yPIDImpFactory::addMC15TuneBDNNV1() {
    const std::string tune = "MC15TuneBDNNV1";
    // Long
    {
      const std::string tkType = "Long";
      add<ReadElectron_Long_BDNN>( tune, "electron", tkType );
      add<ReadMuon_Long_BDNN>( tune, "muon", tkType );
      add<ReadPion_Long_BDNN>( tune, "pion", tkType );
      add<ReadKaon_Long_BDNN>( tune, "kaon", tkType );
      add<ReadProton_Long_BDNN>( tune, "proton", tkType );
      add<ReadGhost_Long_BDNN>( tune, "ghost", tkType );
    }
  }

  void yPIDImpFactory::addMC15TuneCatBoostV1() {
    const std::string tune = "MC15TuneCatBoostV1";
    // Long
    {
      const std::string tkType = "Long";
      add<ReadElectron_Long_CatBoost>( tune, "electron", tkType );
      add<ReadMuon_Long_CatBoost>( tune, "muon", tkType );
      add<ReadPion_Long_CatBoost>( tune, "pion", tkType );
      add<ReadKaon_Long_CatBoost>( tune, "kaon", tkType );
      add<ReadProton_Long_CatBoost>( tune, "proton", tkType );
      add<ReadGhost_Long_CatBoost>( tune, "ghost", tkType );
    }
  }

  // Standard constructor
  yPIDImpFactory::yPIDImpFactory() {
    addMC15TuneFLAT4dV1();
    addMC15TuneDNNV1();
    addMC15TuneBDNNV1();
    addMC15TuneCatBoostV1();
  }

  // Method to get a static instance of the factory
  const yPIDImpFactory& ypidFactory() {
    static yPIDImpFactory factory;
    return factory;
  }

} // namespace ANNGlobalPID

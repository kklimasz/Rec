/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadElectron_Long_TMVA

#include "../../yPIDIClassifierReader.h"

#include <cmath>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

// Include values transformations
#include "preprocessing/preprocessing.h"

// Include Keras model
#include "keras/keras_model.h"

///////////////////////////////////////////// DNN //////////////////////////////////////////////////

class ReadElectron_Long_DNN final : public yPIDIClassifierReader {

public:
  std::vector<std::string>    inputVars;
  IronTransformer             iron;
  std::unique_ptr<KerasModel> model = std::make_unique<KerasModel>();
  // List of vars
  std::vector<std::string> needed_vars = {"MuonNShared",      "MuonIsLooseMuon",   "MuonIsMuon",
                                          "MuonBkgLL",        "MuonMuLL",          "TrackFitVeloChi2",
                                          "TrackFitVeloNDoF", "TrackFitMatchChi2", "TrackGhostProbability",
                                          "TrackP",           "TrackChi2PerDof",   "TrackFitTChi2",
                                          "TrackPt",          "TrackNumDof",       "TrackFitTNDoF",
                                          "TrackDOCA",        "InAccSpd",          "InAccPrs",
                                          "InAccBrem",        "InAccEcal",         "InAccHcal",
                                          "InAccMuon",        "CombDLLmu",         "CombDLLpi",
                                          "CombDLLp",         "CombDLLe",          "CombDLLk",
                                          "RichAboveMuThres", "RichAboveElThres",  "RichAbovePiThres",
                                          "RichAboveKaThres", "RichAbovePrThres",  "RichUsedR1Gas",
                                          "RichUsedR2Gas",    "RichDLLbt",         "RichDLLpi",
                                          "RichDLLe",         "RichDLLp",          "RichDLLmu",
                                          "RichDLLk",         "CaloBremMatch",     "CaloElectronMatch",
                                          "CaloTrMatch",      "CaloTrajectoryL",   "CaloChargedSpd",
                                          "CaloChargedPrs",   "CaloChargedEcal",   "CaloNeutralSpd",
                                          "CaloNeutralPrs",   "CaloNeutralEcal",   "CaloSpdE",
                                          "CaloPrsE",         "CaloEcalE",         "CaloHcalE",
                                          "EcalPIDmu",        "HcalPIDmu",         "PrsPIDe",
                                          "BremPIDe",         "EcalPIDe",          "HcalPIDe"};

  // constructor
  ReadElectron_Long_DNN( const std::vector<std::string>& theInputVars ) : yPIDIClassifierReader() {
    std::vector<std::string> vars( theInputVars.begin(), theInputVars.end() );
    inputVars = vars;

    // Get path to a root directory with models
    const std::string paramEnv  = "CHARGEDPROTOANNPIDPARAMROOT";
    std::string       paramRoot = std::string( getenv( paramEnv.c_str() ) );

    // Load Iron Transformer
    std::string filename = paramRoot + "/data/MC15TuneDNNV1/iron_transform.txt";
    iron.read_transforms( filename );

    // Load Keras DNN model
    std::string model_path = paramRoot + "/data/MC15TuneDNNV1/cpp_dl_model";
    model->LoadModel( model_path );
  }

  double GetMvaValue( const std::vector<double>& inputValues ) const override;

private:
  // method-specific destructor
  // void //Clear();

  // input variable transformation
};

////////////////////////////////////////////////////////////////////////////////////////////////////

inline double ReadElectron_Long_DNN::GetMvaValue( const std::vector<double>& inputValues ) const {
  std::vector<double>      transfromed_inputValues( inputValues.begin(), inputValues.end() );
  std::vector<std::string> transfromed_inputVars( inputVars.begin(), inputVars.end() );
  // std::vector<double> transfromed_inputValues = inputValues;
  // std::vector<std::string> transfromed_inputVars = inputVars;

  // Preprocessing
  preprocess( transfromed_inputValues, transfromed_inputVars, iron );
  transfromed_inputValues = select_features( transfromed_inputValues, transfromed_inputVars, needed_vars );

  // Get predictions
  Tensor input_tensor;
  Tensor output_tensor;

  std::vector<float> vals( transfromed_inputValues.begin(), transfromed_inputValues.end() );
  input_tensor.data_ = vals;
  // KerasModel *model2;
  // model2 = GetModel(model);
  model->Apply( &input_tensor, &output_tensor );

  std::vector<float> predictions = output_tensor.data_;

  // Retunr prediction for a particle
  double retval = (double)predictions[1];
  // 0: "Ghost", 1: "Electron", 2: "Muon", 3: "Pion", 4: "Kaon", 5: "Proton"

  // double retval = 0.2;
  return retval;
}

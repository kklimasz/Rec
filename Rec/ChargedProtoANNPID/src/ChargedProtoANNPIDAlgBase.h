/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoANNPIDAlgBase.h
 *
 * Header file for algorithm ChargedProtoANNPIDAlgBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2010-03-09
 */
//-----------------------------------------------------------------------------

#ifndef ChargedProtoANNPID_ChargedProtoANNPIDAlgBase_H
#define ChargedProtoANNPID_ChargedProtoANNPIDAlgBase_H 1

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

// local
#include "ChargedProtoANNPIDCommonBase.h"

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDAlgBase ChargedProtoANNPIDAlgBase.h
   *
   *  Base class for all ProtoParticle ANN based PID algorithms
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoANNPIDAlgBase : public ANNGlobalPID::ChargedProtoANNPIDCommonBase<GaudiTupleAlg> {

  public:
    /// Standard constructor
    ChargedProtoANNPIDAlgBase( const std::string& name, ISvcLocator* pSvcLocator )
        : ANNGlobalPID::ChargedProtoANNPIDCommonBase<GaudiTupleAlg>( name, pSvcLocator ) {}
  };

} // namespace ANNGlobalPID

#endif // ChargedProtoANNPID_ChargedProtoANNPIDAlgBase_H

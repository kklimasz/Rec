
# Training sample sizes. -1 for each means 
# "Use a 1:1 signal:background mixture of the maximum size you can"
NumSignalEntries     = 2000000
NumBackgroundEntries = 2000000
#NumSignalEntries     = 50000
#NumBackgroundEntries = 50000

# Reweighting
ReweightForOneToOne = no

# TMVA Training names
TMVANAMES = MLP1,MLP2

# MLP-BP-CE Configuration

MLP1:TMVAMethod = MLP
MLP1:MLPLayerTwoScale = 1.3
MLP1:TMVAConfig = !H:V:EpochMonitoring:UseRegulator=false:VarTransform=Norm:NCycles=750:NeuronType=tanh:TrainingMethod=BP:EstimatorType=CE

MLP2:TMVAMethod = MLP
MLP2:MLPLayerTwoScale = 1.3
MLP2:TMVAConfig = !H:V:EpochMonitoring:UseRegulator=false:VarTransform=Norm:NCycles=750:NeuronType=sigmoid:TrainingMethod=BP:EstimatorType=CE

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECINTERFACES_ICHARGEDPROTOANNPIDTOOL_H
#define RECINTERFACES_ICHARGEDPROTOANNPIDTOOL_H 1

// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Event Model
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"

namespace ANNGlobalPID {

  /** @class IChargedProtoANNPIDTool RecInterfaces/IChargedProtoANNPIDTool.h
   *
   *  Interface to tool to access the ANN PID information
   *
   *  @author Chris Jones
   *  @date   2014-06027
   */
  struct IChargedProtoANNPIDTool : extend_interfaces<IAlgTool> {

    DeclareInterfaceID( IChargedProtoANNPIDTool, 2, 0 );

    /// The return type for the tool. Result with status code.
    struct RetType final {
      RetType( bool _s = false, double _v = -3 ) : status( _s ), value( _v ) {}
      bool   status;
      double value;
    };

    /** Access the ANNPID value for a given ProtoParticle, tune and PID type
     *  @param proto Pointer to the ProtoParticle to fill into the tuple
     *  @param pid The PID to assume for this ProtoParticle
     *  @param annPIDTune The ANNPID tune to use
     *  @return The ANNPID result (status code and value)
     */
    virtual RetType annPID( const LHCb::ProtoParticle* proto, const LHCb::ParticleID& pid,
                            const std::string& annPIDTune ) const = 0;

    /** Access the ANNPID value for a given ProtoParticle, tune and PID type
     *  @param part Pointer to the Particle to fill into the tuple
     *  @param pid The PID to assume for this ProtoParticle
     *  @param annPIDTune The ANNPID tune to use
     *  @return The ANNPID result (status code and value)
     */
    RetType annPID( const LHCb::Particle* part, const LHCb::ParticleID& pid, const std::string& annPIDTune ) const {
      return ( part ? annPID( part->proto(), pid, annPIDTune ) : -1 );
    }
  };

} // namespace ANNGlobalPID

#endif // RECINTERFACES_ICHARGEDPROTOANNPIDTUPLETOOL_H

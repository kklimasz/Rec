/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef XGBCLASSIFIER_H
#define XGBCLASSIFIER_H

#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"
#include "xgboost/c_api.h"
#include <string>
#include <vector>
//#include "/afs/cern.ch/user/v/vchekali/public/xgboost/include/xgboost/c_api.h"

class IClassifier {
public:
  virtual ~IClassifier() = default;

  /**    * @brief      Main classification method    *
   * Takes a vector of values of features and returns the corresponding MVA
   * output.
   *    * @param[in]  featureValues  A vector of feature values
   *    * @return     MVA classifier value    */
  virtual double getClassifierValues( const std::vector<double>& featureValues ) = 0;
};

class XGBClassifierPhPi0 : public IClassifier {
public:
  XGBClassifierPhPi0();
  XGBClassifierPhPi0( const std::string& path );
  double                     getClassifierValues( const std::vector<double>& featureValues ) override;
  void                       setPath( const std::string& path );
  ServiceHandle<IMessageSvc> msgh = ServiceHandle<IMessageSvc>( "MessageSvc", "XGBLog" );
  MsgStream                  log  = MsgStream( &( *msgh ), "XGBLog" );

private:
  std::string        xgb_path           = "def_path";
  std::vector<float> m_predictionsCache = {0, 0};
  DMatrixHandle      m_cache_matrix, m_feature_matrix;
  BoosterHandle      m_booster;
};

#endif // XGBCLASSIFIER_H

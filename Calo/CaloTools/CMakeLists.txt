###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: CaloTools
################################################################################
gaudi_subdir(CaloTools v6r13)

gaudi_depends_on_subdirs(Calo/CaloInterfaces
                         Calo/CaloUtils
                         Det/CaloDet
                         Event/LinkerEvent
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiAlg
                         Kernel/LHCbKernel
                         Kernel/LHCbMath
                         Kernel/Relations
                         Tr/TrackKernel
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
find_package(XGBoost REQUIRED)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

if (CMAKE_CXX_COMPILER MATCHES "clang")
  _gaudi_find_standard_lib(libomp.so libomp_libdir)
  if (libomp_libdir)
    message(STATUS "Using libomp.so from ${libomp_libdir}")
    set(libomp_path ${libomp_libdir}/libomp.so)
  else()
    message(FATAL_ERROR "Cannot find libomp.so, needed by XGBoost on clang")
  endif()
endif()

gaudi_add_module(CaloTools
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces Tr/TrackKernel XGBoost
                 LINK_LIBRARIES CaloUtils CaloDetLib LinkerEvent RecEvent TrackEvent GaudiAlgLib TrackKernel LHCbKernel LHCbMathLib RelationsLib XGBoost ${libomp_path})

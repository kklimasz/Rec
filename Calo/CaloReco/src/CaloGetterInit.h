/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOGETTERINIT_H
#define CALOGETTERINIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
// from LHCb
#include "CaloInterfaces/ICaloGetterTool.h"

/** @class CaloGetterInit CaloGetterInit.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-04-17
 */
class CaloGetterInit : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  ICaloGetterTool*             m_getter = nullptr;
  Gaudi::Property<std::string> m_name{this, "ToolName", "CaloGetter"};
};
#endif // CALOGETTERINIT_H

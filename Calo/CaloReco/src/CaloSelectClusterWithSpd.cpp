/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Local
// ============================================================================
#include "CaloSelectClusterWithSpd.h"
// ============================================================================
/** @file
 *  Implementation file for class CaloSelectClusterWithSpd
 *  @date 2009-07-18
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
// ============================================================================
// standard constructor
// ============================================================================
CaloSelectClusterWithSpd::CaloSelectClusterWithSpd( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ICaloClusterSelector>( this );
}
// ============================================================================
// initialization
// ============================================================================
StatusCode CaloSelectClusterWithSpd::initialize() {
  // initialize the base class
  StatusCode sc = GaudiTool::initialize();
  m_toSpd       = tool<ICaloHypo2Calo>( "CaloHypo2Calo", "CaloHypo2Spd", this );
  m_toSpd->setCalos( m_det, "Spd" );
  counterStat = tool<ICounterLevel>( "CounterLevel" );
  return sc;
}
// ============================================================================
// the main method
// ============================================================================
int CaloSelectClusterWithSpd::n_hits( const LHCb::CaloCluster& cluster ) const {
  return m_toSpd->multiplicity( cluster, "Spd" );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Local
// ============================================================================
#include "CaloSelectorAND.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class : CaloSelectorAND
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 27 Apr 2002
 */
// ============================================================================
DECLARE_COMPONENT( CaloSelectorAND )
// ============================================================================
/*  Standard constructor
 *  @see GaudiTool
 *  @see  AlgTool
 *  @see IAlgTool
 *  @param type   tool type (?)
 *  @param name   tool name
 *  @param parent tool parent
 */
// ============================================================================
CaloSelectorAND::CaloSelectorAND( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ICaloClusterSelector>( this );
}
// ============================================================================
/*  standard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode CaloSelectorAND::initialize() {
  // initialize the base class
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Could not initialize the base class GaudiTool", sc ); }
  // locate selectors
  std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(), std::back_inserter( m_selectors ),
                  [&]( const std::string& name ) { return tool<ICaloClusterSelector>( name, this ); } );
  ///
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  standard finalization  of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
// ============================================================================
StatusCode CaloSelectorAND::finalize() {
  // clear containers
  m_selectors.clear();
  m_selectorsTypeNames.clear();
  // finalize the base class
  return GaudiTool::finalize();
}
// ============================================================================

// ============================================================================
/** "select"/"preselect" method
 *  @see ICaloClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloSelectorAND::select( const LHCb::CaloCluster* cluster ) const { return ( *this )( cluster ); }
// ============================================================================

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see ICaloClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloSelectorAND::operator()( const LHCb::CaloCluster* cluster ) const {
  // no selectors!
  if ( m_selectors.empty() ) { return false; } // note: this is not what std::all_of does for an empty range!
  return std::all_of( m_selectors.begin(), m_selectors.end(),
                      [&]( Selectors::const_reference s ) { return ( *s )( cluster ); } );
}
// ============================================================================
// The END
// ============================================================================

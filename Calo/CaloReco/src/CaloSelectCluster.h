/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// ============================================================================
#ifndef CALORECO_CALOSELECTCLUSTER_H
#define CALORECO_CALOSELECTCLUSTER_H 1
// ============================================================================
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"
#include <string>
// ============================================================================

class CaloSelectCluster : public virtual ICaloClusterSelector, public GaudiTool {
public:
  bool         select( const LHCb::CaloCluster* cluster ) const override;
  virtual bool operator()( const LHCb::CaloCluster* cluster ) const override;
  StatusCode   initialize() override;

  CaloSelectCluster( const std::string& type, const std::string& name, const IInterface* parent );

private:
  Gaudi::Property<float>                 m_cut{this, "MinEnergy", 0.};
  Gaudi::Property<float>                 m_etCut{this, "MinEt", 0.};
  Gaudi::Property<int>                   m_mult{this, "MaxDigits", 9999};
  Gaudi::Property<int>                   m_multMin{this, "MinDigits", -9999};
  mutable Gaudi::Accumulators::Counter<> m_counter{this, "selected clusters"};
};
#endif // CALORECO_CALOSELECTCLUSTER_H

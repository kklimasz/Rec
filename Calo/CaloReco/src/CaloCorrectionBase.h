/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOCORRECTIONBASE_H
#define CALOCORRECTIONBASE_H 1

// Include files
#include "CaloDAQ/ICaloDigitFilterTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloRelationsGetter.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloUtils/ClusterFunctors.h"
#include "CaloUtils/ICaloElectron.h"
#include "DetDesc/Condition.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/CaloCellID.h"
#include "Relations/IRelationWeighted2D.h"

// VDT
#include "vdt/vdtMath.h"

inline const InterfaceID IID_CaloCorrectionBase{"CaloCorrectionBase", 1, 0};

/** @class CaloCorrectionBase CaloCorrectionBase.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-05-07
 */

namespace CaloCorrectionUtils {
  class DigitFromCalo {
  public:
    explicit DigitFromCalo( const int calo ) : m_calo( calo ){};
    explicit DigitFromCalo( const std::string& calo ) : DigitFromCalo( CaloCellCode::CaloNumFromName( calo ) ){};
    inline bool operator()( const LHCb::CaloDigit* digit ) const {
      return digit && ( ( (int)digit->cellID().calo() ) == m_calo );
    };
    DigitFromCalo() = delete;

  private:
    int m_calo{0};
  };
} // namespace CaloCorrectionUtils
// DO NOT CHANGE THE FUNCTION ORDER FOR BACKWARD COMPATIBILITY WITH EXISTING CONDITIONS DB
namespace CaloCorrection {
  enum Function {
    InversPolynomial     = 0,
    Polynomial           = 1,
    ExpPolynomial        = 2,
    ReciprocalPolynomial = 3,
    Sigmoid              = 4,
    Sshape               = 5,
    ShowerProfile        = 6,
    SshapeMod            = 7,
    Sinusoidal           = 8,
    ParamList            = 9,  // simple parameter access (by area)
    GlobalParamList      = 10, // simple parameter access (ind. of area)
    Unknown                    // MUST be the last item
  };
  using ParamVector = std::vector<double>;
  using Parameters  = std::pair<CaloCorrection::Function, ParamVector>;
  enum Type {
    // E-Correction parameters
    alphaG,            // global alpha factor
    alphaE,            // alpha(E)
    alphaB,            // alpha(Bary)
    alphaX,            // alpha(Dx)
    alphaY,            // alpha(Dy)
    alphaP,            // alpha(ePrs)
    beta,              // Prs correction (with possible eEcal dependency)
    betaP,             // Prs correction  (with possible ePrs dependency)
    betaPR,            //  Prs correction (with possible ePrs/eEcal dependency)
    betaC,             // Prs correction for converted photons (use beta if not defined)
    betaCP,            // ""
    betaCPR,           // ""
    globalC,           // global factor for converted photons
    globalT,           // global(DeltaTheta) function of incidence angle
    offsetT,           // offset(DeltaTheta) function of incidence angle
    offset,            // offset( sinTheta ) energy (or ET ) offset
    offsetC,           // offset( sinTheta ) energy (or ET ) offset for converted photons
                       //
    ClusterCovariance, // parameters for cluster covariance estimation
                       // L-Correction parameters
    gamma0,
    delta0,
    gammaP,
    deltaP,
    // S-correction parameters
    shapeX,
    shapeY,
    residual,
    residualX,
    residualY,
    asymP,
    asymM,
    angularX,
    angularY,
    // ShowerShape profile
    profile,
    profileC, // for converted photons
              // Cluster masking
    EnergyMask,
    PositionMask,
    lastType // MUST BE THE LAST LINE
  };
  constexpr int            nT           = lastType + 1;
  constexpr int            nF           = Unknown + 1;
  inline const std::string typeName[nT] = {"alphaG",
                                           "alphaE",
                                           "alphaB",
                                           "alphaX",
                                           "alphaY",
                                           "alphaP",
                                           "beta",
                                           "betaP",
                                           "betaPR",
                                           "betaC",
                                           "betaCP",
                                           "betaCPR" // E-corrections
                                           ,
                                           "globalC",
                                           "globalT",
                                           "offsetT",
                                           "offset",
                                           "offsetC",
                                           "ClusterCovariance",
                                           "gamma0",
                                           "delta0",
                                           "gammaP",
                                           "deltaP" // L-Corrections
                                           ,
                                           "shapeX",
                                           "shapeY",
                                           "residual",
                                           "residualX",
                                           "residualY",
                                           "asymP",
                                           "asymM",
                                           "angularX",
                                           "angularY" // S-Corrections
                                           ,
                                           "profile",
                                           "profileC" // Profile shape
                                           ,
                                           "EnergyMask",
                                           "PositionMask",
                                           "Unknown"};

  inline const std::string funcName[nF] = {"InversPolynomial", "Polynomial", "ExpPolynomial",   "ReciprocalPolynomial",
                                           "Sigmoid",          "Sshape",     "ShowerProfile",   "SshapeMod",
                                           "Sinusoidal",       "ParamList",  "GlobalParamList", "Unknown"};
} // namespace CaloCorrection

class CaloCorrectionBase : public GaudiTool, virtual public IIncidentListener {

public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_CaloCorrectionBase; }

  /// Standard constructor
  CaloCorrectionBase( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode finalize() override;   ///< Algorithm finalization

  void       setOrigin( Gaudi::XYZPoint origin ) { m_origin = origin; }
  StatusCode updParams();

  StatusCode setConditionParams( const std::string& cond,
                                 bool               force = false ) { // force = true : forcing access via condDB only
    if ( cond != m_conditionName ) m_conditionName = cond;

    // get parameters from options  :
    if ( !m_useCondDB && !force ) return setOptParams();

    // get from DB if exists :
    if ( !existDet<DataObject>( m_conditionName ) ) {
      if ( force ) {
        if ( m_conditionName != "none" )
          info() << "Condition '" << m_conditionName.value() << "' has not been found " << endmsg;
        return StatusCode::SUCCESS;
      } else {
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " Condition '" << m_conditionName.value() << "' has not found -- try options parameters !"
                  << endmsg;
        return setOptParams();
      }
    }
    return setDBParams();
  }

  // accessors
  CaloCorrection::Parameters         getParams( const CaloCorrection::Type type,
                                                const LHCb::CaloCellID     id = LHCb::CaloCellID() ) const;
  inline CaloCorrection::ParamVector getParamVector( const CaloCorrection::Type type,
                                                     const LHCb::CaloCellID     id = LHCb::CaloCellID() ) const {
    return getParams( type, id ).second;
  }
  double getParameter( CaloCorrection::Type type, unsigned int i, const LHCb::CaloCellID id = LHCb::CaloCellID(),
                       double def = 0. ) const {
    const auto  params = getParams( type, id );
    const auto& data   = params.second;
    return ( i < data.size() ) ? data[i] : def;
  }
  /// return value of analytic derivative for a given function type in cell id at a given point var with default value
  /// def
  double getCorrectionDerivative( const CaloCorrection::Type type, const LHCb::CaloCellID id, double var = 0.,
                                  double def = 0. ) const;
  //// propagate cov.m. cov0 according to Jacobian jac: cov1 = (jac * cov * jac^T), see comments in CaloECorrection.cpp
  /// and CaloSCorrection.cpp
  // void recalculate_cov(const TMatrixD &jac, const TMatrixDSym &cov0, TMatrixDSym &cov1) const;

  double getCorrection( const CaloCorrection::Type type, const LHCb::CaloCellID id, double var = 0.,
                        double def = 1. ) const;
  double incidence( const LHCb::CaloHypo* hypo, bool straight = false ) const;

  void getPrsSpd( const LHCb::CaloHypo* hypo, double& ePrs, double& eSpd ) const {
    typedef const LHCb::CaloHypo::Digits Digits;
    const auto&                          digits = hypo->digits();
    for ( Digits::const_iterator d = digits.begin(); digits.end() != d; ++d ) {
      if ( *d == 0 ) {
        continue;
      } else if ( m_prs( *d ) ) {
        ePrs += ( *d )->e();
      } else if ( m_spd( *d ) ) {
        eSpd += ( *d )->e();
      }
    }
  }

  ICaloDigitFilterTool* pileup() const noexcept { return m_pileup; }

  bool hasConditionChanged() const noexcept { return m_update; }
  void handle( const Incident& ) override { m_update = false; } // reset update flag

protected:
  Gaudi::Property<std::string>              m_conditionName{this, "ConditionName", "none"};
  Gaudi::Property<std::vector<std::string>> m_corrections{this, "Corrections", {"All"}};
  //
  typedef std::vector<LHCb::CaloHypo::Hypothesis> Hypotheses;
  typedef std::vector<int>                        Hypotheses_;
  Hypotheses                                      m_hypos;
  Gaudi::Property<Hypotheses_>                    m_hypos_{this,
                                        "Hypotheses",
                                        {
                                            (int)LHCb::CaloHypo::Hypothesis::Photon,
                                            (int)LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                                            (int)LHCb::CaloHypo::Hypothesis::EmCharged,
                                        },
                                        "acceptable hypotheses"};

  LHCb::ClusterFunctors::ClusterArea     m_area;
  LHCb::ClusterFunctors::ClusterFromCalo m_calo{DeCalorimeterLocation::Ecal};
  CaloCorrectionUtils::DigitFromCalo     m_spd{DeCalorimeterLocation::Spd};
  CaloCorrectionUtils::DigitFromCalo     m_prs{DeCalorimeterLocation::Prs};
  std::string                            m_detData{DeCalorimeterLocation::Ecal};
  const DeCalorimeter*                   m_det = nullptr;
  Gaudi::XYZPoint                        m_origin;
  ICaloDigitFilterTool*                  m_pileup = nullptr;
  Gaudi::Property<bool>                  m_correctCovariance{this, "CorrectCovariance", true};

  template <typename TYPE>
  inline TYPE myexp( const TYPE x ) const {
    // info() << "In myexp " << x << endmsg;
    // return std::exp(x);
    return vdt::fast_exp( x );
  }

  template <typename TYPE>
  inline TYPE mylog( const TYPE x ) const {
    // info() << "In mylog " << x << endmsg;
    // return std::log(x);
    return vdt::fast_log( x );
  }

  template <typename TYPE>
  inline TYPE myatan( const TYPE x ) const {
    // info() << "In myatan " << x << endmsg;
    // return std::atan(x);
    return vdt::fast_atan( x );
  }

  template <typename TYPE>
  inline TYPE myatan2( const TYPE x, const TYPE y ) const {
    // info() << "In myatan2 " << x << " " << y << endmsg;
    // return std::atan2(x,y);
    return vdt::fast_atan2( x, y );
  }

  template <typename TYPE>
  inline TYPE mycos( const TYPE x ) const {
    // info() << "In mycos " << x << endmsg;
    // return std::cos(x);
    return vdt::fast_cos( x );
  }

  template <typename TYPE>
  inline TYPE mysin( const TYPE x ) const {
    // info() << "In mysin " << x << endmsg;
    // return std::sin(x);
    return vdt::fast_sin( x );
  }

  template <typename TYPE>
  inline TYPE mytanh( const TYPE x ) const {
    // info() << "In mytanh " << x << endmsg;
    // return std::tanh(x);
    const auto y = myexp( -2.0 * x );
    return ( 1.0 - y ) / ( 1.0 + y );
  }

  template <typename TYPE>
  inline TYPE mysinh( const TYPE x ) const {
    // info() << "In mysinh " << x << endmsg;
    // return std::sinh(x);
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) - y );
  }

  template <typename TYPE>
  inline TYPE mycosh( const TYPE x ) const {
    // info() << "In mycosh " << x << endmsg;
    // return std::cosh(x);
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) + y );
  }

private:
  inline CaloCorrection::Type stringToCorrectionType( const std::string& type ) {
    for ( int i = 0; i < CaloCorrection::nT; ++i ) {
      if ( CaloCorrection::typeName[i] == type ) return static_cast<CaloCorrection::Type>( i );
    }
    return static_cast<CaloCorrection::Type>( CaloCorrection::lastType );
  }

  ICaloElectron* m_caloElectron = nullptr;

  bool accept( const std::string& name ) {
    for ( auto it = m_corrections.begin(); m_corrections.end() != it; ++it ) {
      if ( name == *it || *it == "All" ) return true;
    }
    return false;
  }

  StatusCode setOptParams();
  StatusCode setDBParams();
  void       checkParams();

  class Params {
  public:
    Params() = default;
    Params( const std::string& t, const CaloCorrection::ParamVector& v ) : active( true ), type( t ), data( v ) {}

  public:
    void clear() {
      active = false;
      data.clear();
    }

  public:
    bool                        active{false};
    std::string                 type;
    CaloCorrection::ParamVector data;

  public:
    typedef std::vector<Params> Vector;
  };

  /// Cache counters, as looking them up as
  ///    counter( CaloCorrection::typeName[ type ] + " correction processing (" + areaName + ")" )
  /// requires the creation of a temporary string every time, which in turn
  /// involves (eventually) a call to 'new' and 'delete'  -- and the above was the source of 10% (!)
  /// of the # of calls to 'new' and 'delete' in the HLT!!!! (FYI: there are, on average, 1K calls
  /// per event to this method!!!)
  ///
  /// On top of that, this also speeds up the actual search for the correct counter,
  /// by making it a two-step process, and the first step is a direct lookup
  /// instead of a 'find'.
  ///
  /// in the end, this change alone speeds up the total HLT by about 1%...
  inline StatEntity& kounter( const CaloCorrection::Type type, const std::string& areaName ) const {
    assert( type < CaloCorrection::lastType + 1 );
    auto a = m_counters[type].find( areaName );
    if ( UNLIKELY( a == std::end( m_counters[type] ) ) ) {
      const auto name = CaloCorrection::typeName[type] + " correction processing (" + areaName + ")";
      auto       r    = m_counters[type].insert( areaName, &counter( name ) );
      assert( r.second );
      a = r.first;
    }
    assert( a->second );
    return *( a->second );
  }

private:
  Params::Vector m_params{CaloCorrection::nT};

  Gaudi::Property<std::map<std::string, std::vector<double>>> m_optParams{this, "Parameters"};

  mutable std::array<GaudiUtils::VectorMap<std::string, StatEntity*>, CaloCorrection::lastType + 1> m_counters;
  Condition*                                                                                        m_cond = nullptr;
  Gaudi::Property<std::string> m_cmLoc{this, "ClusterMatchLocation"};
  ICaloRelationsGetter*        m_tables = nullptr;
  Gaudi::Property<bool>        m_useCondDB{this, "UseCondDB", true};
  bool                         m_update{false};

protected:
  ICounterLevel* counterStat = nullptr;
};
#endif // CALOCORRECTIONBASE_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOSHAREDCELLALG_H
#define CALORECO_CALOSHAREDCELLALG_H 1
// ============================================================================
// Include files
// CaloDet
#include "CaloDet/DeCalorimeter.h"
// CaloEvent/Event
#include "Event/CaloCluster.h"
// from GaudiAlg
#include "CaloInterfaces/ICounterLevel.h"
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class  CaloSharedCellAlg CaloSharedCellAlg.h
 *
 *  A very simple algorithm, which performs the
 *  energy redistribution
 *  between shared cells
 *
 *  @author Ivan Belyaev
 *  @date   30/06/2001
 */

class CaloSharedCellAlg : public GaudiAlgorithm {
public:
  /** standard initialization method
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;
  StatusCode finalize() override;

  /** standard execution method
   *  @see CaloAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /**  Standard constructor
   *   @param name           name of the algorithm
   *   @param pSvcLocator    poinetr to Service Locator
   */
  CaloSharedCellAlg( const std::string& name, ISvcLocator* pSvcLocator );

private:
  bool m_copy = true; ///< copy flag

  Gaudi::Property<bool> m_useSumEnergy{this, "ShareSumEnergy", true,
                                       "should one use the summed cluster energy or central cell energy?"};

  Gaudi::Property<int> m_numIterations{this, "Iterations", 5, "number of iterations iif one use summed cluster energy"};

  Gaudi::Property<bool> m_useDistance{this, "ShareDistance", false, "should one take into account the the distance?"};

  Gaudi::Property<std::vector<double>> m_showerSizes{this,
                                                     "ShowerSizes",
                                                     {
                                                         0.1090 * 121.50 * Gaudi::Units::mm,
                                                         0.1326 * 60.75 * Gaudi::Units::mm,
                                                         0.1462 * 40.50 * Gaudi::Units::mm,
                                                     },
                                                     "shower size parameters (for different areas)"};

  Gaudi::Property<std::string> m_inputData{this, "InputData", LHCb::CaloClusterLocation::Ecal};

  Gaudi::Property<std::string> m_outputData{this, "OutputData"};

  Gaudi::Property<std::string> m_detData{this, "Detector", DeCalorimeterLocation::Ecal};

  ICounterLevel* counterStat = nullptr;
};
// ============================================================================
#endif // CALOSHAREDCELLALG_H

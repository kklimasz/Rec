/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALORECO_CALOECORRECTION_H
#define CALORECO_CALOECORRECTION_H 1
// Include files
#include "CaloCorrectionBase.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "GaudiKernel/Counters.h"
#include <map>
#include <string>

/** @namespace CaloECorrection_Local
 */

/** @class CaloECorrection CaloECorrection.h
 *
 *
 *  @author Deschamps Olivier

 *  @date   2003-03-10
 */
class CaloECorrection : public virtual ICaloHypoTool, public CaloCorrectionBase {

public:
  StatusCode process( LHCb::CaloHypo* hypo ) const override;
  StatusCode operator()( LHCb::CaloHypo* hypo ) const override;

public:
  StatusCode initialize() override;
  StatusCode finalize() override;

  CaloECorrection( const std::string& type, const std::string& name, const IInterface* parent );

private:
  Gaudi::Property<int> m_pFilt{this, "PrsFilter", 0x3, "1 : noPrs ; 2 : Prs ; 3: both"};
  Gaudi::Property<int> m_sFilt{this, "SpdFilter", 0x3, "1 : noSpd ; 2 : Spd ; 3: both"};

  /// input variables calculated once in process() and passed to all calcECorrection() calls
  struct ECorrInputParams {
    LHCb::CaloCellID cellID;
    Gaudi::XYZPoint  seedPos;
    double           eSpd;
    double           dtheta;
    unsigned int     area;
  };

  /// Jacobian elements and intermediate variables sometimes returned from calcECorrection() to process()
  class ECorrOutputParams {
  public:
    ECorrOutputParams()
        : dEcor_dXcl( 0 )
        , dEcor_dYcl( 0 )
        , dEcor_dEcl( 0 )
        , alpha( 0 )
        , beta( 0 )
        , Asx( 0 )
        , Asy( 0 )
        , aG( 0 )
        , aE( 0 )
        , aB( 0 )
        , aX( 0 )
        , aY( 0 )
        , gC( 0 )
        , gT( 0 )
        , betaC_flag( false ) {}

    // output Jacobian elements returned from calcECorrection() to process()
    double dEcor_dXcl;
    double dEcor_dYcl;
    double dEcor_dEcl;

    // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
    double alpha;
    double beta;
    double Asx;
    double Asy;
    double aG;
    double aE;
    double aB;
    double aX;
    double aY;
    double gC;
    double gT;

    bool betaC_flag;
  };

  /// calculate corrected CaloHypo energy depending on CaloCluster position, energy, and Prs energy
  double calcECorrection( double xBar, double yBar, double eEcal, double ePrs,
                          const struct CaloECorrection::ECorrInputParams& _params,
                          CaloECorrection::ECorrOutputParams*             _results ) const;

  using IncCounter = Gaudi::Accumulators::Counter<>;
  using SCounter   = Gaudi::Accumulators::StatCounter<float>;

  mutable IncCounter m_counterSkippedNegativeEnergyCorrection{this, "Skip negative energy correction"};

  mutable SCounter m_counterPileupOffset{this, "Pileup offset"};
  mutable SCounter m_counterPileupSubstractedRatio{this, "Pileup subtracted ratio"};
  mutable SCounter m_counterPileupScale{this, "Pileup scale"};

  mutable IncCounter m_counterUnphysical{this, "Unphysical d(Ehypo)/d(Ecluster)"};

  mutable SCounter m_counterCorrectedEnergy{this, "Corrected energy"};
  mutable SCounter m_counterDeltaEnergy{this, "Delta(E)"};

  mutable IncCounter m_counterUnphysicalVariance{this, "Unphysical variance(Ehypo)"};

  static constexpr int          k_numOfCaloAreas{4};
  mutable std::vector<SCounter> m_countersAlpha;
  mutable std::vector<SCounter> m_countersBetaTimesEprs;
};
#endif // CALORECO_CALOECORRECTION_H

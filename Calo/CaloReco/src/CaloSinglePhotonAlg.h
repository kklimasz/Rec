/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOALGS_CALOSINGLEPHOTONALG_H
#define CALOALGS_CALOSINGLEPHOTONALG_H 1

#include "CaloDet/DeCalorimeter.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloDataFunctor.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"

#include "GaudiAlg/ScalarTransformer.h"
#include "GaudiKernel/Counters.h"

#include <optional>
#include <string>

struct ICaloClusterSelector;
struct ICaloHypoTool;

/** @class CaloSinglePhotonAlg CaloSinglePhotonAlg.h
 *
 *  The simplest algorithm of reconstruction of
 *  single photon in electromagnetic calorimeter.
 *  The implementation is based on F.Machefert's codes.
 *
 *  @author Frederic Machefert machefer@in2p3.fr
 *  @author Vanya Belyaev      Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */

class CaloSinglePhotonAlg
    : public Gaudi::Functional::ScalarTransformer<CaloSinglePhotonAlg, LHCb::CaloHypos( const LHCb::CaloClusters& )> {
public:
  CaloSinglePhotonAlg( const std::string& name, ISvcLocator* pSvc );

  StatusCode initialize() override;
  StatusCode finalize() override;

  using ScalarTransformer::     operator();
  std::optional<LHCb::CaloHypo> operator()( const LHCb::CaloCluster& ) const;
  void                          postprocess( const LHCb::CaloHypos& ) const;

private:
  /// container of names
  typedef std::vector<std::string> Names;
  /// container of selector tools
  typedef std::vector<ICaloClusterSelector*> Selectors;
  /// containers of hypo tools
  typedef std::vector<ICaloHypoTool*> HypoTools;
  /// container of correction tools (S-,L-,...)
  typedef HypoTools Corrections;

  Selectors              m_selectors;
  Gaudi::Property<Names> m_selectorsTypeNames{
      this,
      "SelectionTools",
      {"CaloSelectCluster/PhotonCluster", "CaloSelectNeutralClusterWithTracks/NeutralCluster"},
      "List of tools for selection of clusters"};

  HypoTools              m_hypotools;
  Gaudi::Property<Names> m_hypotoolsTypeNames{this,
                                              "HypoTools",
                                              {"CaloExtraDigits/SpdPrsExtraG"},
                                              "List of generic Hypo-tools to apply to newly created hypos"};

  Corrections            m_corrections;
  Gaudi::Property<Names> m_correctionsTypeNames{this,
                                                "CorrectionTools2",
                                                {
                                                    "CaloECorrection/ECorrection",
                                                    "CaloSCorrection/SCorrection",
                                                    "CaloLCorrection/LCorrection",
                                                },
                                                "List of tools for 'fine-corrections' "};

  Gaudi::Property<std::string> m_detData{this, "Detector", LHCb::CaloAlgUtils::DeCaloLocation( "Ecal" )};

  Gaudi::Property<double> m_eTcut{this, "EtCut", 0., "Threshold on cluster & hypo ET"};

  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> m_eT{
      nullptr}; // TODO: can we combine this into Over_Et_Threshold and make it a property??

  const DeCalorimeter*                       m_det = nullptr;
  mutable Gaudi::Accumulators::StatCounter<> m_counterHypos{this, "hyposNumber"};
};
// ============================================================================
#endif // CALOSINGLEPHOTONALG_H

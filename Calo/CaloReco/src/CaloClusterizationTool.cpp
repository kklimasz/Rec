/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
// ============================================================================
#include "DetDesc/IGeometryInfo.h"
// ============================================================================
#include "CaloUtils/CaloDataFunctor.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CellID.h"
// ============================================================================
#include "CaloUtils/CaloNeighbours.h"
#include "CaloUtils/ClusterFunctors.h"
// local
#include "CaloClusterizationTool.h"
#include "TaggedCellFunctor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloClusterizationTool
//
// 2008-04-04 : Victor Egorychev
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloClusterizationTool )

// ============================================================================
inline bool CaloClusterizationTool::isLocMax( const LHCb::CaloDigit*                   digit,
                                              const CaloClusterizationTool::DirVector& hits,
                                              const DeCalorimeter*                     det ) const {

  const CaloNeighbors& ns = det->neighborCells( digit->cellID() );
  double               e  = digit->e();

  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> et( det );

  double eT = ( m_withET ? et( digit ) : 0. );

  for ( const auto& iN : ns ) {
    const CelAutoTaggedCell* cell = hits[iN];
    if ( !cell ) { continue; }
    const LHCb::CaloDigit* nd = cell->digit();
    if ( !nd ) { continue; }

    if ( nd->e() > e ) { return false; }

    if ( m_withET ) { eT += et( nd ); }
  }

  if ( m_withET && eT < m_ETcut ) { return false; }

  return true;
}
// ============================================================================
/* Application of rules of tagging on one cell
 *   - No action if no clustered neighbor
 *   - Clustered if only one clustered neighbor
 *   - Edge if more then one clustered neighbor
 */
// ============================================================================
inline void CaloClusterizationTool::appliRulesTagger( CelAutoTaggedCell* cell, CaloClusterizationTool::DirVector& hits,
                                                      const DeCalorimeter* det, const bool releaseBool ) const {

  // Find in the neighbors cells tagged before, the clustered neighbors cells
  const LHCb::CaloCellID& cellID               = cell->cellID();
  const CaloNeighbors&    ns                   = det->neighborCells( cellID );
  bool                    hasEdgeNeighbor      = false;
  bool                    hasClusteredNeighbor = false;
  for ( CaloNeighbors::const_iterator iN = ns.begin(); ns.end() != iN; ++iN ) {
    const CelAutoTaggedCell* nei = hits[*iN];
    if ( 0 == nei ) { continue; }
    //
    if ( nei->isEdge() && releaseBool ) {
      hasEdgeNeighbor = true;
      for ( std::vector<LHCb::CaloCellID>::const_iterator id = nei->seeds().begin(); id != nei->seeds().end(); id++ ) {
        if ( !cell->isWithSeed( *id ) ) cell->addSeed( *id );
      }
    }
    //
    if ( !nei->isClustered() ) { continue; }
    hasClusteredNeighbor         = true;
    const LHCb::CaloCellID& seed = nei->seedForClustered();
    if ( cell->isWithSeed( seed ) ) { continue; }
    cell->addSeed( seed );
  }

  // Tag or or not the cell

  switch ( cell->numberSeeds() ) {
  case 0:
    if ( !releaseBool ) break;
    if ( hasEdgeNeighbor && !hasClusteredNeighbor ) cell->setEdge(); //
    break;
  case 1:
    cell->setClustered();
    break;
  default:
    cell->setEdge();
    break;
  }
}

// ============================================================================
inline StatusCode CaloClusterizationTool::setEXYCluster( LHCb::CaloCluster&   cluster,
                                                         const DeCalorimeter* detector ) const {
  ///
  double E, X, Y;
  ///
  StatusCode sc =
      LHCb::ClusterFunctors::calculateEXY( cluster.entries().begin(), cluster.entries().end(), detector, E, X, Y );
  ///
  m_clusterEnergy += E;
  if ( sc.isSuccess() ) {
    cluster.position().parameters()( LHCb::CaloPosition::Index::E ) = E;
    cluster.position().parameters()( LHCb::CaloPosition::Index::X ) = X;
    cluster.position().parameters()( LHCb::CaloPosition::Index::Y ) = Y;
  } else {
    Error( " E,X and Y of cluster could not be evaluated!", sc ).ignore();
  }
  ///
  return sc;
}

unsigned int CaloClusterizationTool::clusterize( std::vector<LHCb::CaloCluster*>& clusters,
                                                 const LHCb::CaloDigits& hits, const DeCalorimeter* m_detector,
                                                 const std::vector<LHCb::CaloCellID>& _cell_list,
                                                 const unsigned int                   m_neig_level ) const {

  //

  // cmb: this needs to be done every time in order
  // to allow different detectors in the same algorithm
  // --> to be revisited in a future round
  CellSelector cellSelectorE;
  CellSelector cellSelectorP;
  cellSelectorE.setSelector( m_usedE );
  cellSelectorP.setSelector( m_usedP );
  cellSelectorE.setDet( m_detector );
  cellSelectorP.setDet( m_detector );
  bool releaseBool = false;
  bool useData     = false;
  //
  LHCb::CaloCellID::Set out_cells;

  /*

  _cell_list.empty()  && level > 0   ->  All local maxima + neighborhood(level)
  _cell_list.empty()  && level = 0   ->  All data (default)
  !_cell_list.empty() && level > 0   ->  seed lis + neighborhood(level)
  !_cell_list.empty() && level = 0   ->  seed list
  */

  // fill with data if level >0 and no predefined seed list
  std::vector<LHCb::CaloCellID> cell_list;
  if ( _cell_list.empty() && m_neig_level > 0 ) {
    useData = true;

    cell_list.reserve( hits.size() );
    for ( const auto& i : hits ) {
      const CaloNeighbors& neighbors = m_detector->neighborCells( i->cellID() );
      if ( std::none_of( neighbors.begin(), neighbors.end(), [&, e = i->e()]( const auto& n ) {
             if ( !m_detector->valid( n ) ) return false;
             const LHCb::CaloDigit* dig = hits( n );
             return dig && dig->e() > e;
           } ) ) {
        cell_list.push_back( i->cellID() );
      }
    }
  } else {
    cell_list.assign( _cell_list.begin(), _cell_list.end() );
  }

  //  info() << "Cell list " << _cell_list << " level = " << m_neig_level  << endmsg;

  // if list of "seed" is not empty
  if ( !cell_list.empty() ) {
    out_cells.insert( cell_list.begin(), cell_list.end() );

    /** find all neighbours for the given set of cells for the givel level
     *  @param cells    (UPDATE) list of cells
     *  @param level    (INPUT)  level
     *  @param detector (INPUT) the detector
     *  @return true if neighbours are added
     */
    LHCb::CaloFunctors::neighbours( out_cells, m_neig_level, m_detector );
  }

  // z-position of cluster
  LHCb::ClusterFunctors::ZPosition zPosition( m_detector );

  // Create access direct and sequential on the tagged cells
  DirVector taggedCellsDirect( (CelAutoTaggedCell*)0 );
  /// container to tagged  cells with sequential access
  std::vector<CelAutoTaggedCell*> taggedCellsSeq;

  size_t                         local_size = cell_list.empty() ? hits.size() : out_cells.size();
  const CelAutoTaggedCell        cell0_     = CelAutoTaggedCell();
  std::vector<CelAutoTaggedCell> local_cells( local_size, cell0_ );

  if ( cell_list.empty() ) {
    taggedCellsDirect.reserve( hits.size() );
    taggedCellsDirect.setSize( 14000 );
    taggedCellsSeq.reserve( hits.size() );
  } else {
    taggedCellsDirect.reserve( out_cells.size() );
    taggedCellsDirect.setSize( 14000 );
    taggedCellsSeq.reserve( out_cells.size() );
  }

  if ( cell_list.empty() ) {
    // fill with the data
    size_t index = 0;

    for ( auto ihit = hits.begin(); hits.end() != ihit; ++ihit, ++index ) {
      const LHCb::CaloDigit* digit = *ihit;
      if ( !digit ) { continue; } // CONTINUE !!!
      CelAutoTaggedCell& taggedCell = *( local_cells.begin() + index );
      taggedCell                    = digit;

      taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
      taggedCellsSeq.push_back( &taggedCell );
    }
  } else { // fill for HLT
    size_t index = 0;

    for ( std::set<LHCb::CaloCellID>::const_iterator icell = out_cells.begin(); out_cells.end() != icell;
          ++icell, ++index ) {

      const LHCb::CaloDigit* digit = hits( *icell );
      if ( 0 == digit ) { continue; } // CONTINUE !!!

      CelAutoTaggedCell& taggedCell = *( local_cells.begin() + index );
      taggedCell                    = digit;

      taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
      taggedCellsSeq.push_back( &taggedCell );
    }
  }

  // Find and mark the seeds (local maxima)
  if ( useData ) {
    for ( std::vector<LHCb::CaloCellID>::iterator seed = cell_list.begin(); seed != cell_list.end(); seed++ ) {
      taggedCellsDirect[*seed]->setIsSeed();
    }
  } else {
    for ( auto itTag = taggedCellsSeq.begin(); taggedCellsSeq.end() != itTag; ++itTag ) {
      if ( isLocMax( ( *itTag )->digit(), taggedCellsDirect, m_detector ) ) { ( *itTag )->setIsSeed(); }
    }
  }

  /// Tag the cells which are not seeds
  auto itTagLastSeed =
      std::stable_partition( taggedCellsSeq.begin(), taggedCellsSeq.end(), TaggedCellFunctor::isClustered() );

  auto         itTagLastClustered = itTagLastSeed;
  auto         itTagFirst         = itTagLastClustered;
  unsigned int nPass              = 0;
  while ( itTagLastClustered != taggedCellsSeq.end() ) {

    // Apply rules tagger for all not tagged cells
    for ( auto itTag = itTagLastClustered; taggedCellsSeq.end() != itTag; ++itTag ) {
      appliRulesTagger( ( *itTag ), taggedCellsDirect, m_detector, releaseBool );
    }

    // Valid result
    std::for_each( itTagFirst, taggedCellsSeq.end(), TaggedCellFunctor::setStatus() );

    itTagLastClustered =
        std::stable_partition( itTagFirst, taggedCellsSeq.end(), TaggedCellFunctor::isClusteredOrEdge() );

    // Test if cells are tagged in this pass
    if ( itTagLastClustered == itTagFirst && releaseBool ) {
      const long number = taggedCellsSeq.end() - itTagLastClustered;
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << " TAGGING NOT FULL - Remain " << std::to_string( number ) << " not clustered cells" << endmsg;
      itTagLastClustered = taggedCellsSeq.end();
    }
    if ( itTagLastClustered == itTagFirst )
      releaseBool = true; // try additional passes releasing appliRulesTagger criteria
    nPass++;
    itTagFirst = itTagLastClustered;
    if ( m_passMax > 0 && nPass >= m_passMax ) break;
  }

  LHCb::CaloDigitStatus::Status usedForE =
      LHCb::CaloDigitStatus::UseForEnergy | LHCb::CaloDigitStatus::UseForCovariance;
  LHCb::CaloDigitStatus::Status usedForP =
      LHCb::CaloDigitStatus::UseForPosition | LHCb::CaloDigitStatus::UseForCovariance;
  LHCb::CaloDigitStatus::Status seed =
      LHCb::CaloDigitStatus::SeedCell | LHCb::CaloDigitStatus::LocalMaximum | usedForP | usedForE;

  itTagLastClustered   = std::stable_partition( itTagLastSeed, taggedCellsSeq.end(), TaggedCellFunctor::isClustered() );
  auto itTagSeed       = taggedCellsSeq.begin();
  auto itTagClustered1 = itTagLastSeed;
  while ( itTagSeed != itTagLastSeed ) {
    const LHCb::CaloDigit* digit = ( *itTagSeed )->digit();

    if ( digit->e() <= 0 ) {
      itTagSeed++;
      m_negativeSeed += digit->e();
      continue; // does not keep cluster with seed.energy <= 0
    }

    LHCb::CaloCellID seedID = ( *itTagSeed )->cellID();
    // Do partitions first
    auto itTagClustered2 =
        std::stable_partition( itTagClustered1, itTagLastClustered, TaggedCellFunctor::isWithSeed( seedID ) );
    auto itTagFirstEdge = itTagLastClustered;
    auto itTagLastEdge =
        std::stable_partition( itTagLastClustered, taggedCellsSeq.end(), TaggedCellFunctor::isWithSeed( seedID ) );

    // Create cluster, reserve, fill
    auto cluster = std::make_unique<LHCb::CaloCluster>();
    cluster->entries().reserve( 1 + ( itTagClustered2 - itTagClustered1 ) + ( itTagLastEdge - itTagFirstEdge ) );

    // set seed
    cluster->entries().emplace_back( digit, seed );
    cluster->setSeed( digit->cellID() );

    // Owned cells
    for ( ; itTagClustered1 != itTagClustered2; ++itTagClustered1 ) {
      LHCb::CaloCellID              cellID = ( *itTagClustered1 )->cellID();
      const LHCb::CaloDigit*        digit  = ( *itTagClustered1 )->digit();
      LHCb::CaloDigitStatus::Status owned  = LHCb::CaloDigitStatus::OwnedCell;
      if ( cellSelectorE( seedID, cellID ) > 0. ) owned |= usedForE;
      if ( cellSelectorP( seedID, cellID ) > 0. ) owned |= usedForP;
      cluster->entries().emplace_back( digit, owned );
    }
    // Shared cells
    for ( ; itTagFirstEdge != itTagLastEdge; ++itTagFirstEdge ) {
      const LHCb::CaloDigit*        digit  = ( *itTagFirstEdge )->digit();
      LHCb::CaloCellID              cellID = ( *itTagFirstEdge )->cellID();
      LHCb::CaloDigitStatus::Status shared = LHCb::CaloDigitStatus::SharedCell;
      if ( cellSelectorE( seedID, cellID ) > 0. ) shared |= usedForE;
      if ( cellSelectorP( seedID, cellID ) > 0. ) shared |= usedForP;
      cluster->entries().emplace_back( digit, shared );
    };

    if ( setEXYCluster( *cluster, m_detector ).isSuccess() ) {
      cluster->setType( LHCb::CaloCluster::Type::CellularAutomaton );
      cluster->position().setZ( zPosition( cluster.get() ) );
      //  put cluster to the output
      clusters.push_back( cluster.release() );
      m_clusterEnergy += clusters.back()->entries().size();
    }
    itTagClustered1 = itTagClustered2;
    itTagSeed++;
  }

  // clear local storages
  taggedCellsSeq.clear();
  taggedCellsDirect.clear();
  local_cells.clear();

  return nPass;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef CALORECO_SUBCLUSTERSELECTORBASE_H
#define CALORECO_SUBCLUSTERSELECTORBASE_H 1

#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloSubClusterTag.h"
#include "Event/CaloDigitStatus.h"
#include "GaudiAlg/GaudiTool.h"

class CaloCluster;

class SubClusterSelectorBase : public virtual ICaloSubClusterTag, public GaudiTool {

public:
  static const LHCb::CaloDigitStatus::Status defaultStatus = LHCb::CaloDigitStatus::UseForEnergy |
                                                             LHCb::CaloDigitStatus::UseForPosition |
                                                             LHCb::CaloDigitStatus::UseForCovariance;

  StatusCode initialize() override;
  StatusCode finalize() override;
  StatusCode process( LHCb::CaloCluster* cluster ) const override;
  StatusCode operator()( LHCb::CaloCluster* cluster ) const override;
  void       setMask( const LHCb::CaloDigitStatus::Status mask ) const override {
    m_mask = mask;
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "The default status tag is changed to " << m_mask
              << " -> use for Energy   : " << ( ( mask & LHCb::CaloDigitStatus::UseForEnergy ) != 0 )
              << " | for Position : " << ( ( mask & LHCb::CaloDigitStatus::UseForPosition ) != 0 )
              << " | for Covariance : " << ( ( mask & LHCb::CaloDigitStatus::UseForCovariance ) != 0 ) << endmsg;
  }
  unsigned int mask() const override { return m_mask; };

protected:
  /**  return  flag to modify the fractions
   *   @return flag to modify the fractions
   */
  inline bool modify() const { return m_modify; }

  /** set new value for "modify" parameter
   *  @param value new value of modify parameter
   */
  inline void setModify( const bool value ) const { m_modify = value; }

  inline const DeCalorimeter* det() { return m_det; }

protected:
  /** Standard Tool Constructor
   *  @param type type of the tool (useless ? )
   *  @param name name of the tool
   *  @param parent the tool parent
   */
  SubClusterSelectorBase( const std::string& type, const std::string& name, const IInterface* parent );

private:
  /// default constructor is private
  SubClusterSelectorBase();
  /// copy    constructor is private
  SubClusterSelectorBase( const SubClusterSelectorBase& );
  /// assignement operator is private
  SubClusterSelectorBase& operator=( const SubClusterSelectorBase& );

private:
  mutable LHCb::CaloDigitStatus::Status m_mask;
  mutable Gaudi::Property<bool>         m_modify{this, "ModifyFractions", false};
  Gaudi::Property<std::string>          m_detData{this, "Detector", DeCalorimeterLocation::Ecal};
  const DeCalorimeter*                  m_det = nullptr;
};
// ============================================================================
#endif // SUBCLUSTERSELECTORBASE_H

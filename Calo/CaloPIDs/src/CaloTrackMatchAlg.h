/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOTRACKMATCHALG_H
#define CALOTRACKMATCHALG_H 1

// Include files
#include "CaloInterfaces/ICaloTrackMatch.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloTrackMatchAlg CaloTrackMatchAlg.h
 *
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-16
 */

using Filter = LHCb::Relation1D<LHCb::Track, bool>;

template <typename TABLE, typename CALOTYPES>
class CaloTrackMatchAlg
    : public Gaudi::Functional::Transformer<TABLE( const LHCb::Tracks&, const CALOTYPES&, const Filter& ),
                                            Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg>> {
  static_assert( std::is_base_of<LHCb::Calo2Track::ITrAccTable, Filter>::value,
                 "Filter must inherit from ITrAccTable" );

public:
  using CaloTrackAlg::_setProperty;
  using CaloTrackAlg::context;
  using CaloTrackAlg::debug;
  using CaloTrackAlg::getProperty;
  using CaloTrackAlg::msgLevel;
  using CaloTrackAlg::use;
  using base_type = Gaudi::Functional::Transformer<TABLE( const LHCb::Tracks&, const CALOTYPES&, const Filter& ),
                                                   Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg>>;
  using KeyValue  = typename base_type::KeyValue;

  // standard constructor
  CaloTrackMatchAlg( const std::string& name, ISvcLocator* pSvc );

  // standard execution
  TABLE operator()( const LHCb::Tracks& tracks, const CALOTYPES& calos, const Filter& filter ) const override;

protected:
  Gaudi::Property<float> m_threshold{this, "Threshold", 10000., "threshold"};
  Gaudi::Property<int>   m_tablesize{this, "TableSize", 100, "table size"};

  // the tool for matching
  // **TODO**: update tool CaloTrackMatch
  mutable ToolHandle<ICaloTrackMatch> m_tool{this, "Tool", "<NOT DEFINED>"};

  // a bit of statistics
  mutable Gaudi::Accumulators::StatCounter<>      m_nMatchFailure{this, "#match failure"};
  mutable Gaudi::Accumulators::StatCounter<>      m_nLinks{this, "#links in table"};
  mutable Gaudi::Accumulators::StatCounter<>      m_nTracks{this, "#good tracks"};
  mutable Gaudi::Accumulators::StatCounter<>      m_nCalos{this, "#total calos"};
  mutable Gaudi::Accumulators::StatCounter<>      m_nOverflow{this, "#above threshold"};
  mutable Gaudi::Accumulators::StatCounter<float> m_chi2{this, "#chi2"};

private:
  const LHCb::CaloPosition* position( const LHCb::CaloCluster* c ) const { return &c->position(); }
  const LHCb::CaloPosition* position( const LHCb::CaloHypo* c ) const { return c->position(); }
};

// ============================================================================
#endif // CALOTRACKMATCHALG_H

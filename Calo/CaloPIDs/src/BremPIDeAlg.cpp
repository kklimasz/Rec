/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloID2DLL.h"

// ============================================================================
/** @class BremPIDeAlg  BremPIDeAlg.cpp
 *  The preconfigured instance of class CaloID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class BremPIDeAlg final : public CaloID2DLL {
public:
  /// Standard protected constructor
  BremPIDeAlg( const std::string& name, ISvcLocator* pSvc ) : CaloID2DLL( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    updateHandleLocation( *this, "Input", CaloIdLocation( "BremChi2", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "BremPIDe", context() ) );

    _setProperty( "nVlong", Gaudi::Utils::toString( 200 ) );
    _setProperty( "nVvelo", Gaudi::Utils::toString( 200 ) );
    _setProperty( "nVupstr", Gaudi::Utils::toString( 200 ) );
    _setProperty( "nMlong", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) );
    _setProperty( "nMvelo", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) );
    _setProperty( "nMupstr", Gaudi::Utils::toString( 50 * Gaudi::Units::GeV ) );

    _setProperty( "HistogramU", "DLL_Long" );
    _setProperty( "HistogramL", "DLL_Long" );
    _setProperty( "HistogramV", "DLL_Long" );
    _setProperty( "ConditionName", "Conditions/ParticleID/Calo/BremPIDe" );

    _setProperty( "HistogramU_THS", "CaloPIDs/CALO/BREMPIDE/h3" );
    _setProperty( "HistogramL_THS", "CaloPIDs/CALO/BREMPIDE/h3" );
    _setProperty( "HistogramV_THS", "CaloPIDs/CALO/BREMPIDE/h3" );

    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                                               LHCb::Track::Types::Upstream ) );
  };
};

// ============================================================================

DECLARE_COMPONENT( BremPIDeAlg )

// ============================================================================

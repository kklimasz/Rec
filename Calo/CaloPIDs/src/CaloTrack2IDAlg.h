/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOPIDS_CALOTRACK2IDALG_H
#define CALOPIDS_CALOTRACK2IDALG_H 1

// Include files
#include "CaloInterfaces/ICaloTrackIdEval.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloTrack2IDAlg CaloTrack2IDAlg.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */

using Table  = LHCb::Relation1D<LHCb::Track, float>;
using Filter = LHCb::Relation1D<LHCb::Track, bool>;

class CaloTrack2IDAlg : public Gaudi::Functional::Transformer<Table( const LHCb::Tracks&, const Filter& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg>> {
  static_assert( std::is_base_of<LHCb::Calo2Track::ITrAccTable, Filter>::value,
                 "Filter must inherit from ITrAccTable" );

public:
  CaloTrack2IDAlg( const std::string& name, ISvcLocator* pSvc );
  Table operator()( const LHCb::Tracks&, const Filter& ) const override;

private:
  // tool to be used for evaluation
  ToolHandle<ICaloTrackIdEval> m_tool{this, "Tool", "<NOT DEFINED>"};

  mutable Gaudi::Accumulators::StatCounter<>       m_nTracks{this, "#total tracks"};
  mutable Gaudi::Accumulators::StatCounter<>       m_nLinks{this, "#links in table"};
  mutable Gaudi::Accumulators::StatCounter<double> m_nEnergy{this, "#total energy"};
};

// ============================================================================
#endif // CALOTRACK2IDALG_H

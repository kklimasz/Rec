/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloAcceptance.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "Linear.h"

// ============================================================================
/** @class InBremAcceptance
 *  The preconfigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InBremAcceptance final : InCaloAcceptance {
  /// standard constructor
  InBremAcceptance( const std::string& type, const std::string& name, const IInterface* parent )
      : InCaloAcceptance( type, name, parent ) {
    _setProperty( "Calorimeter", DeCalorimeterLocation::Ecal );
  }

  /// C++11 non-copyable idiom
  InBremAcceptance()                          = delete;
  InBremAcceptance( const InBremAcceptance& ) = delete;
  InBremAcceptance& operator=( const InBremAcceptance& ) = delete;

  // ==========================================================================
  /** check the track is in acceptance of given calorimeter
   *  @see IInAcceptance
   *  @param  track track to be checked
   *  @return true if the track is in acceptance
   */
  bool inAcceptance( const LHCb::Track* track ) const override;
};

// ============================================================================

DECLARE_COMPONENT( InBremAcceptance )

// ============================================================================
// check the expected bremstrahlung photon is in acceptance of Ecal
// ============================================================================

bool InBremAcceptance::inAcceptance( const LHCb::Track* track ) const {
  // check the goodness of the tarck
  if ( !use( track ) ) { return false; } // RETURN
  //
  // find the appropriate state
  const LHCb::State* state = nullptr;
  for ( const auto& loc : {LHCb::State::Location::AtTT, LHCb::State::Location::EndRich1,
                           LHCb::State::Location::BegRich1, LHCb::State::Location::EndVelo} ) {
    state = CaloTrackTool::state( *track, loc );
    if ( state ) break;
  }
  if ( !state ) {
    // get the closest state to some artificial value
    state = &( track->closestState( 2.0 * Gaudi::Units::meter ) );
    // allowed z ?
    if ( state->z() > 4.0 * Gaudi::Units::meter ) {
      Error( "No appropriate states are found, see 'debug'" ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) print( debug(), track );
      return false;
    }
  }
  // get the line form the state
  const Line l = line( *state );
  // get the point of intersection of the line with the plane
  Gaudi::XYZPoint point;
  double          mu = 0;
  Gaudi::Math::intersection( l, plane(), point, mu );
  //
  return ok( point );
}

// ============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloTrack2IDAlg.h"

// ============================================================================
/** @class Track2HcalEAlg Track2HcalEAlg.cpp
 *  preconfigured instance of class  CaloTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct Track2HcalEAlg final : public CaloTrack2IDAlg {
  Track2HcalEAlg( const std::string& name, ISvcLocator* pSvc ) : CaloTrack2IDAlg( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty( "Output", CaloIdLocation( "HcalE", context() ) );
    _setProperty( "Filter", CaloIdLocation( "InHcal", context() ) );
    _setProperty( "Tool", "HcalEnergyForTrack/HcalE" );
  }
};

// ============================================================================

DECLARE_COMPONENT( Track2HcalEAlg )

// ============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloID2DLL.h"

// ============================================================================
/** @class EcalPIDeAlg  EcalPIDeAlg.cpp
 *  The preconfigured instance of class CaloID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class EcalPIDeAlg final : public CaloID2DLL {
public:
  EcalPIDeAlg( const std::string& name, ISvcLocator* pSvc ) : CaloID2DLL( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    updateHandleLocation( *this, "Input", CaloIdLocation( "EcalChi2", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "EcalPIDe", context() ) );

    _setProperty( "nVlong", Gaudi::Utils::toString( 2500 ) );
    _setProperty( "nVdown", Gaudi::Utils::toString( 2500 ) );
    _setProperty( "nVTtrack", Gaudi::Utils::toString( 2500 ) );
    _setProperty( "nMlong", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );
    _setProperty( "nMdown", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );
    _setProperty( "nMTtrack", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) );

    _setProperty( "HistogramL", "DLL_Long" );
    _setProperty( "HistogramD", "DLL_Downstream" );
    _setProperty( "HistogramT", "DLL_Ttrack" );
    _setProperty( "ConditionName", "Conditions/ParticleID/Calo/EcalPIDe" );

    _setProperty( "HistogramL_THS", "CaloPIDs/CALO/ECALPIDE/h3" );
    _setProperty( "HistogramD_THS", "CaloPIDs/CALO/ECALPIDE/h5" );
    _setProperty( "HistogramT_THS", "CaloPIDs/CALO/ECALPIDE/h6" );

    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                               LHCb::Track::Types::Downstream ) );
  };
};

// ============================================================================

DECLARE_COMPONENT( EcalPIDeAlg )

// ============================================================================

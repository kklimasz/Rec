/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloChi22ID.h"

// =============================================================================
/** @file
 *  Implementation file for class CaloChi22ID
 *  @date 2006-06-18
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// =============================================================================

// DECLARE_COMPONENT( CaloChi22ID )

// =============================================================================
/// Standard protected constructor
// =============================================================================
template <typename TABLEI, typename TABLEO>
CaloChi22ID<TABLEI, TABLEO>::CaloChi22ID( const std::string& name, ISvcLocator* pSvcLocator )
    : base_type( name, pSvcLocator, {KeyValue{"Tracks", ""}, KeyValue{"Input", ""}}, KeyValue{"Output", ""} ) {
  // context-dependent default track container
  updateHandleLocation( *this, "Tracks", LHCb::CaloAlgUtils::TrackLocations( context() ).front() );
}

// =============================================================================
/// algorithm execution
// =============================================================================
template <typename TABLEI, typename TABLEO>
TABLEO CaloChi22ID<TABLEI, TABLEO>::operator()( const LHCb::Tracks& tracks, const TABLEI& input ) const {

  TABLEO output( input.relations().size() + 10 );
  for ( auto const& track : tracks ) {
    if ( !use( track ) ) continue;
    auto links = input.inverse()->relations( track );
    // fill the relation table
    auto chi2 = links.empty() ? m_large.value() : links.front().weight();
    output.i_push( track, chi2 );
  }
  /// MANDATORY: i_sort after i_push
  output.i_sort();

  m_nTracks += tracks.size();
  m_nLinks += output.i_relations().size();

  return output;
}

// =============================================================================

template class CaloChi22ID<LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>,
                           LHCb::Relation1D<LHCb::Track, float>>;
template class CaloChi22ID<LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>,
                           LHCb::Relation1D<LHCb::Track, float>>;

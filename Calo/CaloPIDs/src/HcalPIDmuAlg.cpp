/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloID2DLL.h"

// ============================================================================
/** @class HcalPIDmuAlg  HcalPIDmuAlg.cpp
 *  The preconfigured instance of class CaloID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class HcalPIDmuAlg final : public CaloID2DLL {
public:
  HcalPIDmuAlg( const std::string& name, ISvcLocator* pSvc ) : CaloID2DLL( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    updateHandleLocation( *this, "Input", CaloIdLocation( "HcalE", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "HcalPIDmu", context() ) );

    _setProperty( "nVlong", Gaudi::Utils::toString( 10 * Gaudi::Units::GeV ) );
    _setProperty( "nVdown", Gaudi::Utils::toString( 10 * Gaudi::Units::GeV ) );
    _setProperty( "nVTtrack", Gaudi::Utils::toString( 10 * Gaudi::Units::GeV ) );
    _setProperty( "nMlong", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) );
    _setProperty( "nMdown", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) );
    _setProperty( "nMTtrack", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) );

    _setProperty( "HistogramL", "DLL_Long" );
    _setProperty( "HistogramD", "DLL_Downstream" );
    _setProperty( "HistogramT", "DLL_Ttrack" );
    _setProperty( "ConditionName", "Conditions/ParticleID/Calo/HcalPIDmu" );

    _setProperty( "HistogramL_THS", "CaloPIDs/CALO/HCALPIDM/h3" );
    _setProperty( "HistogramD_THS", "CaloPIDs/CALO/HCALPIDM/h5" );
    _setProperty( "HistogramT_THS", "CaloPIDs/CALO/HCALPIDM/h6" );

    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                               LHCb::Track::Types::Downstream ) );
  };
};

// ============================================================================

DECLARE_COMPONENT( HcalPIDmuAlg )

// ============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class BremChi22ID BremChi22ID.cpp
 *  The preconfigured instance of class CaloChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct BremChi22ID final : public CaloChi22ID<TABLEI, TABLEO> {
  static_assert( std::is_base_of<LHCb::Calo2Track::IHypoTrTable2D, TABLEI>::value,
                 "TABLEI must inherit from IHypoTrTable2D" );
  BremChi22ID( const std::string& name, ISvcLocator* pSvc ) : CaloChi22ID<TABLEI, TABLEO>( name, pSvc ) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    updateHandleLocation( *this, "Input", CaloIdLocation( "BremMatch", context() ) );
    updateHandleLocation( *this, "Output", CaloIdLocation( "BremChi2", context() ) );
    // @todo it must be in agrement with "Threshold" for BremMatchAlg
    _setProperty( "CutOff", "10000" );
    // track types:
    _setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                                               LHCb::Track::Types::Upstream ) );
  };
};

// ============================================================================

DECLARE_COMPONENT( BremChi22ID )

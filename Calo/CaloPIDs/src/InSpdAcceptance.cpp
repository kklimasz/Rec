/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloAcceptance.h"

// ============================================================================
/** @class InSpdAcceptance
 *  The precofigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InSpdAcceptance final : InCaloAcceptance {
  /// standard constructor
  InSpdAcceptance( const std::string& type, const std::string& name, const IInterface* parent )
      : InCaloAcceptance( type, name, parent ) {
    _setProperty( "Calorimeter", DeCalorimeterLocation::Spd );
    _setProperty( "UseFiducial", "false" );
    _setProperty( "Tolerance", "1" ); /// 1 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InSpdAcceptance()                         = delete;
  InSpdAcceptance( const InSpdAcceptance& ) = delete;
  InSpdAcceptance& operator=( const InSpdAcceptance& ) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InSpdAcceptance )

// ============================================================================

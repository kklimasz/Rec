/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloMoniAlg.h"
#include "Event/CaloDigit.h"
#include "GaudiAlg/Consumer.h"

// using namespace LHCb;
// using namespace Gaudi::Units;

//==============================================================================
/** @file
 *
 *  Implementation file for class SpdMonitor
 *
 *  SPD Monitoring
 *
 *  @author Albert Puig apuignav@cern.ch
 *  @date   2007-15-07
 */
//==============================================================================

using Input = LHCb::CaloDigits;

class SpdMonitor final
    : public Gaudi::Functional::Consumer<void( const Input& ), Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  StatusCode initialize() override;
  StatusCode finalize() override;
  void       operator()( const Input& ) const override;

  SpdMonitor( const std::string& name, ISvcLocator* pSvc );

private:
  DeCalorimeter*       m_detSpd = nullptr;
  CaloVector<int>      m_neighN;
  mutable unsigned int m_nEvents = 0;
};

//==============================================================================

DECLARE_COMPONENT( SpdMonitor )

//==============================================================================

SpdMonitor::SpdMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{"Input", LHCb::CaloDigitLocation::Spd},
                } ) {
  m_split = true; // Area splitting is the default !
}

//==============================================================================

StatusCode SpdMonitor::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  // Loads the detectors
  m_detSpd = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
  // Histograms
  bookCalo2D( "1", "Spd occupancy", "Spd" );
  bookCalo2D( "2", "Spd neighbor occupancy", "Spd" );

  // Initialize neighbor matrix
  for ( unsigned int cellIt = 0; cellIt != m_detSpd->numberOfCells(); cellIt++ ) {
    const auto& cell  = m_detSpd->cellIdByIndex( cellIt );
    const auto& neigh = m_detSpd->zsupNeighborCells( cell );
    m_neighN.addEntry( neigh.size(), cell );
  }
  return StatusCode::SUCCESS;
}

//==============================================================================

StatusCode SpdMonitor::finalize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;
  info() << "Number of Events Analyzed : " << m_nEvents << endmsg;
  return Consumer::finalize();
}

//==============================================================================

void SpdMonitor::operator()( const Input& digitsSpd ) const {

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Execute " << endmsg;

  // Fill histos
  for ( const auto& digit : digitsSpd ) {
    if ( digit == nullptr ) continue;
    const auto cell = digit->cellID();
    // histo1
    if ( doHisto( "1" ) ) fillCalo2D( "1", cell, 1.0 );
    // histo2
    if ( doHisto( "2" ) ) {
      const auto neighs = m_detSpd->zsupNeighborCells( cell );
      for ( const auto neighCell : neighs ) {
        const auto value = (float)1. / float( m_neighN[cell] );
        fillCalo2D( "2", neighCell, value );
      }
    }
  }
  m_nEvents++;
  return;
}

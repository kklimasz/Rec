/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// from Gaudi
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloMomentum.h"
#include "Event/MCParticle.h"
// locals
#include "CaloHypoNtp.h"
#include "CaloMoniUtils.h"

namespace {
  /// hack to allow for tools with non-const interfaces...
  template <typename IFace>
  IFace* fixup( const ToolHandle<IFace>& iface ) {
    return &const_cast<IFace&>( *iface );
  }
} // namespace
// =============================================================================

DECLARE_COMPONENT( CaloHypoNtp )

// =============================================================================

CaloHypoNtp::CaloHypoNtp( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default},
                 KeyValue{"InputL0", LHCb::L0DUReportLocation::Default}, KeyValue{"Locations", ""},
                 KeyValue{"TrackLoc", LHCb::TrackLocation::Default},
                 KeyValue{"VertexLoc", LHCb::RecVertexLocation::Primary}} ) {}

// =============================================================================

StatusCode CaloHypoNtp::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  // retrieve tools
  if ( !( m_2MC.retrieve() && m_counterStat.retrieve() && m_estimator.retrieve() ) ) {
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }

  // Configure tool
  std::string seed = m_seed ? "true" : "false";
  std::string line = m_extrapol ? "true" : "false";
  std::string neig = m_neig ? "true" : "false";
  m_estimator->hypo2Calo()->_setProperty( "Seed", seed ).ignore();
  m_estimator->hypo2Calo()->_setProperty( "PhotonLine", line ).ignore();
  m_estimator->hypo2Calo()->_setProperty( "AddNeighbors", neig ).ignore();

  // Configure input locations
  using namespace LHCb::CaloHypoLocation;
  // get the user-override input
  auto                     loc = inputLocation<2>(); // <-- Careful with the index, 'Location' is 3rd.
  std::vector<std::string> m_locs{};
  if ( loc != "" ) m_locs.push_back( loc );
  // get the respective input from Hypos
  for ( const auto h : m_hypos ) {
    if ( h == "Photons" )
      m_locs.push_back( LHCb::CaloAlgUtils::PathFromContext( context(), Photons ) );
    else if ( h == "Electrons" )
      m_locs.push_back( LHCb::CaloAlgUtils::PathFromContext( context(), Electrons ) );
    else if ( h == "MergedPi0s" )
      m_locs.push_back( LHCb::CaloAlgUtils::PathFromContext( context(), MergedPi0s ) );
    else if ( h == "SplitPhotons" )
      m_locs.push_back( LHCb::CaloAlgUtils::PathFromContext( context(), SplitPhotons ) );
  }
  // Finally, update handle
  updateHandleLocation( *this, "Locations", boost::algorithm::join( m_locs, "&" ) );

  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloHypoNtp::operator()( const ODIN& odin, const L0& l0, const Hypos& hypos, const Tracks& tracks,
                              const Vertices& verts ) const {

  using namespace CaloDataType;

  // declare tuple
  Tuple ntp = ( !m_tupling ) ? Tuple{nullptr} : nTuple( 500, "HypoNtp", CLID_ColumnWiseTuple );

  // Collect global info (in pre-Functional implementation, some are optionals)
  const auto run    = odin.runNumber();
  const auto evt    = (double)odin.eventNumber();
  const auto tty    = odin.triggerType();
  const auto nSpd   = (int)l0.dataValue( "Spd(Mult)" );
  const auto nTrack = tracks.size();
  const auto nVert  = verts.size();

  // loop over hypo containers
  bool ok = true;
  for ( const auto& hypo : hypos ) {
    // hypo string
    std::ostringstream type( "" );
    type << hypo->hypothesis();
    std::string hypothesis = type.str();

    // filtering hypo
    if ( !inRange( m_et, fixup( m_estimator )->data( hypo, HypoEt, 0. ) ) ) continue;
    if ( !inRange( m_e, fixup( m_estimator )->data( hypo, HypoE, 0. ) ) ) continue;
    if ( !inRange( m_spdM, fixup( m_estimator )->data( hypo, HypoSpdM, 0. ) ) ) continue;
    if ( !inRange( m_prsE, fixup( m_estimator )->data( hypo, HypoPrsE, 0. ) ) ) continue;

    // MC-associated filtering
    if ( m_checker ) {
      const auto mcp = fixup( m_2MC )->from( hypo )->bestMC();
      if ( mcp == nullptr ) continue;
      if ( m_mcID >= 0 && (int)mcp->particleID().abspid() != m_mcID ) continue;
    }

    // PrintOut
    if ( m_print ) {
      info() << "+++ Run/Evt " << run << "/" << evt << endmsg;
      info() << " === hypothesis " << hypo->hypothesis() << "(" << inputLocation<2>() << ")" << endmsg;
      if ( m_checker ) fixup( m_2MC )->from( hypo )->descriptor();
    }

    // DataTypes statistics
    for ( int i = 0; i < CaloDataType::Last; ++i ) {
      const auto val = fixup( m_estimator )->data( hypo, (DataType)i, 0. );
      if ( m_stat && m_counterStat->isQuiet() ) counter( Name[i] + " for " + hypothesis ) += val;
      if ( m_tupling ) ok &= ntp->column( Name[i], val );
      if ( m_print ) info() << "   --> " << Name[i] << " : " << val << endmsg;
    }

    // Tupling
    if ( m_tupling ) {

      // hypothesis
      ok &= ntp->column( "hypothesis", hypo->hypothesis() );

      // kinematics
      const auto cluster = fixup( m_estimator )->toCluster( CaloClusterType::SplitOrMain );
      ok &= ntp->column( "ClusterR", position3d( cluster ) );
      ok &= ntp->column( "HypoR", position3d( hypo ) );
      ok &= ntp->column( "HypoP", momentum( hypo ) );

      // matched tracks
      for ( int match = 0; match < CaloMatchType::Last; ++match ) {
        const auto track = fixup( m_estimator )->toTrack( (CaloMatchType::MatchType)match );
        ok &= ntp->column( CaloMatchType::Name[match] + "TrackP", momentum( track ) );
      }

      // odin info
      ok &= ntp->column( "Run", run );
      ok &= ntp->column( "Event", evt );
      ok &= ntp->column( "Triggertype", tty );
      ok &= ntp->column( "Nvertices", nVert );
      ok &= ntp->column( "NTracks", nTrack );
      ok &= ntp->column( "spdMult", nSpd );

      // Checker Mode (MC info)
      if ( m_checker ) {
        int        id      = -999;
        double     weight  = -999;
        double     quality = -999;
        const auto mcp     = fixup( m_2MC )->from( hypo )->bestMC();
        if ( mcp != nullptr ) {
          id      = mcp->particleID().pid();
          weight  = fixup( m_2MC )->from( hypo )->weight( mcp );
          quality = fixup( m_2MC )->from( hypo )->quality( mcp );
        }
        ok &= ntp->column( "MCid", id );
        ok &= ntp->column( "MCw", weight );
        ok &= ntp->column( "MCq", quality );
      }
      ok &= ntp->write();
    }
  }

  // Finally, report
  if ( ok ) {
    if ( m_counterStat->isQuiet() ) counter( "Events in tuple" ) += 1;
  } else {
    Warning( "Error with ntupling", StatusCode::SUCCESS ).ignore();
  }
  return;
}

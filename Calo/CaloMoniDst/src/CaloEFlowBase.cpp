/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloEFlowBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloEFlowAlg
//
// 2009-04-08 : Aurelien Martens
//-----------------------------------------------------------------------------

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloEFlowBase::initialize() {
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;
  StatusCode sc = CaloMoniAlg::initialize();
  if ( sc.isFailure() ) return sc;

  // Configure pointer to detector
  const auto loc = deCaloLocation();
  if ( loc == "" ) { return Error( "Unknown detector name " + detData() ); }
  m_calo = getDetIfExists<DeCalorimeter>( loc );
  if ( !m_calo ) return Error( "No DeCalorimeter for " + detData() );

  hBook1( "4", detData() + " : # of Digits", m_calo->numberOfCells(), 0, m_calo->numberOfCells() );

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " initialized" << endmsg;
  return StatusCode::SUCCESS;
}

//==============================================================================
// Main execution
//==============================================================================

void CaloEFlowBase::process_digits( const Input& digits ) const {
  // Reset the counter
  initCounters();

  // Loop over digits
  for ( const auto& digit : digits ) {
    if ( 0 == digit ) continue;

    const auto id = digit->cellID();
    if ( !( m_calo->valid( id ) && !m_calo->isPinId( id ) ) ) continue;

    const double e  = digit->e();
    const double et = e * m_calo->cellSine( id );

    if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) )
      verbose() << " before thresholds :  cellID " << id.index() << " e " << e << " et " << et << endmsg;

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "thresholds are EMin " << m_eFilterMin << " EMax " << m_eFilterMax << " EtMin " << m_etFilterMin
              << " EtMax " << m_etFilterMax << endmsg;

    const double pedShift = m_calo->pedestalShift();
    const double gain     = m_calo->cellGain( id );
    const int    adc      = ( gain != 0. ) ? (int)floor( ( e + pedShift ) / gain ) : (int)0;

    // Skip poor input
    if ( adc < m_ADCFilterMin && m_ADCFilterMin != -999 ) continue;
    if ( adc > m_ADCFilterMax && m_ADCFilterMax != -999 ) continue;
    if ( e < m_eFilterMin && m_eFilterMin != -999 ) continue;
    if ( e > m_eFilterMax && m_eFilterMax != -999 ) continue;
    if ( et < m_etFilterMin && m_etFilterMin != -999 ) continue;
    if ( et > m_etFilterMax && m_etFilterMax != -999 ) continue;

    if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) )
      verbose() << " cellID " << id.index() << " e " << e << " et " << et << endmsg;

    count( id );
    if ( doHisto( "1" ) ) fillCalo2D( "1", id, 1., detData() + " digits position 2D view" );
    if ( doHisto( "2" ) ) fillCalo2D( "2", id, e, detData() + " energy weighted - digits position 2D view" );
    if ( doHisto( "3" ) ) fillCalo2D( "3", id, et, detData() + " Et weighted - digits position 2D view" );

  } // end loop over digits

  // Finally
  fillCounters( "4" );
}

//==============================================================================

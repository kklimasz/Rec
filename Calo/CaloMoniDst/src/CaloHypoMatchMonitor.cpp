/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloMoniAlg.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "Relations/RelationWeighted2D.h"

// =============================================================================

/** @class CaloHypoMatchMonitor CaloHypoMatchMonitor.cpp
 *
 *  The algorithm for trivial monitoring of matching of
 *  "CaloClusters" with Tracks.
 *  It produces 5 histograms:
 *
 *  <ol>
 *  <li> @p log10(#Relations+1)  distribution               </li>
 *  <li> Link multiplicity       distribution               </li>
 *  <li> Minimal Weight          distribution               </li>
 *  <li> Maximal Weight          distribution               </li>
 *  <li>         Weight          distribution               </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input  = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using Inputs = LHCb::CaloHypo::Container;

class CaloHypoMatchMonitor final
    : public Gaudi::Functional::Consumer<void( const Input&, const Inputs& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const Inputs& ) const override;

  CaloHypoMatchMonitor( const std::string& name, ISvcLocator* pSvcLocator );
};

// =============================================================================

DECLARE_COMPONENT( CaloHypoMatchMonitor )

// =============================================================================

CaloHypoMatchMonitor::CaloHypoMatchMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"Inputs", ""}} ) {
  /*
   * During genconf.exe, the default name "DefaultName" is not well-supported
   * by the CaloAlgUtils, returning the null string "" as a location path,
   * which will raise exception in `updateHandleLocation` --> `setProperty`.
   * To get around this, the location will only be updated outside the genconf.
   */
  if ( name != "DefaultName" ) {
    const auto Input  = LHCb::CaloAlgUtils::CaloIdLocation( name, context() );
    const auto Inputs = LHCb::CaloAlgUtils::CaloHypoLocation( name, context() );
    updateHandleLocation( *this, "Input", Input );
    updateHandleLocation( *this, "Inputs", Inputs );
  }
}

// =============================================================================

/// standard algorithm initialization
StatusCode CaloHypoMatchMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error already printedby GaudiAlgorithm
  std::string common = "'" + inputLocation() + "' " + name();
  hBook1( "1", "log10(#Links+1) " + common, 0, 4, 100 );
  hBook1( "2", "Rels per Hypo   " + common, 0, 25, 50 );
  hBook1( "3", "Min weight      " + common, 0, 100, 200 );
  hBook1( "4", "Max weight      " + common, 0, 1000, 200 );
  hBook1( "5", "Weights         " + common, 0, 1000, 500 );
  if ( m_split ) {
    Warning( "No area spliting allowed for CaloHypoMatchMonitor" ).ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloHypoMatchMonitor::operator()( const Input& table, const Inputs& hypos ) const {

  if ( !produceHistos() ) return;

  // logarithm of ( total number of links + 1 )
  hFill1( "1", log10( table.relations().size() + 1. ) );

  // loop over all hypos
  for ( const auto& hypo : hypos ) {
    const auto range = table.relations( hypo );
    // number of related tracks
    hFill1( "2", range.size() );
    if ( range.empty() ) continue;
    // minimal weight
    hFill1( "3", range.front().weight() );
    // maximal weight
    hFill1( "4", range.back().weight() );
    // all weights
    for ( const auto& relation : range ) { hFill1( "5", relation.weight() ); }
  } // end of loop over hypos

  if ( m_counterStat->isQuiet() ) counter( "Monitor " + inputLocation<1>() ) += hypos.size();
  if ( m_counterStat->isQuiet() ) counter( "Monitor " + inputLocation() ) += table.relations().size();

  return;
}

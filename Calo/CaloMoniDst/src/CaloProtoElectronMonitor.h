/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOPROTOELECTRONMONITOR_H
#define CALOPROTOELECTRONMONITOR_H 1

// Includes
#include "CaloMoniAlg.h"
#include "CaloUtils/ICaloElectron.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/Consumer.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

namespace {
  using Input = LHCb::ProtoParticles;
}

/** @class CaloProtoElectronMonitor CaloProtoElectronMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloProtoElectronMonitor final
    : public Gaudi::Functional::Consumer<void( const Input& ), Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  /// Standard constructor
  CaloProtoElectronMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const Input& ) const override;

  /// C++11 non-copyable idiom
  CaloProtoElectronMonitor( const CaloProtoElectronMonitor& ) = delete;
  CaloProtoElectronMonitor& operator=( const CaloProtoElectronMonitor& ) = delete;

private:
  ToolHandle<ICaloElectron>      m_caloElectron{"CaloElectron", this};
  ToolHandle<ITrackExtrapolator> m_extrapolator{"TrackRungeKuttaExtrapolator/Extrapolator", this};

  bool valid_track( const LHCb::ProtoParticle* proto ) const;

  Gaudi::Property<float> m_eOpMin{this, "HistoEoPMin", 0.};

  Gaudi::Property<float> m_eOpMax{this, "HistoEoPMax", 3.};

  Gaudi::Property<int> m_eOpBin{this, "HistoEoPBin", 100};

  Gaudi::Property<float> m_prsCut{this, "PrsCut", 50. * Gaudi::Units::MeV};

  Gaudi::Property<bool> m_pairing{this, "ElectronPairing", false};

  Gaudi::Property<std::vector<int>> m_tracks{
      this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};
};
#endif // CALOPROTOELECTRONMONITOR_H

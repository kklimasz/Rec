/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloMoniAlg.h"
#include "Event/CaloDigit.h"
#include "GaudiAlg/Consumer.h"

// =============================================================================

/** @class CaloDigitMonitor CaloDigitMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloDigit" containers.
 *  The algorithm produces the following histograms:
 *   1. CaloDigit multiplicity
 *   2. CaloDigit ocupancy 2D plot per area
 *   3. CaloDigit energy 2D plot per area
 *  The same set of histograms, but with cut on Et (or E), is produced if specified
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @p "Name" is the name of the algorithm
 *
 *  @see   CaloMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Konstantin Belous Konstantin.Beloous@itep.ru
 *  @date   21/06/2007
 */

using Input = LHCb::CaloDigit::Container;

class CaloDigitMonitor final
    : public Gaudi::Functional::Consumer<void( const Input& ), Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input& ) const override;

  CaloDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  DeCalorimeter*        m_calo = nullptr;
  Gaudi::Property<bool> m_spectrum{this, "Spectrum", false, "activate spectrum per channel histogramming"};
};

// =============================================================================

DECLARE_COMPONENT( CaloDigitMonitor )

// =============================================================================

CaloDigitMonitor::CaloDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"Input", LHCb::CaloAlgUtils::CaloDigitLocation( "" )} ) {
  updateHandleLocation( *this, "Input", LHCb::CaloAlgUtils::CaloDigitLocation( name ) );
}

// =============================================================================
// standard initialize method
// =============================================================================

StatusCode CaloDigitMonitor::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  if ( "Ecal" == detData() ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == detData() ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == detData() ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == detData() ) {
    m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
  } else {
    return Error( "Unknown detector name " + detData() );
  }

  hBook1( "1", detData() + " : # of Digits", m_multMin, m_multMax, m_multBin );
  if ( detData() != "Spd" ) hBook1( "2", detData() + "  digits energy", m_energyMin, m_energyMax, m_energyBin );
  if ( detData() != "Spd" && detData() != "Prs" ) hBook1( "3", detData() + "  digits Et", m_etMin, m_etMax, m_etBin );
  hBook1( "4", "Hypo X        " + inputLocation(), m_xMin, m_xMax, m_xBin );
  hBook1( "5", "Hypo Y        " + inputLocation(), m_yMin, m_yMax, m_yBin );
  hBook2( "6", "Digit position x vs y   " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin, m_yMax, m_yBin );
  hBook2( "7", "Energy-weighted digit position x vs y " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin, m_yMax,
          m_yBin );

  info() << detData() << " digits from " << inputLocation() << endmsg;

  return StatusCode::SUCCESS;
}

// ============================================================================
// standard execution method
// ============================================================================

void CaloDigitMonitor::operator()( const Input& digits ) const {

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << name() << " execute " << endmsg;

  // produce histos ?
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " Producing histo " << produceHistos() << endmsg;
  if ( !produceHistos() ) return; // StatusCode::SUCCESS;

  if ( digits.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
    return;
  }

  initCounters();

  for ( const auto& digit : digits ) {
    if ( digit == nullptr ) continue;
    const auto id = digit->cellID();
    const auto e  = digit->e();
    const auto et = e * m_calo->cellSine( id );
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    count( id );
    hFill1( id, "2", e );
    if ( detData() != "Spd" && detData() != "Prs" ) hFill1( id, "3", et );
    const double x = m_calo->cellCenter( id ).X();
    const double y = m_calo->cellCenter( id ).Y();
    hFill1( id, "4", x );
    hFill1( id, "5", y );
    hFill2( id, "6", x, y );
    hFill2( id, "7", x, y, e );
    if ( doHisto( "8" ) ) fillCalo2D( "8", id, 1., detData() + " digits position 2D view" );
    if ( detData() != "Spd" && doHisto( "9" ) ) fillCalo2D( "9", id, e, detData() + " digits energy 2D view" );

    if ( m_spectrum ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Filling cell by cell histograms" << endmsg;
      const int          col = id.col();
      const int          row = id.row();
      std::ostringstream tit;
      tit << detData() << " channel : " << id;
      const auto unit = detData() + "Cells/" + id.areaName() + "/" + Gaudi::Utils::toString( row ) + ";" +
                        Gaudi::Utils::toString( col );
      if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) verbose() << " et  " << et << " cell " << unit << endmsg;
      if ( detData() == "Prs" )
        plot1D( e, unit, tit.str(), m_energyMin, m_energyMax, m_energyBin );
      else
        plot1D( et, unit, tit.str(), m_etMin, m_etMax, m_etBin );
    }
  }
  fillCounters( "1" );
  return; // StatusCode::SUCCESS;
}

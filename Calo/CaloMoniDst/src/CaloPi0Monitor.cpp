/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloMoniAlg.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloUtils/CaloParticle.h"
#include "Event/CaloHypo.h"
#include "Event/Particle.h"
#include "GaudiAlg/Consumer.h"
#include <vector>

namespace {
  // hack to allow for tools with non-const interfaces...
  template <typename IFace>
  IFace* fixup( const ToolHandle<IFace>& iface ) {
    return &const_cast<IFace&>( *iface );
  }
} // namespace

// =============================================================================

/** @class CaloPi0Monitor CaloPi0Monitor.cpp
 *
 *  Simple pi0 monitoring algorithm
 *
 *  @see   CaloMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::CaloHypo::Container;

class CaloPi0Monitor final
    : public Gaudi::Functional::Consumer<void( const Input& ), Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input& ) const override;

  CaloPi0Monitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  DeCalorimeter*             m_calo = nullptr;
  ToolHandle<ICaloHypo2Calo> m_toSpd{"CaloHypo2Calo/CaloHypo2Spd", this};
  ToolHandle<ICaloHypo2Calo> m_toPrs{"CaloHypo2Calo/CaloHypo2Prs", this};

  bool valid_photon( const LHCb::CaloHypo* g ) const;

  Gaudi::Property<float> m_ptPhoton{this, "PhotonPtFilter", 250 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_ptMaxPhoton{this, "PhotonMaxPtFilter", 0};
  Gaudi::Property<float> m_isol{this, "IsolationFilter", 4};
  Gaudi::Property<float> m_prsPhoton{this, "PhotonPrsFilterMin", 10 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_yCut{this, "RejectedYBand", 300};
  Gaudi::Property<bool>  m_conv{this, "AllowConverted", false};
};

// =============================================================================

DECLARE_COMPONENT( CaloPi0Monitor )

// =============================================================================

CaloPi0Monitor::CaloPi0Monitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"Input", ""} ) {
  m_multMax        = 150;
  const auto Input = LHCb::CaloAlgUtils::CaloHypoLocation( "Photons", context() );
  updateHandleLocation( *this, "Input", Input );
}

// =============================================================================

StatusCode CaloPi0Monitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error already printedby GaudiAlgorithm
  hBook1( "1", "(gg) multiplicity " + inputLocation(), m_multMin, m_multMax, m_multBin );
  hBook1( "2", "(gg) energy " + inputLocation(), m_energyMin, m_energyMax, m_energyBin );
  hBook1( "3", "(gg) et     " + inputLocation(), m_etMin, m_etMax, m_etBin );
  hBook1( "4", "(gg) mass   " + inputLocation(), m_massMin, m_massMax, m_massBin );
  hBook1( "5", "(gg) combinatorial background" + inputLocation(), m_massMin, m_massMax, m_massBin );
  hBook1( "6", "bkg-substracted (gg) mass   " + inputLocation(), m_massMin, m_massMax, m_massBin );
  hBook1( "7", "(gg) mass  for |y|-gamma > " + Gaudi::Utils::toString( m_yCut ) + " " + inputLocation(), m_massMin,
          m_massMax, m_massBin );
  hBook2( "8", "(gg) mass per cell  " + inputLocation(), 0, 6016, 6016, m_massMin, m_massMax, m_massBin );

  // Get, retrieve, configure tools
  m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  if ( !( m_toSpd.retrieve() && m_toPrs.retrieve() ) ) {
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }
  m_toSpd->setCalos( "Ecal", "Spd" );
  m_toPrs->setCalos( "Ecal", "Prs" );

  // set mass window to histo range
  m_massFilterMin = m_massMin;
  m_massFilterMax = m_massMax;

  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloPi0Monitor::operator()( const Input& photons ) const {

  if ( !produceHistos() ) return;
  if ( photons.empty() ) return;

  // Params for lookup later down the loop
  const auto& cells = m_calo->cellParams();

  // loop over the first photon
  initCounters();
  for ( auto g1 = photons.begin(); photons.end() != g1; ++g1 ) {
    if ( !valid_photon( *g1 ) ) continue;
    LHCb::CaloMomentum   momentum1( *g1 );
    Gaudi::LorentzVector v1( momentum1.momentum() );

    // loop over the second photon
    for ( auto g2 = std::next( g1 ); photons.end() != g2; ++g2 ) {
      if ( !valid_photon( *g2 ) ) continue;
      LHCb::CaloMomentum momentum2( *g2 );
      if ( std::max( momentum1.pt(), momentum2.pt() ) < m_ptMaxPhoton ) continue;
      Gaudi::LorentzVector v2( momentum2.momentum() );

      // background shape from (x,y)->(-x,-y) symmetrized g2
      Gaudi::LorentzVector v2Sym( v2 );
      v2Sym.SetPx( -v2.Px() );
      v2Sym.SetPy( -v2.Py() );
      Gaudi::LorentzVector bkg( v1 + v2Sym );
      Gaudi::XYZPoint      p2Sym( -( *g2 )->position()->x(), -( *g2 )->position()->y(), ( *g2 )->position()->z() );
      bool                 isBkg = ( bkg.e() > m_eFilter && bkg.pt() > m_etFilter && bkg.mass() > m_massFilterMin &&
                     bkg.mass() < m_massFilterMax );

      // check pi0
      Gaudi::LorentzVector pi0( v1 + v2 );
      bool                 isPi0 = ( pi0.e() > m_eFilter && pi0.pt() > m_etFilter && pi0.mass() > m_massFilterMin &&
                     pi0.mass() < m_massFilterMax );

      if ( !isPi0 && !isBkg ) continue;

      // Get cellIDs
      auto id1 = LHCb::CaloCellID();
      auto id2 = LHCb::CaloCellID();
      if ( ( *g1 )->clusters().size() > 0 ) {
        const auto cluster = *( ( *g1 )->clusters().begin() );
        if ( cluster != 0 ) id1 = cluster->seed();
      }
      if ( ( *g2 )->clusters().size() > 0 ) {
        const auto cluster = *( ( *g2 )->clusters().begin() );
        if ( cluster != 0 ) id2 = cluster->seed();
      }

      // define pi0 area
      const auto id = ( id1.area() == id2.area() ) ? id1 : LHCb::CaloCellID();

      // isolation criteria
      Gaudi::XYZPoint p1( ( *g1 )->position()->x(), ( *g1 )->position()->y(), ( *g1 )->position()->z() );
      Gaudi::XYZPoint p2( ( *g2 )->position()->x(), ( *g2 )->position()->y(), ( *g2 )->position()->z() );
      const auto      vec     = p2 - p1;
      const auto      vecSym  = p2Sym - p1;
      const auto      cSize   = std::max( m_calo->cellSize( id1 ), m_calo->cellSize( id2 ) );
      const auto      isol    = ( cSize > 0 ) ? vec.Rho() / cSize : 0;
      const auto      isolSym = ( cSize > 0 ) ? vecSym.Rho() / cSize : 0;
      const auto      y1      = m_calo->cellCenter( id1 ).Y();
      const auto      y2      = m_calo->cellCenter( id2 ).Y();

      if ( isPi0 && isol > m_isol ) {
        count( id );
        hFill1( id, "2", pi0.e() );
        hFill1( id, "3", pi0.pt() );
        hFill1( id, "4", pi0.mass() );
        hFill1( id, "6", pi0.mass(), 1. );
        if ( fabs( y1 ) > m_yCut && fabs( y2 ) > m_yCut ) hFill1( "7", pi0.mass(), 1. );
        int index = cells.index( id );
        hFill2( "8", index, pi0.mass() );
      }
      if ( isBkg && isolSym > m_isol ) {
        hFill1( id, "5", bkg.mass() );
        hFill1( id, "6", bkg.mass(), -1. );
      }
    }
  }
  fillCounters( "1" );
  return; // StatusCode::SUCCESS;
}

// =============================================================================

bool CaloPi0Monitor::valid_photon( const LHCb::CaloHypo* g ) const {
  // Return True if this photon is valid to continue the computation
  if ( g == nullptr ) return false;
  if ( !m_conv && fixup( m_toSpd )->multiplicity( *g, "Spd" ) > 0 ) return false;
  if ( fixup( m_toPrs )->energy( *g, "Prs" ) < m_prsPhoton ) return false;
  LHCb::CaloMomentum momentum( g );
  if ( momentum.pt() < m_ptPhoton ) return false;
  return true;
}

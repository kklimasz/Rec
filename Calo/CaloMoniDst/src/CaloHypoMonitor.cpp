/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloMoniAlg.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloMomentum.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/Consumer.h"

// =============================================================================

/** @class CaloHypoMonitor CaloHypoMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloHypo" container
 *  The algorithm produces 10 histograms:
 *  <ol>
 *  <li> @p CaloHypo multiplicity                           </li>
 *  <li> @p CaloHypo energy distribution                    </li>
 *  <li> @p CaloHypo transverse momentum distribution       </li>
 *  <li> @p CaloHypo mass distribution                      </li>
 *  <li> @p CaloHypo x distribution                         </li>
 *  <li> @p CaloHypo y distribution                         </li>
 *  <li> multiplicity of     @p CaloCluster per @p CaloHypo </li>
 *  <li> multiplicity of Spd @p CaloDigit   per @p CaloHypo </li>
 *  <li> multiplicity of Prs @p CaloDigit   per @p CaloHypo </li>
 *  <li> CaloHypo x vs y distribution                       </li>
 *  </ol>
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see CaloAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::CaloHypo::Container;

class CaloHypoMonitor final : public Gaudi::Functional::Consumer<void( const Input&, const LHCb::CaloClusters& ),
                                                                 Gaudi::Functional::Traits::BaseClass_t<CaloMoniAlg>> {
public:
  /// standard algorithm initialization
  StatusCode initialize() override;
  void       operator()( const Input&, const LHCb::CaloClusters& ) const override;

  CaloHypoMonitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  Gaudi::Property<int>   m_clusBin{this, "NClusterBin", 5};
  Gaudi::Property<float> m_clusMax{this, "NClusterMax", 5.};
  Gaudi::Property<float> m_clusMin{this, "NClusterMin", 0.};
  Gaudi::Property<int>   m_spdBin{this, "NSpdBin", 10};
  Gaudi::Property<float> m_spdMax{this, "NSpdMax", 10.};
  Gaudi::Property<float> m_spdMin{this, "NSpdMin", 0.};
  Gaudi::Property<int>   m_prsBin{this, "NPrsBin", 10};
  Gaudi::Property<float> m_prsMax{this, "NPrsMax", 10.};
  Gaudi::Property<float> m_prsMin{this, "NPrsMin", 0.};
};

DECLARE_COMPONENT( CaloHypoMonitor )

// =============================================================================

CaloHypoMonitor::CaloHypoMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{"Input", ""},
                    KeyValue{"InputClusters", ""},
                } ) {
  m_multMax = 250;
  m_multBin = 50;

  /*
   * During genconf.exe, the default name "DefaultName" is not well-supported
   * by the CaloAlgUtils, returning the null string "" as a location path,
   * which will raise exception in `updateHandleLocation` --> `setProperty`.
   * To get around this, the location will only be updated outside the genconf.
   */
  if ( name != "DefaultName" ) {
    const auto InputData     = LHCb::CaloAlgUtils::CaloHypoLocation( name, context() );
    const auto InputClusters = LHCb::CaloAlgUtils::CaloClusterLocation( name, context() );
    updateHandleLocation( *this, "Input", InputData );
    updateHandleLocation( *this, "InputClusters", InputClusters );
  }
}

// =============================================================================

/// standard algorithm initialization
StatusCode CaloHypoMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error already printedby GaudiAlgorithm
  hBook1( "1", "# of Hypos    " + inputLocation(), m_multMin, m_multMax, m_multBin );
  hBook1( "2", "Hypo Energy   " + inputLocation(), m_energyMin, m_energyMax, m_energyBin );
  hBook1( "3", "Hypo Pt       " + inputLocation(), m_etMin, m_etMax, m_etBin );
  if ( inputLocation() == "Rec/Calo/MergedPi0s" || inputLocation() == "Hlt/Calo/MergedPi0s" )
    hBook1( "4", "Hypo Mass     " + inputLocation(), m_massMin, m_massMax, m_massBin );
  hBook1( "5", "Hypo X        " + inputLocation(), m_xMin, m_xMax, m_xBin );
  hBook1( "6", "Hypo Y        " + inputLocation(), m_yMin, m_yMax, m_yBin );
  hBook1( "7", "Clusters/Hypo " + inputLocation(), m_clusMin, m_clusMax, m_clusBin );
  hBook1( "8", "Spd/Hypo      " + inputLocation(), m_spdMin, m_spdMax, m_spdBin );
  hBook1( "9", "Prs/Hypo      " + inputLocation(), m_prsMin, m_prsMax, m_prsBin );
  hBook2( "10", "Hypo barycenter position x vs y   " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin, m_yMax,
          m_yBin );
  hBook2( "11", "Energy-weighted hypo barycenter position x vs y " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin,
          m_yMax, m_yBin );
  hBook1( "14", "#Hypo/#Cluster" + inputLocation(), 0., 1., 100 );
  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloHypoMonitor::operator()( const Input& hypos, const LHCb::CaloClusters& clusters ) const {
  // produce histos ?
  if ( !produceHistos() ) return;

  // check data
  if ( hypos.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Empty hypos found in" << inputLocation() << endmsg;
    return;
  }

  // get functor
  LHCb::CaloDataFunctor::DigitFromCalo spd( DeCalorimeterLocation::Spd );
  LHCb::CaloDataFunctor::DigitFromCalo prs( DeCalorimeterLocation::Prs );

  initCounters();

  // Start looping over hypo
  for ( const auto& hypo : hypos ) {
    LHCb::CaloMomentum momentum( hypo );
    const double       e    = momentum.e();
    const double       et   = momentum.pt();
    const double       mass = momentum.momentum().mass();
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    if ( mass < m_massFilterMin || mass > m_massFilterMax ) continue;
    auto id = LHCb::CaloCellID();
    if ( hypo->clusters().size() > 0 ) {
      const auto cluster = *( hypo->clusters().begin() );
      if ( 0 != cluster ) id = cluster->seed();
    }

    // Start filling histo
    count( id );
    hFill1( id, "2", e );
    hFill1( id, "3", et );
    if ( inputLocation() == "Rec/Calo/MergedPi0s" || inputLocation() == "Hlt/Calo/MergedPi0s" ) hFill1( id, "4", mass );

    const auto position = hypo->position();
    if ( position != nullptr ) {
      hFill1( id, "5", position->x() );
      hFill1( id, "6", position->y() );
      hFill2( id, "10", position->x(), position->y() );
      hFill2( id, "11", position->x(), position->y(), e );
    }

    const auto digits = hypo->digits();
    hFill1( id, "7", hypo->clusters().size() );
    hFill1( id, "8", std::count_if( digits.begin(), digits.end(), spd ) );
    hFill1( id, "9", std::count_if( digits.begin(), digits.end(), prs ) );

    if ( !( id == LHCb::CaloCellID() ) ) {
      if ( doHisto( "12" ) ) fillCalo2D( "12", id, 1., "Hypo position 2Dview " + inputLocation() );
      if ( doHisto( "13" ) ) fillCalo2D( "13", id, e, "Hypo Energy 2Dview " + inputLocation() );
    }
  }
  // fill multiplicity histogram
  fillCounters( "1" );

  // cluster fraction (no area-splittable so far)
  const int    nClus = clusters.size();
  const double frac  = nClus > 0 ? (double)m_count / (double)nClus : 0.;
  hFill1( "14", frac );

  return;
}

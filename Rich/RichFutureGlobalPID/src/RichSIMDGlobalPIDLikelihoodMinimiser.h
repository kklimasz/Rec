/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>
#include <array>
#include <iomanip>
#include <limits>
#include <numeric>
#include <type_traits>
#include <utility>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// VDT
#include "vdt/exp.h"
#include "vdt/log.h"

// boost
#include "boost/format.hpp"
#include "boost/limits.hpp"
#include "boost/numeric/conversion/bounds.hpp"

// Rich Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichUnorderedMap.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec::GlobalPID {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Type for output data
    using OutData = std::tuple<TrackPIDHypos, TrackDLLs::Vector>;
  } // namespace

  /** @class LikelihoodMinimiser RichGlobalPIDRecoSummary.h
   *
   *  Performs the RICH global PID likelihood minimisation.
   *
   *  @author Chris Jones
   *  @date   2016-10-25
   */

  class SIMDLikelihoodMinimiser final : public MultiTransformer<OutData( const Summary::Track::Vector&,             //
                                                                         const Summary::Pixel::Vector&,             //
                                                                         const TrackPIDHypos&,                      //
                                                                         const TrackDLLs::Vector&,                  //
                                                                         const SIMDPixelBackgrounds&,               //
                                                                         const Relations::PhotonToParents::Vector&, //
                                                                         const SIMDPhotonSignals::Vector& ),
                                                                Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    SIMDLikelihoodMinimiser( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    OutData operator()( const Summary::Track::Vector&             gTracks,    ///< tracks
                        const Summary::Pixel::Vector&             gPixels,    ///< pixels
                        const TrackPIDHypos&                      inTkHypos,  ///< track mass hypotheses
                        const TrackDLLs::Vector&                  inTkDLLs,   ///< Starting track DLL values
                        const SIMDPixelBackgrounds&               pixelBkgs,  ///< Pixel backgrounds
                        const Relations::PhotonToParents::Vector& photRel,    ///< photon relations
                        const SIMDPhotonSignals::Vector&          photSignals ///< photon signals
                        ) const override;

  private:
    // definitions

    /// Working scalar type for floating point numbers
    using FloatType = SIMD::DefaultScalarFP;

    /// SIMD floating point type
    using SIMDFP = SIMD::FP<SIMD::DefaultScalarFP>;

    /// Type for local pixel data containers
    using PixelData = SIMD::STDVector<SIMDFP>;

    /// Track list entry. Its current best DLL change and a pointer to the track
    using TrackPair = std::pair<FloatType, const Summary::Track*>;

    /// List of all track list entries
    using TrackList = std::vector<TrackPair>;

    /// Struct to pass around the photon information
    class PhotConts final {
    public:
      PhotConts() = delete;
      PhotConts( const Relations::PhotonToParents::Vector& _photRel, //
                 const SIMDPhotonSignals::Vector&          _photSignals )
          : photRel( _photRel ) //
          , photSignals( _photSignals ) {}

    public:
      const Relations::PhotonToParents::Vector& photRel;
      const SIMDPhotonSignals::Vector&          photSignals;
    };

    /// Struct to pass around pixel containers
    class PixelConts final {
    public:
      PixelConts() = delete;
      PixelConts( const Summary::Pixel::Vector& gPixs, //
                  const SIMDPixelBackgrounds&   pBkgs )
          : gPixels( gPixs )              //
          , pixelBkgs( pBkgs )            //
          , pixSignals( pBkgs.size(), 0 ) //
          , pixCurrlogExp( pBkgs.size(), 0 ) {}

    public:
      const Summary::Pixel::Vector& gPixels;
      const SIMDPixelBackgrounds&   pixelBkgs;     ///< Pixel backgrounds
      PixelData                     pixSignals;    ///< Pixel signals
      PixelData                     pixCurrlogExp; ///< Cached log(exp(x)-1)
    };

    /// Struct to pass around track containers
    class TrackConts final {
    public:
      TrackConts() = delete;
      TrackConts( OutData&                      outD, //
                  const Summary::Track::Vector& gTs )
          : outData( outD ) //
          , gTracks( gTs ) {}

    public:
      TrackPIDHypos&           tkHypos() noexcept { return std::get<0>( outData ); }
      const TrackPIDHypos&     tkHypos() const noexcept { return std::get<0>( outData ); }
      TrackDLLs::Vector&       tkDLLs() noexcept { return std::get<1>( outData ); }
      const TrackDLLs::Vector& tkDLLs() const noexcept { return std::get<1>( outData ); }

    public:
      OutData&                      outData;
      const Summary::Track::Vector& gTracks;
    };

    /// Container for changes to be made following an event iterations
    /// Contains a pointer to a track and the its new hypothesis
    using MinTrList = Rich::UnorderedMap<const Summary::Track*, Rich::ParticleIDType>;

  private: // helpers
    /// Stores information associated to a GlobalPID Track
    class InitTrackInfo final {
    public:
      /// Container
      using Vector = std::vector<InitTrackInfo>;
      /// Constructor
      InitTrackInfo( const Summary::Track*      track, //
                     const Rich::ParticleIDType h,     //
                     const FloatType            mindll )
          : pidTrack( track ), hypo( h ), minDLL( mindll ) {}

    public:
      const Summary::Track* pidTrack{nullptr}; ///< Pointer to the track
      Rich::ParticleIDType  hypo{Rich::Pion};  ///< Track hypothesis
      FloatType             minDLL{0};         ///< The DLL value
    };

  private: // methods
    /// Returns the force change Dll value
    inline FloatType forceChangeDll() const noexcept { return m_forceChangeDll; }

    /// Returns the freeze out Dll value
    inline FloatType freezeOutDll() const noexcept { return m_freezeOutDll; }

    /// Full implementation of log( e^x - 1 )
    template <typename TYPE>
    inline TYPE full_logExp( const TYPE& x ) const noexcept {
      using namespace Rich::Maths;
      return fast_log( fast_exp( x ) - TYPE( 1.0 ) );
    }

    /// Approximate log( e^x - 1 ) for SIMD types
    template <typename TYPE, //
              typename std::enable_if<!std::is_arithmetic<TYPE>::value>::type* = nullptr>
    inline TYPE approx_logExp( const TYPE& x ) const noexcept {
      using namespace Rich::Maths::Approx;
      // Use power series expansion
      // log( e^x - 1 ) ~= log(x) + x/2 + x^2/24
      // works well for x ~ 0.001 to 5
      const TYPE a( 1.0 / 24.0 );
      const TYPE b( 0.5 );
      // return fast_log(x) + ( ( ( a * x ) + b ) * x );
      return vapprox_log( x ) + ( ( ( a * x ) + b ) * x );
    }

    /// Fast implementation of log( e^x - 1 ) for SIMD types
    template <typename TYPE, //
              typename std::enable_if<!std::is_arithmetic<TYPE>::value>::type* = nullptr>
    inline TYPE fast_logExp( const TYPE& x ) const noexcept {
      // Use the fast VDT inspired methods
      // return full_logExp(x);

      // Use the fast approoximation
      return approx_logExp( x );
    }

    /// log( exp(x) - 1 ) or an approximation for small signals
    template <typename TYPE>
    inline TYPE sigFunc( TYPE x ) const noexcept {
      using namespace LHCb::SIMD;
      x( x < m_minSigSIMD ) = m_minSigSIMD;
      return fast_logExp( x );
    }

    /// Calculates logLikelihood for event with a given set of track hypotheses.
    /// Performs full loop over all tracks and hypotheses
    FloatType logLikelihood( const TrackConts& tkC, const PixelConts& pixC ) const;

    /** Starting with all tracks pion, calculate logLikelihood. Then for
     *  each track in turn, holding all others to pion, calculate new
     *  logLikelihood for each particle code. If less than with all pion,
     *  set new minimum track hypothesis.
     *  @return Number of tracks that changed mass hypothesis
     */
    unsigned int initBestLogLikelihood( TrackList&  trackList, //
                                        TrackConts& tkC,       //
                                        PixelConts& pixC,      //
                                        PhotConts&  photC ) const;

    /// Do the event iterations
    unsigned int doIterations( FloatType&  currentBestLL, //
                               TrackList&  trackList,     //
                               TrackConts& tkC,           //
                               PixelConts& pixC,          //
                               PhotConts&  photC ) const;

    // Get the active RICH flags
    inline decltype( auto ) getRICHFlags( MinTrList& minTracks ) const noexcept {
      // RICH flags. Default to flase
      DetectorArray<bool> inR = {{false, false}};
      if ( !minTracks.empty() ) {
        for ( const auto& T : minTracks ) {
          // check if this track is in both RICHes
          for ( const auto rich : Rich::detectors() ) {
            if ( T.first->richActive()[rich] ) { inR[rich] = true; }
          }
          // if both flags now set, stop the loop
          if ( inR[Rich::Rich1] && inR[Rich::Rich2] ) { break; }
        }
      } else {
        inR = {true, true};
      }
      return inR;
    }

    /** Starting with all tracks with best hypotheses as set by
     *  initBestLogLikelihood(), for each track in turn get
     *  logLikelihood for each particle code, and return the track and
     *  particle code which gave the optimal log likelihood.
     *  @return The overall event LL change
     */
    FloatType findBestLogLikelihood( const DetectorArray<bool>& inR,       //
                                     MinTrList&                 minTracks, //
                                     TrackList&                 trackList, //
                                     TrackConts&                tkC,       //
                                     PixelConts&                pixC,      //
                                     PhotConts&                 photC ) const;

    /// Computes the change in the logLikelihood produced by changing given
    /// track to the given hypothesis
    FloatType deltaLogLikelihood( const Summary::Track&      track,   //
                                  const Rich::ParticleIDType curHypo, //
                                  const Rich::ParticleIDType newHypo, //
                                  const PixelConts&          pixC,    //
                                  const PhotConts&           photC ) const noexcept;

    /// Update the best hypothesis for a given track
    void setBestHypo( const Summary::Track&      track,   //
                      TrackPIDHypos&             tkHypos, //
                      const Rich::ParticleIDType newHypo, //
                      PixelConts&                pixC,    //
                      PhotConts&                 photC ) const;

    /// Print the current track list
    void printTrackList( const TrackList&  trackList, //
                         const TrackConts& tkC,       //
                         const MSG::Level  level ) const;

    /// Print the track data
    void print( const TrackConts& tkC, const MSG::Level level ) const;

    /// Print the pixel data
    void print( const PixelConts& pixC, const MSG::Level level ) const;

  private:
    // SIMD caches of properties

    /// SIMD Minimum signal value for full calculation of log(exp(signal)-1)
    SIMDFP m_minSigSIMD = SIMDFP::Zero();

    /// Cached value of log( exp(m_minSig) - 1 ) for efficiency
    SIMDFP m_logMinSig = SIMDFP::Zero();

  private:
    // properties

    /// Minimum signal value for full calculation of log(exp(signal)-1)
    Gaudi::Property<FloatType> m_minSig{this, "MinSignalForNoLLCalc", 1e-3,
                                        "Minimum signal value for full calculation of log(exp(-signal)-1)"};

    /// Threshold for likelihood maximisation
    Gaudi::Property<FloatType> m_epsilon{this, "LikelihoodThreshold", -1e-3, "Threshold for likelihood maximisation"};

    /// Maximum number of track iterations
    Gaudi::Property<unsigned int> m_maxEventIterations{this, "MaxEventIterations", 500u,
                                                       "Maximum number of track iterations"};

    /// Track DLL value to freeze track out from future iterations
    Gaudi::Property<FloatType> m_freezeOutDll{
        this, "TrackFreezeOutDLL", 4,
        "Track freeze out value (The point at which it is no longer considered for change)"};

    /// Track DLL value for a forced change
    Gaudi::Property<FloatType> m_forceChangeDll{this, "TrackForceChangeDLL", -2,
                                                "Track DLL Thresdhold for forced change"};

    /// Flag to turn on final DLL and hypothesis check
    Gaudi::Property<bool> m_doFinalDllCheck{this, "FinalDLLCheck", false,
                                            "Flag to turn on final DLL and hypothesis check"};

    /// Flag to turn on RICH check in LL minimisation
    Gaudi::Property<bool> m_richCheck{this, "RichDetCheck", true,
                                      "Flag to turn on RICH check in LL minimisation (turn off for max precision)"};

    /// Maximum number of tracks to change in a single event iteration
    Gaudi::Property<unsigned int> m_maxTkChanges{this, "MaxTrackChangesPerIt", 5u,
                                                 "Maximum number of tracks to change in a single event iteration"};

    /// Maximum number of iteration retries
    Gaudi::Property<unsigned int> m_maxItRetries{this, "MaxIterationRetries", 10u, "Maximum retries"};

  private:
    // messaging

    /// Maximum iteration retires reached
    mutable WarningCounter m_maxItTriesWarn{this, "Maximum number of iteration re-tries reached"};

    /// Maximum iterations reached
    mutable WarningCounter m_maxItsWarn{this, "Taken more than maximum number of iterations -> quitting"};
  };

  //=============================================================================

  inline SIMDLikelihoodMinimiser::FloatType
  SIMDLikelihoodMinimiser::deltaLogLikelihood( const Summary::Track&      track,   //
                                               const Rich::ParticleIDType curHypo, //
                                               const Rich::ParticleIDType newHypo, //
                                               const PixelConts&          pixC,    //
                                               const PhotConts&           photC    //
                                               ) const noexcept {

    // Note because we no longer support Aerogel, its no longer possible to have
    // the same pixel associated to the same track more than once.
    // This allows better optimisation of this method with 1 loop not 2 ;)

    // Change due to track expectation
    FloatType deltaLL = ( track.totalSignals()[newHypo] - track.totalSignals()[curHypo] );

    // loop over the photons for this track
    for ( const auto& iPhot : track.photonIndices() ) {

      // index for the pixel associated to this photon
      const auto iPix = photC.photRel[iPhot].pixelIndex();

      // signals for this photon
      const auto& sigs = photC.photSignals[iPhot];

      // test signal for this pixel
      const auto pixTestSig = ( pixC.pixSignals[iPix] + sigs[newHypo] - sigs[curHypo] );

      // update the DLL
      deltaLL += ( pixC.pixCurrlogExp[iPix] - sigFunc( pixC.pixelBkgs[iPix] + pixTestSig ) ).sum();
    }

    // return the DLL
    return deltaLL;
  }

  //=============================================================================

} // namespace Rich::Future::Rec::GlobalPID

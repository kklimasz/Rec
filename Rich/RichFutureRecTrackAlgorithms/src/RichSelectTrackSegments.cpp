/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichSelectTrackSegments.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

SelectTrackSegments::SelectTrackSegments( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"InTrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial},
                    KeyValue{"GeomEffsLocation", GeomEffsLocation::Default}},
                   {KeyValue{"OutTrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected}} ) {
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

OutData SelectTrackSegments::operator()( const Relations::TrackToSegments::Vector& tkToSegRels,
                                         const GeomEffs::Vector&                   geomEffs ) const {
  OutData outRels;
  outRels.reserve( tkToSegRels.size() );

  // count the number selected
  // unsigned int nSelSegs(0);

  // loop over the input track -> segment relations
  for ( const auto& inTkRel : tkToSegRels ) {
    // Make an entry in the selected relations for this track
    outRels.emplace_back( inTkRel.tkKey, inTkRel.tkIndex );
    auto& outRel = outRels.back();

    // loop over the segments for this track
    for ( const auto& iSeg : inTkRel.segmentIndices ) {
      // perform segment selection using info now available, but was
      // not when the segment was first formed.

      // Check lightest geom eff. > 0
      if ( ( geomEffs[iSeg] )[lightestActiveHypo()] > 0 ) {
        // save in output relations
        outRel.segmentIndices.push_back( iSeg );
        //++nSelSegs;
      }
    }
  }

  //_ri_debug << "Selected " << nSelSegs << " of " << geomEffs.size()
  //          << " Track Segments" << endmsg;

  return outRels;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SelectTrackSegments )

//=============================================================================

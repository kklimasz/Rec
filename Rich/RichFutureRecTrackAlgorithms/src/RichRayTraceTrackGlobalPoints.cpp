/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichRayTraceTrackGlobalPoints.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

RayTraceTrackGlobalPoints::RayTraceTrackGlobalPoints( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   // inputs
                   {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default}},
                   // outputs
                   {KeyValue{"TrackGlobalPointsLocation", SpacePointLocation::SegmentsGlobal}} ) {
  // init
  m_traceMode.fill( LHCb::RichTraceMode( LHCb::RichTraceMode::DetectorPlaneBoundary::IgnorePDAcceptance ) );
  // debugging
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode RayTraceTrackGlobalPoints::initialize() {

  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // track ray tracing
  _ri_debug << "Rich1Gas Track " << m_traceMode[Rich::Rich1Gas] << endmsg;
  _ri_debug << "Rich2Gas Track " << m_traceMode[Rich::Rich2Gas] << endmsg;
  for ( const auto rad : Rich::radiators() ) {
    m_fTraceMode[rad] = m_traceMode[rad];
    m_fTraceMode[rad].setForcedSide( true );
    m_fTraceMode[rad].setDetPlaneBound( LHCb::RichTraceMode::DetectorPlaneBoundary::IgnorePDAcceptance );
  }

  // return
  return sc;
}

//=============================================================================

SegmentPanelSpacePoints::Vector //
RayTraceTrackGlobalPoints::operator()( const LHCb::RichTrackSegment::Vector& segments ) const {

  // the global points to return
  SegmentPanelSpacePoints::Vector points;
  points.reserve( segments.size() );

  // local photon object to work with
  GeomPhoton photon;

  // Loop over segments
  for ( const auto& trSeg : segments ) {

    // rich and radiator
    const auto rich = trSeg.rich();
    const auto rad  = trSeg.radiator();

    // best start point and direction
    const auto& trackDir = trSeg.bestMomentum();
    const auto& trackPtn = trSeg.bestPoint();

    // ray trace the best point
    auto result = m_rayTrace.get()->traceToDetector( rich, trackPtn, trackDir,        //
                                                     photon, trSeg, m_traceMode[rad], //
                                                     Rich::top );                     // side not used ...
    if ( m_traceMode[rad].traceWasOK( result ) ) {

      // save point and side for the best tracing
      const auto bestPoint = photon.detectionPoint();
      const auto bestSide  = photon.smartID().panel();

      // closest PD
      const auto closestPD = photon.smartID().pdID();

      // Now do the forced left/right up/down tracings

      // left/top
      result =
          m_rayTrace.get()->traceToDetector( rich, trackPtn, trackDir, photon, trSeg, m_fTraceMode[rad], Rich::left );
      const auto lPos = ( m_fTraceMode[rad].traceWasOK( result ) ? photon.detectionPoint() : SpacePoint{0, 0, 0} );

      // right/bottom
      result =
          m_rayTrace.get()->traceToDetector( rich, trackPtn, trackDir, photon, trSeg, m_fTraceMode[rad], Rich::right );
      const auto rPos = ( m_fTraceMode[rad].traceWasOK( result ) ? photon.detectionPoint() : SpacePoint{0, 0, 0} );

      // save the data
      points.emplace_back( bestPoint, lPos, rPos, bestSide, closestPD );
    } else {
      // Save a default object for this segment
      points.emplace_back();
    }

    _ri_verbo << "Segment PD panel point (global) " << points.back() << endmsg;

  } // segment loop

  return points;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RayTraceTrackGlobalPoints )

//=============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichTrackCherenkovAngles.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

//=============================================================================

CherenkovAngles::Vector TrackCherenkovAnglesBase::run( const LHCb::RichTrackSegment::Vector& segments,  //
                                                       const PhotonSpectra::Vector&          tkSpectra, //
                                                       const PhotonYields::Vector&           tkYields ) const {

  // make the data to return
  CherenkovAngles::Vector anglesV;
  anglesV.reserve( segments.size() );

  // iterate over segments and spectra together.
  for ( auto&& [segment, spectra, yields] : Ranges::ConstZip( segments, tkSpectra, tkYields ) ) {
    // make a new object for the cherenkov angles
    anglesV.emplace_back();
    auto& angles = anglesV.back();

    //_ri_verbo << "Tk" << endmsg;

    // Loop over (real) PID types (Below Threshold excluded).
    for ( const auto id : activeParticlesNoBT() ) {

      // the angle
      CherenkovAngles::Type angle = 0;

      // Is this hypo above threshold ?
      if ( yields[id] > 0 ) {

        //_ri_verbo << std::setprecision(9) << id << " yield = " << yields[id] << endmsg;

        // compute track beta
        const auto beta = richPartProps()->beta( segment.bestMomentumMag(), id );
        if ( beta > 0 ) {

          // loop over energy bins
          for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {
            const auto refIn =
                m_refIndex.get()->refractiveIndex( segment.radIntersections(), spectra.binEnergy( iEnBin ) );
            const auto temp = beta * refIn;
            //_ri_verbo << std::setprecision(9)
            //          << " bin " << iEnBin << " " << beta << " " << refIn << endmsg;
            if ( temp > 1 ) {
              const auto f  = Rich::Maths::fast_acos( 1.0 / temp );
              const auto en = ( spectra.energyDist( id ) )[iEnBin];
              //_ri_verbo << std::setprecision(9) << "     " << " " << f << " " << en << endmsg;
              angle += ( en * f );
            }
          }
          // normalise the angle
          angle /= yields[id];
        }
      }

      // save the final angle for this hypo
      //_ri_verbo << std::setprecision(9) << id << " CK theta " << angle << endmsg;
      angles.setData( id, angle );
    }
  }

  // return the new data
  return anglesV;
}

//=============================================================================

// Declaration of the Algorithm Factories
DECLARE_COMPONENT( TrackEmittedCherenkovAngles )
DECLARE_COMPONENT( TrackSignalCherenkovAngles )

//=============================================================================

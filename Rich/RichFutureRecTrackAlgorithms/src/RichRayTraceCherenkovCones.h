/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// STL
#include <sstream>
#include <utility>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// Interfaces
#include "RichFutureInterfaces/IRichRayTracing.h"
#include "RichFutureInterfaces/IRichSmartIDTool.h"

// VDT
#include "vdt/sincos.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RayTraceCherenkovCones RichRayTraceCherenkovCones.h
   *
   *  Creates the Cherenkov cones for each segment and mass hypothesis,
   *  using photon raytracing.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceCherenkovCones final : public Transformer<MassHypoRingsVector( const LHCb::RichTrackSegment::Vector&, //
                                                                               const CherenkovAngles::Vector& ),
                                                          Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    RayTraceCherenkovCones( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Functional operator
    MassHypoRingsVector operator()( const LHCb::RichTrackSegment::Vector& segments, //
                                    const CherenkovAngles::Vector&        ckAngles ) const override;

  private: // helper classes
    /** @class CosSinPhi RichRayTraceCherenkovCone.h
     *
     *  Utility class to cache cos and sin values
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   17/02/2008
     */
    template <typename TYPE>
    class CosSinPhi : public Vc::AlignedBase<Vc::VectorAlignment> {
    public:
      /// Container type
      typedef Rich::SIMD::STDVector<CosSinPhi> Vector;

    public:
      /// Contructor from a phi value
      explicit CosSinPhi( const TYPE _phi ) : phi( _phi ) { Rich::Maths::fast_sincos( phi, sinPhi, cosPhi ); }

    public:
      TYPE phi    = 0; ///< CK phi
      TYPE cosPhi = 0; ///< Cos(CK phi)
      TYPE sinPhi = 0; ///< Sin(CK phi)
    };

  private:
    // SIMD types
    using FP         = Rich::SIMD::DefaultScalarFP;
    using SIMDFP     = SIMD::FP<FP>;
    using SIMDVector = SIMD::Vector<FP>;

  private:
    /// Bailout number
    RadiatorArray<unsigned int> m_nBailout = {{}};

    /// Cached SIMD cos and sin values around the ring, for each radiator
    RadiatorArray<CosSinPhi<SIMDFP>::Vector> m_cosSinPhiV;

    /// Cached trace modes for each radiator
    RadiatorArray<LHCb::RichTraceMode> m_traceModeRad = {{}};

  private:
    /// Number of points to ray trace on each ring, for each radiator
    Gaudi::Property<RadiatorArray<unsigned int>> m_nPoints{this, "NRingPoints", {96u, 96u, 96u}};

    /// Flag to turn on or off checking of intersections with beampipe
    Gaudi::Property<bool> m_checkBeamPipe{this, "CheckBeamPipe", false};

    /// Flag to switch between simple or detail HPD description in ray tracing
    Gaudi::Property<bool> m_useDetailedHPDsForRayT{this, "UseDetailedHPDs", false};

    /** Bailout fraction. If no ray tracings have worked after this
     *  fraction have been perfromed, then give up */
    Gaudi::Property<RadiatorArray<float>> m_bailoutFrac{this, "BailoutFraction", {0.75f, 0.75f, 0.75f}};

    /// Ray tracing tool
    ToolHandle<const IRayTracing> m_rayTrace{this, "RayTracingTool", "Rich::Future::RayTracing/RayTracing"};

    /// RichSmartID Tool
    ToolHandle<const ISmartIDTool> m_idTool{this, "IDTool", "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC"};
  };

} // namespace Rich::Future::Rec

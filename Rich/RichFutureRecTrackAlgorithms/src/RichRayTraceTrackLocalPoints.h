/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// Interfaces
#include "RichFutureInterfaces/IRichSmartIDTool.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class RayTraceTrackLocalPoints RichRayTraceTrackLocalPoints.h
   *
   *  Converts the ray traced track global PD positions to local coordinates.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceTrackLocalPoints final
      : public Transformer<SegmentPanelSpacePoints::Vector( const SegmentPanelSpacePoints::Vector& ),
                           Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    RayTraceTrackLocalPoints( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    SegmentPanelSpacePoints::Vector operator()( const SegmentPanelSpacePoints::Vector& gPoints ) const override;

  private:
    /// RichSmartID Tool
    ToolHandle<const ISmartIDTool> m_idTool{this, "SmartIDTool", "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC"};
  };

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <tuple>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"

// Interfaces
#include "RichInterfaces/IRichDetParameters.h"

// Detector Description
#include "RichDet/DeRich1.h"
#include "RichDet/DeRichAerogelRadiator.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;
  } // namespace

  /** @class EmittedPhotonYields RichEmittedPhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class EmittedPhotonYields final
      : public MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector& ), Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    EmittedPhotonYields( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments ) const override;

  private:
    /// Update the cached parameters
    StatusCode umsUpdate();

  private:
    /// Min and Max photon energies for each radiator
    RadiatorArray<std::pair<double, double>> m_photEnergies = {{}};

    /// Cache the 'paraW' calculation for each energy bin and radiator
    RadiatorArray<std::array<double, NPhotonSpectraBins>> m_paraWDiff = {{}};

    /// particle hypothesis masses squared
    ParticleArray<double> m_particleMassSq = {{}};

    /// Rich1 Detector element
    DeRich1* m_Rich1DE = nullptr;

    /// Detector parameters tool
    ToolHandle<const IDetParameters> m_detParams{"Rich::Future::DetParameters/DetParams:PUBLIC", this};
  };

} // namespace Rich::Future::Rec

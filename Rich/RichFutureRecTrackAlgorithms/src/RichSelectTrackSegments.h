/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

// Rec event model
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecRelations.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// The output data
    using OutData = Relations::TrackToSegments::Vector;
  } // namespace

  /** @class SelectTrackSegments RichSelectTrackSegments.h
   *
   *  Filters the initial track to segment relations to select only
   *  segments with RICH information.
   *
   *  @author Chris Jones
   *  @date   2016-10-31
   */

  class SelectTrackSegments final : public Transformer<OutData( const Relations::TrackToSegments::Vector&, //
                                                                const GeomEffs::Vector& ),
                                                       Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    SelectTrackSegments( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Algorithm execution via transform
    OutData operator()( const Relations::TrackToSegments::Vector& tkToSegRels, //
                        const GeomEffs::Vector&                   geomEffs ) const override;
  };

} // namespace Rich::Future::Rec

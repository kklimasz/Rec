
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Base class
#include "RichCommonQuarticPhotonReco.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Utils
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichUtils/RichRayTracingUtils.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"

namespace Rich::Future::Rec {

  namespace {
    /// Shortcut to the output data type
    using OutData = std::tuple<SIMDCherenkovPhoton::Vector, Relations::PhotonToParents::Vector>;
  } // namespace

  /** @class SIMDQuarticPhotonReco RichSIMDQuarticPhotonReco.h
   *
   *  Reconstructs SIMD photon candidates using the Quartic solution.
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDQuarticPhotonReco final : public MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&,  //
                                                                       const CherenkovAngles::Vector&,         //
                                                                       const CherenkovResolutions::Vector&,    //
                                                                       const SegmentPanelSpacePoints::Vector&, //
                                                                       const SegmentPhotonFlags::Vector&,      //
                                                                       const SIMDPixelSummaries&,              //
                                                                       const Relations::TrackToSegments::Vector& ),
                                                              Traits::BaseClass_t<CommonQuarticPhotonReco>> {

  public:
    // framework

    /// Standard constructor
    SIMDQuarticPhotonReco( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    // methods

    /// Functional operator
    OutData operator()( const LHCb::RichTrackSegment::Vector&     segments,      ///< segments
                        const CherenkovAngles::Vector&            ckAngles,      ///< CK theta angles
                        const CherenkovResolutions::Vector&       ckResolutions, ///< CK theta resolutions
                        const SegmentPanelSpacePoints::Vector&    trHitPntsLoc,  ///< segment hit points
                        const SegmentPhotonFlags::Vector&         segPhotFlags,  ///< segment flags
                        const SIMDPixelSummaries&                 pixels,        ///< RICH SIMD hits
                        const Relations::TrackToSegments::Vector& tkToSegRels    ///< track relations
                        ) const override;

  public:
    // definitions

    using FP      = BasePhotonReco::FP;      ///< Basic precision (float)
    using SIMDFP  = BasePhotonReco::SIMDFP;  ///< SIMD version of FP
    using Mirrors = BasePhotonReco::Mirrors; ///< Type for container of mirror pointers. Same size as SIMDFP.

  private:
    // methods

    /// Compute the best emission point for the gas radiators using
    /// the given spherical mirror reflection points
    SIMDFP::mask_type getBestGasEmissionPoint( const Rich::RadiatorType radiator,       ///< RICH radiator
                                               const SIMD::Point<FP>&   sphReflPoint1,  ///< first spherical refl point
                                               const SIMD::Point<FP>&   sphReflPoint2,  ///< second spherical refl point
                                               const SIMD::Point<FP>&   detectionPoint, ///< detection point
                                               const LHCb::RichTrackSegment& segment,   ///< track segment
                                               SIMD::Point<FP>&              emissionPoint, ///< assumed emission point
                                               SIMDFP&                       fraction       ///< active track fraction
                                               ) const;

    /// Find mirror segments and reflection points for given data
    void findMirrorData( const Rich::DetectorType rich,          ///< The RICH detector
                         const Rich::Side         side,          ///< The RICH panel
                         const SIMD::Point<FP>&   virtDetPoint,  ///< virtual detection point (flat mirror removed)
                         const SIMD::Point<FP>&   emissionPoint, ///< assumed emission point
                         Mirrors&                 primMirr,      ///< primary mirrors
                         Mirrors&                 secMirr,       ///< secondary mirrors
                         SIMD::Point<FP>&         sphReflPoint,  ///< primary mirror reflection point
                         SIMD::Point<FP>&         secReflPoint   ///< secondary mirror reflection point
                         ) const;

  private:
    // SIMD cache of various quantities

    /// SIMD Minimum active segment fraction in each radiator
    RadiatorArray<SIMDFP> m_minActiveFracSIMD = {{}};

    /// SIMD Minimum tolerance for mirror reflection point during iterations
    RadiatorArray<SIMDFP> m_minSphMirrTolItSIMD = {{}};
  };

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <iomanip>
#include <limits>

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Rich Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/ZipRange.h"

// Detector Description
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  // Shortcut to output data
  namespace {
    using OutData = SIMDPhotonSignals::Vector;
  }

  /** @class SIMDPhotonPredictedPixelSignal RichSIMDPhotonPredictedPixelSignal.h
   *
   *  Computes the expected pixel signals for photon candidates.
   *
   *  See note LHCB/98-040 page 11 equation 18
   *
   *  @author Chris Jones
   *  @date   2016-10-19
   */
  class SIMDPhotonPredictedPixelSignal final : public Transformer<OutData( const SIMDPixelSummaries&,                 //
                                                                           const SIMDCherenkovPhoton::Vector&,        //
                                                                           const Relations::PhotonToParents::Vector&, //
                                                                           const LHCb::RichTrackSegment::Vector&,     //
                                                                           const CherenkovAngles::Vector&,            //
                                                                           const CherenkovResolutions::Vector&,       //
                                                                           const PhotonYields::Vector& ),
                                                                  Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonPredictedPixelSignal( const std::string& name, ISvcLocator* pSvcLocator );

    /// Initialization of the tool after creation
    StatusCode initialize() override;

  public:
    /// Functional operator
    OutData operator()( const SIMDPixelSummaries&                 pixels,    ///< pixels
                        const SIMDCherenkovPhoton::Vector&        photons,   ///< photons
                        const Relations::PhotonToParents::Vector& photRels,  ///< photon relations
                        const LHCb::RichTrackSegment::Vector&     segments,  ///< segments
                        const CherenkovAngles::Vector&            ckAngles,  ///< CK theta angles
                        const CherenkovResolutions::Vector&       ckRes,     ///< CK theta track resolutions
                        const PhotonYields::Vector&               photYields ///< photon yields
                        ) const override;

  private:
    /// Scalar type
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD type
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

  private:
    /// The exponential function
    inline SIMDFP myexp( const SIMDFP& x ) const noexcept {
      // Fast VDT like function
      return Rich::Maths::fast_exp( x );
      // Approximation
      // return Rich::Maths::Approx::approx_exp(x);
      // Even faster approximation
      // return Rich::Maths::Approx::vapprox_exp(x);
    }

  private:
    /// Cache SIMD minimum argument value for the probability value
    SIMDFP m_minArgSIMD = SIMDFP::Zero();

    /// Cache exp(min arg)
    SIMDFP m_expMinArg = SIMDFP::Zero();

    /// Cache SIMD minimum cut value for photon probability
    RadiatorArray<SIMDFP> m_minPhotonProbSIMD = {{}};

    /// cached SIMD scale factor / RoC^2
    DetectorArray<SIMDFP> m_factor = {{}};

    /// cached min exp(arg) factor
    RadiatorArray<SIMDFP> m_minExpArgF = {{}};

  private:
    /// The minimum expected track Cherenkov angle to be considered 'Above Threshold'
    Gaudi::Property<RadiatorArray<float>> m_minExpCKT{
        this,
        "MinExpTrackCKTheta",
        {0.0f, 0.0f, 0.0f}, // was { 1e-6f, 1e-6f, 1e-6f }
        "The minimum expected track Cherenkov angle for each radiator (Aero/R1Gas/R2Gas)"};

    /// The minimum cut value for photon probability
    Gaudi::Property<RadiatorArray<float>> m_minPhotonProb{
        this,
        "MinPhotonProbability",
        {1e-15f, 1e-15f, 1e-15f},
        "The minimum allowed photon probability values for each radiator (Aero/R1Gas/R2Gas)"};

    /// Scale factors
    Gaudi::Property<DetectorArray<float>> m_scaleFactor{this, "ScaleFactor", {4.0f, 4.0f}};

    /// The minimum argument value for the probability value
    Gaudi::Property<float> m_minArg{this, "MinExpArg", -80.0f};
  };

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichBasePhotonReco.h"

// boost
#include "boost/format.hpp"

using namespace Rich::Future::Rec;

//=============================================================================

BasePhotonReco::BasePhotonReco( const std::string& name, ISvcLocator* pSvcLocator ) : AlgBase( name, pSvcLocator ) {
  // initialise - pre sel
  m_minROI2PreSel.fill( 0 );
  m_maxROI2PreSel.fill( 0 );
  m_scalePreSel.fill( 0 );
  // initialise - sel
  m_ckBiasCorrs.fill( 0 );
}

//=============================================================================

StatusCode BasePhotonReco::initialize() {
  // Sets up various tools and services
  auto sc = AlgBase::initialize();
  if ( !sc ) return sc;

  // loop over radiators
  for ( const auto rad : Rich::radiators() ) {

    // cache some numbers
    m_minROI2PreSel[rad] = std::pow( m_minROIPreSel[rad], 2 );
    m_maxROI2PreSel[rad] = std::pow( m_maxROIPreSel[rad], 2 );
    m_scalePreSel[rad]   = ( m_ckThetaScale[rad] / m_sepGScale[rad] );
    if ( msgLevel( MSG::DEBUG ) ) {
      // printout for this rad
      auto trad = Rich::text( rad );
      trad.resize( 8, ' ' );
      debug() << trad << " : Pre-Sel. Sep. range     " << boost::format( "%5.1f" ) % m_minROIPreSel[rad] << " -> "
              << boost::format( "%5.1f" ) % m_maxROIPreSel[rad] << " mm  : Tol. "
              << boost::format( "%5.1f" ) % m_nSigmaPreSel[rad] << " # sigma" << endmsg;
    }

    // Local SIMD copies of various properties
    m_minROI2PreSelSIMD[rad] = SIMDFP( m_minROI2PreSel[rad] );
    m_maxROI2PreSelSIMD[rad] = SIMDFP( m_maxROI2PreSel[rad] );
    m_scalePreSelSIMD[rad]   = SIMDFP( m_scalePreSel[rad] );
    m_nSigmaPreSelSIMD[rad]  = SIMDFP( m_nSigmaPreSel[rad] );
    m_minCKthetaSIMD[rad]    = SIMDFP( absMinCKTheta( rad ) );
    m_maxCKthetaSIMD[rad]    = SIMDFP( absMaxCKTheta( rad ) );
    m_nSigmaSIMD[rad]        = SIMDFP( m_nSigma[rad] );
    m_ckThetaCorrSIMD[rad]   = SIMDFP( ckThetaCorrection( rad ) );
  }

  _ri_debug << "Bias Corrs : " << m_ckBiasCorrs << endmsg;

  _ri_debug << "JO   Corrs : " << m_ckJOCorrs.value() << endmsg;

  // return
  return sc;
}

//=============================================================================

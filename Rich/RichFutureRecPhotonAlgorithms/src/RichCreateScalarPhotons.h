/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /// Shortcut to the output data type
  namespace {
    using OutData = CherenkovPhoton::Vector;
  }

  /** @class CreateScalarPhotons RichCreateScalarPhotons.h
   *
   *  Creates Scalar representations of SIMD photons.
   *
   *  @author Chris Jones
   *  @date   2017-11-09
   */
  class CreateScalarPhotons final
      : public Transformer<OutData( const SIMDCherenkovPhoton::Vector& ), Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    CreateScalarPhotons( const std::string& name, ISvcLocator* pSvcLocator );

  private:
    /// SIMD floating point type
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

  public:
    /// Functional operator
    OutData operator()( const SIMDCherenkovPhoton::Vector& simdPhotons ) const override;
  };

} // namespace Rich::Future::Rec

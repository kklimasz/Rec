/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <ostream>
#include <stdint.h>

// Gaudi
#include "GaudiKernel/Point3DTypes.h"

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDet/DeRichPD.h"
#include "RichDet/DeRichSphMirror.h"

namespace Rich::Future {

  /** @class RayTracedCKRingPoint RayTracedCKRingPoint.h
   *
   * Ultility class to represent a point on a ray traced Cherenkov Ring
   *
   * @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   * created Thu Oct  6 16:44:59 2016
   *
   */

  class RayTracedCKRingPoint final {

  public:
    /// typedef for std::vector of RayTracedCKRingPoint
    typedef LHCb::STL::Vector<RayTracedCKRingPoint> Vector;

    /// Describes the how this position lies in the RICH acceptance
    enum Acceptance : int8_t {
      UndefinedAcceptance = 0, // The acceptance is undefined
      OutsideHPDPanel     = 1, // Point is outside the general HPD panel acceptance
      InHPDPanel          = 2, // Point is inside the acceptance of the HPD panels
      InHPDTube           = 3  // Point is inside the acceptance of singe HPD tube
    };

  public:
    /// Constructor from global position, location position, acceptance flag and RichSmartID
    RayTracedCKRingPoint( const Gaudi::XYZPoint& gPos, const Gaudi::XYZPoint& lPos,
                          const LHCb::RichSmartID id = LHCb::RichSmartID(), const Acceptance acc = UndefinedAcceptance,
                          const DeRichSphMirror* primMirr = nullptr, const DeRichSphMirror* secMirr = nullptr,
                          const DeRichPD* pd = nullptr, const float azimuth = -1 )
        : m_globalPosition( gPos )
        , m_localPosition( lPos )
        , m_azimuth( azimuth )
        , m_smartID( id )
        , m_acceptance( acc )
        , m_primaryMirror( primMirr )
        , m_secondaryMirror( secMirr )
        , m_pd( pd ) {}

    /// Default Constructor
    RayTracedCKRingPoint() = default;

  public:
    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const;

  public:
    /// Retrieve const  Position on the ring in global coordinates
    const Gaudi::XYZPoint& globalPosition() const noexcept { return m_globalPosition; }

    /// Retrieve const  Position on the ring in local coordinates
    const Gaudi::XYZPoint& localPosition() const noexcept { return m_localPosition; }

    /// Retrieve const  Azimuthal phi w.r.t the ring centre for this position
    float azimuth() const noexcept { return m_azimuth; }

    /// Retrieve const  RichSmartID identifying the HPD or pixel this point is in, if known
    const LHCb::RichSmartID& smartID() const noexcept { return m_smartID; }

    /// Retrieve const  The RICH acceptance state of the point
    const Acceptance& acceptance() const noexcept { return m_acceptance; }

    /// Retrieve const  Pointer to the associated DeRichSphMirror object for the primary mirror
    const DeRichSphMirror* primaryMirror() const noexcept { return m_primaryMirror; }

    /// Retrieve const  Pointer to the associated DeRichSphMirror object for the secondary mirror
    const DeRichSphMirror* secondaryMirror() const noexcept { return m_secondaryMirror; }

    /// Retrieve const  Pointer to the associated DeRichPD object
    const DeRichPD* photonDetector() const noexcept { return m_pd; }

  public:
    /// Update Position on the ring in global coordinates
    void setGlobalPosition( const Gaudi::XYZPoint& value ) noexcept { m_globalPosition = value; }

    /// Update Position on the ring in local coordinates
    void setLocalPosition( const Gaudi::XYZPoint& value ) noexcept { m_localPosition = value; }

    /// Update Azimuthal phi w.r.t the ring centre for this position
    void setAzimuth( const float value ) noexcept { m_azimuth = value; }

    /// Update RichSmartID identifying the HPD or pixel this point is in, if known
    void setSmartID( const LHCb::RichSmartID value ) noexcept { m_smartID = value; }

    /// Update The RICH acceptance state of the point
    void setAcceptance( const Acceptance value ) noexcept { m_acceptance = value; }

    /// Update Pointer to the associated DeRichSphMirror object for the primary mirror
    void setPrimaryMirror( const DeRichSphMirror* value ) noexcept { m_primaryMirror = value; }

    /// Update Pointer to the associated DeRichSphMirror object for the secondary mirror
    void setSecondaryMirror( const DeRichSphMirror* value ) noexcept { m_secondaryMirror = value; }

    /// Update Pointer to the associated DeRichPD object
    void setPhotonDetector( const DeRichPD* value ) noexcept { m_pd = value; }

  private:
    Gaudi::XYZPoint        m_globalPosition{0, 0, 0}; ///< Position on the ring in global coordinates
    Gaudi::XYZPoint        m_localPosition{0, 0, 0};  ///< Position on the ring in local coordinates
    float                  m_azimuth{-1};             ///< Azimuthal phi w.r.t the ring centre for this position
    LHCb::RichSmartID      m_smartID; ///< RichSmartID identifying the HPD or pixel this point is in, if known
    Acceptance             m_acceptance{UndefinedAcceptance}; ///< The RICH acceptance state of the point
    const DeRichSphMirror* m_primaryMirror{nullptr};   ///< Pointer to the associated DeRichSphMirror object for the
                                                       ///< primary mirror
    const DeRichSphMirror* m_secondaryMirror{nullptr}; ///< Pointer to the associated DeRichSphMirror object for the
                                                       ///< secondary mirror
    const DeRichPD* m_pd{nullptr};                     ///< Pointer to the associated DeRichPD object
  };

  inline std::ostream& RayTracedCKRingPoint::fillStream( std::ostream& s ) const {
    s << "{ "
      << "globalPosition : " << m_globalPosition << std::endl
      << "localPosition : " << m_localPosition << std::endl
      << "azimuth : " << m_azimuth << std::endl
      << "smartID : " << m_smartID << std::endl
      << "acceptance : " << m_acceptance << std::endl
      << "primaryMirror : " << m_primaryMirror << std::endl
      << "secondaryMirror : " << m_secondaryMirror << std::endl
      << "photonDetector :    " << m_pd << std::endl
      << " }";
    return s;
  }

} // namespace Rich::Future

inline std::ostream& operator<<( std::ostream& str, const Rich::Future::RayTracedCKRingPoint& obj ) {
  return obj.fillStream( str );
}

inline std::ostream& operator<<( std::ostream& s, Rich::Future::RayTracedCKRingPoint::Acceptance e ) {
  switch ( e ) {
  case Rich::Future::RayTracedCKRingPoint::UndefinedAcceptance:
    return s << "UndefinedAcceptance";
  case Rich::Future::RayTracedCKRingPoint::OutsideHPDPanel:
    return s << "OutsideHPDPanel";
  case Rich::Future::RayTracedCKRingPoint::InHPDPanel:
    return s << "InHPDPanel";
  case Rich::Future::RayTracedCKRingPoint::InHPDTube:
    return s << "InHPDTube";
  default:
    return s << "ERROR wrong value " << int( e ) << " for enum RayTracedCKRingPoint::Acceptance";
  }
}

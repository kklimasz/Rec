/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <vector>

// Utils
#include "RichUtils/RichSIMDTypes.h"

namespace Rich::Future::Rec {

  /// Type for pixel background values (Scalar)
  using PixelBackgrounds = std::vector<double>;

  /// TES locations
  namespace PixelBackgroundsLocation {
    /// Default TES location for pixel backgrounds
    inline const std::string Default = "Rec/RichFuture/PixelBackgrounds/Default";
  } // namespace PixelBackgroundsLocation

  /// Type for pixel background values (SIMD)
  using SIMDPixelBackgrounds = SIMD::STDVector<Rich::SIMD::FP<Rich::SIMD::DefaultScalarFP>>;

  /// TES locations
  namespace SIMDPixelBackgroundsLocation {
    /// Default TES location for pixel backgrounds
    inline const std::string Default = "Rec/RichFuture/SIMDPixelBackgrounds/Default";
  } // namespace SIMDPixelBackgroundsLocation

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cstddef>
#include <ostream>
#include <string>
#include <utility>

// Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSIMDTypes.h"

// Gaudi
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Range.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Vc
#include <Vc/common/alignedbase.h>

namespace Rich::Future::Rec {

  /** @class SIMDPixel RichFutureRecEvent/RichRecSIMDPixels.h
   *
   *  Representation of a set of RICH pixels in a SIMD format
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDPixel final : public Vc::AlignedBase<Vc::VectorAlignment> {

  public:
    // types

    /// The scalar floating point precision
    using FP = Rich::SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = Rich::SIMD::FP<FP>;
    /// SIMD Point
    using Point = Rich::SIMD::Point<FP>;
    /// Type for SmartIDs.
    using SmartIDs = Rich::SIMD::STDArray<LHCb::RichSmartID, SIMDFP>;
    /// Type for index to original scalar cluster
    using ScIndex = Rich::SIMD::Int32;
    /// Selection mask
    using Mask = SIMDFP::mask_type;

  public:
    // constructors

    /// Constructor from RICH and panel ID
    SIMDPixel( const Rich::DetectorType rich, //
               const Rich::Side         side )
        : m_rich( rich ) //
        , m_side( side ) {}

    /// Constructor from full data
    SIMDPixel( const Rich::DetectorType   rich,     //
               const Rich::Side           side,     //
               const SIMDPixel::SmartIDs& smartIDs, //
               const Point&               gPos,     //
               const Point&               lPos,     //
               const SIMDFP&              effArea,  //
               const ScIndex&             scClusIn, //
               const Mask&                mask )
        : m_rich( rich )
        , m_side( side )
        , m_gloPos( gPos )
        , m_locPos( lPos )
        , m_effArea( effArea )
        , m_smartID( smartIDs )
        , m_scClusIn( scClusIn )
        , m_validMask( mask ) {}

  public:
    // read access

    /// Access the RICH
    inline Rich::DetectorType rich() const noexcept { return m_rich; }

    /// Access the panel
    inline Rich::Side side() const noexcept { return m_side; }

    /// Access the global position
    inline const Point& gloPos() const noexcept { return m_gloPos; }

    /// Access the local position
    inline const Point& locPos() const noexcept { return m_locPos; }

    /// Access the Rich channel IDs
    inline const SmartIDs& smartID() const noexcept { return m_smartID; }

    /// Access the effective cluster area
    inline const SIMDFP& effArea() const noexcept { return m_effArea; }

    /// Access the scalar cluster indices
    inline const ScIndex& scClusIndex() const noexcept { return m_scClusIn; }

    /// Access the validity mask
    inline const Mask& validMask() const noexcept { return m_validMask; }

  public:
    // write access

    /// Access the global position
    inline Point& gloPos() noexcept { return m_gloPos; }

    /// Access the local position
    inline Point& locPos() noexcept { return m_locPos; }

    /// Access the Rich channel IDs
    inline SmartIDs& smartID() noexcept { return m_smartID; }

    /// Access the effective cluster area
    inline SIMDFP& effArea() noexcept { return m_effArea; }

    /// Access the scalar cluster indices
    inline ScIndex& scClusIndex() noexcept { return m_scClusIn; }

    /// Access the validity mask
    inline Mask& validMask() noexcept { return m_validMask; }

  public:
    // messaging

    /// Implement ostream << method
    friend inline std::ostream& operator<<( std::ostream& s, const SIMDPixel& pix ) {
      return s << "[ " << pix.rich()                          //
               << " " << Rich::text( pix.rich(), pix.side() ) //
               << " GloPos " << pix.gloPos()                  //
               << " LocPos " << pix.locPos()                  //
               << " EffArea " << pix.effArea()                //
               << " IDs " << pix.smartID()                    //
               << " ClusIndices " << pix.scClusIndex()        //
               << " Validmask " << pix.validMask() << " ]";
    }

  private:
    // data

    /// RICH
    Rich::DetectorType m_rich{Rich::InvalidDetector};

    /// Panel
    Rich::Side m_side{Rich::InvalidSide};

    /// Global position
    Point m_gloPos;

    /// Local position
    Point m_locPos;

    /// Effective cluster area
    SIMDFP m_effArea{SIMDFP::Zero()};

    /// The channel IDs for the photon detection points
    SmartIDs m_smartID;

    /// Indices to the original scalar clusters
    ScIndex m_scClusIn{-ScIndex::One()};

    /// validity mask (default initialised to false)
    Mask m_validMask{Mask( false )};
  };

  /** @class SIMDPixelSummaries RichFutureRecEvent/RichRecSIMDPixels.h
   *
   *  SIMD pixel summaries
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDPixelSummaries final : public SIMD::STDVector<SIMDPixel>, public Vc::AlignedBase<Vc::VectorAlignment> {

  public:
    // types

    /// Type for storage of SIMD Pixels
    using Vector = SIMD::STDVector<SIMDPixel>;
    /// Type for range access to SIMD pixels for a given RICH and/or side
    using Range = Gaudi::Range_<Vector, Vector::const_iterator>;

  private:
    /// Internal indices storage for ranges
    using Indices = std::pair<std::size_t, std::size_t>;

  public:
    // accessors

    /// Access the range for the given RICH
    inline Range range( const Rich::DetectorType rich ) const noexcept {
      return Range( begin() + m_richRanges[rich].first, begin() + m_richRanges[rich].second );
    }

    /// Access the range for the given RICH and panel
    inline Range range( const Rich::DetectorType rich, //
                        const Rich::Side         side ) const noexcept {
      return Range( begin() + ( m_panelRanges[rich] )[side].first, begin() + ( m_panelRanges[rich] )[side].second );
    }

  public:
    // setters

    /// Set RICH range
    inline void setRange( const Rich::DetectorType rich,    //
                          const std::size_t        beginit, //
                          const std::size_t        endit ) noexcept {
      m_richRanges[rich] = Indices( beginit, endit );
    }

    /// Set Panel range
    inline void setRange( const Rich::DetectorType rich,    //
                          const Rich::Side         side,    //
                          const std::size_t        beginit, //
                          const std::size_t        endit ) noexcept {
      ( m_panelRanges[rich] )[side] = Indices( beginit, endit );
    }

  private:
    // ata

    /// RICH ranges
    Rich::DetectorArray<Indices> m_richRanges;

    /// Panel ranges
    Rich::DetectorArray<PanelArray<Indices>> m_panelRanges;
  };

  /// TES locations
  namespace SIMDPixelSummariesLocation {
    /// Default Location in TES for the pixel SIMD summaries
    inline const std::string Default = "Rec/RichFuture/SIMDPixelSummaries/Default";
  } // namespace SIMDPixelSummariesLocation

} // namespace Rich::Future::Rec

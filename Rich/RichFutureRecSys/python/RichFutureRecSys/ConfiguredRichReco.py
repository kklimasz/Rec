###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def RichTESPixelMap(name="RICH1RICH2"):
    # Common data
    locs = {
        "RichPixelClustersLocation":
        "Rec/Rich/PixelClusters/" + name,
        "RichPixelGlobalPositionsLocation":
        "Rec/Rich/PixelPositions/Global/" + name,
        "RichPixelLocalPositionsLocation":
        "Rec/Rich/PixelPositions/Local/" + name,
        "RichSIMDPixelSummariesLocation":
        "Rec/Rich/SIMDPixelSummaries/" + name
    }
    return locs


def RichTESTrackMap(name):
    # Contruct the naming map for the given name
    locs = {
        # Track data
        "TrackSegmentsLocation":
        "Rec/Rich/TrackSegments/" + name,
        # Segment data
        "SegmentToTrackLocation":
        "Rec/Rich/Relations/SegmentToTrack/" + name,
        "TrackGlobalPointsLocation":
        "Rec/Rich/SegmentPositions/Global/" + name,
        "TrackLocalPointsLocation":
        "Rec/Rich/SegmentPositions/Local/" + name,
        "EmittedPhotonYieldLocation":
        "Rec/Rich/PhotonYields/Emitted/" + name,
        "EmittedPhotonSpectraLocation":
        "Rec/Rich/PhotonSpectra/Emitted/" + name,
        "EmittedCherenkovAnglesLocation":
        "Rec/Rich/CherenkovAngles/Emitted/" + name,
        "SignalCherenkovAnglesLocation":
        "Rec/Rich/CherenkovAngles/Signal/" + name,
        "EmittedMassHypothesisRingsLocation":
        "Rec/Rich/MassHypoRings/Emitted/" + name,
        "DetectablePhotonYieldLocation":
        "Rec/Rich/PhotonYields/Detectable/" + name,
        "DetectablePhotonSpectraLocation":
        "Rec/Rich/PhotonSpectra/Detectable/" + name,
        "SignalPhotonYieldLocation":
        "Rec/Rich/PhotonYields/Signal/" + name,
        "SignalPhotonSpectraLocation":
        "Rec/Rich/PhotonSpectra/Signal/" + name,
        "GeomEffsLocation":
        "Rec/Rich/GeomEffs/" + name,
        "GeomEffsPerPDLocation":
        "Rec/Rich/GeomEffsPerPD/" + name,
        "SegmentPhotonFlagsLocation":
        "Rec/Rich/SegmentPhotonFlags/" + name,
        "CherenkovResolutionsLocation":
        "Rec/Rich/CherenkovResolutions/" + name,
        # photon data
        "CherenkovPhotonLocation":
        "Rec/Rich/CherenkovPhotons/" + name,
        "PhotonSignalsLocation":
        "Rec/Rich/PhotonPixelSignals/" + name,
        # relations
        "InitialTrackToSegmentsLocation":
        "Rec/Rich/Relations/TrackToSegments/Initial/" + name,
        "SelectedTrackToSegmentsLocation":
        "Rec/Rich/Relations/TrackToSegments/Selected/" + name,
        "PhotonToParentsLocation":
        "Rec/Rich/Relations/PhotonToParents/" + name,
        # Summary data
        "SummaryTracksLocation":
        "Rec/Rich/Summary/Tracks/" + name,
        "SummaryPixelsLocation":
        "Rec/Rich/Summary/Pixels/" + name
    }
    return locs


# makes an instance of an algorithm with the given properties set
def makeRichAlg(type, name, props):
    alg = type(name)
    for prop, value in props.iteritems():
        try:
            alg.setProp(prop, value)
        except AttributeError:
            # Does not have this property so just continue
            pass
    return alg


# Creates a default Track location map given the list of types
def trackTESLocations(types=["Long", "Down", "Up"]):
    map = {}
    for type in types:
        map[type] = "Rec/Track/BestRich" + type
    return map


# Creates a default PID location map given the list of types
def pidTESLocations(types=["Long", "Down", "Up"]):
    map = {}
    for type in types:
        map[type] = "Rec/Rich/" + type + "PIDs"
    return map


# Utility method to check the consistency of the configured TES locations
def CheckConfig(inputTrackLocations, outputPIDLocations):

    if 0 == len(inputTrackLocations.keys()):
        raise ValueError("No input track locations specified")
    if 0 == len(outputPIDLocations.keys()):
        raise ValueError("No output PID locations specified")
    if inputTrackLocations.viewkeys() != outputPIDLocations.viewkeys():
        raise ValueError("Track and PID keys do not match")


def RichRecoSequence(
        GroupName="",  # Optional name given to this group.

        #===========================================================
        # General Data processing options
        #===========================================================

        # The data type
        dataType="",

        # Flag to indicate we are running in 'Online Brunel' mode
        onlineBrunelMode=False,

        # PID types to process
        particles=[
            "electron", "muon", "pion", "kaon", "proton", "deuteron",
            "belowThreshold"
        ],

        # radiators to process
        radiators=["Rich1Gas", "Rich2Gas"],

        #===========================================================
        # Input / Output options
        #===========================================================

        # TES location for decoded data
        decodedDataLocation="Raw/Rich/L1Data/RICH1RICH2",

        # Dict of input track locations with name key
        inputTrackLocations={"All": "Rec/Track/Best"},

        # Dict of output PID locations. Name keys must match above.
        outputPIDLocations={"All": "Rec/Rich/PIDs"},

        # Merged PID location.
        # If set, merges the above into one final location
        mergedOutputPIDLocation="",

        # PID version
        pidVersion=2,

        #===========================================================
        # Pixel treatment options
        #===========================================================

        # Maximum number of clusters GEC cut
        maxClusters=200000,

        # Should pixel clustering be run, for either ( RICH1, RICH2 )
        applyPixelClustering=(False, False),

        #===========================================================
        # Track treatment options
        #===========================================================

        # Maximum number of tracks GEC cut
        maxTracks=10000,

        # Disable non-tread-safe tracking tools
        makeTkToolsThreadSafe=False,

        # Development option to preload the detector geometry.
        # Useful for timing studies.
        preloadGeometry=False,

        # Number points for mass hypothesis ring ray tracing
        NRingPoints=(96, 96, 96),

        # Track Extrapolator type
        trackExtrapolator="TrackSTEPExtrapolator/TrackExtrapolator",

        # Allow missing track states to be created on the fly
        createMissingStates=False,

        #===========================================================
        # Photon treatment options
        #===========================================================

        # The photon reconstruction to use. Supports either :-
        #  "Quartic"          - Full solution using the analytic solution.
        #  "FastQuartic"      - Stripped down quartic algorithm.
        #  "CKEstiFromRadius" - Estimation using the ray tracing information
        photonReco="Quartic",

        #===========================================================
        # Settings for the global PID minimisation
        #===========================================================

        # Number of iterations of the global PID background and
        # likelihood minimisation
        nIterations=2,

        # The following are technical settings per iteration.
        # Do not change unless you know what you are doing ;)
        # Array size must be at least as big as the number of iterations

        # Pixel background options

        # Ignore the expected signals based on the track information
        PDBackIgnoreExpSignals=[True, False, False, False],

        # Minimum allowed pixel background value (RICH1,RICH2)
        PDBackMinPixBackground=[(0, 0), (0, 0), (0, 0), (0, 0)],

        # Maximum allowed pixel background value (RICH1,RICH2)
        PDBackMaxPixBackground=[(999, 999), (999, 999), (999, 999), (999,
                                                                     999)],

        # Group Size for PDs  RICH1 RICH2
        PDGroupSize=(4, 4),

        # Likelihood minimizer options

        # Freeze out DLL values
        TrackFreezeOutDLL=[2, 4, 5, 6],

        # Run a final check on the DLL values
        FinalDLLCheck=[False, True, True, True],

        # Force change DLL values
        TrackForceChangeDLL=[-1, -2, -3, -4],

        # likelihood threshold
        LikelihoodThreshold=[-1e-2, -1e-3, -1e-4, -1e-5],

        # Maximum tracks that can change per iteration
        MaxTrackChangesPerIt=[5, 5, 4, 3],

        # The minimum DLL signal value
        MinSignalForNoLLCalc=[1e-3, 1e-3, 1e-3, 1e-3],

        #===========================================================
        # Technical options
        #===========================================================

        # Turn on CPU time measurements
        MeasureTime=True,

        # output level
        OutputLevel=-1

        # end options
):

    #from GaudiConfig.ControlFlow import seq
    from Configurables import GaudiSequencer

    # Check config is sane
    CheckConfig(inputTrackLocations, outputPIDLocations)

    # ==================================================================
    # Processing options
    # ==================================================================

    # Update mode
    isUpgrade = dataType == "Upgrade"

    # If Online Brunel mode, turn off refractive index scale factors
    if onlineBrunelMode:
        from Configurables import UpdateManagerSvc
        UpdateManagerSvc().ConditionsOverride += [
            "Conditions/Environment/Rich1/RefractivityScaleFactor := double CurrentScaleFactor = 1.0;",
            "Conditions/Environment/Rich2/RefractivityScaleFactor := double CurrentScaleFactor = 1.0;"
        ]

    # Dictionary of general algorithm properties
    algprops = {}

    # RICH Particle properties
    ppToolName = "RichPartProp" + GroupName
    ppTool = "Rich::Future::ParticleProperties/" + ppToolName + ":PUBLIC"
    from Configurables import Rich__Future__ParticleProperties as RichPartProps
    RichPartProps("ToolSvc." + ppToolName).ParticleTypes = particles
    algprops["ParticlePropertiesTool"] = ppTool

    # Radiators ( Aerogel not supported here )
    radsF = (False, "Rich1Gas" in radiators, "Rich2Gas" in radiators)
    algprops["Radiators"] = radsF
    # Detectors
    algprops["Detectors"] = (radsF[0] or radsF[1], radsF[2])

    # Name for pixel locations
    pixname = ""
    if algprops["Detectors"][0]: pixname += "RICH1"
    if algprops["Detectors"][1]: pixname += "RICH2"

    # output level
    if OutputLevel > 0: algprops["OutputLevel"] = str(OutputLevel)

    # ==================================================================
    # RICH Pixel Treatment. Common to all track types and instanciations
    # of the RICH sequences.
    # ==================================================================

    from Configurables import Rich__Future__SmartIDClustering as RichClustering
    from Configurables import Rich__Future__Rec__PixelClusterGlobalPositions as RichGlobalPoints
    from Configurables import Rich__Future__Rec__PixelClusterLocalPositions as RichLocalPoints

    # Clustering name extension
    clusName = ""
    if applyPixelClustering[0]: clusName += "R1ClusON"
    if applyPixelClustering[1]: clusName += "R2ClusON"

    # Pixel TES locations
    cLocs = RichTESPixelMap(pixname + clusName)

    # Forms cluster objects from the raw decoded RICH hits.
    richCluster = makeRichAlg(RichClustering, "RichPixClustering", algprops)
    # Options
    richCluster.ApplyPixelClustering = applyPixelClustering
    richCluster.MaxClusters = maxClusters
    # Input
    richCluster.DecodedDataLocation = decodedDataLocation
    # Output
    richCluster.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]

    ## # Creates the space points in global LHCb coordinates
    ## richGloPoints = makeRichAlg( RichGlobalPoints, "RichPixGlobalPoints", algprops )
    ## # Options
    ## richGloPoints.NoClustering = ( not applyPixelClustering[0] and not applyPixelClustering[1] )
    ## # inputs
    ## richGloPoints.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
    ## # outputs
    ## richGloPoints.RichPixelGlobalPositionsLocation = cLocs["RichPixelGlobalPositionsLocation"]

    # Make SIMD pixel summaries
    from Configurables import Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels
    simdPixels = makeRichAlg(RichSIMDPixels, "RichSIMDPixels", algprops)
    # Options
    simdPixels.NoClustering = (not applyPixelClustering[0]
                               and not applyPixelClustering[1])
    # inputs
    simdPixels.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
    # Outputs
    simdPixels.RichSIMDPixelSummariesLocation = cLocs[
        "RichSIMDPixelSummariesLocation"]

    # The pixel sequence
    pixels = GaudiSequencer(
        "RichPixels" + GroupName,
        MeasureTime=MeasureTime,
        Members=[richCluster, simdPixels])

    # The complete sequence
    all = GaudiSequencer("RichRecoSeq" + GroupName, MeasureTime=MeasureTime)
    all.Members = [pixels]

    # Loop over tracks
    for tktype, trackLocation in inputTrackLocations.iteritems():

        # name for this sequence
        name = GroupName + tktype

        # ==================================================================
        # Intermediary data locations.
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        # Sequencer for this track type
        tkSeq = GaudiSequencer("Rich" + name + "Reco", MeasureTime=MeasureTime)
        tkSeq.Members = []  # make sure empty
        all.Members += [tkSeq]

        # ==================================================================
        # RICH Track Treatment.
        # ==================================================================

        from Configurables import Rich__Future__Rec__DetailedTrSegMakerFromTracks as SegmentCreator
        from Configurables import Rich__Future__Rec__RayTraceTrackGlobalPoints as TrackPanelGlobalPoints
        from Configurables import Rich__Future__Rec__RayTraceTrackLocalPoints as TrackPanelLocalPoints
        from Configurables import Rich__Future__Rec__EmittedPhotonYields as EmittedYields
        from Configurables import Rich__Future__Rec__DetectablePhotonYields as DetectableYields
        from Configurables import Rich__Future__Rec__SignalPhotonYields as SignalYields
        from Configurables import Rich__Future__Rec__TrackEmittedCherenkovAngles as EmittedCherenkovAngles
        from Configurables import Rich__Future__Rec__TrackSignalCherenkovAngles as SignalCherenkovAngles
        from Configurables import Rich__Future__Rec__RayTraceCherenkovCones as EmittedMassCones
        from Configurables import Rich__Future__Rec__GeomEffCKMassRings as GeomEff
        from Configurables import Rich__Future__Rec__TrackFunctionalCherenkovResolutions as TrackCKResolutions
        from Configurables import Rich__Future__Rec__SelectTrackSegments as SelectTrackSegments

        # Segment Creator from tracks
        segCr = makeRichAlg(SegmentCreator, "RichTrackSegments" + name,
                            algprops)
        # Properties
        segCr.TrackExtrapolator = trackExtrapolator
        segCr.MaxTracks = maxTracks
        segCr.PreloadGeometry = preloadGeometry
        segCr.CreateMissingStates = createMissingStates
        # Some track tools are not thread safe so turn off their use and extend some z tolerences to compensate...
        # This is temporary ....
        if makeTkToolsThreadSafe:
            segCr.CreateMissingStates = False
            segCr.UseStateProvider = False
            segCr.ZTolerances = (10000, 10000, 10000)
        # Inputs
        segCr.TracksLocation = trackLocation
        # Outputs
        segCr.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        segCr.TrackToSegmentsLocation = locs["InitialTrackToSegmentsLocation"]
        segCr.SegmentToTrackLocation = locs["SegmentToTrackLocation"]

        # Track global impact points on PD plane
        tkGloPnts = makeRichAlg(TrackPanelGlobalPoints,
                                "RichTrackGloPoints" + name, algprops)
        # Inputs
        tkGloPnts.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        # Outputs
        tkGloPnts.TrackGlobalPointsLocation = locs["TrackGlobalPointsLocation"]

        # Track local impact points on PD plane
        tkLocPnts = makeRichAlg(TrackPanelLocalPoints,
                                "RichTrackLocPoints" + name, algprops)
        # Inputs
        tkLocPnts.TrackGlobalPointsLocation = locs["TrackGlobalPointsLocation"]
        # Output
        tkLocPnts.TrackLocalPointsLocation = locs["TrackLocalPointsLocation"]

        # Emitted photon yields
        emitY = makeRichAlg(EmittedYields, "RichEmittedYields" + name,
                            algprops)
        # Inputs
        emitY.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        # Outputs
        emitY.EmittedPhotonYieldLocation = locs["EmittedPhotonYieldLocation"]
        emitY.EmittedPhotonSpectraLocation = locs[
            "EmittedPhotonSpectraLocation"]

        # Expected CK anges using emitted photon spectra
        emitChAngles = makeRichAlg(EmittedCherenkovAngles,
                                   "RichEmittedCKAngles" + name, algprops)
        # Input
        emitChAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        emitChAngles.EmittedPhotonYieldLocation = locs[
            "EmittedPhotonYieldLocation"]
        emitChAngles.EmittedPhotonSpectraLocation = locs[
            "EmittedPhotonSpectraLocation"]
        # Output
        emitChAngles.EmittedCherenkovAnglesLocation = locs[
            "EmittedCherenkovAnglesLocation"]

        # Cherenkov mass cones using emitted spectra
        emitMassCones = makeRichAlg(EmittedMassCones, "RichMassCones" + name,
                                    algprops)
        # Input
        emitMassCones.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        emitMassCones.CherenkovAnglesLocation = locs[
            "EmittedCherenkovAnglesLocation"]
        # Output
        emitMassCones.MassHypothesisRingsLocation = locs[
            "EmittedMassHypothesisRingsLocation"]
        # Options
        emitMassCones.NRingPoints = NRingPoints

        # Detectable photon yields
        detY = makeRichAlg(DetectableYields, "RichDetectableYields" + name,
                           algprops)
        # Inputs
        detY.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        detY.EmittedSpectraLocation = locs["EmittedPhotonSpectraLocation"]
        detY.MassHypothesisRingsLocation = locs[
            "EmittedMassHypothesisRingsLocation"]
        # Output
        detY.DetectablePhotonYieldLocation = locs[
            "DetectablePhotonYieldLocation"]
        detY.DetectablePhotonSpectraLocation = locs[
            "DetectablePhotonSpectraLocation"]

        # Geometrical Eff.
        geomEff = makeRichAlg(GeomEff, "RichGeomEff" + name, algprops)
        # Inputs
        geomEff.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        geomEff.CherenkovAnglesLocation = locs[
            "EmittedCherenkovAnglesLocation"]
        geomEff.MassHypothesisRingsLocation = locs[
            "EmittedMassHypothesisRingsLocation"]
        # Outputs
        geomEff.GeomEffsLocation = locs["GeomEffsLocation"]
        geomEff.GeomEffsPerPDLocation = locs["GeomEffsPerPDLocation"]
        geomEff.SegmentPhotonFlagsLocation = locs["SegmentPhotonFlagsLocation"]

        # Select final track segments
        tkSel = makeRichAlg(SelectTrackSegments, "RichTkSegmentSel" + name,
                            algprops)
        # Inputs
        tkSel.InTrackToSegmentsLocation = locs[
            "InitialTrackToSegmentsLocation"]
        tkSel.GeomEffsLocation = locs["GeomEffsLocation"]
        # Outputs
        tkSel.OutTrackToSegmentsLocation = locs[
            "SelectedTrackToSegmentsLocation"]

        # Signal Photon Yields
        sigYields = makeRichAlg(SignalYields, "RichSignalYields" + name,
                                algprops)
        # Inputs
        sigYields.DetectablePhotonYieldLocation = locs[
            "DetectablePhotonYieldLocation"]
        sigYields.DetectablePhotonSpectraLocation = locs[
            "DetectablePhotonSpectraLocation"]
        sigYields.GeomEffsLocation = locs["GeomEffsLocation"]
        # Outputs
        sigYields.SignalPhotonYieldLocation = locs["SignalPhotonYieldLocation"]
        sigYields.SignalPhotonSpectraLocation = locs[
            "SignalPhotonSpectraLocation"]

        # Signal Cherenkov angles
        sigChAngles = makeRichAlg(SignalCherenkovAngles,
                                  "RichSignalCKAngles" + name, algprops)
        # Inputs
        sigChAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        sigChAngles.SignalPhotonSpectraLocation = locs[
            "SignalPhotonSpectraLocation"]
        sigChAngles.SignalPhotonYieldLocation = locs[
            "SignalPhotonYieldLocation"]
        # Outputs
        sigChAngles.SignalCherenkovAnglesLocation = locs[
            "SignalCherenkovAnglesLocation"]

        # Track Resolutions
        tkRes = makeRichAlg(TrackCKResolutions, "RichCKResolutions" + name,
                            algprops)
        # Inputs
        tkRes.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        tkRes.SignalCherenkovAnglesLocation = locs[
            "SignalCherenkovAnglesLocation"]
        tkRes.MassHypothesisRingsLocation = locs[
            "EmittedMassHypothesisRingsLocation"]
        # Outputs
        tkRes.CherenkovResolutionsLocation = locs[
            "CherenkovResolutionsLocation"]
        # Processing options
        if isUpgrade:
            #                     Aero(ignored)  R1Gas     R2Gas
            tkRes.PDErrors = (0.0005, 0.0002, 0.0002)
            tkRes.PDRefAreas = (7.77016, 7.77016, 37.4688)
            # Scale factors to get pulls closer to one.
            # ToDo. See if we can improve the calculation of the components
            #       to remove the need for this scaling.
            # ToDo. Actually seems to make PID performance a little worse.
            #       To be understood. For now disable this correction.
            #tkRes.ScaleFactor = ( 1.0,           0.8,      1.0     )
            # Turn on full PD size treatment for RICH2 only
            tkRes.FullPDAreaTreatment = (False, False, True)

        # The track sequence
        segs = GaudiSequencer(
            "RichTracks" + name,
            MeasureTime=MeasureTime,
            Members=[
                segCr, tkGloPnts, tkLocPnts, emitY, emitChAngles,
                emitMassCones, detY, geomEff, tkSel, sigYields, sigChAngles,
                tkRes
            ])
        # Add to final sequence
        tkSeq.Members += [segs]

        # ==================================================================
        # RICH Photon Treatment.
        # ==================================================================

        if photonReco == "Quartic" or photonReco == "FastQuartic":
            from Configurables import Rich__Future__Rec__SIMDQuarticPhotonReco as PhotonReco
        elif photonReco == "CKEstiFromRadius":
            from Configurables import Rich__Future__Rec__SIMDCKEstiFromRadiusPhotonReco as PhotonReco
        else:
            raise ValueError("Unknown Photon Reco mode '" + photonReco + "'")
        from Configurables import Rich__Future__Rec__SIMDPhotonPredictedPixelSignal as PhotonPredSignal

        # The photon reconstruction
        photReco = makeRichAlg(PhotonReco, "RichPhotonReco" + name, algprops)
        # Inputs
        photReco.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        photReco.CherenkovAnglesLocation = locs[
            "SignalCherenkovAnglesLocation"]
        photReco.CherenkovResolutionsLocation = locs[
            "CherenkovResolutionsLocation"]
        photReco.TrackLocalPointsLocation = locs["TrackLocalPointsLocation"]
        photReco.TrackToSegmentsLocation = locs[
            "SelectedTrackToSegmentsLocation"]
        photReco.SegmentPhotonFlagsLocation = locs[
            "SegmentPhotonFlagsLocation"]
        photReco.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"]
        # Outputs
        photReco.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
        photReco.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        # Processing options
        if photonReco == "CKEstiFromRadius":
            # This implementation requires the CK rings
            photReco.MassHypothesisRingsLocation = locs[
                "EmittedMassHypothesisRingsLocation"]
        if isUpgrade:
            #                                     Aero  R1Gas  R2gas   (Aero is ignored)
            # Scale factors
            photReco.ScaleFactorCKTheta = (0.24, 0.052, 0.03)
            photReco.ScaleFactorSepG = (342, 98.2, 130)
            # Pre-sel cuts ( original )
            #photReco.PreSelMinTrackROI        = (  230,    0,     0   )
            #photReco.PreSelMaxTrackROI        = (  540,  110,   165   )
            #photReco.PreSelNSigma             = (  17,     6,    10   )
            # Pre-sel cuts ( new )
            photReco.PreSelMinTrackROI = (230, 0, 0)
            photReco.PreSelMaxTrackROI = (540, 110, 165)
            photReco.PreSelNSigma = (17, 6, 10)
            # Final sel cuts ( original )
            #photReco.MinAllowedCherenkovTheta = ( 0.150, 0.005, 0.005 )
            #photReco.MaxAllowedCherenkovTheta = ( 0.310, 0.075, 0.035 )
            #photReco.NSigma                   = ( 9.0,   5.25,  5.0   )
            # Final sel cuts ( new )
            photReco.MinAllowedCherenkovTheta = (0.150, 0.005, 0.005)
            photReco.MaxAllowedCherenkovTheta = (0.310, 0.055, 0.032)
            photReco.NSigma = (9.0, 3.50, 4.40)
        if photonReco == "Quartic":
            # Number of iterations for non-flat secondary mirrors
            photReco.NQuarticIterationsForSecMirrors = (1, 1, 3)
            photReco.FindUnambiguousPhotons = (False, False, False)
            photReco.CheckBeamPipe = (False, False, False)
        if photonReco == "FastQuartic":
            # Turn off various aspects for speed         Aero   R1Gas  R2Gas
            photReco.FindUnambiguousPhotons = (False, False, False)
            photReco.CheckBeamPipe = (False, False, False)
            photReco.NQuarticIterationsForSecMirrors = (1, 1, 1)
            # Tighten the selection cuts
            photReco.NSigma = (9.0, 2.0, 2.0)
        if onlineBrunelMode:
            # Activate loose photon selection for n-1 fits
            # Pre-sel cuts
            photReco.PreSelMinTrackROI = (230, 0, 0)
            photReco.PreSelMaxTrackROI = (540, 110, 165)
            photReco.PreSelNSigma = (40, 35, 25)
            # Final sel cuts
            photReco.MinAllowedCherenkovTheta = (0.150, 0.005, 0.005)
            photReco.MaxAllowedCherenkovTheta = (0.340, 0.075, 0.035)
            photReco.NSigma = (30, 25, 12)

        # Predicted pixel signals
        photPredSig = makeRichAlg(PhotonPredSignal,
                                  "RichPredPixelSignal" + name, algprops)
        # Input
        photPredSig.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"]
        photPredSig.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        photPredSig.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
        photPredSig.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        photPredSig.CherenkovAnglesLocation = locs[
            "SignalCherenkovAnglesLocation"]
        photPredSig.CherenkovResolutionsLocation = locs[
            "CherenkovResolutionsLocation"]
        photPredSig.PhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
        # Output
        photPredSig.PhotonSignalsLocation = locs["PhotonSignalsLocation"]

        # The photon reco sequence
        phots = GaudiSequencer(
            "RichPhotons" + name,
            MeasureTime=MeasureTime,
            Members=[photReco, photPredSig])
        # Add to final sequence
        tkSeq.Members += [phots]

        # ==================================================================
        # Create working event summary of reco info
        # ==================================================================

        from Configurables import Rich__Future__Rec__SIMDRecoSummary as RecSummary

        recSum = makeRichAlg(RecSummary, "RichRecSummary" + name, algprops)
        # Inputs
        recSum.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        recSum.TrackToSegmentsLocation = locs[
            "SelectedTrackToSegmentsLocation"]
        recSum.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        recSum.DetectablePhotonYieldLocation = locs[
            "DetectablePhotonYieldLocation"]
        recSum.SignalPhotonYieldLocation = locs["SignalPhotonYieldLocation"]
        recSum.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
        recSum.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"]
        # Output
        recSum.SummaryTracksLocation = locs["SummaryTracksLocation"]
        recSum.SummaryPixelsLocation = locs["SummaryPixelsLocation"]
        # Add to sequence
        tkSeq.Members += [recSum]

        # ==================================================================
        # RICH Global PID
        # ==================================================================

        from Configurables import Rich__Future__Rec__GlobalPID__InitialisePIDInfo as GPIDInit
        from Configurables import Rich__Future__Rec__GlobalPID__WriteRichPIDs as WriteRichPIDs
        from Configurables import Rich__Future__Rec__SIMDPixelBackgroundsEstiAvHPD as PixelBackgrounds
        from Configurables import Rich__Future__Rec__GlobalPID__SIMDLikelihoodMinimiser as LikelihoodMinimiser

        # Initalise some default locations
        gpidInit = makeRichAlg(GPIDInit, "RichGPIDInit" + name, algprops)
        # Inputs
        gpidInit.SummaryTracksLocation = locs["SummaryTracksLocation"]
        # Output
        gpidInit.TrackPIDHyposLocation = "Rec/Rich/TrackHypos/Init/" + name
        gpidInit.TrackDLLsLocation = "Rec/Rich/TrackDLLs/Init/" + name

        # Save the 'last iteration' containers
        lastTrackPIDHyposLocation = gpidInit.TrackPIDHyposLocation
        lastTrackDLLsInputLocation = gpidInit.TrackDLLsLocation

        # The pid sequence
        pid = GaudiSequencer(
            "RichPID" + name, MeasureTime=MeasureTime, Members=[gpidInit])
        # Add to final sequence
        tkSeq.Members += [pid]

        # PID iterations
        for it in range(0, nIterations):

            itN = "It" + ` it `

            # Pixel backgrounds
            pixBkgs = makeRichAlg(PixelBackgrounds,
                                  "RichPixBackgrounds" + itN + name, algprops)
            # Input
            pixBkgs.TrackPIDHyposLocation = lastTrackPIDHyposLocation
            pixBkgs.TrackToSegmentsLocation = locs[
                "SelectedTrackToSegmentsLocation"]
            pixBkgs.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            pixBkgs.GeomEffsPerPDLocation = locs["GeomEffsPerPDLocation"]
            pixBkgs.DetectablePhotonYieldLocation = locs[
                "DetectablePhotonYieldLocation"]
            pixBkgs.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"]
            # Output
            pixBkgs.PixelBackgroundsLocation = "Rec/Rich/PixelBackgrounds/" + itN + "/" + name
            # Settings
            pixBkgs.IgnoreExpectedSignals = PDBackIgnoreExpSignals[it]
            pixBkgs.MinPixelBackground = PDBackMinPixBackground[it]
            pixBkgs.MaxPixelBackground = PDBackMaxPixBackground[it]
            if isUpgrade:
                pixBkgs.PDGroupSize = PDGroupSize

            # Likelihood minimiser
            like = makeRichAlg(LikelihoodMinimiser,
                               "RichGPIDLikelihood" + itN + name, algprops)
            # Input
            like.SummaryTracksLocation = locs["SummaryTracksLocation"]
            like.SummaryPixelsLocation = locs["SummaryPixelsLocation"]
            like.PixelBackgroundsLocation = pixBkgs.PixelBackgroundsLocation
            like.TrackPIDHyposInputLocation = lastTrackPIDHyposLocation
            like.TrackDLLsInputLocation = lastTrackDLLsInputLocation
            like.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            like.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
            # Output
            like.TrackPIDHyposOutputLocation = "Rec/Rich/TrackHypos/" + itN + "/" + name
            like.TrackDLLsOutputLocation = "Rec/Rich/TrackDLLs/" + itN + "/" + name
            # Settings
            like.TrackFreezeOutDLL = TrackFreezeOutDLL[it]
            like.FinalDLLCheck = FinalDLLCheck[it]
            like.TrackForceChangeDLL = TrackForceChangeDLL[it]
            like.LikelihoodThreshold = LikelihoodThreshold[it]
            like.MaxTrackChangesPerIt = MaxTrackChangesPerIt[it]
            like.MinSignalForNoLLCalc = MinSignalForNoLLCalc[it]

            # Update the 'last' TES locations
            lastTrackPIDHyposLocation = like.TrackPIDHyposOutputLocation
            lastTrackDLLsInputLocation = like.TrackDLLsOutputLocation

            # Append to the PID sequence
            pid.Members += [pixBkgs, like]

        # Write the file RichPID objects
        writePIDs = makeRichAlg(WriteRichPIDs, "RichGPIDWriteRichPIDs" + name,
                                algprops)
        # options
        writePIDs.PIDVersion = pidVersion
        # Inputs
        writePIDs.TracksLocation = trackLocation
        writePIDs.SummaryTracksLocation = locs["SummaryTracksLocation"]
        writePIDs.TrackPIDHyposInputLocation = lastTrackPIDHyposLocation
        writePIDs.TrackDLLsInputLocation = lastTrackDLLsInputLocation
        # Outputs
        writePIDs.RichPIDsLocation = outputPIDLocations[tktype]

        # update the pid sequence
        pid.Members += [writePIDs]

    # ==================================================================
    # Merge the final PIDs
    # ==================================================================

    if "" != mergedOutputPIDLocation:

        from Configurables import Rich__Future__Rec__MergeRichPIDs as MergePIDs
        pidMerge = makeRichAlg(MergePIDs, "MergeRichPIDs" + GroupName,
                               algprops)
        pidMerge.InputRichPIDLocations = outputPIDLocations.values()
        pidMerge.OutputRichPIDLocation = mergedOutputPIDLocation
        pidMerge.PIDVersion = pidVersion

        all.Members += [pidMerge]

    # ==================================================================
    # Return the final sequence
    # ==================================================================
    return all

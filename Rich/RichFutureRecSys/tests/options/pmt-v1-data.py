###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *

from Configurables import CondDB
CondDB().setProp("Upgrade", True)

# Timestamps in messages
from Configurables import LHCbApp
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"

from GaudiConf import IOHelper
data = [
    "PFN:root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Upgrade/Sim09c-Up02/30000000/XDST/00067189_00000052_1.xdst"
]
IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp

#############################################################################


def turnOffTrigger():
    from Configurables import GaudiSequencer
    GaudiSequencer("DecodeTriggerSeq").Members = []


appendPostConfigAction(turnOffTrigger)

Brunel().VetoHltErrorEvents = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK = False

#############################################################################

#Brunel.RichSequences = ["RICH"]

#from Configurables import RecMoniConf
#RecMoniConf().MoniSequence = richs
#Brunel().MCCheckSequence = richs+["PROTO"]

#RecMoniConf().MoniSequence = []
#Brunel().MCCheckSequence = []
#GaudiSequencer("MCLinksCaloSeq").Members = [ ]

#Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Magnet', 'Tr']
#from Configurables import RecSysConf
#RecSysConf().Detectors = Brunel().Detectors
#Brunel().RecoSequence = ["Decoding","TrFast","TrBest","RICH"]

#############################################################################

# Timestamps in messages
#LHCbApp().TimeStamp = True

#Brunel().OutputType = 'None'
#Brunel().OutputType = 'DST'
#Brunel().OutputType = 'LDST'
Brunel().OutputType = 'XDST'

importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

Brunel().EvtMax = -1
Brunel().PrintFreq = 10

#Brunel().Histograms = "Expert"
#Brunel().Histograms = "None"

myBuild = os.environ['User_release_area'].split('/')[-1]
myConf = os.environ['CMTCONFIG']
Brunel().DatasetName = "Brunel-" + myBuild + "-" + myConf

#ApplicationMgr().ExtSvc += [ "AuditorSvc" ]

##AuditorSvc().Auditors += [ "FPEAuditor" ]
## from Configurables import FPEAuditor
## #FPEAuditor().TrapOn = [ "DivByZero", "Overflow", "Underflow" ]
## #FPEAuditor().ActivateAt = ["Execute"]

#AuditorSvc().Auditors += [ "MemoryAuditor" ]

#Brunel().Monitors=["SC","FPE"]

#Brunel().OnlineMode = True

Brunel().FilterTrackStates = False

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichPMT" ]
#msgSvc.OutputLevel = 1
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 30

#from Configurables import UpdateManagerSvc
#UpdateManagerSvc().OutputLevel = 1

#from Configurables import TrackBestTrackCreator, TrackVectorFitter
#fitter = TrackBestTrackCreator().addTool(TrackVectorFitter, 'Fitter')
#fitter.OutputLevel = 1

#from Configurables import CondDB
#CondDB().IgnoreHeartBeat = True
#CondDB().EnableRunStampCheck = False

#from Configurables import TrackSys
#TrackSys().ExpertTracking += ["simplifiedGeometry"]

#from Configurables import FTRawBankDecoder
#FTRawBankDecoder("createFTClusters").DecodingVersion = 2

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{Brunel.py,data/PMTs/MCXDigiFiles.py} 2>&1 | tee Brunel-${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------

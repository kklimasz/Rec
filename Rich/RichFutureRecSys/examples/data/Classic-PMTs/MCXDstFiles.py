###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/MC/Upgrade/Sim09c-Up02/30000000/XDST/",
    # CERN EOS
    "/eos/lhcb/user/j/jonrob/data/MC/Upgrade/Sim09c-Up02/30000000/XDST/",
    # CRJ's CernVM
    "/home/chris/LHCb/Data/MC/Upgrade/Sim09c-Up02/30000000/XDST/",
    # the pit
    "/group/rich/jonesc/LHCb/Data/MC/Upgrade/Sim09c-Up02/30000000/XDST/"
]

print "Data Files :-"
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.xdst"))
    data += ["PFN:" + file for file in files]
    for f in files:
        print f

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"

from Configurables import CondDB
CondDB().setProp("Upgrade", True)

#from Configurables import Brunel
#Brunel().InputType    = "DST"
#Brunel().SkipTracking = True
#Brunel().DataType   = LHCbApp().DataType
#Brunel().Simulation = LHCbApp().Simulation
#Brunel().RecoSequence  = Brunel().RichSequences + [ "PROTO" ]

#from Configurables import DstConf
#DstConf().setProp("EnableUnpack", ["Tracking"] )
#ApplicationMgr().ExtSvc += [ "DataOnDemandSvc" ]

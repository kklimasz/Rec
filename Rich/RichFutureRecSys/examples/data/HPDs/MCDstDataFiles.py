###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/MC/Run2/XDST/",  # Cambridge
    "/home/chris/LHCb/Data/MC/Run2/XDST/"  # CRJ's CernVM
]

data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.xdst"))
    data += ["'PFN:" + file for file in files]

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import Brunel, LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType = "2015"
LHCbApp().DDDBtag = "dddb-20140729"
LHCbApp().CondDBtag = "sim-20140730-vc-md100"

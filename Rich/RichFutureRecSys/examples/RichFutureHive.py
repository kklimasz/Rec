###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

###############################################################
# Job options file
#==============================================================

from Gaudi.Configuration import *
from Configurables import LHCbApp, HiveWhiteBoard

# Enable Hive !!
#==============================================================

LHCbApp().EnableHive = True
LHCbApp().Scheduler = "HLTControlFlowMgr"
LHCbApp().ThreadPoolSize = 2

#from Configurables import AlgResourcePool, HiveWhiteBoard, ForwardSchedulerSvc
#AlgResourcePool().OutputLevel = VERBOSE

#scheduler = ForwardSchedulerSvc()
#scheduler.OutputLevel = VERBOSE
whiteboard = HiveWhiteBoard("EventDataSvc")

#scheduler.MaxAlgosInFlight = 20
#scheduler.ThreadPoolSize   = 30
whiteboard.EventSlots = 2

#==============================================================

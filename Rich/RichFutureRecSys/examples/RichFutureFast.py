###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

###############################################################
# Job options file
#==============================================================

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = [
    'Trigger/RawEvent', 'Rich/RawEvent', 'pRec/Track/Best', 'pMC/Particles'
]

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN("ODINFutureDecode")

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder("RichFutureDecode")
richDecode.Detectors = (True, False)  # RICH1 only

# Explicitly unpack the Tracks

#from Configurables import UnpackTrackFunctional
#tkUnpack = UnpackTrackFunctional("UnpackTracks")
from Configurables import UnpackTrack
tkUnpack = UnpackTrack("UnpackTracks")

# Filter the tracks by type

from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")

# Now get the RICH sequences

#photoRecoType = "Quartic"
photoRecoType = "CKEstiFromRadius"

# Preload the geometry during initialise. Useful for timing studies.
preloadGeom = True
#preloadGeom = False

# Work around for some tracking tools the RICH uses that doesn't seem
# to be thread safe yet. Does not matter for normal execution, but if
# using Gaudi Hive set this to true. Changes how the RICH works to
# a not completely ideal mode, but avoids the problem.
#workAroundTrackTools = False
workAroundTrackTools = True

# Input tracks
tkLocs = {
    "Long": tkFilt.OutLongTracksLocation,
    "Down": tkFilt.OutDownTracksLocation,
    "Up": tkFilt.OutUpTracksLocation
}
#tkLocs = { "All"  : "Rec/Track/Best" }
#tkLocs = { "Long" : tkFilt.OutLongTracksLocation }
#tkLocs = { "Up" : tkFilt.OutUpTracksLocation }

# Output PID
pidLocs = {
    "Long": "Rec/Rich/LongPIDs",
    "Down": "Rec/Rich/DownPIDs",
    "Up": "Rec/Rich/UpPIDs"
}
#pidLocs = { "All" : "Rec/Rich/PIDs" }
#pidLocs = { "Long" : "Rec/Rich/LongPIDs" }
#pidLocs = { "Up" : "Rec/Rich/UpPIDs" }

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# Number of ring points to use during ray tracing
# Default (Offline, HLT2)
#nRingPoints = ( 100, 100, 100 )
# Fast (HLT1??)
nRingPoints = (15, 15, 15)

# Particle Types
#parts = ["electron","muon","pion","kaon","proton","deuteron","belowThreshold"]
parts = ["pion", "kaon"]

# Track extrapolator
#tkExt = "TrackRungeKuttaExtrapolator"
tkExt = "TrackParabolicExtrapolator"

# Radiators
#rads = ["Rich1Gas","Rich2Gas"]
rads = ["Rich1Gas"]

# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence(
    photonReco=photoRecoType,
    preloadGeometry=preloadGeom,
    makeTkToolsThreadSafe=workAroundTrackTools,
    NRingPoints=nRingPoints,
    trackExtrapolator=tkExt,
    particles=parts,
    radiators=rads,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc)

# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors(
    particles=parts, inputTrackLocations=tkLocs, outputPIDLocations=pidLocs)

# Explicitly unpack the MCParticles
from Configurables import UnpackMCParticle
mcPUnpack = UnpackMCParticle()

# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers(
    particles=parts, inputTrackLocations=tkLocs, outputPIDLocations=pidLocs)

# The final sequence to run
all = GaudiSequencer(
    "All",
    Members=[fetcher, odinDecode, richDecode, tkUnpack, tkFilt, RichRec])
all.Members += [RichMoni]
#all.Members += [ mcPUnpack, RichCheck ]
all.MeasureTime = True

ApplicationMgr(
    TopAlg=[all],
    EvtMax=50000,  # events to be processed (default is 10)
    #EvtSel = 'NONE', # do not use any event input
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

EventSelector().PrintFreq = 1000
#LHCbApp().SkipEvents = 1278

CondDB()  # Just to initialise
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# ROOT persistency for histograms
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
HistogramPersistencySvc().OutputFile = "RichFutureFast.root"
#AuditorSvc().Auditors += [ "FPEAuditor" ]
#from Configurables import FPEAuditor
#FPEAuditor().ActivateAt = ["Execute"]

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer
import os

# --------------------------------------------------------------------------------------

# histograms
#histos = "OfflineFull"
histos = "Expert"

# ROOT persistency
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
myBuild = os.environ['User_release_area'].split('/')[-1]
myConf = os.environ['CMTCONFIG']
rootFileBaseName = "RichFuture-" + myBuild + "-" + myConf + "-" + histos
HistogramPersistencySvc().OutputFile = rootFileBaseName + "-Histos.root"

# Event numbers
nEvents = 10000
EventSelector().PrintFreq = 100
#LHCbApp().SkipEvents      = 616

# Just to initialise
CondDB()
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichPMT" ]
#msgSvc.OutputLevel = 1
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
from Configurables import FPEAuditor
FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# Finally set up the application
ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=['ToolSvc', 'AuditorSvc'],
    AuditAlgorithms=True)

#ApplicationMgr().ExtSvc += ["Gaudi::MetaDataSvc"]

# --------------------------------------------------------------------------------------

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent', 'Rich/RawEvent', 'pRec/Track/Best']
all.Members += [fetcher]

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN("ODINFutureDecode")
all.Members += [odinDecode]

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder("RichFutureDecode")
all.Members += [richDecode]

# Explicitly unpack the Tracks

#from Configurables import UnpackTrackFunctional
#tkUnpack = UnpackTrackFunctional("UnpackTracks")
from Configurables import UnpackTrack
tkUnpack = UnpackTrack("UnpackTracks")
all.Members += [tkUnpack]

# Filter the tracks by type

from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")
all.Members += [tkFilt]

# Now get the RICH sequence

photoRecoType = "Quartic"
#photoRecoType = "FastQuartic"
#photoRecoType = "CKEstiFromRadius"

# Preload the geometry during initialise. Useful for timing studies.
preloadGeom = True
#preloadGeom = False

# Work around for some tracking tools the RICH uses that doesn't seem
# to be thread safe yet. Does not matter for normal execution, but if
# using Gaudi Hive set this to true. Changes how the RICH works to
# a not completely ideal mode, but avoids the problem.
workAroundTrackTools = False
#workAroundTrackTools = True

# track extrapolator type
#tkExtrap = "TrackRungeKuttaExtrapolator/TrackExtrapolator"
tkExtrap = "TrackSTEPExtrapolator/TrackExtrapolator"

# Input tracks
#tkLocs = {
#    "Long": tkFilt.OutLongTracksLocation,
#    "Down": tkFilt.OutDownTracksLocation,
#    "Up": tkFilt.OutUpTracksLocation
#}
#tkLocs = { "All"  : "Rec/Track/Best" }
tkLocs = {"Long": tkFilt.OutLongTracksLocation}
#tkLocs = { "Up" : tkFilt.OutUpTracksLocation }
#tkLocs = { "Down" : tkFilt.OutDownTracksLocation }

# Output PID
#pidLocs = {
#    "Long": "Rec/Rich/LongPIDs",
#    "Down": "Rec/Rich/DownPIDs",
#    "Up": "Rec/Rich/UpPIDs"
#}
#pidLocs = { "All" : "Rec/Rich/PIDs" }
pidLocs = {"Long": "Rec/Rich/LongPIDs"}
#pidLocs = { "Up" : "Rec/Rich/UpPIDs" }
#pidLocs = { "Down" : "Rec/Rich/DownPIDs" }

# RICH radiators to use
rads = ["Rich1Gas", "Rich2Gas"]
#rads = ["Rich1Gas"]
#rads = ["Rich2Gas"]

# PID types to process
parts = [
    "electron", "muon", "pion", "kaon", "proton", "deuteron", "belowThreshold"
]
#parts = ["pion","kaon","proton","deuteron","belowThreshold"]

# Number ring points for ray tracing ( Default 96 )
ringPoints = (96, 96, 96)
#ringPoints = ( 48, 48, 48 )

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
#dType = "2016"
dType = "Upgrade"

# Online Brunel mode.
#online = True
online = False

# PD Grouping for backgrounds
pdGroupSize = (4, 4)

# Create missing track states
createStates = False

# Perform clustering ?
pixelClustering = (False, False)
#pixelClustering = ( True, True )

# OutputLevel (-1 means no setting)
outLevel = -1

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence(
    OutputLevel=outLevel,
    dataType=dType,
    onlineBrunelMode=online,
    photonReco=photoRecoType,
    preloadGeometry=preloadGeom,
    radiators=rads,
    particles=parts,
    makeTkToolsThreadSafe=workAroundTrackTools,
    createMissingStates=createStates,
    PDGroupSize=pdGroupSize,
    NRingPoints=ringPoints,
    applyPixelClustering=pixelClustering,
    trackExtrapolator=tkExtrap,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc)

# The final sequence to run
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors(
    inputTrackLocations=tkLocs,
    radiators=rads,
    applyPixelClustering=pixelClustering,
    onlineBrunelMode=online,
    outputPIDLocations=pidLocs,
    histograms=histos)
# Uncomment to enable monitoring
all.Members += [RichMoni]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# UnpackMC
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack
fetcher.DataKeys += ['pMC/Vertices', 'pMC/Particles']
mcUnpack = GaudiSequencer("UnpackMCSeq", MeasureTime=True)
mcUnpack.Members += [
    UnpackMCVertex(),
    UnpackMCParticle(),
    RichSumUnPack("RichSumUnPack")
]
all.Members += [mcUnpack]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers(
    inputTrackLocations=tkLocs,
    radiators=rads,
    applyPixelClustering=pixelClustering,
    onlineBrunelMode=online,
    outputPIDLocations=pidLocs,
    histograms=histos)
# Uncomment to enable checking
all.Members += [RichCheck]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# ProtoParticle monitoring. For detailed PID monitoring.
protoMoniSeq = GaudiSequencer("ProtoMonitoring", MeasureTime=True)
all.Members += [protoMoniSeq]  # Uncomment to enable Proto tuples.
# Remake the protos with the new Rich PID objects
from Configurables import (UnpackProtoParticle,
                           ChargedProtoParticleAddRichInfo,
                           ChargedProtoCombineDLLsAlg)
protoMoniSeq.Members += [
    UnpackProtoParticle(
        name="UnpackChargedProtos",
        OutputName="/Event/Rec/ProtoP/Charged",
        InputName="/Event/pRec/ProtoP/Charged"),
    ChargedProtoParticleAddRichInfo("ChargedProtoPAddRich"),
    ChargedProtoCombineDLLsAlg("ChargedProtoPCombDLLs")
]
# monitoring config
from Configurables import GlobalRecoChecks
GlobalRecoChecks().Sequencer = protoMoniSeq
GlobalRecoChecks().ProtoTupleName = rootFileBaseName + "-ProtoTuple.root"
GlobalRecoChecks().ANNTupleName = rootFileBaseName + "-ANNPIDTuple.root"
# ANNPID monitoring
GlobalRecoChecks().AddANNPIDInfo = False
#GlobalRecoChecks().AddANNPIDInfo = True
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCXDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------

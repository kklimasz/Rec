/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <array>
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelBackgrounds RichPixelBackgrounds.h
   *
   *  Monitors the RICH pixel backgrounds.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class PixelBackgrounds final : public Consumer<void( const SIMDPixelSummaries&, //
                                                       const SIMDPixelBackgrounds& ),
                                                 Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PixelBackgrounds( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries&   pixels, //
                     const SIMDPixelBackgrounds& backgrounds ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Scalar type for SIMD data
    using FP = SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = SIMD::FP<Rich::SIMD::DefaultScalarFP>;

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;
  };

} // namespace Rich::Future::Rec::Moni

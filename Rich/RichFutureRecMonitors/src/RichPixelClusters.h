/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelClusters RichPixelClusters.h
   *
   *  Monitors the RICH pixel clusters.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class PixelClusters final : public Consumer<void( const Rich::PDPixelCluster::Vector& ), //
                                              Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PixelClusters( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    void operator()( const Rich::PDPixelCluster::Vector& clusters ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;

    /// empty cluster warning
    mutable WarningCounter m_emptyClusWarn{this, "Empty relation table!"};
  };

} // namespace Rich::Future::Rec::Moni

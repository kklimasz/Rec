/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <algorithm>
#include <array>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

namespace Rich::Future::Rec::MC::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class CherenkovResolution RichCherenkovResolution.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class CherenkovResolution final : public Consumer<void( const Summary::Track::Vector&,                   //
                                                          const LHCb::Track::Selection&,                   //
                                                          const SIMDPixelSummaries&,                       //
                                                          const Rich::PDPixelCluster::Vector&,             //
                                                          const Relations::PhotonToParents::Vector&,       //
                                                          const LHCb::RichTrackSegment::Vector&,           //
                                                          const CherenkovAngles::Vector&,                  //
                                                          const CherenkovResolutions::Vector&,             //
                                                          const SIMDCherenkovPhoton::Vector&,              //
                                                          const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                          const LHCb::MCRichDigitSummarys& ),
                                                    Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CherenkovResolution( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Selection&                   tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const CherenkovResolutions::Vector&             ckResolutions, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Number of bins for 1D pull plots
    static const std::size_t nPullBins = 10;

    /// momentum bin
    inline std::size_t pullBin( const double ptot, const Rich::RadiatorType rad ) const {
      return ( ptot < m_PtotMin[rad]
                   ? 0
                   : ptot > m_PtotMax[rad] ? nPullBins - 1
                                           : ( std::size_t )( ( ptot - m_PtotMin[rad] ) * m_pullPtotInc[rad] ) );
    }

    /// min max for given momentum bin ( in GeV )
    std::pair<double, double> binMinMaxPtot( const std::size_t bin, const Rich::RadiatorType rad ) const {
      return {( m_PtotMin[rad] + ( bin / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV,
              ( m_PtotMin[rad] + ( ( bin + 1 ) / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV};
    }

    /// Get the histo ID string for a given bin
    inline std::string binToID( const std::size_t i ) const noexcept {
      return "Pulls/PtotBins/ckPull-Bin" + std::to_string( i );
    }

  private:
    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.0f, 0.0f, 0.0f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.005f, 0.008f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.055f, 0.031f}};

    /// Min for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMin{this, "CKResMin", {0.0f, 0.00075f, 0.00040f}};

    /// Max for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMax{this, "CKResMax", {0.025f, 0.0026f, 0.00102f}};

    /// Min momentum by radiator
    Gaudi::Property<RadiatorArray<double>> m_PtotMin{
        this, "PtotMin", {0.0, 2.0 * Gaudi::Units::GeV, 5.0 * Gaudi::Units::GeV}};

    /// Max momentum by radiator
    Gaudi::Property<RadiatorArray<double>> m_PtotMax{
        this, "PtotMax", {100.0 * Gaudi::Units::GeV, 100.0 * Gaudi::Units::GeV, 100.0 * Gaudi::Units::GeV}};

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{this, "SkipElectrons", false};

  private:
    /// cache 1/increment for pull ptot binning
    RadiatorArray<double> m_pullPtotInc = {0, 0, 0};
  };

} // namespace Rich::Future::Rec::MC::Moni

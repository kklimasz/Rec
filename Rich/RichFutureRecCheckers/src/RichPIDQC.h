/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>
#include <mutex>
#include <numeric>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/RichPID.h"
#include "Event/Track.h"

// Relations
#include "RichFutureMCUtils/RichMCRelations.h"
#include "RichFutureMCUtils/TrackToMCParticle.h"

// tools
#include "RichFutureRecInterfaces/IRichPIDPlots.h"
#include "TrackInterfaces/ITrackSelector.h"

// RichUtils
#include "RichUtils/RichMap.h"

// Boost
#include "boost/format.hpp"
#include "boost/lexical_cast.hpp"

namespace Rich::Future::Rec::MC::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PIDQC RichPIDQC.h
   *
   *  MC checking of the RICH PID performance
   *
   *  @author Chris Jones
   *  @date   2016-12-08
   */

  class PIDQC final : public Consumer<void( const LHCb::Track::Selection&, //
                                            const LHCb::RichPIDs&,         //
                                            const Rich::Future::MC::Relations::TkToMCPRels& ),
                                      Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PIDQC( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm finalization
    StatusCode finalize() override;

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Selection&                   tracks, //
                     const LHCb::RichPIDs&                           pids,   //
                     const Rich::Future::MC::Relations::TkToMCPRels& rels ) const override;

  private: // definitions
    /** @class Radiators RichPIDQC.h
     *  Utility class for radiators used
     */
    class Radiators {
    public:
      Radiators( const bool aero  = false, //
                 const bool r1gas = false, //
                 const bool r2gas = false )
          : hasAero( aero ), hasR1Gas( r1gas ), hasR2Gas( r2gas ) {}
      inline std::string radiators() const {
        std::string rads = "'";
        if ( hasAero ) { rads += "Aerogel"; }
        if ( hasR1Gas ) {
          if ( rads != "'" ) { rads += '+'; };
          rads += "Rich1Gas";
        }
        if ( hasR2Gas ) {
          if ( rads != "'" ) { rads += "+"; };
          rads += "Rich2Gas";
        }
        if ( rads == "'" ) { rads += "NoRadiators"; }
        rads += "'";
        rads.resize( 30, ' ' );
        return rads;
      }
      inline bool operator<( const Radiators& rads ) const { return rads.value() < this->value(); }

    private:
      inline int value() const { return ( (int)hasAero ) * 100 + ( (int)hasR1Gas ) * 10 + ( (int)hasR2Gas ); }

    private:
      bool hasAero{false};  ///< Has aerogel info
      bool hasR1Gas{false}; ///< Has Rich1 Gas
      bool hasR2Gas{false}; ///< Has Rich1 Gas
    public:
      using Map = Rich::Map<Radiators, unsigned long long>;
    };

  private:
    /// Fill the table with the given entries
    inline void fillTable( const Rich::ParticleIDType recopid, //
                           const Rich::ParticleIDType mcpid,   //
                           const double               weight = 1.0 ) const {
      plot2D( (int)recopid, (int)mcpid, "pidTable", "PID Performance Table", -1.5, Rich::NParticleTypes + 0.5, -1.5,
              Rich::NParticleTypes + 0.5, Rich::NParticleTypes + 1, Rich::NParticleTypes + 1, weight );
      m_sumTab[mcpid][recopid] += weight;
    }

  private:
    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};

    /// PID plots tool
    ToolHandle<const IPIDPlots> m_pidPlots{this, "PlotsTool", "Rich::Future::Rec::PIDPlots/PIDPlots"};

  private: // mutable tallies
    /// Tally of tracks which each type of radiator
    mutable Radiators::Map m_radCount;

    /// Correlations between ID'ed and MC PID types
    mutable ParticleArray<ParticleArray<double>> m_sumTab = {{}};

    /// Event count ( [0] == all events, [1] == events with PID information )
    mutable unsigned long long m_nEvents[2] = {0, 0};

    /// Track count ( [0] == all tracks, [1] == tracks with PID information )
    mutable unsigned long long m_nTracks[2] = {0, 0};

  private:
    /// mutex lock for updating the various tallies.
    mutable std::mutex m_updateLock;
  };

} // namespace Rich::Future::Rec::MC::Moni

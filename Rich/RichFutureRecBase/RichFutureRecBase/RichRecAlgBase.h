/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecAlgBase.h
 *
 *  Header file for reconstruction algorithm base class : Rich::Rec::AlgBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   05/04/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// base classes
#include "RichFutureKernel/RichAlgBase.h"
#include "RichFutureRecBase/RichRecBase.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class AlgBase RichRecAlgBase.h RichRecBase/RichRecAlgBase.h
   *
   *  Abstract base class for RICH reconstruction algorithms providing
   *  some basic functionality.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   05/04/2002
   */
  //-----------------------------------------------------------------------------

  class AlgBase : public Rich::Future::AlgBase, public Rich::Future::Rec::CommonBase<Rich::Future::AlgBase> {

  public:
    /// Standard constructor
    AlgBase( const std::string& name, ISvcLocator* pSvcLocator );

    /** Initialization of the algorithm after creation
     *
     * @return The status of the initialization
     * @retval StatusCode::SUCCESS Initialization was successful
     * @retval StatusCode::FAILURE Initialization failed
     */
    virtual StatusCode initialize() override;

    /** The main event processing method. Called once for each event
     *
     * @return The status of the event processing
     * @retval StatusCode::SUCCESS Event processing was successful
     * @retval StatusCode::FAILURE Event processing failed
     */
    virtual StatusCode execute() override;

    /** Finalization of the algorithm before deletion
     *
     * @return The status of the finalization
     * @retval StatusCode::SUCCESS Finalization was successful
     * @retval StatusCode::FAILURE Finalization failed
     */
    virtual StatusCode finalize() override;
  };

} // namespace Rich::Future::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecTupleAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction monitor
 *  algorithm base class : RichRecTupleAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecTupleAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::TupleAlgBase>;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::TupleAlgBase::TupleAlgBase( const std::string& name, //
                                               ISvcLocator*       pSvcLocator )
    : Rich::Future::TupleAlgBase( name, pSvcLocator ) //
    , Rich::Future::Rec::CommonBase<Rich::Future::TupleAlgBase>( this ) {}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::initialize() {
  // Initialise base class
  const auto sc = Rich::Future::TupleAlgBase::initialize();
  // Common initialisation
  return ( sc.isSuccess() ? initialiseRichReco() : sc );
}
// ============================================================================

// ============================================================================
// Main execute method
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::execute() {
  // All algorithms should re-implement this method
  error() << "Default Rich::RecTupleAlgBase::execute() called !!" << endmsg;
  return StatusCode::FAILURE;
}
// ============================================================================

// ============================================================================
// Finalize
// ============================================================================
StatusCode Rich::Future::Rec::TupleAlgBase::finalize() {
  // Common finalisation
  const auto sc = finaliseRichReco();
  // Finalize base class
  return ( sc.isSuccess() ? Rich::Future::TupleAlgBase::finalize() : sc );
}
// ============================================================================

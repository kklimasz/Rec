/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecHistoToolBase.cpp
 *
 *  Implementation file for RICH reconstruction tool base class : RichRecHistoToolBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005/01/13
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecHistoToolBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::HistoToolBase>;
// ============================================================================

// ============================================================================
// Standard constructor
// ============================================================================
Rich::Future::Rec::HistoToolBase::HistoToolBase( const std::string& type, //
                                                 const std::string& name, //
                                                 const IInterface*  parent )
    : Rich::Future::HistoToolBase( type, name, parent ) //
    , Rich::Future::Rec::CommonBase<Rich::Future::HistoToolBase>( this ) {}
// ============================================================================

// ============================================================================
// Initialise
// ============================================================================
StatusCode Rich::Future::Rec::HistoToolBase::initialize() {
  // Initialise base class
  auto sc = Rich::Future::HistoToolBase::initialize();
  // Common initialisation
  return ( sc.isSuccess() ? initialiseRichReco() : sc );
}
// ============================================================================

// ============================================================================
// Finalise
// ============================================================================
StatusCode Rich::Future::Rec::HistoToolBase::finalize() {
  // Common finalisation
  const auto sc = finaliseRichReco();
  // base class finalize
  return ( sc.isSuccess() ? Rich::Future::HistoToolBase::finalize() : sc );
}
// ============================================================================

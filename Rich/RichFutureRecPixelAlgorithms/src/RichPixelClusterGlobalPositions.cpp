/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "RichPixelClusterGlobalPositions.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelClusterGlobalPositions
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusterGlobalPositions::PixelClusterGlobalPositions( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default}},
                   {KeyValue{"RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal}} ) {
  // setProperty( "OutputLevel", MSG::DEBUG );
}

//=============================================================================

SpacePointVector PixelClusterGlobalPositions::operator()( const Rich::PDPixelCluster::Vector& clusters ) const {
  return m_idTool.get()->globalPositions( clusters, m_noClustering );
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusterGlobalPositions )

//=============================================================================

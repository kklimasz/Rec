/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// interfaces
#include "RichFutureInterfaces/IRichSmartIDTool.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelClusterGlobalPositions RichPixelClusterGlobalPositions.h
   *
   *  Computes the global space points for the given pixel clusters.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class PixelClusterGlobalPositions final
      : public Transformer<SpacePointVector( const Rich::PDPixelCluster::Vector& ), Traits::BaseClass_t<AlgBase>> {

  public:
    /// Standard constructor
    PixelClusterGlobalPositions( const std::string& name, ISvcLocator* pSvcLocator );

  public:
    /// Algorithm execution via transform
    SpacePointVector operator()( const Rich::PDPixelCluster::Vector& clusters ) const override;

  private:
    /// RichSmartID Tool
    ToolHandle<const ISmartIDTool> m_idTool{this, "SmartIDTool", "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC"};

    /// Shortcut incase clustering is disabled
    Gaudi::Property<bool> m_noClustering{this, "NoClustering", false};
  };

} // namespace Rich::Future::Rec

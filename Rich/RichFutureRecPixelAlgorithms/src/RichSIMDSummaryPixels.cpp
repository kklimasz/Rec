
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichSIMDSummaryPixels.h"

using namespace Rich::Future::Rec;

//=============================================================================

SIMDSummaryPixels::SIMDSummaryPixels( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   // input data
                   {KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default}},
                   // output data
                   {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default}} ) {
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

SIMDPixelSummaries SIMDSummaryPixels::operator()( const Rich::PDPixelCluster::Vector& clusters ) const {

  // Pixel Summaries
  SIMDPixelSummaries summaries;

  // Reserve size based on # pixels and SIMD address size.
  // Add 4 for padding in each RICH panel.
  summaries.reserve( ( clusters.size() / SIMDPixel::SIMDFP::Size ) + 4 );

  // last RICH
  Rich::DetectorType lastRich{Rich::InvalidDetector};
  // last side
  Rich::Side lastSide{Rich::InvalidSide};

  // Working SIMD values
  SIMDPixel::SIMDFP  gX( 0 ), gY( 0 ), gZ( 0 ); // global positions
  SIMDPixel::SIMDFP  effArea( 0 );              // effective area
  SIMDPixel::ScIndex scClusIn( -1 );            // indices to original clusters
  SIMDPixel::Mask    mask{false};               // selection mask

  // SIMD index
  std::uint32_t index = 0;

  // Working Smart IDs
  SIMDPixel::SmartIDs smartIDs;

  // Functor to set the OK mask for a reduced range based on index
  auto setMask = [&mask, &index]() { mask = SIMDPixel::SIMDFP::IndexesFromZero() < index; };

  // Functor to save a SIMD pixel
  auto savePix = [&]() {
    const auto gPos = SIMDPixel::Point( gX, gY, gZ );
    const auto lPos = m_idTool.get()->globalToPDPanel( lastRich, lastSide, gPos );
    summaries.emplace_back( lastRich, lastSide, smartIDs, //
                            gPos, lPos,                   //
                            effArea, scClusIn, mask );
    // reset
    index = 0;
  };

  // Functor to add padding info at the end of an incomplete SIMD pixel
  auto addPadding = [&]() {
    // area and position info
    const auto not_mask = !mask;
    effArea( not_mask ) = SIMDPixel::SIMDFP::Zero();
    gX( not_mask )      = SIMDPixel::SIMDFP::Zero();
    gY( not_mask )      = SIMDPixel::SIMDFP::Zero();
    // Set default z to values roughly right for each RICH, to
    // avoid precision issues in the photon reco later on.
    gZ( not_mask ) = SIMDPixel::SIMDFP( Rich::Rich1 == lastRich ? 1600 : 10500 );
    // cast mask for int types...
    const auto not_im  = LHCb::SIMD::simd_cast<SIMDPixel::ScIndex::mask_type>( not_mask );
    scClusIn( not_im ) = -SIMDPixel::ScIndex::One();
    // SmartIDs cannot be done with mask.
    GAUDI_LOOP_UNROLL( SIMDPixel::SIMDFP::Size )
    for ( auto i = index; i < SIMDPixel::SIMDFP::Size; ++i ) { smartIDs[i] = LHCb::RichSmartID(); }
  };

  // make the global positions from the clusters
  const auto gPoints = m_idTool.get()->globalPositions( clusters, m_noClustering );

  // Scalar cluster index
  std::size_t clusIn = 0;

  // Note relying on fact they are sorted by panel and rich here.
  for ( const auto&& [cluster, gloPos] : Ranges::ConstZip( clusters, gPoints ) ) {

    // which RICH and side
    const auto rich = cluster.rich();
    const auto side = cluster.panel();

    // If different RICH or side ?
    if ( UNLIKELY( rich != lastRich || side != lastSide ) ) {
      // Skip saving anything if index is 0.
      // Also covers not saving first time in loop.
      if ( 0 != index ) {
        // Set the selection mask
        setMask();
        // add padding if needed
        addPadding();
        // Save the current info
        savePix();
      }
      // update RICH and panel
      lastRich = rich;
      lastSide = side;
    }

    // Update global position
    gX[index] = gloPos.X();
    gY[index] = gloPos.Y();
    gZ[index] = gloPos.Z();
    // effective area * cluster size
    effArea[index] = cluster.size() * cluster.dePD()->effectivePixelArea();
    // (primary) SmartID
    smartIDs[index] = cluster.primaryID();
    // set scalar cluster index (then increment)
    scClusIn[index] = clusIn;
    ++clusIn;

    // If this is the last index, push to container and start again
    if ( UNLIKELY( SIMDPixel::SIMDFP::Size - 1 == index ) ) {
      // Set the selection mask as all OK
      mask = SIMDPixel::Mask( true );
      // save an entry. No need to handle padding here as SIMD data is full.
      savePix();
    } else {
      // increment index for next scalar pixel
      ++index;
    }
  }

  // Save last one if needed
  if ( 0 != index ) {
    // Set the selection mask
    setMask();
    // add padding if needed
    addPadding();
    // Save the current info
    savePix();
  }

  // Update pixel ranges.
  // Note we are storing indices not iterators as these remain valid
  // under container relocations. Conversion to ranges happens on access.

  // RICH1 top -> bottom boundary ( or start of RICH2 )
  auto itR1FirstBot = std::find_if( summaries.begin(), summaries.end(), []( const auto& p ) {
    return ( Rich::bottom == p.side() || Rich::Rich2 == p.rich() );
  } );
  // RICH1 -> RICH2 boundary
  auto itFirstR2 =
      std::find_if( itR1FirstBot, summaries.end(), []( const auto& p ) { return ( Rich::Rich2 == p.rich() ); } );
  // RICH2 left -> right boundary
  auto itR2FirstRight =
      std::find_if( itFirstR2, summaries.end(), []( const auto& p ) { return ( Rich::right == p.side() ); } );

  const std::size_t first        = 0;
  const std::size_t r2start      = itFirstR2 - summaries.begin();
  const std::size_t r1botstart   = itR1FirstBot - summaries.begin();
  const std::size_t r2rightstart = itR2FirstRight - summaries.begin();
  const std::size_t end          = summaries.end() - summaries.begin();

  summaries.setRange( Rich::Rich1, first, r2start );
  summaries.setRange( Rich::Rich2, r2start, end );

  summaries.setRange( Rich::Rich1, Rich::top, first, r1botstart );
  summaries.setRange( Rich::Rich1, Rich::bottom, r1botstart, r2start );

  summaries.setRange( Rich::Rich2, Rich::left, r2start, r2rightstart );
  summaries.setRange( Rich::Rich2, Rich::right, r2rightstart, end );

  // -------------------------------------------------------------------------------------------

  // Some verbose bugging printout
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {

    // explicitly count number in each region to compare to ranges
    DetectorArray<PanelArray<unsigned int>> count = {{}};
    for ( const auto& p : summaries ) { ++count[p.rich()][p.side()]; }

    std::size_t rangeSum = 0;
    bool        OK       = true;
    // Loop over each range and test against full container
    for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) {
      for ( const auto side : {Rich::firstSide, Rich::secondSide} ) {
        // get the range for this rich and side
        const auto rPixs = summaries.range( rich, side );
        for ( const auto& p : rPixs ) {
          // make sure pixel agrees with range
          const auto pixOK = ( rich == p.rich() && side == p.side() );
          OK &= pixOK;
        }
        // count sum of range sizes
        rangeSum += rPixs.size();
        // does range have same size as explicit count ?
        if ( count[rich][side] != rPixs.size() ) {
          OK = false;
          error() << "Problem with " << rich << " " << Rich::text( rich, side ) << " range. Explicit count "
                  << count[rich][side] << " != " << rPixs.size() << endmsg;
        }
      }
    }

    // test final size
    OK &= ( summaries.size() == rangeSum );
    if ( UNLIKELY( !OK || msgLevel( MSG::VERBOSE ) ) ) {
      verbose() << "Total SIMD pixels = " << summaries.size() << " == " << rangeSum << endmsg;

      verbose() << "R1       indices : " << first << " -> " << r2start << endmsg;
      verbose() << "R2       indices : " << r2start << " -> " << end << endmsg;

      verbose() << "R1 top   indices : " << first << " -> " << r1botstart << endmsg;
      verbose() << "R1 bot   indices : " << r1botstart << " -> " << r2start << endmsg;
      verbose() << "R2 left  indices : " << r2start << " -> " << r2rightstart << endmsg;
      verbose() << "R2 right indices : " << r2rightstart << " -> " << end << endmsg;

      for ( const auto& p : summaries ) { verbose() << " " << p << endmsg; }
    }
  }

  // -------------------------------------------------------------------------------------------

  // return
  return summaries;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDSummaryPixels )

//=============================================================================

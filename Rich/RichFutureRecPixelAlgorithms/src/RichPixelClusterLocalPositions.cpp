/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichPixelClusterLocalPositions.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelClusterLocalPositions
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusterLocalPositions::PixelClusterLocalPositions( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal}},
                   {KeyValue{"RichPixelLocalPositionsLocation", SpacePointLocation::PixelsLocal}} ) {
  // setProperty( "OutputLevel", MSG::DEBUG );
}

//=============================================================================

SpacePointVector PixelClusterLocalPositions::operator()( const SpacePointVector& gPoints ) const {
  // the container to return
  SpacePointVector lPoints;
  lPoints.reserve( gPoints.size() );

  for ( const auto& gPos : gPoints ) {
    lPoints.emplace_back( m_idTool.get()->globalToPDPanel( gPos ) );
    _ri_debug << gPos << " -> " << lPoints.back() << endmsg;
  }

  // return the final space points
  return lPoints;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusterLocalPositions )

//=============================================================================

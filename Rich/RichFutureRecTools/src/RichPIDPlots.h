/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichPIDPlots.h
 *
 *  Header file for RICH tool : Rich::Rec::PIDPlots
 *
 *  @author Chris Jones     Christopher.Rob.Jones@cern.ch
 *  @date   2008-04-14
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <sstream>

// base class
#include "RichFutureRecBase/RichRecHistoToolBase.h"

// interfaces
#include "RichFutureRecInterfaces/IRichPIDPlots.h"

// Event
#include "Event/ProtoParticle.h"
#include "Event/RichPID.h"
#include "Event/Track.h"

namespace Rich::Future::Rec {

  /** @class PIDPlots RichPIDPlots.h
   *
   *  Tool which creates standardised PID plots for
   *  monitoring and calibration purposes.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2008-04-14
   */

  class PIDPlots final : public HistoToolBase, virtual public IPIDPlots {

  public:
    /// Standard constructor
    PIDPlots( const std::string& type, const std::string& name, const IInterface* parent );

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  public:
    // Fill the plots for the given RichPID and PID hypothesis
    void plots( const LHCb::RichPID* pid, const Rich::ParticleIDType hypo ) const override;

    // Fill the plots for the given track and PID hypothesis
    void plots( const LHCb::Track* track, const Rich::ParticleIDType hypo ) const override;

    // Fill the plots for the given ProtoParticle and PID hypothesis
    void plots( const LHCb::ProtoParticle* proto, const Rich::ParticleIDType hypo ) const override;

  private:
    /// Get the track momentum (first State) in GeV
    inline double trackP( const LHCb::Track* track ) const noexcept { return ( track ? track->p() : 0 ); }

    /// Get the track momentum (first State) in GeV
    inline double trackP( const LHCb::RichPID* pid ) const noexcept { return ( pid ? trackP( pid->track() ) : 0 ); }

    /// Get the track momentum (first State) in GeV
    inline double trackPt( const LHCb::Track* track ) const noexcept { return ( track ? track->pt() : 0 ); }

    /// Get the track momentum (first State) in GeV
    inline double trackPt( const LHCb::RichPID* pid ) const noexcept { return ( pid ? trackPt( pid->track() ) : 0 ); }

    /// Track selection
    inline bool selected( const LHCb::Track* track ) const noexcept {
      return ( track && trackP( track ) > m_minP && trackP( track ) < m_maxP && trackPt( track ) > m_minPt &&
               trackPt( track ) < m_maxPt );
    }

  private:
    // data

    /// Dll cut value
    Gaudi::Property<double> m_dllCut{this, "DllCut", 0.0, "Cut value for DLL>cut efficiency plots"};

    /// Dll range for plots
    Gaudi::Property<double> m_dllRange{this, "DllPlotRange", 100.0, "DLL Range for plots"};

    /// The minimum track momentum (MeV/c)
    Gaudi::Property<double> m_minP{this, "MinP", 2 * Gaudi::Units::GeV, "Minimum track momentum"};

    /// The maximum track momentum (MeV/c)
    Gaudi::Property<double> m_maxP{this, "MaxP", 100 * Gaudi::Units::GeV, "Maximum track momentum"};

    /// The minimum track transverse (to z-axis) momentum (MeV/c)
    Gaudi::Property<double> m_minPt{this, "MinPt", 0 * Gaudi::Units::GeV, "Minimum track transverse momentum"};

    /// The maximum track transverse (to z-axis) momentum (MeV/c)
    Gaudi::Property<double> m_maxPt{this, "MaxPt", 10 * Gaudi::Units::GeV, "Maximum track transverse momentum"};

  private:
    // messaging

    mutable WarningCounter m_nullProto{this, "Null ProtoParticle pointer passed !"};
    mutable WarningCounter m_nullTrack{this, "ProtoParticle has null Track pointer !"};
  };

} // namespace Rich::Future::Rec

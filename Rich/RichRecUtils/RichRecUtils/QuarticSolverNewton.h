/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//----------------------------------------------------------------------
/** @file QuarticSolverNewton.h
 *
 *  @author Christina Quast, Rainer Schwemmer, Chris Jones
 *  @date   2017-02-03
 */
//----------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Point3DTypes.h"

// LHCbMath
#include "LHCbMath/SIMDTypes.h"

// STL
#include <array>
#include <math.h>
#include <type_traits>

namespace Rich::Rec {

  //-----------------------------------------------------------------------------
  /** @class QuarticSolverNewton
   *
   *  Utility class that implements the solving of the Quartic equation
   *  for the RICH
   *
   *  @author Christina Quast, Rainer Schwemmer, Chris Jones
   *  @date   2017-02-03
   */
  //-----------------------------------------------------------------------------
  class QuarticSolverNewton {

  private:
    // parameters

    /// order of the polynominal being solved.
    static constexpr std::size_t ORDER = 4;

    /// parameters for polynominal
    template <typename TYPE>
    using Params = std::array<TYPE, ORDER + 1>;

  public:
    /** Solves the characteristic quartic equation for the RICH optical system.
     *
     *  See note LHCB/98-040 RICH section 3 (equation 3) for more details
     *
     *  @param emissionPoint Assumed photon emission point on track
     *  @param CoC           Spherical mirror centre of curvature
     *  @param virtDetPoint  Virtual detection point
     *  @param radius        Spherical mirror radius of curvature
     *  @param sphReflPoint  The reconstructed reflection pont on the spherical mirror
     *
     *  @return boolean indicating status of the quartic solution
     *  @retval true  Calculation was successful. sphReflPoint is valid.
     *  @retval false Calculation failed. sphReflPoint is not valid.
     */
    template <typename TYPE,                             ///< basic floating point type
              std::size_t BISECTITS   = 2,               ///< number of bisect iterations
              std::size_t NEWTONITS   = 3,               ///< number of Newton-Rhapson iterations
              typename EMISSIONPOINT  = Gaudi::XYZPoint, ///< type for emission point
              typename COC            = Gaudi::XYZPoint, ///< type for mirror centre of curvature
              typename DETECTIONPOINT = Gaudi::XYZPoint, ///< type for detection point
              typename REFLECTPOINT   = Gaudi::XYZPoint  ///< type for reflection point
              >
    inline void __attribute__( ( always_inline ) ) //
    solve( const EMISSIONPOINT&  emissionPoint,    //
           const COC&            CoC,              //
           const DETECTIONPOINT& virtDetPoint,     //
           const TYPE            radius,           //
           REFLECTPOINT&         sphReflPoint ) const noexcept {

      using namespace std;
      using namespace LHCb::SIMD;
      static const TYPE one( 1.0f );

      // vector from mirror centre of curvature to assumed emission point
      const auto evec( emissionPoint - CoC );
      const TYPE e2 = evec.Dot( evec );

      // vector from mirror centre of curvature to virtual detection point
      const auto dvec( virtDetPoint - CoC );
      const TYPE d2 = dvec.Dot( dvec );

      // |e|^2 * |d|^2
      const auto ed2 = e2 * d2;

      // e.d dot product
      const auto eDotd = evec.Dot( dvec );

      // // Handle case e and/or d have zero size.
      // // This actually cannot happen in reality as requires input detection
      // // point and/or emission point to be exactly the same as the CoC,
      // // which never happens. So skip.
      // if constexpr ( all_arithmetic_v<TYPE> ) {
      //   // scalar
      //   if ( UNLIKELY( ed2 <= 0.0f ) ) { ed2 = eDotd = TYPE( 1.0f ); }
      // } else if constexpr ( all_SIMD_v<TYPE> ) {
      //   // SIMD
      //   const auto ed2mask = ( ed2 <= TYPE::Zero() );
      //   if ( UNLIKELY( any_of( ed2mask ) ) ) {
      //     eDotd( ed2mask ) = TYPE::One();
      //     ed2( ed2mask )   = TYPE::One();
      //   }
      // } else {
      //   // If get here cause compilation failure.
      //   TYPE::WillFail();
      // }

      const TYPE cosgamma2 = ( eDotd * eDotd ) / ed2;
      const TYPE singamma2 = one - cosgamma2;
      const TYPE dy2       = d2 * singamma2;
      const TYPE dy        = std::sqrt( dy2 );
      const TYPE dx        = std::sqrt( d2 * cosgamma2 );
      const TYPE e         = std::sqrt( e2 );
      const TYPE edx       = e + dx;
      const TYPE r2        = radius * radius;

      // Fill array for quartic equation
      // Newton solver doesn't care about a0 being not 1.0. Remove costly division and several
      // multiplies. This has some downsides though. The a-values are hovering around a numerical
      // value of 10^15. single precision float max is 10^37. A single square and some multiplies
      // will push it over the limit of what single precision float can handle. It's ok for the
      // newton method, but Halley or higher order Housholder will fail without this normalization.
      // const auto inv_a0  =   ( a0 > 0 ? 1.0 / a0 : std::numeric_limits<TYPE>::max() );
      static const TYPE  two( 2.0f ), four( 4.0f );
      const TYPE         dyrad2 = two * dy * radius;
      const Params<TYPE> aa     = {
          four * ed2,                                             //
          -( two * dyrad2 * e2 ),                                 //
          ( ( dy2 * r2 ) + ( edx * edx * r2 ) - ( four * ed2 ) ), //
          ( dyrad2 * e * ( e - dx ) ),                            //
          ( ( e2 - r2 ) * dy2 )                                   //
      };

      // Use optimized newton solver on quartic equation.
      const TYPE sinbeta = solve_quartic_newton_RICH<TYPE, BISECTITS, NEWTONITS>( aa );

      // construct rotation transformation
      // Set vector magnitude to radius
      // rotate vector and update reflection point
      // rotation matrix uses sin(beta) and cos(beta) to perform rotation
      // even fast_asinf (which is only single precision and defeats the purpose
      // of this class being templatable to double btw) is still too slow
      // plus there is a cos and sin call inside AngleAxis ...
      // We can do much better by just using the cos(beta) we already have to calculate
      // sin(beta) and do our own rotation. On top of that we rotate non-normalized and save several
      // Divisions by normalizing only once at the very end.
      // Again, care has to be taken since we are close to float_max here without immediate
      // normalization. As far as we have tried with extreme values in the rich coordinate systems
      // this is fine.

      const TYPE nx  = ( evec.Y() * dvec.Z() ) - ( evec.Z() * dvec.Y() );
      const TYPE ny  = ( evec.Z() * dvec.X() ) - ( evec.X() * dvec.Z() );
      const TYPE nz  = ( evec.X() * dvec.Y() ) - ( evec.Y() * dvec.X() );
      const TYPE nx2 = nx * nx;
      const TYPE ny2 = ny * ny;
      const TYPE nz2 = nz * nz;
      const TYPE n2  = nx2 + ny2 + nz2;

      const TYPE enorm = radius / ( e * n2 );

      const TYPE a = sinbeta * std::sqrt( n2 );
      const TYPE b = ( one - std::sqrt( one - ( sinbeta * sinbeta ) ) );

      const TYPE bnxny = b * nx * ny;
      const TYPE bnxnz = b * nx * nz;
      const TYPE bnynz = b * ny * nz;

      // non-normalized rotation matrix
      const std::array<TYPE, 9> M = {
          n2 - b * ( nz2 + ny2 ), ( a * nz ) + bnxny,     ( -a * ny ) + bnxnz,       //
          ( -a * nz ) + bnxny,    n2 - b * ( nx2 + nz2 ), ( a * nx ) + bnynz,        //
          ( a * ny ) + bnxnz,     ( -a * nx ) + bnynz,    n2 - ( b * ( ny2 + nx2 ) ) //
      };

      // re-normalize rotation and scale to radius in one step
      const TYPE ex = enorm * ( evec.X() * M[0] + evec.Y() * M[3] + evec.Z() * M[6] );
      const TYPE ey = enorm * ( evec.X() * M[1] + evec.Y() * M[4] + evec.Z() * M[7] );
      const TYPE ez = enorm * ( evec.X() * M[2] + evec.Y() * M[5] + evec.Z() * M[8] );

      // set the final reflection point
      sphReflPoint = {CoC.X() + ex, CoC.Y() + ey, CoC.Z() + ez};
    }

  private:
    // A newton iteration solver for the Rich quartic equation
    // Since the polynomial that is evaluated here is extremely constrained
    // (root is in small interval, one root guaranteed), we can use a much more
    // efficient approximation (which still has the same precision) instead of the
    // full blown mathematically absolute correct method and still end up with
    // usable results

    /** Horner's method to evaluate the polynomial and its derivatives with as little
     *  math operations as possible. We use a template here to allow the compiler to
     *  unroll the for loops and produce code that is free from branches and optimized
     *  for the grade of polynomial and derivatives as necessary.
     */
    template <typename TYPE, std::size_t DIFFGRADE>
    inline decltype( auto ) __attribute__( ( always_inline ) ) //
    evalPolyHorner( const Params<TYPE>& a,                     //
                    const TYPE&         x                      //
                    ) const noexcept {
      std::array<TYPE, DIFFGRADE + 1> res{{a[0]}};
      GAUDI_LOOP_UNROLL( ORDER )
      for ( std::size_t j = 1; j <= ORDER; ++j ) {
        res[0]       = ( res[0] * x ) + a[j];
        const auto l = std::min( ORDER - j, DIFFGRADE );
        for ( std::size_t i = 1; i <= l; ++i ) { res[i] = ( res[i] * x ) + res[i - 1]; }
      }
      if constexpr ( DIFFGRADE > 1 ) {
        TYPE l( 1.0f );
        for ( std::size_t i = 2; i <= DIFFGRADE; ++i ) {
          l *= TYPE( i );
          res[i] *= l;
        }
      }
      return res;
    }

    /// (for order 4) a[0]x^4 + a[1]x^3 + a[2]x^2 + a[3]x + a[4]
    template <typename TYPE>
    inline TYPE __attribute__( ( always_inline ) ) //
    f4( const Params<TYPE>& a, const TYPE& x ) const noexcept {
      TYPE res = a[0];
      GAUDI_LOOP_UNROLL( ORDER )
      for ( std::size_t i = 1; i <= ORDER; ++i ) { res = ( res * x ) + a[i]; }
      return res;
    }

    /** Newton-Rhapson method for calculating the root of the rich polynomial.
     *  It uses the bisection method in the beginning to get close enough to the root to
     *  allow the second stage newton method to converge faster. After 4 iterations of
     *  newton precision is as good as single precision floating point will get you.
     *  We have introduced a few tuning parameters like the newton gain factor and a
     *  slightly skewed bisection division, which in this particular case help to speed
     *  things up.
     *  TODO: These tuning parameters have been found by low effort experimentation on
     *  random input data. A more detailed study should be done with real data to find
     *  the best values.
     */
    template <typename TYPE, std::size_t BISECTITS, std::size_t NEWTONITS>
    inline TYPE __attribute__( ( always_inline ) ) //
    solve_quartic_newton_RICH( const Params<TYPE>& a ) const noexcept {

      using namespace LHCb::SIMD;

      // We start a bit off center since the distribution of roots tends to be more
      // to the left side
      TYPE m( 0.2f );

      // Do the bisest loops.
      // Use N steps of bisection method to find starting point for newton.
      if constexpr ( BISECTITS > 0 ) {
        TYPE l( 0.0f ), u( 0.5f );
        for ( std::size_t i = 0; i < BISECTITS; ++i ) {
          // get sign comparison mask. true if opposite sign.
          const auto oSign = ( isnegative( f4( a, m ) ) ^ isnegative( f4( a, l ) ) );
          // Most likely by far is all values have opposite sign
          if ( LIKELY( all_of( oSign ) ) ) {
            // equivalent to below when all values in mask are true.
            u = m;
          } else {
            // we have a mixture, so need to handle this with iif masked write
            l = iif( oSign, l, m );
            u = iif( oSign, m, u );
          }
          // 0.4 instead of 0.5 to speed up convergence.
          // Most roots seem to be closer to 0 than to the extreme end
          m = ( u + l ) * TYPE( 0.4f );
        }
      }

      // Most of the times we are approaching the root of the polynomial from one side
      // and fall short by a certain fraction. This fraction seems to be around 1.04 of
      // the quotient which is subtracted from x. By scaling it up, we take bigger steps
      // towards the root and thus converge faster.
      // TODO: study this factor more closely it's pure guesswork right now. We might
      // get away with 3 iterations if we can find an exact value.
      for ( std::size_t i = 0; i < NEWTONITS; ++i ) {
        const auto        res = evalPolyHorner<TYPE, 1>( a, m );
        static const TYPE gain( 1.04f ); // static seems to give best Vc performance here..
        m -= gain * ( res[0] / res[1] );
      }

      // return the result
      return m;
    }
  };

} // namespace Rich::Rec

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <cstdint>
#include <string>
#include <vector>

// ROOT includes
#include <TF1.h>
#include <TH1.h>

// LHCb Kernel
#include "Kernel/RichRadiatorType.h"

namespace Rich::Rec {
  /** @class CKResolutionFitter RichRecUtils/RichCKResolutionFitter.h
   *
   *  Utility class to perform a fit to the Cherenkov resolution plots.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2017-08-02
   */
  class CKResolutionFitter final {

  public:
    /// Fitter Parameters
    class FitParams final {

    private:
      /// Number of Radiators
      static const std::uint8_t NRads = 2;

    public: // General fitting parameters
      /// Type for list for fit types for each radiator
      using RichFitTypes_t = std::array<std::vector<std::string>, NRads>;

      //                                               RICH1                RICH2
      /// Starting resolution guesses
      std::array<double, NRads> RichStartRes{{0.0017, 0.0007}};
      /// Starting prefit deltas
      std::array<double, NRads> RichStartDelta{{0.0025, 0.00105}};
      /// Fit Range Minimums
      std::array<double, NRads> RichFitMin{{-0.0079, -0.00395}};
      /// Fit range Maxmimums
      std::array<double, NRads> RichFitMax{{0.0077, 0.00365}};
      /// Default Gauss Asymmetry
      std::array<double, NRads> RichStartAsym{{0.0000, 0.0000}};
      /// Fit Type(s)
      RichFitTypes_t RichFitTypes{
          {{"AsymNormal:Hyperbolic", "AsymNormal:FreeNPol"}, {"AsymNormal:Hyperbolic", "AsymNormal:FreeNPol"}}};

    public: // Free polynominal background fit options
      /// Polynominal degree
      std::array<unsigned int, NRads> RichNPol{{3, 3}};

    public: // Fixed polynominal background function
      /// polynominal Background forms
      inline std::string RichFixedNPolFunc( const Rich::RadiatorType rad, const unsigned int iFirstParam ) const {
        const auto s = std::to_string( iFirstParam );
        return ( Rich::Rich1Gas == rad ? "(1.977+[" + s + "]*x*1.701e1-x*x*2.249e3+x*x*x*9.226e3)"
                                       : "(1.072+[" + s + "]*x*1.618e1-x*x*8.240e3-x*x*x*3.240e5)" );
      }

    private:
      // polynominal function
      inline std::string Pol( const int degree, unsigned int iFirstParam ) const noexcept {
        return ( "auto pol = [](double *x, double *p) -> double "
                 "{ "
                 " const unsigned int iFP = " +
                 std::to_string( iFirstParam ) +
                 "; "
                 " const unsigned int deg = " +
                 std::to_string( degree ) +
                 "; "
                 " auto ret = p[iFP+deg]; "
                 " for ( int i = deg-1; i >= 0; --i ) { ret = (ret*x[0]) + p[iFP+i]; }; "
                 " return ret;"
                 "}; " );
      }

      // hyperbolic fnction
      inline std::string Hyperbolic( const unsigned int iFirstParam ) const noexcept {
        return ( "auto hyperbol = [](double *x, double *p) -> double "
                 "{ "
                 // The fit parameters
                 " const double A  = p[" +
                 std::to_string( iFirstParam ) +
                 "]; "
                 " const double B  = p[" +
                 std::to_string( iFirstParam + 1 ) +
                 "]; "
                 " const double Cm = p[" +
                 std::to_string( iFirstParam + 2 ) +
                 "]; "
                 " const double Cp = p[" +
                 std::to_string( iFirstParam + 3 ) +
                 "]; "
                 " const double D  = p[" +
                 std::to_string( iFirstParam + 4 ) +
                 "]; "
                 // Which C
                 " const double C = ( x[0] < D ? Cm : Cp ); "
                 // the function
                 " return ( A - ( std::sqrt( B + C*std::pow(x[0]-D,2) ) ) ); "
                 "}; " );
      }

      // Asymmetric Normal function
      inline std::string AsymNormal( const unsigned int iFirstParam ) const noexcept {
        return ( "auto normal = [](double *x, double *p) -> double "
                 "{ "
                 // The fit parameters
                 " const double N      =      p[" +
                 std::to_string( iFirstParam ) +
                 "]; "
                 " const double mean   =      p[" +
                 std::to_string( iFirstParam + 1 ) +
                 "]; "
                 " const double sigma  = fabs(p[" +
                 std::to_string( iFirstParam + 2 ) +
                 "]); "
                 " const double alpha  =      p[" +
                 std::to_string( iFirstParam + 3 ) +
                 "]; "
                 " const double sigmaa = sigma * ( 1.0 + ( 0.5 * alpha ) ); "
                 " const double sigmab = sigma * ( 1.0 - ( 0.5 * alpha ) ); "
                 " const double s      = ( x[0] > mean ? sigmaa : sigmab ); "
                 // the function
                 " return ( s > 0 ? N * std::exp( -0.5 * std::pow((x[0]-mean)/s,2) ) : 0 ); "
                 "}; " );
      }

      // Normal function
      inline std::string Normal( const unsigned int iFirstParam ) const noexcept {
        return ( "auto normal = [](double *x, double *p) -> double "
                 "{ "
                 // The fit parameters
                 " const double N     =      p[" +
                 std::to_string( iFirstParam ) +
                 "]; "
                 " const double mean  =      p[" +
                 std::to_string( iFirstParam + 1 ) +
                 "]; "
                 " const double sigma = fabs(p[" +
                 std::to_string( iFirstParam + 2 ) +
                 "]); "
                 // the function
                 " return ( sigma > 0 ? N * std::exp( -0.5 * std::pow((x[0]-mean)/sigma,2) ) : 0 ); "
                 "}; " );
      }

      // Skewed Normal function
      inline std::string SkewedNormal( const unsigned int iFirstParam ) const noexcept {
        return ( "auto normal = [](double *x, double *p) -> double "
                 "{ "
                 // The fit parameters
                 " const double N     =      p[" +
                 std::to_string( iFirstParam ) +
                 "]; "
                 " const double mean  =      p[" +
                 std::to_string( iFirstParam + 1 ) +
                 "]; "
                 " const double sigma = fabs(p[" +
                 std::to_string( iFirstParam + 2 ) +
                 "]); "
                 " const double alpha =      p[" +
                 std::to_string( iFirstParam + 3 ) +
                 "]; "
                 // convert fit parameters to function parameters
                 " const double pi    = 3.14159265358979; "
                 " const double delta = alpha / std::sqrt( 1.0 + std::pow(alpha,2) ); "
                 " const double w     = sigma / std::sqrt( 1.0 - (2.0*std::pow(delta,2)/pi) ); "
                 " const double eta   = mean - (w*delta*std::sqrt(2.0/pi)); "
                 " const double xx    = ( x[0] - eta ) / w; "
                 // the function
                 " return N * std::exp(-0.5*xx*xx) * ( 1.0 + std::erf(alpha*xx/std::sqrt(2.0)) ); "
                 "}; " );
      }

    public: // Hyperbolic functions
      //                                             RICH1       RICH2
      /// Side band sizes
      std::array<double, NRads> SideBandSize{{0.002, 0.001}};
      /// Init values for parameter B
      std::array<double, NRads> HyperInitB{{1e8, 1e8}};
      /// Init values for parameter D
      std::array<double, NRads> HyperInitD{{7e-4, 5e-4}};

      /// lambda function for signal + background
      inline std::string NormalHyperbolFitFunc( const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + Normal( iFirstParam ) + Hyperbolic( iFirstParam + 3 ) +
                 "return hyperbol(x,p) + normal(x,p); }" );
      }

      /// lambda function for signal + background
      inline std::string AsymNormalHyperbolFitFunc( const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + AsymNormal( iFirstParam ) + Hyperbolic( iFirstParam + 4 ) +
                 "return hyperbol(x,p) + normal(x,p); }" );
      }

      /// lambda function for signal + background
      inline std::string NormalPolFitFunc( const int degree, const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + Normal( iFirstParam ) + Pol( degree, iFirstParam + 3 ) +
                 "return pol(x,p) + normal(x,p); }" );
      }

      /// lambda function for signal + background
      inline std::string AsymNormalPolFitFunc( const int degree, const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + AsymNormal( iFirstParam ) + Pol( degree, iFirstParam + 4 ) +
                 "return pol(x,p) + normal(x,p); }" );
      }

      /// lambda function for hyperbol background only
      inline std::string HyperbolFitFunc( const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + Hyperbolic( iFirstParam ) + "return hyperbol(x,p); }" );
      }

      /// lambda function for pol background only
      inline std::string PolFitFunc( const int degree, const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + Pol( degree, iFirstParam ) + "return pol(x,p); }" );
      }

      /// lambda function for skewed normal signal
      inline std::string SkewedNormalFitFunc( const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + SkewedNormal( iFirstParam ) + "return normal(x,p); }" );
      }

      /// lambda function for skewed normal signal + polynominal background
      inline std::string SkewedNormalPolFitFunc( const unsigned int degree, const unsigned int iFirstParam = 0 ) const
          noexcept {
        return ( "[&](double *x, double *p) { " + SkewedNormal( iFirstParam ) + Pol( degree, iFirstParam + 4 ) +
                 "return pol(x,p) + normal(x,p); }" );
      }

      /// lambda function for skewed normal signal + hyperbolic background
      inline std::string SkewedNormalHyperbolFitFunc( const unsigned int iFirstParam = 0 ) const noexcept {
        return ( "[&](double *x, double *p) { " + SkewedNormal( iFirstParam ) + Hyperbolic( iFirstParam + 4 ) +
                 "return hyperbol(x,p) + normal(x,p); }" );
      }

    public: // Fit Quality options
      /// Maximum abs fitted mean for OK fit
      std::array<double, NRads> RichMaxMean{{0.006, 0.003}};
      /// Maximum abs fitted sigma for OK fit
      std::array<double, NRads> RichMaxSigma{{0.010, 0.005}};
      /// Maximum shift error for OK fit
      std::array<double, NRads> MaxShiftError{{0.001, 0.001}};
      /// Maximum resolution error for OK fit
      std::array<double, NRads> MaxSigmaError{{0.0002, 0.0001}};
    };

    /// Class to store the result of a histogram fit
    class FitResult final {
    public:
      /// Overall fit status
      bool fitOK{false};
      /// CK shift
      double ckShift{0};
      /// CK shift error
      double ckShiftErr{9999};
      /// CK resolution
      double ckResolution{0};
      /// CK resolution
      double ckResolutionErr{9999};
      /// The minimum of the used fit range
      double fitMin{0};
      /// The maximum of the used fit range
      double fitMax{0};
      /// The overall fitted function
      TF1 overallFitFunc;
      /// The fitted background function
      TF1 bkgFitFunc;
    };

  public:
    /// Default Constructor
    CKResolutionFitter();

    /// Constructor with a given set of parameters
    CKResolutionFitter( const FitParams& prms ) : m_params( prms ) {}

  public:
    /// Perform a fit to the given TH1 histogram
    FitResult fit( TH1& hist, const Rich::RadiatorType rad ) const;

    /// Write access to the fit parameter object
    FitParams& params() noexcept { return m_params; }

    /// Read access to the fit parameter object
    const FitParams& params() const noexcept { return m_params; }

  private:
    // Run a given fit type
    FitResult fitImp( TH1& hist, const Rich::RadiatorType rad, const std::string& fitType ) const;

    /// Check histogram fit result
    bool fitIsOK( const TF1& fFitF, const Rich::RadiatorType rad ) const;

    /// check fit status
    template <typename RESULT>
    inline bool fitIsValid( const RESULT& fitR ) const {
      return ( fitR.Get() ? fitR->IsValid() : false );
    }

    /// Get the RICH array index from enum
    inline std::uint8_t rIndex( const Rich::RadiatorType rad ) const noexcept {
      return ( Rich::Rich1Gas == rad ? 0 : 1 );
    }

  private:
    /// Parameters
    FitParams m_params;
  };

} // namespace Rich::Rec

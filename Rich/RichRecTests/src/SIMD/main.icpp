/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <iostream>
#include <random>
#include <string>
#include <typeinfo>
#include <vector>

// Rich Utils
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/ZipRange.h"

// SIMD types
#include "RichUtils/RichSIMDTypes.h"

// maths
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/Truncate.h"

#include "../Common.h"

template <typename FTYPE, typename ITYPE>
class Data {
public:
  using Vector = std::vector<Data, Vc::Allocator<Data>>;

  using SIMDFP  = LHCb::SIMD::FP<FTYPE>;
  using SIMDINT = LHCb::SIMD::INT<ITYPE>;
  using MASK    = typename SIMDFP::mask_type;
  using SCALAR  = typename SIMDFP::value_type;

public:
  SCALAR  s, t;
  SIMDFP  a, b, c, d;
  MASK    m;
  SIMDINT i;

public:
  Data() {
    // randomn generator
    static std::default_random_engine            gen;
    static std::uniform_real_distribution<float> x( -1, 1 );
    s = x( gen );
    t = x( gen );
    for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
      a[i] = x( gen );
      b[i] = x( gen );
      c[i] = std::max( 0.01f, abs( x( gen ) ) );
      d[i] = std::max( 0.01f, abs( x( gen ) ) );
    }
    m = MASK( false );
  }
};

template <typename DATA>
void __attribute__( ( noinline ) ) run_test( DATA& dataV ) {
  for ( auto& data : dataV ) {

    using namespace LHCb::Math;

    GAUDI_LOOP_UNROLL( LHCb::SIMD::FPF::Size )
    for ( std::size_t i = 0; i < decltype( data.a )::Size; ++i ) {
      if ( data.a[i] > 0 ) { data.b[i] = data.a[i]; }
    }

    // data.s = truncate<PrecisionMode::Absolute, 4>( data.s );
    // std::cout << data.s << " truncates to " << data.t << std::endl;

    // data.b = truncate<PrecisionMode::Absolute, 4>( data.b );
    // std::cout << data.b << " truncates to " << data.a << std::endl;

    // data.d = (data.a*data.b) + data.c; // FMA OK

    // data.d = (data.a/data.b); // divide OK

    // data.d = std::sqrt( data.a ); // sqrt VE slower
    // data.d = data.a * rsqrt( data.a );

    // data.m = ( data.a < data.b );

    // data.d( data.a < data.b ) = data.c;

    // data.d = LHCb::Math::fast_atan2( data.a, data.b );
    // data.d = LHCb::Math::Approx::vapprox_atan2( data.a, data.b );
  }
}

template <typename DATA>
unsigned long long int __attribute__( ( noinline ) ) test( DATA& dataV ) {
  timespec               start, end;
  unsigned long long int best_dur{99999999999999999};
  const unsigned int     nTests = 50000;
  for ( unsigned int i = 0; i < nTests; ++i ) {
    clock_gettime( CLOCK_MONOTONIC_RAW, &start );
    run_test( dataV );
    clock_gettime( CLOCK_MONOTONIC_RAW, &end );
    const auto duration = time_diff( &start, &end );
    if ( duration < best_dur ) { best_dur = duration; }
  }
  return best_dur;
}

int main( int /*argc*/, char** /*argv*/ ) {

  // test CPU supports compilation level
  if ( !checkCPU() ) { return 77; }

  // create data
  const std::size_t nData = 48e2;
  std::cout << "Creating " << nData << " data values ..." << std::endl;
  using DATA = Data<float, unsigned int>;
  DATA::Vector data( nData );

  auto fTime = test( data );

  std::cout << " Time " << fTime << std::endl;

  return 0;
}

2017-09-06 RecSys v21r6p1
---
This version uses Gaudi v28r2, LHCb v42r6p1 and Lbcom v20r6p1 (and LCG_88 with ROOT 6.08.06)

This version is released on `2017-patches` branch. 


## Bug fixes

**[MR !707] Fix MuonID compilation warnings in clang and gcc7**  

**[MR !738] RichFutureRecPixelAlgorithms - Add out of bounds checks**  
Add some additional checks to make sure no out-of-bounds read access to vectors are performed in PixelBackgroundsEstiAvHPD, related to MR LHCb!873  



2019-02-14 Rec v30r3
===

This version uses Lbcom v30r3, LHCb v50r3, Gaudi v31r0 and LCG_95 with ROOT 6.16.00
<p>
This version is released on `master` branch.

Built relative to Rec v30r2, with the following changes:

### New features

- Add forward compatibility functions for Track V1, !1362 (@graven)   
  - Provide freestanding functions for `LHCb::Event::v1::Track` which can be found by ADL, and provide forward compatible ways of eg. getting `Measurements`, and which provide backwards compatible `closestState` functionality.  
  - Use the above `closestState` in VeloExpectation, MuonTrackAlignMonitor, and TrackCaloMatchMonitor  

- Primary vertex dumper for Allen, !1351 (@freiss)   
  * Add PrimaryVertexDumper, similar to TrackerDumper, to write out MC PV information, to be used in Allen project  
  * Writes out number of MC particles and x,y,z position for each MC PV  

- Add "Extreme' flag to "drop" modules [0,1,....13] in Velo clustering and Velo Tracking, !1335 (@rquaglia)   

- Added new PV algorithm TrackBeamLineVertexFinder, !1269 (@wouter)   
  New implementation of a fast PV finder. 
  For documentation, see https://gitlab.cern.ch/wouter/pvfinder  

- Added FT geometry dumper for GPU project, !1386 (@lfunke)   

- Added options script for JPsiMuMu events for GPU input, !1381 (@dovombru)   


### Enhancements

- Small improvements in PrPixelTracking boundary search windows finding, !1395 (@rquaglia)   
  According to the flamegraph lower/upper bound calls was taking 10% of the time in Velo tracking.   

- Update the input to the MuonMatchVeloUT algorithm, !1388 (@cprouve)   
  - change the VeloUT track input from vector to selection to be in line with other tracking algorithms  
  - change names of KeyValues to me more intuitive  
  - cosmetic changes files in MuonMatch/MuonMatch to be more intuitive and readable  
  - change to TrackSys/RecoUpgradeTracking.py to set up the MuonMatch sequence with the correct input for MuonMatchVeloUT  
  - add script to generate n-tuples (with MC-matching information for the tracks)  
  - add script to do studies for the VeloUT-Muon matching algorithm

- Remove the use of the high-threshold bit in PrLongLivedTracking, !1384 (@decianm)   
  Small drop in efficiency, as the value in the MVA has to be set constant. Preparation for removal of highThreshold member in UTHit.  

- Remove check for high-threshold hits in VeloUT, !1383 (@decianm)   

- Keep Measurements 'by value' inside TrackFitResult, Keep Trajectories 'by value' inside Measurement, !1380 (@graven)   

- Apply IP cut in a separate algorithm, !1369 (@sponce)   

- Follow lhcb/LHCb!1698, !1366 (@graven)   
  DeSTSector: use BrokenLineTrajectory instead of PiecewiseTrajectory  

- One measurement to rule them all, !1361 (@graven)   
  Replace the polymorphic `Measurement` and its implementations with a single, concrete `Measurement` class  

- Use C99 (and C++20) designated initializers to avoid default constructing and subsequent overwriting, !1360 (@graven)   
  
- Add [temporary] `Pr::FilterIP` algorithm to avoid using LoKi IP cuts, !1345 (@olupton)   
  - Recover speed lost when applying IP cuts in `Pr::Filter`  
  - Uses IP cut implementation that was removed in 5851e4bf9cea51c660c579497c62269ebff51708.

- Improve IMeasurementProvider interface, !1343 (@graven)   
  * remove 'measurement' call which forces, for each measurement, a seperate lookup in the event store, followed by a (linear!) search in the cluster/hit container for the corresponding hit/cluster  
  * update users of the IMeasurementProvider interface  
  * update implementations of the IMeasurementProvider interface, reducing the number of lookups to one per type of hit per track  

- BayesianDNN (x4 speed-up of DNN), !1342 (@aryzhiko)   
  
- Fix and Improve VSPClus, !1333 (@ahennequ)   
  *  Simplification of the linkV Lookup Table (65536 -> 256 ways)  
  *  Fix a bug in the left scan that was causing out of bounds accesses (scan was in the wrong way)  
  *  Add UP and DOWN checks in the scans to improve the results  
  VSPClus's result is now closer to the reference VPClus algorithm.

- Reduce (temporary) memory usage in PrepareMuonHits, !1326 (@graven)   

- Bypassing measurement providers for fitting, !1311 (@pkardos)   
  Transfers hit positions, fiber directions and measurement errors straight from pattern recognition algorithms to the parametrized fitter via additional members in the track. Bypassing measurement providers greatly increases performance for the best physics case, additional data members in track reduce performance in all cases, to varying degrees.

- Use selections in tracking algorithms, !1291 (@olupton)   
  Modify tracking algorithms:  
  - Both `PrVeloUT` and `PrForwardTracking` now expect `Pr::Selection<LHCb::Event::v2::Track>`.  
  - `PrVeloUT` no longer takes a container of PVs or applies IP cuts, as these are done in a `Pr::Filter` instance instead.  
    
  Modify `v2::LoKi`:  
  - Change `namespace { struct _Instantiations { ... }; }` in `Rec/LoKiTrack_v2/LoKi_v2/LoKiTrack_dct.h` as it seems to clash with non-`v2::LoKi` in some situations.  
  - Make `v2::LoKi` functors expect containers of `v2::RecVertex`.  
    
  Modify python configuration:  
  - Insert `Pr::MakeSelection<T>` and `Pr::Filter<T>` before `PrVeloUT` in `RecoUpgradeTracking`, configure the filter to apply IP cuts.  
  - Insert `Pr::MakeSelection<T>` before `PrForwardTracking` in `RecoUpgradeTracking`.  


### Thread safety

- Made UpgradeGhostId thread safe, !1377 (@sponce)   

- Remove unnecessary mutable, !1370 (@graven)   
  
- Made TMVAs used in MiniBrunel thread safe, !1365 (@sponce)   
  Fixes part of the issues of the minibrunel.hive-multi-thread described in Brunel!609


### Bug fixes

- Fixed wrong order of checks in PrPixelTracking, !1407 (@sponce)   

- PrForwardTracking: fix a bug which may cause segmentation fault, !1393 (@jihu)   
  Physics performance not affected.

- Fixes to PrimaryVertexChecker, !1382 (@freiss) (@adudziak)   
  *  number of close and isolated MC PVs now add up to the total number of MC PVs  
  *  don't skip anymore events where no PVs are reconstructed  

- Fix bug in VeloExpectation::zBeamLine due to shadowing, !1363 (@graven)   

- PrForwardTracking - Fix ubsan error due to null pointer dereferencing, !1348 (@jonrob)   

- Fix usage of uninitialized member in PrHit constructor, !1341 (@raaij)   

- No longer set PatQuality in TrackV2 in PrForwardTracking, !1330 (@jihu)   
  This change introduces a slight drop in ghost rate and a 0.03% loss in efficiency for `FromB P > 5GeV Pt > 500MeV` tracks. The cause for this diff is that previously UT IDs were "accidentally" used for calculating overlap between tracks, while in this newer version, only FT IDs are used  

### Code modernisations and cleanups

- Remove PrChecker (replaced by PrChecker2), !1408 (@rangel)   

- RICH Misc. Updates, !1404 (@jonrob)

- PrChecker2.h: Change isNoTT to isNotTT, !1403 (@mengzhen)   

- remove throw from PrLHCbID2MCParticle.cpp, !1399 (@nnolte)   

- reduce the usage of Measurement::type(), !1396 (@graven)   

- follow changes in lhcb/LHCb!1701, !1390 (@graven)   
  Fix dangerous DeVP::sensor interface  

- Remove highThreshold bit from PrTrack(er)Dumpers, !1389 (@decianm)   

- Remove use of highThreshold bit in PrDebugUTTruthTool, !1385 (@decianm)   

- PrPlaneCounter: make counting single-hit planes explicit, !1379 (@jihu)   
  
- Minor changes to help formatting, !1372 (@clemenci)   

- Remove unused TrackCheckerNT, !1364 (@graven)   

- Migrate LHCb::TrackFitResult,Measurement,Node from LHCb to Rec, !1359 (@graven)   

- streamline PVDumper, !1355 (@graven)   
  
- RICH - Fix compilation with range v3 0.4, !1352 (@jonrob)   

- Avoid ubsan errors in ParameterizedKalmanFit due to excessively large data members, !1350 (@jonrob)   

- Fix missing includes in header files, !1347 (@sponce)   

- Changed dx_t_covXtX to dz_t_covXTx to match definition., !1340 (@ldufour)   

- change ICaloFutureClusterTool,ICaloFutureSubClusterTag, and ICaloFutureHypoTool IFs to accept ref intstead of ptr, !1337 (@dgolubko)   
  Addresses #42   

- Minor changes to the titles of some RICH histograms to avoid the use of '|', !1336 (@jonrob)   

- Follow removal of unused Track V2 data members, !1317 (@pkardos)   
  Addresses LHCb/issues#16

- Prepare for new JobOptionsSvc, !1234 (@clemenci)   
  backward compatible changes to prepare for gaudi/Gaudi!577


### Monitoring changes

- add the label for PairProduction, HadronicInteraction and isDecay in PrChecker2, !1394 (@mengzhen)   

- Converted all counters of TrackBestTrackCreator to new counters and added one for ghostProbability, !1371 (@sponce)   
  The new counter on ghostProbability allows to see discrepancies that then change the Rich output (see discussion in Brunel!609 and fixes in !1365)  

- Added counters in PrMatchNN as a rough check that everything is ok, !1367 (@sponce)   

- Add counters for muonMVA{1,2}, !1332 (@nkazeev)   


### Changes to tests

- Update references to follow gaudi/Gaudi!703, !1349 (@cattanem)   

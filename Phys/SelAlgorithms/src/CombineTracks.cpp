/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Functors/Function.h"
#include "Functors/IFactory.h"
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrSelection.h"
#include "SelKernel/TrackCombination.h"
#include "SelKernel/TrackZips.h"
#include "TrackKernel/TrackCompactVertex.h"
#include "TrackKernel/TrackNProngVertex.h"
#include <TFile.h>
#include <TTree.h>
#include <tuple>

namespace {
  using FPtype = double;

  template <typename T, std::size_t N>
  struct TypeHelper {};

  template <std::size_t N>
  struct TypeHelper<LHCb::Event::v1::Track, N> {
    constexpr static auto ExtraChildCutHeaders       = {"Event/Track_v1.h"};
    constexpr static auto ExtraVertexCutHeaders      = {"TrackKernel/TrackCompactVertex.h"};
    constexpr static auto ExtraCombinationCutHeaders = {"Event/Track_v1.h", "SelKernel/TrackCombination.h"};
    using InputType                                  = Pr::Selection<LHCb::Event::v1::Track> const&;
  };

  template <std::size_t N>
  struct TypeHelper<LHCb::Event::v2::Track, N> {
    constexpr static auto ExtraChildCutHeaders       = {"Event/Track_v2.h"};
    constexpr static auto ExtraVertexCutHeaders      = {"TrackKernel/TrackCompactVertex.h"};
    constexpr static auto ExtraCombinationCutHeaders = {"Event/Track_v2.h", "SelKernel/TrackCombination.h"};
    using InputType                                  = Pr::Selection<LHCb::Event::v2::Track> const&;
  };

  template <std::size_t N>
  struct TypeHelper<LHCb::Pr::Fitted::Forward::Tracks, N> {
    constexpr static auto ExtraChildCutHeaders       = {"Event/PrIterableFittedForwardTracks.h"};
    constexpr static auto ExtraVertexCutHeaders      = {"TrackKernel/TrackCompactVertex.h"};
    constexpr static auto ExtraCombinationCutHeaders = {"Event/PrIterableFittedForwardTracks.h",
                                                        "SelKernel/TrackCombination.h"};
    using InputType                                  = LHCb::Pr::Iterable::Scalar::Fitted::Forward::Tracks const&;
  };

  template <std::size_t N>
  struct TypeHelper<LHCb::Pr::Fitted::Forward::TracksWithPVs, N> {
    constexpr static auto ExtraChildCutHeaders       = {"SelKernel/TrackZips.h"};
    constexpr static auto ExtraVertexCutHeaders      = {"TrackKernel/TrackCompactVertex.h"};
    constexpr static auto ExtraCombinationCutHeaders = {"SelKernel/TrackZips.h", "SelKernel/TrackCombination.h"};
    using InputType = LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs const&;
  };

  template <std::size_t N>
  struct TypeHelper<LHCb::Pr::Fitted::Forward::TracksWithMuonID, N> {
    constexpr static auto ExtraChildCutHeaders       = {"SelKernel/TrackZips.h"};
    constexpr static auto ExtraVertexCutHeaders      = {"TrackKernel/TrackCompactVertex.h"};
    constexpr static auto ExtraCombinationCutHeaders = {"SelKernel/TrackZips.h", "SelKernel/TrackCombination.h"};
    using InputType = LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID const&;
  };

  template <std::size_t N>
  using OutputStorage = std::vector<LHCb::TrackKernel::TrackCompactVertex<N, FPtype>>;

  // using OutputView = std::vector<float>; // FIXME

  // Shorthand for below.
  template <typename T, std::size_t N>
  using FilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple</* OutputView,*/ OutputStorage<N>>(
      typename TypeHelper<T, N>::InputType )>;
} // namespace

/** @class CombineTracks PrCombineTracks.cpp
 *
 *  @tparam T TODO
 */
template <typename T, std::size_t N, bool EnableDumping = false>
class CombineTracks final : public FilterTransform<T, N> {
public:
  static_assert( N == 2 ); // FIXME not immediately implementing the logic for >2-body combinatorics
  using Base   = FilterTransform<T, N>;
  using Helper = TypeHelper<T, N>;
  using Base::debug;
  using Base::info;
  using Base::msgLevel;
  using KeyValue       = typename Base::KeyValue;
  using InputType      = typename Helper::InputType;
  using TrackType      = typename std::decay_t<InputType>::value_type;
  using StateType      = typename std::decay_t<decltype( std::declval<TrackType>().closestToBeamState() )>;
  using Combination    = TrackCombination<TrackType, N>;
  using VertexType     = typename OutputStorage<N>::value_type;
  using VertexFitType  = LHCb::TrackKernel::TrackNProngVertex<N, FPtype, StateType>;
  using CombinationCut = Functors::Functor<bool( Combination const& )>;
  using VertexCut      = Functors::Functor<bool( VertexType const& )>;
  using VoidFun        = Functors::Functor<float()>;
  using TrackFun       = Functors::Functor<float( TrackType const& )>;
  using CombinationFun = Functors::Functor<float( Combination const& )>;
  using VertexFun      = Functors::Functor<float( VertexType const& )>;

  CombineTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : Base( name, pSvcLocator, {"InputTracks", ""},
              {KeyValue{"OutputVertices", ""} /* , KeyValue{"OutputVerticesStorage", ""}*/} ) {}

  std::tuple<bool, /*OutputView, */ OutputStorage<N>> operator()( InputType in ) const override {
    // Prepare the output storage
    OutputStorage<N> storage;

    // Do some potentially-expensive preparation before we enter the loop
    auto vertex_cut      = m_vertex_cut.prepare( /* event_context */ );
    auto combination_cut = m_combination_cut.prepare( /* event_context */ );
    // Buffer our counters to reduce the number of atomic operations
    auto npassed_vertex_cut      = m_npassed_vertex_cut.buffer();
    auto npassed_vertex_fit      = m_npassed_vertex_fit.buffer();
    auto npassed_combination_cut = m_npassed_combination_cut.buffer();

    if constexpr ( EnableDumping ) {
      // These are, by definition, independent of the candidate/combination,
      // so we can populate them now. This also means there's no reason to
      // bother with the explicit preparation step.
      for ( auto& functor_tuple : m_void_dump_functors ) {
        auto const& functor   = std::get<1>( functor_tuple );
        auto&       value_ptr = std::get<2>( functor_tuple );
        *value_ptr            = functor();
      }
    }

    // TODO: replace with N-dependent combination generation...
    for ( auto t1_iter = in.begin(); t1_iter != in.end(); ++t1_iter ) {
      auto const& t1 = *t1_iter;
      for ( auto t2_iter = std::next( t1_iter ); t2_iter != in.end(); ++t2_iter ) {
        auto const& t2 = *t2_iter;
        Combination combination{{t1, t2}};
        auto        pass_combination_cut = combination_cut( combination );
        npassed_combination_cut += pass_combination_cut;
        if ( !pass_combination_cut ) { continue; }

        // Get the states -- this is a bit horrible for the moment
        auto const& s1 = t1.closestToBeamState();
        auto const& s2 = t2.closestToBeamState();

        // Make a new vertex
        VertexFitType vertex_fit{s1, s2};
        auto          fit_succeeded = ( vertex_fit.fit() == VertexFitType::FitSuccess );
        npassed_vertex_fit += fit_succeeded;
        if ( !fit_succeeded ) { continue; }

        // Convert it to the output vertex type
        // TODO: is this really needed? could we cut on vertex_fit and convert afterwards?
        auto& vertex = storage.emplace_back( convertToCompactVertex( vertex_fit ) );

        // Hackily add the child relations
        vertex.setChildZipIdentifier( 0, in.zipIdentifier() );
        vertex.setChildZipIdentifier( 1, in.zipIdentifier() );
        vertex.setChildIndex( 0, std::distance( in.begin(), t1_iter ) );
        vertex.setChildIndex( 1, std::distance( in.begin(), t2_iter ) );

        // Apply the vertex cut
        auto pass_vertex_cut = vertex_cut( vertex );
        npassed_vertex_cut += pass_vertex_cut;
        if ( !pass_vertex_cut ) {
          storage.pop_back();
          continue;
        }

        if constexpr ( EnableDumping ) {
          // For simplicity we only dump the children/combination/vertex
          // quantities after the vertex fit has passed. There is, therefore,
          // an implicit requirement that the vertex fit passed, and the
          // distributions are biased by this requirement + "later" cuts that
          // you choose to apply. But this way the relationship between
          // child/combination/parent in the output TTree is trivial, and if
          // you want to reduce the size of the output tuple by applying cuts
          // then that's easy too.
          for ( auto nchild = 0ul; nchild < N; ++nchild ) {
            for ( auto& functor_tuple : m_child_dump_functors[nchild] ) {
              auto const& functor   = std::get<1>( functor_tuple );
              auto&       value_ptr = std::get<2>( functor_tuple );
              *value_ptr            = functor( combination[nchild] );
            }
          }
          for ( auto& functor_tuple : m_combination_dump_functors ) {
            auto const& functor   = std::get<1>( functor_tuple );
            auto&       value_ptr = std::get<2>( functor_tuple );
            *value_ptr            = functor( combination );
          }
          for ( auto& functor_tuple : m_vertex_dump_functors ) {
            auto const& functor   = std::get<1>( functor_tuple );
            auto&       value_ptr = std::get<2>( functor_tuple );
            *value_ptr            = functor( vertex );
          }
          m_root_tree->Fill();
        }
      }
    }

    // OutputView view; // TODO make this a useful N-independent view into 'storage'
    auto filter_pass = !storage.empty();
    return {filter_pass, /* std::move( view ), */ std::move( storage )};
  }

  StatusCode initialize() override {
    auto sc = Base::initialize();
    decode_vertex();
    decode_combination();
    if constexpr ( EnableDumping ) { set_up_dumping(); }
    return sc;
  }

  StatusCode finalize() override {
    if constexpr ( EnableDumping ) { cleanup_dumping(); }
    return Base::finalize();
  }

private:
  /// Number of combinations passing the combination cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_combination_cut{this, "# passed combination cut"};
  /// Number of successfully fitted combinations
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_fit{this, "# passed vertex fit"};
  /// Number of fitted vertices passing vertex cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_cut{this, "# passed vertex cut"};

  /// Member variables relating to the dumping functionality. If only I could use if constexpr here...
  mutable TTree*         m_root_tree{nullptr};
  std::unique_ptr<TFile> m_root_file;
  template <typename FunType>
  using FunctorList = std::vector<std::tuple<std::string, FunType, std::unique_ptr<typename FunType::result_type>>>;
  mutable std::array<FunctorList<TrackFun>, N> m_child_dump_functors;
  mutable FunctorList<VoidFun>                 m_void_dump_functors;
  mutable FunctorList<VertexFun>               m_vertex_dump_functors;
  mutable FunctorList<CombinationFun>          m_combination_dump_functors;
  Gaudi::Property<std::string>                 m_root_file_name{this, "DumpFileName", "PrCombineTracks.root"};
  using string_vector         = std::vector<std::string>;
  using map_of_string_vectors = std::map<std::string, string_vector>;
  Gaudi::Property<map_of_string_vectors> m_child_dump_functors_config{this,
                                                                      "ChildDump_Functors",
                                                                      {},
                                                                      [this]( auto& ) {
                                                                        if ( this->FSMState() <
                                                                             Gaudi::StateMachine::INITIALIZED )
                                                                          return;
                                                                        this->decode_child_dump();
                                                                      }},
      m_combination_dump_functors_config{this,
                                         "CombinationDump_Functors",
                                         {},
                                         [this]( auto& ) {
                                           if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                           this->decode_combination_dump();
                                         }},
      m_vertex_dump_functors_config{this,
                                    "VertexDump_Functors",
                                    {},
                                    [this]( auto& ) {
                                      if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                      this->decode_vertex_dump();
                                    }},
      m_void_dump_functors_config{this, "VoidDump_Functors", {}, [this]( auto& ) {
                                    if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                    this->decode_void_dump();
                                  }};

  template <typename FunctorList>
  void setup_branches( std::string_view prefix, FunctorList& functors ) {
    for ( auto& functor_tuple : functors ) {
      auto const& nickname  = std::get<0>( functor_tuple );
      auto        value_ptr = std::get<2>( functor_tuple ).get();
      m_root_tree->Branch( ( std::string{prefix} + nickname ).c_str(), value_ptr );
    }
  }

  void set_up_dumping() {
    decode_child_dump();
    decode_combination_dump();
    decode_vertex_dump();
    decode_void_dump();
    m_root_file.reset( TFile::Open( m_root_file_name.value().c_str(), "recreate" ) );
    m_root_file->cd();
    // m_root_tree gets managed by m_root_file, this isn't a dangling pointer
    m_root_tree = new TTree( "CombinerTree", "" );
    setup_branches( "", m_void_dump_functors );
    for ( auto i = 0ul; i < N; ++i ) {
      setup_branches( "child" + std::to_string( i ) + "_", m_child_dump_functors[i] );
    }
    setup_branches( "comb_", m_combination_dump_functors );
    setup_branches( "vertex_", m_vertex_dump_functors );
  }

  void cleanup_dumping() {
    m_root_file->Write();
    m_root_file->Close();
  }

  // Boilerplate to load the functors -- TODO try and factor this out into some mixin
  VertexCut m_vertex_cut;
  bool      m_vertex_code_updated{false};
  bool      m_vertex_headers_updated{false};

  CombinationCut m_combination_cut;
  bool           m_combination_code_updated{false};
  bool           m_combination_headers_updated{false};

  // CombinationCut functor string
  Gaudi::Property<std::string> m_combination_code{this, "CombinationCut_Code", "::Functors::AcceptAll{}",
                                                  [this]( auto& ) {
                                                    this->m_combination_code_updated = true;
                                                    if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                                    if ( !this->m_combination_headers_updated ) return;
                                                    this->decode_combination();
                                                  }};

  // This is the list of headers that must be included for the functor to be compiled
  Gaudi::Property<std::vector<std::string>> m_combination_headers{
      this, "CombinationCut_Headers", {"Functors/Function.h"}, [this]( auto& ) {
        this->m_combination_headers_updated = true;
        if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
        if ( !this->m_combination_code_updated ) return;
        this->decode_combination();
      }};

  // VertexCut functor string
  Gaudi::Property<std::string> m_vertex_code{this, "VertexCut_Code", "::Functors::AcceptAll{}", [this]( auto& ) {
                                               this->m_vertex_code_updated = true;
                                               if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                               if ( !this->m_vertex_headers_updated ) return;
                                               this->decode_vertex();
                                             }};

  // Extra headers needed to compile the VertexCut functor
  Gaudi::Property<std::vector<std::string>> m_vertex_headers{
      this, "VertexCut_Headers", {"Functors/Function.h"}, [this]( auto& ) {
        this->m_vertex_headers_updated = true;
        if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
        if ( !this->m_vertex_code_updated ) return;
        this->decode_vertex();
      }};

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory"};

  void decode_combination() {
    m_factory.retrieve().ignore();
    auto headers = m_combination_headers.value();
    for ( auto const& h : Helper::ExtraCombinationCutHeaders ) { headers.push_back( h ); }
    m_combination_cut             = m_factory->get<CombinationCut>( this, m_combination_code.value(), headers );
    m_combination_code_updated    = false;
    m_combination_headers_updated = false;
  }

  void decode_vertex() {
    m_factory.retrieve().ignore();
    auto headers = m_vertex_headers.value();
    for ( auto const& h : Helper::ExtraVertexCutHeaders ) { headers.push_back( h ); }
    m_vertex_cut             = m_factory->get<VertexCut>( this, m_vertex_code.value(), headers );
    m_vertex_code_updated    = false;
    m_vertex_headers_updated = false;
  }

  template <typename FunctorList, typename HeaderList>
  void decode_functor_dictionary( map_of_string_vectors const& config_map, FunctorList& functor_list,
                                  HeaderList const& extra_headers ) {
    using FunctorType =
        typename std::tuple_element_t<1, typename FunctorList::value_type>; // some Functor<Out(In)> type
    m_factory.retrieve().ignore();
    functor_list.clear();
    for ( auto const& [nickname, code_and_headers] : config_map ) {
      auto const&   code = code_and_headers.front(); // first element is the functor expression
      string_vector headers{std::next( code_and_headers.begin() ), code_and_headers.end()};
      for ( auto const& h : extra_headers ) { headers.push_back( h ); }
      functor_list.emplace_back( nickname, m_factory->get<FunctorType>( this, code, headers ),
                                 std::make_unique<typename FunctorType::result_type>( 0 ) );
    }
  }

  void decode_child_dump() {
    for ( auto i = 0ul; i < N; ++i ) {
      decode_functor_dictionary( m_child_dump_functors_config, m_child_dump_functors[i], Helper::ExtraChildCutHeaders );
    }
  }

  void decode_combination_dump() {
    decode_functor_dictionary( m_combination_dump_functors_config, m_combination_dump_functors,
                               Helper::ExtraCombinationCutHeaders );
  }

  void decode_vertex_dump() {
    decode_functor_dictionary( m_vertex_dump_functors_config, m_vertex_dump_functors, Helper::ExtraVertexCutHeaders );
  }

  void decode_void_dump() {
    decode_functor_dictionary( m_void_dump_functors_config, m_void_dump_functors, std::array<const char*, 0>{} );
  }
};

using CombineTracks__2Body__Track_v2                     = CombineTracks<LHCb::Event::v2::Track, 2>;
using CombineTracks__2Body__Track_v2__Dumper             = CombineTracks<LHCb::Event::v2::Track, 2, true>;
using CombineTracks__2Body__PrFittedForwardTracks        = CombineTracks<LHCb::Pr::Fitted::Forward::Tracks, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithPVs = CombineTracks<LHCb::Pr::Fitted::Forward::TracksWithPVs, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID =
    CombineTracks<LHCb::Pr::Fitted::Forward::TracksWithMuonID, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper =
    CombineTracks<LHCb::Pr::Fitted::Forward::TracksWithMuonID, 2, true>;
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2, "CombineTracks__2Body__Track_v2" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2__Dumper, "CombineTracks__2Body__Track_v2__Dumper" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracks, "CombineTracks__2Body__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithPVs,
                           "CombineTracks__2Body__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper" )
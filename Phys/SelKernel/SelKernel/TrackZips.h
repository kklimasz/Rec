/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrIterableMuonPIDs.h"
#include "SelKernel/IterableVertexRelations.h"

namespace LHCb::Pr::Fitted::Forward {
  using TracksWithPVs    = LHCb::Pr::zip_t<LHCb::Pr::Fitted::Forward::Tracks, BestVertexRelations>;
  using TracksWithMuonID = LHCb::Pr::zip_t<LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
} // namespace LHCb::Pr::Fitted::Forward

namespace LHCb::Pr::Iterable::Scalar::Fitted::Forward {
  using TracksWithPVs    = LHCb::Pr::unwrapped_zip_t<LHCb::Pr::Fitted::Forward::Tracks, BestVertexRelations>;
  using TracksWithMuonID = LHCb::Pr::unwrapped_zip_t<LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
} // namespace LHCb::Pr::Iterable::Scalar::Fitted::Forward

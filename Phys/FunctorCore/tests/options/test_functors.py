#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Small script to test functors instantiation
#  @author Saverio Mariani
##
# =============================================================================

from Configurables import PrFilter__PrVeloTracks, PrFilter__PrFittedForwardTracks, PrFilter__PrFittedForwardTracksWithPVs
from Functors import *

all_track_functors = [
    CLOSESTTOBEAMZ > -9999,
    ETA > -10,
    PHI > -3.14,
    MINIP("") > 0,
]

only_velo_track_functors = [
    NHITS > 0,
]

only_fitted_track_functors = [
    P > 0,
    PT > 0,
    CHI2DOF > 0,
]


def test_functors(algo, algo_name, functor_to_test):
    test = algo(algo_name)
    test.Cut = functor_to_test
    return test


from Configurables import ApplicationMgr, LHCbApp
from Configurables import MessageSvc
from Gaudi.Configuration import DEBUG

app = LHCbApp()
app.EvtMax = 0
#MessageSvc().OutputLevel = DEBUG

#Basic declaration test
all_functors = all_track_functors + only_velo_track_functors + only_fitted_track_functors
for ifunct in all_functors:
    assert len(ifunct.code()) > 0 and len(ifunct.headers()) > 0

#Simple instantiation test: are the templates working?
for ifunct in all_track_functors + only_velo_track_functors:
    test_on_velo = test_functors(PrFilter__PrVeloTracks,
                                 "PrFilter_PrVeloTracks_" + ifunct.code(),
                                 ifunct)

    ApplicationMgr().TopAlg += [test_on_velo]

for ifunct in all_track_functors + only_fitted_track_functors:
    test_on_fitted = test_functors(
        PrFilter__PrFittedForwardTracks,
        "PrFilter_PrFittedForwardTracks_" + ifunct.code(), ifunct)

    test_on_fitted_withPVs = test_functors(
        PrFilter__PrFittedForwardTracksWithPVs,
        "PrFilter__PrFittedForwardTracksWithPVs" + ifunct.code(), ifunct)

    ApplicationMgr().TopAlg += [test_on_fitted, test_on_fitted_withPVs]

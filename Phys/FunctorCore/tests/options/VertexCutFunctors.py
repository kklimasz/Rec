#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import CombineTracks__2Body__PrFittedForwardTracks, CombineTracks__2Body__PrFittedForwardTracksWithPVs
from Functors import *
from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from Configurables import ApplicationMgr, LHCbApp
from Configurables import MessageSvc
from Gaudi.Configuration import DEBUG


def test_functor(algo, algo_name, functor_to_test):
    test = algo(algo_name)
    test.VertexCut_Headers = functor_to_test.headers()
    test.VertexCut_Code = functor_to_test.code()
    return test


app = LHCbApp()
app.EvtMax = 0
#MessageSvc().OutputLevel = DEBUG

#Basic declaration test
vertex_functors = [(MASS(Masses=[106., 106.]) > 200. * MeV)]

for vertex_functor in vertex_functors:
    functor_test = test_functor(
        CombineTracks__2Body__PrFittedForwardTracks,
        "CombineTracks__2Body__PrFittedForwardTracks_" + vertex_functor.code(),
        vertex_functor)
    ApplicationMgr().TopAlg += [functor_test]

    functor_test_WithPVs = test_functor(
        CombineTracks__2Body__PrFittedForwardTracksWithPVs,
        "CombineTracks__2Body__PrFittedForwardTracksWithPVs" +
        vertex_functor.code(), vertex_functor)
    ApplicationMgr().TopAlg += [functor_test_WithPVs]

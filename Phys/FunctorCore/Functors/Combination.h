/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "TrackKernel/TrackVertexUtils.h"

/** @file  Combination.h
 *  @brief Definitions of functors for tuple-likes of track-like objects.
 */

/** @namespace Functors::Combination
 *
 * TODO
 */
namespace Functors::Combination {
  struct DistanceOfClosestApproach : public Function<DistanceOfClosestApproach> {
    template <typename CombinationType>
    auto operator()( CombinationType const& combination ) const {
      return combination.maxdoca();
    }
    template <typename StateLike>
    auto operator()( StateLike const& t1, StateLike const& t2 ) const {
      return LHCb::TrackVertexUtils::doca( t1, t2 );
    }
  };

  struct DistanceOfClosestApproachChi2 : public Function<DistanceOfClosestApproachChi2> {
    template <typename CombinationType>
    auto operator()( CombinationType const& combination ) const {
      return combination.maxdocachi2();
    }
    template <typename StateLike>
    auto operator()( StateLike const& t1, StateLike const& t2 ) const {
      return LHCb::TrackVertexUtils::vertexChi2( t1, t2 );
    }
  };

  struct DistanceOfClosestApproachChi2Cut : public Function<DistanceOfClosestApproachChi2Cut> {
    double m_thresh;
    DistanceOfClosestApproachChi2Cut( double thresh ) : m_thresh( thresh ) {}

    template <typename CombinationType>
    auto operator()( CombinationType const& combination ) const {
      return combination.maxdocachi2cut( m_thresh );
    }
  };
} // namespace Functors::Combination

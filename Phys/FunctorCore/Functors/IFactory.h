/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/extend_interfaces.h"

namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

namespace Functors {
  /** @file  IFactory.h
   *  @brief Interface to the service that JIT-compiles functors or loads them from the cache.
   *
   *  This defines the Functors::IFactory interface.
   */

  /** @class IFunctorFactory
   *  @brief Interface for turning strings into Functor<Out(In)> instances.
   */
  struct IFactory : extend_interfaces<IInterface> {
  protected:
    using functor_base_t                     = std::unique_ptr<Functors::AnyFunctor>;
    constexpr static auto functor_base_t_str = "std::unique_ptr<Functors::AnyFunctor>";
    /** Implementation method that gets an input/output-type-agnostic std::unique_ptr<AnyFunctor>
     *  object from either cling or the cache.
     */
    virtual functor_base_t get_impl( Gaudi::Algorithm* owner, std::string_view code, std::string_view functor_type,
                                     std::vector<std::string> const& hdrs ) = 0;

  public:
    DeclareInterfaceID( IFactory, 1, 0 );

    /** Factory method to get a C++ functor object from a string, either from
     *  the cache or using JIT compilation.
     *
     * @param  owner   The algorithm that owns the functor, this is needed to
     *                 set up the functor's data dependencies correctly.
     * @param  code    String representing the functor.
     * @param  headers List of headers that are needed to compile the functor.
     * @tparam FType   Functor<Out(In)> type that will be returned. This
     *                 specifies precisely how the functor is instantiated.
     * @return         Functor<Out(In)> object of the given type, may be empty.
     */
    template <typename FType>
    FType get( Gaudi::Algorithm* owner, ThOr::FunctorDesc const& fb ) {
      auto any_functor = get_impl( owner, fb.code, System::typeinfoName( typeid( FType ) ), fb.headers );
      if ( any_functor ) {                                          // check the unique_ptr<AnyFunctor> isn't empty
        auto ftype_ptr = dynamic_cast<FType*>( any_functor.get() ); // cast AnyFunctor* -> FType*
        if ( ftype_ptr ) {                                          // check the AnyFunctor -> Functor conversion was OK
          return std::move( *ftype_ptr ); // move the contents into the FType we return by value
        }
      }
      return {}; // return an empty FType if anything went wrong
    }

    template <typename FType>
    FType get( Gaudi::Algorithm* owner, std::string_view code, std::vector<std::string> const& headers ) {
      auto any_functor = get_impl( owner, code, System::typeinfoName( typeid( FType ) ), headers );
      if ( any_functor ) {                                          // check the unique_ptr<AnyFunctor> isn't empty
        auto ftype_ptr = dynamic_cast<FType*>( any_functor.get() ); // cast AnyFunctor* -> FType*
        if ( ftype_ptr ) {                                          // check the AnyFunctor -> Functor conversion was OK
          return std::move( *ftype_ptr ); // move the contents into the FType we return by value
        }
      }
      return {}; // return an empty FType if anything went wrong
    }
  };
} // namespace Functors

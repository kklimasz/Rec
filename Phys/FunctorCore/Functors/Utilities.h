/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/RecVertex_v2.h"
#include "Functors/Function.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/detected.h"
#include <vector>

/** @file  Utilities.h
 *  @brief Helper classes for defining functors.
 */
namespace Functors::detail {
  /** Helper to determine if the given type has a name() method */
  template <typename T>
  using has_name_ = decltype( std::declval<T>().name() );

  template <typename T>
  inline constexpr bool has_name_v = Gaudi::cpp17::is_detected_v<has_name_, T>;

  /** Return the value of obj.name() if that exists, otherwise a default string. */
  template <typename Derived>
  std::string get_name( Derived const& obj ) {
    if constexpr ( has_name_v<Derived> ) {
      return obj.name();
    } else {
      return "Unknown functor";
    }
  }

  template <template <typename> typename BaseType, typename F, typename... TESDepTypes>
  struct DataDepWrapper : public BaseType<DataDepWrapper<BaseType, F, TESDepTypes...>> {
    template <typename... CArgs>
    DataDepWrapper( CArgs&&... tes_locs ) : m_tes_locs{std::forward<CArgs>( tes_locs )...} {}

    template <typename... CArgs>
    DataDepWrapper( F f, CArgs&&... tes_locs ) : m_f{std::move( f )}, m_tes_locs{std::forward<CArgs>( tes_locs )...} {}

    /** Set up the DataHandles, attributing the data dependencies to the given
     *  algorithm.
     */
    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      // Set up the functor we are wrapping and injecting data dependencies into
      detail::bind( m_f, alg );

      // Delegate to a helper that sets up each DataHandle in the tuple.
      bind_helper( alg, std::index_sequence_for<TESDepTypes...>{} );
    }

    /** Retrieve the data dependencies from the TES and bake them into the
     *  "prepared" functor that we return. If you wanted to do more complex
     *  data preparation then this base class is not suitable...
     */
    auto prepare() const {
      // Get a tuple of references to the data objects on the TES
      auto tuple_of_deps = prepare_helper( std::index_sequence_for<TESDepTypes...>{} );

      // Bake this into a new lambda that we return
      return [f = detail::prepare( m_f ), tuple_of_deps]( auto const&... item ) {
        // call f( DType1 const&, DType2 const&, ...,
        //         AType1 const&, AType2 const&, ... )
        // where DTypeN are the elements of TESDepTypes and ATypeN are the
        // arguments that will be passed to the prepared functor when it's
        // called.
        return std::apply( f, std::tuple_cat( tuple_of_deps, std::tie( item... ) ) );
      };
    }

    /** Accessor for the functor we're injecting data-dependencies for, this is
     *  useful for writing tests (and probably absolutely nothing else)!
     */
    auto const& wrapped_functor() const { return m_f; }

  private:
    F                                                               m_f;
    std::array<std::string, sizeof...( TESDepTypes )>               m_tes_locs;
    std::tuple<std::optional<DataObjectReadHandle<TESDepTypes>>...> m_handles;

    /** Set up each member of m_handles by calling .emplace() with the TES
     *  location and algorithm pointer.
     */
    template <typename Algorithm, std::size_t... Is>
    void bind_helper( Algorithm* alg, std::index_sequence<Is...> ) {
      static_assert( std::is_base_of_v<IDataHandleHolder, Algorithm>,
                     "You must include the full declaration of the owning algorithm type!" );
      ( std::get<Is>( m_handles ).emplace( m_tes_locs[Is], alg ), ... );
    }

    /** Make a tuple of references to the result of dereferencing each
     *  DataHandle in m_handles.
     */
    template <std::size_t... Is>
    auto prepare_helper( std::index_sequence<Is...> ) const {
      return std::tie( deref( std::get<Is>( m_handles ) )... );
    }

    /** Check an optional data handle is initialised and doesn't return
     *  nullptr, then return a reference to the actual data.
     */
    template <typename T>
    auto const& deref( std::optional<T> const& optional_handle ) const {
      if ( UNLIKELY( !optional_handle ) ) {
        throw GaudiException{"Functor (" + get_name( m_f ) + ") called without being bound to an algorithm",
                             get_name( m_f ), StatusCode::FAILURE};
      }
      auto data_ptr = optional_handle->get();
      if ( UNLIKELY( !data_ptr ) ) {
        throw GaudiException{"Functor got nullptr from its DataHandle", get_name( m_f ), StatusCode::FAILURE};
      }
      return *data_ptr;
    }
  };

  template <template <typename> typename BaseType, typename... TESDepTypes, typename F, typename... CArgs>
  auto add_data_deps( F f, CArgs&&... args ) {
    return DataDepWrapper<BaseType, F, TESDepTypes...>{std::move( f ), std::forward<CArgs>( args )...};
  }

  /** Default type of the TES container of PVs. */
  using DefaultPVContainer_t = std::vector<LHCb::Event::v2::RecVertex>;
} // namespace Functors::detail

/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ODIN.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
// This is very important, otherwise bind() can give a **very** confusing error message
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include <optional>
#include <string>
#include <type_traits>
#include <utility>

namespace Functors::detail {
  template <typename DataObject>
  struct Size {
    /** Make some error messages more informative. */
    static constexpr auto name() { return "TES::Size"; }

    /** Try and deduce the size of the given object.
     *
     *  @todo include an if constexpr check that the DataObject type, which may
     *        have been explicitly specified as some other type, has a .size()
     *        method and just call it if so.
     */
    int operator()( DataObject const& obj ) const {
      auto input_ptr = &obj; // so we don't have to catch exceptions from dynamic_cast
      auto container = dynamic_cast<ObjectContainerBase const*>( input_ptr );
      if ( container ) { return container->numberOfObjects(); }
      auto anydata = dynamic_cast<AnyDataWrapperBase const*>( input_ptr );
      if ( anydata ) { return anydata->size().value_or( -1 ); }
      return -1;
    }
  };

  template <typename ODIN>
  struct RunNumber {
    /** Make some error messages more informative. */
    static constexpr auto name() { return "TES::RunNumber"; }

    /** DataDepWrapper prepends the extra data dependencies to the argument
     *  list. We request one extra dependency (LHCb::ODIN) in its template
     *  argument list, so we get given that, and this is a "void" functor,
     *  so we don't get given anything else. (i.e. we do not also act on a
     *  track, vertex, etc.)
     */
    auto operator()( ODIN const& odin ) const { return odin.runNumber(); }
  };

  template <typename ODIN>
  struct EventNumber {
    /** Make some error messages more informative. */
    static constexpr auto name() { return "TES::EventNumber"; }

    /** See RunNumber::operator() for explanation. */
    auto operator()( ODIN const& odin ) const { return odin.eventNumber(); }
  };

  template <typename ODIN>
  struct EventType {
    /** Make some error messages more informative. */
    static constexpr auto name() { return "TES::EventType"; }

    /** See RunNumber::operator() for explanation. */
    auto operator()( ODIN const& odin ) const { return odin.eventType(); }
  };
} // namespace Functors::detail

/** @file  TES.h
 *  @brief Functors that implement basic TES interactions.
 */
namespace Functors::TES {
  /** @brief Get the size of a container at the given TES location. */
  template <typename DataObject = DataObject>
  auto Size( std::string tes_loc ) {
    return detail::DataDepWrapper<Function, detail::Size<DataObject>, DataObject>( std::move( tes_loc ) );
  }

  /** @brief Get the run number from ODIN. */
  template <typename ODIN = LHCb::ODIN>
  auto RunNumber( std::string odin_loc ) {
    return detail::DataDepWrapper<Function, detail::RunNumber<ODIN>, ODIN>( std::move( odin_loc ) );
  }

  /** @brief Get the event number from ODIN. */
  template <typename ODIN = LHCb::ODIN>
  auto EventNumber( std::string odin_loc ) {
    return detail::DataDepWrapper<Function, detail::EventNumber<ODIN>, ODIN>( std::move( odin_loc ) );
  }

  /** @brief Get the event type from ODIN. */
  template <typename ODIN = LHCb::ODIN>
  auto EventType( std::string odin_loc ) {
    return detail::DataDepWrapper<Function, detail::EventType<ODIN>, ODIN>( std::move( odin_loc ) );
  }
} // namespace Functors::TES
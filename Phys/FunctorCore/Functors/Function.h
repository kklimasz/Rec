/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrZip.h"
#include "Functors/Core.h"
#include "GaudiKernel/detected.h"
#include "PrKernel/PrSelection.h"
#include "SOAExtensions/ZipSelection.h"
#include "SelKernel/Utilities.h"
#include <cmath>

/** @file  Function.h
 *  @brief Helper types and operator overloads for composition of functions.
 */

namespace Functors {
  /** @class FunctionBase
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All functors inherit from this class.
   */
  struct FunctionBase {};

  /** @class PredicateBase
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All predicates inherit from this class.
   */
  struct PredicateBase {};

  /** @namespace Functors::detail
   *
   *  Internal helpers for new style functors.
   */
  namespace detail {
    template <typename T>
    using has_prepare_ = decltype( std::declval<T>().prepare() );

    template <typename T>
    inline constexpr bool must_be_prepared_v = Gaudi::cpp17::is_detected_v<has_prepare_, T>;

    template <typename Functor>
    auto prepare( Functor const& f ) {
      if constexpr ( must_be_prepared_v<Functor> ) {
        return f.prepare();
      } else {
        return std::cref( f );
      }
    }
  } // namespace detail

  /** @class Function
   *  @brief Template base class for all functors.
   */
  template <class DerivedFunctor>
  struct Function : public FunctionBase {
    /** @brief Apply a functor to a single value and return the result */
    template <typename... Input>
    auto invoke( detail::TopLevelInfo const&, /* event_context, */ Input&&... input ) const {
      auto const& functor = static_cast<DerivedFunctor const&>( *this );
      // Just pass the input container straight to the functor, which should be expecting it
      return detail::prepare( functor /*, event_context */ )( std::forward<Input>( input )... );
    }
  };

  /** @class NumericValue
   *  @brief Trivial function that holds a numeric constant.
   *
   *  This is the type that is generated for the RHS of expressions such as
   *  "ETA > 2".
   */
  template <typename PODtype>
  struct NumericValue final : public Function<NumericValue<PODtype>> {
    constexpr NumericValue( PODtype value ) : m_value( value ) {}
    template <typename... Input>
    constexpr PODtype operator()( Input&&... ) const {
      return m_value;
    }

  private:
    PODtype m_value;
  };

  /** @class Predicate
   *  @brief Template base class for all predicates.
   *
   *  This defines the boilerplate for the different possible input container
   *  types. So far there are implementations for:
   *  @li Refining a Pr::Selection
   *  @li Copying a subset of an STL container, e.g. @p std::vector<T>
   *  @li Making a new Zipping::ExportedSelection from a Zipping::SelectionView
   */
  template <class DerivedFunctor>
  struct Predicate : public PredicateBase, public Function<DerivedFunctor> {
    using Function<DerivedFunctor>::invoke; // Enable predicate( object ) -> bool

    /** @brief Conditionally copy an std::vector.
     */
    template <typename T, typename A>
    std::vector<T, A> invoke( detail::TopLevelInfo const&, std::vector<T, A> const& input ) const {
      auto const&       functor = static_cast<DerivedFunctor const&>( *this );
      std::vector<T, A> out;
      out.reserve( input.size() );
      std::copy_if( input.begin(), input.end(), std::back_inserter( out ), detail::prepare( functor ) );
      return out;
    }

    /** @brief Filter a container that can be made iterable by LHCb::Pr::Zip
     *
     *  This overload only participates if LHCb::Pr::make_zip( container ) is
     *  valid, so far this typically means LHCb::Pr::*::Tracks containers. The
     *  condition may have to be tightened up later.
     */
    template <typename PrTracks>
    auto invoke( detail::TopLevelInfo const& top_level, PrTracks const& input ) const
        -> std::decay_t<decltype( LHCb::Pr::make_zip( std::declval<PrTracks>() ), std::declval<PrTracks>() )> {
      auto const& functor = static_cast<DerivedFunctor const&>( *this );
      // Try and give an informative error message if we failed to use the same
      // instruction set in the stack compilation and with cling.
      namespace wrapper = SIMDWrapper::best;
      if ( UNLIKELY( wrapper::instructionSet() != wrapper::stackInstructionSet() ) ) {
        top_level.Warning( "Stack:" + SIMDWrapper::instructionSetName( wrapper::stackInstructionSet() ) +
                           ", Functor:" + SIMDWrapper::instructionSetName( wrapper::instructionSet() ) +
                           ", instruction set mismatch. ROOT/cling was not compiled with the same options as the "
                           "stack, try the functor cache" );
      }

      using dType = wrapper::types;

      // Get an iterable version of the input
      auto iterable_input = LHCb::Pr::make_zip( input );

      // Filter the container using the prepared functor
      return iterable_input.template filter<dType, false>( detail::prepare( functor ) );
    }

    /** @brief Filter a LHCb::Pr::Zip<A, B, Containers...> container
     *
     *  The default SIMD and unwrap settings of the input are ignored.
     */
    template <SIMDWrapper::InstructionSet def_simd, bool def_unwrap, typename... PrTracks>
    auto invoke( detail::TopLevelInfo const&                             top_level,
                 LHCb::Pr::Zip<def_simd, def_unwrap, PrTracks...> const& input ) const {
      auto const& functor = static_cast<DerivedFunctor const&>( *this );
      // Try and give an informative error message if we failed to use the same
      // instruction set in the stack compilation and with cling.
      namespace wrapper = SIMDWrapper::best;
      if ( UNLIKELY( wrapper::instructionSet() != wrapper::stackInstructionSet() ) ) {
        top_level.Warning( "Stack:" + SIMDWrapper::instructionSetName( wrapper::stackInstructionSet() ) +
                           ", Functor:" + SIMDWrapper::instructionSetName( wrapper::instructionSet() ) +
                           ", instruction set mismatch. ROOT/cling was not compiled with the same options as the "
                           "stack, try the functor cache" );
      }

      using dType = wrapper::types;

      // Filter the container using the prepared functor
      return input.template filter<dType, false>( detail::prepare( functor ) );
    }

    /** @brief Refine a Pr::Selection.
     */
    template <typename T>
    Pr::Selection<T> invoke( detail::TopLevelInfo const&, Pr::Selection<T> const& input ) const {
      auto const& functor = static_cast<DerivedFunctor const&>( *this );
      return input.select( detail::prepare( functor ) );
    }

    /** @brief Refine a Zipping::SelectionView.
     */
    template <typename Skin>
    Zipping::ExportedSelection<> invoke( detail::TopLevelInfo const&,
                                         Zipping::SelectionView<Skin> const& input ) const {
      auto const& functor = static_cast<DerivedFunctor const&>( *this );
      return input.select( detail::prepare( functor ) );
    }
  };

  /** @class AcceptAll
   *  @brief Trivial functor that always returns true.
   *  @todo  Figure out how to make this return an appropriate
   *         type like SIMDWrapper::best::mask_v if appropriate.
   */
  struct AcceptAll : public Predicate<AcceptAll> {
    template <typename... Data>
    constexpr bool operator()( Data&&... ) const {
      return true;
    }
  };

  /** @class AcceptNone
   *  @brief Trivial functor that always returns false.
   */
  struct AcceptNone : public Predicate<AcceptNone> {
    template <typename... Data>
    bool operator()( Data&&... ) const {
      return false;
    }
  };

  namespace detail {
    /** Sub-helper for calling bind() on every element of a tuple of functors. */
    template <typename Algorithm, typename... F, std::size_t... I>
    void bind_tuple_helper( Algorithm* alg, std::tuple<F...>& functors, std::index_sequence<I...> ) {
      ( detail::bind( std::get<I>( functors ), alg ), ... );
    }

    /** Helper for calling bind() on every element of a tuple of functors. */
    template <typename Algorithm, typename... F>
    void bind( std::tuple<F...>& functors, Algorithm* alg ) {
      bind_tuple_helper( alg, functors, std::index_sequence_for<F...>{} );
    }

    /** @class ComposedFunctor
     *  @brief Workhorse for producing a new functor from existing ones.
     *
     *  This class is used to implement all binary function operations:
     *  @li arithmetric operators (+, -, *, /, %), two functions -> function.
     *  @li comparison operators (>, <, etc.), two functions -> predicate.
     *  @li logical operators (&, |), two predicates -> predicate.
     *
     *  It is also be used to implement functor transformations, like
     *  math::log( PT ), math::pow( PT, 2 ), math::in_range( min, PT, max )
     *  and so on. And the unary negation operators.
     */
    template <typename Operator, typename InputBase, template <typename...> typename NewType, typename... F>
    struct ComposedFunctor : public NewType<ComposedFunctor<Operator, InputBase, NewType, F...>> {
      template <typename... U, typename = std::enable_if_t<std::is_constructible_v<std::tuple<F...>, U...>>>
      ComposedFunctor( U&&... u ) : m_fs( std::forward<U>( u )... ) {
        check_types( std::index_sequence_for<F...>{} );
      }

      /** Prepare the composite functor for use. */
      auto prepare() const { return prepare( std::index_sequence_for<F...>{} ); }

      /** Contained functors may have data-dependencies that need to be
       *  associated to the owning algorithm.
       */
      template <typename Algorithm>
      void bind( Algorithm* alg ) {
        detail::bind( m_fs, alg );
      }

    private:
      /** Helper for compile-time check that all owned functors inherit from
       *  the specified base class.
       */
      template <std::size_t... I>
      constexpr void check_types( std::index_sequence<I...> ) {
        static_assert( ( std::is_base_of_v<InputBase, std::tuple_element_t<I, std::tuple<F...>>> && ... ) );
      }

      /** Helper to prepare all owned functors and return a new lambda that
       *  holds the prepared functors and the knowledge (encoded by Operator)
       *  of how to combine them into the result of the new (composed) functor.
       */
      template <std::size_t... I>
      auto prepare( std::index_sequence<I...> ) const {
        // Prepare all of the functors and capture the resulting tuple
        return [fs = std::tuple{detail::prepare( std::get<I>( m_fs ) )...}]( auto const&... input ) {
          if constexpr ( std::is_same_v<Operator, std::logical_and<>> ) {
            static_assert( sizeof...( I ) == 2 );
            // Operator{}( ... ) would not short-circuit in this case
            auto first = std::get<0>( fs )( input... );
            // Short circuit vectorised cuts as well as scalar ones
            using Sel::Utils::none; // for the scalar case
            if ( none( first ) ) return first;
            return first && std::get<1>( fs )( input... );
          } else if constexpr ( std::is_same_v<Operator, std::logical_or<>> ) {
            static_assert( sizeof...( I ) == 2 );
            // Operator{}( ... ) would not short-circuit in this case
            auto first = std::get<0>( fs )( input... );
            // Short circuit vectorised cuts as well as scalar ones
            using Sel::Utils::all; // for the scalar case
            if ( all( first ) ) return first;
            return first || std::get<1>( fs )( input... );
          } else {
            // Evaluate all the functors and let the given Operator combine the
            // results.
            return Operator{}( std::get<I>( fs )( input... )... );
          }
        };
      }
      std::tuple<F...> m_fs;
    };

    /** Helper function to make a ComposedFunctor that deduces the actual
     *  functor types from its argument types, but leaves us to specify the
     *  other details explicitly.
     */
    template <typename Operator, typename InputBase, template <typename...> typename NewType, typename... Args>
    constexpr auto compose( Args&&... args ) {
      return ComposedFunctor<Operator, InputBase, NewType, Args...>( std::forward<Args>( args )... );
    }

    /** Use our own helper to define in one place which types we allow to be
     *  promoted to NumericValue.
     */
    template <typename F>
    inline constexpr bool is_promotable_v = std::is_arithmetic_v<std::decay_t<F>>;

    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_function_v = std::is_base_of_v<FunctionBase, std::decay_t<F>>;

    /** Helper to promote arithmetic constants to NumericValue instances while
     *  not doing anything to functors.
     */
    template <typename F>
    constexpr auto promote( F f ) {
      if constexpr ( is_promotable_v<F> ) {
        return NumericValue( f );
      } else {
        static_assert( is_functor_function_v<F>, "Functors::detail::promote( f ) got invalid input" );
        return f;
      }
    }

    /** Helper to make a ComposedFunctor from a set of arguments that can be a
     *  mix of arithmetic constants and functor expressions. Arithmetic
     *  constants are promoted to NumericValue instances using promote().
     */
    template <typename Operator, typename InputBase, template <typename...> typename NewType, typename... Args>
    constexpr auto promote_and_compose( Args&&... args ) {
      return compose<Operator, InputBase, NewType>( promote( std::forward<Args>( args ) )... );
    }

    /** Helper for checking if our overloaded operators should be considered.
     *
     *  The condition is that either both LHS and RHS are our functions, or one
     *  of them is and the other side of the expression is an arithmetic
     *  constant that we can promote to a NumericValue.
     */
    template <typename F1, typename F2>
    using enable_function_operator =
        std::enable_if_t<( is_functor_function_v<F1> && (is_promotable_v<F2> || is_functor_function_v<F2>)) ||
                             (is_promotable_v<F1> && is_functor_function_v<F2>),
                         int>;

    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_predicate_v = std::is_base_of_v<PredicateBase, std::decay_t<F>>;

    /** SFINAE helper for defining boolean (bitwise...) logic overloads.
     */
    template <typename F1, typename F2>
    using enable_if_both_predicates = std::enable_if_t<is_functor_predicate_v<F1> && is_functor_predicate_v<F2>, int>;
  } // namespace detail

  /** operator+ overload covering all three combinations of function/arithmetic
   *
   *  This turns two functions (hence FunctionBase) into a new function (hence
   *  Function). The other operator overloads defined here are similar.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator+( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::plus<>, FunctionBase, Function>( std::forward<F1>( lhs ),
                                                                             std::forward<F2>( rhs ) );
  }

  /** operator- overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator-( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::minus<>, FunctionBase, Function>( std::forward<F1>( lhs ),
                                                                              std::forward<F2>( rhs ) );
  }

  /** operator* overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator*( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::multiplies<>, FunctionBase, Function>( std::forward<F1>( lhs ),
                                                                                   std::forward<F2>( rhs ) );
  }

  /** operator/ overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator/( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::divides<>, FunctionBase, Function>( std::forward<F1>( lhs ),
                                                                                std::forward<F2>( rhs ) );
  }

  /** operator% overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator%( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::modulus<>, FunctionBase, Function>( std::forward<F1>( lhs ),
                                                                                std::forward<F2>( rhs ) );
  }

  /** Unary operator- overload. */
  template <typename F, typename std::enable_if_t<detail::is_functor_function_v<F>, int> = 0>
  constexpr auto operator-( F&& f ) {
    return detail::compose<std::negate<>, FunctionBase, Function>( std::forward<F>( f ) );
  }

  /** operator| overload for turning two predicates into another one.
   *
   *  detail::promote_and_compose is not used here because we don't support
   *  logical operations between predicates and arithmetic constants.
   */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator|( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_or<>, PredicateBase, Predicate>( std::forward<F1>( lhs ),
                                                                         std::forward<F2>( rhs ) );
  }

  /** operator& overload for turning two predicates into another one. */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator&( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_and<>, PredicateBase, Predicate>( std::forward<F1>( lhs ),
                                                                          std::forward<F2>( rhs ) );
  }

  /** operator~ overload, creates a new functor representing the logical
   *  negation of the given functor.
   */
  template <typename F, typename std::enable_if_t<detail::is_functor_predicate_v<F>, int> = 0>
  constexpr auto operator~( F&& obj ) {
    return detail::compose<std::logical_not<>, PredicateBase, Predicate>( std::forward<F>( obj ) );
  }

  /** operator> overload covering all three combinations of function/arithmetic
   *
   *  This, and the others below, transforms two Functions (hence FunctionBase)
   *  into a Predicate (hence the 3rd template argument).
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                                 std::forward<F2>( rhs ) );
  }

  /** operator< overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                              std::forward<F2>( rhs ) );
  }

  /** operator>= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater_equal<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                                       std::forward<F2>( rhs ) );
  }

  /** operator<= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less_equal<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                                    std::forward<F2>( rhs ) );
  }

  /** operator== overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator==( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::equal_to<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                                  std::forward<F2>( rhs ) );
  }

  /** operator!= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator!=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::not_equal_to<>, FunctionBase, Predicate>( std::forward<F1>( lhs ),
                                                                                      std::forward<F2>( rhs ) );
  }

  namespace detail::math {
    struct pow_helper {
      template <typename... Args>
      constexpr auto operator()( Args&&... args ) const {
        using std::pow;
        return pow( std::forward<Args>( args )... );
      }
    };

    struct log_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::log;
        return log( std::forward<Arg1>( arg1 ) );
      }
    };

    struct in_range_helper {
      /** This acts on the actual functor return values. */
      template <typename Min, typename Val, typename Max>
      constexpr auto operator()( Min min, Val val, Max max ) const {
        return ( val > min ) && ( val < max );
      }
    };
  } // namespace detail::math

  /** @namespace Functors::math
   *
   *  Mathematical functions that act on new style functors.
   */
  namespace math {
    /** Wrapper for std::pow( contained_functor_value, arg ).
     *
     *  e.g. math::pow( PT, 2 ) is a functor that returns the squared PT
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 2>>
    constexpr auto pow( Args&&... args ) {
      return detail::promote_and_compose<detail::math::pow_helper, FunctionBase, Function>(
          std::forward<Args>( args )... );
    }

    /** Wrapper for std::log( contained_functor_value ).
     */
    template <typename F>
    constexpr auto log( F&& f ) {
      return detail::promote_and_compose<detail::math::log_helper, FunctionBase, Function>( std::forward<F>( f ) );
    }

    /** More efficient shorthand for (f > x) && (f < y) that only evaluates
     *  the value (f) once.
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 3>>
    constexpr auto in_range( Args&&... args ) {
      return detail::promote_and_compose<detail::math::in_range_helper, FunctionBase, Predicate>(
          std::forward<Args>( args )... );
    }
  } // namespace math
} // namespace Functors
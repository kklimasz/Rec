/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/detected.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"
#include "TrackKernel/TrackCompactVertex.h"

/** @file  Composite.h
 *  @brief Definitions of functors for composite-particle-like objects.
 */

/** @namespace Functors::Composite
 *
 *  Functors that make sense for composite particles (i.e. ones that have a
 *  vertex as well as a trajectory)
 */
namespace Functors::detail {
  /** Helper to determine if the given type has a charge() method.
   *  @todo probably retire this helper + convertToState()
   */
  template <typename T>
  using has_charge_ = decltype( std::declval<T>().charge() );

  template <typename T>
  inline constexpr bool has_charge_v = Gaudi::cpp17::is_detected_v<has_charge_, T>;

  template <typename ParticleLike>
  LHCb::State convertToState( ParticleLike const& part ) {
    // Shamelessly stolen from Particle2State.cpp
    LHCb::State s;
    auto        slopes = part.slopes();
    int         charge{0};
    if constexpr ( has_charge_v<ParticleLike> ) { charge = part.charge(); }
    s.setState( part.referencePoint().X(), part.referencePoint().Y(), part.referencePoint().Z(), slopes.X(), slopes.Y(),
                charge / part.p() );

    Gaudi::Matrix5x5 cov5Dtmp;
    // 2x2 for x and y (dropping z) goes in the upper left corner
    cov5Dtmp.Place_at( part.posCovMatrix().template Sub<Gaudi::SymMatrix2x2>( 0, 0 ), 0, 0 );

    // 2x3 for pos and mom goes in the lower left corner (dropping z)
    cov5Dtmp.Place_at( part.posThreeMomCovMatrix().template Sub<Gaudi::Matrix3x2>( 0, 0 ), 2, 0 );

    // 3x3 for mom goes in the lower right corner
    cov5Dtmp.Place_at( part.threeMomCovMatrix(), 2, 2 );

    Gaudi::SymMatrix5x5 cov5DtmpSym{cov5Dtmp.LowerBlock()};

    // the variables
    auto const Px = part.threeMomentum().X();
    auto const Py = part.threeMomentum().Y();
    auto const Pz = part.threeMomentum().Z();
    auto const P  = part.threeMomentum().R();

    Gaudi::Matrix5x5 Jacob;
    // unit matrix in the top left corner
    Jacob( 0, 0 ) = 1.; //  dx/dy
    Jacob( 1, 1 ) = 1.; //  dy/dy
    // non unit matrix in the bottom right corner
    Jacob( 2, 2 ) = 1 / Pz;                       //  dTx/dPx = 1/Pz
    Jacob( 2, 3 ) = 0.;                           //  dTx/dPy = 0.
    Jacob( 2, 4 ) = -Px / ( Pz * Pz );            //  dTx/dPz = -Px/Pz^2
    Jacob( 3, 2 ) = 0.;                           //  dTy/dPx = 0.
    Jacob( 3, 3 ) = 1 / Pz;                       //  dTy/dPy = 1/Pz
    Jacob( 3, 4 ) = -Py / ( Pz * Pz );            //  dTy/dPz = -Py/Pz^2
    Jacob( 4, 2 ) = -charge * Px / ( P * P * P ); //  do/dPx  = -charge*Px/P^3
    Jacob( 4, 3 ) = -charge * Py / ( P * P * P ); //  do/dPy  = -charge*Py/P^3
    Jacob( 4, 4 ) = -charge * Pz / ( P * P * P ); //  do/dPz  = -charge*Pz/P^3

    s.setCovariance( ROOT::Math::Similarity( Jacob, cov5DtmpSym ) );

    return s;
  }

  template <typename VContainer>
  struct FlightDistanceChi2ToVertex {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "FlightDistanceChi2ToVertex"; }

    template <typename Particle>
    auto operator()( VContainer const& vertices, Particle const& composite ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      auto const& bestPV = Sel::getBestPV( composite, vertices );

      // Now calculate the flight distance chi2 between 'composite' and 'bestPV'
      return Sel::Utils::flightDistanceChi2( composite, bestPV );
    }
  };

  template <typename VContainer>
  struct CosDirectionAngleToVertex {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "CosDirectionAngleToVertex"; }

    template <typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      auto const& bestPV = Sel::getBestPV( composite, vertices );

      // Calculate the angle between the momentum vector of the composite and
      // the vector connecting its decay vertex to the origin
      auto const& mom = composite.threeMomentum();

      // The flight direction vector
      auto const flight = composite.endVertex() - bestPV.position();

      // Calculate the cosine of the angle
      return mom.Dot( flight ) / std::sqrt( flight.Mag2() * mom.Mag2() );
    }
  };

  template <typename VContainer>
  struct ImpactParameterChi2ToVertexAsState {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "ImpactParameterChi2ToVertexAsState"; }

    template <typename Particle>
    auto operator()( VContainer const& vertices, Particle const& particle ) const {
      // Converting the composite particle to a state loses z
      // uncertainty/correlation information!
      return Sel::Utils::impactParameterChi2( detail::convertToState( particle ),
                                              Sel::getBestPV( particle, vertices ) );
    }
  };

  template <typename VContainer>
  struct PseudoRapidityFromVertex {
    static constexpr auto name() { return "PseudoRapidityFromVertex"; }

    template <typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      auto const& bestPV = Sel::getBestPV( composite, vertices );
      auto        flight = composite.endVertex() - bestPV.position();
      return flight.eta();
    }
  };

  template <typename VContainer>
  struct CorrectedMass {
    float m_mass_hypo;
    CorrectedMass( float mass_hypo ) : m_mass_hypo{mass_hypo} {}

    static constexpr auto name() { return "CorrectedMass"; }

    template <typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      // Get the associated [primary] vertex
      auto const& bestPV = Sel::getBestPV( composite, vertices );

      // Get the decay vertex
      auto const& decay_vertex = composite.endVertex();

      // Use m_mass_hypo to get the 4-momentum of the composite
      std::array<float, Composite::NumChildren> child_masses;
      child_masses.fill( m_mass_hypo );
      Gaudi::LorentzVector p4;
      {
        // @todo This does too much work!
        Gaudi::XYZPoint     position;
        Gaudi::SymMatrix3x3 poscov;
        Gaudi::SymMatrix4x4 p4cov;
        Gaudi::Matrix4x3    momposcov;
        LHCb::TrackKernel::computeParticleParams( composite, child_masses, position, p4, poscov, p4cov, momposcov );
      }

      // TODO this is lifted from LoKi but some checks were dropped.

      // Calculate the corrected mass using the 4-momentum (p4) and the
      // flight vector:
      auto const d = decay_vertex - bestPV.position();

      // Three-momentum
      auto const mom = p4.Vect();

      // Get the uncorrected squared mass.
      auto const m2 = p4.M2();

      // Get the pT variable that we need
      auto const dmag2 = d.Mag2();
      auto const perp  = composite.threeMomentum() - d * ( mom.Dot( d ) / dmag2 );
      auto const pt    = perp.R();

      // Calulate the corrected mass
      return pt + std::sqrt( m2 + std::pow( pt, 2 ) );
    }
  };
} // namespace Functors::detail

namespace Functors::Composite {
  /** @brief Flight distance chi2 to the "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto FlightDistanceChi2ToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::FlightDistanceChi2ToVertex<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Calculate the cosine of the angle between the momentum vector of
   *         the composite and the vector connecting its decay vertex to the
   *         "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto CosDirectionAngleToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::CosDirectionAngleToVertex<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** Alternative implementation of impact parameter chi2 for a composite
   *  particle that converts the composite to an LHCb::State and then uses the
   *  State implementation of the ipchi2 calculation.
   *
   *  @todo Retire this.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto ImpactParameterChi2ToVertexAsState( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::ImpactParameterChi2ToVertexAsState<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Pseudorapidity of the flight vector of the given composite. */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto PseudoRapidityFromVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::PseudoRapidityFromVertex<VContainer>, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Calculate the corrected mass, using the given mass hypothesis. */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto CorrectedMass( float assumed_mass, std::string vertex_location ) {
    return detail::add_data_deps<Function, VContainer>( detail::CorrectedMass<VContainer>{assumed_mass},
                                                        std::move( vertex_location ) );
  }

  struct Mass : public Function<Mass> {
    std::vector<float> daughter_masses;
    // float daughter_masses;
    Mass( std::vector<float> masses ) : daughter_masses{masses} {}
    template <typename CombinationType>
    auto operator()( CombinationType const& combination ) const {
      assert( daughter_masses.size() == combination.NumChildren );
      float P2 = combination.p() * combination.p();
      float E  = 0.;
      for ( size_t idau = 0; idau < daughter_masses.size(); ++idau ) {
        E += std::sqrt( combination.daughters.P( idau ) * combination.daughters.P( idau ) +
                        daughter_masses[idau] * daughter_masses[idau] );
      }
      float mass = std::sqrt( E * E - P2 );
      return mass;
    }
  };
} // namespace Functors::Composite
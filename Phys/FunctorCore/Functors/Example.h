/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include <algorithm>
#include <tuple>

namespace Functors::Examples {
  struct TimesTwo : public Function<TimesTwo> {
    template <typename Data>
    auto operator()( Data const& value ) const {
      return value * 2;
    }
  };

  struct GreaterThan : public Predicate<GreaterThan> {
    int m_v;
    GreaterThan( int v ) : m_v( v ) {}
    template <typename Data>
    bool operator()( Data const& value ) const {
      return value > m_v;
    }
  };

  struct ThorBeatsLoki : public Predicate<ThorBeatsLoki> {
    // for reference see https://www.youtube.com/watch?v=-JPkvO1e1Gw
    template <typename... Data>
    bool operator()( Data const&... ) const {
      return true;
    }
  };
} // namespace Functors::Examples

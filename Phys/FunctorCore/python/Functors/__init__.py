###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Functors.grammar import Functor, BoundFunctor

P = Functor('P', "Track::Momentum", "TrackLike.h", "Momentum.")
PT = Functor('PT', "Track::TransverseMomentum", "TrackLike.h",
             "Transverse momentum.")
PHI = Functor('PHI', "Track::Phi", "TrackLike.h", "Azimuthal angle, phi.")
ETA = Functor('ETA', "Track::PseudoRapidity", "TrackLike.h", "Pseudorapidity.")
ISMUON = Functor('ISMUON', "Track::IsMuon", "TrackLike.h", "IsMuon.")
CHI2DOF = Functor('CHI2DOF', "Track::Chi2PerDoF", "TrackLike.h",
                  "chi^2 per degree of freedom.")
GHOSTPROB = Functor('GHOSTPROB', "Track::GhostProbability", "TrackLike.h",
                    "Ghost probability.")
CLOSESTTOBEAMZ = Functor(
    'CLOSESTTOBEAMZ', "Track::ClosestToBeamZ", "TrackLike.h",
    "Z coordinate of the track state closest to the beam.")
NHITS = Functor('NHITS', "Track::nHits", "TrackLike.h",
                "Track number of hits.")
MINIP = Functor(
    'MINIP',
    "Track::MinimumImpactParameter",
    "TrackLike.h",
    """Calculate the minimum impact parameter w.r.t. any of the given vertices.
    MINIPCUT may be more efficient.""",
    Params=[('Vertices', 'TES location of input [primary] vertices', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
MINIPCHI2 = Functor(
    'MINIPCHI2',
    "Track::MinimumImpactParameterChi2",
    "TrackLike.h",
    """Calculate the minimum impact parameter chi2 w.r.t. any of the given
    vertices. MINIPCHI2CUT may be more efficient.""",
    Params=[('Vertices', 'TES location of input [primary] vertices', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
MINIPCUT = Functor(
    'MINIPCUT',
    "Track::MinimumImpactParameterCut",
    "TrackLike.h",
    """Require the minimum impact parameter w.r.t. any of the given vertices is
    greater than some threshold.""",
    Params=[('IPCut', 'The impact parameter cut value', float),
            ('Vertices', 'TES location of input [primary] vertices', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
MINIPCHI2CUT = Functor(
    'MINIPCHI2CUT',
    "Track::MinimumImpactParameterChi2Cut",
    "TrackLike.h",
    """Require the minimum impact parameter chi2 w.r.t. any of the given
    vertices is greater than some threshold.""",
    Params=[('IPChi2Cut', 'The impact parameter chi2 cut value', float),
            ('Vertices', 'TES location of input [primary] vertices', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
ALL = Functor('ALL', "AcceptAll", "Function.h",
              "Accept everything; always evaluates to 'true'.")
NONE = Functor('NONE', "AcceptNone", "Function.h",
               "Accept nothing; always evaluates to 'false'.")
SUM = Functor(
    'SUM',
    "Adapters::Accumulate",
    "Adapters.h",
    "Calculate the [scalar] sum of the given functor value.",
    Params=[('Functor', 'The functor to accumulate the return value of.',
             BoundFunctor)])
MIN = Functor(
    'MIN',
    "Adapters::Minimum",
    "Adapters.h",
    "Calculate the minimum of the given functor value.",
    Params=[('Functor', 'The functor to find the minimum return value of.',
             BoundFunctor)])
MAX = Functor(
    'MAX',
    "Adapters::Maximum",
    "Adapters.h",
    "Calculate the maximum of the given functor value.",
    Params=[('Functor', 'The functor to find the maximum return value of.',
             BoundFunctor)])
MASS = Functor(
    'MASS',
    'Composite::Mass',
    'Composite.h',
    '''Invariant mass of a combined particle.''',
    Params=[('Masses', 'Masses of the children', list)])
DOCA = Functor(
    'DOCA', "Combination::DistanceOfClosestApproach", "Combination.h",
    "Compute the distance of closest approach between two 'states'.")
DOCACHI2 = Functor(
    'DOCACHI2', "Combination::DistanceOfClosestApproachChi2", "Combination.h",
    """Compute the significance of the distance of closest
    approach between two 'states'.""")
MAXDOCACHI2CUT = Functor(
    'MAXDOCACHI2CUT',
    "Combination::DistanceOfClosestApproachChi2Cut",
    "Combination.h",
    """Cut on the significance of the distance of closest
    approach between two 'states'.""",
    Params=[('thresh', 'Threshold for cut', float)])
SIZE = Functor(
    'SIZE',
    "TES::Size",
    "TES.h",
    "Get the size of the given container",
    Params=[('Container',
             'The TES location of the container whose size we return', str)],
    TemplateParams=[('ContainerType', 'Type of the container')])
BPVETA = Functor(
    'BPVETA',
    'Composite::PseudoRapidityFromVertex',
    'Composite.h',
    '''Compute the pseudorapidity of the vector connecting the associated
    [primary] vertex to the composite particle decay vertex.''',
    Params=[('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
BPVCORRM = Functor(
    'BPVCORRM',
    'Composite::CorrectedMass',
    'Composite.h',
    '''Compute the corrected mass of the composite using the given mass
    hypotheses and the associated [primary] vertex.''',
    Params=[('Mass', 'Mass hypotheses to apply to the composite children',
             float),
            ('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
BPVDIRA = Functor(
    'BPVDIRA',
    'Composite::CosDirectionAngleToVertex',
    'Composite.h',
    '''Compute the cosine of the angle between the particle momentum and the
    vector from the associated [primary] vertex to the composite particle
    decay vertex.''',
    Params=[('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
BPVIPCHI2 = Functor(
    'BPVIPCHI2',
    'Track::ImpactParameterChi2ToVertex',
    'TrackLike.h',
    '''Return the impact parameter chi2 w.r.t. the associated vertex, assuming
    that it is from the container of vertices that is passed. If no association
    is available, compute one.''',
    Params=[('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
BPVFDCHI2 = Functor(
    'BPVFDCHI2',
    'Composite::FlightDistanceChi2ToVertex',
    'Composite.h',
    '''Return the flight distance chi2 w.r.t. the associated vertex, assuming
    that it is from the container of vertices that is passed. If no association
    is available, compute one.''',
    Params=[('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
BPVIPCHI2STATE = Functor(
    'BPVIPCHI2STATE',
    'Composite::ImpactParameterChi2ToVertexAsState',
    'Composite.h',
    '''Return the impact parameter chi2 w.r.t. the associated vertex, assuming
    that it is from the container of vertices that is passed. If no association
    is available, compute one.''',
    Params=[('Vertices', 'TES location of input [primary] vertices.', str)],
    TemplateParams=[('VerticesType', 'Input vertex container type')])
RUNNUMBER = Functor(
    'RUNNUMBER',
    'TES::RunNumber',
    'TES.h',
    '''Return the run number from ODIN.''',
    Params=[('ODIN', 'TES location of ODIN information.', str)],
    TemplateParams=[('ODINType', 'Type of the ODIN object')])
EVENTNUMBER = Functor(
    'EVENTNUMBER',
    'TES::EventNumber',
    'TES.h',
    '''Return the event number from ODIN.''',
    Params=[('ODIN', 'TES location of ODIN information.', str)],
    TemplateParams=[('ODINType', 'Type of the ODIN object')])
EVENTTYPE = Functor(
    'EVENTTYPE',
    'TES::EventType',
    'TES.h',
    '''Return the event type from ODIN.''',
    Params=[('ODIN', 'TES location of ODIN information.', str)],
    TemplateParams=[('ODINType', 'Type of the ODIN object')])

# Examples:
Ex_TimesTwo = Functor('TimesTwo', 'Examples::TimesTwo', 'Example.h',
                      ''' calculate the double ''')

Ex_GreaterThan = Functor(
    'GreaterThan',
    'Examples::GreaterThan',
    'Example.h',
    ''' value greater than v? ''',
    Params=[('v', 'reference value', float)])

Ex_TBL = Functor('TBL', 'Examples::ThorBeatsLoki', 'Example.h', ''' ... ''')


def mva_input_formatter(config):
    from Functors.grammar import python_to_cpp_str
    inputs = []
    headers = []
    for key, val in config.items():
        assert type(key) == str
        key_code, key_headers = python_to_cpp_str(key)
        val_code, val_headers = python_to_cpp_str(val)
        inputs.append('MVAInput( ' + key_code + ', ' + val_code + ' )')
        headers += key_headers + val_headers
    return ', '.join(inputs), list(set(headers))


def mva_impl_formatter(config):
    return 'Sel::' + config, ['SelTools/' + config + '.h']


MVA = Functor(
    'MVA',
    'MVA',
    'MVA.h',
    '''Evaluate an MVA.''',
    Params=[('Config', 'MVA config', dict),
            ('Inputs', 'MVA inputs', dict, mva_input_formatter)],
    TemplateParams=[('MVAType', 'The MVA implementation to use',
                     mva_impl_formatter)])


def comb_locations_formatter(locations):
    from Functors.grammar import python_to_cpp_str
    expressions, headers = [], []
    for loc in locations:
        code, headers = python_to_cpp_str(loc)
        expressions.append(code)
        headers += headers
    return ', '.join(expressions), list(set(headers))


def comb_types_formatter(type_list):
    return ', '.join([x[0] for x in type_list]), list(
        set([header for x in type_list for header in x[1:]]))


COMB = Functor(
    'COMB',
    'Adapters::CombinationFromComposite',
    'Adapters.h',
    '''Re-constructs a 'combination' object from a 'composite' object and
    returns the result of applying the given functor to that 'combination'
    object.''',
    Params=[('Functor', "The functor to apply to the 'combination' object.",
             BoundFunctor),
            ('ChildContainers',
             "List of data locations where the child objects can be found.",
             list, comb_locations_formatter)],
    TemplateParams=[('ChildContainerTypes',
                     'List of types of the given child locations.',
                     comb_types_formatter)])

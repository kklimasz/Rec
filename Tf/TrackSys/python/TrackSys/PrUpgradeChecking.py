###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import GaudiSequencer
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm

MCCuts = {
    "Velo": {
        "01_velo": "isVelo",
        "02_long": "isLong",
        "03_long>5GeV": "isLong & over5",
        "04_long_strange": "isLong & strange",
        "05_long_strange>5GeV": "isLong & strange & over5",
        "06_long_fromB": "isLong & fromB",
        "07_long_fromB>5GeV": "isLong & fromB & over5",
        "08_long_electrons": "isLong & isElectron",
        "09_long_fromB_electrons": "isLong & isElectron & fromB",
        "10_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "Forward": {
        "01_long": "isLong",
        "02_long>5GeV": "isLong & over5",
        "03_long_strange": "isLong & strange",
        "04_long_strange>5GeV": "isLong & strange & over5",
        "05_long_fromB": "isLong & fromB",
        "06_long_fromB>5GeV": "isLong & fromB & over5",
        "07_long_electrons": "isLong & isElectron",
        "08_long_electrons_P>5GeV": "isLong & isElectron & over5",
        "09_long_fromB_electrons": "isLong & isElectron & fromB",
        "10_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "Up": {
        "01_velo": "isVelo",
        "02_velo+UT": "isVelo & isUT",
        "03_velo+UT>5GeV": "isVelo & isUT & over5",
        "04_velo+notLong": "isNotLong & isVelo ",
        "05_velo+UT+notLong": "isNotLong & isVelo & isUT",
        "06_velo+UT+notLong>5GeV": "isNotLong & isVelo & isUT & over5",
        "07_long": "isLong",
        "08_long>5GeV": "isLong & over5 ",
        "09_long_fromB": "isLong & fromB",
        "10_long_fromB>5GeV": "isLong & fromB & over5",
        "11_long_electrons": "isLong & isElectron",
        "12_long_fromB_electrons": "isLong & isElectron & fromB",
        "13_long_fromB_electrons_P>5GeV": "isLong & isElectron & over5 & fromB"
    },
    "T": {
        "01_hasT":
        "isSeed ",
        "02_long":
        "isLong",
        "03_long>5GeV":
        "isLong & over5",
        "04_long_fromB":
        "isLong & fromB",
        "05_long_fromB>5GeV":
        "isLong & fromB & over5",
        "06_UT+T_strange":
        "strange & isDown",
        "07_UT+T_strange>5GeV":
        "strange & isDown & over5",
        "08_noVelo+UT+T_strange":
        "strange & isDown & isNotVelo",
        "09_noVelo+UT+T_strange>5GeV":
        "strange & isDown & over5 & isNotVelo",
        "10_UT+T_SfromDB":
        "strange & isDown & ( fromB | fromD )",
        "11_UT+T_SfromDB>5GeV":
        "strange & isDown & over5 & ( fromB | fromD )",
        "12_noVelo+UT+T_SfromDB>5GeV":
        "strange & isDown & isNotVelo & over5 & ( fromB | fromD )"
    },
    "Down": {
        "01_UT+T":
        "isDown ",
        "02_UT+T>5GeV":
        "isDown & over5",
        "03_UT+T_strange":
        " strange & isDown",
        "04_UT+T_strange>5GeV":
        " strange & isDown & over5",
        "05_noVelo+UT+T_strange":
        " strange & isDown & isNotVelo",
        "06_noVelo+UT+T_strange>5GeV":
        " strange & isDown & over5 & isNotVelo",
        "07_UT+T_fromB":
        "isDown & fromB",
        "08_UT+T_fromB>5GeV":
        "isDown & fromB & over5",
        "09_noVelo+UT+T_fromB":
        "isDown & fromB & isNotVelo",
        "10_noVelo+UT+T_fromB>5GeV":
        "isDown & fromB & over5 & isNotVelo",
        "11_UT+T_SfromDB":
        " strange & isDown & ( fromB | fromD )",
        "12_UT+T_SfromDB>5GeV":
        " strange & isDown & over5 & ( fromB | fromD )",
        "13_noVelo+UT+T_SfromDB":
        " strange & isDown & isNotVelo & ( fromB | fromD )",
        "14_noVelo+UT+T_SfromDB>5GeV":
        " strange & isDown & isNotVelo & over5 & ( fromB | fromD ) ",
        "15_noVelo+UT+T_electrons":
        "isDown & isNotVelo & isElectron",
        "16_noVelo+UT+T_electrons_P>5GeV":
        "isDown & isNotVelo & isElectron & over5",
    },
    "UTForward": {
        "01_long": "isLong",
        "02_long>5GeV": "isLong & over5"
    },
    "UTDown": {
        "01_has seed": "isSeed",
        "02_has seed +noVelo, T+UT": "isSeed & isNotVelo & isDown",
        "03_down+strange": "strange & isDown",
        "04_down+strange+>5GeV": "strange & isDown & over5",
        "05_pi<-Ks<-B": "fromKsFromB",
        "06_pi<-Ks<-B+> 5 GeV": "fromKsFromB & over5"
    },
}

TriggerMCCuts = {
    "Velo": {
        "11_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "12_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "Forward": {
        "10_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "11_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "Up": {
        "14_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "15_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "New": {
        "long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTForward": {
        "03_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "04_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTDown": {
        "07_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "08_UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
    "UTNew": {
        "long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger",
        "UT_long_fromB_P>3GeV_Pt>0.5GeV": "isLong & fromB & trigger & isUT"
    },
}


def getMCCuts(key, triggerNumbers=False, etaRange25=True):
    cuts = dict(MCCuts[key]) if key in MCCuts else {}
    if triggerNumbers and key in TriggerMCCuts:
        cuts.update(TriggerMCCuts[key])
    if etaRange25:
        for name in cuts.keys():
            cuts[name] = cuts[name] + " & (MCETA > 2.0) & (MCETA < 5.0)"
    return cuts


# Has to mirror the enum HitType in PrTrackCounter.h
HitType = {"VP": 3, "UT": 4, "FT": 8}


def getHitTypeMask(dets):
    mask = 0
    for det in dets:
        if det not in HitType:
            log.warning(
                "Hit type to check unknown. Ignoring hit type, counting all.")
            return 0
        mask += HitType[det]

    return mask


def configureFilter(name, inputLocation, code):
    from Configurables import TrackListRefiner
    filterTracks = TrackListRefiner(
        name, inputLocation=inputLocation, outputLocation=inputLocation + name)
    from Configurables import LoKi__Hybrid__TrackSelector as LoKiTrackSelector
    filterTracks.addTool(LoKiTrackSelector, name="LokiTracksSelector")
    filterTracks.Selector = LoKiTrackSelector(name="LokiTracksSelector")
    filterTracks.Selector.Code = code
    filterTracks.Selector.StatPrint = True
    return filterTracks


def PrUpgradeChecking(defTracks={},
                      tracksToConvert=[],
                      defTracksConverted={},
                      extraTracksToCheck=[]):
    # match hits and tracks
    log.warning("Run upgrade checkers.")

    from Configurables import UnpackMCParticle, UnpackMCVertex, PrLHCbID2MCParticle, \
                              LHCbApp, VPCluster2MCParticleLinker, VPFullCluster2MCParticleLinker, VPClusFull
    # Check if VP is part of the list of detectors.
    withVP = False
    if hasattr(LHCbApp(), "Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            if 'VP' in LHCbApp().upgradeDetectors():
                withVP = True
    trackTypes = TrackSys().getProp("TrackTypes")

    prLHCbID2MCParticle = PrLHCbID2MCParticle()
    prLHCbID2MCParticle.VPFullClustersLocation = 'Raw/VP/FullClusters'
    prLHCbID2MCParticle.VPFullClustersLinkLocation = 'Link/' + prLHCbID2MCParticle.VPFullClustersLocation
    prLHCbID2MCParticle.UTHitsLocation = 'UT/UTHits'
    prLHCbID2MCParticle.UTHitsLinkLocation = 'Link/Raw/UT/Clusters'
    prLHCbID2MCParticle.FTLiteClustersLocation = 'Raw/FT/LiteClusters'
    prLHCbID2MCParticle.FTLiteClustersLinkLocation = 'Link/' + prLHCbID2MCParticle.FTLiteClustersLocation
    if "Truth" in trackTypes:
        truthSeq = GaudiSequencer("RecoTruthSeq")
        truthSeq.Members = [UnpackMCParticle(), UnpackMCVertex()]
        if withVP:
            truthSeq.Members += [VPCluster2MCParticleLinker()]
        truthSeq.Members += [prLHCbID2MCParticle]
    else:
        GaudiSequencer("MCLinksTrSeq").Members = []
        if withVP:
            if TrackSys().getProp("VPLinkingOfflineClusters"):
                #re-run VPClustering for FULL Velo cluster [ with info of pixels to clusters] and its own linker algorithm
                GaudiSequencer("MCLinksTrSeq").Members = [
                    VPClusFull(),
                    VPFullCluster2MCParticleLinker(),
                ]
            else:
                GaudiSequencer("MCLinksTrSeq").Members = [
                    VPCluster2MCParticleLinker(),
                ]
        GaudiSequencer("MCLinksTrSeq").Members.append(prLHCbID2MCParticle)

    newDefTracks = defTracks
    #Re-create a keyed container for a sub-set of tracks
    from Configurables import PrTrackAssociator
    from Configurables import LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track
    from Configurables import LHCb__Converters__Track__v1__fromVectorLHCbTrack as FromV1VectorV1Tracks
    for tracktype in defTracks:
        seq = GaudiSequencer(tracktype + "Checker")
        if tracktype in tracksToConvert:
            log.warning(
                "Pre-appending " + tracktype +
                " track copy in TES from vector<Track> to KeyedContainer to allow truth matching, this is an hack, not a final solution"
            )
            if "ParameterizedKalman" in TrackSys().getProp(
                    "ExpertTracking") and tracktype == "ForwardFastFitted":
                trconverter = FromV1VectorV1Tracks(tracktype + "Converter")
                # In this case the output at the normal location is Pr::Selection<v1::Track>, this gets us the (hidden)
                # underlying vector<v1::Track> storage instead. Hacky to the max.
                storage_suffix = 'Storage'
            else:
                trconverter = FromV2TrackV1Track(tracktype + "Converter")
                storage_suffix = ''
            trconverter.InputTracksName = defTracks[tracktype][
                "Location"] + storage_suffix
            trconverter.OutputTracksName = defTracksConverted[tracktype][
                "Location"]
            seq.Members += [trconverter]
            #insert in the sequence the converter for the tracks listed in UpgrateTracksToConvert
            trassociator = PrTrackAssociator(tracktype + "Associator")
            trassociator.SingleContainer = defTracksConverted[tracktype][
                "Location"]
            trassociator.OutputLocation = 'Link/' + defTracksConverted[
                tracktype]["Location"]
            #update the track locations for the PrTrackChecker
            seq.Members += [trassociator]
            GaudiSequencer("MCLinksTrSeq").Members += [seq]
            newDefTracks[tracktype]["Location"] = defTracksConverted[
                tracktype]["Location"]

    for tracktype in extraTracksToCheck:
        seq = GaudiSequencer(tracktype + "ExtraChecker")
        #insert in the sequence the converter for the tracks listed in UpgrateTracksToConvert
        trassociator = PrTrackAssociator(tracktype + "Associator")
        trassociator.SingleContainer = defTracks[tracktype]["Location"]
        trassociator.OutputLocation = 'Link/' + defTracks[tracktype]["Location"]
        #update the track locations for the PrTrackChecker
        seq.Members += [trassociator]
        GaudiSequencer("MCLinksTrSeq").Members += [seq]

    from Configurables import LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as FromVectorLHCbRecVertex
    vertexConverter = FromVectorLHCbRecVertex("VertexConverter")
    vertexConverter.InputVerticesName = "Rec/Vertex/Vector/Primary"
    vertexConverter.InputTracksName = defTracksConverted["Velo"]["Location"]
    vertexConverter.OutputVerticesName = "Rec/Vertex/Primary"
    GaudiSequencer("MCLinksTrSeq").Members += [vertexConverter]

    from Configurables import PrTrackChecker, PrUTHitChecker
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]

    trackTypes = TrackSys().getProp("TrackTypes")
    veloChecker = PrTrackChecker(
        "VeloMCCheck",
        Title="Velo",
        Tracks=newDefTracks["Velo"]["Location"],
        Links="Link/" + newDefTracks["Velo"]["Location"],
        TriggerNumbers=False,
        CheckNegEtaPlot=True,
        HitTypesToCheck=getHitTypeMask(["VP"]),
        MyCuts=getMCCuts("Velo", triggerNumbers=True, etaRange25=True))
    forwardChecker = PrTrackChecker(
        "ForwardMCCheck",
        Title="Forward",
        Tracks=newDefTracks["ForwardFast"]["Location"],
        Links="Link/" + newDefTracks["ForwardFast"]["Location"],
        TriggerNumbers=True,
        HitTypesToCheck=getHitTypeMask(["FT"]),
        MyCuts=getMCCuts("Forward", triggerNumbers=True, etaRange25=True))
    upChecker = PrTrackChecker(
        "UpMCCheck",
        Title="Upstream",
        Tracks=newDefTracks["Upstream"]["Location"],
        Links="Link/" + newDefTracks["Upstream"]["Location"],
        TriggerNumbers=True,
        HitTypesToCheck=getHitTypeMask(["UT"]),
        MyCuts=getMCCuts("Up", triggerNumbers=True, etaRange25=True))
    utforwardChecker = PrUTHitChecker(
        "UTForwardMCCheck",
        Title="UTForward",
        Tracks=newDefTracks["ForwardFast"]["Location"],
        Links="Link/" + newDefTracks["ForwardFast"]["Location"],
        TriggerNumbers=True,
        MyCuts=getMCCuts("UTForward", triggerNumbers=True, etaRange25=True))
    GaudiSequencer("CheckPatSeq").Members += [
        veloChecker, forwardChecker, upChecker, utforwardChecker
    ]
    if "TrBest" in TrackSys().getProp("TrackingSequence"):
        forwardChecker = PrTrackChecker(
            "ForwardBestMCCheck",
            Title="Forward",
            Tracks=newDefTracks["ForwardBest"]["Location"],
            Links="Link/" + newDefTracks["ForwardBest"]["Location"],
            HitTypesToCheck=getHitTypeMask(["FT"]),
            MyCuts=getMCCuts("Forward"))
        matchChecker = PrTrackChecker(
            "MatchMCCheck",
            Title="Match",
            Tracks=newDefTracks["Match"]["Location"],
            Links="Link/" + newDefTracks["Match"]["Location"],
            HitTypesToCheck=getHitTypeMask(["FT"]),
            MyCuts=getMCCuts("Forward"))
        tChecker = PrTrackChecker(
            "TMCCheck",
            Title="TTrack",
            Tracks=newDefTracks["Seeding"]["Location"],
            Links="Link/" + newDefTracks["Seeding"]["Location"],
            HitTypesToCheck=getHitTypeMask(["FT"]),
            MyCuts=getMCCuts("T"))
        downChecker = PrTrackChecker(
            "DownMCCheck",
            Title="Downstream",
            Tracks=newDefTracks["Downstream"]["Location"],
            Links="Link/" + newDefTracks["Downstream"]["Location"],
            HitTypesToCheck=getHitTypeMask(["UT"]),
            MyCuts=getMCCuts("Down"))
        filterTracksBest = configureFilter("FilterForBestMCCheck",
                                           newDefTracks["Best"]["Location"],
                                           "(~TrINVALID)")
        bestChecker = PrTrackChecker(
            "BestMCCheck",
            Title="Best",
            Tracks=filterTracksBest.outputLocation,
            Links="Link/" + newDefTracks["Best"]["Location"],
            HitTypesToCheck=getHitTypeMask(["VP", "UT", "FT"]),
            MyCuts=getMCCuts("Forward"))
        filterTracksLong = configureFilter("FilterForBestLongMCCheck",
                                           newDefTracks["Best"]["Location"],
                                           "(~TrINVALID) & TrLONG ")
        filterTracksLongAndGhostProb = configureFilter(
            "FilterForBestLongAndGhostProbMCCheck",
            newDefTracks["Best"]["Location"],
            "(~TrINVALID) & TrLONG & (TrGHOSTPROB < 0.5)")
        bestLongChecker = PrTrackChecker(
            "BestLongMCCheck",
            Title="BestLong",
            Tracks=filterTracksLong.outputLocation,
            Links="Link/" + newDefTracks["Best"]["Location"],
            HitTypesToCheck=getHitTypeMask(["VP", "UT", "FT"]),
            TrackType="Long",
            MyCuts=getMCCuts("Forward"))
        bestLongAndGhostProbChecker = PrTrackChecker(
            "BestLongAndGhostProbMCCheck",
            Title="BestLongAndGhostProb",
            Tracks=filterTracksLongAndGhostProb.outputLocation,
            Links="Link/" + newDefTracks["Best"]["Location"],
            HitTypesToCheck=getHitTypeMask(["VP", "UT", "FT"]),
            TrackType="Long",
            MyCuts=getMCCuts("Forward"))
        filterTracksDown = configureFilter("FilterForBestDownMCCheck",
                                           newDefTracks["Best"]["Location"],
                                           "(~TrINVALID) & TrDOWNSTREAM")
        filterTracksDownAndGhostProb = configureFilter(
            "FilterForBestDownAndGhostProbMCCheck",
            newDefTracks["Best"]["Location"],
            "(~TrINVALID) & TrDOWNSTREAM & (TrGHOSTPROB < 0.5)")
        bestDownChecker = PrTrackChecker(
            "BestDownMCCheck",
            Title="BestDown",
            Tracks=filterTracksDown.outputLocation,
            Links="Link/" + newDefTracks["Best"]["Location"],
            HitTypesToCheck=getHitTypeMask(["UT", "FT"]),
            TrackType="Downstream",
            MyCuts=getMCCuts("Down"))
        bestDownAndGhostProbChecker = PrTrackChecker(
            "BestDownAndGhostProbMCCheck",
            Title="BestDownAndGhostProb",
            Tracks=filterTracksDownAndGhostProb.outputLocation,
            Links="Link/" + newDefTracks["Best"]["Location"],
            HitTypesToCheck=getHitTypeMask(["UT", "FT"]),
            TrackType="Downstream",
            MyCuts=getMCCuts("Down"))
        utforwardChecker = PrUTHitChecker(
            "UTForwardBestMCCheck",
            Title="UTForward",
            Tracks=newDefTracks["ForwardBest"]["Location"],
            Links="Link/" + newDefTracks["ForwardBest"]["Location"],
            MyCuts=getMCCuts("UTForward"))
        utmatchChecker = PrUTHitChecker(
            "UTMatchMCCheck",
            Title="UTMatch",
            Tracks=newDefTracks["Match"]["Location"],
            Links="Link/" + newDefTracks["Match"]["Location"],
            MyCuts=getMCCuts("UTForward"))
        utdownChecker = PrUTHitChecker(
            "UTDownMCCheck",
            Title="UTDownstream",
            Tracks=newDefTracks["Downstream"]["Location"],
            Links="Link/" + newDefTracks["Downstream"]["Location"],
            MyCuts=getMCCuts("UTDown"))
        GaudiSequencer("CheckPatSeq").Members += [
            forwardChecker, matchChecker, tChecker, downChecker,
            filterTracksBest, filterTracksLong, filterTracksLongAndGhostProb,
            filterTracksDown, filterTracksDownAndGhostProb, bestChecker,
            bestLongChecker, bestLongAndGhostProbChecker, bestDownChecker,
            bestDownAndGhostProbChecker, utforwardChecker, utmatchChecker,
            utdownChecker
        ]

    #dedicated Velo Checking
    if "Velo" in newDefTracks:
        checkervelo = GetVeloChecker(newDefTracks["Velo"]["Location"])
        GaudiSequencer("CheckPatSeq").Members += [checkervelo]

    #Track resolution checker fast stage
    from Configurables import TrackResChecker
    if not TrackSys().getProp("VeloUpgradeOnly"):
        trChecker = TrackResChecker("TrackResCheckerFast")
        trChecker.HistoPrint = False
        trChecker.TracksInContainer = defTracksConverted["ForwardFastFitted"][
            "Location"]
        trChecker.LinkerInTable = "Link/" + defTracksConverted[
            "ForwardFastFitted"]["Location"]
        GaudiSequencer("CheckPatSeq").Members += [trChecker]
        from Configurables import PrimaryVertexChecker
        PVCheck = PrimaryVertexChecker("PVChecker")
        if not PVCheck.isPropertySet('inputVerticesName'):
            PVCheck.inputVerticesName = "Rec/Vertex/Primary"
        if not PVCheck.isPropertySet('matchByTracks'):
            PVCheck.matchByTracks = False
        if not PVCheck.isPropertySet('nTracksToBeRecble'):
            PVCheck.nTracksToBeRecble = 4
        if not PVCheck.isPropertySet('inputTracksName'):
            PVCheck.inputTracksName = defTracksConverted["VeloFitted"][
                "Location"]
        GaudiSequencer("CheckPatSeq").Members += [PVCheck]


def GetVeloChecker(velotracklocation):
    from Configurables import PrTrackChecker
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]
    cutDictionary = {
        "Electron": "(isElectron)",
        "notElectron": "(isNotElectron)",
        "Forward": "(MCETA>0)",
        "Backward": "(MCETA<0)",
        "Eta25": "(MCETA>2.0) & (MCETA<5.0)",
        "p>5GeV": "(MCP>5000)",
        "p>3GeV": "(MCP>3000)",
        "p<5GeV": "(MCP<5000)",
        "pt>400MeV": "(MCPT>400)",
        "pt>600MeV": "(MCPT>600)",
        "pt>800MeV": "(MCPT>800)",
        "pt>1000MeV": "(MCPT>1000)",
        "Velo": "(isVelo)",
        "Long": "(isLong)",
        "FromB": "(fromB)",
        "FromD": "(fromD)",
        "strange": "(strange)"
    }
    all_of = lambda args: " & ".join([cutDictionary[i] for i in args])
    return PrTrackChecker(
        "VeloFullMCCheck",
        Title="Velo",
        Tracks=velotracklocation,
        Links="Link/" + velotracklocation,
        TriggerNumbers=False,
        CheckNegEtaPlot=True,
        VetoElectrons=False,
        HitTypesToCheck=getHitTypeMask(["VP"]),
        MyCuts={
            "01_notElectron_Velo":
            all_of(["notElectron", "Velo"]),
            "02_notElectron_Velo_Forward":
            all_of(["notElectron", "Velo", "Forward"]),
            "03_notElectron_Velo_Backward":
            all_of(["notElectron", "Velo", "Backward"]),
            "04_notElectron_Velo_Eta25":
            all_of(["notElectron", "Velo", "Eta25"]),
            "05_notElectron_Long_Eta25":
            all_of(["notElectron", "Long", "Eta25"]),
            "06_notElectron_Long_Eta25 p>5GeV":
            all_of(["notElectron", "Long", "Eta25", "p>5GeV"]),
            "07_notElectron_Long_Eta25 p<5GeV":
            all_of(["notElectron", "Long", "Eta25", "p<5GeV"]),
            "08_notElectron_Long_Eta25 p>3GeV pt>400MeV":
            all_of(["notElectron", "Long", "Eta25", "p>3GeV", "pt>400MeV"]),
            "09_notElectron_Long_FromB_Eta25":
            all_of(["notElectron", "Long", "Eta25", "FromB"]),
            "10_notElectron_Long_FromB_Eta25 p>5GeV":
            all_of(["notElectron", "Long", "Eta25", "FromB", "p>5GeV"]),
            "11_notElectron_Long_FromB_Eta25 p<5GeV":
            all_of(["notElectron", "Long", "Eta25", "FromB", "p<5GeV"]),
            "12_notElectron_Long_FromB_Eta25 p>3GeV pt>400MeV":
            all_of([
                "notElectron", "Long", "Eta25", "FromB", "p>3GeV", "pt>400MeV"
            ]),
            "13_notElectron_Long_FromD_Eta25":
            all_of(["notElectron", "Long", "Eta25", "FromD"]),
            "14_notElectron_Long_FromD_Eta25 p>5GeV":
            all_of(["notElectron", "Long", "Eta25", "FromD", "p>5GeV"]),
            "15_notElectron_Long_FromD_Eta25 p<5GeV":
            all_of(["notElectron", "Long", "Eta25", "FromD", "p<5GeV"]),
            "16_notElectron_Long_FromD_Eta25 p>3GeV pt>400MeV":
            all_of([
                "notElectron", "Long", "Eta25", "FromD", "p>3GeV", "pt>400MeV"
            ]),
            "17_notElectron_Long_strange_Eta25":
            all_of(["notElectron", "Long", "Eta25", "strange"]),
            "18_notElectron_Long_strange_Eta25 p>5GeV":
            all_of(["notElectron", "Long", "Eta25", "strange", "p>5GeV"]),
            "19_notElectron_Long_strange_Eta25 p<5GeV":
            all_of(["notElectron", "Long", "Eta25", "strange", "p<5GeV"]),
            "20_notElectron_Long_strange_Eta25 p>3GeV pt>400MeV":
            all_of([
                "notElectron", "Long", "Eta25", "strange", "p>3GeV",
                "pt>400MeV"
            ]),
            "21_Electron_Velo":
            all_of(["Electron", "Velo"]),
            "22_Electron_Velo_Forward":
            all_of(["Electron", "Velo", "Forward"]),
            "23_Electron_Velo_Backward":
            all_of(["Electron", "Velo", "Backward"]),
            "24_Electron_Velo_Eta25":
            all_of(["Electron", "Velo", "Eta25"]),
            "25_Electron_Long_Eta25":
            all_of(["Electron", "Long", "Eta25"]),
            "26_Electron_Long_Eta25 p>5GeV":
            all_of(["Electron", "Long", "Eta25", "p>5GeV"]),
            "27_Electron_Long_Eta25 p<5GeV":
            all_of(["Electron", "Long", "Eta25", "p<5GeV"]),
            "28_Electron_Long_Eta25 p>3GeV pt>400MeV":
            all_of(["Electron", "Long", "Eta25", "p>3GeV", "pt>400MeV"]),
            "29_Electron_Long_FromB_Eta25":
            all_of(["Electron", "Long", "Eta25", "FromB"]),
            "30_Electron_Long_FromB_Eta25 p>5GeV":
            all_of(["Electron", "Long", "Eta25", "FromB", "p>5GeV"]),
            "31_Electron_Long_FromB_Eta25 p<5GeV":
            all_of(["Electron", "Long", "Eta25", "FromB", "p<5GeV"]),
            "32_Electron_Long_FromB_Eta25 p>3GeV pt>400MeV":
            all_of(
                ["Electron", "Long", "Eta25", "FromB", "p>3GeV", "pt>400MeV"]),
            "33_Electron_Long_FromD_Eta25":
            all_of(["Electron", "Long", "Eta25", "FromD"]),
            "34_Electron_Long_FromD_Eta25 p>5GeV":
            all_of(["Electron", "Long", "Eta25", "FromD", "p>5GeV"]),
            "35_Electron_Long_FromD_Eta25 p<5GeV":
            all_of(["Electron", "Long", "Eta25", "FromD", "p<5GeV"]),
            "36_Electron_Long_FromD_Eta25 p>3GeV pt>400MeV":
            all_of(
                ["Electron", "Long", "Eta25", "FromD", "p>3GeV", "pt>400MeV"]),
            "37_Electron_Long_strange_Eta25":
            all_of(["Electron", "Long", "Eta25", "strange"]),
            "38_Electron_Long_strange_Eta25 p>5GeV":
            all_of(["Electron", "Long", "Eta25", "strange", "p>5GeV"]),
            "39_Electron_Long_strange_Eta25 p<5GeV":
            all_of(["Electron", "Long", "Eta25", "strange", "p<5GeV"]),
            "40_Electron_Long_strange_Eta25 p>3GeV pt>400MeV":
            all_of([
                "Electron", "Long", "Eta25", "strange", "p>3GeV", "pt>400MeV"
            ])
        })

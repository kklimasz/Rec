/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "PatKernel/IPatMvaClassifier.h"

#include <string>
#include <vector>

namespace PatBBDTSeedClassifier_details {
  // static constexpr int s_featuresNumber = s_binsEdgeMap.size();
  // static constexpr int s_binPerFeature = s_binsEdgeMap[0].second.size();
  static constexpr int s_nBins = 1048576; //  ipow( s_binPerFeature+1, s_featuresNumber );
} // namespace PatBBDTSeedClassifier_details

class PatBBDTSeedClassifier : public extends<GaudiTool, IPatMvaClassifier> {
public:
  using base_class::base_class;

  StatusCode initialize() override;

  // TODO: even better, pass a vector of LHCb::Track, and return a vector
  //      of MVA values
  // TODO: even better, pass a vector of const LHCb::Track*, and add a property to this
  //      tool which specifies a cut on the BDT (or pass it as argument), and return a
  //      vector of the const LHCb::Track* which survive the cut...
  double getMvaValue( const LHCb::Track& track ) const override;

private:
  std::vector<float> m_tupleClassifier;

  Gaudi::Property<std::string> m_lookupTableLocation{this, "LookupTableLocation", "PatLLT_BBDT_tuple_DR_v1.csv"};
};

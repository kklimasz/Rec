/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "OTDet/DeOTDetector.h"
// from TrackEvent
#include "Event/StateParameters.h"
#include "Event/Track.h"

// local
#include "PatSeeding.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PatSeeding
//
// 2006-10-13 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PatSeeding )

//=============================================================================
// Initialization
//=============================================================================
StatusCode PatSeeding::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  m_seedingTool = tool<IPatSeedingTool>( "PatSeedingTool", "PatSeedingTool", this );

  if ( m_doTiming ) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" );
    m_timerTool->increaseIndent();
    m_seedTime = m_timerTool->addTimer( "Internal PatSeeding" );
    m_timerTool->decreaseIndent();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PatSeeding::execute() {
  if ( m_doTiming ) m_timerTool->start( m_seedTime );

  LHCb::Tracks* outputTracks = new LHCb::Tracks();

  m_seedingTool->prepareHits();
  StatusCode sc = m_seedingTool->performTracking( *outputTracks );

  put( outputTracks, m_outputTracksName );

  if ( m_doTiming ) m_timerTool->stop( m_seedTime );

  return sc;
}

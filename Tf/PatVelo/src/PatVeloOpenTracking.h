/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATVELOOPENTRACKING_H
#define PATVELOOPENTRACKING_H 1

#include "PatVeloOpenTrack.h"
#include "PatVeloPhiHitManager.h"
#include "PatVeloRHitManager.h"
#include "PatVeloTrackTool.h"

#include "PatKernel/IPatDebugTool.h"

#include "GaudiAlg/GaudiAlgorithm.h"

/** @class PatVeloOpenTracking PatVeloOpenTracking.h
 *
 *
 *  @author Olivier Callot
 *  @date   2007-02-27
 */
class PatVeloOpenTracking : public GaudiAlgorithm {
public:
  /// Standard constructor
  PatVeloOpenTracking( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PatVeloOpenTracking(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  void printRCoords( Tf::PatVeloRHitManager::Station* station, int zone, std::string title ) {
    std::vector<Tf::PatVeloRHit*>::const_iterator itC;
    info() << " ... coordinates in " << title << " sensor " << station->sensor()->sensorNumber() << " zone " << zone
           << " ..." << endmsg;
    for ( itC = station->hits( zone ).begin(); station->hits( zone ).end() != itC; ++itC ) { printCoord( *itC, " " ); }
  }

  void printPhiCoords( Tf::PatVeloPhiHitManager::Station* station, int zone, std::string title ) {
    std::vector<Tf::PatVeloPhiHit*>::const_iterator itC;
    info() << " ... coordinates in " << title << " sensor " << station->sensor()->sensorNumber() << " zone " << zone
           << " ..." << endmsg;
    for ( itC = station->hits( zone ).begin(); station->hits( zone ).end() != itC; ++itC ) { printCoord( *itC, " " ); }
  }

  template <class TYPE>
  void printCoord( const TYPE* hit, std::string title ) {
    info() << "  " << title
           << format( " sensor %3d z%7.1f strip%5d coord%10.5f used%2d ", hit->sensorNumber(), hit->z(),
                      hit->hit()->strip(), hit->hit()->coordHalfBox(), hit->hit()->isUsed() );
    LHCb::LHCbID myId = hit->hit()->lhcbID();
    if ( 0 != m_debugTool ) m_debugTool->printKey( info(), myId );
    info() << endmsg;
  }

  template <class TYPE>
  bool matchKey( TYPE* coord ) {
    if ( 0 == m_debugTool ) return false;
    LHCb::LHCbID id = coord->hit()->lhcbID();
    return m_debugTool->matchKey( id, m_wantedKey );
  }

  void matchShared( PatVeloOpenTrack& track, std::vector<PatVeloOpenTrack>& tracks );

  void tagManyUsed( PatVeloOpenTrack& track );

private:
  Tf::PatVeloRHitManager*   m_rHitManager;
  Tf::PatVeloPhiHitManager* m_phiHitManager;
  Tf::PatVeloTrackTool*     m_trackTool;
  IPatDebugTool*            m_debugTool;

  DeVelo* m_velo; ///< Detector element

  std::string m_debugToolName;
  int         m_wantedKey;

  double m_rMatchTol;
  double m_rExtrapTol;
  double m_phiMatchTol;
  double m_phiExtrapTol;
  double m_phiAngularTol;
  double m_maxSlope;
  double m_maxChi2;
  int    m_maxMissedR;

  std::string m_rHitManagerName;   ///< name of the R hit manager instance
  std::string m_phiHitManagerName; ///< name of the Phi hit manager instance
  std::string m_trackToolName;     /// name of the track tool instance used here
};
#endif // PATVELOOPENTRACKING_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FASTPVFINDER_H
#define FASTPVFINDER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FastPVFinder FastPVFinder.h
 *
 *
 *  @author Olivier Callot
 *  @date   2011-11-15
 */
class FastPVFinder : public GaudiAlgorithm {
public:
  /// Standard constructor
  FastPVFinder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  StatusCode updateBeamSpot();

private:
  std::string m_inputLocation;
  std::string m_outputLocation;
  std::string m_beamSpotCondition;

  double       m_maxIPToBeam;
  unsigned int m_minTracksInPV;
  double       m_maxDeltaZ;
  double       m_maxChi2ToAdd;
  double       m_maxChi2Fit;
  double       m_maxChi2PerDoF;

  double m_xBeam;
  double m_yBeam;

  bool m_debug;
};
#endif // FASTPVFINDER_H

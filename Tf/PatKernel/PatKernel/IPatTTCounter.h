/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATKERNEL_IPATTTCOUNTER_H
#define PATKERNEL_IPATTTCOUNTER_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace LHCb {
  class MCParticle;
  class LHCbID;
} // namespace LHCb

static const InterfaceID IID_IPatTTCounter( "IPatTTCounter", 1, 1 );

/** @class IPatTTCounter IPatTTCounter.h PatKernel/IPatTTCounter.h
 *
 *
 *  @author Wenbin Qian
 *  @date   2011-03-21
 */
class IPatTTCounter : virtual public IAlgTool {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IPatTTCounter; }

  virtual void initEvent() = 0;

  virtual void count( const LHCb::MCParticle* part, std::vector<bool> flags, std::vector<LHCb::LHCbID>& ids ) = 0;

  virtual void setContainer( std::string name ) = 0;

  virtual void addSelection( std::string name ) = 0;

  virtual void printStatistics() = 0;

protected:
private:
};
#endif // PATKERNEL_IPATTTCOUNTER_H

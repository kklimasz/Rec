/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATKERNEL_IPATDEBUGTRACKTOOL_H
#define PATKERNEL_IPATDEBUGTRACKTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IPatDebugTrackTool( "IPatDebugTrackTool", 1, 0 );

/** @class IPatDebugTrackTool IPatDebugTrackTool.h PatKernel/IPatDebugTrackTool.h
 *  Interface to get truth information on a track
 *
 *  @author Olivier Callot
 *  @date   2009-03-31
 */
class IPatDebugTrackTool : virtual public IAlgTool {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IPatDebugTrackTool; }

  virtual std::vector<int> keys( const LHCb::Track* track ) = 0;

protected:
private:
};
#endif // PATKERNEL_IPATDEBUGTRACKTOOL_H

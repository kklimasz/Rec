/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATTSTATIONHITMANAGER_H
#define PATTSTATIONHITMANAGER_H 1
// Include files
#include "PatKernel/PatForwardHit.h"
#include "TfKernel/TStationHitManager.h"

/** @class PatTStationHitManager PatTStationHitManager.h
 *
 *  T station hit manager for Pat.
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */

struct PatTStationHitManager : Tf::TStationHitManager<PatForwardHit> {
  using Tf::TStationHitManager<PatForwardHit>::TStationHitManager;
  void prepareHits() const override;
};

#endif // PATTSTATIONHITMANAGER_H

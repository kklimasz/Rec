/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "boost/dynamic_bitset.hpp"

// local
#include "OTHitCleaner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : OTHitCleaner
//
// 2007-07-20 : Chris Jones
//-----------------------------------------------------------------------------

namespace {

  // keep the items which have a bit set in the mask
  template <typename Container, typename Block, typename Allocator>
  void keep_masked( Container& c, boost::dynamic_bitset<Block, Allocator> keep ) {
    assert( c.size() == keep.size() );
    constexpr auto npos = boost::dynamic_bitset<Block, Allocator>::npos;
    using size_type     = typename boost::dynamic_bitset<Block, Allocator>::size_type;

    //@TODO: annoyingly, dynamic_bitset can only find the next 'set' bit,
    //       but we also need the next 'unset' bit.
    //       so we need an inverted copy of the mask...
    auto erase = ~keep;
    // initialize two cursors: in the container, and the mask
    auto      current      = c.begin();
    size_type current_mask = 0;
    // find first one to erase
    auto index = erase.find_first();
    while ( index != npos ) {
      current += index - current_mask;     // move container cursor to first item to erase
      auto next = keep.find_next( index ); // find next one to keep in mask
      if ( next == npos ) {
        current = c.erase( current, c.end() );
        break;
      }
      // move both cursors forward
      current      = c.erase( current, std::next( current, next - index ) );
      current_mask = next;
      // find next one to erase
      index = erase.find_next( current_mask );
    }
  }

} // namespace

using namespace Tf;

// Declaration of the Tool Factory
DECLARE_COMPONENT( OTHitCleaner )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OTHitCleaner::OTHitCleaner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareInterface<IOTHitCleaner>( this );
}

//=============================================================================
// initialize
//=============================================================================
StatusCode OTHitCleaner::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  info() << "Max OT cluster size     = " << m_maxClusterSize.value() << endmsg;
  info() << "Max OT module occupancy = " << m_maxModuleOcc.value() << endmsg;

  return sc;
}

OTHits OTHitCleaner::cleanHits( const OTHits::const_iterator begin, const OTHits::const_iterator end ) const {
  // first remove hot modules (faster)
  auto output = removeHotModules( begin, end );

  // clean out large clusters
  if ( m_maxClusterSize > 0 ) removeClusters( output );

  // how many hits where cleaned
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Selected " << output.size() << " out of " << std::distance( begin, end ) << " OTHits" << endmsg;
  return output;
}

void OTHitCleaner::removeClusters( OTHits& output ) const {
  // what makes this complicated is that it needs to go by mono
  // layer. I'll try to do it such that I don't need to think about
  // it. The result is pretty horrible.

  auto begin = output.begin();
  auto end   = output.end();

  auto                               currentend = begin;
  size_t                             numhits    = std::distance( begin, end );
  boost::dynamic_bitset<>            selectedhits( numhits );
  std::array<std::vector<size_t>, 2> indexmono;
  indexmono[0].reserve( 64 );
  indexmono[1].reserve( 64 );

  while ( currentend != end ) {
    // first select hits in this module
    auto currentbegin = currentend;
    currentend =
        std::find_if( std::next( currentend ), end, [module_addr = &( *currentbegin )->module()]( const OTHit* hit ) {
          return &hit->module() != module_addr;
        } );

    // now divide the hits by mono layer.
    indexmono[0].clear();
    indexmono[1].clear();
    for ( auto it = currentbegin; it != currentend; ++it )
      indexmono[( *it )->monolayer()].push_back( std::distance( begin, it ) );

    // process each monolayer
    for ( const auto& index : indexmono ) {
      if ( index.empty() ) continue;
      auto clusterbeginindex = index.begin();
      auto clusterendindex   = index.begin();
      auto endindex          = index.end();
      auto currenthit        = std::next( begin, *clusterendindex );
      while ( clusterendindex != endindex ) {
        bool endofcluster = ++clusterendindex == endindex;
        if ( !endofcluster ) {
          auto nexthit = std::next( begin, *clusterendindex );
          endofcluster = abs( int( ( *currenthit )->straw() ) - int( ( *nexthit )->straw() ) ) != 1;
          currenthit   = nexthit;
        }
        if ( endofcluster ) {
          // flip the bools if the cluster is accepted
          if ( std::distance( clusterbeginindex, clusterendindex ) <= m_maxClusterSize )
            for ( ; clusterbeginindex != clusterendindex; ++clusterbeginindex ) selectedhits.set( *clusterbeginindex );
          clusterbeginindex = clusterendindex;
        }
      }
    }
  }
  // now we have a bitmask of elements to keep.
  keep_masked( output, selectedhits );
}

OTHits OTHitCleaner::removeHotModules( const OTHits::const_iterator begin, const OTHits::const_iterator end ) const {
  OTHits output;
  output.reserve( std::distance( begin, end ) );
  auto currentend = begin;
  while ( currentend != end ) {
    // select hits in current module
    auto        currentbegin = currentend;
    const auto& module       = ( *currentbegin )->module();
    currentend               = std::find_if( std::next( currentend ), end,
                               [module_addr = &module]( const OTHit* hit ) { return &hit->module() != module_addr; } );
    auto nhits               = std::distance( currentbegin, currentend );
    auto nchan               = module.nChannels();
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Module " << module.uniqueModule() << " has occupancy " << double( nhits ) / double( nchan )
                << endmsg;

    if ( nhits <= m_maxModuleOcc * nchan )
      output.insert( output.end(), currentbegin, currentend );
    else if ( msgLevel( MSG::DEBUG ) )
      debug() << "DeOTModule " << module.uniqueModule()
              << " suppressed due to high occupancy = " << double( nhits ) / double( nchan ) << " > " << m_maxModuleOcc
              << " maximum" << endmsg;
  }
  return output;
}

//=============================================================================

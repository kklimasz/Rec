/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file HitExtension.h
 *
 *  Header file for track find class Tf::HitExtension
 *
 *  CVS Log :-
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-05-30
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_HITEXTENSION_H
#define TFKERNEL_HITEXTENSION_H 1

namespace Tf {

  /** @class HitExtension
   *  Base class from which which algorithm specific hit classes can be derived.
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   **/

  template <class Hit>
  class HitExtension {

  public:
    /// Constructor from pointer to a Hit
    HitExtension( const Hit* hit ) : m_hit( hit ) {}

    /// Constructor from reference to a Hit
    HitExtension( const Hit& hit ) : m_hit( &hit ) {}

    /// Destructor
    ~HitExtension() = default;

    /// Access to the underlying common Hit object
    const Hit* hit() const { return m_hit; }

  private:
    const Hit* m_hit; ///< Pointer to the underlying hit object
  };

} // namespace Tf

#endif // TFKERNEL_HITEXTENSION_H

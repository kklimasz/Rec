/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TFKERNEL_RECOFUNCS_H
#define TFKERNEL_RECOFUNCS_H 1

//-----------------------------------------------------------------------------
/** @file RecoFuncs.h
 *
 *  Collection of functions frequently used in all
 *  pattern reco algorithms
 *
 *  @authors S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-03
 **/
//-----------------------------------------------------------------------------

#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "TfKernel/OTHit.h"
#include <algorithm>

namespace Tf {
  /** @class compByX TfKernel/RecoFuncs.h
   *  Binary function to compare a hit to a given x value and return true if hit is upstream of that point.
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   */
  class compByX {
  public:
    /** Comparison operator
     *  @param[in] obj     The Hit object to test
     *  @param[in] testVal The test value of x
     *  @return boolean
     *  @retval TRUE hit is upstream of testVal
     *  @retval TRUE hit is downstream of testVal
     */
    template <class Hit>
    bool operator()( const Hit* obj, const double testVal ) const {
      return obj->hit()->xAtYEq0() < testVal;
    }
    template <class Hit>
    bool operator()( const double testVal, const Hit* obj ) const {
      return testVal < obj->hit()->xAtYEq0();
    }
  };

  /** @class increasingByProjection TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by projection
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  projection should be ordered by LHCbID
   */
  template <bool forceOrder = true>
  struct increasingByProjection {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by projection
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( lhs->projection() < rhs->projection() ) return true;
      if ( rhs->projection() < lhs->projection() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <>
  struct increasingByProjection<false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by projection
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      return lhs->projection() < rhs->projection();
    }
  };

  /** @class increasingByZ TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by z
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  z should be ordered by LHCbID
   */
  template <bool forceOrder = true>
  struct increasingByZ {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by z
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( lhs->z() < rhs->z() ) return true;
      if ( rhs->z() < lhs->z() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <>
  struct increasingByZ<false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by z
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      return lhs->z() < rhs->z();
    }
  };

  /** @class increasingByX TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by x
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  x should be ordered by LHCbID
   */
  template <bool forceOrder = true>
  struct increasingByX {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( lhs->x() < rhs->x() ) return true;
      if ( rhs->x() < lhs->x() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <>
  struct increasingByX<false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      return lhs->x() < rhs->x();
    }
  };

  /** @class increasingByXAtYEq0 TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by x at the point y=0
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  x at y= 0 should be ordered by LHCbID
   */
  template <bool forceOrder = true>
  struct increasingByXAtYEq0 {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x at the point y=0
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( lhs->hit()->xAtYEq0() < rhs->hit()->xAtYEq0() ) return true;
      if ( rhs->hit()->xAtYEq0() < lhs->hit()->xAtYEq0() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <>
  struct increasingByXAtYEq0<false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x at the point y=0
     */
    template <typename Hit>
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      return lhs->xAtYEq0() < rhs->xAtYEq0();
    }
  };

  /** @class decreasingByProjection TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by projection
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  projection should be ordered by LHCbID
   */
  template <class Hit, bool forceOrder = true>
  struct decreasingByProjection {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by projection
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( rhs->projection() < lhs->projection() ) return true;
      if ( lhs->projection() < rhs->projection() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <class Hit>
  struct decreasingByProjection<Hit, false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by projection
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const { return rhs->projection() < lhs->projection(); }
  };

  /** @class decreasingByZ TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by z
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  z should be ordered by LHCbID
   */
  template <class Hit, bool forceOrder = true>
  struct decreasingByZ {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by z
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( rhs->z() < lhs->z() ) return true;
      if ( lhs->z() < rhs->z() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <class Hit>
  struct decreasingByZ<Hit, false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by z
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const { return rhs->z() < lhs->z(); }
  };

  /** @class decreasingByX TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by x
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  x should be ordered by LHCbID
   */
  template <class Hit, bool forceOrder = true>
  struct decreasingByX {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( rhs->x() < lhs->x() ) return true;
      if ( lhs->x() < rhs->x() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <class Hit>
  struct decreasingByX<Hit, false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const { return rhs->x() < lhs->x(); }
  };

  /** @class decreasingByXAtYEq0 TfKernel/RecoFuncs.h
   *  Binary sorting function to sort hits by x at the point y=0
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   *  @author M. Schiller
   *  @date 2012-02-07
   *
   *  the template parameter forceOrder specifies if hits with same
   *  x at y= 0 should be ordered by LHCbID
   */
  template <class Hit, bool forceOrder = true>
  struct decreasingByXAtYEq0 {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x at the point y=0
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const {
      if ( rhs->hit()->xAtYEq0() < lhs->hit()->xAtYEq0() ) return true;
      if ( lhs->hit()->xAtYEq0() < rhs->hit()->xAtYEq0() ) return false;
      return lhs->hit()->lhcbID() < rhs->hit()->lhcbID();
    }
  };

  template <class Hit>
  struct decreasingByXAtYEq0<Hit, false> {
    /** Comparison operator
     *  @param[in] lhs Hit object on lhs of < operator
     *  @param[in] rhs Hit object on rhs of < operator
     *  @return boolean indicating the sorting of lhs and rhs by x at the point y=0
     */
    bool operator()( const Hit* lhs, const Hit* rhs ) const { return rhs->xAtYEq0() < lhs->xAtYEq0(); }
  };

  template <class Hit>
  inline constexpr double updateTTHitForTrack( Hit&& hit, const double y0, const double dyDz ) {
    return updateNonOTHitForTrack( std::forward<Hit>( hit ), y0, dyDz );
  }

  template <class Hit>
  inline constexpr double updateUTHitForTrack( Hit&& hit, const double y0, const double dyDz ) {
    return updateNonOTHitForTrack( std::forward<Hit>( hit ), y0, dyDz );
  }

  template <class Hit>
  inline double updateNonOTHitForTrack( Hit* hit, const double y0, const double dyDz ) {
    auto y = ( y0 + dyDz * hit->hit()->zAtYEq0() ) / ( 1. - hit->hit()->dzDy() * dyDz );
    hit->setZ( hit->hit()->z( y ) );
    hit->setX( hit->hit()->x( y ) );
    return y;
  }

  template <class Hit>
  inline double updateOTHitForTrack( Hit* hit, const double y0, const double dyDz ) {
    auto y = updateNonOTHitForTrack( hit, y0, dyDz );
    hit->setDriftDistance( hit->hit()->othit()->untruncatedDriftDistance( y ) );
    return y;
  }

  template <class Hit>
  inline double approxUpdateOTHitForTrack( Hit* hit, const double y0, const double dyDz ) {
    auto y = updateNonOTHitForTrack( hit, y0, dyDz );
    hit->setDriftDistance( hit->hit()->othit()->approxUntruncatedDriftDistance( y ) );
    return y;
  }

  template <class Hit>
  inline double updateHitForTrack( Hit* hit, const double y0, const double dyDz ) {
    auto             y     = updateNonOTHitForTrack( hit, y0, dyDz );
    const Tf::OTHit* otHit = hit->hit()->othit();
    if ( otHit ) hit->setDriftDistance( otHit->untruncatedDriftDistance( y ) );
    return y;
  }

  template <class Hit>
  inline double approxUpdateHitForTrack( Hit* hit, const double y0, const double dyDz ) {
    auto             y     = updateNonOTHitForTrack( hit, y0, dyDz );
    const Tf::OTHit* otHit = hit->hit()->othit();
    if ( otHit ) hit->setDriftDistance( otHit->approxUntruncatedDriftDistance( y ) );
    return y;
  }

  /** Function to find the intersection of a (line) hit with a plane
   *  @param[in] hit The hit object pointer
   *  @param[in] plane The plane to find the intersection with
   *  @param[out] intersect The interestion point
   *  @return boolean indicating the status of the interestion
   *  @retval TRUE An intersection point was successfully found
   *  @retval FALSE No intersection point was found
   */
  template <class Hit>
  inline bool intersection( const Hit* hit, const Gaudi::Plane3D& plane, Gaudi::XYZPoint& intersect ) {
    double mu = 0.0;
    return Gaudi::Math::intersection( *hit->hit(), plane, intersect, mu );
  }

} // namespace Tf

#endif // TFKERNEL_RECOFUNCS_H

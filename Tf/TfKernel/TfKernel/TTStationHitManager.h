/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TTStationHitManager.h
 *
 *  Header file for class : Tf::TTStationHitManager
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_TTSTATIONHITMANAGER_H
#define TFKERNEL_TTSTATIONHITMANAGER_H 1

// Include files
#include <functional>
#include <memory>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/ToolHandle.h"

#include "TfKernel/HitExtension.h"
#include "TfKernel/ITTHitCreator.h"
#include "TfKernel/IndexedHitContainer.h"
#include "TfKernel/LineHit.h"
#include "TfKernel/RecoFuncs.h"
#include "TfKernel/RegionID.h"
#include "TfKernel/TfIDTypes.h"

// boost
#include "boost/iterator/transform_iterator.hpp"

/// Static interface ID
static const InterfaceID IID_TTStationHitManager( "TTStationHitManager", 2, 0 );

namespace Tf {

  /** @class TTStationHitManager TTStationHitManager.h TfKernel/TTStationHitManager.h
   *
   *  TT station hit manager. Used to manage extended hit objects for the TT
   *  Stations.
   *
   *  Methods are provided to return the hits in a selected part of the detectors.
   *  E.g.
   *
   *  @code
   *  // Get all the hits in the TT stations
   *  Tf::TTStationHitManager::HitRange range = hitMan->hits();
   *
   *  // Get all the hits in one specific TT station
   *  Tf::TTStationID sta = ...;
   *  Tf::TTStationHitManager::HitRange range = hitMan->hits(sta);
   *
   *  // Get all the hits in one specific layer of one TT station
   *  Tf::TTStationID sta = ...;
   *  Tf::TTLayerID   lay = ...;
   *  Tf::TTStationHitManager::HitRange range = hitMan->hits(sta,lay);
   *
   *  // Get all the hits in a specific 'region' of one layer of one TT station
   *  Tf::TTStationID sta = ...;
   *  Tf::TTLayerID   lay = ...;
   *  Tf::TTRegionID  reg = ...;
   *  Tf::TTStationHitManager::HitRange range = hitMan->hits(sta,lay,reg);
   *  @endcode
   *
   *
   *  In all cases the returned Range object acts like a standard vector or container :-
   *
   *  @code
   *   // Iterate over the returned range
   *  for ( Tf::TTStationHitManager::HitRange::const_iterator iR = range.begin();
   *        iR != range.end(); ++iR )
   *  {
   *    // do something with the hit
   *  }
   *  @endcode
   *
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   **/

  template <class Hit>
  class TTStationHitManager : public GaudiTool, public IIncidentListener {

  public:
    /// Type for container of Hits
    typedef IndexedHitContainer<Tf::RegionID::TTIndex::kNStations, Tf::RegionID::TTIndex::kNLayers,
                                Tf::RegionID::TTIndex::kNRegions, Hit*>
        Hits;

    /// range object for Hits
    typedef typename Hits::HitRange HitRange;

    /// InterfaceID for this tool
    static const InterfaceID& interfaceID() { return IID_TTStationHitManager; }

    /// Standard Constructor
    TTStationHitManager( const std::string& type, const std::string& name, const IInterface* parent );

    /// Tool initialization
    StatusCode initialize() override;

    /// Tool finalization
    StatusCode finalize() override;

    /// Handle method for incident service callbacks
    void handle( const Incident& incident ) override {
      if ( IncidentType::BeginEvent == incident.type() ) clearHits();
    }

    /** Load the hits for a given region of interest
     *
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *  @param[in] region Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    inline HitRange hits( const TTStationID sta, const TTLayerID lay, const TTRegionID region ) const {
      if ( UNLIKELY( !m_hits_ready ) ) prepareHits();
      return m_hits.range( sta, lay, region );
    }

    /** Load the all hits
     *  @return Range object for the hits in the selected region of interest
     */
    HitRange hits() const {
      if ( UNLIKELY( !m_hits_ready ) ) prepareHits();
      return m_hits.range();
    }

    /** Load the hits for a given region of interest
     *  In addition, specify a minimum x value
     *
     *  @param[in] xMin   Minimum x value of hit (at y=0)
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *  @param[in] region Region within the layer
     *
     *  @return Range object for the hits in the selected region of interest
     */
    HitRange hitsWithMinX( const double xMin, const TTStationID sta, const TTLayerID lay,
                           const TTRegionID region ) const {
      if ( UNLIKELY( !m_hits_ready ) ) prepareHits();
      auto range = m_hits.range( sta, lay, region );
      return {std::lower_bound( range.begin(), range.end(), xMin, Tf::compByX() ), range.end()};
    }

    /** Load the sorted hits for a given station and layer of interest
     *
     *  @param[in] sta    Station ID
     *  @param[in] lay    Station layer ID
     *
     *  @return Range object for the hits in the selected region of interest
     */
    HitRange sortedLayerHits( const TTStationID sta, const TTLayerID lay ) const {
      if ( !m_hits_ready_layers ) this->prepareHitsLayers();
      return m_sortedLayerHits.range( sta, lay );
    }

    /** Retrieve the STRegion for a certain TT region ID. The region
     *   knows its 'size' and gives access to its hits.
     *
     *  @param[in] iStation Station ID
     *  @param[in] iLayer   Station layer ID
     *  @param[in] iRegion  Region within the layer
     *
     *  @return Pointer to the STRegion object
     */
    const ITTHitCreator::STRegion* region( const TTStationID iStation, const TTLayerID iLayer,
                                           const TTRegionID iRegion ) const {
      return m_tthitcreator->region( iStation, iLayer, iRegion );
    }

    /// max number of regions
    static constexpr TTStationID maxStations() { return TTStationID( Tf::RegionID::TTIndex::kNStations ); }

    /// Maximum number of layers
    static constexpr TTLayerID maxLayers() { return TTLayerID( Tf::RegionID::TTIndex::kNLayers ); }

    /// Maximum number of regions
    static constexpr TTRegionID maxRegions() { return TTRegionID( Tf::RegionID::TTIndex::kNRegions ); }

    /** Initialise all the hits for the current event
     */
    virtual void prepareHits() const;

  private:
    void prepareHitsLayers() const;

    /// Clear the hit containers for a new event
    void clearHits() const;

    /// The underlying TT hit creator
    ToolHandle<Tf::ITTHitCreator> m_tthitcreator;

    mutable Hits m_hits;            ///< Hits, split in regions
    mutable Hits m_sortedLayerHits; ///< hits, sorted and split in layers only

    // Flags to indicate which hits are ready
    mutable bool m_hits_ready;
    mutable bool m_hits_ready_layers; ///< Flags to indicate which stations and layers have hits ready
  };

  template <class Hit>
  TTStationHitManager<Hit>::TTStationHitManager( const std::string& type, const std::string& name,
                                                 const IInterface* parent )
      : GaudiTool( type, name, parent ), m_tthitcreator( "Tf::STHitCreator<Tf::TT>/TTHitCreator" ) {
    declareInterface<TTStationHitManager<Hit>>( this );
  }

  template <class Hit>
  StatusCode TTStationHitManager<Hit>::initialize() {
    StatusCode sc = GaudiTool::initialize();
    if ( sc.isFailure() ) return sc;

    sc = m_tthitcreator.retrieve();
    if ( sc.isFailure() ) return sc;

    // set up to be told about each new event
    incSvc()->addListener( this, IncidentType::BeginEvent );

    // make sure we are ready for first event
    this->clearHits();

    return sc;
  }

  template <class Hit>
  StatusCode TTStationHitManager<Hit>::finalize() {
    this->clearHits();
    return GaudiTool::finalize();
  }

  template <class Hit>
  void TTStationHitManager<Hit>::clearHits() const {
    HitRange rng = m_hits.range();
    std::for_each( rng.begin(), rng.end(), std::default_delete<Hit>() );
    m_hits.clear();

    rng = m_sortedLayerHits.range();
    std::for_each( rng.begin(), rng.end(), std::default_delete<Hit>() );
    m_sortedLayerHits.clear();

    m_hits_ready        = false;
    m_hits_ready_layers = false;
  }

  template <class Hit>
  void TTStationHitManager<Hit>::prepareHits() const {
    if ( m_hits_ready ) return;
    auto fun = []( Tf::STHitRange::const_reference hit ) { return new Hit( *hit ); };
    for ( TTStationID sta = 0; sta < maxStations(); ++sta ) {
      for ( TTLayerID lay = 0; lay < maxLayers(); ++lay ) {
        for ( TTRegionID reg = 0; reg < maxRegions(); ++reg ) {
          auto tthits = m_tthitcreator->hits( sta, lay, reg );
          auto b      = boost::make_transform_iterator( std::begin( tthits ), std::cref( fun ) );
          auto e      = boost::make_transform_iterator( std::end( tthits ), std::cref( fun ) );
          m_hits.insert( sta, lay, reg, b, e );
          auto rng = m_hits.range_( sta, lay, reg );
          std::sort( rng.first, rng.second, Tf::increasingByXAtYEq0<>() );
        }
      }
    }
    m_hits_ready = true;
  }

  // sort hits in all layers and stations
  template <class Hit>
  void TTStationHitManager<Hit>::prepareHitsLayers() const {
    if ( m_hits_ready_layers ) return;
    auto fun = []( Tf::STHitRange::const_reference hit ) { return new Hit( *hit ); };
    for ( TTStationID sta = 0; sta < maxStations(); ++sta ) {
      for ( TTLayerID lay = 0; lay < maxLayers(); ++lay ) {
        auto tthits = m_tthitcreator->hits( sta, lay );
        auto b      = boost::make_transform_iterator( std::begin( tthits ), std::cref( fun ) );
        auto e      = boost::make_transform_iterator( std::end( tthits ), std::cref( fun ) );
        m_sortedLayerHits.insert( sta, lay, b, e );
        auto rng = m_sortedLayerHits.range_( sta, lay );
        std::sort( rng.first, rng.second, Tf::increasingByXAtYEq0<>() );
      }
    }
    m_hits_ready_layers = true;
  }

} // namespace Tf

#endif // TFKERNEL_TTSTATIONHITMANAGER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_EXTENDENDVELORHITMANAGER_H
#define INCLUDE_EXTENDENDVELORHITMANAGER_H 1

#include <string>

#include "TfKernel/DefaultVeloRHitManager.h"

namespace Tf {

  static const InterfaceID IID_ExtendedVeloRHitManager( "Tf::ExtendedVeloRHitManager", 1, 0 );

  /** @class ExtendedVeloRHitManager ExtendedVeloRHitManager.h
   * Base class for user dereved VeloR HitManagers.
   *
   * @author Kurt Rinnert <kurt.rinnert@cern.ch>
   * @date   2007-08-07
   */
  template <typename HITEXTENSION>
  class ExtendedVeloRHitManager : public VeloHitManager<DeVeloRType, HITEXTENSION, 4> {

  public:
    typedef VeloHitManager<DeVeloRType, HITEXTENSION, 4>    HitManagerBase;         ///< shortcut to base class type
    typedef typename HitManagerBase::Station                Station;                ///< shortcut for station
    typedef typename HitManagerBase::StationIterator        StationIterator;        ///< shortcut for station iterator
    typedef typename HitManagerBase::StationReverseIterator StationReverseIterator; ///< shortcut for station reverse
                                                                                    ///< iterator

    typedef typename DefaultVeloRHitManager::Station         DefaultStation; ///< shortcut for default manager access
    typedef typename DefaultVeloRHitManager::StationIterator DefaultStationIterator; ///< shortcut for default manager
                                                                                     ///< access
    typedef
        typename DefaultVeloRHitManager::StationReverseIterator DefaultStationReverseIterator; ///< shortcut for default
                                                                                               ///< manager access

  public:
    /// Retrieve interface ID
    static const InterfaceID& interfaceID() { return IID_ExtendedVeloRHitManager; }

    /// Standard Constructor
    ExtendedVeloRHitManager( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override; ///< Tool initialization

  protected:
    DefaultVeloRHitManager* m_defaultHitManager;

  private:
    std::string m_defaultHitManagerName;
  };

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  template <typename HITEXTENSION>
  ExtendedVeloRHitManager<HITEXTENSION>::ExtendedVeloRHitManager( const std::string& type, const std::string& name,
                                                                  const IInterface* parent )
      : VeloHitManager<DeVeloRType, HITEXTENSION, 4>( type, name, parent ) {
    GaudiTool::declareInterface<ExtendedVeloRHitManager<HITEXTENSION>>( this );

    GaudiTool::declareProperty( "DefaultHitManagerName", m_defaultHitManagerName = "DefaultVeloRHitManager" );
  }

  //=============================================================================
  // Initialization
  //=============================================================================
  template <typename HITEXTENSION>
  StatusCode ExtendedVeloRHitManager<HITEXTENSION>::initialize() {
    StatusCode sc = VeloHitManager<DeVeloRType, HITEXTENSION, 4>::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;                                            // error printed already by GaudiTool

    GaudiTool::debug() << "==> Initialize" << endmsg;

    m_defaultHitManager =
        GaudiTool::tool<Tf::DefaultVeloRHitManager>( "Tf::DefaultVeloRHitManager", m_defaultHitManagerName );

    return StatusCode::SUCCESS;
  }

} // namespace Tf
#endif // INCLUDE_EXTENDENDVELORHITMANAGER_H

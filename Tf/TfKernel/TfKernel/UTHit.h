/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file UTHit.h
 *
 *  Header file for track finding class Tf::UTHit
 *
 *  @author A. Beiter (based on code by S. Hansmann-Menzemer,
 *  W. Hulsbergen, C. Jones, K. Rinnert)
 *  @date   2018-09-04
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_UTHIT_H
#define TFKERNEL_UTHIT_H 1

// TfKernel
#include "TfKernel/LineHit.h"

#include "GaudiKernel/Range.h"

#include "Event/UTLiteCluster.h"

#include <iostream>

namespace Tf {

  /** @class UTHit
   *
   *  Representation of an UT hit
   *
   *  @authors S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-05-30
   */

  class UTHit : public LineHit {

  public:
    typedef DeUTSector DetectorElementType; ///< Detector element type for UT
    typedef st_hit_tag hit_type_tag;        ///< The hit type tag

  public:
    /** Constructor from a DeUTSector and a UTLiteCluster
     *  @param[in] aSector Reference to the associated DeUTSector
     *  @param[in] clus    The raw UT hit (UTLiteCluster)
     */
    UTHit( const DeUTSector& aSector, const LHCb::UTLiteCluster& clus );

  public: // Simple accessors to internal data members
    /** Access the cluster size */
    inline int size() const noexcept { return m_cluster.pseudoSize(); }

    /** Interstrip fraction [2 bit precision] */
    inline double frac() const noexcept { return m_cluster.interStripFraction(); }

    /** cluster has the high threshold */
    inline bool highThreshold() const noexcept { return m_cluster.highThreshold(); }

    /** Access the raw UTLiteCluster cluster for this hit */
    inline const LHCb::UTLiteCluster& cluster() const noexcept { return m_cluster; }

    /** accessor to the channelID of this hit */
    inline LHCb::UTChannelID channelID() const noexcept { return cluster().channelID(); }

    /** accessor to the unique readout sector identifier */
    inline unsigned int uniqueSector() const noexcept { return cluster().channelID().uniqueSector(); }

    /** Access the associated DeUTSector for this hit */
    inline const DeUTSector& sector() const noexcept { return *m_sector; }

    /// printOut method to Gaudi message stream
    std::ostream& fillStream( std::ostream& os ) const;

  private:
    const DeUTSector*   m_sector;  ///< Pointer to the associated DeUTSector
    LHCb::UTLiteCluster m_cluster; ///< The raw UTLiteCluster for this hit
  };

  inline std::ostream& operator<<( std::ostream& str, const UTHit& obj ) { return obj.fillStream( str ); }

  /// Type for container for OTHit
  typedef std::vector<const UTHit*> UTHits;
  /// Type for range of UTHits within a container
  typedef Gaudi::Range_<UTHits> UTHitRange;
  /// Type for a container for UTHitRange
  typedef std::vector<UTHitRange> UTHitRanges;

  //----------------------------------------------------------------------------
  // Inline function definitions
  //----------------------------------------------------------------------------
  inline UTHit::UTHit( const DeUTSector& aSector, const LHCb::UTLiteCluster& clus )
      : LineHit( aSector, clus ), m_sector( &aSector ), m_cluster( clus ) {}

  // our dynamic casts
  // Definition is in the HitBase class
  inline const UTHit* HitBase::uthit() const noexcept {
    return type() == RegionID::UT ? static_cast<const UTHit*>( this ) : nullptr;
  }

  inline std::ostream& UTHit::fillStream( std::ostream& s ) const {

    s << "UTHit { " << m_sector->nickname() << " strip " << m_cluster.channelID().strip()
      << " cluster size: " << m_cluster.pseudoSize() << std::endl
      << " Coordinates xMid " << xMid() << " yMid " << yMid() << " zMid " << zMid() << "} " << std::endl;
    return s;
  }

} // namespace Tf

#endif // TFKERNEL_UTHIT_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STCosmicFilter_H
#define STCosmicFilter_H 1

#include "Kernel/STHistoAlgBase.h"

namespace LHCb {
  class STCluster;
}

/** @class STCosmicFilter STCosmicFilter.h
 *
 *  Class for monitoring STClusters
 *
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   04/12/2006
 */

class STCosmicFilter : public ST::HistoAlgBase {

public:
  /// constructer
  STCosmicFilter( const std::string& name, ISvcLocator* svcloc );

  /// destructer
  virtual ~STCosmicFilter();

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

private:
  bool                                   reselect( const std::vector<LHCb::STCluster*>& clusters ) const;
  std::string                            m_summaryLocation;
  std::string                            m_clusterLocation;
  double                                 m_chargeThreshold;
  double                                 m_highChargeThreshold;
  unsigned int                           m_maxClusters;
  unsigned int                           m_nClus1;
  unsigned int                           m_nClus2;
  double                                 m_xWindow;
  mutable Gaudi::Accumulators::Counter<> m_selectedCount{this, "selected"};
};

#endif // MCSTCosmicFilter_H

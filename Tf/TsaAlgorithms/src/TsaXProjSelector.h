/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $ Exp $
#ifndef _TsaXProjSelector_H
#define _TsaXProjSelector_H

#include "TsaKernel/ITsaSeedStep.h"
#include "TsaKernel/SeedFunctor.h"
#include "TsaKernel/SeedTrack.h"

#include "GaudiAlg/GaudiTool.h"

#include <string>

namespace Tf {
  namespace Tsa {

    /** @class XProjSelector TsaXProjSelector.h
     * Follow track and pick up hits
     * @author M. Needham
     **/

    class XProjSelector : public GaudiTool, virtual public ITsaSeedStep {

    public:
      /// constructer
      XProjSelector( const std::string& type, const std::string& name, const IInterface* parent );

      // destructer
      virtual ~XProjSelector();

      // execute method
      StatusCode execute( std::vector<SeedTrack*>& seeds, std::vector<SeedHit*> hits[6] = 0 ) override;

      StatusCode execute( LHCb::State& hint, std::vector<SeedTrack*>& seeds,
                          std::vector<SeedHit*> hits[6] = 0 ) override;

    private:
      double m_fracUsed;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaXProjSelector_H

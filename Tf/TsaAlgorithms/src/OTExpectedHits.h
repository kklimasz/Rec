/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _OTExpectedHits_H
#define _OTExpectedHits_H

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"

// Tsa Kernel
#include "TsaKernel/IOTExpectedHits.h"
#include "TsaKernel/Line.h"
#include "TsaKernel/Line3D.h"
#include "TsaKernel/Parabola.h"

// Geometry
#include "OTDet/DeOTDetector.h"
#include "OTDet/DeOTLayer.h"
#include "OTDet/DeOTModule.h"
#include "OTDet/DeOTQuarter.h"

// Kernel
#include "Kernel/OTChannelID.h"
#include "LHCbMath/GeomFun.h"

namespace Tf {
  namespace Tsa {

    /** @class OTExpectedHits OTExpectedHits.h
     * How many hits do we expect in in the OT detector
     * @author M. Needham
     * @date 30.05.2004
     */

    class OTExpectedHits : public GaudiTool, virtual public IOTExpectedHits {

    public:
      /// constructer
      OTExpectedHits( const std::string& type, const std::string& name, const IInterface* parent );

      // destructer
      virtual ~OTExpectedHits();

      /// init
      StatusCode initialize() override;

      // execute method
      StatusCode collect( const Parabola& parab, const Line& line, const LHCb::OTChannelID& aChan,
                          std::vector<IOTExpectedHits::OTPair>& hits, const unsigned int iSector ) const override;

    private:
      bool insideModule( const DeOTModule* layer, const Line3D& line ) const;

      Gaudi::XYZPoint intersection( const Line3D& line, const DeOTModule* module, const Gaudi::XYZPoint& aPoint ) const;

      Gaudi::XYZPoint intersection( const Line3D& line, const Gaudi::Plane3D& aPlane ) const;

      bool correctSector( const unsigned int quart, const unsigned int iSector ) const;

      DeOTModule* findModule( const Parabola& parab, const Line& line, const LHCb::OTChannelID& aChan,
                              const unsigned int iSector ) const;

      DeOTDetector* m_tracker;
    };

    inline bool OTExpectedHits::correctSector( const unsigned int quart, const unsigned int iSector ) const {

      return ( iSector == 3 ? quart < 2 : quart > 1 );
    }

  } // namespace Tsa
} // namespace Tf

#endif // _OTExpectedHits_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TsaSeedSelector_H
#define _TsaSeedSelector_H

#include "TsaSelectorBase.h"

#include <string>

namespace Tf {
  namespace Tsa {

    /** @class SeedSelector TsaSeedSelector.h
     * Follow track and pick up hits
     * @author M. Needham
     **/

    class SeedSelector : public SelectorBase {

    public:
      /// constructer
      SeedSelector( const std::string& type, const std::string& name, const IInterface* parent );

      // destructer
      virtual ~SeedSelector();

      // execute method
      StatusCode execute( std::vector<SeedTrack*>& seeds, std::vector<SeedHit*> hits[6] = 0 ) override;

      StatusCode execute( LHCb::State& hint, std::vector<SeedTrack*>& seeds,
                          std::vector<SeedHit*> hits[6] = 0 ) override;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaSeedSelector_H

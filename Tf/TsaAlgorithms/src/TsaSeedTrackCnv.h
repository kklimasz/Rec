/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TsaSeedTrackCnv_H_
#define _TsaSeedTrackCnv_H_

#include "TsaBaseAlg.h"

#include <string>
#include <vector>

// TsaKernel
#include "TsaKernel/ITsaSeedTrackCnvTool.h"
#include "TsaKernel/SeedPnt.h"
#include "TsaKernel/SeedTrack.h"
#include "TsaKernel/TsaConstants.h"
#include "TsaKernel/TsaFun.h"

//  Event
#include "Event/State.h"
#include "Event/Track.h"

// gsl
#include "gsl/gsl_math.h"

namespace Tf {
  namespace Tsa {

    /** @class SeedTrackCnv TsaSeedTrackCnv.h
     *
     *  Converter for Tsa Tracks to standard LHCb Tracks
     *
     *  @author M.Needham
     *  @date   30/04/2004
     */

    class SeedTrackCnv : public BaseAlg {

    public:
      // Constructors and destructor
      SeedTrackCnv( const std::string& name, ISvcLocator* pSvcLocator );
      virtual ~SeedTrackCnv();

      StatusCode initialize() override;

      StatusCode execute() override;

    private:
      double      m_likCut;
      std::string m_inputLocation;
      std::string m_outputLocation;

      Tf::Tsa::ITsaSeedTrackCnvTool* m_Tsa2TrackCnv;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaToTrTrack_H_

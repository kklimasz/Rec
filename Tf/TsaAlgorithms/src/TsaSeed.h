/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TSASEED_H_
#define _TSASEED_H_

#include <string>
#include <vector>

#include "TsaBaseAlg.h"

#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiKernel/AnyDataHandle.h"

//#include "TsaKernel/TimeSummary.h"
#include "TsaKernel/ITsaSeedStep.h"
#include "TsaKernel/ITsaStubExtender.h"
#include "TsaKernel/ITsaStubFind.h"
#include "TsaKernel/ITsaStubLinker.h"
#include "TsaKernel/SeedHit.h"
#include "TsaKernel/SeedStub.h"
#include "TsaKernel/SeedTrack.h"

#include "ITsaSeedAddHits.h"

struct IOTRawBankDecoder;

namespace Tf {
  namespace Tsa {

    /** @class Seed
     *
     *  Main Seeding algorithm for Tsa
     *
     *  @author M.Needham
     *  @date   07/03/2002
     */

    class Seed : public BaseAlg {

    public:
      // Constructors and destructor
      Seed( const std::string& name, ISvcLocator* pSvcLocator );

      // IAlgorithm members
      virtual StatusCode initialize() override;
      virtual StatusCode execute() override;

    private:
      std::string m_seedTrackLocation;
      std::string m_seedHitLocation;
      std::string m_seedStubLocation;

      std::string m_selectorType;
      bool        m_calcLikelihood;
      bool        m_addHitsInITOverlap;

      std::vector<ITsaSeedStep*> m_xSearchStep;
      std::vector<ITsaSeedStep*> m_stereoStep;
      ITsaSeedStep*              m_xSelection     = nullptr;
      ITsaSeedStep*              m_finalSelection = nullptr;
      ITsaSeedStep*              m_likelihood     = nullptr;
      ITsaSeedAddHits*           m_addHits        = nullptr;
      ITsaStubFind*              m_stubFind       = nullptr;
      ITsaStubLinker*            m_stubLinker     = nullptr;
      ITsaStubExtender*          m_extendStubs    = nullptr;

      bool   m_onlyGood;
      double m_discardChi2;

      unsigned int m_maxNumberOTHits;
      unsigned int m_maxNumberITHits;

      IOTRawBankDecoder*                                 m_rawBankDecoder = nullptr;
      AnyDataHandle<LHCb::STLiteCluster::STLiteClusters> m_clusters{LHCb::STLiteClusterLocation::ITClusters,
                                                                    Gaudi::DataHandle::Reader, this};
      ISequencerTimerTool*                               m_timerTool = nullptr;
      int                                                m_seedTime;
      bool                                               m_doTiming;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TSASEED

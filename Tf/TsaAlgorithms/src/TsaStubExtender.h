/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $ Exp $
#ifndef _TsaStubExtender_H
#define _TsaStubExtender_H

#include <algorithm>

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"

#include "TsaKernel/ITsaStubExtender.h"
#include "TsaKernel/SeedFun.h"
#include "TsaKernel/SeedPnt.h"
#include "TsaKernel/SeedStub.h"
#include "TsaKernel/SeedTrack.h"
#include "TsaKernel/TsaConstants.h"

#include "SeedLineFit.h"
#include "SeedParabolaFit.h"

#include "LHCbMath/GeomFun.h"

#include "TfKernel/RecoFuncs.h"

namespace Tf {
  namespace Tsa {

    /** @class StubExtender TsaStubExtender.h
     * Extend the IT stubs into the OT
     * @author M. Needham
     **/

    class StubExtender : public GaudiTool, virtual public ITsaStubExtender {

    public:
      /// constructer
      StubExtender( const std::string& type, const std::string& name, const IInterface* parent );

      /// destructer
      virtual ~StubExtender();

      StatusCode initialize() override;
      StatusCode finalize() override;

      StatusCode execute( int& sect, std::vector<SeedStub*> stubs[], std::vector<SeedHit*> hits[],
                          std::vector<SeedHit*> sHits[], std::vector<SeedTrack*>& seeds ) override;

    private:
      double           m_scth;
      SeedLineFit*     m_fitLine;
      SeedParabolaFit* m_parabolaFit;

      double m_dSlopeCut;
      double m_searchTol;
      int    m_nxCut;
      int    m_nyCut;
      double m_txCut;
      double m_tyCut;
      double m_yTol;
      int    m_nTotalCut1;
      int    m_nTotalCut2;
      double m_y0Cut1;
      double m_y0Cut2;
      double m_dxCut;
      double m_dyCut;
      double m_outlierCutLine;
      double m_outlierCutParabola;
    };

  } // namespace Tsa
} // namespace Tf

#endif // _TsaStubLinker_H

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TsaAlgorithms
################################################################################
gaudi_subdir(TsaAlgorithms v3r31)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Kernel/Relations
                         ST/STKernel
                         Tf/TsaKernel
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TsaAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces
                 LINK_LIBRARIES CaloUtils RelationsLib STKernelLib TsaKernel TrackFitEvent)

gaudi_install_python_modules()


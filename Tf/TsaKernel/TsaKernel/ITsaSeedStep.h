/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _ITsaSeedStep_H
#define _ITsaSeedStep_H

#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_ITsaSeedStep( "ITsaSeedStep", 0, 0 );

#include <vector>

namespace LHCb {
  class State;
}

namespace Tf {
  namespace Tsa {

    class SeedTrack;
    class SeedHit;

    /** @class ITsaSeedStep ITsaSeedStep.h TsaKernel/ITsaSeedStep
     *
     *  Interface to data svc
     *
     *  @author M.Needham
     *  @date   12/11/2006
     */

    class ITsaSeedStep : virtual public IAlgTool {
    public:
      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_ITsaSeedStep; }

      virtual StatusCode execute( std::vector<SeedTrack*>& seeds, std::vector<SeedHit*> hits[6] = 0 ) = 0;

      virtual StatusCode execute( LHCb::State& hint, std::vector<SeedTrack*>& seeds,
                                  std::vector<SeedHit*> hits[6] = 0 ) = 0;
    };

  } // namespace Tsa
} // namespace Tf

#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _SeedFun_H_
#define _SeedFun_H_

#include <algorithm>
#include <functional>

#include "TsaKernel/SeedFunctor.h"
#include "TsaKernel/SeedHit.h"

namespace Tf {
  namespace Tsa {

    namespace SeedFun {

      std::vector<SeedHit*>::iterator startX( std::vector<SeedHit*>& hits, const double x1, const double z1,
                                              const double sx, const double tol );

      std::vector<SeedHit*>::iterator stopX( std::vector<SeedHit*>& hits, const double x1, const double z1,
                                             const double sx, const double tol );

      std::vector<SeedHit*>::iterator startStereo( std::vector<SeedHit*>& hits, const double x, const double tol );
      std::vector<SeedHit*>::iterator endStereo( std::vector<SeedHit*>& hits, const double x, const double tol );

    } // namespace SeedFun

    inline std::vector<SeedHit*>::iterator SeedFun::startX( std::vector<SeedHit*>& hits, const double x1,
                                                            const double z1, const double sx, const double tol ) {

      if ( hits.empty() == true ) return hits.end();

      // we don't know which double layer first hit is - hence 10 mm
      const double xTest = x1 + ( sx * ( hits.front()->z() - z1 ) ) - tol - fabs( 10.0 * sx );
      return std::lower_bound( hits.begin(), hits.end(), xTest, SeedFunctor::compByX_LB<const SeedHit*>() );
    }

    inline std::vector<SeedHit*>::iterator SeedFun::stopX( std::vector<SeedHit*>& hits, const double x1,
                                                           const double z1, const double sx, const double tol ) {

      if ( hits.empty() == true ) return hits.end();

      // we don't know which double layer first hit is - hence 10 mm
      const double xTest = x1 + ( sx * ( hits.front()->z() - z1 ) ) + tol + fabs( 10.0 * sx );
      return std::lower_bound( hits.begin(), hits.end(), xTest, SeedFunctor::compByX_LB<const SeedHit*>() );
    }

    inline std::vector<SeedHit*>::iterator SeedFun::startStereo( std::vector<SeedHit*>& hits, const double x,
                                                                 const double tol ) {
      const double xTest = x - tol;
      return std::lower_bound( hits.begin(), hits.end(), xTest, SeedFunctor::compByXMin_LB<const SeedHit*>() );
    }

    inline std::vector<SeedHit*>::iterator SeedFun::endStereo( std::vector<SeedHit*>& hits, const double x,
                                                               const double tol ) {
      const double xTest = x + tol;
      return std::lower_bound( hits.begin(), hits.end(), xTest, SeedFunctor::compByXMax_LB<const SeedHit*>() );
    }

  } // namespace Tsa
} // namespace Tf

#endif

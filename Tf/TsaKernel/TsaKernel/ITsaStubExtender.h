/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _ITsaStubExtender_H
#define _ITsaStubExtender_H

#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_ITsaStubExtender( "ITsaStubExtender", 0, 0 );

#include <vector>

namespace Tf {
  namespace Tsa {

    class SeedStub;
    class SeedTrack;
    class SeedHit;

    /** @class ITsaStubExtender ITsaStubExtender.h TsaKernel/ITsaStubExtender.h
     *
     *  Interface to Tsa Stub extender
     *
     *  @author M.Needham
     *  @date   12/11/2006
     */

    class ITsaStubExtender : virtual public IAlgTool {
    public:
      /// Retrieve interface ID
      static const InterfaceID& interfaceID() { return IID_ITsaStubExtender; }

      virtual StatusCode execute( int& sect, std::vector<SeedStub*> stubs[], std::vector<SeedHit*> hits[],
                                  std::vector<SeedHit*> sHits[], std::vector<SeedTrack*>& seeds ) = 0;
    };

  } // namespace Tsa
} // namespace Tf

#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SEEDTRACK_H
#define SEEDTRACK_H 1

#include "GaudiKernel/KeyedObject.h"

#include <algorithm>

#include "TsaKernel/SeedHit.h"
#include "TsaKernel/SeedPnt.h"

namespace LHCb {
  class LHCbID;
}

#include <array>

// Class ID definition
static const CLID CLID_TsaSeedTrack = 19111;

namespace Tf {
  namespace Tsa {

    // Namespace for locations in TES
    namespace SeedTrackLocation {
      static const std::string& Default = "/Event/Rec/Tsa/SeedTracks";
    }

    /** @class SeedTrack SeedTrack.h
     *  An object to hold the information needed per track for track seeding
     *
     *  @author Roger Forty
     *  @date   2006-1-27
     */

    class SeedTrack final : public KeyedObject<int> {

    public:
      /// Constructor
      SeedTrack();

      /// Constructor with x points
      SeedTrack( std::vector<SeedPnt> xPnts, int sector, double dth = 0 );

      /// Constructor with x and y points
      SeedTrack( std::vector<SeedPnt> xPnts, std::vector<SeedPnt> yPnts, int sector );

      /// Retrieve pointer to class definition structure
      const CLID&        clID() const override;
      static const CLID& classID();

      void setSelect( const bool value );
      void setLive( const bool value );
      void setNx( const int value );
      void setNy( const int value );
      void setX0( const double value );
      void setY0( const double value );
      void setSx( const double value );
      void setSy( const double value );
      void setTx( const double value );
      void setXChi2( const double value );
      void setYChi2( const double value );
      void setSector( const int value );
      void setDth( const double value );
      void setLik( const double value );
      void setXPnts( const std::vector<SeedPnt>& value );
      void setYPnts( const std::vector<SeedPnt>& value );

      bool select() const;
      bool live() const;
      int  nx() const;
      int  ny() const;
      int  nHit() const;

      double x0() const;
      double y0() const;
      double sx() const;
      double sy() const;
      double tx() const;
      double xChi2() const;
      double yChi2() const;
      int    sector() const;
      double dth() const;
      double lik() const;

      std::vector<SeedPnt>&       xPnts();
      std::vector<SeedPnt>&       yPnts();
      const std::vector<SeedPnt>& xPnts() const;
      const std::vector<SeedPnt>& yPnts() const;

      std::vector<SeedPnt> pnts() const;
      std::vector<SeedPnt> usedPnts() const;

      bool onTrack( const LHCb::LHCbID id ) const;

      bool xHitOnTrack( const LHCb::LHCbID id ) const;

      bool yHitOnTrack( const LHCb::LHCbID id ) const;

      bool onTrack( const LHCb::LHCbID id, const int layer ) const;

      double x( const double z, const double z0 ) const;
      double xSlope( const double z, const double z0 ) const;
      double y( const double z, const double z0 ) const;

      double xErr( unsigned i ) const;
      double yErr( unsigned i ) const;

      void setXerr( unsigned i, double value );
      void setYerr( unsigned i, double value );

      void setXParams( const double& tx, const double& sx, const double& x0 );

      void setYParams( const double& sy, const double& y0 );

    public:
      /// Print this SeedHit in a human readable way
      std::ostream& fillStream( std::ostream& s ) const override;

      /// Implement ostream << method
      friend inline std::ostream& operator<<( std::ostream& s, const SeedTrack& track ) {
        return track.fillStream( s );
      }

    public:
      void addToYPnts( const SeedPnt& pnt );

      void addToXPnts( const SeedPnt& pnt );

    private:
      class pntByIncreasingZ {
      public:
        bool operator()( const SeedPnt& first, const SeedPnt& second ) const;
      };

      bool   m_select;
      bool   m_live;
      int    m_nx;
      int    m_ny;
      double m_x0;
      double m_y0;
      double m_sx;
      double m_sy;
      double m_tx;
      double m_xChi2;
      double m_yChi2;
      int    m_sector;

      double               m_lik;
      double               m_dth;
      std::vector<SeedPnt> m_xPnts;
      std::vector<SeedPnt> m_yPnts;

      std::array<double, 6> m_xErr; // covariance matrix elements (11, 12, 13, 22, 23, 33) of X
      std::array<double, 3> m_yErr; // covariance matrix elements (11, 12, 22) of Y fit
    };

    /// Constructor
    inline SeedTrack::SeedTrack()
        : KeyedObject<int>()
        , m_select( 0 )
        , m_live( 1 )
        , m_nx( 0 )
        , m_ny( 0 )
        , m_x0( 0. )
        , m_y0( 0. )
        , m_sx( 0. )
        , m_sy( 0. )
        , m_tx( 0. )
        , m_xChi2( 0. )
        , m_yChi2( 0. )
        , m_sector( -1 )
        , m_lik( 0. )
        , m_dth( 0. ) {

      m_xErr.fill( 0 );
      m_yErr.fill( 0 );
      m_yPnts.reserve( 24 );
    }

    /// Constructor
    inline SeedTrack::SeedTrack( std::vector<SeedPnt> xPnts, int sector, double dth )
        : KeyedObject<int>()
        , m_select( 0 )
        , m_live( 1 )
        , m_nx( 0 )
        , m_ny( 0 )
        , m_x0( 0. )
        , m_y0( 0. )
        , m_sx( 0. )
        , m_sy( 0. )
        , m_tx( 0. )
        , m_xChi2( 0. )
        , m_yChi2( 0. )
        , m_sector( sector )
        , m_lik( 0. )
        , m_dth( dth )
        , m_xPnts( std::move( xPnts ) ) {

      m_xErr.fill( 0 );
      m_yErr.fill( 0 );
      m_yPnts.reserve( 24 );
    }

    /// Constructor
    inline SeedTrack::SeedTrack( std::vector<SeedPnt> xPnts, std::vector<SeedPnt> yPnts, int sector )
        : KeyedObject<int>()
        , m_select( 0 )
        , m_live( 1 )
        , m_nx( 0 )
        , m_ny( 0 )
        , m_x0( 0. )
        , m_y0( 0. )
        , m_sx( 0. )
        , m_sy( 0. )
        , m_tx( 0. )
        , m_xChi2( 0. )
        , m_yChi2( 0. )
        , m_sector( sector )
        , m_lik( 0. )
        , m_dth( 0. )
        , m_xPnts( std::move( xPnts ) )
        , m_yPnts( std::move( yPnts ) ) {

      m_xErr.fill( 0 );
      m_yErr.fill( 0 );
    }

    inline const CLID& SeedTrack::clID() const { return SeedTrack::classID(); }

    inline const CLID& SeedTrack::classID() { return CLID_TsaSeedTrack; }

    inline void SeedTrack::setSelect( const bool value ) { m_select = value; }

    inline void SeedTrack::setLive( const bool value ) { m_live = value; }

    inline void SeedTrack::setNx( int value ) { m_nx = value; }

    inline void SeedTrack::setNy( const int value ) { m_ny = value; }

    inline void SeedTrack::setX0( const double value ) { m_x0 = value; }

    inline void SeedTrack::setY0( const double value ) { m_y0 = value; }

    inline void SeedTrack::setSx( const double value ) { m_sx = value; }

    inline void SeedTrack::setSy( const double value ) { m_sy = value; }

    inline void SeedTrack::setTx( const double value ) { m_tx = value; }

    inline void SeedTrack::setXParams( const double& tx, const double& sx, const double& x0 ) {

      m_tx = tx;
      m_sx = sx;
      m_x0 = x0;
    }

    inline void SeedTrack::setYParams( const double& sy, const double& y0 ) {

      m_sy = sy;
      m_y0 = y0;
    }

    inline void SeedTrack::setXChi2( const double value ) { m_xChi2 = value; }

    inline void SeedTrack::setYChi2( const double value ) { m_yChi2 = value; }

    inline void SeedTrack::setSector( const int value ) { m_sector = value; }

    inline void SeedTrack::setDth( const double value ) { m_dth = value; }

    inline void SeedTrack::setLik( const double value ) { m_lik = value; }

    inline void SeedTrack::setXPnts( const std::vector<SeedPnt>& value ) { m_xPnts = value; }

    inline void SeedTrack::setYPnts( const std::vector<SeedPnt>& value ) { m_yPnts = value; }

    inline bool SeedTrack::select() const { return m_select; }

    inline bool SeedTrack::live() const { return m_live; }

    inline int SeedTrack::nx() const { return m_nx; }

    inline int SeedTrack::nHit() const { return m_nx + m_ny; }

    inline int SeedTrack::ny() const { return m_ny; }

    inline double SeedTrack::x0() const { return m_x0; }

    inline double SeedTrack::y0() const { return m_y0; }

    inline double SeedTrack::sx() const { return m_sx; }

    inline double SeedTrack::sy() const { return m_sy; }

    inline double SeedTrack::tx() const { return m_tx; }

    inline double SeedTrack::xChi2() const { return m_xChi2; }

    inline double SeedTrack::yChi2() const { return m_yChi2; }

    inline int SeedTrack::sector() const { return m_sector; }

    inline double SeedTrack::dth() const { return m_dth; }

    inline double SeedTrack::lik() const { return m_lik; }

    inline std::vector<SeedPnt>& SeedTrack::xPnts() { return m_xPnts; }

    inline std::vector<SeedPnt>& SeedTrack::yPnts() { return m_yPnts; }

    inline const std::vector<SeedPnt>& SeedTrack::xPnts() const { return m_xPnts; }

    inline const std::vector<SeedPnt>& SeedTrack::yPnts() const { return m_yPnts; }

    inline double SeedTrack::y( const double z, const double z0 ) const { return y0() + sy() * ( z - z0 ); }

    inline double SeedTrack::x( const double z, const double z0 ) const {
      return x0() + sx() * ( z - z0 ) + tx() * ( z - z0 ) * ( z - z0 );
    }

    inline double SeedTrack::xSlope( const double z, const double z0 ) const { return sx() + 2.0 * tx() * ( z - z0 ); }

    inline std::vector<SeedPnt> SeedTrack::pnts() const {
      std::vector<SeedPnt> tmp;
      tmp.reserve( m_xPnts.size() + m_yPnts.size() );
      tmp.insert( tmp.begin(), m_xPnts.begin(), m_xPnts.end() );
      tmp.insert( tmp.begin(), m_yPnts.begin(), m_yPnts.end() );
      std::sort( tmp.begin(), tmp.end(), pntByIncreasingZ() );
      return tmp;
    }

    inline std::vector<SeedPnt> SeedTrack::usedPnts() const {
      std::vector<SeedPnt> allPnts = pnts();
      allPnts.erase( std::remove_if( allPnts.begin(), allPnts.end(), []( const SeedPnt& pnt ) { return pnt.skip(); } ),
                     allPnts.end() );
      return allPnts;
    }

    inline double SeedTrack::xErr( unsigned i ) const { return ( i < m_xErr.size() ? m_xErr[i] : 9999.0 ); }

    inline double SeedTrack::yErr( unsigned i ) const { return ( i < m_yErr.size() ? m_yErr[i] : 9999.0 ); }

    inline void SeedTrack::setXerr( unsigned i, double value ) {
      if ( i < m_xErr.size() ) m_xErr[i] = value;
    }

    inline void SeedTrack::setYerr( unsigned i, double value ) {
      if ( i < m_yErr.size() ) m_yErr[i] = value;
    }

    inline void SeedTrack::addToYPnts( const SeedPnt& pnt ) { m_yPnts.push_back( pnt ); }

    inline void SeedTrack::addToXPnts( const SeedPnt& pnt ) { m_xPnts.push_back( pnt ); }

    inline bool SeedTrack::pntByIncreasingZ::operator()( const SeedPnt& first, const SeedPnt& second ) const {
      //-------------------------------------------------------------------------
      //  Define the sort sequence for hits
      //-------------------------------------------------------------------------
      return ( first.z() < second.z() );
    }

    inline bool SeedTrack::onTrack( const LHCb::LHCbID id, const int layer ) const {
      return ( layer == 1 || layer == 2 ? yHitOnTrack( id ) : xHitOnTrack( id ) );
    }

    // Defintion of keyed container for Tsa::Track
    typedef KeyedContainer<SeedTrack, Containers::HashMap> SeedTracks;

  } // namespace Tsa
} // namespace Tf

#endif // SEEDTRACK_H

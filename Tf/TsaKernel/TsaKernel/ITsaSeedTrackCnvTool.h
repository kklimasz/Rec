/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TSAKERNEL_ITSASEEDTRACKCNVTOOL_H
#define TSAKERNEL_ITSASEEDTRACKCNVTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_ITsaSeedTrackCnvTool( "ITsaSeedTrackCnvTool", 1, 1 );

/** @class ITsaSeedTrackCnvTool ITsaSeedTrackCnvTool.h TsaKernel/ITsaSeedTrackCnvTool.h
 *
 *  Interface to tool which converts TsaSeedTracks to LHCb::Tracks
 *
 *  @author Johannes Albrecht
 *  @date   2007-10-31
 */

namespace Tf {
  namespace Tsa {

    class SeedTrack;

    class ITsaSeedTrackCnvTool : virtual public IAlgTool {
    public:
      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_ITsaSeedTrackCnvTool; }

      virtual LHCb::Track* convert( const SeedTrack* aTrack ) const = 0;

    protected:
    private:
    };
  } // namespace Tsa
} // namespace Tf

#endif // TSAKERNEL_ITSASEEDTRACKCNVTOOL_H

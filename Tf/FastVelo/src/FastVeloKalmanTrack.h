/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FastVeloKalmanTrack_H
#define FastVeloKalmanTrack_H

#include "Event/Track.h"

namespace LHCb {
  class State;
}

class FastVeloTrack;
class FastVeloHit;

class FastVeloKalmanTrack {
private:
  const FastVeloTrack*            m_track;
  std::vector<const FastVeloHit*> m_hits;
  std::vector<double>             kalmanScatteringNoiseParameters;

private:
  enum Direction { Forward = +1, Backward = -1 };
  void fit( LHCb::State& state, double& chi2, int& ndof, const int direction, const float noise2PerLayer ) const;

public:
  // constructor
  FastVeloKalmanTrack( const FastVeloTrack& track );
  // single member function
  void addStates( LHCb::Track& lhcbtrack, const LHCb::State& basestate, double transverseMomentumForScattering,
                  std::vector<double> kalmanScatteringNoiseParameters, bool addStateBeamLine,
                  bool addStateLastMeasurement, bool addStateEndVelo ) const;
};

#endif

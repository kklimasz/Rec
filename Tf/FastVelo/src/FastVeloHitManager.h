/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FASTVELOHITMANAGER_H
#define FASTVELOHITMANAGER_H 1

// Include files
// from Gaudi
#include "FastVeloSensor.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "VeloDet/DeVelo.h"
#include "VeloEvent/VeloDecodeStatus.h"

static const InterfaceID IID_FastVeloHitManager( "FastVeloHitManager", 1, 0 );

/** @class FastVeloHitManager FastVeloHitManager.h
 *  Tool to handle the Fast velo geometry and hits
 *
 *  @author Olivier Callot
 *  @date   2010-09-08
 */
class FastVeloHitManager : public GaudiTool, public IIncidentListener {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_FastVeloHitManager; }

  /// Standard constructor
  FastVeloHitManager( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode finalize() override;

  void buildFastHits();

  void clearHits();

  void handle( const Incident& incident ) override;

  FastVeloHits& hits( unsigned int sensor, int zone ) { return m_sensors[sensor]->hits( zone ); }

  FastVeloSensor*       sensor( unsigned int n ) { return m_sensors[n]; }
  const FastVeloSensor* sensor( unsigned int n ) const { return m_sensors[n]; }

  unsigned int firstRSensor() const { return m_firstR; }
  unsigned int lastRSensor() const { return m_lastR; }
  unsigned int firstPhiSensor() const { return m_firstPhi; }
  unsigned int lastPhiSensor() const { return m_lastPhi; }

  double cosPhi( unsigned int zone ) const { return m_cosPhi[zone]; }
  double sinPhi( unsigned int zone ) const { return m_sinPhi[zone]; }

  int nbHits() const { return m_nextInPool - m_pool.begin(); }
  int maxSize() const { return m_maxSize; }

  StatusCode rebuildGeometry(); ///< Recompute the geometry in case of change

  FastVeloHit* hitByLHCbID( LHCb::LHCbID id );

  /// get a hit by LHCbID and reset all internal state to the default
  FastVeloHit* defaultStateHitByLHCbID( LHCb::LHCbID id );

  void resetUsedFlags();

  /// get the status of the VeloRawDecoder for this sensor
  inline bool wasDecoded( unsigned int nSensor ) {
    return ( m_decStatus ? m_decStatus->wasDecoded( nSensor ) : false );
  }

private:
  DeVelo*                                             m_velo = nullptr;
  AnyDataHandle<LHCb::VeloLiteCluster::FastContainer> m_input{LHCb::VeloLiteClusterLocation::Default,
                                                              Gaudi::DataHandle::Reader, this};
  std::vector<FastVeloHit>                            m_pool;
  std::vector<FastVeloHit>::iterator                  m_nextInPool;
  std::vector<FastVeloSensor*>                        m_sensors;
  unsigned int                                        m_firstR   = 0;
  unsigned int                                        m_lastR    = 0;
  unsigned int                                        m_firstPhi = 0;
  unsigned int                                        m_lastPhi  = 0;
  std::vector<double>                                 m_cosPhi;
  std::vector<double>                                 m_sinPhi;
  int                                                 m_maxSize          = 0;
  bool                                                m_eventReady       = false;
  double                                              m_lastXOffsetRight = 0.;
  double                                              m_lastXOffsetLeft  = 0.;

  LHCb::VeloDecodeStatus* m_decStatus = nullptr; ///< status of the VeloRawDecoder
};
#endif // FASTVELOHITMANAGER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include <bitset>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/VPChannelID.h"
#include "VPKernel/VeloPixelInfo.h"
#include <DetDesc/ConditionAccessorHolder.h>
#include <VPDet/VPGeometry.h>

// FIXME still need to handle nx and ny for the clusters

// Namespace for locations in TDS
namespace LHCb {
  namespace VPClusterLocation {
    inline const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
} // namespace LHCb

namespace LHCb::Pr::Velo {
  class VSPClus : public Gaudi::Functional::MultiTransformer<
                      std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
                          const EventContext&, const RawEvent& ),
                      Gaudi::Functional::Traits::BaseClass_t<DetDesc::ConditionAccessorHolder<GaudiAlgorithm>>> {

  public:
    /// Standard constructor
    VSPClus( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm execution
    std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
    operator()( const EventContext&, const RawEvent& ) const override;

  private:
    ConditionAccessor<VPGeometry> m_vp{this, "VPGeometry", VPGeometry::CondKey};

    std::bitset<VP::NModules>                  m_modulesToSkipMask;
    Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{this,
                                                               "ModulesToSkip",
                                                               {},
                                                               [=]( const auto& ) {
                                                                 m_modulesToSkipMask.reset();
                                                                 for ( unsigned int moduleNumber : m_modulesToSkip )
                                                                   m_modulesToSkipMask.set( moduleNumber );
                                                               },
                                                               Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                                               "List of modules that should be skipped in decoding"};

    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
  };
} // namespace LHCb::Pr::Velo

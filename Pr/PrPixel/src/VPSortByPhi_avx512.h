/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

#include <cstdint>

#include <immintrin.h>
inline __attribute__( ( always_inline ) ) __m512 _mm512_arctan2_ps( __m512 y, __m512 x ) {
  const __m512 c1    = _mm512_set1_ps( M_PI / 4.0 );
  const __m512 c2    = _mm512_set1_ps( 3.0 * M_PI / 4.0 );
  __m512       abs_y = _mm512_abs_ps( y ); // assume x and y != 0 so no 0/0 condition

  __m512 x_plus_y = _mm512_add_ps( x, abs_y );
  __m512 x_sub_y  = _mm512_sub_ps( x, abs_y );
  __m512 y_sub_x  = _mm512_sub_ps( abs_y, x );

  __mmask16 x_sign = _mm512_cmp_ps_mask( x, _mm512_setzero_ps(), _CMP_LT_OQ );

  __m512 nom =
      _mm512_mask_mov_ps( x_sub_y, x_sign, x_plus_y ); // Chose between x_sub_y and x_plus_y based on the sign of x
  __m512 den = _mm512_mask_mov_ps( x_plus_y, x_sign, y_sub_x );

  __m512 r     = _mm512_div_ps( nom, den );
  __m512 angle = _mm512_sub_ps( _mm512_mask_mov_ps( c1, x_sign, c2 ), _mm512_mul_ps( c1, r ) );

  // Set sign of y to angle :
  return _mm512_xor_ps( angle, _mm512_and_ps( y, _mm512_castsi512_ps( _mm512_set1_epi32( 0x80000000 ) ) ) );
}

inline __attribute__( ( always_inline ) ) __m512 compute_key( PlaneSoA* P, int i ) {
  const __m512  key_mask = _mm512_castsi512_ps( _mm512_set1_epi32( 0xFFFFFE00 ) );
  const __m512i idx      = _mm512_setr_epi32( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 );
  const __m512  padv     = _mm512_set1_ps( 42.f );

  __mmask16 m_pad = ( ( i + 16 ) > P->n_hits ) ? ( 0xFFFF << ( P->n_hits & 15 ) ) : 0;

  __m512 x = _mm512_load_ps( &( P->Gx[i] ) );
  __m512 y = _mm512_load_ps( &( P->Gy[i] ) );

  __m512 phi = _mm512_arctan2_ps( y, x );

  phi = _mm512_mask_mov_ps( phi, m_pad, padv );

  __m512 key = _mm512_castsi512_ps( _mm512_add_epi32( _mm512_set1_epi32( i ), idx ) );

  phi = _mm512_and_ps( phi, key_mask );
  phi = _mm512_or_ps( phi, key );

  return phi;
}

inline __attribute__( ( always_inline ) ) void permute_hits( PlaneSoA* Pin, PlaneSoA* Pout, __m512 key, int i ) {
  const __m512 key_mask = _mm512_castsi512_ps( _mm512_set1_epi32( 0xFFFFFE00 ) );

  // Unpack key
  key         = _mm512_andnot_ps( key_mask, key );
  __m512i idx = _mm512_castps_si512( key );

  // Gather / store
  __m512  vf;
  __m512i vi;
  vf = _mm512_i32gather_ps( idx, Pin->Gx, sizeof( float ) );
  _mm512_store_ps( &( Pout->Gx[i] ), vf );

  vf = _mm512_i32gather_ps( idx, Pin->Gy, sizeof( float ) );
  _mm512_store_ps( &( Pout->Gy[i] ), vf );

  vf = _mm512_i32gather_ps( idx, Pin->Gz, sizeof( float ) );
  _mm512_store_ps( &( Pout->Gz[i] ), vf );

  vi = _mm512_i32gather_epi32( idx, Pin->cId, sizeof( int32_t ) );
  _mm512_store_si512( &( Pout->cId[i] ), vi );
}

inline __m512 avx512_sort_loop_ps( const __m512 v, __mmask16 sort_mask, int n ) {
  __m512  result = v;
  __m512i index  = _mm512_setzero_si512();
  __m512i incr   = _mm512_set1_epi32( 1 );

  for ( int i = 0; i < n; i++ ) {
    const __m512   b    = _mm512_permutexvar_ps( index, v );
    const uint16_t lt   = _mm_popcnt_u32( _mm512_mask_cmp_ps_mask( sort_mask, v, b, _CMP_LT_OQ ) );
    const uint16_t mask = ( uint32_t( 1 ) << lt );

    result = _mm512_mask_mov_ps( result, mask, b );
    index  = _mm512_add_epi32( index, incr );
  }

  return result;
}

inline void scalar_partition_ps( float* array, const float pivot, int& left, int& right ) {
  while ( left <= right ) {
    while ( array[left] < pivot ) left += 1;
    while ( array[right] > pivot ) right -= 1;

    if ( left <= right ) {
      const float t = array[left];
      array[left]   = array[right];
      array[right]  = t;

      left += 1;
      right -= 1;
    }
  }
}

void quicksort_avx512( float* arr, int left, int right ) {
  int i = left;
  int j = right;
  int n = j - i + 1;
  if ( n <= 16 ) {
    __m512 v = _mm512_loadu_ps( &( arr[i] ) );
    __m512 result;
    switch ( n ) {
    case 16:
      result = avx512_sort_loop_ps( v, 0xFFFF, 16 );
      break;
    case 15:
      result = avx512_sort_loop_ps( v, 0x7FFF, 15 );
      break;
    case 14:
      result = avx512_sort_loop_ps( v, 0x3FFF, 14 );
      break;
    case 13:
      result = avx512_sort_loop_ps( v, 0x1FFF, 13 );
      break;
    case 12:
      result = avx512_sort_loop_ps( v, 0x0FFF, 12 );
      break;
    case 11:
      result = avx512_sort_loop_ps( v, 0x07FF, 11 );
      break;
    case 10:
      result = avx512_sort_loop_ps( v, 0x03FF, 10 );
      break;
    case 9:
      result = avx512_sort_loop_ps( v, 0x01FF, 9 );
      break;
    case 8:
      result = avx512_sort_loop_ps( v, 0x00FF, 8 );
      break;
    case 7:
      result = avx512_sort_loop_ps( v, 0x007F, 7 );
      break;
    case 6:
      result = avx512_sort_loop_ps( v, 0x003F, 6 );
      break;
    case 5:
      result = avx512_sort_loop_ps( v, 0x001F, 5 );
      break;
    case 4:
      result = avx512_sort_loop_ps( v, 0x000F, 4 );
      break;
    case 3:
      result = avx512_sort_loop_ps( v, 0x0007, 3 );
      break;
    case 2:
      if ( arr[i] > arr[j] ) {
        float t = arr[i];
        arr[i]  = arr[j];
        arr[j]  = t;
      }
      return;
    case 1:
    default:
      return;
    }
    _mm512_storeu_ps( &( arr[i] ), result );
    return;
  }

  const float pivot = arr[( i + j ) / 2];
  scalar_partition_ps( arr, pivot, i, j );

  if ( left < j ) quicksort_avx512( arr, left, j );
  if ( i < right ) quicksort_avx512( arr, i, right );
}

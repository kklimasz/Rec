/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// @Author: Arthur Hennequin (LIP6, CERN)

#pragma once

#include <cstdint>

#include <immintrin.h>

inline __m256 _mm256_abs_ps( __m256 x ) {
  return _mm256_and_ps( x, _mm256_castsi256_ps( _mm256_set1_epi32( 0x7FFFFFFF ) ) );
}

inline __m256 _mm256_arctan2_ps( __m256 y, __m256 x ) {
  const __m256 c1    = _mm256_set1_ps( M_PI / 4.0 );
  const __m256 c2    = _mm256_set1_ps( 3.0 * M_PI / 4.0 );
  __m256       abs_y = _mm256_abs_ps( y ); // assume x and y != 0 so no 0/0 condition

  __m256 x_plus_y = _mm256_add_ps( x, abs_y );
  __m256 x_sub_y  = _mm256_sub_ps( x, abs_y );
  __m256 y_sub_x  = _mm256_sub_ps( abs_y, x );

  __m256 nom = _mm256_blendv_ps( x_sub_y, x_plus_y, x ); // Chose between x_sub_y and x_plus_y based on the sign of x
  __m256 den = _mm256_blendv_ps( x_plus_y, y_sub_x, x );

  __m256 r     = _mm256_div_ps( nom, den );
  __m256 angle = _mm256_sub_ps( _mm256_blendv_ps( c1, c2, x ), _mm256_mul_ps( c1, r ) );

  // Set sign of y to angle :
  return _mm256_xor_ps( angle, _mm256_and_ps( y, _mm256_castsi256_ps( _mm256_set1_epi32( 0x80000000 ) ) ) );
}

inline __attribute__( ( always_inline ) ) __m256 compute_key( PlaneSoA* P, int i ) {
  const __m256  key_mask = _mm256_castsi256_ps( _mm256_set1_epi32( 0xFFFFFE00 ) );
  const __m256i idx      = _mm256_setr_epi32( 0, 1, 2, 3, 4, 5, 6, 7 );
  const __m256  padv     = _mm256_set1_ps( 42.f );

  __mmask8 m_pad = ( ( i + 8 ) > P->n_hits ) ? ( 0xFF << ( P->n_hits & 7 ) ) : 0;

  __m256 x = _mm256_load_ps( &( P->Gx[i] ) );
  __m256 y = _mm256_load_ps( &( P->Gy[i] ) );

  __m256 phi = _mm256_arctan2_ps( y, x );

  phi = _mm256_mask_mov_ps( phi, m_pad, padv );

  __m256 key = _mm256_castsi256_ps( _mm256_add_epi32( _mm256_set1_epi32( i ), idx ) );

  phi = _mm256_and_ps( phi, key_mask );
  phi = _mm256_or_ps( phi, key );

  return phi;
}

inline __attribute__( ( always_inline ) ) void permute_hits( PlaneSoA* Pin, PlaneSoA* Pout, __m256 key, int i ) {
  const __m256 key_mask = _mm256_castsi256_ps( _mm256_set1_epi32( 0xFFFFFE00 ) );

  // Unpack key
  key         = _mm256_andnot_ps( key_mask, key );
  __m256i idx = _mm256_castps_si256( key );

  // Gather / store
  __m256  vf;
  __m256i vi;
  vf = _mm256_i32gather_ps( Pin->Gx, idx, sizeof( float ) );
  _mm256_store_ps( &( Pout->Gx[i] ), vf );

  vf = _mm256_i32gather_ps( Pin->Gy, idx, sizeof( float ) );
  _mm256_store_ps( &( Pout->Gy[i] ), vf );

  vf = _mm256_i32gather_ps( Pin->Gz, idx, sizeof( float ) );
  _mm256_store_ps( &( Pout->Gz[i] ), vf );

  vi = _mm256_i32gather_epi32( (int*)Pin->cId, idx, sizeof( int32_t ) );
  _mm256_store_si256( (__m256i*)&( Pout->cId[i] ), vi );
}

inline __m256 avx256_sort_loop_ps( const __m256 v, __mmask8 sort_mask, int n ) {
  __m256  result = v;
  __m256i index  = _mm256_setzero_si256();
  __m256i incr   = _mm256_set1_epi32( 1 );

  for ( int i = 0; i < n; i++ ) {
    const __m256   b    = _mm256_permutexvar_ps( index, v );
    const uint16_t lt   = _mm_popcnt_u32( _mm256_mask_cmp_ps_mask( sort_mask, v, b, _CMP_LT_OQ ) );
    const uint16_t mask = ( uint32_t( 1 ) << lt );

    result = _mm256_mask_mov_ps( result, mask, b );
    index  = _mm256_add_epi32( index, incr );
  }

  return result;
}

inline void scalar_partition_ps( float* array, const float pivot, int& left, int& right ) {
  while ( left <= right ) {
    while ( array[left] < pivot ) left += 1;
    while ( array[right] > pivot ) right -= 1;

    if ( left <= right ) {
      const float t = array[left];
      array[left]   = array[right];
      array[right]  = t;

      left += 1;
      right -= 1;
    }
  }
}

void quicksort_avx256( float* arr, int left, int right ) {
  int i = left;
  int j = right;
  int n = j - i + 1;
  if ( n <= 8 ) {
    __m256 v = _mm256_loadu_ps( &( arr[i] ) );
    __m256 result;
    switch ( n ) {
    case 8:
      result = avx256_sort_loop_ps( v, 0xFF, 8 );
      break;
    case 7:
      result = avx256_sort_loop_ps( v, 0x7F, 7 );
      break;
    case 6:
      result = avx256_sort_loop_ps( v, 0x3F, 6 );
      break;
    case 5:
      result = avx256_sort_loop_ps( v, 0x1F, 5 );
      break;
    case 4:
      result = avx256_sort_loop_ps( v, 0x0F, 4 );
      break;
    case 3:
      result = avx256_sort_loop_ps( v, 0x07, 3 );
      break;
    case 2:
      if ( arr[i] > arr[j] ) {
        float t = arr[i];
        arr[i]  = arr[j];
        arr[j]  = t;
      }
      return;
    case 1:
    default:
      return;
    }
    _mm256_storeu_ps( &( arr[i] ), result );
    return;
  }

  const float pivot = arr[( i + j ) / 2];
  scalar_partition_ps( arr, pivot, i, j );

  if ( left < j ) quicksort_avx256( arr, left, j );
  if ( i < right ) quicksort_avx256( arr, i, right );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "PrKernel/PrPixelModule.h"
#include "PrKernel/VeloPixelInfo.h"
#include "VPDet/VPGeometry.h"

#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"

namespace LHCb::Pr::Velo {

  namespace VPInfos {
    constexpr int NPlanes           = 26;
    constexpr int NModulesPerPlane  = 2;
    constexpr int NModules          = NPlanes * NModulesPerPlane;
    constexpr int NSensorsPerModule = 4;
    constexpr int NSensors          = NModules * NSensorsPerModule;
    constexpr int NSensorsPerPlane  = NModulesPerPlane * NSensorsPerModule;
  } // namespace VPInfos

  namespace TrackParams {
    constexpr float max_scatter_seeding    = 0.1f;
    constexpr float max_scatter_forwarding = 0.1f;
    constexpr float max_scatter_3hits      = 0.02f;
    constexpr int   max_allowed_skip       = 1;
  } // namespace TrackParams

  /**
   * @class ClusterTrackingSIMD ClusterTrackingSIMD.h
   * Experimental Clustering and Tracking algorithm using SIMD (WIP)
   *
   * @author Arthur Hennequin (CERN, LIP6)
   */
  class ClusterTrackingSIMD : public Gaudi::Functional::MultiTransformer<std::tuple<Hits, Tracks, Tracks>(
                                                                             const RawEvent&, const VPGeometry& ),
                                                                         DetDesc::usesConditions<VPGeometry>> {

  public:
    /// Standard constructor
    ClusterTrackingSIMD( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm execution
    std::tuple<Hits, Tracks, Tracks> operator()( const RawEvent&, const VPGeometry& ) const override;

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };
} // namespace LHCb::Pr::Velo

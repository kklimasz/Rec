/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

//#define DEBUG_HISTO // fill some histograms while the algorithm runs.
//#define DEBUG_LOGIC // control flow logic checker to spot issues.
// Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/PrVeloTracks.h"
#include "Event/RawEvent.h"
#include "Event/Track_v2.h"
#include "Event/VPLightCluster.h"

// Rec
#include "PrKernel/VeloPixelInfo.h"

// Local
#include "PrPixelTrack.h"
#include "VPClus.h"
#include "VPDet/VPTrackingInfo.h"

#include "Kernel/STLExtensions.h"
#include "fastAtan2.h"
#include <boost/container/static_vector.hpp>

#include <string_view>

namespace LHCb::Pr::Velo {

  enum SearchDirection {
    Default  = 0,
    Forward  = 1, // Move from most Downstream module in z backwards in z for the pair and track extrapolation
    Backward = 2
  }; // Move from most Upstream module in z forward in z for the pair creation and track extrapolation

  enum AddHitMode {
    ExactExtimation = 0, // Hit on the extrapolated line are added calculating hte expected phi value
    SameSide        = 1, // Hit on the extrapolated line are added assuming a constant phi in the same side module
    ChangeSide      = 2  // Hit on the extrapolated line are added assuming a constran phi in the opposite side module
  };
  // Overall configuration of tracking strategy in PixelTracking, which can be passed as argument as specified in the
  // PrPixelTracking.cpp first lines
  namespace VPConf {
    enum class ConfAlgo {
      DefaultAlgo         = 0,
      OnlyForward         = 1,
      OnlyBackward        = 2,
      ForwardThenBackward = 3,
      BackwardThenForward = 4
    };
    std::string   toString( const ConfAlgo& configuration );
    std::ostream& toStream( const ConfAlgo& configuration, std::ostream& os ) {
      return os << std::quoted( toString( configuration ), '\'' );
    }
    StatusCode parse( ConfAlgo& result, std::string_view input );
    // and allow printout..
    inline std::ostream& operator<<( std::ostream& os, const ConfAlgo& s ) { return toStream( s, os ); }
  } // namespace VPConf

  /** @class PrPixelTracking PrPixelTracking.h
   *  This is the main tracking for the Velo Pixel upgrade
   *
   *  @author Olivier Callot
   *  @author Sebastien Ponce
   */

  /**
    Pattern recognition is achieved using the hitbuffer container.
    It simply contains a list of size_t indices pointing to the index where the hit is placed in TES
    inside the flat container.
    Thus, hibuffer[foundhit-1], is the last hit found added on the track
    Clusters in TES are sorted by Phi value and offsets indices indicates in order the starting point in the vector of
    moduleID.
    Sorting is done as module = [0,1,2,3,4,5,6,7.....,51]
    You have 53 offsets value pointing to [module0beg, module1beg, .....module51beg, module51end]
    The beg of next module is the end of previous.
    We sort the clusters by phi in each module in VPClus algorithm, thus we have to recompute at the beginning of the
    algorithm the value of phi of each hit, stored in the hits_phi vector.

    Phi windows are used in different modes in the track building step.
    //Under ChangeSide and SameSide, you expect to have the same phi of the previous hit.
  */

  class PixelTracking : public Gaudi::Functional::MultiTransformer<
                            std::tuple<Tracks, Tracks>( const EventContext&, const std::vector<VPLightCluster>&,
                                                        const std::array<unsigned, VeloInfo::Numbers::NOffsets>& ),
                            Gaudi::Functional::Traits::BaseClass_t<DetDesc::ConditionAccessorHolder<
#ifdef DEBUG_HISTO
                                GaudiHistoAlg
#else
                                GaudiAlgorithm
#endif
                                >>> {

  public:
    /// Standard constructor
    PixelTracking( const std::string& name, ISvcLocator* pSvcLocator );

    /// Algorithm initialization
    StatusCode initialize() override;

    /// Algorithm execution
    std::tuple<Tracks, Tracks> operator()( const EventContext& ctx, const std::vector<VPLightCluster>& clusters,
                                           const std::array<unsigned, VeloInfo::Numbers::NOffsets>& ) const override;

  private:
    /// Compute and store aligned to VPLightCluster ordering the phi values (requires sorting to be done in clustering)
    std::vector<float> getPhi( span<const VPLightCluster>                        clusters,
                               span<const unsigned, VeloInfo::Numbers::NOffsets> offsets ) const;

    template <SearchDirection configuration>
    void updatenextstationsearch( unsigned int& next, int value ) const;

    template <SearchDirection configuration>
    void doPairSearch( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                       span<unsigned char>                                      clusterIsUsed,
                       const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                       std::tuple<Tracks, Tracks>& outputTracks, std::vector<size_t>& ThreeHitVec, PixelTrack& FitTrack,
                       boost::container::static_vector<size_t, 35>& hitbuffer, span<const float> phi_hits,
                       span<const unsigned int> modulesToLoop ) const;

    /// Extrapolate a seed track and try to add further hits.
    template <SearchDirection configuration>
    void extendTrack( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                      const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                      boost::container::static_vector<size_t, 35>& hitbuffer, unsigned int currentModule,
                      span<const float> phi_hits, span<unsigned char> clusterIsUsed ) const;

    /// Search for tracks starting from pair of hits on adjacent sensors
    std::tuple<Tracks, Tracks> searchByPair( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                                             const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                             span<const float>                                        phi_hits ) const;

    /// Produce Track list understandable to other LHCb applications.
    template <SearchDirection configuration>
    void makeLHCbTracks( const PixelTrack& track, const boost::container::static_vector<size_t, 35>& hitbuffer,
                         span<const VPLightCluster> clusters, std::tuple<Tracks, Tracks>& outputTracks ) const;

    template <AddHitMode mode>
    size_t bestHit( const VPTrackingInfo& vpInfo, span<const VPLightCluster> clusters,
                    const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                    const unsigned int moduleIDlastAdded, const unsigned int foundHits, const unsigned int next,
                    const boost::container::static_vector<size_t, 35>& hitbuffer, span<const float> phi_hits,
                    span<unsigned char> clusterIsUsed ) const;

    void printTrack( PixelTrack& track ) const;

    /// Properties
    Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.400, "X Slope limit for seed pairs"};
    Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.400, "Y Slope limit for seed pairs"};
    Gaudi::Property<float> m_extraTol{this, "ExtraTol", 0.6 * Gaudi::Units::mm, "Tolerance window when adding hits"};
    Gaudi::Property<unsigned int> m_maxMissedConsecutive{
        this, "MaxMissedConsecutive", 1,
        "Number of consective pairs of modules (left/right) without a hit after which to stop extrapolation!"};
    Gaudi::Property<unsigned int> m_maxMissedOnTrack{
        this, "MaxMissedOnTrack", 3,
        "Number of pairs of modules (left/right) without a hit on the full track building when extrapolating"};
    float                  m_maxScatterSq = 0.0;
    Gaudi::Property<float> m_maxScatter{this,
                                        "MaxScatter",
                                        0.004,
                                        [=]( auto& ) {
                                          m_maxScatterSq = std::pow( m_maxScatter.value(), 2 );
                                        }, // update the square of the scatter to avoid calls to sqrt()
                                        Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                        "Acceptance criteria for adding new hits [(DeltaX^{2} + DeltaY^{2} )/ ( "
                                        "DeltaZ^{2}) ] , where DeltaX = x-xExtrapolated , best Scatter is taken."};
    Gaudi::Property<float> m_maxChi2Short{this, "MaxChi2Short", 20.0,
                                          "Acceptance criteria for track candidates : Max. chi2 for 3-hit tracks"};
    Gaudi::Property<float> m_fractionUnused{this, "FractionUnused", 0.5,
                                            "Acceptance criteria for track candidates : Min. fraction of unused hits"};
    Gaudi::Property<bool>  m_stateClosestToBeamKalmanFit{this, "ClosestToBeamStateKalmanFit", true,
                                                        "Parameter for Kalman fit"};
    Gaudi::Property<bool>  m_stateEndVeloKalmanFit{this, "EndVeloStateKalmanFit", false, "Parameter for Kalman fit"};
    Gaudi::Property<VPConf::ConfAlgo> m_ConfAlgo{this, "AlgoConfig", VPConf::ConfAlgo::DefaultAlgo};
    // Specific settings for Forward and Backward search
    // Values -100. , +100. is used due to MC studies looking to tracks with eta >0 and eta <0 observing that dr/dz is
    // expected to be >0 ( <0 ) for tracks with hits at z>-100. (<100.) mm
    Gaudi::Property<float> m_ForwardTracks_minZ{
        this, "MinZ_ForwardTracks", -100.,
        "Break doublet building when searching Forward Tracks at z = MinZ_ForwardTracks [mm]"};
    Gaudi::Property<float> m_BackwardTracks_maxZ{
        this, "MaxZ_BackwardTracks", +100.,
        "Break doublet building when searching Backward Tracks at z = MaxZ_ForwardTracks [mm]"};

    Gaudi::Property<bool>  m_hardFlagging{this, "HardFlagging", false,
                                         "Do not re-use flagged hits, flag also 3 hit tracks and break"};
    Gaudi::Property<bool>  m_skiploopsens{this, "SkipLoopSens", false, "Skip 1 station in pair creation"};
    Gaudi::Property<float> m_PhiPairs{this, "PhiWindow", 5.5,
                                      "Tolerance in DeltaPhi to pick hits in doublet search for the modules"};
    Gaudi::Property<float> m_PhiExtrap{this, "PhiWindowExtrapolation", 5.5,
                                       "Tolerance in DeltaPhi to pick hits in hit propagation"};

    // PixelTracking("PrPixelTrackingFast").ModulesToSkip = [ 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 26, 27, 28, 29, 30,
    // 31, 38, 39, 40, 41, 42, 43 ] 94% tr. effs.

    Gaudi::Property<std::vector<unsigned int>> m_modulesToSkip{
        this, "ModulesToSkip", {}, "List of modules that should be skipped in decoding and tracking"};
    Gaudi::Property<std::vector<unsigned int>> m_modulesToSkipForPairs{
        this,
        "ModulesToSkipForPairs",
        {},
        "List of modules that should be skipped when creating seed pairs but not when propagating tracks"};

    Gaudi::Property<bool> m_earlykill3hittracks{
        this, "EarlyKill3HitTracks", false,
        "Enforce kill of triplets same side (from consecutive modules and early check in Phi)"};

    Gaudi::Property<bool> m_usePhiPerRegionForward{this, "UsePhiPerRegionsForward", false,
                                                   "Use Phi Per regionds forward search "};

    Gaudi::Property<std::vector<float>> m_PhiWindowsForwardPerRegions{
        this, "PhiWindowsPerRegionForward", {5.0f, 15.0f, 10.0f, 30.0f}, "Region in Forward Search Phi Windows "};

    Gaudi::Property<bool> m_doDrDzCut{this, "DoDrDzCut", true,
                                      "Allow DrDz > and < 0 in all modules for pair building "};
    // < 35 Region 1 , < 43 region 2, < 50 region 3
    const std::array<unsigned int, 4> m_ForwardRegionModulesID = {35, 43, 49, VP::NModules - 1};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCounter{this, "NbVeloTracksProduced"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_clustersCounter{this, "NbClustersProduced"};

    /// List of modules to be skipped in decoding and tracking
    std::bitset<VP::NModules> m_modulesToSkipMask;
    /// List of modules to be skipped when creating seed pairs
    std::bitset<VP::NModules> m_modulesToSkipForPairsMask;

    Gaudi::Property<bool> m_boostPhysics{
        this, "BoostPhysics", false,
        "Use the full atan2 computation in track-follow extrapolation, allow for a better physics outcome"};

    Gaudi::Property<bool> m_addQoverP{this, "AddQoverP", true, "Add a default q/p to the track states"};

    Gaudi::Property<float> m_ptVelo{this, "ptVelo", 400 * Gaudi::Units::MeV, "Default pT for Velo tracks"};

    const size_t badhit = 999999999;

    ConditionAccessor<VPTrackingInfo> m_vpInfo{this, "VPTrackingInfo", VPTrackingInfo::CondKey};
  };
} // namespace LHCb::Pr::Velo

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FastAtan2_H
#define FastAtan2_H 1

#include <cmath>
constexpr float ONEQTR_PI   = M_PI / 4.0;
constexpr float THRQTR_PI   = 3.0 * M_PI / 4.0;
constexpr float RADDEG      = 180.0 / M_PI;
constexpr float PI_FLOAT    = M_PI;
constexpr float PIBY2_FLOAT = M_PI / 2;

inline float atan2_approximation1( const float y, const float x ) noexcept {
  float abs_y = std::abs( y ) + 1e-10f;
  // used to prevent 0/0 conditio
  if ( x < 0.0f ) {
    float r     = ( x + abs_y ) / ( abs_y - x );
    float angle = THRQTR_PI + ( 0.1963f * r * r - 0.9817f ) * r;
    ;
    if ( y < 0.0f ) return -angle * RADDEG;
    return angle * RADDEG;
  } else {
    float r     = ( x - abs_y ) / ( x + abs_y );
    float angle = ONEQTR_PI + ( 0.1963f * r * r - 0.9817f ) * r;
    ;
    if ( y < 0.0f ) return -angle * RADDEG;
    return angle * RADDEG;
  }
}

inline float faster_atan2( const float y, const float x ) noexcept {
  const float coeff_1 = M_PI / 4.f;
  const float coeff_2 = 3.f * coeff_1;
  float       abs_y   = fabs( y );

  float nom = x < 0 ? ( x + abs_y ) : ( x - abs_y );
  float den = x < 0 ? ( abs_y - x ) : ( x + abs_y );

  float r     = nom / den;
  float angle = ( x < 0 ? coeff_2 : coeff_1 ) - coeff_1 * r;

  return y < 0 ? -angle : angle;
}

#endif // FASTATAN2_H

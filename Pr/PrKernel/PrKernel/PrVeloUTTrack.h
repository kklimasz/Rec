/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRVELOUTTRACK_H
#define PRVELOUTTRACK_H 1

#include "Event/Track.h"
#include "Kernel/LHCbID.h"
#include "boost/container/static_vector.hpp"

// Include files
/** @class PrVeloUTTrack PrVeloUTTrack.h
 *  Small class to hold output of VeloUT.
 *
 *  @author Michel De Cian
 *  @date   2018-02-08
 */

struct PrVeloUTTrack final {

  using LHCbIDs = boost::container::static_vector<LHCb::LHCbID, 8>;

  float              qOverP;
  LHCbIDs            UTIDs;
  const LHCb::Track* veloTr;
};

using PrVeloUTTracks = std::vector<PrVeloUTTrack>;

#endif // PRVUTTRACK_H

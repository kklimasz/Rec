/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRKERNEL_UTHITINFO_H
#define PRKERNEL_UTHITINFO_H 1
// Include files
#include "Event/UTLiteCluster.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include <string>
/** @class PrUTHitInfo PrUTHitInfo.h PrKernel/PrUTHitInfo.h
 *  @author Renato Quagliani, Christoph Hasse
 *  @date   2016-11-15
 */
namespace UT {
  namespace Info {
    //--- Final Hit Location
    const std::string HitLocation = "UT/UTHits";
    //--- Detector Location of UT
    const std::string DetLocation = DeUTDetLocation::UT;
    // DeUTDetLocation::location("UT");
    //--- Cluster location
    const std::string ClusLocation = LHCb::UTLiteClusterLocation::UTClusters;

    const int kMinStation = 0;  ///< Minimum valid station number for UT
    const int kMaxStation = 1;  ///< Maximum valid station number for UT
    const int kNStations  = 2;  ///< Number of UT stations
    const int kMinLayer   = 0;  ///< Minimum valid layer number for a UT station
    const int kMaxLayer   = 1;  ///< Maximum valid layer number for a UT station
    const int kNLayers    = 2;  ///< Number of UT layers within a station
    const int kMinRegion  = 0;  ///< Minimum valid region number for a UT layer
    const int kMaxRegion  = 11; ///< Maximum valid region number for a UT layer
    const int kNRegions   = 12; ///< Number of UT regions within a layer
  }                             // namespace Info
} // namespace UT
#endif // PRKERNEL_UTHITINFO_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IPRDEBUGMATCHTOOL_H
#define IPRDEBUGMATCHTOOL_H 1

// Include files

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

// forward declarations

/** @class PrDebugMatchTool
 *
 *  @author Sevda Esem
 *  @date   2017-02-21
 */
struct IPrDebugMatchTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IPrDebugMatchTool, 2, 0 );

  virtual int matchMCPart( const LHCb::Track& velo, const LHCb::Track& seed ) const = 0;

  virtual void fillTuple( const LHCb::Track& velo, const LHCb::Track& seed, const std::vector<float>& vars ) const = 0;
};
#endif // IPRDEBUGMATCHTOOL_H

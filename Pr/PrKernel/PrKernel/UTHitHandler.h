/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STl
#include <cassert>
#include <cstdint>
#include <vector>

// Include files
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "Kernel/STLExtensions.h"
#include "PrKernel/UTHit.h"
#include "PrKernel/UTHitInfo.h"

//#include <boost/container/small_vector.hpp>

/**
 *  UTHitHandler contains the hits in the UT detector and the accessor to them
 */
namespace UT {
  class HitHandler {

  private:
    /// Type for hit storage
    using HitVector = std::vector<UT::Hit>;

    /// Internal indices storage for ranges
    using HitIndices = std::pair<std::size_t, std::size_t>;
    /*
    using HitsInRegion  = std::array<HitIndices, 98>;
    using HitsInLayer   = std::array<HitsInRegion, 3>;
    using HitsInStation = std::array<HitsInLayer, 2>;
    using HitsInUT      = std::array<HitsInStation, 2>;*/

    using HitsInUT = std::array<HitIndices, 2 * 2 * 3 * 98>;

  public:
    using HitRange = Gaudi::Range_<HitVector, HitVector::const_iterator>;
    // using HitRange = LHCb::span<const UT::Hit>; // use this ??

  public:
    // Method to add Hit in the container
    void AddHit( const DeUTSector* aSector, unsigned int station, unsigned int layer, unsigned int region,
                 unsigned int sector, unsigned int strip, double fracStrip, LHCb::UTChannelID chanID, unsigned int size,
                 bool highThreshold ) {
      double dxDy{0};
      double dzDy{0};
      double xAtYEq0{0};
      double zAtYEq0{0};
      double yBegin{0};
      double yEnd{0};
      //--- this method allow to set the values
      const auto fracStripOvfour = fracStrip / 4;
      aSector->trajectory( strip, fracStripOvfour, dxDy, dzDy, xAtYEq0, zAtYEq0, yBegin, yEnd );
      const auto cos   = aSector->cosAngle();
      const auto error = aSector->pitch() / std::sqrt( 12.0 );

      if ( UNLIKELY( dzDy != 0 ) ) { throw GaudiException( "dzDy is not zero", "UTHitHandler", StatusCode::FAILURE ); }

      // NB : The way the indices are setup here assumes all hits for a given
      //      station, layer, region and sector come in order, which appears
      //      to be the case. This must remain so...
      //
      //      Currently, what I am seeing from the MC has this sorting.
      //      But this would also need to be the case for real data.

      const int fullChanIdx = ( ( ( station - 1 ) * 2 + ( layer - 1 ) ) * 3 + ( region - 1 ) ) * 98 + ( sector - 1 );

      // get the indices for this region
      auto& indices = m_indices[fullChanIdx]; // m_indices[station - 1][layer - 1][region - 1][sector - 1];

      // if first for this range, set the begin and end indices
      if ( &indices != last_indices ) {
        // check to see if thi range has been filled previously.
        // If it has, assumed ordering is broken
        assert( indices.first == indices.second );
        // reset indices to current end of container
        indices = {m_allhits.size(), m_allhits.size()};
        // update used last index cache
        last_indices = &indices;
      }

      // add a new hit
      m_allhits.emplace_back( chanID, size, highThreshold, dxDy, xAtYEq0, zAtYEq0, yBegin, yEnd, cos, error, strip,
                              fracStripOvfour );

      // increment the end index for current range
      ++( indices.second );
    }

    /// Reserve size in the overall hit container
    void reserve( const std::size_t nHits ) { m_allhits.reserve( nHits ); }

    /// Access the range for a given set of hits
    const HitRange hits( unsigned int station, unsigned int layer, unsigned int region, unsigned int sector ) const {
      const int   fullChanIdx = ( ( ( station - 1 ) * 2 + ( layer - 1 ) ) * 3 + ( region - 1 ) ) * 98 + ( sector - 1 );
      const auto& indices =
          m_indices[fullChanIdx]; // const auto& indices = m_indices[station - 1][layer - 1][region - 1][sector - 1];
      return HitRange( m_allhits.begin() + indices.first, m_allhits.begin() + indices.second );
    }

    /// Access the range for a given set of hits
    const HitRange hits( const int fullChanIdx ) const {
      const auto& indices = m_indices[fullChanIdx];
      return HitRange( m_allhits.begin() + indices.first, m_allhits.begin() + indices.second );
    }

    /// get the total number of hits
    HitVector::size_type nbHits() const noexcept { return m_allhits.size(); }

  private:
    // Indices for each range
    HitsInUT m_indices;

    // single vector of all hits
    HitVector m_allhits;

    // cache pointer to last indices used
    HitIndices* last_indices = nullptr;
  };
} // namespace UT

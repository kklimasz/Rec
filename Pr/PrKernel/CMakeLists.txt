###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PrKernel
################################################################################
gaudi_subdir(PrKernel v1r10)

gaudi_depends_on_subdirs(Event/RecEvent
                         Kernel/LHCbKernel
                         Tf/TfKernel
                         GaudiAlg)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(PrKernel
                  src/*.cpp
                  PUBLIC_HEADERS PrKernel
                  LINK_LIBRARIES RecEvent GaudiAlgLib)

gaudi_add_dictionary(PrKernel
                     dict/PrKernelDict.h
                     dict/PrKernelDict.xml
                     INCLUDE_DIRS Pr/PrKernel
                     LINK_LIBRARIES RecEvent GaudiAlgLib
                     OPTIONS "-U__MINGW32__")

gaudi_add_unit_test(TestSelection
                    tests/src/TestSelection.cpp
                    LINK_LIBRARIES RecEvent GaudiKernel
                    TYPE Boost)

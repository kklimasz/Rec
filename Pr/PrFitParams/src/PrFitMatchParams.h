/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFITMATCHPARAMS_H
#define PRFITMATCHPARAMS_H 1

#include "PrFitParameters.h"
#include "PrFitTool.h"

#include "Event/MCTrackInfo.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/ILHCbMagnetSvc.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Point3DTypes.h"

#include <vector>

/** @class PrFitMatchParams PrFitMatchParams.h
 *  Parameterize the PrMatch tracks
 *
 *  Algorithm to calculate the different parameters in PatMatch, similar to FwdFitParams it tries to fit polynomial
 * dependencies. Uses purely MC information from MCHits ie this will only run when they are available (XDIGI, XDST) Only
 * Velo and T-station information is used. Some UT information is calculated, but not used at the moment.
 *
 *  - NTupleName: Name of the output nTuple
 *  - ZUT1: z-position of reference point in UT
 *  - ZRef: z-position of reference plane in T-stations
 *  - ZVelo: z-position of reference plane in Velo
 *  - zMagnetParams: Initial parameters for calculation of z of 'kink-position' of magnet
 *  - momentumParams: Initial paramters to calculate the momentum.
 *  - bendYParams: Initial paramters to calculate the bending in y.
 *  - MaxZVertex: Maximum z-position of PV to take tracks into account.
 *  - MinMomentum: Minimum momentum to take tracks into account.
 *  - zMatchY: z-position of matching in Y.
 *  - RequireUTHits: Require presence of UT hits for the calculation
 *  - MagnetScaleFactor: The scale factor for the magnet. Can be obtained with the field service, but hardcoded here
 * such that it can be run without. Note that only as many paramters are used for the caluclation as are given as input,
 * although internally more might be defined. The momentum paramters are not directly set in 'PatMatchTool', but in
 * 'FastMomentumEstimate', and only the cubic solution is calculated at the moment. Nothing else from the Brunel
 * sequence is needed when running this code, a possible way to run it would be:
 *
 *  @code
 *  def doIt():
 *      GaudiSequencer("BrunelSequencer").Members =  [ PrFitMatchParams("PrFitMatchParams", ZRef = 8520, zMagnetParams =
 * [ ... ], momentumParams = [ ... ], bendYParams = [ ... ]) ]
 *
 *  appendPostConfigAction( doIt )
 *  @endcode
 *
 *
 *  @author Michel De Cian
 *  @date   2014-12-21
 *  @copied by Sevda Esen for the upgrade
 */
class PrFitMatchParams : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;
  IPrFitTool*     m_fitTool;

  Gaudi::Property<std::string> m_tupleName{this, "NTupleName", "Track"};
  Gaudi::Property<std::string> m_inputLocationSeed{this, "SeedInput", LHCb::TrackLocation::Seed};

  Gaudi::Property<double> m_zUT1{this, "ZUT1", 2469.0 * Gaudi::Units::mm};
  Gaudi::Property<double> m_zRef{this, "ZRef", 9410.0 * Gaudi::Units::mm};
  Gaudi::Property<double> m_zVelo{this, "ZVelo", 0.0 * Gaudi::Units::mm};
  Gaudi::Property<double> m_maxZVertex{this, "MaxZVertex", 500 * Gaudi::Units::mm};
  Gaudi::Property<double> m_minMomentum{this, "MinMomentum", 2 * Gaudi::Units::GeV};
  Gaudi::Property<double> m_maxMomentum{this, "MaxMomentum", 1000 * Gaudi::Units::GeV};
  Gaudi::Property<double> m_zMatchY{this, "zMatchY", 8420 * Gaudi::Units::mm};
  Gaudi::Property<double> m_magnetScaleFactor{this, "MagnetScaleFactor", -1};

  Gaudi::Property<bool> m_requireUTHits{this, "RequireUTHits", false};
  Gaudi::Property<bool> m_resolution{this, "Resolution", true};

  Gaudi::Property<std::vector<double>> m_zMagParams{this, "zMagnetParams", {0., 0., 0., 0., 0.}};
  Gaudi::Property<std::vector<double>> m_momParams{this, "momentumParams", {0., 0., 0., 0., 0., 0.}};
  Gaudi::Property<std::vector<double>> m_bendYParams{this, "bendParamYParams", {0., 0.}};
  PrFitParameters                      m_zMagPar;
  PrFitParameters                      m_momPar;
  PrFitParameters                      m_bendParamY;

  float momentum( const LHCb::State* vState, const LHCb::State* tState );
  void  resolution();
  int   m_nEvent{0};
  int   m_nTrack{0};
};
#endif // MATCHFITPARAMS_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFITTOOL_H
#define PRFITTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/Point3DTypes.h"

#include "PrFitParams/IPrFitTool.h"
#include "PrFitParams/LinParFit.h"

/** @class PrFitTool PrFitTool.h
 *
 *
 *  @author Olivier Callot
 *  @date   2006-12-08
 */

class PrFitTool final : public extends<GaudiTool, IPrFitTool> {
public:
  /// Standard constructor
  using extends::extends;

  std::optional<std::tuple<double, double>> fitLine( const std::vector<Gaudi::XYZPoint>& hit, XY mode,
                                                     double z0 ) const override;

  std::optional<std::tuple<double, double, double>> fitParabola( const std::vector<Gaudi::XYZPoint>& hit, XY mode,
                                                                 double z0 ) const override;

  std::optional<std::tuple<double, double, double, double>> fitCubic( const std::vector<Gaudi::XYZPoint>& hit, XY mode,
                                                                      double z0 ) const override;

private:
  mutable LinParFit<double> m_fit2{2};
  mutable LinParFit<double> m_fit3{3};
  mutable LinParFit<double> m_fit4{4};
};
#endif // PRFITTOOL_H

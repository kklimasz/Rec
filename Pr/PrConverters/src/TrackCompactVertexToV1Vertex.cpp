/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrSelection.h"
#include "TrackKernel/TrackCompactVertex.h"
#include <assert.h>

/**
 *  Small helper to convert std::vector<LHCb::RecVertex> to LHCb::RecVertices
 */

namespace LHCb::Converters::TrackCompactVertex {

  struct VectorOf2Trackv2CompactVertexToVectorOfRecVertex
      : public Gaudi::Functional::Transformer<std::vector<LHCb::RecVertex>(
            std::vector<LHCb::TrackKernel::TrackCompactVertex<2, double>> const&,
            const Pr::Selection<LHCb::Event::v2::Track>&, const std::vector<LHCb::Track>& )> {
    VectorOf2Trackv2CompactVertexToVectorOfRecVertex( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer(
              name, pSvcLocator,
              {KeyValue{"InputVertices", ""}, KeyValue{"TracksInVertices", ""}, KeyValue{"ConvertedTracks", ""}},
              KeyValue{"OutputVertices", ""} ) {}
    /// The main function, converts the vertex and puts it into a keyed container
    std::vector<LHCb::RecVertex>
    operator()( const std::vector<LHCb::TrackKernel::TrackCompactVertex<2, double>>& vertices,
                const Pr::Selection<LHCb::Event::v2::Track>&                         tracks,
                const std::vector<LHCb::Track>&                                      conv_tracks ) const override {
      std::vector<LHCb::RecVertex> converted_vertices;
      for ( const auto& vertex : vertices ) {
        auto converted_vertex = LHCb::RecVertex{vertex.position()};
        // converted_vertex->setTechnique( static_cast<LHCb::RecVertex::RecVertexType>( vertex.technique() ) );
        converted_vertex.setChi2( vertex.chi2() );
        converted_vertex.setNDoF( vertex.nDoF() );
        converted_vertex.setCovMatrix( vertex.posCovMatrix() );
        for ( int i = 0; i < 2; ++i ) {
          auto const& trk = tracks[vertex.child_relations()[i].index()];
          auto        track_in_converted_container =
              std::find_if( std::begin( conv_tracks ), std::end( conv_tracks ), [&]( const auto& t ) {
                return ( ( t.nLHCbIDs() == trk.nLHCbIDs() ) && ( t.containsLhcbIDs( trk.lhcbIDs() ) ) );
              } );
          assert( track_in_converted_container != std::end( conv_tracks ) );
          converted_vertex.addToTracks( &*track_in_converted_container, 1 ); // TODO 1 -> real weight
        }
        converted_vertices.push_back( std::move( converted_vertex ) );
      }
      return converted_vertices;
    }
  };
  DECLARE_COMPONENT( VectorOf2Trackv2CompactVertexToVectorOfRecVertex )
} // namespace LHCb::Converters::TrackCompactVertex

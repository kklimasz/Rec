/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H
#define LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H

// Include files
// from Gaudi
#include "Event/Track.h"
#include "GaudiAlg/ScalarTransformer.h"
#include "PrKernel/PrVeloUTTrack.h"

/** @class fromPrVeloUTTrack fromPrVeloUTTrack.h
 *
 *  Small helper to convert std::vector of custom class for output of PrVeloUT
 *  to LHCb::Tracks.
 *
 *  @author Michel De Cian
 *  @date   2018-03-09
 */

namespace LHCb {
  namespace Converters {
    namespace Track {
      namespace v1 {

        struct fromPrVeloUTTrack
            : public Gaudi::Functional::ScalarTransformer<fromPrVeloUTTrack, LHCb::Tracks( const PrVeloUTTracks& )> {

          fromPrVeloUTTrack( const std::string& name, ISvcLocator* pSvcLocator )
              : ScalarTransformer( name, pSvcLocator, KeyValue{"InputTracksName", LHCb::TrackLocation::VeloTT},
                                   KeyValue{"OutputTracksName", "Rec/Track/VeloTTLHCbTracks"} ) {}

          using ScalarTransformer::operator();

          /// The main function, converts the track
          LHCb::Track* operator()( const PrVeloUTTrack& track ) const {

            auto outTr = std::make_unique<LHCb::Track>( *( track.veloTr ) );
            // set q/p in all of the existing states
            for ( auto& state : outTr->states() ) state->setQOverP( track.qOverP );

            // -- As we don't need the state in the UT, it is not added in PrVeloUT
            // -- and can't be added here.
            outTr->addToLhcbIDs( track.UTIDs );
            outTr->setType( LHCb::Track::Types::Upstream );
            outTr->setHistory( LHCb::Track::History::PrVeloUT );
            outTr->addToAncestors( track.veloTr );
            outTr->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );

            return outTr.release();
          }
        };
        DECLARE_COMPONENT( fromPrVeloUTTrack )
      } // namespace v1
    }   // namespace Track
  }     // namespace Converters
} // namespace LHCb

#endif // LHCB_CONVERTERS_TRACK_V1_FROMPRVELOUTTRACK_H

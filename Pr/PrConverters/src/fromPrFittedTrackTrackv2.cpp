/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"

#include "Event/PrFittedForwardTracks.h"
#include "Event/PrForwardTracks.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"
#include "Event/PrZip.h"

// Gaudi
#include "GaudiKernel/StdArrayAsProperty.h"

/**
 * Converter between LHCb::Pr::Fitted::Forward::Tracks ( SoA PoD ) and vector<Track_v2>
 *
 *
 * @author Sascha Stahl
 */

namespace {
  using dType = SIMDWrapper::scalar::types;
  using I     = dType::int_v;
  using F     = dType::float_v;
  constexpr std::array<float, 5> default_covarianceValues{4.0, 400.0, 4.e-6, 1.e-4, 0.1};

  LHCb::State get_state( Vec3<F> const& pos, Vec3<F> const& dir, F const qop, Vec3<F> const& covX, Vec3<F> const& covY,
                         F const qopError ) {
    LHCb::State state;
    state.setState( pos.x.cast(), pos.y.cast(), pos.z.cast(), dir.x.cast(), dir.y.cast(), qop.cast() );
    state.covariance()( 0, 0 ) = covX.x.cast();
    state.covariance()( 0, 2 ) = covX.y.cast();
    state.covariance()( 2, 2 ) = covX.z.cast();
    state.covariance()( 1, 1 ) = covY.x.cast();
    state.covariance()( 1, 3 ) = covY.y.cast();
    state.covariance()( 3, 3 ) = covY.z.cast();
    state.covariance()( 4, 4 ) = qopError.cast();
    return state;
  }

  std::vector<LHCb::Event::v2::Track> convert_tracks( LHCb::Pr::Forward::Tracks const&         forward_tracks,
                                                      LHCb::Pr::Fitted::Forward::Tracks const& fitted_tracks,
                                                      LHCb::Pr::Velo::Hits const&              velo_hits,
                                                      std::array<float, 5> const               covarianceValues ) {
    std::vector<LHCb::Event::v2::Track> out;
    out.reserve( fitted_tracks.size() );

    for ( int t = 0; t < fitted_tracks.size(); t++ ) {
      auto  forward_track_index = fitted_tracks.trackFT<I>( t ).cast();
      auto& newTrack            = out.emplace_back();

      // set track flags
      newTrack.setType( LHCb::Event::v2::Track::Type::Long );
      newTrack.setHistory( LHCb::Event::v2::Track::History::PrForward );
      newTrack.setPatRecStatus( LHCb::Event::v2::Track::PatRecStatus::PatRecIDs );
      newTrack.setFitStatus( LHCb::Event::v2::Track::FitStatus::Fitted );
      // get momentum
      F qop      = fitted_tracks.QoP<F>( t );
      F qopError = covarianceValues[4] * qop * qop;

      // closest to beam state
      LHCb::State closesttobeam_state =
          get_state( fitted_tracks.beamStatePos<F>( t ), fitted_tracks.beamStateDir<F>( t ), qop,
                     fitted_tracks.covX<F>( t ), fitted_tracks.covY<F>( t ), qopError );
      closesttobeam_state.setLocation( LHCb::State::Location::ClosestToBeam );

      const Vec3<F> covX{covarianceValues[0], 0.f, covarianceValues[2]};
      const Vec3<F> covY{covarianceValues[1], 0.f, covarianceValues[3]};

      // scifi state
      LHCb::State scifi_state =
          get_state( forward_tracks.statePos<F>( forward_track_index ),
                     forward_tracks.stateDir<F>( forward_track_index ), qop, covX, covY, qopError );
      scifi_state.setLocation( LHCb::State::Location::AtT );

      // add states
      newTrack.addToStates( closesttobeam_state );
      newTrack.addToStates( scifi_state );

      // set chi2 / chi2ndof
      newTrack.setChi2PerDoF( LHCb::Event::v2::Track::Chi2PerDoF{fitted_tracks.chi2<F>( t ).cast(),
                                                                 fitted_tracks.chi2nDof<I>( t ).cast()} );

      // If we rely on pointers internally stored in the classes we can take it from fitted tracks
      auto lhcbids = fitted_tracks.lhcbIDs( t, velo_hits );
      newTrack.addToLhcbIDs( lhcbids, LHCb::Tag::Unordered_tag{} );
    }
    return out;
  }

} // namespace

namespace LHCb::Converters::Track::v2 {
  class fromPrFittedForwardTrackUsingAncestors : public Gaudi::Functional::Transformer<std::vector<Event::v2::Track>(
                                                     const Pr::Fitted::Forward::Tracks&, const Pr::Velo::Hits& )> {

  public:
    fromPrFittedForwardTrackUsingAncestors( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, {KeyValue{"FittedTracks", ""}, KeyValue{"VeloHits", ""}},
                       KeyValue{"OutputTracks", ""} ) {}
    Gaudi::Property<std::array<float, 5>> m_covarianceValues{this, "covarianceValues", default_covarianceValues};

    std::vector<Event::v2::Track> operator()( const Pr::Fitted::Forward::Tracks& fitted_tracks,
                                              const Pr::Velo::Hits&              velo_hits ) const override {
      auto const* forward_tracks = fitted_tracks.getForwardAncestors();
      if ( forward_tracks == nullptr ) {
        error()
            << "Forward tracks container of Fitted forward tracks does not exist. Conversion to track v2 will not work."
            << endmsg;
        return std::vector<Event::v2::Track>{};
      }
      std::vector<Event::v2::Track> out =
          convert_tracks( *forward_tracks, fitted_tracks, velo_hits, m_covarianceValues );
      m_nbTracksCounter += out.size();
      return out;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  }; // namespace
  DECLARE_COMPONENT( fromPrFittedForwardTrackUsingAncestors )

} // namespace LHCb::Converters::Track::v2

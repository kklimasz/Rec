/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// Mathlib
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#include "Event/State.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/LUTForFunction.h"
#include "Kernel/STLExtensions.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// local
#include "PrUTMagnetTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrUTMagnetTool
//
// 2006-09-25 : Mariusz Witek
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::UTMagnetTool, "PrUTMagnetTool" )

namespace LHCb::Pr {
  namespace {
    struct bdl_t {
      float BdlTrack, zHalfBdlTrack;
    };

    bdl_t f_bdl( const IMagneticFieldSvc& fieldSvc, float slopeY, float zOrigin, float zStart, float zStop ) {

      if ( zStart > zStop ) return {0, 0};

      float Bdl      = 0.0;
      float zHalfBdl = 0.0;

      Gaudi::XYZPoint  aPoint( 0., 0., 0. );
      Gaudi::XYZVector bField;

      constexpr int np = 500;
      float         dz = ( zStop - zStart ) / np;
      float         dy = dz * slopeY;
      float         z  = zStart + dz / 2.;
      float         y  = slopeY * ( zStart - zOrigin );

      // vectors to calculate z of half Bdl
      std::vector<float> bdlTmp, zTmp;
      bdlTmp.reserve( np + 1 );
      zTmp.reserve( np + 1 );
      while ( z < zStop ) {

        aPoint.SetY( y );
        aPoint.SetZ( z );

        fieldSvc.fieldVector( aPoint, bField ).ignore();
        Bdl += dy * bField.z() - dz * bField.y();
        if ( z > 100. * Gaudi::Units::cm ) {
          bdlTmp.push_back( Bdl );
          zTmp.push_back( z );
        }
        z += dz;
        y += dy;
      }

      float bdlhalf = std::abs( Bdl ) / 2.;

      for ( unsigned int i = 5; i < bdlTmp.size() - 5; i++ ) {
        if ( std::abs( bdlTmp[i] ) > bdlhalf ) {
          float zrat = ( Bdl / 2. - bdlTmp[i - 1] ) / ( bdlTmp[i] - bdlTmp[i - 1] );
          zHalfBdl   = zTmp[i - 1] + dz * zrat;
          break;
        }
      }

      return {Bdl, zHalfBdl};
    }

  } // namespace

  //=========================================================================
  //  setMidUT
  //=========================================================================
  StatusCode UTMagnetTool::initialize() {

    StatusCode sc = extends::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;       // error printed already by GaudiAlgorithm
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

    // retrieve pointer to magnetic field service
    m_magFieldSvc = svc<IMagneticFieldSvc>( "MagneticFieldSvc", true );

    // == Get the UT Detector Element
    m_UTDet = getDet<DeUTDetector>( DeUTDetLocation::UT );

    // subscribe to the updatemanagersvc with a dependency on the magnetic field svc
    IUpdateManagerSvc* m_updMgrSvc = svc<IUpdateManagerSvc>( "UpdateManagerSvc", true );
    ILHCbMagnetSvc*    m_fieldSvc  = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
    m_updMgrSvc->registerCondition( this, m_fieldSvc, &UTMagnetTool::updateField );

    // initialize with the current conditions
    return m_updMgrSvc->update( this );
  }

  //=========================================================================
  // Callback function for field updates
  //=========================================================================
  StatusCode UTMagnetTool::updateField() {
    prepareBdlTables();
    prepareDeflectionTables();

    // check whether B=0
    auto bdl  = f_bdl( *m_magFieldSvc, 0., 0., 400. * Gaudi::Units::mm, m_zCenterUT );
    m_noField = ( std::abs( bdl.BdlTrack ) < 10e-4 );

    m_zMidField = zBdlMiddle( 0.05, 0.0, 0.0 );

    if ( m_noField ) info() << " No B field detected." << endmsg;
    return StatusCode::SUCCESS;
  }

  //=========================================================================
  // prepareBdlTables
  //=========================================================================
  void UTMagnetTool::prepareBdlTables() {

    info() << "Start generation of VeloUT Bdl LUTs" << endmsg;
    // prepare table with Bdl integrations
    // Bdl integral depends on 3 track parameters
    //  slopeY     - y slope of the track
    //  zOrigin    - z of track intersection with z axis (in YZ projection)
    //  zVeloEnd   - z of the track at which slopeY is given
    //                      slopeY    zOrigin    zVeloEnd
    // m_zCenterUT is a normalization plane which should be close to middle of UT.
    // It is used to normalize dx deflection at different UT layers.
    // No need to update with small UT movement up to +- 5 cm.

    m_zCenterUT     = 2484.6;
    float zCenterUT = 0.;

    assert( m_UTDet->layers().size() == 4 );
    unsigned il = 0;
    for ( const auto& l : m_UTDet->layers() ) {
      float zlay      = l->sectors().front()->globalCentre().Z();
      m_zLayers[il++] = zlay;
      zCenterUT += zlay;
    }
    zCenterUT /= m_zLayers.size();

    if ( std::abs( m_zCenterUT - zCenterUT ) > 50. ) {
      warning() << "Calculated center of UT station far away from nominal value: " << zCenterUT << " wrt nominal "
                << m_zCenterUT << endmsg;
      warning() << " Calculated value taken: " << zCenterUT << endmsg;
      m_zCenterUT = zCenterUT;
    }
    // warning: layers a-priori not in order of increasing z!
    std::sort( m_zLayers.begin(), m_zLayers.end() );

    m_lutBdl.fillTable( [&]( LHCb::span<const float, 3> var ) {
      return f_bdl( *m_magFieldSvc, var[0], var[1], var[2], m_zCenterUT ).BdlTrack;
    } );

    m_lutZHalfBdl.fillTable( [&]( LHCb::span<const float, 3> var ) {
      return f_bdl( *m_magFieldSvc, var[0], var[1], var[2], m_zCenterUT ).zHalfBdlTrack;
    } );

    info() << "Generation of VeloUT Bdl LUTs finished" << endmsg;
  }

  //=========================================================================
  // prepareDeflectionTables
  //=========================================================================
  void UTMagnetTool::prepareDeflectionTables() {

    info() << "Start generation of VeloUT deflection LUTs" << endmsg;

    // prepare deflection tables

    // Retrieve extrapolators
    ITrackExtrapolator* my_linear    = tool<ITrackExtrapolator>( "TrackLinearExtrapolator" );
    ITrackExtrapolator* my_parabolic = tool<ITrackExtrapolator>( "TrackRungeKuttaExtrapolator" );

    // tmp state
    LHCb::State tmpState;
    float       qpBeg = 1. / ( 10. * Gaudi::Units::GeV );
    tmpState.setState( 0., 0., 0., 0., 0., qpBeg );
    // set dummy covariance matrix
    Gaudi::TrackSymMatrix cov = Gaudi::TrackSymMatrix();
    cov( 0, 0 )               = 0.1;
    cov( 1, 1 )               = 0.1;
    cov( 2, 2 )               = 0.1;
    cov( 3, 3 )               = 0.1;
    cov( 4, 4 )               = 0.1;
    tmpState.setCovariance( cov );

    // determine normalization factors for deflections in different UT layers
    // wrt center of UT
    auto dxLay = [&]( LHCb::span<const float, 2> lutVar ) {
      auto idLay   = int( lutVar[0] + 0.000001 );
      auto dydzBeg = lutVar[1];
      auto zLay    = m_zLayers[idLay];

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << lutVar[0] << " " << lutVar[1] << " "
                << " idlay " << idLay << " " << m_zLayers.size() << " zlay" << zLay << " " << endmsg;
      }

      tmpState.setState( 0., 0., 0., 0., dydzBeg, qpBeg );

      LHCb::State stalin_mid = tmpState;
      LHCb::State stapar_mid = tmpState;
      // extrapolate state to middle of UT
      StatusCode sc1 = my_linear->propagate( stalin_mid, m_zCenterUT );
      StatusCode sc2 = my_parabolic->propagate( stapar_mid, m_zCenterUT );
      if ( sc1.isFailure() || sc2.isFailure() ) { Warning( "Extrapolation failed ", StatusCode::SUCCESS, 0 ).ignore(); }

      float       ratio      = 0.;
      LHCb::State stalin_lay = tmpState;
      LHCb::State stapar_lay = tmpState;

      StatusCode sc3 = my_linear->propagate( stalin_lay, zLay );
      StatusCode sc4 = my_parabolic->propagate( stapar_lay, zLay );
      if ( sc3.isFailure() || sc4.isFailure() ) {
        Warning( "Extrapolation failed ", StatusCode::SUCCESS, 0 ).ignore();
      } else {
        auto dx_mid = stapar_mid.x() - stalin_mid.x();
        auto dx_lay = stapar_lay.x() - stalin_lay.x();
        if ( std::abs( dx_mid ) > 1.e-8 ) ratio = dx_mid / dx_lay;
      }
      return ratio;
    };

    m_lutDxLay.fillTable( dxLay );

    // distance to momentum table (depends on y slope only)
    auto dxToMom = [&]( LHCb::span<const float, 1> var ) {
      tmpState.setState( 0., 0., 0., 0., var[0], qpBeg );
      LHCb::State stalin_mid = tmpState;
      LHCb::State stapar_mid = tmpState;
      // extrapolate state to middle of UT
      StatusCode sc1 = my_linear->propagate( stalin_mid, m_zCenterUT );
      StatusCode sc2 = my_parabolic->propagate( stapar_mid, m_zCenterUT );

      float dx2mom = 0.;
      if ( sc1.isFailure() || sc2.isFailure() ) {
        Warning( "Extrapolation failed ", StatusCode::SUCCESS, 0 ).ignore();
      } else {
        float dx = stapar_mid.x() - stalin_mid.x();
        if ( std::abs( dx ) > 1e-8 ) dx2mom = qpBeg / dx;
      }
      return dx2mom;
    };

    m_lutDxToMom.fillTable( dxToMom );

    // determine distToMomentum parameter
    m_dist2mom = m_lutDxToMom.getValue( {0.05} );

    info() << "Generation of VeloUT deflection LUTs finished" << endmsg;
  }
} // namespace LHCb::Pr
//****************************************************************************

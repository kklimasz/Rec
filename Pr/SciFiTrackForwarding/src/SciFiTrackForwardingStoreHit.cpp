/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTInfo.h"
#include "SciFiTrackForwardingHits.h"
#include <boost/container/small_vector.hpp>
#include <limits>

#include <string>

#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"
#include "PrKernel/PrFTZoneHandler.h"

#include "range/v3/iterator_range.hpp"
#include "range/v3/view/transform.hpp"
#include <boost/numeric/conversion/cast.hpp>

/** @class SciFiTrackForwardingStoreHit SciFiTrackForwardingStoreHit.cpp
 *
 *  \brief Transforms FTLiteClusters into the input format needed by the SciFiTrackForwarding
 */

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

class SciFiTrackForwardingStoreHit
    : public Gaudi::Functional::Transformer<SciFiTrackForwardingHits( FTLiteClusters const& )> {
public:
  SciFiTrackForwardingStoreHit( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"HitsLocation", LHCb::FTLiteClusterLocation::Default},
                     KeyValue{"Output", "Rec/SciFiTrackForwarding/Hits"} ) {}

  StatusCode initialize() override;

  SciFiTrackForwardingHits operator()( FTLiteClusters const& ) const override;

  StatusCode buildGeometry();

private:
  /// detector element
  DeFTDetector* m_ftDet;

  /// derived condition caching computed zones
  PrFTZoneHandler* m_zoneHandler;

  /// Cached resolution
  std::array<float, 9> m_invClusResolution;

  /// partial SoA cache for mats
  std::array<float, 2 << 11>             m_mats_dxdy;
  std::array<float, 2 << 11>             m_mats_dzdy;
  std::array<float, 2 << 11>             m_mats_globaldy;
  std::array<float, 2 << 11>             m_mats_uBegin;
  std::array<float, 2 << 11>             m_mats_halfChannelPitch;
  std::array<float, 2 << 11>             m_mats_dieGap;
  std::array<float, 2 << 11>             m_mats_sipmPitch;
  std::array<Gaudi::XYZPointF, 2 << 11>  m_mats_mirrorPoint;
  std::array<Gaudi::XYZVectorF, 2 << 11> m_mats_ddx;
};

DECLARE_COMPONENT( SciFiTrackForwardingStoreHit )

SciFiTrackForwardingHits SciFiTrackForwardingStoreHit::operator()( FTLiteClusters const& clusters ) const {

  int                      size = clusters.size() + 2 * 24; // N clusters + 2 guards / zone
  SciFiTrackForwardingHits tmp{};

  auto& hitvec = tmp.hits;
  auto& IDvec  = tmp.IDs;

  hitvec.reserve( size );
  IDvec.reserve( size );

  // TODO: Verify that the hits are sorted as expected
  /*assert( hitHandler.hits().is_sorted( []( const auto& lhs, const auto& rhs ) { return lhs.x() < rhs.x(); } ) &&
          "FT hits must be properly sorted for the pattern recognition "
          "Lower by X for each zone" );*/

  // I want my hit zones in the order bottom zones for layer 0, 3, 1, 2 top 0, 3, 1, 2
  // This is to hopefully maximise cache locality

  constexpr auto xu  = PrFTInfo::xZonesUpper;
  constexpr auto uvu = PrFTInfo::uvZonesUpper;

  constexpr auto xd  = PrFTInfo::xZonesLower;
  constexpr auto uvd = PrFTInfo::uvZonesLower;

  constexpr auto hitzones = std::array<int, 24>{
      xd[0], xd[1], uvd[0], uvd[1], xd[2], xd[3], uvd[2], uvd[3], xd[4], xd[5], uvd[4], uvd[5], /*NOW UPPER*/
      xu[0], xu[1], uvu[0], uvu[1], xu[2], xu[3], uvu[2], uvu[3], xu[4], xu[5], uvu[4], uvu[5]};

  size  = 0;
  int j = 0;
  for ( auto i : hitzones ) {
    hitvec.emplace_back( std::numeric_limits<float>::lowest() );
    IDvec.emplace_back( 0 );

    for ( int quarter = 0; quarter < 2; quarter++ ) {
      int iQuarter = i * 2 + quarter;
      for ( auto const& clus : clusters.range( iQuarter ) ) {
        LHCb::FTChannelID id    = clus.channelID();
        auto              index = id.uniqueMat();
        float             dxdy  = m_mats_dxdy[index];

        float uFromChannel =
            m_mats_uBegin[index] + ( 2 * id.channel() + 1 + clus.fractionBit() ) * m_mats_halfChannelPitch[index];
        if ( id.die() ) uFromChannel += m_mats_dieGap[index];
        uFromChannel += id.sipm() * m_mats_sipmPitch[index];

        auto endPoint = m_mats_mirrorPoint[index] + m_mats_ddx[index] * uFromChannel;

        float x0 = endPoint.x() - dxdy * endPoint.y();

        hitvec.emplace_back( x0 );
        IDvec.emplace_back( LHCb::LHCbID( id ).lhcbID() );
      }
    }

    hitvec.emplace_back( std::numeric_limits<float>::max() );
    IDvec.emplace_back( 0 );

    int* offsets = &tmp.startS1L3Down;
    offsets[j++] = hitvec.size();
  }

  return tmp;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode SciFiTrackForwardingStoreHit::initialize() {
  // parent initialization
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // Load detector and GeometryBuild is SUPER-time consuming, so this is stored
  // as a condition in the detector store and updated only when the geometry changes
  // This is possible thanks to the UpdateManager service that allows to register
  // a condition and the method to be called when it changes
  // TODO move this to Condition Handles once these ones they are available

  // register a derived condition object for the zone cache
  detSvc()->registerObject( PrFTInfo::FTCondLocation, new DataObject() ).ignore();
  m_zoneHandler = new PrFTZoneHandler();
  detSvc()->registerObject( PrFTInfo::FTZonesLocation, m_zoneHandler ).ignore();
  // make sure the detector element is updated before the algorithm is executed
  registerCondition( DeFTDetectorLocation::Default, m_ftDet, &SciFiTrackForwardingStoreHit::buildGeometry );
  // make sure the derived condition is updated when the conditions of this algorithm are
  updMgrSvc()->registerCondition( m_zoneHandler, this );
  // This is needed to work around a missing update of the pointer when loading the detector element
  // This is a bug a priori specific to detector elements
  updMgrSvc()->update( m_zoneHandler ).ignore();

  // TODO: this should be ~80 micron; get this from a tool
  std::array<float, 9> clusRes = {0.05f, 0.08f, 0.11f, 0.14f, 0.17f, 0.20f, 0.23f, 0.26f, 0.29f};
  std::transform( clusRes.begin(), clusRes.end(), m_invClusResolution.begin(), []( float c ) { return 1.f / c; } );

  return StatusCode::SUCCESS;
}

//=============================================================================
// buildGeometry
//=============================================================================
StatusCode SciFiTrackForwardingStoreHit::buildGeometry() {
  info() << "FtDEt = " << m_ftDet << endmsg;
  if ( m_ftDet->version() < 61 ) {
    error() << "This version requires FTDet v6.1 or higher" << endmsg;
    return StatusCode::FAILURE;
  }
  for ( auto station : m_ftDet->stations() ) {
    for ( auto layer : station->layers() ) {
      int id = 4 * ( station->stationID() - 1 ) + layer->layerID();

      DetectorSegment seg( 0, layer->globalZ(), layer->dxdy(), layer->dzdy(), 0., 0. );
      float           xmax = 0.5f * layer->sizeX();
      float           ymax = 0.5f * layer->sizeY();

      // The setGeometry defines the z at y=0, the dxDy and the dzDy, as well as the isX properties of the zone.
      // This is important, since these are used in the following.
      // They are set once for each zone in this method.
      m_zoneHandler->MakeZone( 2 * id + 1, seg, -xmax, xmax, -25.f, ymax ); // Small overlap (25 mm) for stereo layers
      m_zoneHandler->MakeZone( 2 * id, seg, -xmax, xmax, -ymax, 25.f );     // Small overlap (25 mm) for stereo layers

      for ( auto quarter : layer->quarters() ) {
        for ( auto module : quarter->modules() ) {
          for ( auto mat : module->mats() ) {
            auto index                     = mat->elementID().uniqueMat();
            m_mats_dxdy[index]             = mat->dxdy();
            m_mats_dzdy[index]             = mat->dzdy();
            m_mats_globaldy[index]         = mat->globaldy();
            m_mats_uBegin[index]           = mat->uBegin();
            m_mats_halfChannelPitch[index] = mat->halfChannelPitch();
            m_mats_dieGap[index]           = mat->dieGap();
            m_mats_sipmPitch[index]        = mat->sipmPitch();
            m_mats_mirrorPoint[index]      = mat->mirrorPoint();
            m_mats_ddx[index]              = mat->ddx();
          }
        }
      }

      //----> Debug zones
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Layer " << id << " z " << m_zoneHandler->zone( 2 * id ).z() << " angle "
                << m_zoneHandler->zone( 2 * id ).dxDy() << endmsg;
      }
      //----> Debug zones
    }
  }

  return StatusCode::SUCCESS;
}

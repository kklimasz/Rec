/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "PrKernel/PrHit.h"
#include <vector>

struct SciFiTrackForwardingHits {
  std::vector<float>        hits{};
  std::vector<unsigned>     IDs{};
  std::vector<PrHit const*> fullhits{}; // TODO: remove

  // Station 1 down
  int startS1L3Down{0};
  int startS1L1Down{0};
  int startS1L2Down{0};
  // Station 2 down
  int startS2L0Down{0};
  int startS2L3Down{0};
  int startS2L1Down{0};
  int startS2L2Down{0};
  // Station 3 down
  int startS3L0Down{0};
  int startS3L3Down{0};
  int startS3L1Down{0};
  int startS3L2Down{0};

  // Station 2 up
  int startS1L0Up{0};
  int startS1L3Up{0};
  int startS1L1Up{0};
  int startS1L2Up{0};
  // Station 2 up
  int startS2L0Up{0};
  int startS2L3Up{0};
  int startS2L1Up{0};
  int startS2L2Up{0};
  // Station 3 up
  int startS3L0Up{0};
  int startS3L3Up{0};
  int startS3L1Up{0};
  int startS3L2Up{0};

  int pad{0};
};

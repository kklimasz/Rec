/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTInfo.h"
#include "SciFiTrackForwardingHits.h"
#include <boost/container/small_vector.hpp>
#include <limits>

/** @class SciFiTrackForwarding_Input_Converter SciFiTrackForwarding_Input_Converter.cpp
 *
 *  \brief Transforms hits in a PrFTHitHandler into the input format needed by the SciFiTrackForwarding
 *
 *
 */

class SciFiTrackForwarding_Input_Converter
    : public Gaudi::Functional::Transformer<SciFiTrackForwardingHits( PrFTHitHandler<PrHit> const& )> {
public:
  SciFiTrackForwarding_Input_Converter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"HitsLocation", PrFTInfo::FTHitsLocation},
                     KeyValue{"Output", "Rec/SciFiTrackForwarding/Hits"} ) {}
  SciFiTrackForwardingHits operator()( PrFTHitHandler<PrHit> const& ) const override;

private:
};

DECLARE_COMPONENT( SciFiTrackForwarding_Input_Converter )

SciFiTrackForwardingHits SciFiTrackForwarding_Input_Converter::
                         operator()( PrFTHitHandler<PrHit> const& hithandler ) const {

  int                      size = 0;
  SciFiTrackForwardingHits tmp{};

  // I want my hit zones in the order bottom zones for layer 0, 3, 1, 2 top 0, 3, 1, 2
  // This is to hopefully maximise cache locality

  constexpr auto xu  = PrFTInfo::xZonesUpper;
  constexpr auto uvu = PrFTInfo::uvZonesUpper;

  constexpr auto xd  = PrFTInfo::xZonesLower;
  constexpr auto uvd = PrFTInfo::uvZonesLower;

  constexpr auto hitzones = std::array<int, 24>{
      xd[0], xd[1], uvd[0], uvd[1], xd[2], xd[3], uvd[2], uvd[3], xd[4], xd[5], uvd[4], uvd[5], /*NOW UPPER*/
      xu[0], xu[1], uvu[0], uvu[1], xu[2], xu[3], uvu[2], uvu[3], xu[4], xu[5], uvu[4], uvu[5]};

  for ( auto i : hitzones ) {
    size += hithandler.hits( i ).size() + 2;
    // Station 1 down
    if ( i == xd[0] ) tmp.startS1L3Down = size;
    if ( i == xd[1] ) tmp.startS1L1Down = size;
    if ( i == uvd[0] ) tmp.startS1L2Down = size;
    // Station 2 down
    if ( i == uvd[1] ) tmp.startS2L0Down = size;
    if ( i == xd[2] ) tmp.startS2L3Down = size;
    if ( i == xd[3] ) tmp.startS2L1Down = size;
    if ( i == uvd[2] ) tmp.startS2L2Down = size;
    // Station 3 down
    if ( i == uvd[3] ) tmp.startS3L0Down = size;
    if ( i == xd[4] ) tmp.startS3L3Down = size;
    if ( i == xd[5] ) tmp.startS3L1Down = size;
    if ( i == uvd[4] ) tmp.startS3L2Down = size;

    // Station 2 up
    if ( i == uvd[5] ) tmp.startS1L0Up = size;
    if ( i == xu[0] ) tmp.startS1L3Up = size;
    if ( i == xu[1] ) tmp.startS1L1Up = size;
    if ( i == uvu[0] ) tmp.startS1L2Up = size;
    // Station 2 up
    if ( i == uvu[1] ) tmp.startS2L0Up = size;
    if ( i == xu[2] ) tmp.startS2L3Up = size;
    if ( i == xu[3] ) tmp.startS2L1Up = size;
    if ( i == uvu[2] ) tmp.startS2L2Up = size;
    // Station 3 up
    if ( i == uvu[3] ) tmp.startS3L0Up = size;
    if ( i == xu[4] ) tmp.startS3L3Up = size;
    if ( i == xu[5] ) tmp.startS3L1Up = size;
    if ( i == uvu[4] ) tmp.startS3L2Up = size;
  }

  auto& hitvec     = tmp.hits;
  auto& IDvec      = tmp.IDs;
  auto& fullhitvec = tmp.fullhits;

  hitvec.reserve( size );
  IDvec.reserve( size );
  fullhitvec.reserve( size );

  for ( auto i : hitzones ) {
    hitvec.emplace_back( std::numeric_limits<float>::lowest() );
    IDvec.emplace_back( 0 );
    fullhitvec.emplace_back( nullptr );
    for ( auto const& hit : hithandler.hits( i ) ) {
      hitvec.emplace_back( hit.x() );
      IDvec.emplace_back( hit.id().lhcbID() );
      fullhitvec.emplace_back( &hit );
    }
    hitvec.emplace_back( std::numeric_limits<float>::max() );
    IDvec.emplace_back( 0 );
    fullhitvec.emplace_back( nullptr );
  }

  // std::cout << "==================DEBUG=========================\n";
  // std::cout << "==================DOWN=========================\n";

  // std::cout << "==================S1L0=========================\n";

  // size_t idx = 0;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L3Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L1Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L2Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS2L0Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L0=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L3Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L1Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L2Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L0Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L0=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L3Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L1Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS3L2Down;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L0Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "===========================================\n";
  // std::cout << "==================UP=========================\n";
  // std::cout << "===========================================\n";

  // std::cout << "==================S1L0=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L3Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L1Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS1L2Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S1L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS2L0Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L0=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L3Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L1Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS2L2Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S2L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L0Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L0=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L3Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L3=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;
  // idx = tmp.startS3L1Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L1=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx = tmp.startS3L2Up;
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  // std::cout << "==================S3L2=========================\n";
  // std::cout << "PAD: " << hitvec[idx] << " hit: " << hitvec[idx+1] << std::endl;
  // std::cout << "PAD: " << IDvec[idx] << " hit: " << LHCb::LHCbID(IDvec[idx+1]).ftID() << std::endl;

  // idx=hitvec.size();
  // std::cout << "IDx: " << idx << std::endl;
  // std::cout << "PAD: " << hitvec[idx-1] << " hit: " << hitvec[idx-2] << std::endl;
  // std::cout << "PAD: " << IDvec[idx-1] << " hit: " << LHCb::LHCbID(IDvec[idx-2]).ftID() << std::endl;

  return tmp;
}

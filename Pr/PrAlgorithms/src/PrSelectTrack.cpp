/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track_v2.h"
#include "GaudiAlg/Transformer.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi_v2/ITrackFunctorFactory.h"
#include "LoKi_v2/TrackTypes.h"
#include "boost/algorithm/string/join.hpp"
#include "boost/format.hpp"
#include <vector>

namespace Pr {
  class SelectTracks final : public Gaudi::Functional::Transformer<std::vector<LoKi::Pr::TrackType>(
                                 std::vector<LoKi::Pr::TrackType> const& )> {
  public:
    SelectTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"Input", ""}, KeyValue{"Output", ""} ) {}
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nAllTracks{this, "All Tracks"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nSelectedTracks{this, "Selected Tracks"};

    std::vector<LoKi::Pr::TrackType> operator()( std::vector<LoKi::Pr::TrackType> const& in ) const override {
      m_nAllTracks += in.size();
      auto selection = std::vector<LoKi::Pr::TrackType>{};
      for ( auto const& tr : in ) {
        if ( m_predicate( &tr ) ) { selection.emplace_back( tr ); }
      }
      m_nSelectedTracks += selection.size();
      return selection;
    }

    StatusCode initialize() override {
      auto sc = Transformer::initialize();
      decode();
      return sc;
    }

  private:
    void decode();

    LoKi::Pr::TrackTypes::TrCut m_predicate = LoKi::BasicFunctors<LoKi::Pr::TrackType const*>::BooleanConstant( false );
    //        BinomialCounter counterPass;

    // ToolHandle<LoKi::Pr::ITrackFunctorFactory>    m_factory  { this,
    // "Factory","LoKi::Hybrid::Pr::TrackFunctorFactory/TrackFunctorFactory:PUBLIC" };
    ToolHandle<LoKi::Pr::ITrackFunctorFactory> m_factory{this, "Factory",
                                                         "LoKi::Hybrid::Pr::TrackFunctorFactory/TrackFunctorFactory"};

    bool m_preambulo_updated = false;
    bool m_code_updated      = false;

    Gaudi::Property<std::vector<std::string>> m_preambulo{this, "Preambulo", {}, [this]( auto& ) {
                                                            m_preambulo_updated = true;
                                                            if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED )
                                                              return;
                                                            if ( !this->m_code_updated ) return;
                                                            this->decode();
                                                          }};
    Gaudi::Property<std::string>              m_code{this, "Code", {}, [this]( auto& ) {
                                          this->m_code_updated = true;
                                          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                          if ( !this->m_preambulo_updated ) return;
                                          this->decode();
                                        }};
  };

  void SelectTracks::decode() {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "decoding " << m_code.value() << endmsg;
    m_factory.retrieve();
    StatusCode sc = m_factory->get( m_code.value(), m_predicate, boost::algorithm::join( m_preambulo.value(), "\n" ) );
    if ( sc.isFailure() ) throw GaudiException{"Failure to decode", "SelectTracks", StatusCode::FAILURE};
    m_factory.release();
    m_code_updated      = false;
    m_preambulo_updated = false;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "decoded " << m_code.value() << endmsg;
  }

  DECLARE_COMPONENT( SelectTracks )
} // namespace Pr

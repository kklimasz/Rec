/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

// Include files
// Range v3
#include <range/v3/all.hpp>

#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Math/CholeskyDecomp.h"

// local
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "PrHybridSeeding.h"
#include "PrLineFitterY.h"
#include "PrPlaneHybridCounter.h"

// ASSUME statement
#include "Kernel/STLExtensions.h"

// New Hit maker and handler
//#include "SciFiTrackForwarding/SciFiTrackForwardingHits.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrHybridSeeding
//
// @author Renato Quagliani (rquaglia@cern.ch)
// @date 2015-03-11
//-----------------------------------------------------------------------------

namespace {
  using namespace ranges;

  // Hit handling/ sorting etc...
  struct compX {
    bool operator()( float lv, float rv ) const { return lv < rv; }
    bool operator()( const ModPrHit& lhs, float rv ) const { return ( *this )( lhs.x(), rv ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.x() ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.x(), rhs.x() ); }
    bool operator()( const ModPrHit* lhs, float rv ) const { return ( *this )( lhs->x(), rv ); }
    bool operator()( float lv, const ModPrHit* rhs ) const { return ( *this )( lv, rhs->x() ); }
    bool operator()( const ModPrHit* lhs, const ModPrHit* rhs ) const { return ( *this )( lhs->x(), rhs->x() ); }
  };
  struct compXreverse {
    bool operator()( float lv, float rv ) const { return lv > rv; }
    bool operator()( const ModPrHit& lhs, float rv ) const { return ( *this )( lhs.x(), rv ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.x() ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.x(), rhs.x() ); }
    bool operator()( const ModPrHit* lhs, float rv ) const { return ( *this )( lhs->x(), rv ); }
    bool operator()( float lv, const ModPrHit* rhs ) const { return ( *this )( lv, rhs->x() ); }
    bool operator()( const ModPrHit* lhs, const ModPrHit* rhs ) const { return ( *this )( lhs->x(), rhs->x() ); }
  };
  struct compCoor {
    bool operator()( float lv, float rv ) const { return lv < rv; }
    bool operator()( const ModPrHit& lhs, float rv ) const { return ( *this )( lhs.coord, rv ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.coord ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.coord, rhs.coord ); }
    bool operator()( const ModPrHit* lhs, float rv ) const { return ( *this )( lhs->coord, rv ); }
    bool operator()( float lv, const ModPrHit* rhs ) const { return ( *this )( lv, rhs->coord ); }
    bool operator()( const ModPrHit* lhs, const ModPrHit* rhs ) const { return ( *this )( lhs->coord, rhs->coord ); }
  };

  template <typename Range>
  Range get_until( Range r, float xMax ) {
    return r.first( std::distance( r.begin(), std::upper_bound( r.begin(), r.end(), xMax, compX{} ) ) );
  }
  // search lowerBound from a known position to another known position (linear time)
  HitIter get_lowerBound_lin( HitIter begin, HitIter end, float xMin ) {
    return std::find_if_not( begin, end, [xMin, cmp = compX{}]( const auto& i ) { return cmp( i, xMin ); } );
  }
  // search linearly a lowerBound in low/high range starting from high
  HitIter get_lowerBound_lin_reverse( HitIter low, HitIter high, float xMin ) {
    return std::find_if( std::reverse_iterator{high}, std::reverse_iterator{low},
                         [xMin, cmp = compXreverse{}]( const auto& i ) { return cmp( xMin, i ); } )
        .base();
  }
  // search upperBound from known position to another known position (linear time)
  HitIter get_upperBound_lin( HitIter begin, HitIter end, float xMax ) {
    return std::find_if( begin, end, [xMax, cmp = compX{}]( const auto& i ) { return cmp( xMax, i ); } );
  }

  IterPairs InitializeBB( const unsigned int& ZoneNumber, const PrFTHitHandler<ModPrHit>& FTHitHandler ) noexcept {
    auto r = FTHitHandler.hits( ZoneNumber );
    return {r.begin(), r.end()};
  }

  // Update the bounds (Hiterator Pairs) and old Min => new Min searching linearly around current boundary begin
  void LookAroundMin( IterPairs& bound, float& oMin, const float& nMin, const ModPrHitConstIter& eZone,
                      const ModPrHitConstIter& bZone ) {
    bound.begin = ( nMin < oMin ? get_lowerBound_lin_reverse( bZone, bound.begin, nMin )
                                : get_lowerBound_lin( bound.begin, eZone, nMin ) );
    oMin        = nMin;
  }

  /** @brief It increases the iterators for the loop through the hits in the pool m_myStereo
   */
  void IncreaseIters( StereoIter& itBeg, StereoIter& itEnd, const unsigned int& minUV, const StereoIter& stereoEnd ) {
    ++itBeg;
    itEnd = ( itBeg + minUV <= stereoEnd ? itBeg + minUV : stereoEnd );
  }

  /** @brief Check if the found line in the hough cluster satisfies the selection criteria
   */
  bool LineOK( const float& minChi2Low, const float& minChi2High, const Pr::Hybrid::SeedTrackX& xProje,
               const PrLineFitterY& line, const unsigned int nHits ) {
    return ( xProje.chi2PerDoF() + line.Chi2DoF() < ( nHits > 10 ? minChi2High : minChi2Low ) );
  }
} // namespace

// Clone removal namespace
namespace {

  inline size_t nUsed( SeedTrackHits::const_iterator itBeg, SeedTrackHits::const_iterator itEnd,
                       const PrFTHitHandler<ModPrHit>& hitHandler ) {
    return std::count_if( itBeg, itEnd,
                          [&hitHandler]( const ModPrHit& h ) { return !hitHandler.hit( h.hitIndex ).isValid(); } );
  }

  template <typename Range>
  static bool hasT1T2T3Track( const Range& hits ) noexcept {
    std::bitset<3> T{};
    for ( const auto& hit : hits ) {
      int planeBit = hit.planeCode / 4;
      ASSUME( planeBit < 3 );
      T[planeBit] = true;
      if ( T.all() ) return true;
    }
    return false;
  }

  /** @brief Check if two tracks are passing through T1/T2/T3 at a distance less than "distance".
   *  @param tr1 First track in comparison
   *  @param tr2 Second track in comparison
   *  @param distance Distance to check if two tracks are close one to anohter
   *  @return bool Success, tracks are close enough
   */
  bool CloseEnough( const Pr::Hybrid::AbsSeedTrack& tr1, const Pr::Hybrid::AbsSeedTrack& tr2, const float& distance ) {
    return ( std::abs( tr1.xT1() - tr2.xT1() ) < std::abs( distance ) ||
             std::abs( tr1.xT2() - tr2.xT2() ) < std::abs( distance ) ||
             std::abs( tr1.xT3() - tr2.xT3() ) < std::abs( distance ) );
  }

  /** @brief Given two tracks it checks if they are clones. Clone threshold is defined by the
      amount of shared hits ( clones if nCommon > maxCommon)
  */
  bool areClones( const Pr::Hybrid::SeedTrackX& tr1, const Pr::Hybrid::SeedTrackX& tr2,
                  const unsigned int& maxCommon ) {
    unsigned int nCommon = maxCommon;
    auto         itH1    = tr1.hits().begin();
    auto         itBeg2  = tr2.hits().begin();
    auto         itEnd1  = tr1.hits().end();
    auto         itEnd2  = tr2.hits().end();
    while ( nCommon != 0 && itH1 != itEnd1 ) {
      for ( auto itH2 = itBeg2; itH2 != itEnd2; itH2++ )
        if ( itH1->hit->id() == itH2->hit->id() ) {
          --nCommon;
          break;
        }
      ++itH1;
    }
    return ( nCommon == 0 );
  }

  /** @brief Given two tracks it checks if they are clones. Clone threshold is defined by the
      amount of shared hits ( clones if nCommon > maxCommon)
  */
  bool areClones( const Pr::Hybrid::SeedTrack& tr1, const Pr::Hybrid::SeedTrack& tr2, const unsigned int& maxCommon ) {
    unsigned int nCommon = maxCommon;
    auto         itH1    = tr1.hits().begin();
    auto         itEnd1  = tr1.hits().end();
    auto         itEnd2  = tr2.hits().end();
    while ( nCommon != 0 && itH1 != itEnd1 ) {
      for ( auto itH2 = tr2.hits().begin(); itH2 != itEnd2; itH2++ )
        if ( itH1->hit->id() == itH2->hit->id() ) {
          --nCommon;
          break;
        }
      ++itH1;
    }
    return ( nCommon == 0 );
  }

  // Removes the worst of two tracks
  bool removeWorstTrack( Pr::Hybrid::SeedTrackX& t1, Pr::Hybrid::SeedTrackX& t2 ) {
    bool ret = !Pr::Hybrid::SeedTrackX::LowerBySize( t1, t2 );
    if ( !ret )
      t2.setValid( false );
    else
      t1.setValid( false );
    return ret;
  }

  // Removes the worst of two tracks
  bool removeWorstTrack( Pr::Hybrid::SeedTrack& t1, Pr::Hybrid::SeedTrack& t2 ) {
    bool ret = !Pr::Hybrid::SeedTrack::LowerBySize( t1, t2 );
    if ( !ret )
      t2.setValid( false );
    else
      t1.setValid( false );
    return ret;
  }
  // X-track clone removal
  void removeClonesX( unsigned int part, XCandidates& xCandidates ) {
    //---LoH: it is surely faster to do by a simpler alg
    for ( auto itT1 = xCandidates[part].begin(); itT1 < xCandidates[part].end() - 1; ++itT1 ) {
      if ( !( *itT1 ).valid() ) continue;
      for ( auto itT2 = itT1 + 1; itT2 < xCandidates[part].end(); ++itT2 ) {
        if ( !( *itT2 ).valid() ) continue;
        if ( !CloseEnough( ( *itT1 ), ( *itT2 ), 2.f ) ) continue;
        if ( areClones( ( *itT1 ), ( *itT2 ), PrHybridSeeding::commonXHits[12 - itT1->size() - itT2->size()] ) ) {
          if ( removeWorstTrack( *itT1, *itT2 ) ) break;
        }
      }
    }
  }

  // Track clone removal
  void removeClones( unsigned int part, TrackCandidates& trackCandidates ) {
    for ( auto itT1 = trackCandidates[(int)part].begin(); itT1 < trackCandidates[(int)part].end() - 1; ++itT1 ) {
      if ( !( *itT1 ).valid() ) continue;
      for ( auto itT2 = itT1 + 1; trackCandidates[(int)part].end() != itT2; ++itT2 ) {
        if ( !( *itT2 ).valid() ) continue;
        if ( !CloseEnough( ( *itT1 ), ( *itT2 ), 2.f ) ) continue;
        if ( areClones( ( *itT1 ), ( *itT2 ),
                        PrHybridSeeding::commonHits[12 - std::min( itT1->size(), itT2->size() )] ) ) {
          if ( removeWorstTrack( *itT1, *itT2 ) ) break;
        }
      }
    }
  }

  // Remove tracks to recover
  void removeClonesRecover( unsigned int part, TrackToRecover& trackToRecover, XCandidates& xCandidates ) {
    for ( auto itT1 = trackToRecover[(int)part].begin(); itT1 != trackToRecover[(int)part].end(); ++itT1 ) {
      if ( !( *itT1 ).recovered() ) continue;
      for ( auto itT2 = itT1 + 1; itT2 != trackToRecover[(int)part].end(); ++itT2 ) {
        if ( !( *itT2 ).recovered() ) continue;
        if ( !CloseEnough( ( *itT1 ), ( *itT2 ), 5.f ) ) continue;
        if ( areClones( ( *itT1 ), ( *itT2 ),
                        PrHybridSeeding::commonRecoverHits[6 - std::min( itT1->size(), itT2->size() )] ) ) {
          if ( Pr::Hybrid::SeedTrackX::LowerBySize( *itT1, *itT2 ) )
            itT2->setRecovered( false );
          else {
            itT1->setRecovered( false );
            break;
          }
        }
      }
      if ( ( *itT1 ).recovered() ) { xCandidates[int( part )].push_back( ( *itT1 ) ); }
    }
  }
} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrHybridSeeding )
//=============================================================================
// Standard constructor, initializes variable
//=============================================================================

PrHybridSeeding::PrHybridSeeding( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                   KeyValue{"OutputName", LHCb::TrackLocation::Seed} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrHybridSeeding::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) info() << "==> Initialize" << endmsg;
  m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

  if ( (int)m_nCases > 3 ) {
    error() << "Algorithm does not support more than 3 Cases" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( (int)m_recoMinTotHits < 9 ) {
    error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
    return StatusCode::FAILURE;
  }
  for ( unsigned int i = 0; i < m_nCases; i++ )
    if ( m_minTot[i] < 9 ) {
      error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
      return StatusCode::FAILURE;
    }

  // Zones cache, retrieved from the detector store
  registerCondition<PrHybridSeeding>( PrFTInfo::FTZonesLocation, m_zoneHandler );

  //---LoH: UV zones
  m_dxDy[0]    = m_zoneHandler->zone( T1U ).dxDy(); // u-layers
  m_dxDy[1]    = m_zoneHandler->zone( T1V ).dxDy(); // v-layers
  m_recDxDy[0] = 1.f / m_dxDy[0];
  m_recDxDy[1] = 1.f / m_dxDy[1];
  for ( auto layer : {T1X1, T1X2, T2X1, T2X2, T3X1, T3X2, T1U, T1V, T2U, T2V, T3U, T3V} )
    for ( auto part : {0, 1} ) {
      m_z[layer - part]         = m_zoneHandler->zone( layer - part ).z();
      m_planeCode[layer - part] = m_zoneHandler->zone( layer - part ).planeCode();
    }

  //---LoH: Precalculate y borders
  m_yMins[0][0] = m_yMin;
  m_yMins[0][1] = m_yMin_TrFix;
  m_yMins[1][0] = -(float)m_yMax_TrFix;
  m_yMins[1][1] = -std::abs( m_yMax );

  m_yMaxs[0][0] = std::abs( m_yMax );
  m_yMaxs[0][1] = (float)m_yMax_TrFix;
  m_yMaxs[1][0] = -m_yMin_TrFix;
  m_yMaxs[1][1] = -m_yMin; // positive

  // Create the m_minUV here
  m_minUV[0][0] = m_minUV6[0];
  m_minUV[0][1] = m_minUV5[0];
  m_minUV[0][2] = m_minUV4[0];
  m_minUV[1][0] = m_minUV6[1];
  m_minUV[1][1] = m_minUV5[1];
  m_minUV[1][2] = m_minUV4[1];
  m_minUV[2][0] = m_minUV6[2];
  m_minUV[2][1] = m_minUV5[2];
  m_minUV[2][2] = m_minUV4[2];

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<PrHybridSeeding::Track> PrHybridSeeding::operator()( const PrFTHitHandler<PrHit>& FTHitHandler ) const {
  // Containers
  TrackCandidates trackCandidates; //---LoH: probably could be changed to array(static_vector)
  trackCandidates[0].reserve( maxCandidates );
  trackCandidates[1].reserve( maxCandidates );
  XCandidates    xCandidates;    //---LoH: probably could be changed to static_vector
  TrackToRecover trackToRecover; //---LoH: probably could be changed to static_vector

  //==========================================================
  // Hits are ready to be processed
  //==========================================================
  PrFTHitHandler<ModPrHit> hitHandler = makeHitHandler( FTHitHandler );
  //========================================================
  //------------------MAIN SEQUENCE IS HERE-----------------
  //========================================================
  //----- Loop through lower and upper half
  // Swap them ? externally icase & inner loop part? xCandidates
  for ( unsigned int icase = 0; m_nCases > icase; ++icase ) {
    for ( unsigned int part = 0; 2 > part; ++part ) {
      xCandidates[(int)part].clear(); // x candidates up cleaned every Case!
      xCandidates[(int)part].reserve( maxXCandidates );
      findXProjections( part, icase, hitHandler, xCandidates );
      if ( m_removeClonesX ) { removeClonesX( part, xCandidates ); }
      addStereo( part, icase, hitHandler, trackCandidates, xCandidates, trackToRecover );
      // Flag found Hits at the end of each single case ( exclude the latest one )
      if ( ( icase + 1 < m_nCases ) ) { flagHits( icase, part, trackCandidates, hitHandler ); }
    }
  }
  // Clone removal ( up/down )
  for ( unsigned int part = 0; part < 2; part++ ) { removeClones( part, trackCandidates ); }
  // Recovering step
  if ( m_recover ) {
    xCandidates[0].clear();
    xCandidates[1].clear();
    xCandidates[0].reserve( maxXCandidates );
    xCandidates[1].reserve( maxXCandidates );
    RecoverTrack( hitHandler, trackCandidates, xCandidates, trackToRecover );
  }
  std::vector<Track> result{};
  result.reserve( trackCandidates[0].size() + trackCandidates[1].size() );
  // Convert LHCb tracks
  for ( unsigned int part = 0; part < 2; part++ ) { makeLHCbTracks( result, part, trackCandidates ); }
  return result;
}

//---LoH: Can be put outside of the classx
PrFTHitHandler<ModPrHit> PrHybridSeeding::makeHitHandler( const PrFTHitHandler<PrHit>& FTHitHandler ) const noexcept {
  // Construct hit handler of ModPrHits
  // The track candidates will contain copies of the ModHits, but each will contain their
  // own index in the hit container, which can be used used to flag the original hits.
  std::vector<ModPrHit> hits;
  hits.reserve( FTHitHandler.hits().size() );
  size_t i = 0;
  for ( const auto& hit : FTHitHandler.hits().range() ) {
    hits.emplace_back( &hit, 0, hit.planeCode(), i );
    ++i;
  }
  PrFTHitHandler<ModPrHit> hitHandler( std::move( hits ), FTHitHandler.hits().offsets() );
  return hitHandler;
}

//===========================================================================================================================
//
// EVERYTHING RELATED TO UV-HIT ADDITION, by order of apparition
//

void PrHybridSeeding::addStereo( unsigned int part, unsigned int iCase, const PrFTHitHandler<ModPrHit>& FTHitHandler,
                                 TrackCandidates& trackCandidates, XCandidates& xCandidates,
                                 TrackToRecover& trackToRecover ) const noexcept {
  // Initialise bounds
  BoundariesUV                                       Bounds;
  BoundariesUV                                       borderZones;
  std::array<std::array<std::array<float, 2>, 3>, 2> xMinPrev;
  initializeUVBounds( FTHitHandler, Bounds, borderZones, xMinPrev );

  PrLineFitterY BestLine;
  float         initCoord = std::numeric_limits<float>::max();
  float         ClusterSpread[3][DEPTH_HOUGH];
  StereoHits    myStereo;
  StereoIter    initBeg[3][DEPTH_HOUGH];
  StereoIter    initEnd[3][DEPTH_HOUGH];

  //---LoH: Loop on the xCandidates
  //---LoH: They are not sorted by xProje and their order can vary
  for ( auto&& itT : xCandidates[part] ) {
    if ( !itT.valid() ) continue;
    myStereo.clear();
    CollectUV( part, itT, borderZones, Bounds, xMinPrev, myStereo );
    // Define the min UV hits based on the amount of hits of the track
    // initialize the clusters ( here 9 )
    //---LoH: Is it necessary?
    for ( unsigned int i = 0; i < 3; ++i ) {
      for ( unsigned int j = 0; j < DEPTH_HOUGH; ++j ) {
        ClusterSpread[i][j] = initCoord;
        initBeg[i][j]       = myStereo.end();
        initEnd[i][j]       = myStereo.end();
      }
    }
    HoughClusterSearch( iCase, myStereo, initBeg, initEnd, ClusterSpread );
    // you have a minimal number of total hits to find on track dependent if standard 3 cases or from Recover routine
    BestLine.setXProj( itT );
    //---LoH: 16k times
    bool hasAdded =
        createTracksFromHough( iCase, trackCandidates[(int)part], itT, BestLine, initBeg, initEnd, myStereo );

    if ( !hasAdded && ( m_recover ) ) trackToRecover[(int)part].push_back( itT );
  }
}

void PrHybridSeeding::addStereoReco( unsigned int part, unsigned int iCase,
                                     const PrFTHitHandler<ModPrHit>& FTHitHandler, TrackCandidates& trackCandidates,
                                     XCandidates& xCandidates ) const noexcept {
  //---LoH: reco is used here to switch on the track recovering configurables
  // Initialise bounds
  BoundariesUV                                       Bounds;
  BoundariesUV                                       borderZones;
  std::array<std::array<std::array<float, 2>, 3>, 2> xMinPrev;
  initializeUVBounds( FTHitHandler, Bounds, borderZones, xMinPrev );

  PrLineFitterY BestLine;
  float         initCoord = std::numeric_limits<float>::max();
  float         ClusterSpread[3][DEPTH_HOUGH];
  StereoHits    myStereo;
  StereoIter    initBeg[3][DEPTH_HOUGH];
  StereoIter    initEnd[3][DEPTH_HOUGH];

  //---LoH: Loop on the xCandidates
  //---LoH: They are not sorted by xProje and their order can vary
  for ( auto&& itT : xCandidates[part] ) {
    if ( !itT.valid() ) continue;
    myStereo.clear();
    CollectUV( part, itT, borderZones, Bounds, xMinPrev, myStereo );
    // Define the min UV hits based on the amount of hits of the track
    // initialize the clusters ( here 9 )
    //---LoH: Is it necessary?
    for ( unsigned int i = 0; i < 3; ++i ) {
      for ( unsigned int j = 0; j < DEPTH_HOUGH; ++j ) {
        ClusterSpread[i][j] = initCoord;
        initBeg[i][j]       = myStereo.end();
        initEnd[i][j]       = myStereo.end();
      }
    }
    HoughClusterSearchReco( myStereo, initBeg, initEnd, ClusterSpread );
    // you have a minimal number of total hits to find on track dependent if standard 3 cases or from Recover routine
    BestLine.setXProj( itT );
    //---LoH: 3.4k times
    createTracksFromHoughReco( iCase, trackCandidates[(int)part], itT, BestLine, initBeg, initEnd, myStereo );
  }
}

//---LoH: Can be put outside of class
void PrHybridSeeding::initializeUVBounds( const PrFTHitHandler<ModPrHit>& FTHitHandler, BoundariesUV& Bounds,
                                          BoundariesUV&                                       borderZones,
                                          std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const
    noexcept {
  std::array<unsigned int, PrFTInfo::Numbers::NFTUVLayers> uvLayers = {T1U, T2U, T3U, T1V, T2V, T3V};
  for ( unsigned int iLayer = 0; iLayer < PrFTInfo::Numbers::NFTUVLayers / 2; iLayer++ ) {
    for ( unsigned int iPart = 0; iPart < 2; iPart++ ) {
      // Fill the U bounds
      Bounds[0][iLayer][iPart]      = InitializeBB( uvLayers[iLayer] - iPart, FTHitHandler );
      auto r                        = FTHitHandler.hits( uvLayers[iLayer] - iPart );
      borderZones[0][iLayer][iPart] = {r.begin(), r.end()};
      xMinPrev[0][iLayer][iPart]    = std::numeric_limits<float>::lowest();
      /// Fill the V bounds
      Bounds[1][iLayer][iPart]      = InitializeBB( uvLayers[iLayer + 3] - iPart, FTHitHandler );
      r                             = FTHitHandler.hits( uvLayers[iLayer + 3] - iPart );
      borderZones[1][iLayer][iPart] = {r.begin(), r.end()};
      xMinPrev[1][iLayer][iPart]    = std::numeric_limits<float>::lowest();
    }
  }
}

//---LoH: Can be put outside of class if takes the seeding as argument.
void PrHybridSeeding::CollectUV( unsigned int part, const Pr::Hybrid::SeedTrackX& xProje,
                                 const BoundariesUV& borderZones, BoundariesUV& Bounds,
                                 std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev,
                                 StereoHits&                                         myStereo ) const noexcept {
  //---LoH: called 19k times
  // u-layers
  for ( const unsigned int& layer : {T1U, T2U, T3U} )
    CollectLayerU( part, layer, xProje, borderZones[0], Bounds[0], xMinPrev[0], myStereo );
  // v-layers
  for ( const unsigned int& layer : {T1V, T2V, T3V} )
    CollectLayerV( part, layer, xProje, borderZones[1], Bounds[1], xMinPrev[1], myStereo );
  std::sort( myStereo.begin(), myStereo.end() ); // Necessary for the Hough-cluster search
}

//---LoH: Can be templated
void PrHybridSeeding::CollectLayerU( unsigned int part, unsigned int layer, const Pr::Hybrid::SeedTrackX& xProje,
                                     const BoundaryUV& borderZones, BoundaryUV& Bounds,
                                     std::array<std::array<float, 2>, 3>& xMinPrev, StereoHits& myStereo ) const
    noexcept {
  float        partSign = ( part == 0 ) ? 1.f : -1.f;
  float        dxDy     = m_dxDy[0]; // 0 if u, 1 if v
  float        recDxDy  = m_recDxDy[0];
  float        zPlane   = m_z[layer];
  float        xPred    = xProje.x( zPlane );
  float        factor   = recDxDy * partSign / zPlane;
  unsigned int iLayer   = layer / 8;
  // Part 0
  float yMin = m_yMins[part][0];
  float yMax = m_yMaxs[part][0];
  float xMin = xPred - yMax * dxDy;
  float xMax = xPred - yMin * dxDy;
  LookAroundMin( Bounds[iLayer][0], xMinPrev[iLayer][0], xMin, borderZones[iLayer][0].end,
                 borderZones[iLayer][0].begin );
  Bounds[iLayer][0].end = get_upperBound_lin( Bounds[iLayer][0].begin, borderZones[iLayer][0].end, xMax );
  for ( auto itH = Bounds[iLayer][0].end - 1; itH >= Bounds[iLayer][0].begin; itH-- )
    AddUVHit( itH, xPred, factor, myStereo );

  // Part 1
  yMin = m_yMins[part][1];
  yMax = m_yMaxs[part][1];
  xMin = xPred - yMax * dxDy;
  xMax = xPred - yMin * dxDy;
  LookAroundMin( Bounds[iLayer][1], xMinPrev[iLayer][1], xMin, borderZones[iLayer][1].end,
                 borderZones[iLayer][1].begin );
  for ( Bounds[iLayer][1].end = Bounds[iLayer][1].begin; Bounds[iLayer][1].end != borderZones[iLayer][1].end;
        Bounds[iLayer][1].end++ ) {
    if ( Bounds[iLayer][1].end->hit->x() > xMax ) break;
    AddUVHit( Bounds[iLayer][1].end, xPred, factor, myStereo );
  }
}

//---LoH: Can be templated
void PrHybridSeeding::CollectLayerV( unsigned int part, unsigned int layer, const Pr::Hybrid::SeedTrackX& xProje,
                                     const BoundaryUV& borderZones, BoundaryUV& Bounds,
                                     std::array<std::array<float, 2>, 3>& xMinPrev, StereoHits& myStereo ) const
    noexcept {
  float        partSign = ( part == 0 ) ? 1.f : -1.f;
  float        dxDy     = m_dxDy[1]; // 0 if u, 1 if v
  float        recDxDy  = m_recDxDy[1];
  float        zPlane   = m_z[layer];
  float        factor   = recDxDy * partSign / zPlane;
  float        xPred    = xProje.x( zPlane );
  unsigned int iLayer   = layer / 8;
  // Part 0
  float yMin = m_yMins[part][0];
  float yMax = m_yMaxs[part][0];
  float xMin = xPred - yMin * dxDy;
  float xMax = xPred - yMax * dxDy;
  LookAroundMin( Bounds[iLayer][0], xMinPrev[iLayer][0], xMin, borderZones[iLayer][0].end,
                 borderZones[iLayer][0].begin );
  Bounds[iLayer][0].end = get_upperBound_lin( Bounds[iLayer][0].begin, borderZones[iLayer][0].end, xMax );
  for ( auto itH = Bounds[iLayer][0].begin; itH != Bounds[iLayer][0].end; itH++ )
    AddUVHit( itH, xPred, factor, myStereo );
  // Part 1
  yMin = m_yMins[part][1];
  yMax = m_yMaxs[part][1];
  xMin = xPred - yMin * dxDy;
  xMax = xPred - yMax * dxDy;
  LookAroundMin( Bounds[iLayer][1], xMinPrev[iLayer][1], xMin, borderZones[iLayer][1].end,
                 borderZones[iLayer][1].begin );
  for ( Bounds[iLayer][1].end = Bounds[iLayer][1].begin; Bounds[iLayer][1].end != borderZones[iLayer][1].end;
        Bounds[iLayer][1].end++ ) {
    if ( Bounds[iLayer][1].end->hit->x() > xMax ) break;
    AddUVHit( Bounds[iLayer][1].end, xPred, factor, myStereo );
  }
}

//---LoH: Can be put outside of the class.
void PrHybridSeeding::AddUVHit( const ModPrHitConstIter& it, const float& xPred, const float& factor,
                                StereoHits& myStereo ) const noexcept {
  if ( !it->isValid() ) return;
  const PrHit* hit = it->hit;
  float        y   = ( xPred - hit->x() ) * factor;
  myStereo.push_back( *it );
  myStereo.back().coord = y;
}

// Deprecated for now
bool PrHybridSeeding::doTriangleSecondOrderFix( const unsigned int part, const unsigned int kk, float& yMin,
                                                float& yMax, const PrHit* hit ) const noexcept {
  if ( part == 0 ) {
    // Up track
    if ( kk % 2 != 0 ) {
      // Up track Up modules
      if ( hit->yMin() < 0.f ) { // leaking area to y negative in up modules
        yMin = m_yMin;
        yMax = m_yMax; //[-1.0, 2500]
      } else {         // y positive but cut away
        yMin = -2.f + std::fabs( hit->yMin() );
        yMax = m_yMax;
      }
    } else { // kk%2 ==0
      // Up track Down modules
      if ( hit->yMax() < 0.f ) {
        return true;
      } else {
        yMin = -1.f;
        yMax = 2.f + std::fabs( hit->yMax() );
      }
    }
  } else {
    // Down track
    if ( kk % 2 == 0 ) {
      // Down track Down modules
      if ( hit->yMax() > 0.f ) { // leaking area to y positive in down module
        yMin = -m_yMax;
        yMax = -m_yMin; //[-2500, 1.0]
      } else {          // y negative but cut away
        yMin = -std::fabs( m_yMax );
        yMax = +2.f - std::fabs( hit->yMax() ); // [ -2500, 2.0 ]
      }
    } else { // kk%2 ==1
      // Down track Up modules
      if ( hit->yMin() < 0.f ) {
        yMin = -2.f - std::fabs( hit->yMin() );
        yMax = +1.f;
      } else {
        return true;
      }
    }
  }
  return false;
}

// Deprecated for now
bool PrHybridSeeding::isInHole( float x, float y ) const noexcept {
  return ( std::abs( y ) < m_holeWidthY && std::abs( x ) < m_holeWidthX );
}

void PrHybridSeeding::HoughClusterSearch( unsigned int iCase, StereoHits& myStereo, StereoIter initBeg[3][DEPTH_HOUGH],
                                          StereoIter initEnd[3][DEPTH_HOUGH],
                                          float      ClusterSpread[3][DEPTH_HOUGH] ) const noexcept {
  // Matrix 3xDEPTHHOUGH with 2iterators to myStereo (begin/end)
  //
  // matrix is filled advancing of steps of size 4 in myStereo when good cluster if found
  //
  //     | 6Hits(spread1) 6Hits(spread2) 6Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
  // M = | 5Hits(spread1) 5Hits(spread2) 5Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
  //     | 4Hits(spread1) 4Hits(spread2) 4Hits(spread3) ---- 6Hits(spreadHOUGHDEPTH) |
  //
  //---- spread1< spread2 < spread3 ....< spreadHOUGHDEPTH
  auto itBeg = myStereo.cbegin();
  auto itEnd = itBeg + 4 <= myStereo.cend() ? itBeg + 4 : myStereo.cend();
  for ( ; itEnd - itBeg >= 4; IncreaseIters( itBeg, itEnd, 4, myStereo.end() ) ) {
    float begVal = ( *itBeg ).coord;
    float tolTy  = (float)m_tolTyOffset[iCase] + m_tolTySlope[iCase] * begVal;
    // always steps of 4 as minimum!
    for ( int i = 0; i < 3; ++i ) {
      // 3 because you have clusters with 4/5/6 hits (more  u/v layers ? CHANGE IT!)
      auto itLast = itEnd + 2 - i <= myStereo.end() ? itEnd + 2 - i : myStereo.end();
      // spread6 computed first
      float spread = ( *( itLast - 1 ) ).coord - begVal;
      if ( spread < tolTy ) {
        // cluster is fine check
        unsigned int j;
        // find the cluster worse than the current cluster
        for ( j = 0; j < DEPTH_HOUGH; ++j ) {
          if ( spread < ClusterSpread[i][j] ) break;
        }
        if ( j == DEPTH_HOUGH ) continue;
        // Shift clusters after the found position
        for ( unsigned int k = DEPTH_HOUGH - 1; k > j; --k ) {
          initBeg[i][k]       = initBeg[i][k - 1];
          ClusterSpread[i][k] = ClusterSpread[i][k - 1];
          initEnd[i][k]       = initEnd[i][k - 1];
        }
        // insert current cluster at found position
        initBeg[i][j]       = itBeg;
        ClusterSpread[i][j] = spread;
        initEnd[i][j]       = itLast;
        break;
      }
    }
  }
}

void PrHybridSeeding::HoughClusterSearchReco( StereoHits& myStereo, StereoIter initBeg[3][DEPTH_HOUGH],
                                              StereoIter initEnd[3][DEPTH_HOUGH],
                                              float      ClusterSpread[3][DEPTH_HOUGH] ) const noexcept {
  auto  itBeg = myStereo.cbegin();
  auto  itEnd = itBeg + 4 <= myStereo.cend() ? itBeg + 4 : myStereo.cend();
  float tolTy = (float)m_recoTolTy;
  for ( ; itEnd - itBeg >= 4; IncreaseIters( itBeg, itEnd, 4, myStereo.end() ) ) {
    float begVal = ( *itBeg ).coord;
    // always steps of 4 as minimum!
    for ( int i = 0; i < 3; ++i ) {
      // 3 because you have clusters with 4/5/6 hits (more  u/v layers ? CHANGE IT!)
      auto itLast = itEnd + 2 - i <= myStereo.end() ? itEnd + 2 - i : myStereo.end();
      // spread6 computed first
      float spread = ( *( itLast - 1 ) ).coord - begVal;
      if ( spread < tolTy ) {
        // cluster is fine check
        unsigned int j;
        // find the cluster worse than the current cluster
        for ( j = 0; j < DEPTH_HOUGH; ++j ) {
          if ( spread < ClusterSpread[i][j] ) break;
        }
        if ( j == DEPTH_HOUGH ) continue;
        // Shift clusters after the found position
        for ( unsigned int k = DEPTH_HOUGH - 1; k > j; --k ) {
          initBeg[i][k]       = initBeg[i][k - 1];
          ClusterSpread[i][k] = ClusterSpread[i][k - 1];
          initEnd[i][k]       = initEnd[i][k - 1];
        }
        // insert current cluster at found position
        initBeg[i][j]       = itBeg;
        ClusterSpread[i][j] = spread;
        initEnd[i][j]       = itLast;
        break;
      }
    }
  }
}

bool PrHybridSeeding::createTracksFromHough( const unsigned int&                 iCase,
                                             std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                             const Pr::Hybrid::SeedTrackX& xCandidate, PrLineFitterY& BestLine,
                                             StereoIter initBeg[3][DEPTH_HOUGH], StereoIter initEnd[3][DEPTH_HOUGH],
                                             const StereoHits& myStereo ) const noexcept {
  float        minChi2LineLow  = m_Chi2LowLine[iCase];
  float        minChi2LineHigh = m_Chi2HighLine[iCase];
  unsigned int maxNClusters    = m_maxNClusters[iCase];
  unsigned int nUVtarget       = 0;
  float        maxChi2DoF      = m_maxChi2PerDoF[iCase];
  unsigned int minTot          = m_minTot[iCase];
  StereoHits   uvHits;
  unsigned int nXinit = xCandidate.size();
  unsigned int minUV  = m_minUV[iCase][6 - nXinit];
  unsigned int nCandidates( 0 );
  bool         hasMaxCands( false );
  for ( unsigned int i = 0; i < 3; ++i ) {
    for ( unsigned int j = 0; j < DEPTH_HOUGH; ++j ) {
      if ( hasMaxCands ) break;
      // you are at the cluster Number (1+j) +(3*i)
      // By construction the first clusters you process are the ones with smaller spread
      auto itBeg = initBeg[i][j];
      auto itEnd = initEnd[i][j];
      if ( itBeg == myStereo.end() ) { break; }

      unsigned int nLay = 0;
      ExtendCluster( itBeg, itEnd, myStereo.end(), iCase, nLay );
      // nUVtarget are the minimal UV layers to find since track quality is based on nHits
      if ( nXinit + nLay < minTot ) continue;
      // if you already have less than minTot layers, look to next "extended" cluster"
      if ( nLay < minUV || ( nLay <= nUVtarget ) ) continue; //<= if at the end you have only >= nUVtarget.
      // You increased the clusters and check to have at least minUV hits and number
      // of fired layers in current clusters can kill current candidates produced
      nCandidates++;
      if ( nCandidates == maxNClusters ) hasMaxCands = true; // will finish current loop, but then break
      // Set the plane counter
      PrPlaneHybridCounter plCount;
      plCount.set( itBeg, itEnd );
      if ( !plCount.isOK() ) { continue; } //---LoH: having a hit in all three layers
      Pr::Hybrid::SeedTrack trackCand;
      bool                  fit   = false;
      unsigned int          nInit = nXinit + itEnd - itBeg;
      // All layers fired only once.
      if ( plCount.nbSingle() == nLay ) {
        fit = BestLine.fit( itBeg, itEnd );
        fit &= LineOK( minChi2LineLow, minChi2LineHigh, xCandidate, BestLine, nInit );
        if ( !fit ) continue;
        trackCand.setFromXCand( xCandidate );
        for ( auto hit = itBeg; hit < itEnd; hit++ ) trackCand.hits().push_back( *hit );
      } else {
        // More than one hit per layer in the cluster
        // Remove outliers from multiple hit in layers is forced here
        uvHits.clear();
        for ( auto itH = itBeg; itEnd != itH; ++itH ) { uvHits.push_back( *itH ); }
        auto itBeg2 = uvHits.begin();
        auto itEnd2 = uvHits.end();
        fit         = BestLine.fit2( itBeg2, itEnd2, plCount );
        //---LoH: could probably be done in one pass with minimal loss
        while ( itEnd2 - itBeg2 > nLay ) {
          auto  worstMultiple = itEnd2;
          float worstchi      = std::numeric_limits<float>::min();
          float chi_hit;
          for ( auto line_hit = itBeg2; line_hit != itEnd2; ++line_hit ) {
            if ( plCount.nbInPlane( line_hit->planeCode ) == 1 ) continue;
            chi_hit = BestLine.chi2hit( *line_hit );
            if ( chi_hit > worstchi ) {
              worstchi      = chi_hit;
              worstMultiple = line_hit;
            }
          }
          // it will just change the m_planeCode[int] not the whole internal hit counting ! )
          plCount.removeHitInPlane( worstMultiple->planeCode );
          // Avoid relocations by swapping the worst hit to the last hit and reducing the end by one.
          --itEnd2;
          std::iter_swap( itEnd2, worstMultiple );
          fit = BestLine.fit2( itBeg2, itEnd2, plCount );
          nInit--;
          if ( nInit == minTot ) break; // Can't remove more.
        }
        // fit status is the Success of the fit ( no requirements on Chi2 )
        fit &= LineOK( minChi2LineLow, minChi2LineHigh, xCandidate, BestLine, nInit );
        if ( !fit ) continue;
        trackCand.setFromXCand( xCandidate );
        for ( auto hit = itBeg2; hit < itEnd2; hit++ ) trackCand.hits().push_back( *hit );
      }
      trackCand.setYParam( BestLine.ay(), BestLine.by() );
      //---LoH: performs the simultaneous fit
      trackCand.setnXnY( nXinit, plCount.nbDifferent() );

      bool ok = fitSimultaneouslyXY( trackCand, iCase );
      if ( !ok && ( trackCand.size() > minTot ) )
        //---LoH: right now, ONLY DOES SOMETHING because of removeWorst
        //---LoH: and it somehow improves the situation timing-wise
        std::sort( trackCand.hits().begin(), trackCand.hits().end(), compLHCbID() );

      while ( !ok && trackCand.size() > minTot ) {
        ok = removeWorstAndRefit( trackCand, iCase ); //---LoH: called 2000 times
      }
      if ( !ok ) continue;
      if ( trackCand.chi2PerDoF() >= maxChi2DoF ) continue;
      unsigned int nUVfound = trackCand.size() - nXinit;
      if ( nUVfound < nUVtarget ) continue;
      // Building the track
      if ( nUVfound != nUVtarget ) {
        if ( ( ( trackCand.size() < 11 && ( std::fabs( trackCand.y0() ) < m_maxY0Low[iCase] &&
                                            std::fabs( trackCand.yRef() ) < m_maxYZrefLow[iCase] ) ) ||
               ( trackCand.size() > 10 ) ) ) {

          if ( nUVtarget == 0 )
            trackCandidates.push_back( trackCand );
          else
            trackCandidates[trackCandidates.size() - 1] = trackCand;
          nUVtarget = nUVfound;
        }
      } else
        continue;
    }
    if ( nUVtarget == 6 ) break;
  }
  return ( nUVtarget != 0 );
}
void PrHybridSeeding::createTracksFromHoughReco( const unsigned int&                 iCase,
                                                 std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                                 const Pr::Hybrid::SeedTrackX& xCandidate, PrLineFitterY& BestLine,
                                                 StereoIter initBeg[3][DEPTH_HOUGH], StereoIter initEnd[3][DEPTH_HOUGH],
                                                 const StereoHits& myStereo ) const noexcept {
  float        minChi2LineLow  = m_recoLineLow;
  float        minChi2LineHigh = m_recoLineHigh;
  unsigned int maxNClusters    = m_recoNCluster; // max N of clusters to process changed when in reco case.
  unsigned int nUVtarget       = 0;
  float        maxChi2DoF      = m_recoFinalChi2;
  unsigned int minTot          = m_recoMinTotHits;
  StereoHits   uvHits;
  unsigned int nXinit = xCandidate.size();
  unsigned int minUV  = m_recover_minUV[6 - nXinit];
  unsigned int nCandidates( 0 );
  bool         hasMaxCands( false );
  for ( unsigned int i = 0; i < 3; ++i ) {
    for ( unsigned int j = 0; j < DEPTH_HOUGH; ++j ) {
      if ( hasMaxCands ) break;
      // you are at the cluster Number (1+j) +(3*i)
      // By construction the first clusters you process are the ones with smaller spread
      auto itBeg = initBeg[i][j];
      auto itEnd = initEnd[i][j];
      if ( itBeg == myStereo.end() ) { break; }

      unsigned int nLay = 0;
      ExtendClusterReco( itBeg, itEnd, myStereo.end(), nLay );
      // nUVtarget are the minimal UV layers to find since track quality is based on nHits
      if ( nXinit + nLay < minTot ) continue;
      // if you already have less than minTot layers, look to next "extended" cluster"
      if ( nLay < minUV || ( nLay < nUVtarget ) ) continue; //<= if at the end you have only >= nUVtarget.
      // You increased the clusters and check to have at least minUV hits and number
      // of fired layers in current clusters can kill current candidates produced
      nCandidates++;
      if ( nCandidates == maxNClusters ) hasMaxCands = true; // will finish current loop, but then break
      // Set the plane counter
      PrPlaneHybridCounter plCount;
      plCount.set( itBeg, itEnd );
      if ( !plCount.isOK() ) { continue; } //---LoH: having a hit in all three layers
      Pr::Hybrid::SeedTrack trackCand;
      bool                  fit   = false;
      unsigned int          nInit = nXinit + itEnd - itBeg;
      // All layers fired only once.
      if ( plCount.nbSingle() == nLay ) {
        fit = BestLine.fit( itBeg, itEnd );
        fit &= LineOK( minChi2LineLow, minChi2LineHigh, xCandidate, BestLine, nInit );
        if ( !fit ) continue;
        trackCand.setFromXCand( xCandidate );
        for ( auto hit = itBeg; hit < itEnd; hit++ ) trackCand.hits().push_back( *hit );
      } else {
        // More than one hit per layer in the cluster
        // Remove outliers from multiple hit in layers is forced here
        uvHits.clear();
        for ( auto itH = itBeg; itEnd != itH; ++itH ) { uvHits.push_back( *itH ); }
        auto itBeg2 = uvHits.begin();
        auto itEnd2 = uvHits.end();
        fit         = BestLine.fit2( itBeg2, itEnd2, plCount );
        //---LoH: could probably be done in one pass with minimal loss
        while ( itEnd2 - itBeg2 > nLay ) {
          auto  worstMultiple = itEnd2;
          float worstchi      = std::numeric_limits<float>::min();
          float chi_hit;
          for ( auto line_hit = itBeg2; line_hit != itEnd2; ++line_hit ) {
            if ( plCount.nbInPlane( line_hit->planeCode ) < 2 ) continue;
            chi_hit = BestLine.chi2hit( *line_hit );
            if ( chi_hit > worstchi ) {
              worstchi      = chi_hit;
              worstMultiple = line_hit;
            }
          }
          // it will just change the m_planeCode[int] not the whole internal hit counting ! )
          plCount.removeHitInPlane( worstMultiple->planeCode );
          // Avoid relocations by swapping the worst hit to the last hit and reducing the end by one.
          --itEnd2;
          std::iter_swap( itEnd2, worstMultiple );
          fit = BestLine.fit2( itBeg2, itEnd2, plCount );
          nInit--;
          if ( nInit == minTot ) break; // Can't remove more.
        }
        // fit status is the Success of the fit ( no requirements on Chi2 )
        fit &= LineOK( minChi2LineLow, minChi2LineHigh, xCandidate, BestLine, nInit );
        if ( !fit ) continue;
        trackCand.setFromXCand( xCandidate );
        for ( auto hit = itBeg2; hit < itEnd2; hit++ ) trackCand.hits().push_back( *hit );
      }
      trackCand.setYParam( BestLine.ay(), BestLine.by() );
      //---LoH: performs the simultaneous fit
      trackCand.setnXnY( nXinit, plCount.nbDifferent() );

      bool ok = fitSimultaneouslyXY( trackCand, iCase );
      if ( !ok && ( trackCand.size() > minTot ) )
        //---LoH: right now, ONLY DOES SOMETHING because of removeWorst
        //---LoH: and it somehow improves the situation timing-wise
        std::sort( trackCand.hits().begin(), trackCand.hits().end(), compLHCbID() );

      while ( !ok && trackCand.size() > minTot ) {
        ok = removeWorstAndRefit( trackCand, iCase ); //---LoH: called 2000 times
      }
      if ( !ok ) continue;
      if ( trackCand.chi2PerDoF() >= maxChi2DoF ) continue;
      unsigned int nUVfound = trackCand.size() - nXinit;
      if ( nUVfound < nUVtarget ) continue;
      // Building the track
      if ( std::fabs( trackCand.y0() ) < m_recomaxY0 ) {
        if ( nUVtarget == 0 )
          trackCandidates.push_back( trackCand );
        else if ( removeWorstTrack( trackCandidates.back(), trackCand ) )
          std::swap( trackCandidates.back(), trackCand );
        // ordered by spread take one small spread passing selections.
        nUVtarget = nUVfound;
      } else
        continue;
    }
  }
}

//===========================================================================================================================
//
// EVERYTHING RELATED TO TRACK RECOVERY, by order of apparition
//

void PrHybridSeeding::RecoverTrack( PrFTHitHandler<ModPrHit>& FTHitHandler, TrackCandidates& trackCandidates,
                                    XCandidates& xCandidates, TrackToRecover& trackToRecover ) const noexcept {
  // Flagging all hits in the track candidates
  for ( int part = 0; part < 2; part++ ) {
    for ( auto&& itT1 : trackCandidates[(int)part] ) {
      if ( !itT1.valid() ) continue;
      for ( auto&& hit : itT1.hits() ) { FTHitHandler.hit( hit.hitIndex ).setInvalid(); }
    }
  }

  for ( int part = 0; part < 2; part++ ) {
    for ( auto&& itT1 : trackToRecover[(int)part] ) {
      int nUsed_threshold = m_nusedthreshold[6 - itT1.size()]; // LoH
      //---LoH: this should be asserted: size must be 4, 5 or 6
      //---LoH: this checks if we have enough valid hits.
      int nUsedHits = nUsed( itT1.hits().begin(), itT1.hits().end(), FTHitHandler );
      if ( nUsedHits < nUsed_threshold ) {
        if ( nUsedHits > 0 ) {
          while ( nUsedHits > 0 ) {
            auto it = std::remove_if( itT1.hits().begin(), itT1.hits().end(), [&FTHitHandler]( const ModPrHit& hit ) {
              return !FTHitHandler.hit( hit.hitIndex ).isValid();
            } );
            auto n  = std::distance( it, itT1.hits().end() );
            itT1.hits().erase( it, itT1.hits().end() );
            nUsedHits -= n;
          }
          if ( itT1.hits().size() < m_minXPlanes ) continue;
          if ( hasT1T2T3Track( itT1.hits() ) ) continue;
          fitXProjection( itT1, 2 );
        }
        itT1.setRecovered( true );
      }
    }
    removeClonesRecover( part, trackToRecover, xCandidates );
    addStereoReco( part, 2, FTHitHandler, trackCandidates, xCandidates );
  }
}

void PrHybridSeeding::flagHits( unsigned int icase,
                                // void PrHybridSeeding::flagHits( unsigned int icase,
                                unsigned int part, TrackCandidates& trackCandidates,
                                PrFTHitHandler<ModPrHit>& hitHandler ) const noexcept {
  // The hits in the track candidates are copies, but they each contain their own index
  // in the original hit container, which is used to flag the original hits.
  for ( auto& track : trackCandidates[(int)part] ) {
    if ( !track.valid() ) continue;
    if ( ( track.size() == 12 ) || ( ( track.size() == 11 && track.chi2PerDoF() < m_MaxChi2Flag[icase] &&
                                       std::fabs( track.ax() - track.bx() * Pr::Hybrid::zReference +
                                                  track.cx() * m_ConstC ) < m_MaxX0Flag[icase] ) ) )
      for ( const auto& hit : track.hits() ) {
        if ( (int)part == (int)hit.hit->zone() % 2 ) continue;
        hitHandler.hit( hit.hitIndex ).setInvalid();
      }
  }
}

//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
//---LoH: there are some things to understand, such as:
// - does the scaleFactor or the momentumScale change? If not, momentumScale/(-scaleFactor) is much better.
//
void PrHybridSeeding::makeLHCbTracks( std::vector<Track>& result, unsigned int part,
                                      const TrackCandidates& trackCandidates ) const noexcept {
  for ( const auto& track : trackCandidates[(int)part] ) {
    if ( !track.valid() ) continue;
    //***** EXACT SAME IMPLEMENTATION OF PatSeedingTool *****//
    auto& out = result.emplace_back();
    out.setType( Track::Type::Ttrack );
    out.setHistory( Track::History::PrSeeding );
    out.setPatRecStatus( Track::PatRecStatus::PatRecIDs );
    for ( const auto& hit : track.hits() ) { out.addToLhcbIDs( hit.hit->id() ); }
    LHCb::State temp( Gaudi::TrackVector( track.x0(), track.yRef(), track.xSlope0(), //---LoH: can be simplified into
                                                                                     // ax(), ay()
                                          track.ySlope(), 0.f ),
                      Pr::Hybrid::zReference, LHCb::State::Location::AtT );
    double      qOverP, sigmaQOverP;
    const float scaleFactor = m_magFieldSvc->signedRelativeCurrent();
    if ( m_momentumTool->calculate( &temp, qOverP, sigmaQOverP, true ).isFailure() ) {
      if ( std::abs( scaleFactor ) < 1.e-4f ) {
        qOverP      = ( ( track.cx() < 0 ) ? -1.f : 1.f ) * ( ( scaleFactor < 0.f ) ? -1.f : 1.f ) / Gaudi::Units::GeV;
        sigmaQOverP = 1.f / Gaudi::Units::MeV;
      } else {
        qOverP      = track.cx() * m_momentumScale / ( -1.f * scaleFactor );
        sigmaQOverP = 0.5f * qOverP;
      }
    }
    temp.setQOverP( qOverP );
    Gaudi::TrackSymMatrix& cov = temp.covariance();
    cov( 0, 0 )                = m_stateErrorX2;
    cov( 1, 1 )                = m_stateErrorY2;
    cov( 2, 2 )                = m_stateErrorTX2;
    cov( 3, 3 )                = m_stateErrorTY2;
    cov( 4, 4 )                = sigmaQOverP * sigmaQOverP;
    for ( const float z : m_zOutputs ) {
      temp.setX( track.x( z ) );
      temp.setY( track.y( z ) );
      temp.setZ( z );
      temp.setTx( track.xSlope( z ) );
      temp.setTy( track.ySlope() );
      out.addToStates( temp );
    }
    out.setChi2PerDoF( {track.chi2PerDoF(), ( (int)track.hits().size() - 5 )} );
  }
}

//=========================================================================
//  Fit the track, return OK if fit sucecssfull
//=========================================================================
//---LoH: probably can integrate some chi2 checks in here
bool PrHybridSeeding::fitSimultaneouslyXY( Pr::Hybrid::SeedTrack& track, unsigned int iCase ) const noexcept {
  if ( track.nx() < 4 || track.ny() < 4 ) return false;
  float mat[15];
  float rhs[5];
  // unsigned int nHitsX = 0;
  // unsigned int nHitsStereo = 0;
  mat[0] = 0.f;
  for ( const auto& modHit : track.hits() ) { mat[0] += modHit.hit->w(); }
  for ( unsigned int loop = 0; 3 > loop; ++loop ) {
    if ( loop == 1 && m_useCorrPos ) {
      float RadiusPosition =
          std::sqrt( ( 5.f * 5.f * 1.e-8f * track.ax() * track.ax() + 1e-6f * track.yRef() * track.yRef() ) );
      float dRatioPos = -1.f * ( m_dRatioPar[0] + m_dRatioPar[1] * RadiusPosition +
                                 m_dRatioPar[2] * RadiusPosition * RadiusPosition );
      track.setdRatio( dRatioPos );
    }
    std::fill( mat + 1, mat + 15, 0.f );
    std::fill( rhs, rhs + 5, 0.f );
    for ( const auto& modHit : track.hits() ) {
      const PrHit& hit      = *( modHit.hit );
      const float  w        = hit.w();
      const float  dxdy     = hit.dxDy();
      const float  yOnTrack = track.yOnTrack( hit );
      const float  dz       = 0.001f * ( hit.z( yOnTrack ) - Pr::Hybrid::zReference );
      const float  deta     = dz * dz * ( 1.f + dz * track.dRatio() );
      const float  wdz      = w * dz;
      const float  weta     = w * deta;
      const float  wdxdy    = w * dxdy;
      const float  wdxdydz  = wdxdy * dz;
      const float  dist     = track.distance( hit );
      mat[1] += wdz;
      mat[2] += wdz * dz;
      mat[3] += weta;
      mat[4] += weta * dz;
      mat[5] += weta * deta;
      mat[6] -= wdxdy;
      mat[7] -= wdxdydz;
      mat[8] -= wdxdy * deta;
      mat[9] += wdxdy * dxdy;
      mat[10] -= wdxdydz;
      mat[11] -= wdxdydz * dz;
      mat[12] -= wdxdydz * deta;
      mat[13] += wdxdydz * dxdy;
      mat[14] += wdxdydz * dz * dxdy;
      // fill right hand side
      rhs[0] += w * dist;
      rhs[1] += wdz * dist;
      rhs[2] += weta * dist;
      rhs[3] -= wdxdy * dist;
      rhs[4] -= wdxdydz * dist;
    } // Loop over Hits to fill the matrix
    // decompose matrix, protect against numerical trouble
    // track.setnXnY( nHitsX, nHitsStereo );
    // if(nHitsX < 4 || nHitsStereo < 4) return false;
    ROOT::Math::CholeskyDecomp<float, 5> decomp( mat );
    if ( !decomp ) return false;
    decomp.Solve( rhs );
    rhs[1] *= 1.e-3f;
    rhs[2] *= 1.e-6f;
    rhs[4] *= 1.e-3f;
    rhs[3] -= rhs[4] * Pr::Hybrid::zReference;
    // crazy values!
    //---LoH: commented as it slows down more that accelerates things
    //    if ( ( std::fabs( rhs[0] ) > 1e4f || std::fabs( rhs[1] ) > 5.f || std::fabs( rhs[2] ) > 1e-3f ||
    //           std::fabs( rhs[3] ) > 1e4f || std::fabs( rhs[4] ) > 1.f ) )
    //      return false;
    track.updateParameters( rhs[0], rhs[1], rhs[2], rhs[3], rhs[4] );
  }
  float maxChi2    = std::numeric_limits<float>::min();
  float chi2_onHit = std::numeric_limits<float>::max();
  float sumChi2    = 0;
  for ( const auto& itH : track.hits() ) {
    chi2_onHit = track.chi2( *( itH.hit ) );
    sumChi2 += chi2_onHit;
    if ( chi2_onHit > maxChi2 ) { maxChi2 = chi2_onHit; }
  } // Set Max Chi2DoF
  track.setChi2( sumChi2, track.hits().size() - 5 );
  if ( ( track.recovered() ) ) {
    if ( maxChi2 < m_recoChiOutlier ) return true;
    return false;
  } else {
    if ( ( track.size() > 10 ) && maxChi2 < m_maxChi2HitFullFitHigh[iCase] ) return true;
    if ( maxChi2 < m_maxChi2HitFullFitLow[iCase] && track.size() < 11 ) return true;
  }
  return false;
}

//=======================================
// Fit Only X Projection
//=======================================
bool PrHybridSeeding::fitXProjection( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept {
  //---LoH: called 30k times
  //  if ( track.size() < m_minXPlanes ) return false;
  float mat[6];
  std::fill( mat, mat + 3, 0.f );
  float rhs[3];
  // First loop to set some things
  for ( const auto& modHit : track.hits() ) {
    const PrHit& hit = *( modHit.hit );
    float        w   = hit.w(); // squared
    const float  dz  = 0.001f * ( hit.z() - Pr::Hybrid::zReference );
    mat[0] += w;
    mat[1] += w * dz;
    mat[2] += w * dz * dz;
  }
  for ( unsigned int loop = 0; 3 > loop; ++loop ) {
    std::fill( mat + 3, mat + 6, 0.f );
    std::fill( rhs, rhs + 3, 0.f );
    for ( const auto& modHit : track.hits() ) {
      // for( auto itH = track.hits().begin(); track.hits().end() != itH; ++itH ){
      const PrHit& hit    = *( modHit.hit );
      const float  dRatio = track.dRatio();
      float        w      = hit.w(); // squared
      const float  dz     = 0.001f * ( hit.z() - Pr::Hybrid::zReference );
      const float  deta   = dz * dz * ( 1.f + dRatio * dz );
      const float  dist   = track.distance( hit );
      mat[3] += w * deta;
      mat[4] += w * dz * deta;
      mat[5] += w * deta * deta;
      rhs[0] += w * dist;
      rhs[1] += w * dist * dz;
      rhs[2] += w * dist * deta;
    }
    ROOT::Math::CholeskyDecomp<float, 3> decomp( mat ); //---LoH: can probably be made more rapidly
    if ( !decomp ) { return false; }
    // Solve linear system
    decomp.Solve( rhs );
    rhs[1] *= 1.e-3f;
    rhs[2] *= 1.e-6f;
    // protect against unreasonable track parameter corrections
    //---LoH: commented as it slows down more that accelerates things
    //    if ( std::fabs( rhs[0] ) > 1.e4f || std::fabs( rhs[1] ) > 5.f || std::fabs( rhs[2] ) > 1.e-3f ) return false;
    // Small corrections
    track.updateParameters( rhs[0], rhs[1], rhs[2] );
    // Put back later faster maybe
    if ( loop > 0 && std::abs( rhs[0] ) < 5e-5f && std::abs( rhs[1] ) < 5e-8f && std::abs( rhs[2] ) < 5e-11f ) {
      break;
    }
  }
  // Compute some values on the track
  auto       hitChi2 = [&track]( const ModPrHit& hit ) { return track.chi2( *hit.hit ); };
  const auto chi2s   = track.hits() | view::transform( hitChi2 );
  float      sum( 0 );
  for ( auto chi2 : chi2s ) sum += chi2;
  track.setChi2( sum, track.hits().size() - 3 );
  return v3::max( chi2s ) < m_maxChi2HitsX[iCase];
}

//---LoH: somehow this cares about the sorting.
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::removeWorstAndRefit( Pr::Hybrid::SeedTrack& track, unsigned int iCase ) const noexcept {
  // maybe useless?
  const auto worst =
      std::min_element( track.hits().begin(), track.hits().end(), [&track]( ModPrHit& lhs, ModPrHit& rhs ) {
        return track.chi2( *( lhs.hit ) ) > track.chi2( *( rhs.hit ) );
      } );
  track.hits().erase( worst );
  track.setnXnY( track.nx() - ( (int)worst->hit->isX() ), track.ny() - ( (int)!( worst->hit->isX() ) ) );
  return fitSimultaneouslyXY( track, iCase );
}

//=========================================================================
//  Remove the worst hit and refit.
//=========================================================================
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::removeWorstAndRefitX( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept {
  track.hits().erase(
      std::min_element( track.hits().begin(), track.hits().end(), [&track]( ModPrHit& lhs, ModPrHit& rhs ) {
        return track.chi2( *( lhs.hit ) ) > track.chi2( *( rhs.hit ) );
      } ) );
  return fitXProjection( track, iCase );
}

//=========================================================================
//  Finding the X projections. This is the most time-consuming part of the algorithm
//=========================================================================

void PrHybridSeeding::findXProjections( unsigned int part, unsigned int iCase,
                                        const PrFTHitHandler<ModPrHit>& FTHitHandler, XCandidates& xCandidates ) const
    noexcept {
  ZonesXSearch xZones = {0.f, 0.f, {}, {}, {}, {}, {}}; // zones, planeCodes, z, dz, dz2
  initializeXProjections( iCase, part, xZones );

  float slope = ( m_tolAtX0Cut[iCase] - m_tolX0SameSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange[iCase] );
  float slopeopp =
      ( m_tolAtx0CutOppSign[iCase] - m_tolX0OppSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange2[iCase] );
  float accTerm1 = slope * m_x0SlopeChange[iCase] - m_tolX0SameSign[iCase];
  float accTerm2 = slopeopp * m_x0SlopeChange2[iCase] - m_tolX0OppSign[iCase];

  //======================================================================================================================================================
  ParabolaParams solver0;
  ParabolaParams solver1;
  buildParabola( xZones, 2, solver0 );
  buildParabola( xZones, 3, solver1 );
  Boundaries Bounds;
  Boundaries svgBounds;
  //============Initialization
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> begZones;
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> endZones;
  // Always: first=0 last=1 middles=2,3 remains=4,5 (order of loops)
  for ( unsigned int i = 0; i < PrFTInfo::Numbers::NFTXLayers; i++ ) {
    Bounds[i]    = InitializeBB( xZones.zones[i], FTHitHandler );
    svgBounds[i] = InitializeBB( xZones.zones[i], FTHitHandler );
    auto r       = FTHitHandler.hits( xZones.zones[i] );
    begZones[i]  = r.begin();
    endZones[i]  = r.end();
  }
  for ( unsigned int i = 1; i < PrFTInfo::Numbers::NFTXLayers; i++ ) {
    Bounds[i].end    = Bounds[i].begin;
    svgBounds[i].end = svgBounds[i].begin;
  }
  ModPrHitConstIter Fhit;
  ModPrHitConstIter Lhit;
  // Used to cache the position to do a "look-around search"
  std::array<float, PrFTInfo::Numbers::NFTXLayers> xminPrev;
  xminPrev.fill( std::numeric_limits<float>::lowest() );

  std::array<std::vector<ModPrHit>, 3> parabolaSeedHits; // 0: layer T2x1, 1: layer T2x2, 2: extrapolated T2x2
  parabolaSeedHits[0].reserve( 100 );
  parabolaSeedHits[1].reserve( 100 );
  parabolaSeedHits[2].reserve( 100 );
  bool              OK = false;
  bool              first;
  TwoHitCombination hitComb;
  float             tolHp = m_TolFirstLast[iCase];
  for ( ; Bounds[0].begin != Bounds[0].end; ++Bounds[0].begin ) // for a hit in first layer
  {
    Fhit = Bounds[0].begin;
    if ( !Fhit->isValid() ) continue;
    const auto* fHit      = Fhit->hit;
    float       xFirst    = fHit->x();
    float       tx_inf    = xFirst * xZones.invZf;
    float       xProjeInf = tx_inf * xZones.zLays[1];
    float       maxXl     = xProjeInf + tx_inf * m_alphaCorrection[iCase] + tolHp;
    float       minXl     = maxXl - 2 * tolHp;
    // Setting the last-layer bounds
    Bounds[1].begin = get_lowerBound_lin( Bounds[1].begin, endZones[1], minXl ); //---LoH: should be very small
    Bounds[1].end   = get_upperBound_lin( Bounds[1].end, endZones[1], maxXl ); //---LoH: between 6 and 20 times roughly
    // Reinitialise
    Bounds[2].begin = svgBounds[2].begin;
    Bounds[3].begin = svgBounds[3].begin;
    first           = true;
    for ( Lhit = Bounds[1].begin; Lhit != Bounds[1].end; ++Lhit ) { // for a hit in last layer
      if ( !Lhit->isValid() ) continue;
      //---LoH: For 100 events, this is ran 1.6M times (number of updateXZCombination)
      float xLast = Lhit->hit->x();
      updateXZCombinationPars( iCase, xFirst, xLast, xZones, slope, slopeopp, accTerm1, accTerm2,
                               hitComb ); // New parameters
      parabolaSeedHits[0].clear();
      parabolaSeedHits[1].clear();
      parabolaSeedHits[2].clear();
      // Look for parabola hits and update bounds
      OK = findParabolaHits( 2, xZones, hitComb, endZones, Bounds, parabolaSeedHits[0] );
      OK += findParabolaHits( 3, xZones, hitComb, endZones, Bounds, parabolaSeedHits[1] );
      if ( first ) {
        svgBounds[2].begin = Bounds[2].begin;
        svgBounds[3].begin = Bounds[3].begin;
        first              = false;
      }
      if ( !OK ) continue;
      updateParabola( xFirst, xLast, solver0 );
      updateParabola( xFirst, xLast, solver1 );
      // First parabola
      for ( unsigned int i = 0; i < parabolaSeedHits[0].size(); ++i )
        fillXhits0( iCase, part, Fhit, parabolaSeedHits[0][i], Lhit, xZones, begZones, endZones, xCandidates,
                    parabolaSeedHits[2], Bounds, xminPrev, solver0 );

      //---LoH: Remove hits in T2x2 that were already built in T2x1 extension. Happens 42k times in 100 events.
      for ( auto hit1 : parabolaSeedHits[2] ) {
        auto pos = std::find( parabolaSeedHits[1].begin(), parabolaSeedHits[1].end(), hit1 );
        if ( pos != parabolaSeedHits[1].end() ) { parabolaSeedHits[1].erase( pos ); }
      }
      // Second parabola
      for ( unsigned int i = 0; i < parabolaSeedHits[1].size(); ++i )
        fillXhits1( iCase, part, Fhit, parabolaSeedHits[1][i], Lhit, xZones, begZones, endZones, xCandidates, Bounds,
                    xminPrev, solver1 );
    }
  } // end loop first zone
}

// Builds the parabola solver from solely z information
//---LoH: Can be put outside of the class if m_dRatio is externalised
void PrHybridSeeding::buildParabola( const ZonesXSearch& xZone, unsigned int iLayer, ParabolaParams& pars ) const
    noexcept {
  pars.z1 = xZone.dzLays[0];
  pars.z2 = xZone.dzLays[iLayer];
  pars.z3 = xZone.dzLays[1];

  pars.corrZ1 = ( 1.f + m_dRatio * pars.z1 ) * pars.z1 * pars.z1;
  pars.corrZ2 = ( 1.f + m_dRatio * pars.z2 ) * pars.z2 * pars.z2;
  pars.corrZ3 = ( 1.f + m_dRatio * pars.z3 ) * pars.z3 * pars.z3;
  pars.det    = pars.z1 * ( pars.corrZ3 - pars.corrZ2 ) + pars.z2 * ( pars.corrZ1 - pars.corrZ3 ) +
             pars.z3 * ( pars.corrZ2 - pars.corrZ1 );
  pars.recdet = 1.f / pars.det;

  pars.dz13     = pars.z1 - pars.z3;
  pars.dcorr13  = pars.corrZ1 - pars.corrZ3;
  pars.dzcorr13 = pars.z1 * pars.corrZ3 - pars.z3 * pars.corrZ1;
}

// Updates the parabola solver with two-hit information
//---LoH: Can be put outside of the class.
void PrHybridSeeding::updateParabola( const float& x1, const float& x3, ParabolaParams& pars ) const noexcept {
  pars.det1 = x1 * ( pars.z2 - pars.z3 ) + x3 * ( pars.z1 - pars.z2 );
  pars.det2 = x1 * ( pars.corrZ3 - pars.corrZ2 ) + x3 * ( pars.corrZ2 - pars.corrZ1 );
  pars.det3 = pars.corrZ1 * pars.z2 * x3 + pars.corrZ2 * ( pars.z3 * x1 - x3 * pars.z1 ) - pars.corrZ3 * pars.z2 * x1;
}

// Solves the parabola
//---LoH: Can be put outside of the class.
void PrHybridSeeding::solveParabola( const ParabolaParams& pars, const float& x2, float& a1, float& b1,
                                     float& c1 ) const noexcept {
  const float det1 = pars.det1 - x2 * pars.dz13;
  const float det2 = pars.det2 + x2 * pars.dcorr13;
  const float det3 = pars.det3 + x2 * pars.dzcorr13;

  a1 = pars.recdet * det1;
  b1 = pars.recdet * det2;
  c1 = pars.recdet * det3;
}

// Fill hits due to parabola seeds in T2x1
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits0( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, std::vector<ModPrHit>& parabolaSeedHits, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev,
                                  ParabolaParams&                                   Pars ) const noexcept {
  float          a( 0.f ), b( 0.f ), c( 0.f );
  SeedTrackHitsX xHits;
  solveParabola( Pars, Phit.hit->x(), a, b, c ); // Extrapolation with dRatio
  if ( fillXhitParabola( iCase, 3, xHits, a * xZones.dz2Lays[3] + b * xZones.dzLays[3] + c, Bounds ) )
    parabolaSeedHits.push_back( xHits[0] );
  fillXhitRemaining( iCase, 4, a * xZones.dz2Lays[4] + b * xZones.dzLays[4] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );   //---Called 767k times in total
  if ( xHits.size() == 0 ) return; //---LoH: ~650k times out of 750k
  // Add the last hit
  fillXhitRemaining( iCase, 5, a * xZones.dz2Lays[5] + b * xZones.dzLays[5] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );  // Missing T3x
  if ( xHits.size() < 2 ) return; //---LoH: ~50k times out of 750k
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x1
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits ); //---LoH: called 27k times
  return;
}

// Fill hits due to parabola seeds in T2x2
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits1( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev,
                                  ParabolaParams&                                   Pars ) const noexcept {
  float          a( 0.f ), b( 0.f ), c( 0.f );
  SeedTrackHitsX xHits;
  solveParabola( Pars, Phit.hit->x(), a, b, c ); // Extrapolation with dRatio
  fillXhitParabola( iCase, 2, xHits, a * xZones.dz2Lays[2] + b * xZones.dzLays[2] + c, Bounds );
  fillXhitRemaining( iCase, 4, a * xZones.dz2Lays[4] + b * xZones.dzLays[4] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );   // Missing T1x
  if ( xHits.size() == 0 ) return; //---LoH: ~650k times out of 750k
  // Add the last hit
  fillXhitRemaining( iCase, 5, a * xZones.dz2Lays[5] + b * xZones.dzLays[5] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );  // Missing T3x
  if ( xHits.size() < 2 ) return; //---LoH: ~50k times out of 750k
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x1
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits );
}

void PrHybridSeeding::createXTrack( const unsigned int& iCase, const unsigned int& part, XCandidates& xCandidates,
                                    SeedTrackHitsX& xHits ) const noexcept {
  //---LoH: called 27k times
  //------    Create the track
  Pr::Hybrid::SeedTrackX xCand( xHits );
  //------    Set the dRatio value for the track here
  xCand.setdRatio( m_dRatio );
  //------    Algorithm allows to go down to 4 hits only from 6 hits track refit by construction.
  //-----     OK is the status of the fit
  bool OK = fitXProjection( xCand, iCase );
  while ( !OK ) {
    if ( xHits.size() != 6 || xCand.size() <= m_minXPlanes ) { break; }
    OK = removeWorstAndRefitX( xCand, iCase );
  }
  if ( !OK ) return;
  //---- Fit is successful and  make a x/z-candidate
  if ( ( ( xCand.chi2PerDoF() < m_maxChi2DoFX[iCase] ) ) ) {
    xCand.setXT1( xCand.x( StateParameters::ZBegT ) );
    xCand.setXT2( xCand.x0() ); //---LoH: it is zReference
    xCand.setXT3( xCand.x( StateParameters::ZEndT ) );
    xCandidates[(int)part].push_back( xCand ); // The X Candidate is created
  }
}

void PrHybridSeeding::ExtendCluster( StereoIter& itBeg, StereoIter& itEnd, const StereoIter& stereoEnd,
                                     unsigned int iCase, unsigned int& nLay ) const noexcept {
  std::bitset<PrFTInfo::Numbers::NFTUVLayers + PrFTInfo::Numbers::NFTXLayers> planes;
  std::for_each( itBeg, itEnd, [&]( const ModPrHit hit ) { planes.set( hit.planeCode ); } );
  nLay         = planes.count(); // Number of x planes crossed.
  float begVal = ( *itBeg ).coord;
  float tolTy  = (float)m_tolTyOffset[iCase] + begVal * m_tolTySlope[iCase];
  while ( itEnd != stereoEnd && ( itEnd->coord - begVal ) < tolTy && nLay < PrFTInfo::Numbers::NFTUVLayers ) {
    // as soon as you find a sixth element exit the loop (in other case extend up to tolerance
    planes.set( itEnd->planeCode );
    ++itEnd;
    nLay = planes.count();
  }
}

void PrHybridSeeding::ExtendClusterReco( StereoIter& itBeg, StereoIter& itEnd, const StereoIter& stereoEnd,
                                         unsigned int& nLay ) const noexcept {
  std::bitset<PrFTInfo::Numbers::NFTUVLayers + PrFTInfo::Numbers::NFTXLayers> planes;
  std::for_each( itBeg, itEnd, [&]( const ModPrHit hit ) { planes.set( hit.planeCode ); } );
  nLay         = planes.count(); // Number of x planes crossed.
  float begVal = ( *itBeg ).coord;
  float tolTy  = (float)m_recoTolTy;
  while ( itEnd != stereoEnd && ( itEnd->coord - begVal ) < tolTy && nLay < PrFTInfo::Numbers::NFTUVLayers ) {
    // as soon as you find a sixth element exit the loop (in other case extend up to tolerance
    planes.set( itEnd->planeCode );
    ++itEnd;
    nLay = planes.count();
  }
}

bool PrHybridSeeding::fillXhitParabola( const int& iCase, const int& iLayer, SeedTrackHitsX& xHits, const float& xAtZ,
                                        const std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>& Bounds ) const
    noexcept {
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase]; // * (1. + dz*invZref );
  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
  ModPrHitConstIter bestProj = Bounds[iLayer].end;
  float             bestDist = m_tolRemaining[iCase]; // 1 cm around predicted position!!
  const PrHit*      hit;
  float             tmpDist;
  for ( auto itH = Bounds[iLayer].begin; itH != Bounds[iLayer].end; ++itH ) {
    if ( !itH->isValid() ) { continue; }
    hit = itH->hit;
    if ( hit->x() < xMinAtZ ) { continue; }
    tmpDist = std::fabs( hit->x() - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = itH;
    } else
      break;
  }
  // Can happen if there is only one hit in the loop.
  if ( bestProj != Bounds[iLayer].end ) {
    xHits.push_back( *bestProj );
    return true;
  }
  return false;
}

void PrHybridSeeding::fillXhitRemaining( const int& iCase, const int& iLayer, const float& xAtZ,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                         SeedTrackHitsX&                                           xHits,
                                         std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>&     Bounds,
                                         std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev ) const noexcept {
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase];
  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
  LookAroundMin( Bounds[iLayer], xminPrev[iLayer], xMinAtZ, endZones[iLayer], begZones[iLayer] ); // only costs 33%
  ModPrHitConstIter bestProj = endZones[iLayer];
  const PrHit*      hit;
  float             bestDist = m_tolRemaining[iCase];
  float             tmpDist;
  for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end < endZones[iLayer]; Bounds[iLayer].end++ ) {
    if ( !Bounds[iLayer].end->isValid() ) continue;
    hit = Bounds[iLayer].end->hit;
    if ( hit->x() < xMinAtZ ) { continue; }
    tmpDist = std::fabs( hit->x() - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = Bounds[iLayer].end;
    } else
      break;
  }
  if ( bestProj != endZones[iLayer] ) { xHits.push_back( *bestProj ); }
  return;
}

void PrHybridSeeding::updateXZCombinationPars( const unsigned int& iCase, const float& xFirst, const float& xLast,
                                               const ZonesXSearch& xZones, const float& slope, const float& slopeopp,
                                               const float& accTerm1, const float& accTerm2,
                                               TwoHitCombination& hitComb ) const noexcept {
  hitComb.tx    = ( xLast - xFirst ) * xZones.invZlZf;
  hitComb.x0    = xFirst - hitComb.tx * xZones.zLays[0];
  hitComb.x0new = hitComb.x0 * ( 1.f + m_x0Corr[iCase] );
  if ( !m_parabolaSeedParabolicModel ) {
    if ( hitComb.x0 > 0.f ) {
      hitComb.minPara = hitComb.x0 > m_x0SlopeChange[iCase] ? -slope * hitComb.x0 + accTerm1 : -m_tolX0SameSign[iCase];
      hitComb.maxPara =
          hitComb.x0 > m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 - accTerm2 : +m_tolX0OppSign[iCase];
    } else {
      hitComb.maxPara = hitComb.x0 < -m_x0SlopeChange[iCase] ? -slope * hitComb.x0 - accTerm1 : m_tolX0SameSign[iCase];
      hitComb.minPara =
          hitComb.x0 < -m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 + accTerm2 : -m_tolX0OppSign[iCase];
    }
  } else {
    hitComb.maxPara =
        m_parPol0[iCase] + hitComb.x0new * m_parPol1[iCase] + hitComb.x0new * hitComb.x0new * m_parPol2[iCase];
    hitComb.minPara = hitComb.maxPara - 2 * hitComb.x0new * m_parPol1[iCase];
  }
}

void PrHybridSeeding::initializeXProjections( unsigned int iCase, unsigned int part, ZonesXSearch& xZones ) const
    noexcept {
  unsigned int firstZoneId( 0 ), lastZoneId( 0 );
  if ( 0 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X2 - part;
  } else if ( 1 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 2 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 3 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X2 - part;
  }

  // Array[0] = first layer in T1 for 2-hit combo

  xZones.zones[0]     = firstZoneId;
  xZones.zLays[0]     = m_z[firstZoneId];
  xZones.dzLays[0]    = xZones.zLays[0] - Pr::Hybrid::zReference;
  xZones.dz2Lays[0]   = xZones.dzLays[0] * xZones.dzLays[0] * ( 1.f + m_dRatio * xZones.dzLays[0] );
  xZones.planeCode[0] = m_planeCode[firstZoneId];
  // Array[1] = last  layer in T3 for 2-hit combo
  xZones.zones[1]     = lastZoneId;
  xZones.zLays[1]     = m_z[lastZoneId];
  xZones.dzLays[1]    = xZones.zLays[1] - Pr::Hybrid::zReference;
  xZones.dz2Lays[1]   = xZones.dzLays[1] * xZones.dzLays[1] * ( 1.f + m_dRatio * xZones.dzLays[1] );
  xZones.planeCode[1] = m_planeCode[lastZoneId];

  // Array[2] = T2-1st x-layers
  xZones.zones[2]     = T2X1 - part;
  xZones.zLays[2]     = m_z[T2X1 - part];
  xZones.dzLays[2]    = xZones.zLays[2] - Pr::Hybrid::zReference;
  xZones.dz2Lays[2]   = xZones.dzLays[2] * xZones.dzLays[2] * ( 1.f + m_dRatio * xZones.dzLays[2] );
  xZones.planeCode[2] = m_planeCode[lastZoneId];
  // Array[3] = T2-2nd x-layers
  xZones.zones[3]     = T2X2 - part;
  xZones.zLays[3]     = m_z[T2X2 - part];
  xZones.dzLays[3]    = xZones.zLays[3] - Pr::Hybrid::zReference;
  xZones.dz2Lays[3]   = xZones.dzLays[3] * xZones.dzLays[3] * ( 1.f + m_dRatio * xZones.dzLays[3] );
  xZones.planeCode[3] = m_planeCode[T2X2 - part];

  unsigned int i = 4;
  // Add extra layer HERE, if needed!!!!
  for ( unsigned int xZoneId : {T1X1, T1X2, T3X1, T3X2} ) {
    xZoneId = xZoneId - part;
    if ( xZoneId != firstZoneId && xZoneId != lastZoneId ) {
      xZones.zones[i]     = xZoneId;
      xZones.zLays[i]     = m_z[xZoneId];
      xZones.dzLays[i]    = xZones.zLays[i] - Pr::Hybrid::zReference;
      xZones.dz2Lays[i]   = xZones.dzLays[i] * xZones.dzLays[i] * ( 1.f + m_dRatio * xZones.dzLays[i] );
      xZones.planeCode[i] = m_planeCode[xZoneId];
      i++;
    }
  }
  xZones.invZf   = 1.f / xZones.zLays[0];
  xZones.invZlZf = 1.f / ( xZones.zLays[1] - xZones.zLays[0] );
}

//---LoH: 40% of the findXProjection timing.
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::findParabolaHits( const unsigned int& iLayer, const ZonesXSearch& xZones,
                                        const TwoHitCombination&                                  hitComb,
                                        const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                        Boundaries& Bounds, std::vector<ModPrHit>& parabolaSeedHits ) const noexcept {
  float        xProjectedCorrected = xZones.zLays[iLayer] * hitComb.tx + hitComb.x0new; // Target
  float        xMin                = xProjectedCorrected + hitComb.minPara;             // may add a 1.0 mm here?
  float        xMax                = xProjectedCorrected + hitComb.maxPara;             // may add a 1.0 mm here?
  const PrHit* hit;
  Bounds[iLayer].begin = get_lowerBound_lin( Bounds[iLayer].begin, endZones[iLayer], xMin );
  for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end != endZones[iLayer]; ++Bounds[iLayer].end ) {
    if ( !Bounds[iLayer].end->isValid() ) continue;
    hit = Bounds[iLayer].end->hit;
    if ( hit->x() > xMax ) break;
    parabolaSeedHits.push_back( *( Bounds[iLayer].end ) );
  }
  return ( parabolaSeedHits.size() != 0 );
}

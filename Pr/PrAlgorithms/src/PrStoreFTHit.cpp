/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PrStoreFTHit.h"

#include <boost/numeric/conversion/cast.hpp>

#include "range/v3/version.hpp"
#include "range/v3/view/transform.hpp"
// upstream has renamed namespace ranges::view ranges::views
#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

//-----------------------------------------------------------------------------
// Implementation file for class : PrStoreFTHit
// This algorithms is expected to store the hits in TES together with Geometry
// information ( should be fixed ?) . Geometry information are encoded in the
// PrFTHiHandler::m_zones private variable
//
// 2016-07-07 : Renato Quagliani
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrStoreFTHit )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrStoreFTHit::PrStoreFTHit( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"InputLocation", LHCb::FTLiteClusterLocation::Default},
                   KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrStoreFTHit::initialize() {
  // parent initialization
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // Load detector and GeometryBuild is SUPER-time consuming, so this is stored
  // as a condition in the detector store and updated only when the geometry changes
  // This is possible thanks to the UpdateManager service that allows to register
  // a condition and the method to be called when it changes
  // TODO move this to Condition Handles once these ones they are available

  // register a derived condition object for the zone cache
  detSvc()->registerObject( PrFTInfo::FTCondLocation, new DataObject() ).ignore();
  m_zoneHandler = new PrFTZoneHandler();
  detSvc()->registerObject( PrFTInfo::FTZonesLocation, m_zoneHandler ).ignore();
  // make sure the detector element is updated before the algorithm is executed
  registerCondition( DeFTDetectorLocation::Default, m_ftDet, &PrStoreFTHit::buildGeometry );
  // make sure the derived condition is updated when the conditions of this algorithm are
  updMgrSvc()->registerCondition( m_zoneHandler, this );
  // This is needed to work around a missing update of the pointer when loading the detector element
  // This is a bug a priori specific to detector elements
  updMgrSvc()->update( m_zoneHandler ).ignore();

  // TODO: this should be ~80 micron; get this from a tool
  std::array<float, 9> clusRes = {0.05, 0.08, 0.11, 0.14, 0.17, 0.20, 0.23, 0.26, 0.29};
  std::transform( clusRes.begin(), clusRes.end(), m_invClusResolution.begin(), []( float c ) { return 1. / c; } );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
PrFTHitHandler<PrHit> PrStoreFTHit::operator()( const FTLiteClusters& clusters ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "==> Execute" << endmsg;
    debug() << "Detector version used: " << m_ftDet->version() << endmsg;
  }
  // create a hitHandler to be returned and stored in the TES
  PrFTHitHandler<PrHit> hitHandler( clusters.size() );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Retrieved " << clusters.size() << " clusters" << endmsg;

  for ( uint16_t iQuarter = 0; iQuarter < DeFTDetector::nQuarters; ++iQuarter ) {
    uint info = ( iQuarter >> 1 ) | ( ( ( iQuarter << 4 ) ^ ( iQuarter << 5 ) ^ 128u ) & 128u );

    auto r = ranges::views::transform( clusters.range( iQuarter ), [&]( LHCb::FTLiteCluster clus ) -> PrHit {
      LHCb::FTChannelID id       = clus.channelID();
      auto              index    = id.uniqueMat();
      float             dxdy     = m_mats_dxdy[index];
      float             dzdy     = m_mats_dzdy[index];
      float             globaldy = m_mats_globaldy[index];
      float             uFromChannel =
          m_mats_uBegin[index] + ( 2 * id.channel() + 1 + clus.fractionBit() ) * m_mats_halfChannelPitch[index];
      if ( id.die() ) uFromChannel += m_mats_dieGap[index];
      uFromChannel += id.sipm() * m_mats_sipmPitch[index];
      auto  endPoint = Gaudi::XYZPointF( m_mats_mirrorPoint[index] + m_mats_ddx[index] * uFromChannel );
      float x0       = endPoint.x() - dxdy * endPoint.y();
      float z0       = endPoint.z() - dzdy * endPoint.y();
      float yMin     = endPoint.y();
      float yMax     = yMin + globaldy;
      if ( id.isBottom() ) std::swap( yMin, yMax );

      assert( clus.pseudoSize() < 9 && "Pseudosize of cluster is > 8. Out of range." );
      float werrX = m_invClusResolution[clus.pseudoSize()];

      return {LHCb::LHCbID( id ), x0, z0, dxdy, dzdy, yMin, yMax, werrX, werrX * werrX, info};
    } );

    hitHandler.insert( ( iQuarter >> 1 ), r.begin(), r.end() );
  }

  // Verify that the hits are sorted as expected
  assert( hitHandler.hits().is_sorted( []( const auto& lhs, const auto& rhs ) { return lhs.x() < rhs.x(); } ) &&
          "FT hits must be properly sorted for the pattern recognition "
          "Lower by X for each zone" );

  if ( UNLIKELY( msgLevel( MSG::VERBOSE ) ) ) {
    const auto& container = hitHandler.hits();
    const auto& offsets   = container.offsets();
    for ( size_t i = 0; i < container.size(); ++i ) {
      unsigned id = 2 * container.hit( i ).planeCode() + container.hit( i ).zone();
      verbose() << std::setw( 6 ) << std::right << i << " [" << offsets[id].first << ";" << offsets[id].second << "] "
                << std::setw( 6 ) << std::right << id << std::setw( 10 ) << std::right << container.hit( i ).x()
                << endmsg;
    }
  }

  // return
  return hitHandler;
}

//=============================================================================
// buildGeometry
//=============================================================================
StatusCode PrStoreFTHit::buildGeometry() {
  info() << "FtDEt = " << m_ftDet << endmsg;
  if ( m_ftDet->version() < 61 ) {
    error() << "This version requires FTDet v6.1 or higher" << endmsg;
    return StatusCode::FAILURE;
  }
  for ( auto station : m_ftDet->stations() ) {
    for ( auto layer : station->layers() ) {
      int id = 4 * ( station->stationID() - 1 ) + layer->layerID();

      DetectorSegment seg( 0, layer->globalZ(), layer->dxdy(), layer->dzdy(), 0., 0. );
      float           xmax = 0.5 * layer->sizeX();
      float           ymax = 0.5 * layer->sizeY();

      // The setGeometry defines the z at y=0, the dxDy and the dzDy, as well as the isX properties of the zone.
      // This is important, since these are used in the following.
      // They are set once for each zone in this method.
      m_zoneHandler->MakeZone( 2 * id + 1, seg, -xmax, xmax, -25., ymax ); // Small overlap (25 mm) for stereo layers
      m_zoneHandler->MakeZone( 2 * id, seg, -xmax, xmax, -ymax, 25. );     // Small overlap (25 mm) for stereo layers

      for ( auto quarter : layer->quarters() ) {
        for ( auto module : quarter->modules() ) {
          for ( auto mat : module->mats() ) {
            auto index                     = mat->elementID().uniqueMat();
            m_mats_dxdy[index]             = mat->dxdy();
            m_mats_dzdy[index]             = mat->dzdy();
            m_mats_globaldy[index]         = mat->globaldy();
            m_mats_uBegin[index]           = mat->uBegin();
            m_mats_halfChannelPitch[index] = mat->halfChannelPitch();
            m_mats_dieGap[index]           = mat->dieGap();
            m_mats_sipmPitch[index]        = mat->sipmPitch();
            m_mats_mirrorPoint[index]      = mat->mirrorPoint();
            m_mats_ddx[index]              = mat->ddx();
          }
        }
      }

      //----> Debug zones
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Layer " << id << " z " << m_zoneHandler->zone( 2 * id ).z() << " angle "
                << m_zoneHandler->zone( 2 * id ).dxDy() << endmsg;
      }
      //----> Debug zones
    }
  }

  return StatusCode::SUCCESS;
}

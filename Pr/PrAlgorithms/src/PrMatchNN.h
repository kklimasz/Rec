/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRMATCHNN_H
#define PRMATCHNN_H 1

// Include files
// from Gaudi
#include "Event/Track_v2.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "GaudiKernel/IRegistry.h"

#include "IPrAddUTHitsTool.h"
#include "PrKernel/IPrDebugMatchTool.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"

#include "weights/TMVAClassification_MLPMatching.class.C"

#include <memory>

/** @class PrMatchNN PrMatchNN.h
 *  Match Velo and Seed tracks
 *
 *  @author Michel De Cian (migration to Upgrade)
 *  @date 2013-11-15
 *
 *  @author Olivier Callot
 *  @date   2007-02-07
 */

class PrMatchNN : public Gaudi::Functional::Transformer<std::vector<LHCb::Event::v2::Track>(
                      const std::vector<LHCb::Event::v2::Track>&, const std::vector<LHCb::Event::v2::Track>& )> {
  using Track = LHCb::Event::v2::Track;

public:
  /// Standard constructor
  PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  //  main method
  std::vector<Track> operator()( const std::vector<Track>&, const std::vector<Track>& ) const override;

  /** @class MatchCandidate PrMatchNN.h
   *
   * Match candidate for PrMatcNNh algorithm
   *
   * @author Manuel Schiller
   * @date 2012-01-31
   * 	code cleanups
   *
   * @author Olivier Callot
   * @date   2007-02-07
   * 	initial implementation
   */
  class MatchCandidate {
  public:
    MatchCandidate( const Track* vTr, const Track* sTr, float dist ) : m_vTr( vTr ), m_sTr( sTr ), m_dist( dist ) {}

    const Track* vTr() const { return m_vTr; }
    const Track* sTr() const { return m_sTr; }
    float        dist() const { return m_dist; }

  private:
    const Track* m_vTr = nullptr;
    const Track* m_sTr = nullptr;
    float        m_dist{0};
  };

private:
  /// calculate matching chi^2
  float getChi2Match( const LHCb::State& vState, const LHCb::State& sState,
                      std::array<float, 6>& mLPReaderInput ) const;

  /// merge velo and seed segment to output track
  Track makeTrack( const Track& velo, const Track& seed ) const;

  Gaudi::Property<std::vector<double>> m_zMagParams{
      this, "ZMagnetParams", {5287.6, -7.98878, 317.683, 0.0119379, -1418.42}};
  Gaudi::Property<std::vector<double>> m_momParams{
      this, "MomentumParams", {1.24386, 0.613179, -0.176015, 0.399949, 1.64664, -9.67158}};
  Gaudi::Property<std::vector<double>> m_bendYParams{this, "bendParamYParams", {-347.801, -42663.6}};
  // -- Parameters for matching in y-coordinate
  Gaudi::Property<float> m_zMatchY{this, "zMatchY", 10000. * Gaudi::Units::mm};
  // -- Tolerances
  Gaudi::Property<float> m_dxTol{this, "dxTol", 8. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dxTolSlope{this, "dxTolSlope", 80. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTol{this, "dyTol", 6. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTolSlope{this, "dyTolSlope", 300. * Gaudi::Units::mm};
  Gaudi::Property<float> m_fastYTol{this, "FastYTol", 250.};
  // -- The main cut values
  Gaudi::Property<float> m_maxChi2{this, "MaxMatchChi2", 15.0};
  Gaudi::Property<float> m_minNN{this, "MinMatchNN", 0.25};
  Gaudi::Property<float> m_maxdDist{this, "MaxdDist", 0.10};

  std::unique_ptr<IClassifierReader> m_MLPReader;

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCount{this, "#MatchingTracks"};
  mutable Gaudi::Accumulators::SummingCounter<float>        m_tracksMLP{this, "TracksMLP"};
  mutable Gaudi::Accumulators::SummingCounter<float>        m_tracksChi2{this, "#MatchingChi2"};

  ToolHandle<IPrAddUTHitsTool>       m_addUTHitsTool{this, "AddUTHitsToolName", "PrAddUTHitsTool"};
  ToolHandle<IPrDebugMatchTool>      m_matchDebugTool{this, "MatchDebugToolName", ""};
  ToolHandle<ITrackMomentumEstimate> m_fastMomentumTool{this, "FastMomentumToolName", "FastMomentumEstimate"};

  typedef std::pair<const Track*, const LHCb::State*> TrackStatePair;
  typedef std::vector<TrackStatePair>                 TrackStatePairs;
};

#endif // PRMATCH_H

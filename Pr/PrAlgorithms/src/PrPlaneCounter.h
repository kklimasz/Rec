/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRPLANECOUNTER_H
#define PRPLANECOUNTER_H 1

// Include files
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHit.h"

/** @class PrPlaneCounter PrPlaneCounter.h
 *  Small class to count how many different planes are in a list and how many planes with a single hit fired
 *
 *  @author Olivier Callot
 *  @date   2012-03-23
 *  @author Michel De Cian
 *  @date   2014-03-12 Added number of planes which only have a single hit
 *  @author Thomas Nikodem
 *  @date   2016-03-04
 *
 */

class PrPlaneCounter final {
public:
  /** Update values for additional hit
   *  @param hit Hit to be added
   *  @return int number of different planes
   */
  void addHit( const ModPrHit& hit ) { m_nbDifferent += ( ++m_planeList[hit.planeCode] == 1 ); }
  void addHit( const PrHit& hit ) { m_nbDifferent += ( ++m_planeList[hit.planeCode()] == 1 ); }

  /** Update values for removed hit
   *  @param hit Hit to be removed
   *  @return int number of different planes
   */
  void removeHit( const ModPrHit& hit ) { m_nbDifferent -= ( --m_planeList[hit.planeCode] == 0 ); }
  void removeHit( const PrHit& hit ) { m_nbDifferent -= ( --m_planeList[hit.planeCode()] == 0 ); }

  /** Set values (fired planes, single planes, different planes) for a given range of iterators
   *  @brief Set values for a given range of iterators
   *  @param itBeg First iterator, begin of range
   *  @param itEnd Last iterator, end of range
   */
  void set( PrHits::const_iterator it, PrHits::const_iterator itEnd ) {
    for ( ; itEnd != it; ++it ) addHit( **it );
  }
  void set( const PrHits& hits ) {
    for ( const auto& hit : hits ) addHit( *hit );
  }

  /// returns number of different planes
  unsigned int nbDifferent() const { return m_nbDifferent; }

  /// returns number of hits in specified plane
  int nbInPlane( const int plane ) const { return m_planeList[plane]; }
  int nbInPlane( const ModPrHit& hit ) const { return m_planeList[hit.planeCode]; }

  /// returns number of single planes
  // TODO make this smarter?
  unsigned int nbSingle() const {
    return std::count_if( m_planeList.begin(), m_planeList.end(), []( int n ) { return n == 1; } );
  }

  /// clear list with hits in planes and number of different planes / single-hit planes
  void clear() {
    m_nbDifferent = 0;
    m_planeList.fill( 0 );
  }

private:
  /// array for: number of different plane (0) and number of planes with single hit (1)
  unsigned int                            m_nbDifferent = 0;
  std::array<int, PrFTInfo::NFTZones / 2> m_planeList   = {};
};

#endif // PRPLANECOUNTER_H

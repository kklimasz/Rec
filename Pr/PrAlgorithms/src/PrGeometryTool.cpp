/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PrGeometryTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrGeometryTool
//
// 2012-03-22 : Olivier Callot
//-----------------------------------------------------------------------------

#include <numeric>
namespace {
  template <typename Coefficients, typename Parameters>
  auto inner_product( const Coefficients& c, const Parameters& p ) {
    assert( c.size() == p.size() + 1 );
    return std::inner_product( begin( p ), end( p ), std::next( begin( c ) ), c.front() );
  }
} // namespace

// Declaration of the Tool Factory
DECLARE_COMPONENT( PrGeometryTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrGeometryTool::PrGeometryTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<PrGeometryTool>( this );
}

//=========================================================================
//  Initialization
//=========================================================================
StatusCode PrGeometryTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isSuccess() ) m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  return sc;
}

//=========================================================================
//  Returns the best momentum estimate for a seed: assumes it comes from (0,0,0)
//  and use the same parameterisation.
//=========================================================================
float PrGeometryTool::qOverP( const Pr::Hybrid::SeedTrack& track ) const {
  float qop( 1.0 / Gaudi::Units::GeV );
  float magscalefactor = m_magFieldSvc->signedRelativeCurrent();
  if ( std::abs( magscalefactor ) > 1e-6 ) {
    float bx  = track.xSlope( 0. );
    float bx2 = bx * bx;
    //== Compute the slopes before the magnet: Assume the track comes from (0,0,0) and
    //== crosses the T station part at zMagnet
    float zMagnet  = ( m_zMagnetParams[0] );
    float xMagnet  = track.x( zMagnet );
    float yMagnet  = track.y( zMagnet );
    float slXFront = xMagnet / zMagnet;
    float slYFront = yMagnet / zMagnet;
    float slX2     = slXFront * slXFront;
    float slY2     = slYFront * slYFront;
    float dSlope   = slXFront - bx;
    //== Iterate as ZMagnet depends on the slope before...
    zMagnet  = inner_product( m_zMagnetParams.value(), std::array{dSlope * dSlope, slX2, slY2} );
    xMagnet  = track.x( zMagnet );
    yMagnet  = track.y( zMagnet );
    slXFront = xMagnet / zMagnet;
    slYFront = yMagnet / zMagnet;
    slX2     = slXFront * slXFront;
    slY2     = slYFront * slYFront;
    float coef =
        inner_product( m_momentumParams.value(), std::array{bx2, bx2 * bx2, bx * slXFront, slY2, slX2 * slY2} );

    float proj = sqrt( ( 1. + slX2 + slY2 ) / ( 1. + slX2 ) );
    qop        = dSlope / ( coef * Gaudi::Units::GeV * proj * magscalefactor );
  }
  return qop;
}

//=========================================================================
//  Returns the best momentum estimate
//=========================================================================
float PrGeometryTool::qOverP( const Track& vTr, const Track& sTr ) const {

  float zMagnet = ( m_zMagnetParams[0] );

  float qop( 1.0 / Gaudi::Units::GeV );
  float magscalefactor = m_magFieldSvc->signedRelativeCurrent();
  if ( std::abs( magscalefactor ) > 1e-6 ) {

    const LHCb::State& vState = vTr.closestState( 0. );
    const LHCb::State& sState = sTr.closestState( zMagnet );

    float txT    = sState.tx();
    float txV    = vState.tx();
    float tyV    = vState.ty();
    float txV2   = txV * txV;
    float tyV2   = tyV * tyV;
    float dSlope = txV - txT;

    //== Iterate as ZMagnet depends on the slope before...
    zMagnet = inner_product( m_zMagnetParams.value(), std::array{dSlope * dSlope, txV2, tyV2} );

    const LHCb::State& sState2 = sTr.closestState( zMagnet );
    txT                        = sState2.tx();
    float txT2                 = txT * txT;

    float coef = inner_product( m_momentumParams.value(), std::array{txT2, txT2 * txT2, txT * txV, tyV2, tyV2 * tyV2} );

    float proj = sqrt( ( 1. + txV2 + tyV2 ) / ( 1. + txV2 ) );
    qop        = ( txV - txT ) / ( coef * Gaudi::Units::GeV * proj * magscalefactor );
  }
  return qop;
}

//=========================================================================
//  Default covariance matrix: Large errors as input to Kalman fitter.
//=========================================================================
Gaudi::TrackSymMatrix PrGeometryTool::covariance( float qOverP ) const {
  Gaudi::TrackSymMatrix cov;
  cov( 0, 0 ) = m_covarianceValues[0];
  cov( 1, 1 ) = m_covarianceValues[1];
  cov( 2, 2 ) = m_covarianceValues[2];
  cov( 3, 3 ) = m_covarianceValues[3];
  cov( 4, 4 ) = m_covarianceValues[4] * qOverP * qOverP;
  return cov;
}
//=============================================================================

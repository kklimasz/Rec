/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "PrMatchNN.h"
#include "Event/StateParameters.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrMatchNN
//
// 2013-11-15 : Michel De Cian, migration to Upgrade
//
// 2007-02-07 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrMatchNN )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrMatchNN::PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"VeloInput", LHCb::TrackLocation::Velo}, KeyValue{"SeedInput", LHCb::TrackLocation::Seed}},
                   KeyValue{"MatchOutput", LHCb::TrackLocation::Match} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrMatchNN::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

  // Input variable names for TMVA
  // they are only needed to make sure same variables are used in MVA
  // we can remove them once the algorithm is final
  std::array<std::string, 6> inputVars = {"var0", "var1", "var2", "var3", "var4", "var5"};

  m_MLPReader = std::make_unique<ReadMLPMatching>( inputVars );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<PrMatchNN::Track> PrMatchNN::operator()( const std::vector<PrMatchNN::Track>& velos,
                                                     const std::vector<PrMatchNN::Track>& seeds ) const {
  std::vector<Track> matches;
  matches.reserve( 200 );

  if ( velos.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Track container '" << inputLocation<0>() << "' is empty" << endmsg;
    return matches;
  }

  if ( seeds.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Track container '" << inputLocation<1>() << "' is empty" << endmsg;
    return matches;
  }

  std::vector<MatchCandidate> cands;
  cands.reserve( seeds.size() );

  std::array<float, 6> mLPReaderInput = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  // -- make pairs of Velo track and state
  // -- TrackStatePair is std::pair<const Track*, const LHCb::State*>
  // -- TrackStatePairs is std::vector<TrackStatePair>
  // -- typedef in header file
  TrackStatePairs veloPairs;
  veloPairs.reserve( velos.size() );

  for ( auto const& vTr : velos ) {
    if ( vTr.checkFlag( Track::Flag::Invalid ) ) continue;
    if ( vTr.checkFlag( Track::Flag::Backward ) ) continue;
    const LHCb::State* vState = vTr.stateAt( LHCb::State::Location::EndVelo );
    assert( vState != nullptr );
    veloPairs.emplace_back( &vTr, vState );
  }

  // -- sort according to approx y position
  // -- We don't know deltaSlope, so we just extrapolate linearly
  std::sort( veloPairs.begin(), veloPairs.end(), [&]( const TrackStatePair& sP1, const TrackStatePair& sP2 ) {
    const float posA = sP1.second->y() + ( 0.0 - sP1.second->z() ) * sP1.second->ty();
    const float posB = sP2.second->y() + ( 0.0 - sP2.second->z() ) * sP2.second->ty();
    return posA < posB;
  } );

  // -- make pairs of Seed track and state
  TrackStatePairs seedPairs;
  seedPairs.reserve( seeds.size() );

  for ( auto const& sTr : seeds ) {
    if ( sTr.checkFlag( Track::Flag::Invalid ) ) continue;
    const LHCb::State& sState = sTr.closestState( m_zMatchY );
    seedPairs.emplace_back( &sTr, &sState );
  }

  // -- sort according to approx y position
  std::sort( seedPairs.begin(), seedPairs.end(), [&]( const TrackStatePair& sP1, const TrackStatePair& sP2 ) {
    const float posA = sP1.second->y() + ( m_zMatchY - sP1.second->z() ) * sP1.second->ty();
    const float posB = sP2.second->y() + ( m_zMatchY - sP2.second->z() ) * sP2.second->ty();
    return posA < posB;
  } );

  auto mlpCounterBuf  = m_tracksMLP.buffer();
  auto chi2CounterBuf = m_tracksChi2.buffer();
  for ( auto const& vP : veloPairs ) {
    cands.clear();

    const float posYApproxV = vP.second->y() + ( m_zMatchY - vP.second->z() ) * vP.second->ty();
    // -- The TrackStatePairs are sorted according to the approximate extrapolated y position
    // -- We can use a binary search to find the starting point from where we need to calculate the chi2
    // -- The tolerance should be large enough such that it is essentially losseless, but speeds things up
    // significantly.
    auto it = std::lower_bound(
        seedPairs.begin(), seedPairs.end(), m_fastYTol, [&]( const TrackStatePair& sP, const float tol ) {
          const float posYApproxS = sP.second->y() + ( m_zMatchY - sP.second->z() ) * sP.second->ty();
          return posYApproxS < posYApproxV - tol;
        } );

    // -- The loop to calculate the chi2 between Velo and Seed track
    for ( ; it < seedPairs.end(); ++it ) {
      TrackStatePair sP = *it;

      // -- Stop the loop at the upper end of the tolerance interval
      const float posYApproxS = sP.second->y() + ( m_zMatchY - sP.second->z() ) * sP.second->ty();
      if ( posYApproxS > posYApproxV + m_fastYTol ) break;

      const float chi2 = getChi2Match( *vP.second, *sP.second, mLPReaderInput );

      if ( m_matchDebugTool.isEnabled() ) {
        std::vector<float> v( std::begin( mLPReaderInput ), std::end( mLPReaderInput ) );
        /// TODO: This needs to be updated with Track_v2 (PrMCTools/src/PrDebugMatchTool.{h,cpp} and
        /// PrKernel/PrKernel/IPrDebugMatchTool.h)
        // m_matchDebugTool->fillTuple( *vP.first, *sP.first, v );
      }

      if ( chi2 < m_maxChi2 ) {
        const float mlp = m_MLPReader->GetMvaValue( mLPReaderInput );
        mlpCounterBuf += mlp;
        chi2CounterBuf += chi2;
        if ( mlp > m_minNN ) cands.emplace_back( vP.first, sP.first, mlp );
      }
    }

    std::sort( cands.begin(), cands.end(),
               []( const MatchCandidate& lhs, const MatchCandidate& rhs ) { return lhs.dist() > rhs.dist(); } );

    // convert unused match candidates to tracks
    for ( const MatchCandidate& cand : cands ) {

      if ( cands[0].dist() - cand.dist() > m_maxdDist ) break;

      const Track* vTr = cand.vTr();
      const Track* sTr = cand.sTr();

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << " Candidate"
                << " Seed  chi2 " << cand.dist() << endmsg;
      }

      auto& match = matches.emplace_back( makeTrack( *vTr, *sTr ) );

      if ( m_addUTHitsTool.isEnabled() ) {
        StatusCode sc = m_addUTHitsTool->addUTHits( match );
        if ( sc.isFailure() ) Warning( "adding UT clusters failed!", sc ).ignore();
      }
    } // end loop match cands
  }   // end loop velo tracks

  m_tracksCount += matches.size();
  return matches;
}
//=============================================================================
//

float PrMatchNN::getChi2Match( const LHCb::State& vState, const LHCb::State& sState,
                               std::array<float, 6>& mLPReaderInput ) const {
  const float tx2 = vState.tx() * vState.tx();
  const float ty2 = vState.ty() * vState.ty();

  const float dSlope = vState.tx() - sState.tx();
  if ( std::abs( dSlope ) > 1.5 ) return 99.;

  const float dSlopeY = vState.ty() - sState.ty();
  if ( std::abs( dSlopeY ) > 0.15 ) return 99.;

  const float zForX = m_zMagParams[0] + m_zMagParams[1] * std::abs( dSlope ) + m_zMagParams[2] * dSlope * dSlope +
                      m_zMagParams[3] * std::abs( sState.x() ) + m_zMagParams[4] * vState.tx() * vState.tx();

  const float dxTol2      = m_dxTol * m_dxTol;
  const float dxTolSlope2 = m_dxTolSlope * m_dxTolSlope;

  const float xV = vState.x() + ( zForX - vState.z() ) * vState.tx();
  // -- This is the function that calculates the 'bending' in y-direction
  // -- The parametrisation can be derived with the MatchFitParams package
  const float yV = vState.y() + ( m_zMatchY - vState.z() ) * vState.ty() +
                   vState.ty() * ( m_bendYParams[0] * dSlope * dSlope + m_bendYParams[1] * dSlopeY * dSlopeY );

  const float xS = sState.x() + ( zForX - sState.z() ) * sState.tx();
  const float yS = sState.y() + ( m_zMatchY - sState.z() ) * sState.ty();

  const float distX = xS - xV;
  if ( std::abs( distX ) > 400 ) return 99.;
  const float distY = yS - yV;
  if ( std::abs( distX ) > 250 ) return 99.;

  const float teta2 = tx2 + ty2;
  const float tolX  = dxTol2 + dSlope * dSlope * dxTolSlope2;
  const float tolY  = m_dyTol * m_dyTol + teta2 * m_dyTolSlope * m_dyTolSlope;

  float chi2 = distX * distX / tolX + distY * distY / tolY;

  // chi2 += dslY * dslY / sState.errTy2() / 16.;
  chi2 += dSlopeY * dSlopeY * 10000 * 0.0625;

  if ( m_maxChi2 < chi2 ) return chi2;

  mLPReaderInput[0] = chi2;
  mLPReaderInput[1] = teta2;
  mLPReaderInput[2] = std::abs( distX );
  mLPReaderInput[3] = std::abs( distY );
  mLPReaderInput[4] = std::abs( dSlope );
  mLPReaderInput[5] = std::abs( dSlopeY );

  return chi2;
}

PrMatchNN::Track PrMatchNN::makeTrack( const PrMatchNN::Track& velo, const PrMatchNN::Track& seed ) const {
  auto output = Track{};
  output.addToAncestors( velo );
  output.addToAncestors( seed );
  //== Adjust flags
  output.setType( Track::Type::Long );
  output.setHistory( Track::History::PrMatch );
  output.setPatRecStatus( Track::PatRecStatus::PatRecIDs );
  //== copy LHCbIDs
  output.addToLhcbIDs( velo.lhcbIDs(), LHCb::Tag::Sorted );
  output.addToLhcbIDs( seed.lhcbIDs(), LHCb::Tag::Sorted );
  //== copy Velo and T states at the usual pattern reco positions
  std::vector<LHCb::State> newstates;
  newstates.reserve( 6 );
  if ( velo.hasStateAt( LHCb::State::Location::ClosestToBeam ) )
    newstates.push_back( *velo.stateAt( LHCb::State::Location::ClosestToBeam ) );
  if ( velo.hasStateAt( LHCb::State::Location::FirstMeasurement ) )
    newstates.push_back( *velo.stateAt( LHCb::State::Location::FirstMeasurement ) );
  if ( velo.hasStateAt( LHCb::State::Location::EndVelo ) )
    newstates.push_back( *velo.stateAt( LHCb::State::Location::EndVelo ) );
  newstates.push_back( seed.closestState( StateParameters::ZBegT ) );
  newstates.push_back( seed.closestState( StateParameters::ZMidT ) );
  // make sure we don't include same state twice
  if ( std::abs( newstates[newstates.size() - 2].z() - newstates.back().z() ) < 300. ) { newstates.pop_back(); }
  newstates.push_back( seed.closestState( StateParameters::ZEndT ) );
  // make sure we don't include same state twice
  if ( std::abs( newstates[newstates.size() - 2].z() - newstates.back().z() ) < 300. ) { newstates.pop_back(); }

  //== estimate q/p
  double             qOverP, sigmaQOverP;
  bool const         cubicFit = seed.checkHistory( Track::History::PrSeeding );
  const LHCb::State& vState   = velo.closestState( 0. );
  const LHCb::State& sState   = seed.closestState( m_zMatchY );
  StatusCode         sc       = m_fastMomentumTool->calculate( &vState, &sState, qOverP, sigmaQOverP, cubicFit );
  if ( sc.isFailure() ) {
    Warning( "momentum determination failed!", sc ).ignore();
    // assume the Velo/T station standalone reco do something reasonable
  } else {
    // adjust q/p and its uncertainty
    sigmaQOverP = sigmaQOverP * sigmaQOverP;
    for ( auto& st : newstates ) {
      st.covariance()( 4, 4 ) = sigmaQOverP;
      st.setQOverP( qOverP );
    }
  }
  //== add copied states to output track
  output.addToStates( newstates, LHCb::Tag::Unordered );
  return output;
}

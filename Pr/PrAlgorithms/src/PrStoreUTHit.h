/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <memory>
#include <numeric>
#include <tuple>

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTTELL1BoardErrorBank.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/UTClusterWord.h"
#include "Kernel/UTDecoder.h"
#include "PrKernel/UTHitHandler.h"
#include "PrKernel/UTHitInfo.h"
#include "UTDAQ/UTReadoutTool.h"
#include "UTDet/DeUTDetector.h"

namespace LHCb::Pr {

  namespace {
    constexpr int NBoards         = 240;
    constexpr int NSectorPerBoard = 6;
  } // namespace

  class StoreUTHit : public Gaudi::Functional::Transformer<UT::HitHandler( const RawEvent& )> {
  public:
    StoreUTHit( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;

    UT::HitHandler operator()( const RawEvent& ) const override;

  private:
    std::vector<unsigned int>               missingInAction( span<const RawBank*> banks ) const;
    unsigned int                            pcnVote( span<const RawBank*> banks ) const;
    StatusCode                              decodeBanks( const RawEvent& rawEvt, UT::HitHandler& fCont ) const;
    std::unique_ptr<UTTELL1BoardErrorBanks> decodeErrors( const RawEvent& raw ) const;
    bool canBeRecovered( const UTTELL1BoardErrorBank* bank, const UTClusterWord& word, const unsigned int pcn ) const;
    bool checkDataIntegrity( UTDecoder& decoder, const UTTell1Board* aBoard, const unsigned int bankSize,
                             const UTDAQ::version& bankVersion ) const;

  private:
    using DecoderData =
        std::tuple<UTDAQ::version, UTTell1Board*, const RawBank*, bool, UTTELL1BoardErrorBank*, unsigned int>;

  private:
    DeUTDetector* m_utDet = nullptr;

    /** Faster access to sectors **/
    std::array<DeUTSector*, NBoards * NSectorPerBoard>                     m_sectors_cache;
    std::array<UTTell1Board::ExpandedChannelID, NBoards * NSectorPerBoard> m_fullchan_cache;

    ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_lostBanks{this, "lost Banks"};
    mutable Gaudi::Accumulators::Counter<>                    m_noBanksFound{this, "no banks found"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_skippedBanks{this, "skipped Banks"};
    mutable Gaudi::Accumulators::Counter<>                    m_validBanks{this, "# valid banks"};
    mutable Gaudi::Accumulators::Counter<>                    m_validSourceID{this, "# valid source ID"};
    mutable Gaudi::Accumulators::Counter<>                    m_eventsWithError{this, "events with error banks"};
    mutable Gaudi::Accumulators::Counter<>                    m_skippedBanksCounter{this, "events with error banks"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_errorBanksCounter{this, "skipped Banks"};

    Gaudi::Property<bool> m_skipErrors{this, "skipBanksWithErrors", false};
    Gaudi::Property<bool> m_recoverMode{this, "recoverMode", true};
  };
} // namespace LHCb::Pr

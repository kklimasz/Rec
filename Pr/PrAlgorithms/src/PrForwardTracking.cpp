/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "Event/StateParameters.h"
#include "Event/Track.h"

// local
#include "PrForwardTracking.h"
#include "PrKernel/PrFTInfo.h"

// event
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"

// boost
#include "boost/container/static_vector.hpp"

// range v3
#include "range/v3/algorithm/find_if.hpp"
#include "range/v3/numeric/accumulate.hpp"
#include "range/v3/version.hpp"
#include "range/v3/view.hpp"
// upstream has renamed namespace ranges::view ranges::views
#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

// Vc
#include <Vc/Vc>

// STL
#include <algorithm>
#include <cassert>
#include <functional>
#include <optional>

// *********************************************************************************
// ************************ Introduction to Forward Tracking **********************
// *********************************************************************************
//
//  A detailed introduction in Forward tracking (with real pictures!) can be
//  found here:
//  (2002) http://cds.cern.ch/record/684710/files/lhcb-2002-008.pdf
//  (2007) http://cds.cern.ch/record/1033584/files/lhcb-2007-015.pdf
//  (2014) http://cds.cern.ch/record/1641927/files/LHCb-PUB-2014-001.pdf
//
// *** Short Introduction in geometry:
//
// The SciFi Tracker Detector, or simple Fibre Tracker (FT) consits out of 3 stations.
// Each station consists out of 4 planes/layers. Thus there are in total 12 layers,
// in which a particle can leave a hit. The reasonable maximum number of hits a track
// can have is thus also 12 (sometimes 2 hits per layer are picked up).
//
// Each layer consists out of several Fibre mats. A fibre has a diameter of below a mm.(FIXME)
// Several fibres are glued alongside each other to form a mat.
// A Scintilating Fibre produces light, if a particle traverses. This light is then
// detected on the outside of the Fibre mat.
//
// Looking from the collision point, one (X-)layer looks like the following:
//
//                    y       6m
//                    ^  ||||||||||||| Upper side
//                    |  ||||||||||||| 2.5m
//                    |  |||||||||||||
//                   -|--||||||o||||||----> -x
//                       |||||||||||||
//                       ||||||||||||| Lower side
//                       ||||||||||||| 2.5m
//
// All fibres are aranged parallel to the y-axis. There are three different
// kinds of layers, denoted by X,U,V. The U/V layers are rotated with respect to
// the X-layers by +/- 5 degrees, to also get a handle of the y position of the
// particle. As due to the magnetic field particles are only deflected in
// x-direction, this configuration offers the best resolution.
// The layer structure in the FT is XUVX-XUVX-XUVX.
//
// The detector is divided into an upeer and a lower side (>/< y=0). As particles
// are only deflected in x direction there are only very(!) few particles that go
// from the lower to the upper side, or vice versa. The reconstruction algorithm
// can therefore be split into two independent steps: First track reconstruction
// for tracks in the upper side, and afterwards for tracks in the lower side.
//
// Due to construction issues this is NOT true for U/V layers. In these layers the
// complete(!) fibre modules are rotated, producing a zic-zac pattern at y=0, also
// called  "the triangles". Therefore for U/V layers it must be explicetly also
// searched for these hit on the "other side", if the track is close to y=0.
// Sketch (rotation exagerated!):
//                                          _.*
//     y ^   _.*                         _.*
//       | .*._      Upper side       _.*._
//       |     *._                 _.*     *._
//       |--------*._           _.*           *._----------------> x
//       |           *._     _.*                 *._     _.*
//                      *._.*       Lower side      *._.*
//
//
//
//
//
//       Zone ordering defined on PrKernel/PrFTInfo.h
//
//     y ^
//       |    1  3  5  7     9 11 13 15    17 19 21 23
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    x  u  v  x     x  u  v  x     x  u  v  x   <-- type of layer
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |------------------------------------------------> z
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    0  2  4  6     8 10 12 14    16 18 20 22
//
//
// *** Short introduction in the Forward Tracking algorithm
//
// The track reconstruction is seperated into several steps:
//
// 1) Using only X-hits
//    1.1) Preselection: collectAllXHits()
//    1.2) Hough Transformation: xAtRef_SamePlaneHits()
//    1.3) Cluster search: selectXCandidates()
//    1.4) Linear and than Cubic Fit of X-Projection
// 2) Introducing U/V hits or also called stereo hits
//    2.1) Preselection: collectStereoHits
//    2.2) Cluster search: selectStereoHits
//    2.3) Fit Y-Projection
// 3) Using all (U+V+X) hits
//    3.1) Fitting X-Projection
//    3.2) calculating track quality with a Neural Net
//    3.3) final clone+ghost killing
//
// *****************************************************************

//-----------------------------------------------------------------------------
// Implementation file for class : PrForwardTracking
//
// 2012-03-20 : Olivier Callot
// 2013-03-15 : Thomas Nikodem
// 2015-02-13 : Sevda Esen [additional search in the triangles by Marian Stahl]
// 2016-03-09 : Thomas Nikodem [complete restructuring]
// 2018-11-07 : Olli Lupton [merged PrForwardTool and PrForwardTracking]
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory

DECLARE_COMPONENT_WITH_ID( PrForwardTracking<LHCb::Pr::Upstream::Tracks>, "PrForwardTracking" )
DECLARE_COMPONENT_WITH_ID( PrForwardTracking<LHCb::Pr::Velo::Tracks>, "PrForwardTrackingVelo" )

namespace {
  using dType = SIMDWrapper::scalar::types;
  using F     = dType::float_v;
  using I     = dType::int_v;

  template <typename T, typename Predicate>
  auto drop_while( LHCb::span<T> s, Predicate&& p ) {
    return s.subspan(
        std::distance( s.begin(), std::find_if_not( s.begin(), s.end(), std::forward<Predicate>( p ) ) ) );
  }

  template <typename T>
  std::tuple<LHCb::Pr::Velo::Tracks const*, LHCb::Pr::Upstream::Tracks const*> get_ancestors( T const& input_tracks ) {
    LHCb::Pr::Velo::Tracks const*     velo_ancestors     = nullptr;
    LHCb::Pr::Upstream::Tracks const* upstream_ancestors = nullptr;
    if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
      velo_ancestors     = input_tracks.getVeloAncestors();
      upstream_ancestors = &input_tracks;
    } else {
      velo_ancestors     = &input_tracks;
      upstream_ancestors = nullptr;
    }
    return {velo_ancestors, upstream_ancestors};
  }

  template <typename Range, typename Projection, typename Comparison, typename Value>
  auto best_element( Range&& range, Projection&& proj, Comparison&& cmp, Value best ) {
    using Element = std::pair<std::decay_t<Value>, std::add_const_t<ranges::range_value_type_t<Range>>*>;
    return ranges::accumulate(
        std::forward<Range>( range ), Element{std::move( best ), nullptr},
        [proj = std::forward<Projection>( proj ), cmp = std::forward<Comparison>( cmp )]( Element b, const auto& e ) {
          auto v = std::invoke( proj, e );
          if ( std::invoke( cmp, v, b.first ) ) {
            b.first  = std::move( v );
            b.second = &e;
          }
          return b;
        } );
  }

  // Input variable names for TMVA NeuralNet
  const std::vector<std::string> mlpInputVars{{"nPlanes"}, {"dSlope"}, {"dp"}, {"slope2"}, {"dby"}, {"dbx"}, {"day"}};

  // original PrGeometry Params
  constexpr auto zMagnetParams  = std::array{5212.38f, 406.609f, -1102.35f, -498.039f};
  constexpr auto xParams        = std::array{18.6195f, -5.55793f};
  constexpr auto byParams       = std::array{-0.667996f};
  constexpr auto cyParams       = std::array{-3.68424e-05f};
  constexpr auto momentumParams = std::array{1.21014f, 0.637339f, -0.200292f, 0.632298f, 3.23793f, -27.0259f};
  //  constexpr auto m_covarianceValues = std::array{ 4.0f,
  //                                                        400.0f, // ErrX = 2mm
  //                                                        4.e-6f, // ErrSlX = 2 mrad
  //                                                        1.e-4f, // ErrSlY = 10 mrad
  //                                                        0.1f};  // errQQoverP = 10% of qOverP
  constexpr float zReference              = 8520.f;
  constexpr float zRefInv                 = 1.f / zReference;
  constexpr int   numberallXHitsAfterCuts = 2500;

  // parameters for extrapolating from EndVelo to ZReference
  constexpr auto xExtParams = std::array{4.08934e+06f, 6.31187e+08f, 131.999f, -1433.64f, -325.055f, 3173.52f};
  // Params for momentum dependent search window estimate
  constexpr auto pUp =
      std::array{1.46244e+02f, 5.15348e+02f, -4.17237e-05f}; // upper window to include 98% of hits(can't be too greedy
                                                             // here or the window size would explode)
  constexpr auto pLo =
      std::array{5.00000e+01f, 9.61409e+02f, -1.31317e-04f}; // lower window, the same to include 98% of hits

  // -- Calculate window size based on minimum PT and slope (= minimum p)
  float calcDxRef( const VeloSeed& seed, const float pt ) {
    return 3973000.f * std::sqrt( seed.slope2 ) / pt - 2200.f * seed.ty2 - 1000.f * seed.tx2; // tune this window
  }

  // find matching stereo hit if available
  bool matchStereoHit( LHCb::span<const PrHit>& range, float xMinUV, float xMaxUV ) {
    assert( xMinUV <= xMaxUV );
    // search for same side UV hit
    range = drop_while( range, [=]( const PrHit& h ) { return h.x() < xMinUV; } );
    return !range.empty() && range[0].x() < xMaxUV;
  }
  // find matching stereo hit if available, also searching in triangle region
  template <PrHitZone::Side SIDE>
  bool matchStereoHitWithTriangle( LHCb::span<const PrHit>& range, float yInZone, float xMinUV, float xMaxUV,
                                   float tolerance ) {
    assert( xMinUV <= xMaxUV );
    assert( std::is_sorted( range.begin(), range.end(), []( const auto a, const auto b ) { return a.x() < b.x(); } ) );

    auto match = [&range, xMaxUV]( auto predicate ) {
      while ( !range.empty() && range[0].x() < xMaxUV ) {
        if ( predicate( range[0] ) ) return true;
        range = range.subspan( 1 );
      }
      return false;
    };

    // search for opposite side UV hit
    range = drop_while( range, [=]( const PrHit& h ) { return h.x() < xMinUV; } );
    if constexpr ( SIDE == PrHitZone::Side::Upper ) {
      // test lower layer, thus only ymax
      return match( [yy = yInZone - tolerance]( const PrHit& h ) { return h.yMax() > yy; } );
    } else if constexpr ( SIDE == PrHitZone::Side::Lower ) {
      // test upper layer, thus only ymin
      return match( [yy = yInZone + tolerance]( const PrHit& h ) { return h.yMin() < yy; } );
    }
  }
  //=========================================================================
  //  Returns estimate of magnet kick position
  //=========================================================================
  float zMagnet( const PrForwardTrack& track ) {
    return ( zMagnetParams[0] + zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2 );
  }
  //=========================================================================
  //  Returns the best momentum estimate
  //=========================================================================
  float calcqOverP( const PrForwardTrack& track, const float magScaleFactor ) {
    constexpr float inv_gev = 1.0 / Gaudi::Units::GeV;
    float           qop     = inv_gev;
    if ( std::abs( magScaleFactor ) > 1e-6f ) {
      const float bx  = track.xSlope( zReference );
      const float bx2 = bx * bx;
      const auto  obs = {bx2, bx2 * bx2, bx * track.seed().tx, track.seed().ty2, track.seed().ty2 * track.seed().ty2};
      const float coef =
          std::inner_product( begin( obs ), end( obs ), std::next( begin( momentumParams ) ), momentumParams[0] );
      const float proj = std::sqrt( ( 1.f + track.seed().slope2 ) / ( 1.f + track.seed().tx2 ) );
      qop *= ( track.seed().tx - bx ) / ( coef * proj * magScaleFactor );
    }
    return qop;
  }
  namespace details {
    template <int first, int last, typename Iterators>
    void inplace_merge( const Iterators& iters ) {
      static_assert( last >= first );
      if constexpr ( first == last || last == first + 1 ) return;
      constexpr int pivot = first + ( 1 + last - first ) / 2;
      inplace_merge<first, pivot>( iters );
      inplace_merge<pivot, last>( iters );
      std::inplace_merge( iters[first], iters[pivot], iters[last] );
    }
  } // namespace details

  //=========================================================================
  //  Merge the layers of x hits for the hough transform
  //=========================================================================
  template <int N, typename Container>
  void mergeSortedRanges( Container& allXHits, LHCb::span<int, N> boundaries ) {
    static_assert( N > 1 );
    std::array<typename Container::iterator, N> offset;
    using std::begin;
    using std::end;
    std::transform( begin( boundaries ), end( boundaries ), begin( offset ),
                    [start = begin( allXHits )]( int b ) { return start + b; } );
    details::inplace_merge<0, N - 1>( offset );
  }

  template <typename Iterator>
  void setHitsUsed( Iterator first, Iterator last, const PrForwardTrack& track, const PrParameters& pars ) {
    // Hits before it1 are not checked. Thus this method does not work perfect. However, it seems good enough :)
    assert( first <= last );

    const auto& xPars = track.getXParams();
    const float x1    = xPars.get( 0 ) + 2 * pars.maxXWindow;

    while ( first != last ) {
      first = std::find_if( first, last, []( const auto& h ) { return h.isValid(); } );
      if ( first == last || first->coord > x1 ) break;
      // search hit in track
      const auto& hits = track.hits();
      auto        it   = std::find( hits.begin(), hits.end(), first->hit );
      if ( it != hits.end() ) first->setInvalid(); // coord == max means the same as hit is deleted
      ++first;
    }
  }

  //=========================================================================
  //  Set the parameters of the track, from the (average) x at reference
  //=========================================================================
  void setTrackParameters( PrForwardTrack& track, const float xAtRef ) {

    float       dSlope    = ( track.seed().x( zReference ) - xAtRef ) / ( zReference - zMagnetParams[0] );
    const float zMagSlope = zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2;
    const float zMag      = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
    assert( zMag != zReference && "zMag can not be equal to zReference" );
    const float xMag   = track.seed().x( zMag );
    const float slopeT = ( xAtRef - xMag ) / ( zReference - zMag );
    dSlope             = slopeT - track.seed().tx;
    const float dyCoef = dSlope * dSlope * track.seed().ty;

    track.setParams( {xAtRef, slopeT, 1.e-6f * xParams[0] * dSlope, 1.e-9f * xParams[1] * dSlope,
                      track.seed().y( zReference ), track.seed().ty + dyCoef * byParams[0], dyCoef * cyParams[0]} );
  }

  //=========================================================================
  //  Compute the x projection at the reference plane
  //=========================================================================
  void xAtRef_SamePlaneHits( const PrForwardTrack& track, std::vector<ModPrHit>::iterator itH,
                             const std::vector<ModPrHit>::iterator itEnd ) {
    // calculate xref for this plane
    const float zHit = itH->hit->z(); // all hits in same layer
    // const float yHit    = track.yFromVelo( zHit );
    const float xFromVelo_Hit = track.seed().x( zHit );
    const float zMagSlope     = zMagnetParams[2] * track.seed().tx2 + zMagnetParams[3] * track.seed().ty2;

    const float tx            = track.seed().tx;
    const float dSlopeDivPart = 1.f / ( zHit - zMagnetParams[0] );
    const float dz            = 1.e-3f * ( zHit - zReference );

    // Vector of float
    Vc::float_v xHits{Vc::Zero};

    while ( itH < itEnd ) {
      auto itH2 = itH;
      for ( unsigned int i = 0; i < Vc::float_v::Size && itH2 != itEnd; ++i, ++itH2 ) { xHits[i] = itH2->coord; }

      const Vc::float_v dSlope = ( xFromVelo_Hit - xHits ) * dSlopeDivPart;
      const Vc::float_v zMag   = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
      const Vc::float_v xMag   = xFromVelo_Hit + tx * ( zMag - zHit );
      const Vc::float_v dxCoef = dz * dz * ( xParams[0] + dz * xParams[1] ) * dSlope;
      const Vc::float_v ratio  = ( zReference - zMag ) / ( zHit - zMag );
      const Vc::float_v x      = xMag + ratio * ( xHits + dxCoef - xMag );
      xHits                    = x;

      for ( unsigned int i = 0; i < Vc::float_v::Size && itH != itEnd; ++i, ++itH ) { itH->coord = xHits[i]; }
    }
  }

  struct TimerGuard {
    ISequencerTimerTool* t = nullptr;
    int                  n = 0;
    TimerGuard( ISequencerTimerTool& t_, int n_ ) : t{&t_}, n{n_} { t->start( n ); }
    ~TimerGuard() { t->stop( n ); }
  };

  std::optional<TimerGuard> timerGuard( ISequencerTimerTool* tt, int n ) {
    if ( !tt ) return {};
    return TimerGuard{*tt, n};
  }

  struct counting_inserter {
    int                count = 0;
    counting_inserter& operator++() { return *this; }
    counting_inserter& operator*() { return *this; }
    template <typename T>
    counting_inserter& operator=( const T& ) {
      ++count;
      return *this;
    }
  };

  void removeDuplicates( std::vector<PrForwardTrack>& tracks, std::vector<std::vector<LHCb::LHCbID>> const& ids,
                         float const deltaQuality ) {
    float const zEndT = StateParameters::ZEndT;
    for ( size_t i1 = 0, nTotal = tracks.size(); i1 < nTotal; ++i1 ) {
      auto& t1 = tracks[i1];
      if ( !t1.valid() ) continue;
      for ( size_t i2 = i1 + 1; i2 < nTotal; ++i2 ) {
        auto& t2 = tracks[i2];
        if ( !t2.valid() ) continue;
        if ( std::abs( t1.x( zEndT ) - t2.x( zEndT ) ) > 50.f )
          break; // The distance only gets larger, as the vectors are sorted
        if ( std::abs( t1.y( zEndT ) - t2.y( zEndT ) ) > 100.f ) continue;

        int const nCommon =
            std::set_intersection( ids[i1].begin(), ids[i1].end(), ids[i2].begin(), ids[i2].end(), counting_inserter{} )
                .count;

        if ( nCommon > .4f * ( ids[i1].size() + ids[i2].size() ) ) {
          float const delta_q = t2.quality() - t1.quality();
          if ( delta_q > deltaQuality ) {
            t2.setValid( false );
          } else if ( delta_q < -deltaQuality ) {
            t1.setValid( false );
          }
        }
      }
    }
  }

  std::vector<std::vector<LHCb::LHCbID>> extractAndSortLHCbIDs( std::vector<PrForwardTrack> const& tracks ) {
    std::vector<std::vector<LHCb::LHCbID>> ids;
    ids.reserve( tracks.size() );
    for ( auto const& track : tracks ) {
      auto const& hits  = track.hits();
      auto&       idVec = ids.emplace_back();
      idVec.reserve( hits.size() );
      std::transform( hits.begin(), hits.end(), std::back_inserter( idVec ),
                      []( PrHit const* hit ) { return hit->id(); } );
      std::sort( idVec.begin(), idVec.end() );
    }
    return ids;
  }

  class yShiftCalculator final {
  private:
    float m_t1;
    float m_t2;
    float m_magscf;

  public:
    yShiftCalculator( VeloSeed const& seed, const float magscalefactor ) : m_magscf{magscalefactor} {
      const float ty  = seed.ty;
      const float ty2 = ty * ty;
      const float ty3 = ty2 * ty;
      const float tx  = seed.tx;
      const float tx2 = tx * tx;
      const float tx3 = tx * tx2;
      // parameters of the polynomial are obtained from MC toy studies
      // (see
      // https://indico.cern.ch/event/810764/contributions/3394473/attachments/1830241/2997521/Fast_Upgrade_Forward_Tracking.pdf)
      m_t1 = ( 1.95859013e+01f * ty + -1.69927519e+03f * tx2 * ty + 9.96117334e+02f * ty3 +
               6.18235545e+04f * tx2 * ty3 ); // doesn't depend on sign of qop
      m_t2 = ( 1.01414534e+00f + 1.96863566e+03f * tx * ty + 5.42636977e+03f * tx3 * ty +
               -1.23244167e+04f * tx * ty3 ); // need to multiply by a factor of sign of qop
    }
    inline const Vc::float_v calcYCorrection( Vc::float_v const& dSlope, int layer ) const {
      constexpr float                c0      = 1.f / 0.2677f;
      constexpr std::array<float, 6> scaling = {0.62f, 0.63f, 0.83f, 0.85f, 1.00f, 1.03f};
      const Vc::float_v              factor  = m_magscf * Vc::copysign( 1.f, dSlope );
      return ( m_t1 + m_t2 * factor ) * std::abs( dSlope ) * c0 * scaling[layer];
    }
  };

} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
template <typename T>
PrForwardTracking<T>::PrForwardTracking( const std::string& name, ISvcLocator* pSvcLocator )
    : base_class_t( name, pSvcLocator,
                    std::array{typename base_class_t::KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                               typename base_class_t::KeyValue{"InputName", LHCb::TrackLocation::Velo}},
                    typename base_class_t::KeyValue{"OutputName", "Rec/Track/FTSoA"} )
    , m_MLPReader_1st{mlpInputVars}
    , m_MLPReader_2nd{mlpInputVars} {}

//=============================================================================
// Initialization
//=============================================================================
template <typename T>
StatusCode PrForwardTracking<T>::initialize() {
  auto sc = base_class_t::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // Initialise stuff we imported from PrForwardTool

  if ( m_addUTHitsTool.name().empty() ) m_addUTHitsTool.disable();

  // check options
  if ( m_maxChi2StereoLinear <= m_maxChi2Stereo ) {
    error() << "Error: m_maxChi2StereoLinear must be chosen larger than m_maxChi2Stereo" << endmsg;
    return StatusCode::FAILURE;
  }

  // Zones cache, retrieved from the detector store
  this->template registerCondition<base_class_t>( PrFTInfo::FTZonesLocation, m_zoneHandler );

  // Finish initialising stuff that used to be in PrForwardTool

  if ( !m_debugToolName.empty() ) {
    m_debugTool = this->template tool<IPrDebugTool>( m_debugToolName );
  } else {
    m_wantedKey = -100; // no debug
  }

  if ( m_doTiming ) {
    m_timerTool = this->template tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timeTotal = m_timerTool->addTimer( "PrForward total" );
    m_timerTool->increaseIndent();
    m_timePrepare = m_timerTool->addTimer( "PrForward prepare" );
    m_timeExtend  = m_timerTool->addTimer( "PrForward extend" );
    m_timeFinal   = m_timerTool->addTimer( "PrForward final" );
    m_timerTool->decreaseIndent();
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
template <typename T>
LHCb::Pr::Forward::Tracks PrForwardTracking<T>::operator()( PrFTHitHandler<PrHit> const& prFTHitHandler,
                                                            T const&                     input_tracks ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  auto timeTotal   = timerGuard( m_timerTool, m_timeTotal );
  auto timePrepare = timerGuard( m_timerTool, m_timePrepare );

  if ( input_tracks.size() == 0 ) {
    auto [velo_ancestors, upstream_ancestors] = get_ancestors( input_tracks );
    return {velo_ancestors, upstream_ancestors};
  }

  //== If needed, debug the cluster associated to the requested MC particle.
  if ( 0 <= m_wantedKey ) {
    info() << "--- Looking for MCParticle " << m_wantedKey << endmsg;
    for ( unsigned int zone = 0; PrFTInfo::nbZones() > zone; ++zone ) {
      for ( const auto& i : prFTHitHandler.hits( zone ) ) {
        if ( matchKey( i ) ) printHit( i, " " );
      }
    }
  }

  //============================================================
  //== Main processing: Extend selected tracks
  //============================================================
  timePrepare.reset();
  auto timeExtend = timerGuard( m_timerTool, m_timeExtend );

  // -- Loop over all Velo input tracks and try to find extension in the T-stations
  // -- This is the main 'work' of the forward tracking.
  auto candidates = extendTracks( input_tracks, prFTHitHandler );

  //============================================================
  //== Final processing: filtering of duplicates,...
  //============================================================
  timeExtend.reset();
  auto timeFinal = timerGuard( m_timerTool, m_timeFinal );

  // -- Sort the tracks according to their x-position of the state in the T-stations
  // -- in order to make the final loop faster.
  float const zEndT = StateParameters::ZEndT;
  std::sort( candidates.begin(), candidates.end(),
             [&zEndT]( auto const& track1, auto const& track2 ) { return track1.x( zEndT ) < track2.x( zEndT ); } );

  auto ids = extractAndSortLHCbIDs( candidates );
  removeDuplicates( candidates, ids, m_deltaQuality );
  auto outputTracks = makeLHCbTracks( candidates, std::move( ids ), input_tracks );

  m_nbOutputTracksCounter += outputTracks.size();
  return outputTracks;
}

//=========================================================================
//  Main method: Process a track
//=========================================================================
template <typename T>
PrForwardTracks PrForwardTracking<T>::extendTracks( T const& velo, const PrFTHitHandler<PrHit>& FTHitHandler ) const {
  std::vector<PrForwardTrack> tracks;
  tracks.reserve( velo.size() );
  PrForwardTracks result;
  result.reserve( velo.size() ); // TODO: Reserve proper size, maybe find a fraction of forwarded tracks

  // in the worst(!) case preslection <<< HERE!
  for ( int i{0}; i < velo.size(); ++i ) {

    const auto [endv_qp, endv_pos, endv_dir] = [&]() {
      if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
        Vec3<F> const endv_pos = velo.template statePos<F>( i );
        Vec3<F> const endv_dir = velo.template stateDir<F>( i );
        return std::tuple{velo.template stateQoP<F>( i ).cast(), endv_pos, endv_dir};
      } else {
        Vec3<F> const endv_pos = velo.template statePos<F>( i, 1 );
        Vec3<F> const endv_dir = velo.template stateDir<F>( i, 1 );
        return std::tuple{1.0f, endv_pos, endv_dir};
      }
    }();

    // everything I need from the end velo state
    float const endv_x    = endv_pos.x.cast();
    float const endv_y    = endv_pos.y.cast();
    float const endv_tx   = endv_dir.x.cast();
    float const endv_ty   = endv_dir.y.cast();
    float const endv_z    = endv_pos.z.cast();
    float const endv_tx2  = endv_tx * endv_tx;
    float const endv_ty2  = endv_ty * endv_ty;
    float const endv_txy2 = endv_tx2 + endv_ty2;
    float const endv_txy  = std::sqrt( endv_txy2 );
    float const endv_pq   = 1.f / endv_qp;
    float const endv_p    = std::abs( endv_pq );
    float const endv_pz   = endv_p / std::sqrt( 1.f + endv_txy2 );
    float const endv_pt   = endv_pz * endv_txy;

    if ( m_useMomentumEstimate ) {
      if ( endv_p < 1000.f ) continue;
      if ( m_preselection ) {
        if ( endv_pt < m_preselectionPT ) continue;
      }
    }
    tracks.emplace_back( i, endv_x, endv_y, endv_z, endv_tx, endv_ty, endv_qp );
  }
  // First loop Hough Cluster search
  PrParameters pars{m_minXHits, m_maxXWindow, m_maxXWindowSlope, m_maxXGap, 4u};
  PrParameters pars2ndLoop{m_minXHits2nd, m_maxXWindow2nd, m_maxXWindowSlope2nd, m_maxXGap2nd, 4u};

  // -- This does not change throughout the code
  const float magScaleFactor = m_geoTool->magscalefactor();

  PrForwardTracks trackCandidates;
  PrForwardTracks trackCandidates2ndLoop;

  std::vector<ModPrHit> allXHitsUpper;
  allXHitsUpper.reserve( numberallXHitsAfterCuts );
  std::vector<ModPrHit> allXHitsLower;
  allXHitsLower.reserve( numberallXHitsAfterCuts );

  for ( const auto& track : tracks ) {
    allXHitsUpper.clear();
    allXHitsLower.clear();
    trackCandidates.clear();
    trackCandidates2ndLoop.clear();

    // calculate y of velo at zref to decide on which side to search
    const float yAtRef = track.seed().y( zReference );

    // -- it happens rarely that we have to collect in both halves
    if ( yAtRef > -5.f ) {
      collectAllXHits<PrHitZone::Side::Upper>( allXHitsUpper, track, FTHitHandler, pars, magScaleFactor );
      selectXCandidates<PrHitZone::Side::Upper>( trackCandidates, track, allXHitsUpper, FTHitHandler, pars );
    }
    if ( yAtRef < 5.f ) {
      collectAllXHits<PrHitZone::Side::Lower>( allXHitsLower, track, FTHitHandler, pars, magScaleFactor );
      selectXCandidates<PrHitZone::Side::Lower>( trackCandidates, track, allXHitsLower, FTHitHandler, pars );
    }

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << endmsg << "=============== Selected " << trackCandidates.size() << " candidates in 1st loop." << endmsg
             << endmsg;
      int nValidTracks = 0;
      for ( const auto& trackcand : trackCandidates ) {
        if ( trackcand.valid() ) printTrack( trackcand );
        nValidTracks++;
      }
      info() << "  valid tracks :   " << nValidTracks << endmsg;
    }
    // -- < Debug --------

    // Stereo hit search and full Fit
    selectFullCandidates( trackCandidates, FTHitHandler, pars, magScaleFactor );

    // erase tracks not valid
    trackCandidates.erase( std::remove_if( trackCandidates.begin(), trackCandidates.end(),
                                           []( const auto& track ) { return !track.valid(); } ),
                           trackCandidates.end() );
    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << "********** final list of 1st loop candidates ********" << endmsg;
      for ( const auto& trackcand : trackCandidates ) printTrack( trackcand );
    }
    // -- < Debug --------

    // check tracks, if no OK track is found start second Loop search
    bool ok = std::any_of( trackCandidates.begin(), trackCandidates.end(),
                           []( const auto& track ) { return track.hits().size() > 10; } );

    if ( !ok && m_secondLoop ) {
      // Second loop Hough Cluster search
      if ( yAtRef > -5.f )
        selectXCandidates<PrHitZone::Side::Upper>( trackCandidates2ndLoop, track, allXHitsUpper, FTHitHandler,
                                                   pars2ndLoop );
      if ( yAtRef < 5.f )
        selectXCandidates<PrHitZone::Side::Lower>( trackCandidates2ndLoop, track, allXHitsLower, FTHitHandler,
                                                   pars2ndLoop );
      selectFullCandidates( trackCandidates2ndLoop, FTHitHandler, pars2ndLoop, magScaleFactor );

      std::copy_if( std::make_move_iterator( trackCandidates2ndLoop.begin() ),
                    std::make_move_iterator( trackCandidates2ndLoop.end() ), std::back_inserter( trackCandidates ),
                    []( const auto& track ) { return track.valid(); } );

      ok = !trackCandidates.empty();
    }

    // clone+ghost killing after merging
    if ( ok || !m_secondLoop ) {
      auto [minQuality, best] = best_element(
          trackCandidates, [&]( const auto& t ) { return t.quality() + m_deltaQuality; }, std::less<>{},
          m_maxQuality.value() );
      std::copy_if( std::make_move_iterator( trackCandidates.begin() ),
                    std::make_move_iterator( trackCandidates.end() ), std::back_inserter( result ),
                    [minQuality = minQuality]( const auto& t ) { return t.quality() <= minQuality; } );
    }
  }
  return result;
}

//=========================================================================
//  Create Full candidates out of xCandidates
//  Searching for stereo hits
//  Fit of all hits
//  save everything in track candidate folder
//=========================================================================
template <typename T>
void PrForwardTracking<T>::selectFullCandidates( PrForwardTracks&             trackCandidates,
                                                 const PrFTHitHandler<PrHit>& FTHitHandler, PrParameters& pars,
                                                 const float magScaleFactor ) const {

  int                nbOK = 0;
  std::vector<float> mlpInput( 7, 0. );

  for ( auto& cand : trackCandidates ) {
    if ( !cand.valid() ) continue;
    cand.setValid( false ); // set only true after track passed everything

    // at least 4 stereo hits OR  minTotalHits - found xHits (WATCH unsigned numbers!)
    pars.minStereoHits = 4u;
    if ( cand.hits().size() + pars.minStereoHits < m_minTotalHits )
      pars.minStereoHits = m_minTotalHits - cand.hits().size();

    // search for hits in U/V layers
    std::vector<ModPrHit> stereoHits = collectStereoHits( cand, FTHitHandler );
    if ( stereoHits.size() < pars.minStereoHits ) continue;
    std::sort( stereoHits.begin(), stereoHits.end() );

    // select best U/V hits
    if ( !selectStereoHits( cand, FTHitHandler, stereoHits, pars ) ) continue;

    // reset pc to count ALL hits
    PrPlaneCounter pc;
    pc.set( cand.hits() );

    // make a fit of ALL hits
    if ( !fitXProjection( cand, pars, pc ) ) continue;

    // check in empty x layers for hits
    auto checked_empty = ( cand.y( zReference ) < 0.f )
                             ? addHitsOnEmptyXLayers<PrHitZone::Side::Lower>( cand, true, FTHitHandler, pars, pc )
                             : addHitsOnEmptyXLayers<PrHitZone::Side::Upper>( cand, true, FTHitHandler, pars, pc );

    if ( !checked_empty ) continue;

    // track has enough hits, calcualte quality and save if good enough
    if ( pc.nbDifferent() >= m_minTotalHits ) {

      const float qOverP = calcqOverP( cand, magScaleFactor );

      // orig params before fitting , TODO faster if only calc once?? mem usage?
      const float xAtRef    = cand.x( zReference );
      float       dSlope    = ( cand.seed().x( zReference ) - xAtRef ) / ( zReference - zMagnetParams[0] );
      const float zMagSlope = zMagnetParams[2] * cand.seed().tx2 + zMagnetParams[3] * cand.seed().ty2;
      const float zMag      = zMagnetParams[0] + zMagnetParams[1] * dSlope * dSlope + zMagSlope;
      const float xMag      = cand.seed().x( zMag );
      const float slopeT    = ( xAtRef - xMag ) / ( zReference - zMag );
      dSlope                = slopeT - cand.seed().tx;
      const float dyCoef    = dSlope * dSlope * cand.seed().ty;

      float bx = slopeT;
      float ay = cand.seed().y( zReference );
      float by = cand.seed().ty + dyCoef * byParams[0];

      // ay,by,bx params
      const auto  yPars = cand.getYParams();
      const float ay1   = yPars.get( 0 );
      const float by1   = yPars.get( 1 );
      const auto  xPars = cand.getXParams();
      const float bx1   = xPars.get( 1 );

      mlpInput[0] = pc.nbDifferent();
      mlpInput[1] = qOverP;
      mlpInput[2] = ( std::abs( cand.seed().qOverP ) < 1e-9f || !m_useMomentumEstimate )
                        ? 0.f
                        : cand.seed().qOverP - qOverP; // input_tracks - scifi
      mlpInput[3] = cand.seed().slope2;
      mlpInput[4] = by - by1;
      mlpInput[5] = bx - bx1;
      mlpInput[6] = ay - ay1;

      /// WARNING: if the NN classes straight out of TMVA are used, put a mutex here!
      float quality = ( pars.minXHits > 4 ? m_MLPReader_1st.GetMvaValue( mlpInput )    // 1st loop NN
                                          : m_MLPReader_2nd.GetMvaValue( mlpInput ) ); // 2nd loop NN
      quality       = 1.f - quality;                                                   // backward compability

      if ( quality < m_maxQuality ) {
        cand.setValid( true );
        cand.setQuality( quality );
        cand.setQoP( qOverP );
        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) ) {
          info() << "*** Accepted as track " << nbOK << " ***" << endmsg;
          printTrack( cand );
        }
        // -- < Debug --------
        ++nbOK;
      }
    }
  }
}

//=========================================================================
//  Collect all X hits, within a window defined by the minimum Pt.
//  Better restrictions possible, if we use the momentum of the input track.
//  Ask for the presence of a stereo hit in the same biLayer compatible.
//  This reduces the efficiency. X-alone hits to be re-added later in the processing
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE>
void PrForwardTracking<T>::collectAllXHits( std::vector<ModPrHit>& allXHits, const PrForwardTrack& track,
                                            const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                                            const float magScaleFactor ) const {

  // TODO improve check if hits can be found at all
  const VeloSeed&        seed = track.seed();
  const yShiftCalculator yc{seed, magScaleFactor};

  //== Compute the size of the search window in the reference plane
  float dxRef = calcDxRef( seed, m_minPT );
  // dxRef *= 1.10; //== 10% tolerance
  dxRef *= 0.9f; // make windows a bit too small REALLY?
  const float zMag  = zMagnet( track );
  const float dzInv = 1.f / ( zReference - zMag );

  // -- Compute some thing we need later on
  const float dir = magScaleFactor * ( -1.f ) * std::copysign( 1.f, seed.qOverP );

  // -- This is all for the treatment of "wrong sign tracks", ie tracks that got the wrong sign assigned in VeloUT
  // -- Only do this once (outside the loop)

  const float pt = std::sqrt( track.seed().slope2 ) *
                   ( ( std::abs( 1.f / track.seed().qOverP ) ) / std::sqrt( 1.f + track.seed().slope2 ) );
  const bool wSignTreatment = m_useMomentumEstimate && m_useWrongSignWindow && pt > m_wrongSignPT;

  float dxRefWS = 0;
  if ( wSignTreatment ) {
    dxRefWS = calcDxRef( seed, m_wrongSignPT );
    dxRefWS *= 0.9f; // make windows a bit too small - FIXME check effect of this, seems wrong
  }

  if ( m_useMomSearchWindow && m_useMomentumEstimate ) {
    const float p     = 1.f / std::abs( seed.qOverP );
    const float InvPz = std::sqrt( seed.slope2 ) / pt;
    // calculate the extrapolation at Reference plane
    const float xExt = ( xExtParams[0] + xExtParams[1] * InvPz ) * InvPz + xExtParams[2] * std::abs( seed.tx ) +
                       xExtParams[3] * seed.tx2 + xExtParams[4] * std::abs( seed.ty ) + xExtParams[5] * seed.ty2;
    // Calculate the search window in the direction and inverse direction of bending
    const float upLimit = xExt + ( pUp[0] + pUp[1] * std::exp( pUp[2] * p ) );
    const float loLimit = xExt - ( pLo[0] + pLo[1] * std::exp( pLo[2] * p ) );

    // the search window should be limited inside the minPt cut window
    if ( dxRef > upLimit ) dxRef = upLimit;
    if ( loLimit > -dxRefWS && loLimit < dxRef ) dxRefWS = -loLimit;
  }

  // ----------------------------------

  boost::container::static_vector<int, 7> iZoneEnd = {0}; // 6 x planes

  for ( unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; iZone++ ) {

    const unsigned int zoneNumber = PrFTZoneHandler::getXZone<SIDE>( iZone );
    const auto&        zone       = m_zoneHandler->zone( zoneNumber );

    const float zZone   = zone.z();
    const float xInZone = seed.x( zZone );
    const float yInZone = seed.y( zZone );
    // TODO do this check more clever at the beginning?!
    if ( !zone.isInside( xInZone, yInZone ) ) continue;

    const float ratio = ( zZone > zReference || ( m_useMomSearchWindow && m_useMomentumEstimate ) )
                            ? ( zZone - zMag ) * dzInv
                            : zZone * zRefInv;

    const float xTol = dxRef * ratio;
    float       xMin = xInZone - xTol;
    float       xMax = xInZone + xTol;

    // -- Use momentum estimate from VeloUT tracks
    if ( m_useMomentumEstimate && 0.f != seed.qOverP ) {

      // TODO tune this window
      // -- Extra window to catch wrong sign tracks
      float xTolWS = 0.0;
      if ( wSignTreatment || m_useMomSearchWindow ) { xTolWS = dxRefWS * ratio; }

      if ( dir > 0 ) {
        xMin = xInZone - xTolWS;
      } else {
        xMax = xInZone + xTolWS;
      }
    }
    // --

    const unsigned int uvZoneNumber = PrFTZoneHandler::getUVZone<SIDE>( iZone );
    const unsigned int triangleZone = PrFTZoneHandler::getTriangleZone<SIDE>( iZone );

    // -- Use search to find the lower bound of the range of x values
    // range of xHits of current window ([xMin, xMax]) on the current zone
    const auto xHits      = FTHitHandler.getRange( zoneNumber, xMin, xMax ); // is a span
    auto       itxHits    = xHits.begin();
    const auto itxHitsEnd = xHits.end();

    if ( UNLIKELY( xHits.empty() ) ) continue; // otherwise crash in calculating xMinUV

    // MatchStereoHits
    const auto& zoneUv       = m_zoneHandler->zone( uvZoneNumber );
    const float invDz        = 1.f / ( zZone - zMag );
    const float zUVZone      = zoneUv.z();
    const float xInUv        = seed.x( zUVZone );
    const float yInUv        = seed.y( zUVZone );
    const float zRatio       = ( zUVZone - zMag ) * invDz;
    const float uvDxDy       = zoneUv.dxDy();
    const float dx           = yInUv * uvDxDy; // x correction from rotation by stereo angle
    const float xCentral     = xInZone + dx;
    const float xPredUVProto = xInUv - xInZone * zRatio - dx;
    const float xPredUvStart = xPredUVProto + xHits[0].x() * zRatio; // predicted hit in UV-layer
    const float maxDxProto   = m_tolYCollectX + std::abs( yInZone ) * m_tolYSlopeCollectX;
    const float maxDxStart   = maxDxProto + std::abs( xHits[0].x() - xCentral ) * m_tolYSlopeCollectX;
    const float xMinUVStart  = xPredUvStart - maxDxStart;

    const bool triangleSearch = std::abs( yInZone ) < m_tolYTriangleSearch; // check if triangle search should be done
    auto       rUV1           = FTHitHandler.getRange_lowerBound( uvZoneNumber, xMinUVStart );
    LHCb::span<const PrHit> rUV2;
    if ( triangleSearch ) rUV2 = FTHitHandler.getRange_lowerBound( triangleZone, xMinUVStart );

    Vc::float_v xVc{Vc::Zero};
    for ( auto it = itxHits; itxHits != itxHitsEnd; ) { // loop over all xHits in a layer between xMin and xMax
      for ( size_t i = 0; i < Vc::float_v::Size && it != itxHitsEnd; ++i, ++it ) { xVc[i] = it->x(); }
      const Vc::float_v dSlp    = ( xVc - xInZone ) * invDz;
      const Vc::float_v yCorrec = yc.calcYCorrection( dSlp, iZone );
      const Vc::float_v xPredUv = xPredUVProto + xVc * zRatio + yCorrec * uvDxDy; // predicted hit in UV-layer
      const Vc::float_v maxDx   = maxDxProto + Vc::abs( xVc - xCentral ) * m_tolYSlopeCollectX.value();
      const Vc::float_v xMinUV  = xPredUv - maxDx;
      const Vc::float_v xMaxUV  = xPredUv + maxDx;
      for ( size_t i = 0; i < Vc::float_v::Size && itxHits != itxHitsEnd; ++i, ++itxHits ) {
        // first check normal range for matching stereo hit,
        // if not found and triangle search enabled, then also check triangle zone
        if ( matchStereoHit( rUV1, xMinUV[i], xMaxUV[i] ) ||
             ( triangleSearch &&
               matchStereoHitWithTriangle<SIDE>( rUV2, yInZone, xMinUV[i], xMaxUV[i], m_yTolUVSearch ) ) ) {
          allXHits.emplace_back( std::addressof( *itxHits ), itxHits->x(), itxHits->planeCode(), 0 );
        }
      }
    }

    const auto iStart = allXHits.begin() + iZoneEnd.back();
    const auto iEnd   = allXHits.end();
    iZoneEnd.push_back( allXHits.size() );
    if ( LIKELY( iStart != iEnd ) ) {
      xAtRef_SamePlaneHits( track, iStart, iEnd ); // calc xRef for all hits on same layer
    }
  }

  if ( LIKELY( iZoneEnd.size() == 7 ) ) {
    mergeSortedRanges<7>( allXHits, iZoneEnd );
  } else {
    // default sort
    std::sort( allXHits.begin(), allXHits.end() );
  }

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    const float xWindow = pars.maxXWindow + std::abs( seed.x( zReference ) ) * pars.maxXWindowSlope;
    info() << "**** Processing Velo track  zone "
           << " Selected " << allXHits.size() << " hits, window size " << xWindow << endmsg;
  }
  // -- < Debug --------
}

//=========================================================================
//  Select the zones in the allXHits array where we can have a track
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE>
void PrForwardTracking<T>::selectXCandidates( PrForwardTracks& trackCandidates, const PrForwardTrack& seed,
                                              std::vector<ModPrHit>&       allXHits,
                                              const PrFTHitHandler<PrHit>& FTHitHandler,
                                              const PrParameters&          pars ) const {
  if ( allXHits.size() < pars.minXHits ) return;
  const float xStraight = seed.x( zReference );

  // Parameters for X-hit only fit, thus do not require stereo hits
  PrParameters xFitPars{pars};
  xFitPars.minStereoHits = 0;

  PrLineFitter lineFitter;
  PrHits       coordToFit;
  coordToFit.reserve( 16 );
  std::array<std::vector<ModPrHit>, 12> otherHits;

  const auto itEnd = std::end( allXHits );
  auto       it1   = std::begin( allXHits );
  while ( true ) {
    // find next unused Hits
    while ( it1 + pars.minXHits - 1 < itEnd && !it1->isValid() ) ++it1;
    auto it2 = it1 + pars.minXHits; // it2 pointing at last+1 element, like list.end()
    while ( it2 <= itEnd && !( it2 - 1 )->isValid() ) ++it2;
    if ( it2 > itEnd ) break; // Minimum is after the end!

    // define search window for Cluster
    // TODO better xWindow calculation?? how to tune this???
    const float xWindow =
        pars.maxXWindow + ( std::abs( it1->coord ) + std::abs( it1->coord - xStraight ) ) * pars.maxXWindowSlope;

    // If window is to small, go one step right
    if ( ( ( it2 - 1 )->coord - it1->coord ) > xWindow ) {
      ++it1;
      continue;
    }

    // Cluster candidate found, now count planes
    PrPlaneCounter pc;
    std::for_each( it1, it2, [&pc]( const auto& h ) {
      if ( h.isValid() ) pc.addHit( h );
    } );

    // Improve cluster (at the moment only add hits to the right)
    auto itLast = it2 - 1;
    while ( it2 < itEnd ) {
      // if last/first hit isUsed, skip this
      if ( !it2->isValid() ) {
        ++it2;
        continue;
      }
      // now  the first and last+1 hit exist and are not used!

      // Add next hit,
      // if there is only a small gap between the hits
      //    or inside window and plane is still empty
      if ( ( it2->coord < itLast->coord + pars.maxXGap ) ||
           ( ( it2->coord - it1->coord < xWindow ) && ( pc.nbInPlane( *it2 ) == 0 ) ) ) {
        pc.addHit( *it2 );
        itLast = it2;
        ++it2;
        continue;
      }
      // Found nothing to improve
      break;
    }

    // if not enough different planes, start again from the very beginning with next right hit
    if ( pc.nbDifferent() < pars.minXHits ) {
      ++it1;
      continue;
    }

    //====================================================================
    //  Now we have a (rather) clean candidate, do best hit selection
    //  Two possibilities:
    //  1) If there are enough planes with only one hit, do a straight
    //      line fit through these and select good matching others
    //  2) Do some magic
    //====================================================================

    coordToFit.clear();
    coordToFit.reserve( 16 );
    float              xAtRef   = 0.;
    const unsigned int nbSingle = pc.nbSingle();

    if ( nbSingle >= m_minSingleHits && nbSingle != pc.nbDifferent() ) {
      // 1) we have enough single planes (thus two) to make a straight line fit

      lineFitter.reset( zReference, &coordToFit );
      for ( auto& others : otherHits ) others.clear();

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "--- " << nbSingle << " planes with a single hit. Select best chi2 in other planes ---" << endmsg;
        std::for_each( it1, it2, [&]( const auto& h ) {
          if ( h.isValid() ) printHit( *h.hit );
        } );
      }
      // -- < Debug --------

      // seperate single and double hits
      std::for_each( it1, it2, [&]( const auto& h ) {
        if ( !h.isValid() ) return;
        if ( pc.nbInPlane( h ) == 1 ) {
          lineFitter.addHit( h );
        } else {
          assert( h.planeCode < static_cast<int>( otherHits.size() ) );
          otherHits[h.planeCode].push_back( h );
        }
      } );
      lineFitter.solve();

      // select best other hits (only best other hit is enough!)
      for ( const auto& hits : otherHits ) {
        auto [chi2, best] = best_element(
            hits, [&]( const auto& hit ) { return lineFitter.chi2( hit ); }, std::less<>{}, 1e9f );
        if ( !best ) continue;
        lineFitter.addHit( *best );
        lineFitter.solve();
      }
      xAtRef = lineFitter.coordAtRef();

    } else {
      // 2) Try to find a small distance containing at least 5(4) different planes
      //    Most of the time do nothing

      const unsigned int nPlanes       = std::min( pc.nbDifferent(), uint{5} );
      auto               itWindowStart = it1;
      auto               itWindowEnd   = it1 + nPlanes; // pointing at last+1
      // Hit is used, go to next unused one
      while ( itWindowEnd <= it2 && !( itWindowEnd - 1 )->isValid() ) ++itWindowEnd;
      if ( itWindowEnd > it2 ) continue; // start from very beginning

      float minInterval = 1.e9f;
      auto  best        = itWindowStart;
      auto  bestEnd     = itWindowEnd;

      PrPlaneCounter lpc;
      std::for_each( itWindowStart, itWindowEnd, [&lpc]( const auto& h ) {
        if ( h.isValid() ) lpc.addHit( h );
      } );

      while ( itWindowEnd <= it2 ) {
        if ( lpc.nbDifferent() >= nPlanes ) {
          // have nPlanes, check x distance
          const float dist = ( itWindowEnd - 1 )->coord - itWindowStart->coord;
          if ( dist < minInterval ) {
            minInterval = dist;
            best        = itWindowStart;
            bestEnd     = itWindowEnd;
          }
        } else {
          // too few planes, add one hit
          ++itWindowEnd;
          while ( itWindowEnd <= it2 && !( itWindowEnd - 1 )->isValid() ) ++itWindowEnd;
          if ( itWindowEnd > it2 ) break;
          lpc.addHit( *( itWindowEnd - 1 ) );
          continue;
        }
        // move on to the right
        lpc.removeHit( *itWindowStart );
        ++itWindowStart;
        while ( itWindowStart < itWindowEnd && !itWindowStart->isValid() ) ++itWindowStart;
        // last hit guaranteed to be not used. Therefore there is always at least one hit to go to. No additional if
        // required.
      }

      // TODO tune minInterval cut value
      if ( minInterval < 1.f ) {
        it1 = best;
        it2 = bestEnd;
      }

      // Fill coords and compute average x at reference
      std::for_each( it1, it2, [&]( const auto& h ) {
        if ( h.isValid() ) {
          coordToFit.push_back( h.hit );
          xAtRef += h.coord;
        }
      } );
      xAtRef /= ( (float)coordToFit.size() );
    }

    //=== We have a candidate :)

    // overwriting is faster than resetting, attention: values which are not overwritten do not make sense!!
    pc.clear();
    pc.set( coordToFit ); // too difficult to keep track of add and delete, just do it again..
    // only unused(!) hits in coordToFit now

    bool           ok = pc.nbDifferent() > 3;
    PrForwardTrack track( seed );
    if ( ok ) {
      track.replaceHits( std::move( coordToFit ) );
      setTrackParameters( track, xAtRef );
      fastLinearFit( track, pars, pc );
      if ( pc.nbDifferent() < PrFTInfo::NFTXLayers )
        addHitsOnEmptyXLayers<SIDE>( track, false, FTHitHandler, pars, pc );
      ok = pc.nbDifferent() > 3;
    }
    //== Fit and remove hits...
    if ( ok ) ok = fitXProjection( track, xFitPars, pc );
    if ( ok ) ok = track.chi2PerDoF() < m_maxChi2PerDoF;
    if ( ok && pc.nbDifferent() < PrFTInfo::NFTXLayers )
      ok = addHitsOnEmptyXLayers<SIDE>( track, true, FTHitHandler, xFitPars, pc );
    if ( ok ) {
      // set ModPrHits used , challenge: we don't have the link any more!
      // Do we really need isUsed in Forward? We can otherwise speed up the search quite a lot!
      // --> we need it for the second loop
      setHitsUsed( it1, itEnd, track, xFitPars );
      trackCandidates.emplace_back( std::move( track ) );
      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "=== Storing track candidate " << trackCandidates.size() << endmsg;
        printTrack( trackCandidates.back() );
        info() << endmsg;
      }
      // -- < Debug --------
    }
    // next one
    ++it1;
  }
}
//=========================================================================
//  Fit a linear form, remove the external worst as long as chi2 is big...
//=========================================================================
template <typename T>
void PrForwardTracking<T>::fastLinearFit( PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc ) const {
  bool fit = true;
  while ( fit ) {
    //== Fit a line
    float s0  = 0.;
    float sz  = 0.;
    float sz2 = 0.;
    float sd  = 0.;
    float sdz = 0.;

    for ( const auto& hit : track.hits() ) {
      const float zHit = hit->z();
      const float d    = hit->distanceXHit( track.x( zHit ) );
      const float w    = hit->w();
      const float z    = zHit - zReference;
      s0 += w;
      sz += w * z;
      sz2 += w * z * z;
      sd += w * d;
      sdz += w * d * z;
    }
    float den = ( sz * sz - s0 * sz2 );
    if ( !( std::abs( den ) > 1e-5f ) ) return;
    const float da = ( sdz * sz - sd * sz2 ) / den;
    const float db = ( sd * sz - s0 * sdz ) / den;
    track.addXParams<2>( {da, db} );
    fit = false;

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << "Linear fit, current status : " << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( track.hits().size() < pars.minXHits ) return;

    auto       worst       = track.hits().end();
    float      maxChi2     = 0.f;
    const bool notMultiple = pc.nbDifferent() == track.hits().size();
    // TODO how many multiple hits do we normaly have?
    // how often do we do the right thing here?
    // delete two hits at same time?

    for ( auto itH = begin( track.hits() ); end( track.hits() ) != itH; ++itH ) {
      float chi2 = track.chi2XHit( **itH );
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( ( *itH )->planeCode() ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    //== Remove grossly out hit, or worst in multiple layers

    if ( maxChi2 > m_maxChi2LinearFit || ( !notMultiple && maxChi2 > 4.f ) ) {

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "Remove hit ";
        printHit( **worst );
      }
      // -- < Debug --------

      pc.removeHit( **worst );
      // removing hit from track list, no need to keep track of isUsed
      std::iter_swap( worst, std::prev( track.hits().end() ) ); // faster than just erase, order does not matter
      track.hits().pop_back();
      fit = true;
    }
  }
}
//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
template <typename T>
bool PrForwardTracking<T>::fitXProjection( PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc ) const {

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    info() << "  -- Entering fitXProjection with:" << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minXHits ) {

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) info() << "  == Not enough layers ( " << pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------

    return false;
  }

  bool doFit = true;
  while ( doFit ) {
    //== Fit a cubic
    float s0   = 0.f;
    float sz   = 0.f;
    float sz2  = 0.f;
    float sz3  = 0.f;
    float sz4  = 0.f;
    float sd   = 0.f;
    float sdz  = 0.f;
    float sdz2 = 0.f;

    for ( const auto& hit : track.hits() ) {
      float d = track.distance( *hit );
      float w = hit->w();
      float z = .001f * ( hit->z() - zReference );
      s0 += w;
      sz += w * z;
      sz2 += w * z * z;
      sz3 += w * z * z * z;
      sz4 += w * z * z * z * z;
      sd += w * d;
      sdz += w * d * z;
      sdz2 += w * d * z * z;
    }
    const float b1  = sz * sz - s0 * sz2;
    const float c1  = sz2 * sz - s0 * sz3;
    const float d1  = sd * sz - s0 * sdz;
    const float b2  = sz2 * sz2 - sz * sz3;
    const float c2  = sz3 * sz2 - sz * sz4;
    const float d2  = sdz * sz2 - sz * sdz2;
    const float den = ( b1 * c2 - b2 * c1 );
    if ( !( std::abs( den ) > 1e-5f ) ) return false;
    const float db = ( d1 * c2 - d2 * c1 ) / den;
    const float dc = ( d2 * b1 - d1 * b2 ) / den;
    const float da = ( sd - db * sz - dc * sz2 ) / s0;
    track.addXParams<3>( {da, db * 1.e-3f, dc * 1.e-6f} );

    float maxChi2 = 0.f;
    float totChi2 = 0.f;
    // int   nDoF = -3; // fitted 3 parameters
    int        nDoF        = -3;
    const bool notMultiple = pc.nbDifferent() == track.hits().size();

    const auto itEnd = end( track.hits() );
    auto       worst = itEnd;
    for ( auto itH = begin( track.hits() ); itEnd != itH; ++itH ) {
      float chi2 = track.chi2( **itH );
      totChi2 += chi2;
      ++nDoF;
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( ( *itH )->planeCode() ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }
    if ( nDoF < 1 ) return false;
    track.setChi2nDof( {totChi2, (float)nDoF} );

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << "  -- In fitXProjection, maxChi2 = " << maxChi2 << " totCHi2/nDof " << totChi2 / nDoF << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( worst == itEnd ) return true;
    doFit = false;
    if ( totChi2 > m_maxChi2PerDoF * nDoF || maxChi2 > m_maxChi2XProjection ) {
      pc.removeHit( **worst );                         // only valid "unused" hits in track.hits()
      std::iter_swap( worst, track.hits().end() - 1 ); // faster than just erase, order does not matter
      track.hits().pop_back();

      if ( pc.nbDifferent() < pars.minXHits + pars.minStereoHits ) {

        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) ) {
          info() << "  == Not enough layers with hits" << endmsg;
          printTrack( track );
        }
        // -- < Debug --------

        return false;
      }
      doFit = true;
    }
  }

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    info() << "  -- End fitXProjection -- " << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  return true;
}

//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
template <typename T>
bool PrForwardTracking<T>::fitYProjection( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                           const PrParameters& pars, PrPlaneCounter& pc ) const {

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) {
    info() << "  -- Entering fitYProjection with:" << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minStereoHits ) {
    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) info() << "  == Not enough layers ( " << pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------
    return false;
  }

  float maxChi2  = 1.e9f;
  bool  parabola = false; // first linear than parabola

  //== Fit a line
  const float tolYMag =
      m_tolYMag + m_tolYMagSlope * std::abs( track.xStraight( zReference ) - track.seed().x( zReference ) );
  const float wMag = 1.f / ( tolYMag * tolYMag );

  bool doFit = true;
  while ( doFit ) {

    // Use position in magnet as constrain in fit
    // although bevause wMag is quite small only little influence...
    float       zMag  = zMagnet( track );
    const float dyMag = track.yStraight( zMag ) - track.seed().y( zMag );
    zMag -= zReference;
    float s0  = wMag;
    float sz  = wMag * zMag;
    float sz2 = wMag * zMag * zMag;
    float sd  = wMag * dyMag;
    float sdz = wMag * dyMag * zMag;

    auto itEnd = stereoHits.end();

    if ( parabola ) {
      float sz2m = 0.;
      float sz3  = 0.;
      float sz4  = 0.;
      float sdz2 = 0.;

      for ( const PrHit* hit : stereoHits ) {
        const float d = -track.distance( *hit ) / hit->dxDy(); // TODO multiplication much faster than division!
        const float w = hit->w();
        const float z = hit->z() - zReference;
        s0 += w;
        sz += w * z;
        sz2m += w * z * z;
        sz2 += w * z * z;
        sz3 += w * z * z * z;
        sz4 += w * z * z * z * z;
        sd += w * d;
        sdz += w * d * z;
        sdz2 += w * d * z * z;
      }
      const float b1  = sz * sz - s0 * sz2;
      const float c1  = sz2m * sz - s0 * sz3;
      const float d1  = sd * sz - s0 * sdz;
      const float b2  = sz2 * sz2m - sz * sz3;
      const float c2  = sz3 * sz2m - sz * sz4;
      const float d2  = sdz * sz2m - sz * sdz2;
      const float den = ( b1 * c2 - b2 * c1 );
      if ( !( std::abs( den ) > 1e-5f ) ) return false;

      const float db = ( d1 * c2 - d2 * c1 ) / den;
      const float dc = ( d2 * b1 - d1 * b2 ) / den;
      const float da = ( sd - db * sz - dc * sz2 ) / s0;
      track.addYParams<3>( {da, db, dc} );

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "fitYProjection Parabolic Fit da: " << da << " db: " << db << " dc: " << dc << endmsg;
      }
      // -- < Debug --------
    } else {

      for ( const auto& hit : stereoHits ) {
        const float d = -track.distance( *hit ) / hit->dxDy();
        const float w = hit->w();
        const float z = hit->z() - zReference;
        s0 += w;
        sz += w * z;
        sz2 += w * z * z;
        sd += w * d;
        sdz += w * d * z;
      }
      const float den = ( s0 * sz2 - sz * sz );
      if ( !( std::abs( den ) > 1e-5f ) ) return false;
      const float da = ( sd * sz2 - sdz * sz ) / den;
      const float db = ( sdz * s0 - sd * sz ) / den;
      track.addYParams<2>( {da, db} );

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) { info() << "fitYProjection Linear Fit da: " << da << " db: " << db << endmsg; }
      // -- < Debug --------
    } // fit end, now doing outlier removal

    auto worst = end( stereoHits );
    maxChi2    = 0.;
    for ( auto itH = begin( stereoHits ); itEnd != itH; ++itH ) {
      const float chi2 = track.chi2( **itH );
      if ( chi2 > maxChi2 ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    if ( maxChi2 < m_maxChi2StereoLinear && !parabola ) {
      parabola = true;
      maxChi2  = 1.e9f;
      continue;
    }

    if ( maxChi2 > m_maxChi2Stereo ) {
      pc.removeHit( **worst ); // stereo hits never 'used'
      if ( pc.nbDifferent() < pars.minStereoHits ) {

        // -- > Debug --------
        if ( msgLevel( MSG::DEBUG ) )
          info() << "-- not enough different planes after removing worst: " << pc.nbDifferent() << " for "
                 << pars.minStereoHits << " --" << endmsg;
        // -- < Debug --------

        return false;
      }
      stereoHits.erase( worst );
      continue;
    }

    break;
  }

  return true;
}
//=========================================================================
//  Add hits on empty X layers, and refit if something was added
//=========================================================================
template <typename T>
template <PrHitZone::Side SIDE>
bool PrForwardTracking<T>::addHitsOnEmptyXLayers( PrForwardTrack& track, bool fullFit,
                                                  const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                                                  PrPlaneCounter& pc ) const {

  // is there an empty plane? otherwise skip here!
  if ( pc.nbDifferent() > 11 ) return true;

  bool        added     = false;
  const auto& xPars     = track.getXParams();
  const float x1        = xPars.get( 0 );
  const float xStraight = track.seed().x( zReference );
  const float xWindow   = pars.maxXWindow + ( std::abs( x1 ) + std::abs( x1 - xStraight ) ) * pars.maxXWindowSlope;

  for ( unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; iZone++ ) {
    const unsigned int zoneNumber = PrFTZoneHandler::getXZone<SIDE>( iZone );
    if ( pc.nbInPlane( zoneNumber / 2 ) != 0 ) continue;

    const float zZone = m_zoneHandler->zone( zoneNumber ).z();
    const float xPred = track.x( zZone );

    // -- Use a search to find the lower bound of the range of x values
    auto [bestChi2, best] = best_element(
        FTHitHandler.getRange_lowerBound( zoneNumber, xPred - xWindow ) |
            ranges::views::take_while( [maxX = xPred + xWindow]( const PrHit& h ) { return h.x() <= maxX; } ),
        [xPred]( const PrHit& hit ) {
          const float d = hit.distanceXHit( xPred ); // fast distance good enough at this point (?!)
          return d * d * hit.w();
        },
        std::less<>{}, 1.e9f );
    if ( best ) {

      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << format( "AddHitOnEmptyXLayer:    chi2%8.2f", bestChi2 );
        printHit( *best, " " );
      }
      // -- < Debug --------

      track.addHit( *best );
      pc.addHit( *best );
      added = true;
    }
  }
  if ( !added ) return true;
  if ( fullFit ) { return fitXProjection( track, pars, pc ); }
  fastLinearFit( track, pars, pc );
  return true;
}
//=========================================================================
//  Add hits on empty stereo layers, and refit if something was added
//=========================================================================
template <typename T>
bool PrForwardTracking<T>::addHitsOnEmptyStereoLayers( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                                       const PrFTHitHandler<PrHit>& FTHitHandler,
                                                       const PrParameters& pars, PrPlaneCounter& pc ) const {
  // at this point pc is counting only stereo HITS!
  if ( pc.nbDifferent() > 5 ) return true;

  bool added = false;
  for ( unsigned int zoneNumber = 0; PrFTInfo::nbZones() > zoneNumber; zoneNumber += 1 ) {
    if ( pc.nbInPlane( zoneNumber / 2 ) != 0 ) continue;  // there is already one hit
    const auto& zone = m_zoneHandler->zone( zoneNumber ); // maybe we want it outside to be faster?
    if ( zone.isX() ) continue;                           // exclude X zones

    float zZone       = zone.z();
    float yZone       = track.y( zZone );
    zZone             = zone.z( yZone ); // Correct for dzDy
    yZone             = track.y( zZone );
    const float xPred = track.x( zZone );

    const bool triangleSearch = std::abs( yZone ) < m_tolYTriangleSearch;
    if ( !triangleSearch && ( 2.f * float( ( ( zoneNumber % 2 ) == 0 ) ) - 1.f ) * yZone > 0.f ) continue;

    // only version without triangle search!
    const float dxTol = m_tolY + m_tolYSlope * ( std::abs( xPred - track.seed().x( zZone ) ) + std::abs( yZone ) );
    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account
    const auto hits =
        FTHitHandler.getRange_lowerBound( zoneNumber, -dxTol - yZone * zone.dxDy() + xPred ) |
        ranges::views::take_while( [yZone, xMax = dxTol + xPred]( const PrHit& h ) { return h.x( yZone ) <= xMax; } );
    auto chi2 = [&]( const PrHit& h ) {
      auto d = h.distance( xPred, yZone );
      return d * d * h.w();
    };
    auto triangleNotOK = [ymax = yZone - m_yTolUVSearch, ymin = yZone + m_yTolUVSearch]( const PrHit& h ) {
      return ( ymax > h.yMax() ) || ( ymin < h.yMin() );
    };

    auto [bestChi2, best] = ( triangleSearch ? best_element( hits | ranges::views::remove_if( triangleNotOK ), chi2,
                                                             std::less<>{}, m_maxChi2Stereo.value() )
                                             : best_element( hits, chi2, std::less<>{}, m_maxChi2Stereo.value() ) );

    if ( best ) {
      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << format( "AddHitOnEmptyStereoLayer:    chi2%8.2f", bestChi2 );
        printHit( *best, " " );
        info() << "zZone: " << zZone << " xpred: " << xPred << " dxTol " << dxTol << endmsg;
      }
      // -- < Debug --------
      stereoHits.push_back( best );
      pc.addHit( *best );
      added = true;
    }
  }
  if ( !added ) return true;
  return fitYProjection( track, stereoHits, pars, pc );
}

//=========================================================================
//  Collect all hits in the stereo planes compatible with the track
//=========================================================================
template <typename T>
std::vector<ModPrHit> PrForwardTracking<T>::collectStereoHits( PrForwardTrack&              track,
                                                               const PrFTHitHandler<PrHit>& FTHitHandler ) const {
  std::vector<ModPrHit> stereoHits;
  stereoHits.reserve( PrFTInfo::nbZones() );

  // -- > Debug --------
  if ( msgLevel( MSG::DEBUG ) ) info() << "== Collecte stereo hits. wanted ones: " << endmsg;
  // -- < Debug --------

  for ( const auto& zoneNumber : PrFTInfo::stereoZones ) {
    const auto& zone  = m_zoneHandler->zone( zoneNumber );
    float       zZone = zone.z();
    const float yZone = track.y( zZone );
    zZone             = zone.z( yZone ); // Correct for dzDy
    const float xPred = track.x( zZone );

    const bool triangleSearch = std::abs( yZone ) < m_tolYTriangleSearch;
    if ( !triangleSearch && ( 2.f * float( ( zoneNumber % 2 ) == 0 ) - 1.f ) * yZone > 0.f ) continue;

    // float dxDySign = 1.f - 2.f *(float)(zone.dxDy()<0); // same as ? zone.dxDy()<0 : -1 : +1 , but faster??!!
    const float dxDySign = zone.dxDy() < 0 ? -1.f : 1.f;
    const float dxTol    = m_tolY + m_tolYSlope * ( std::abs( xPred - track.seed().x( zZone ) ) + std::abs( yZone ) );

    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account
    auto range =
        FTHitHandler.getRange_lowerBound( zoneNumber, -dxTol - yZone * zone.dxDy() + xPred ) |
        ranges::views::take_while( [yZone, xMax = xPred + dxTol]( const PrHit& h ) { return h.x( yZone ) <= xMax; } );

    auto triangleNotOK = [yMax = yZone - m_yTolUVSearch, yMin = yZone + m_yTolUVSearch]( const PrHit& h ) {
      return ( yMax > h.yMax() ) || ( yMin < h.yMin() );
    };
    if ( triangleSearch ) {
      for ( const PrHit& h : range | ranges::views::remove_if( triangleNotOK ) ) {
        const float dx = h.x( yZone ) - xPred;
        stereoHits.emplace_back( &h, dx * dxDySign, h.planeCode(), 0 );
      }
    } else { // no triangle search, thus no min max check
      for ( const PrHit& h : range ) {
        const float dx = h.x( yZone ) - xPred;
        stereoHits.emplace_back( &h, dx * dxDySign, h.planeCode(), 0 );
      }
    }
  }

  return stereoHits;
}
//=========================================================================
//  Fit the stereo hits
//=========================================================================
template <typename T>
bool PrForwardTracking<T>::selectStereoHits( PrForwardTrack& track, const PrFTHitHandler<PrHit>& FTHitHandler,
                                             const std::vector<ModPrHit>& allStereoHits,
                                             const PrParameters&          pars ) const {
  // why do we rely on xRef? --> coord is NOT xRef for stereo HITS!

  std::vector<const PrHit*> bestStereoHits;
  trackPars<3>              originalYParams = track.getYParams();
  trackPars<3>              bestYParams;
  float                     bestMeanDy    = 1e9f;
  const auto                minStereoHits = pars.minStereoHits;

  if ( minStereoHits > allStereoHits.size() ) return false; // otherwise crash if minHits is too large

  auto beginRange = std::begin( allStereoHits ) - 1;
  auto endLoop    = std::end( allStereoHits );
  while ( std::distance( beginRange, endLoop ) > minStereoHits ) {
    ++beginRange;

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << " stereo start at ";
      printHit( *( beginRange->hit ) );
    }
    // -- > Debug --------

    PrPlaneCounter pc; // counting now stereo hits
    auto           endRange = beginRange;
    float          sumCoord = 0.;
    while ( pc.nbDifferent() < minStereoHits || endRange->coord - std::prev( endRange )->coord < m_minYGap ) {
      pc.addHit( *endRange );
      sumCoord += endRange->coord;
      ++endRange;
      if ( endRange == allStereoHits.end() ) break;
    }

    // clean cluster
    while ( true ) {
      const float averageCoord = sumCoord / float( endRange - beginRange );

      // remove first if not single and furthest from mean
      if ( pc.nbInPlane( *beginRange ) > 1 && ( beginRange->coord + std::prev( endRange )->coord <
                                                2 * averageCoord ) ) { // tune this value has only little effect?!
        pc.removeHit( *beginRange );
        sumCoord -= ( beginRange++ )->coord;
        continue;
      }

      if ( endRange == allStereoHits.end() ) break; // already at end, cluster cannot be expanded anymore

      // add next, if it decreases the range size and is empty
      if ( pc.nbInPlane( *endRange ) == 0 && ( beginRange->coord + endRange->coord < 2 * averageCoord ) ) {
        pc.addHit( *endRange );
        sumCoord += ( endRange++ )->coord;
        continue;
      }

      break;
    }

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) {
      info() << "Selected stereo range from " << endmsg;
      printHit( *( beginRange->hit ) );
      printHit( *( ( endRange - 1 )->hit ) );
    }
    // -- < Debug --------

    // Now we have a candidate, lets fit him

    // track = original; //only yparams are changed
    track.setYParams( originalYParams );
    std::vector<const PrHit*> trackStereoHits;
    trackStereoHits.reserve( std::distance( beginRange, endRange ) );
    std::transform( beginRange, endRange, std::back_inserter( trackStereoHits ),
                    []( const ModPrHit& hit ) { return hit.hit; } );

    // fit Y Projection of track using stereo hits
    if ( !fitYProjection( track, trackStereoHits, pars, pc ) ) continue;

    if ( !addHitsOnEmptyStereoLayers( track, trackStereoHits, FTHitHandler, pars, pc ) ) continue;

    if ( trackStereoHits.size() < bestStereoHits.size() ) continue; // number of hits most important selection criteria!

    //== Calculate  dy chi2 /ndf
    float meanDy = ranges::accumulate( trackStereoHits, 0.f,
                                       [&track]( float m, const auto* hit ) {
                                         const float d = track.distance( *hit ) / hit->dxDy();
                                         return m + d * d;
                                       } ) /
                   float( trackStereoHits.size() - 1 );

    if ( trackStereoHits.size() > bestStereoHits.size() || meanDy < bestMeanDy ) {
      // if same number of hits take smaller chi2
      // -- > Debug --------
      if ( msgLevel( MSG::DEBUG ) ) {
        info() << "************ Store candidate, nStereo " << trackStereoHits.size() << " meanDy " << meanDy << endmsg;
      }
      // -- < Debug --------
      bestYParams    = track.getYParams();
      bestMeanDy     = meanDy;
      bestStereoHits = std::move( trackStereoHits );
    }
  }
  if ( !bestStereoHits.empty() ) {
    track.setYParams( bestYParams ); // only y params have been modified
    track.addHits( bestStereoHits );
    return true;
  }
  return false;
}

//=========================================================================
//  Convert the local track to the LHCb representation
//=========================================================================
template <typename T>
LHCb::Pr::Forward::Tracks PrForwardTracking<T>::makeLHCbTracks( PrForwardTracks const&                 trackCandidates,
                                                                std::vector<std::vector<LHCb::LHCbID>> ids,
                                                                T const& input_tracks ) const {
  auto [velo_ancestors, upstream_ancestors] = get_ancestors( input_tracks );
  LHCb::Pr::Forward::Tracks result( velo_ancestors, upstream_ancestors );

  for ( auto&& [cand, id] : Gaudi::Functional::details::zip::range( trackCandidates, ids ) ) {
    int const currentsize = result.size();
    if ( !cand.valid() ) continue;

    int uttrack = cand.track();

    if constexpr ( std::is_same_v<T, LHCb::Pr::Upstream::Tracks> ) {
      result.store_trackVP<I>( currentsize, input_tracks.template trackVP<I>( uttrack ) );
      result.store_trackUT<I>( currentsize, uttrack );
    } else {
      result.store_trackVP<I>( currentsize, uttrack );
      result.store_trackUT<I>( currentsize, -1 );
      // only used to disable unused warning in the velo track input case
      uttrack = input_tracks.size();
    }

    const double qOverP = cand.getQoP();

    result.store_stateQoP<F>( currentsize, qOverP );

    LHCb::State tState;
    float const z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::Location::AtT );
    tState.setState( cand.x( z ), cand.y( z ), z, cand.xSlope( z ), cand.ySlope( z ), qOverP );

    auto pos = Vec3<F>( tState.x(), tState.y(), tState.z() );
    auto dir = Vec3<F>( tState.tx(), tState.ty(), 1.f );

    result.store_statePos<F>( currentsize, pos );
    result.store_stateDir<F>( currentsize, dir );

    if ( m_addUTHitsTool.isEnabled() && !m_useMomentumEstimate ) { // FIXME switch off if input_tracks tracks as input
      double      chi2{0};
      LHCb::State vState;
      vState.setState( cand.seed().x0, cand.seed().y0, cand.seed().z0, cand.seed().tx, cand.seed().ty, qOverP );
      auto uthits = m_addUTHitsTool->returnUTHits( vState, chi2, vState.p() );
      // There are candidates with more than 8 UT hits. To be understood. Better protect this....
      if ( uthits.size() < 3 || uthits.size() > 20 ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << " Failure in adding UT hits to track" << endmsg;
      } else {
        for ( auto const hit : uthits ) id.emplace_back( hit.HitPtr->chanID() );
        std::sort( id.begin(), id.end() );
      }
    }

    //== LHCb ids.
    for ( size_t idx{0}; idx < id.size(); ++idx ) { result.store_hit<I>( currentsize, idx, id[idx].lhcbID() ); }
    result.store_nHits<I>( currentsize, id.size() );

    result.size() += 1;

    // -- > Debug --------
    if ( msgLevel( MSG::DEBUG ) ) info() << "Store track  quality " << cand.quality() << endmsg;
    // -- < Debug --------
  } // next candidate
  return result;
}

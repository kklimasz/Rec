/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrFittedForwardTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/TrackWithMuonPIDSkin.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Functors/Function.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "GaudiAlg/Transformer.h"
#include "PrKernel/PrSelection.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipSelection.h"
#include "SelKernel/TrackZips.h"
#include <vector>

namespace {
  constexpr static auto                 DefaultFunctorCode    = "Functors::AcceptAll{}";
  constexpr static auto                 DefaultFunctorRepr    = "ALL";
  static std::vector<std::string> const DefaultFunctorHeaders = {"Functors/Function.h"};

  // Helper class used to collect the type names we need to construct LoKi functors for various objects
  template <typename T>
  struct TypeHelper {};

  template <>
  struct TypeHelper<::LHCb::Pr::Velo::Tracks> {
    using InputType                    = ::LHCb::Pr::Velo::Tracks const&;
    using OutputType                   = ::LHCb::Pr::Velo::Tracks;
    constexpr static bool WrapOutput   = false;
    constexpr static auto ExtraHeaders = {"Event/PrIterableVeloTracks.h"};
  };

  template <>
  struct TypeHelper<::LHCb::Pr::Fitted::Forward::Tracks> {
    using InputType                    = ::LHCb::Pr::Fitted::Forward::Tracks const&;
    using OutputType                   = ::LHCb::Pr::Fitted::Forward::Tracks;
    constexpr static bool WrapOutput   = false;
    constexpr static auto ExtraHeaders = {"Event/PrIterableFittedForwardTracks.h"};
  };

  template <>
  struct TypeHelper<LHCb::Pr::Fitted::Forward::TracksWithPVs> {
    using InputType = LHCb::Pr::Fitted::Forward::TracksWithPVs const&;
    // TODO provide a nicer way of deducing this type
    // it could have a backend something like std::declval<InputType>().filter( [](auto const&){ return true; } )
    using OutputType = LHCb::Pr::detail::merged_t<LHCb::Pr::Fitted::Forward::Tracks, BestVertexRelations>;
    constexpr static bool WrapOutput   = true;
    constexpr static auto ExtraHeaders = {"SelKernel/TrackZips.h"};
  };

  template <>
  struct TypeHelper<LHCb::Pr::Fitted::Forward::TracksWithMuonID> {
    using InputType = LHCb::Pr::Fitted::Forward::TracksWithMuonID const&;
    // TODO provide a nicer way of deducing this type
    // it could have a backend something like std::declval<InputType>().filter( [](auto const&){ return true; } )
    using OutputType = LHCb::Pr::detail::merged_t<LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
    constexpr static bool WrapOutput   = true;
    constexpr static auto ExtraHeaders = {"SelKernel/TrackZips.h"};
  };

  template <>
  struct TypeHelper<LHCb::Event::v1::Track> {
    // We could template on this too, if we want to filter on both vector<Track> and Pr::Selection<Track>
    using OutputType                   = Pr::Selection<LHCb::Event::v1::Track>;
    using InputType                    = OutputType const&;
    constexpr static bool WrapOutput   = false;
    constexpr static auto ExtraHeaders = {"Event/Track_v1.h"};
  };

  template <>
  struct TypeHelper<LHCb::Event::v2::Track> {
    // We could template on this too, if we want to filter on both vector<Track> and Pr::Selection<Track>
    using OutputType                   = Pr::Selection<LHCb::Event::v2::Track>;
    using InputType                    = OutputType const&;
    constexpr static bool WrapOutput   = false;
    constexpr static auto ExtraHeaders = {"Event/Track_v2.h"};
  };

  // Type names for filtering on a TrackWithMuonID view
  struct TrackWithMuonID {};
  template <>
  struct TypeHelper<TrackWithMuonID> {
    // These should probably all be defined elsewhere
    using TrackContainer  = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackSkin>>;
    using MuonIDContainer = Zipping::ZipContainer<SOA::Container<std::vector, LHCb::Event::v2::MuonID>>;
    using TrackMuonIDView =
        Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>;

    using InputType                    = TrackMuonIDView const&;
    using SelViewType                  = Zipping::SelectionView<const TrackMuonIDView>;
    using OutputType                   = Zipping::ExportedSelection<>;
    using PredicateType                = Functors::Functor<OutputType( SelViewType const& )>;
    constexpr static auto ExtraHeaders = {"Event/MuonPID_v2.h", "Event/TrackWithMuonPIDSkin.h",
                                          "SOAExtensions/ZipSelection.h", "SOAContainer/SOAContainer.h"};
  };

  // Given the tag type T, TypeHelper tells us both the return type of the
  // functor (OutputType) and whether this return type is to be converted into
  // a non-owning view (WrapOutput), which means that we can deduce the output
  // type of the convert() function too, just using the tag type T. This is the
  // return type of operator() in the algorithm. We also need to deduce that
  // type with the leading bool stripped off, so that we can write down the
  // name of the functional framework base class that the algorithm inherits
  // from
  template <typename TagType>
  struct OutputHelper {
    static constexpr bool WrapOutput = TypeHelper<TagType>::WrapOutput;
    using FilteredType               = typename TypeHelper<TagType>::OutputType;

    template <typename KeyValue>
    static auto names() {
      if constexpr ( WrapOutput ) {
        return std::array<KeyValue, 2>{{KeyValue{"Output", ""}, KeyValue{"OutputStorage", ""}}};
      } else {
        return KeyValue{"Output", ""};
      }
    }

    static auto convert( bool filter_passed, FilteredType&& filtered ) {
      if constexpr ( WrapOutput ) {
        // OutputType is some owning type that we don't want to use directly, but
        // which we need to keep alive so that we can use a non-owning view into
        // it. Achieve this by wrapping it up in a unique_ptr -- so the address
        // of the contained object is stable -- and returning that.
        auto storage = std::make_unique<FilteredType>( std::move( filtered ) );
        // Make a non-owning view, which will store the address of the object
        // hidden inside the unique_ptr.
        auto view = LHCb::Pr::make_zip( *storage.get() );
        return std::tuple{filter_passed, std::move( view ), std::move( storage )};
      } else {
        return std::tuple{filter_passed, std::move( filtered )};
      }
    }

    // this is std::tuple<bool, A[, B]>
    using AlgorithmOutput = std::invoke_result_t<decltype( convert ), bool, FilteredType>;

    // helper to strip off the bool from the type
    template <typename T>
    struct remove_first_type {};

    template <typename T, typename... Ts>
    struct remove_first_type<std::tuple<T, Ts...>> {
      using type = std::tuple<Ts...>;
    };

    // this is std::tuple<A[, B]>
    using DataTuple = typename remove_first_type<AlgorithmOutput>::type;
  };

  // Just shorthand for below
  template <typename T>
  using FilterTransform = Gaudi::Functional::MultiTransformerFilter<typename OutputHelper<T>::DataTuple(
      typename TypeHelper<T>::InputType )>;

  template <typename T>
  using SOAFilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple<typename TypeHelper<T>::OutputType>(
      typename TypeHelper<T>::InputType, Zipping::ExportedSelection<> const& )>;
} // namespace

namespace Pr {
  /** @class Filter PrFilter.cpp
   *
   *  Filter<T> applies a selection to an input Selection<T> and returns a new Selection<T> object.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). By contruction this is not copied, as the
   *            input/output type Selection<T> is just a view of some other underlying storage.
   */
  template <typename T>
  class Filter final : public FilterTransform<T> {
  public:
    using Base = FilterTransform<T>;
    using Base::debug;
    using Base::info;
    using Base::msgLevel;
    using KeyValue         = typename Base::KeyValue;
    using OHelper          = OutputHelper<T>;
    using Helper           = TypeHelper<T>;
    using InputType        = typename Helper::InputType;
    using FilterOutputType = typename Helper::OutputType;

    // This is a type-erased exchange type that only depends on the input and output types.
    using Predicate = Functors::Functor<FilterOutputType( InputType )>;

    Filter( const std::string& name, ISvcLocator* pSvcLocator )
        : Base( name, pSvcLocator, {"Input", ""}, OHelper::template names<KeyValue>() ) {}

    // Return type is std::tuple<bool, A[, B]>
    typename OHelper::AlgorithmOutput operator()( InputType in ) const override {
      // Apply the functor to get something we can return
      auto filtered = this->m_pred( in );

      // Update the statistics. Maybe the interface has changed and we can do this more elegantly?
      auto buffer = m_cutEff.buffer();
      for ( decltype( in.size() ) i = 0; i < in.size(); ++i ) { buffer += i < filtered.size(); }

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      // Add an extra conversion step if requested
      return OHelper::convert( filter_pass, std::move( filtered ) );
    }

    StatusCode initialize() override {
      auto sc = FilterTransform<T>::initialize();
      decode();
      return sc;
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

    // This is the functor cut string
    Gaudi::Property<ThOr::FunctorDesc> m_cut{
        this, "Cut", {DefaultFunctorCode, DefaultFunctorHeaders, DefaultFunctorRepr}, [this]( auto& ) {
          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
          this->decode();
        }};

    // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
    // I think this has some overhead, but it may be avoidable...TBC
    Predicate m_pred;

    // This is the factory tool that handles the functor cache and/or the just-in-time compilation
    ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory/PrTrackFunctorFactory"};

    void decode() {
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Pr::Filter<T>::decode()" << endmsg; }
      m_factory.retrieve().ignore();
      auto headers = m_cut.value().headers;
      for ( auto const& h : TypeHelper<T>::ExtraHeaders ) { headers.push_back( h ); }
      m_pred = m_factory->get<Predicate>( this, m_cut.value().code, headers );
    }
  };

  template <typename T>
  class SOAFilter final : public SOAFilterTransform<T> {
  public:
    using SOAFilterTransform<T>::info;
    using SOAFilterTransform<T>::debug;
    using SOAFilterTransform<T>::msgLevel;
    using InputType  = typename TypeHelper<T>::InputType;
    using OutputType = typename TypeHelper<T>::OutputType;
    using Predicate  = typename TypeHelper<T>::PredicateType;
    using KeyValue   = typename SOAFilterTransform<T>::KeyValue;

    SOAFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : SOAFilterTransform<T>( name, pSvcLocator, {KeyValue{"Input", ""}, KeyValue{"InputSelection", ""}},
                                 KeyValue{"Output", ""} ) {}

    std::tuple<bool, OutputType> operator()( InputType in, Zipping::ExportedSelection<> const& in_sel ) const override {
      // Apply the input selection to our input data
      auto selected_input = Zipping::SelectionView{&in, in_sel};

      // Apply the functor to get the refined selection
      auto filtered = this->m_pred( selected_input );

      // Update the statistics. Maybe the interface has changed and we can do this more elegantly?
      auto buffer = m_cutEff.buffer();
      for ( auto i = 0ul; i < selected_input.size(); ++i ) { buffer += i < filtered.size(); }

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      return {filter_pass, std::move( filtered )};
    }

    StatusCode initialize() override {
      auto sc = SOAFilterTransform<T>::initialize();
      decode();
      return sc;
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

    // This is the functor cut string
    Gaudi::Property<ThOr::FunctorDesc> m_cut{
        this, "Cut", {DefaultFunctorCode, DefaultFunctorHeaders, DefaultFunctorRepr}, [this]( auto& ) {
          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
          this->decode();
        }};

    // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
    // I think this has some overhead, but it may be avoidable...TBC
    Predicate m_pred;

    // This is the factory tool that handles the functor cache and/or the just-in-time compilation
    ServiceHandle<Functors::IFactory> m_factory{this, "Factory", "FunctorFactory/PrTrackFunctorFactory"};

    void decode() {
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Pr::SOAFilter<T>::decode()" << endmsg; }
      m_factory.retrieve().ignore();
      auto headers = m_cut.value().headers;
      for ( auto const& h : TypeHelper<T>::ExtraHeaders ) { headers.push_back( h ); }
      m_pred = m_factory->get<Predicate>( this, m_cut.value().code, headers );
    }
  };

  // Pr::Selection<v1::Track> -> Pr::Selection<v1::Track>
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::v1::Track>, "PrFilter__Track_v1__NewFunctors" )

  // Pr::Selection<v2::Track> -> Pr::Selection<v2::Track>
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::v2::Track>, "PrFilter__Track_v2" )

  // LHCb::Pr::Velo::Tracks -> LHCb::Pr::Velo::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Velo::Tracks>, "PrFilter__PrVeloTracks" )

  // LHCb::Pr::Fitted::Forward::Tracks -> LHCb::Pr::Fitted::Forward::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::Tracks>, "PrFilter__PrFittedForwardTracks" )

  // LHCb::Pr::Fitted::Forward::TracksWithPVs
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                             "PrFilter__PrFittedForwardTracksWithPVs" )

  // LHCb::Pr::Fitted::Forward::TracksWithMuonID
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                             "PrFilter__PrFittedForwardTracksWithMuonID" )

  // SOA::Container: refine a selection on a view
  DECLARE_COMPONENT_WITH_ID( SOAFilter<TrackWithMuonID>, "SOAFilter__TrackWithMuonIDView" )
} // namespace Pr

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "PrKernel/PrSelection.h"
#include "boost/algorithm/string/join.hpp"
#include <vector>

namespace {
  // Just shorthand for below
  template <typename Track, typename Vertex>
  using FilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple<Pr::Selection<Track>>(
      Pr::Selection<Track> const&, std::vector<Vertex> const& )>;
} // namespace

namespace Pr {
  /** @class FilterIP PrFilterIP.cpp
   *
   *  FilterIP<T> applies a selection to an input Selection<T> and returns a new Selection<T> object.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). By contruction this is not copied, as the
   *            input/output type Selection<T> is just a view of some other underlying storage.
   */
  template <typename Track, typename Vertex>
  class FilterIP final : public FilterTransform<Track, Vertex> {
  public:
    using KeyValue = typename FilterTransform<Track, Vertex>::KeyValue;

    FilterIP( const std::string& name, ISvcLocator* pSvcLocator )
        : FilterTransform<Track, Vertex>(
              name, pSvcLocator,
              {KeyValue{"Input", ""}, KeyValue{"InputVertices", LHCb::Event::v2::RecVertexLocation::Velo3D}},
              {KeyValue{"Output", ""}} ) {}

    std::tuple<bool, Selection<Track>> operator()( Selection<Track> const&    in,
                                                   std::vector<Vertex> const& vertices ) const override {
      auto buffer            = m_cutEff.buffer();
      auto pred_with_counter = [&buffer, &vertices, min_ip2 = std::pow( m_ipcut.value(), 2 )]( auto const& track ) {
        auto dec = !track.checkFlag( Track::Flag::Backward );
        dec =
            dec && std::none_of( std::begin( vertices ), std::end( vertices ), [&track, min_ip2]( auto const& vertex ) {
              const auto& pos   = vertex.position();
              const auto& state = track.closestState( pos.Z() );
              auto        line  = Gaudi::Math::Line{state.position(), state.slopes()};
              auto        ipvec = Gaudi::Math::closestPoint( pos, line ) - pos;
              return ipvec.Mag2() < min_ip2;
            } );

        buffer += dec; // record selection efficiency
        return dec;
      };

      // Make a new Selection by applying an extra cut to 'in'
      auto filtered = in.select( pred_with_counter );

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      return {filter_pass, std::move( filtered )};
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

    // This is the IP cut value
    Gaudi::Property<float> m_ipcut{this, "IPcut", 0.0};
  };

  using FilterIP_v2 = FilterIP<LHCb::Event::v2::Track, LHCb::Event::v2::RecVertex>;
  DECLARE_COMPONENT_WITH_ID( FilterIP_v2, "PrFilterIP__Track_v2__RecVertex_v2" )
} // namespace Pr

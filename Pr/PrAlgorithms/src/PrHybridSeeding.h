/*
  LoH:
  Change the removes for non-sorted tracks
  Remove sorting
  What the hell happens with fillX?
  PrHybridSeedTrack is inadapted

 */
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRHYBRIDSEEDING_H
#define PRHYBRIDSEEDING_H 1

#include "Event/MCParticle.h"
#include "Event/StateParameters.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "PrHybridSeedTrack.h"
#include "PrKernel/IPrDebugTool.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "TfKernel/RecoFuncs.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"

#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"

#include <boost/container/static_vector.hpp>

// Should we really have the typedefs here and not in a namaespace?
//---LoH: ModPrHits is a vector of hits and not pointers.
//===== This defines the depth for 4/5/6 uv-hits in the stored
//===== clusters the depth at which you store them
using HitIter = PrFTHitHandler<ModPrHit>::HitIter;
struct IterPairs {
  HitIter begin;
  HitIter end;
};
typedef std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers> Boundaries;
typedef std::array<std::array<IterPairs, 2>, PrFTInfo::Numbers::NFTXLayers / 2>
                                                           BoundaryUV;   // BoundariesUV[U/V][T1,T2,T3][part]
typedef std::array<BoundaryUV, 2>                          BoundariesUV; // BoundariesUV[U/V][T1,T2,T3][part]
typedef std::array<std::vector<Pr::Hybrid::SeedTrack>, 2>  TrackCandidates;
typedef std::array<std::vector<Pr::Hybrid::SeedTrackX>, 2> XCandidates;
typedef std::array<std::vector<Pr::Hybrid::SeedTrackX>, 2> TrackToRecover;

// Precalculated constants for a x-search in a given case
struct ZonesXSearch {
  float                                                   invZf;
  float                                                   invZlZf;
  std::array<unsigned int, PrFTInfo::Numbers::NFTXLayers> zones;
  std::array<unsigned int, PrFTInfo::Numbers::NFTXLayers> planeCode;
  std::array<float, PrFTInfo::Numbers::NFTXLayers>        zLays;
  std::array<float, PrFTInfo::Numbers::NFTXLayers>        dzLays;
  std::array<float, PrFTInfo::Numbers::NFTXLayers>        dz2Lays;
};

// Parameters corresponding to a single two-hit combination in X
struct TwoHitCombination {
  float tx;
  float x0;
  float x0new;
  float minPara;
  float maxPara;
};
// Parameters corresponding to a pre-calculated parabola
struct ParabolaParams {
  float z1;
  float z2;
  float z3;
  float corrZ1;
  float corrZ2;
  float corrZ3;
  float det1;
  float det2;
  float det3;
  float dz13;
  float dcorr13;
  float dzcorr13;
  float det;
  float recdet;
};

// Stations Zones numbering
// this should be done in a better way (e.g. in FTHitHandler)
namespace {
  constexpr unsigned int T1X1 = 1;
  constexpr unsigned int T1U  = 3;
  constexpr unsigned int T1V  = 5;
  constexpr unsigned int T1X2 = 7;
  constexpr unsigned int T2X1 = 9;
  constexpr unsigned int T2U  = 11;
  constexpr unsigned int T2V  = 13;
  constexpr unsigned int T2X2 = 15;
  constexpr unsigned int T3X1 = 17;
  constexpr unsigned int T3U  = 19;
  constexpr unsigned int T3V  = 21;
  constexpr unsigned int T3X2 = 23;
  // Physics constants
  constexpr float m_momentumScale = 35.31328;

  // Former options now (very) unlikely to change
  constexpr unsigned int maxNHits = 12;
  // Compiler option
  constexpr unsigned int maxXCandidates  = 800;
  constexpr unsigned int maxCandidates   = 500;
  constexpr unsigned int maxParabolaHits = 64;
  constexpr unsigned int DEPTH_HOUGH     = 3;
} // namespace

//---LoH: Has to be included only now...
#include "PrLineFitterY.h"

/** @class PrHybridSeeding PrHybridSeeding.h
 *  Stand-alone seeding for the FT T stations
 *  Used for the upgrade tracker TDR
 *  - OutputName : Name of the output container for the seed tracks. By Default it's LHCb::TrackLocation::Seed
 *  - NCases : Number of Cases for the algorithm ( value must be <=3 )
 *  - SlopeCorr : False by default. It change the errors on the hits from 1./err => 1./(err*cos(tx)*cos(tx))
 *  - maxNbestCluster[Case]: Amount of first N best clusters to process for each Case
 *  - MaxNHits : Force the algorithm to find tracks with at maximum N Hits ( N hits = N layers )
 *  - RemoveClones : Flag that allow to run the global clones removal ( true by default )
 *  - minNCommonUV : Number of common hits in the global clone removal step
 *  - RemoveClonesX : Flag that allow to run the intermediate clone killing for x-z projection
 *  - FlagHits : Flag that allow to flag the hits on track found by each Case
 *  - RemoveFlagged : If set to true, whatever flagged hits found by the Case-i is not used in Case-j , where j>i
 *  - UseCubicCorrection: Modify the track model for x(z) = ax+bx*dz + cx*dz*dz*(1+dRatio*dz)
 *  - dRatio : dRatio value
 *  - CConst : Constant to compute the backward projection x(0) ~ a_x - b_x * zref + cx * C ;
               C = Integral (0, zRef) Integral (0, z) By(z') * dz * dz'
 *  - UseCorrPosition : Correct the position for the simultaneous fit using the yOnTrack shift , i.e. z(y) instead of
 z(0);
 *  - SizeToFlag[Case] : Tracks with NHits>=SizeToFlag will have the hits flagged
 *  - Flag_MaxChi2DoF_11Hits[Case] : If Hits<12 Flag only hits on track having Chi2DoF<Flag_MaxChi2DoF_11Hits[Case]
 *  - Flag_MaxX0_11Hits[Case] : If Hits<12 Flag only hits on track having |x0(backProjection)| < Flag_MaxX0_11Hits[Case]
 *
 *  ----------------------------- Parameters x-z projection search
 *  - 2-hit combination from T1-x + T3-x : given txinf = xT1/ZT1
 *  - L0_AlphaCorr[Case]  : Rotation angle obtained looking to txinf vs ( Delta ) , where Delta = xT3(True) -
 xT1+txinf(zT3-zT1).
 *  - L0_tolHp[Case]      : After rotating Delta' =  Delta + L0_alphaCorrd[Case] vs tx_inf,
 *                          L0_alphaCorr[Case]*txinf - L0_tolHp[Case] <  xT3  <  L0_alphaCorr[Case]*txinf +
 L0_tolHp[Case]
 *  - 3-hit combination given straight line joining T1X and T3X. x0 is the straight line prediction
 *    from the two picked hits at z=0.
 *  - tx_picked = (xT3-xT1)/(zT3-zT1). x0 = xT1 - zT1 * tx_picked
 *  - xPredT2 = x0 + zT2 * tx_picked ( linear prediction from the 2 hit combination )
 *  - x0Corr[Case] : defines a new xPredT2' = xPredT2 + x0*x0Corr[Case]
 *                   ( rotation in the plane (xTrue - xPredT2) vs x0 to allign for the tolerances.
 *  --- considering only x0>0 ( equal by symmetry  for x0 <0)
 *      ( see https://indico.cern.ch/event/455022/contribution/2/attachments/1186203/1719828/main.pdf  for reference )
 *  - X0SlopeChange[Case] : value of x0 at which start to open a larger upper tolerance ( max )
 *  - z0SlopeChangeDown[Case] : value of x0 at which start to open a larger lower toleance ( min )
 *  - ToleranceX0Up[Case] : upper tolerance when x0< X0SlopeChange[Case]
 *  - ToleranceX0Down[Case] : lower tolerance when x0 < X0SlopeChangeDown[Case]
 *  - x0Cut[Case] : Value of X0 where to define the new tolerance up and down
 *                  ( which then implicitely imply the opening tolerance up to X0SlopeChange(Down)[Case].
 *                    Must be > X0SlopeChange(Down) )
 *  - TolAtX0CutOpp[Case] : lower tolerance for the deviation to collect hits in T2 at x0Cut from the xPredT2'
 *  - TolAtX0Cut[Case]     : upper tolerance for the deviation to collect hits in T2 at x0Cut from the xPredT2'
 *  - maxParabolaSeedHits: max number of hits to process collected in T2 given a 2 hit combination.
 *  ------ Collect remaining layers once a 3 hit combination is formed
 *  - TolXRemainign[Case] : from 3 hit the parabola ( + cubic correction ) is computed and the remaining
 *                          xlayers hits are collected if  the hits are found in within
 *                          TolXRemaining ( | hit::x(0 ) - xPred | < tolXPremaining )
 *  ----- Track is fitted in this scheme:
 *  - maxChi2HitsX[Case] : if Max Chi2(Hit) > maxChi2HitsX[Case] fit is failed and the outliers removed down to
 m_minXPlanes hits
 *  - maxChi2DoFX[Case] : max value of the Chi2 per DoF of the xz projections for each Case
 *  -------------------------------UV Search
 *  - Collect compatible hits in UV layers:
 *  - HoleShape  : NoHole (do not account for the hole), Circular (see HoleRadius), or Rectangular (see HoleWidthX)
 *  - HoleRadius : remove hits found to have sqrt( x*x + y*y ) < HoleRadius (Circular geometry)
 *  - HoleWidthX : remove hits found to have (abs(x) < HoleWidthX and abs(y) < HoleWidthY) (Rectangular geometry)
 *  - HoleWidthY : see HoleWidthX
 *  - Positive defined tolerances for the y search  ( swapped when looking to lower module )
 *  - yMin : yMin Value to collect compatible hits in stereo
 *  - yMax : yMax Value to collect compatible hits in stereo    { upper track search in upper modyules : yMin < y < yMax
 }
 *  - yMin_TrFix : y Min Value to collect compatible hits in stereo when triangle fix is on
 *  - yMax_TrFix : y Max Value to collect compatible hits in stereo when triangle fix is on
 *                  { upper track search in lower modules yMin_TrFix < t < yMax_TrFix
 *  - DoAsym : do asymmetric hit search taking into account stereo layers
 *  - TriangleFix : use triangle fixing
 *  - TriangleFix2ndOrder : use the info in Hit::yMax and Hit::yMin
 *                          to remove the hits in upper modules in upper track leaking to y<0
 *  -----------------------------Select hough cluster
 *  - UseLineY : Do a preliminary selection of the clusters fitting for a Line in Y
 *  - minUV6[Case] : minUVLayers when XZ candidate has 6 hits  (planes)
 *  - minUV5[Case] : minUVLayers when XZ candidate has 5 hits  (planes)
 *  - minUV4[Case] : minUVLayers when XZ candidate has 4 hits  (planes)
 *  - Chi2LowLine[Case]  : if NXZ + NUV (Planes) <=10  XZChi2DoF + YLineChi2DoF < Chi2LowLine
 *  - Chi2HighLine[Case] : if NXZ + NUV (Planes) >10 XZChi2DoF + YLineChi2DoF < Chi2HighLine
 *  - minTot[Case]       : remove outliers until reaching minToT[Case] hits
 *  - Hough like cluster selection : select cluster ( sorted by |y/z| ) if |y/z| last
 *                                                    - |y/z| first < TolTyOffset + TolTySlope * |y/z| first,
 *                                                    where |y/z| first is always < than |y/z| last
 *  - TolTyOffset[Case]
 *  - TolTySlope[Case]
 *  --------------------------- Simultaneously fitting of the tracks
 *  - maxChi2Hits_11and12Hit[Case] : N Layers > 10 ( 11 and 12 hits ) outliers removed if MaxChi2Hit >
 maxChi2Hits_11and12Hit
 *  - maxChi2Hits_less11Hit[Case]  : N Layers < 11 (9,10) outliers removed if MaxChi2Hit< maxChi2Hits_less11Hit
 *  - maxYatZeroLow[Case]    :  If N Layers < 11: kill tracks having y(z=0) > maxYatZeroLow[Case]
 *                              ( important for ghost suppression )
 *  - maxYatzRefLow[Case]    : If N Layers < 11 : kill tracks having y(zRef)> maxYatzRefLow [Case ]
 *                             ( important for ghost suppression )
 *  - maxChi2PerDoF[Case]    : Max Chi2 per DoF of found tracks.
 *  @author Renato Quagliani
 *  @date   2015-03-11
 */

class PrHybridSeeding
    : public Gaudi::Functional::Transformer<std::vector<LHCb::Event::v2::Track>( const PrFTHitHandler<PrHit>& )> {
  using Track = LHCb::Event::v2::Track;

public:
  /// Standard constructor
  PrHybridSeeding( const std::string& name, ISvcLocator* pSvcLocator );
  /// initialization
  virtual StatusCode initialize() override;
  /// main execution
  std::vector<Track> operator()( const PrFTHitHandler<PrHit>& ) const override;

protected:
  inline PrFTHitHandler<ModPrHit> makeHitHandler( const PrFTHitHandler<PrHit>& FTHitHandler ) const noexcept;

  /** @brief Collect Hits in X layers producing the xz projections
   * @param part (if 1, y<0 ; if 0 , y>0)
   */
  inline void findXProjections( unsigned int part, unsigned int iCase, const PrFTHitHandler<ModPrHit>& hitHandler,
                                XCandidates& xCandidates ) const noexcept;
  /** @brief Internal method to construct parabolic parametrisation + cubic
      correction with available information on z positions.
   */
  inline void buildParabola( const ZonesXSearch& xZone, unsigned int iLayer, ParabolaParams& params ) const noexcept;

  /** @brief Updates the parabola solver when x1 and x3 informations get known
   */
  inline void updateParabola( const float& x1, const float& x3, ParabolaParams& params ) const noexcept;

  /** @brief Solves the parabola for a given x2
   */
  inline void solveParabola( const ParabolaParams& params, const float& x2, float& a1, float& b1, float& c1 ) const
      noexcept;

  /** @brief Initialises parameters for X-hit finding
   */

  inline void initializeXProjections( unsigned int iCase, unsigned int part, ZonesXSearch& xZones ) const noexcept;

  /** @brief Updates parameters for each XZ pair
   */

  inline void updateXZCombinationPars( const unsigned int& iCase, const float& xFirst, const float& xLast,
                                       const ZonesXSearch& xZones, const float& slope, const float& slopeopp,
                                       const float& accTerm1, const float& accTerm2, TwoHitCombination& hitComb ) const
      noexcept;

  /** @brief Finds all hits corresponding to a possible parabola
   */
  inline bool findParabolaHits( const unsigned int& iLayer, const ZonesXSearch& xZones,
                                const TwoHitCombination&                                  hitComb,
                                const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones, Boundaries& Bounds,
                                std::vector<ModPrHit>& parabolaSeedHits ) const noexcept;

  /** @brief Fills hits from the first parabola layer
   */
  inline void fillXhits0( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit, const ModPrHit& Phit,
                          const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                          const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                          const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones, XCandidates& xCandidates,
                          std::vector<ModPrHit>& parabolaSeedHits, Boundaries& Bounds,
                          std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev, ParabolaParams& Pars ) const
      noexcept;

  /** @brief Fills hits from the second parabola layer
   */
  inline void fillXhits1( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit, const ModPrHit& Phit,
                          const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                          const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                          const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones, XCandidates& xCandidates,
                          Boundaries& Bounds, std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev,
                          ParabolaParams& Pars ) const noexcept;

  /** @brief Creates a X track from xHits
   */
  inline void createXTrack( const unsigned int& iCase, const unsigned int& part, XCandidates& xCandidates,
                            SeedTrackHitsX& xHits ) const noexcept;

  /** @brief Finds hit corresponding to the missing parabola layer
   */
  inline bool fillXhitParabola( const int& iCase, const int& iLayer, SeedTrackHitsX& xHits, const float& xAtZ,
                                const std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>& Bounds ) const noexcept;

  /** @brief Finds hit corresponding to remaining layers
   */
  inline void fillXhitRemaining( const int& iCase, const int& iLayer, const float& xAtZ,
                                 const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                 const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                 SeedTrackHitsX& xHits, std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>& Bounds,
                                 std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev ) const noexcept;

  /** @brief Fit the track combining the only in the XZ plane
   *  @param track The track to fit
   *  @param Refit Iteration in the Refitting after removal worst hit
   *  @return bool Success of the XZ Fit
   **/
  inline bool fitXProjection( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept;

  /** @brief Remove the hit which gives the largest contribution to the chi2 and refit XZ
   *  @param track The track to fit
   *  @return bool Success of the fit
   */
  inline bool removeWorstAndRefitX( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept;

  /** @brief Add Hits from Stereo layers on top of the x-z projections found.
      The hough clusters (collection of hits having ~same y/z
      are pre-stored in a 3 x DEPTH_HOUGH matrix
  */
  inline void addStereo( unsigned int part, unsigned int iCase, const PrFTHitHandler<ModPrHit>& FTHitHandler,
                         TrackCandidates& trackCandidates, XCandidates& xCandidates,
                         TrackToRecover& trackToRecover ) const noexcept;

  /** @brief Add Hits from Stereo layers on top of the x-z projections found.
      The hough clusters (collection of hits having ~same y/z
      are pre-stored in a 3 x DEPTH_HOUGH matrix
  */
  inline void addStereoReco( unsigned int part, unsigned int iCase, const PrFTHitHandler<ModPrHit>& FTHitHandler,
                             TrackCandidates& trackCandidates, XCandidates& xCandidates ) const noexcept;

  /** @brief Creates the full tracks from the Hough clusters
   */
  inline bool createTracksFromHough( const unsigned int& iCase, std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                     const Pr::Hybrid::SeedTrackX& xCandidate, PrLineFitterY& BestLine,
                                     StereoIter itBeg[3][DEPTH_HOUGH], StereoIter itEnd[3][DEPTH_HOUGH],
                                     const StereoHits& myStereo ) const noexcept;

  /** @brief Creates the full tracks from the Hough clusters
   */
  inline void createTracksFromHoughReco( const unsigned int& iCase, std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                         const Pr::Hybrid::SeedTrackX& xCandidate, PrLineFitterY& BestLine,
                                         StereoIter itBeg[3][DEPTH_HOUGH], StereoIter itEnd[3][DEPTH_HOUGH],
                                         const StereoHits& myStereo ) const noexcept;

  /** @brief Given two iterators of a sorted range (itBeg, itEnd)
      in the Stereo compatible hits sorted by ty = y/z the hough cluster
      is extended in forward direction (itEnd++) until the tolerance is satisfied and you
      reach 6 fired layers. Also the number of different layers in the cluster is returned
  */
  inline void ExtendCluster( StereoIter& itBeg, StereoIter& itEnd, const StereoIter& stereoEnd, unsigned int iCase,
                             unsigned int& nLay ) const noexcept;

  /** @brief Given two iterators of a sorted range (itBeg, itEnd)
      in the Stereo compatible hits sorted by ty = y/z the hough cluster
      is extended in forward direction (itEnd++) until the tolerance is satisfied and you
      reach 6 fired layers. Also the number of different layers in the cluster is returned
  */
  inline void ExtendClusterReco( StereoIter& itBeg, StereoIter& itEnd, const StereoIter& stereoEnd,
                                 unsigned int& nLay ) const noexcept;

  /** @brief Collect Hits in UV layers given the tolerances
   * @param xProje x-z plane track projection
   * @param uvZones UV Layers where to look the hits
   * @return vector of u-v hits compatible with y tolerances
   */
  inline void CollectUV( unsigned int part, const Pr::Hybrid::SeedTrackX& xProje, const BoundariesUV& borderZones,
                         BoundariesUV& Bounds, std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev,
                         StereoHits& myStereo ) const noexcept;

  inline void CollectLayerU( unsigned int part, unsigned int layer, const Pr::Hybrid::SeedTrackX& xProje,
                             const BoundaryUV& borderZones, BoundaryUV& Bounds,
                             std::array<std::array<float, 2>, 3>& xMinPrev, StereoHits& myStereo ) const noexcept;

  inline void CollectLayerV( unsigned int part, unsigned int layer, const Pr::Hybrid::SeedTrackX& xProje,
                             const BoundaryUV& borderZones, BoundaryUV& Bounds,
                             std::array<std::array<float, 2>, 3>& xMinPrev, StereoHits& myStereo ) const noexcept;

  inline void AddUVHit( const ModPrHitConstIter& it, const float& xPred, const float& factor,
                        StereoHits& myStereo ) const noexcept;

  inline void initializeUVBounds( const PrFTHitHandler<ModPrHit>& FTHitHandler, BoundariesUV& Bounds,
                                  BoundariesUV&                                       borderZones,
                                  std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const noexcept;

  /** @brief Determines if a predicted (u-v) position is in the hole or not
   */
  inline bool isInHole( float x, float y ) const noexcept;

  /** @brief Applies the triangle fix
   */
  inline bool doTriangleSecondOrderFix( const unsigned int part, const unsigned int kk, float& yMin, float& yMax,
                                        const PrHit* hit ) const noexcept;

  /** @brief Performs the Hough cluster search
   */
  inline void HoughClusterSearch( unsigned int iCase, StereoHits& myStereo, StereoIter initBeg[3][DEPTH_HOUGH],
                                  StereoIter initEnd[3][DEPTH_HOUGH], float ClusterSpread[3][DEPTH_HOUGH] ) const
      noexcept;

  /** @brief Performs the Hough cluster search
   */
  inline void HoughClusterSearchReco( StereoHits& myStereo, StereoIter initBeg[3][DEPTH_HOUGH],
                                      StereoIter initEnd[3][DEPTH_HOUGH], float ClusterSpread[3][DEPTH_HOUGH] ) const
      noexcept;

  /** @brief Track recovering routine for x/z discarded tracks
   */
  inline void RecoverTrack( PrFTHitHandler<ModPrHit>& FTHitHandler, TrackCandidates& trackCandidates,
                            XCandidates& xCandidates, TrackToRecover& trackToRecover ) const noexcept;

  /** @brief Flag Hits on found tracks
   */
  inline void flagHits( unsigned int icase, unsigned int part, TrackCandidates& trackCandidates,
                        //  inline void flagHits( unsigned int icase, unsigned int part, TrackCandidates&
                        //  trackCandidates,
                        PrFTHitHandler<ModPrHit>& hitHandler ) const noexcept;

  /** @brief Fit the track combining the XZ and YZ projections
   *  @param track The track to fit
   *  @param Refit Iteration in the Refitting after removal worst hit
   *  @return bool Success of the XY+XZ Fit
   **/
  inline bool fitSimultaneouslyXY( Pr::Hybrid::SeedTrack& track, unsigned int iCase ) const noexcept;

  /** @brief Remove the hit which gives the largest contribution to the chi2 and refit XZ + YZ
   *  @param track The track to fit
   *  @return bool Success of the fit
   */
  inline bool removeWorstAndRefit( Pr::Hybrid::SeedTrack& track, unsigned int iCase ) const noexcept;

  /** @brief Transform the tracks from the internal representation into Tracks
   *  @param tracks The tracks to transform
   */
  inline void makeLHCbTracks( std::vector<Track>& result, unsigned int part,
                              const TrackCandidates& trackCandidates ) const noexcept;

private:
  /// derived condition caching computed zones
  PrFTZoneHandler* m_zoneHandler = nullptr;

  //------------- Track recovering routine parameters
  Gaudi::Property<bool>                      m_recover{this, "Recover", true};
  Gaudi::Property<std::vector<int>>          m_nusedthreshold{this, "nUsedThreshold", {3, 2, 1}};
  Gaudi::Property<unsigned int>              m_recoNCluster{this, "Recover_MaxNCluster", 3};
  Gaudi::Property<float>                     m_recomaxY0{this, "Recover_maxY0", 3000.};
  Gaudi::Property<float>                     m_recoLineHigh{this, "Recover_LineFitHigh", 90.0};
  Gaudi::Property<float>                     m_recoLineLow{this, "Recover_LineFitLow", 16.0};
  Gaudi::Property<float>                     m_recoFinalChi2{this, "Recover_MaxChiDoF", 10.0};
  Gaudi::Property<unsigned int>              m_recoMinTotHits{this, "Recover_minTotHits", 9};
  Gaudi::Property<float>                     m_recoTolTy{this, "Recover_tolTy", 0.015};
  Gaudi::Property<float>                     m_recoChiOutlier{this, "Recover_OutliersChi", 4.5};
  Gaudi::Property<std::vector<unsigned int>> m_recover_minUV{this, "Recover_minUV", {4, 5, 5}};

  //------------- Global configuration of the algorithm
  Gaudi::Property<unsigned int> m_minXPlanes{this, "MinXPlanes", 4};
  Gaudi::Property<unsigned int> m_nCases{this, "NCases", 3};
  Gaudi::Property<bool>         m_removeClonesX{this, "RemoveClonesX", true};

  //------------ X-Z projections search parametrisation
  //=== 1st (T1) / Last (T3) Layer search windows
  // TODO make them P dependent
  Gaudi::Property<std::vector<float>> m_alphaCorrection{this, "L0_AlphaCorr", {120.64, 510.64, 730.64}};
  Gaudi::Property<std::vector<float>> m_TolFirstLast{this, "L0_tolHp", {280.0, 540.0, 1080.0}};

  //=== Add of the third hit in middle layers (p and Pt dependent, i.e., case dependent)
  // TODO make it Pt dependent

  Gaudi::Property<unsigned int>       m_maxParabolaSeedHits{this, "maxParabolaSeedHits", 8};
  Gaudi::Property<bool>               m_parabolaSeedParabolicModel{this, "ParabolaSeedParabolicModel", false};
  Gaudi::Property<std::vector<float>> m_x0Cut{this, "x0Cut", {1500., 4000., 6000.}};
  Gaudi::Property<std::vector<float>> m_x0Corr{this, "x0Corr", {0.002152, 0.001534, 0.001834}};

  // In case we use the linear model
  Gaudi::Property<std::vector<float>> m_x0SlopeChange{this, "X0SlopeChange", {400., 500., 500.}};
  Gaudi::Property<std::vector<float>> m_tolX0SameSign{this, "ToleranceX0Up", {0.75, 0.75, 0.75}};

  Gaudi::Property<std::vector<float>> m_tolAtX0Cut{this, "TolAtX0Cut", {4.5, 8.0, 14.0}};
  Gaudi::Property<std::vector<float>> m_tolX0OppSign{this, "ToleranceX0Down", {0.75, 0.75, 0.75}};
  Gaudi::Property<std::vector<float>> m_x0SlopeChange2{this, "X0SlopeChangeDown", {2000., 2000., 2000.}};
  Gaudi::Property<std::vector<float>> m_tolAtx0CutOppSign{this, "TolAtX0CutOpp", {0.75, 2.0, 7.0}};

  // In case we use the parabola model
  Gaudi::Property<std::vector<float>> m_parPol0{this, "ParabolaSeedPol0", {1., 1.56435, 2.22193}};
  Gaudi::Property<std::vector<float>> m_parPol1{this, "ParabolaSeedPol1", {-0.000262611, -0.000297483, -0.000436888}};
  Gaudi::Property<std::vector<float>> m_parPol2{this, "ParabolaSeedPol2", {7.61125e-07, 2.94707e-07, 2.93121e-07}};

  //=== Add Hits in remaining X Layers after parabolic shape is found
  Gaudi::Property<std::vector<float>> m_tolRemaining{this, "TolXRemaining", {1.0, 1.0, 1.0}};

  //----------- Track Model parameters
  Gaudi::Property<float>              m_dRatio{this, "dRatio", -0.000262};
  Gaudi::Property<std::vector<float>> m_dRatioPar{this, "dRatioPar", {0.000267957, -8.651e-06, 4.60324e-05}};
  Gaudi::Property<float> m_ConstC{this, "CConst", 2.458e8}; // Const value to compute the backward projection

  //----------- Fit X/Z projection tolerances
  Gaudi::Property<std::vector<float>> m_maxChi2HitsX{this, "maxChi2HitsX", {5.5, 5.5, 5.5}};
  Gaudi::Property<std::vector<float>> m_maxChi2DoFX{this, "maxChi2DoFX", {4.0, 5.0, 6.0}};

  //----------- Full Fit tolerances in standard "Cases"

  Gaudi::Property<std::vector<float>> m_maxChi2HitFullFitHigh{this, "maxChi2Hits_11and12Hit", {5.5, 5.5, 5.5}};
  Gaudi::Property<std::vector<float>> m_maxChi2HitFullFitLow{this, "maxChi2Hits_less11Hit", {2.5, 2.5, 2.5}};
  Gaudi::Property<std::vector<float>> m_maxY0Low{this, "maxYatZeroLow", {50., 60., 70.}};
  Gaudi::Property<std::vector<float>> m_maxYZrefLow{this, "maxYatzRefLow", {400., 550., 700.}};

  //  Gaudi::Property<std::vector<float> >            m_maxChi2FullFit;
  //  Gaudi::Property<std::vector<float> >            m_maxChi2HitLow;
  //  Gaudi::Property<std::vector<float> >            m_maxChi2HitFull ;

  //----------- UV-hits search parameters
  Gaudi::Property<float> m_yMin{this, "yMin", -1.0 * Gaudi::Units::mm};
  Gaudi::Property<float> m_yMin_TrFix{this, "yMin_TrFix", -2.0 * Gaudi::Units::mm};
  Gaudi::Property<float> m_yMax{this, "yMax", +2700. * Gaudi::Units::mm};
  Gaudi::Property<float> m_yMax_TrFix{this, "yMax_TrFix", +30.0 * Gaudi::Units::mm};

  Gaudi::Property<float> m_holeWidthX{this, "HoleXWidth", 130.8 * Gaudi::Units::mm};
  Gaudi::Property<float> m_holeWidthY{this, "HoleYWidth", 115.0 * Gaudi::Units::mm};

  // Triangle Fix
  Gaudi::Property<bool>                      m_useFix2ndOrder{this, "TriangleFix2ndOrder", true}; // deprecated
  Gaudi::Property<std::vector<float>>        m_Chi2LowLine{this, "Chi2LowLine", {5.0, 6.5, 7.5}};
  Gaudi::Property<std::vector<float>>        m_Chi2HighLine{this, "Chi2HighLine", {30.0, 50.0, 80.0}};
  Gaudi::Property<std::vector<unsigned int>> m_minUV6{this, "minUV6", {4, 4, 4}};
  Gaudi::Property<std::vector<unsigned int>> m_minUV5{this, "minUV5", {5, 5, 4}};
  Gaudi::Property<std::vector<unsigned int>> m_minUV4{this, "minUV4", {6, 6, 5}};
  Gaudi::Property<std::vector<unsigned int>> m_minTot{this, "minTot", {9, 9, 9}};
  // Gaudi::Property<std::vector<float> > m_X0ChangeCoord; (use Backward projection to define hough cluster?) ( to be
  // implemented )
  Gaudi::Property<std::vector<float>> m_tolTyOffset{this, "TolTyOffset", {0.0017, 0.0025, 0.0035}};
  Gaudi::Property<std::vector<float>> m_tolTySlope{this, "TolTySlope", {0.0, 0.025, 0.035}};
  // X+Y fit configure
  Gaudi::Property<std::vector<float>> m_maxChi2PerDoF{this, "maxChi2PerDoF", {4.0, 6.0, 7.0}};
  // Flag Hits Settings
  Gaudi::Property<std::vector<float>>        m_MaxChi2Flag{this, "Flag_MaxChi2DoF_11Hits", {0.5, 1.0, 1.0}};
  Gaudi::Property<std::vector<float>>        m_MaxX0Flag{this, "Flag_MaxX0_11Hits", {100., 8000., 200.}};
  Gaudi::Property<std::vector<unsigned int>> m_SizeFlag{this, "SizeToFlag", {12, 11, 10}};

  // dRatio correction to use (temporary)
  Gaudi::Property<bool> m_useCorrPos{this, "UseCorrPosition", true};

  //== Make LHCb track states tool and parameters
  Gaudi::Property<std::vector<float>> m_zOutputs{
      this, "ZOutputs", {StateParameters::ZBegT, StateParameters::ZMidT, StateParameters::ZEndT}};
  PublicToolHandle<ITrackMomentumEstimate> m_momentumTool{this, "MomentumToolName", "FastMomentumEstimate"};

  Gaudi::Property<float> m_stateErrorX2{this, "StateErrorX2", 4.};
  Gaudi::Property<float> m_stateErrorY2{this, "StateErrorY2", 400.};
  Gaudi::Property<float> m_stateErrorTX2{this, "StateErrorTX2", 6e-5};
  Gaudi::Property<float> m_stateErrorTY2{this, "StateErrorTY2", 1e-4};
  ILHCbMagnetSvc*        m_magFieldSvc = nullptr;

  Gaudi::Property<std::vector<int>> m_maxNClusters{this, "maxNbestCluster", {2, 4, 4}};

  // Internal variables

  //  // Stereo layers
  float m_z[T3X2 + 1];
  int   m_planeCode[T3X2 + 1];
  float m_dxDy[2];
  float m_recDxDy[2];

  unsigned int m_minUV[3][3]; // m_minUV[case][6-nHits]
  float        m_yMins[2][2];
  float        m_yMaxs[2][2];

public:
  constexpr static unsigned int commonHits[5]        = {8, 7, 7, 6, 5};    // 70%*(12-nHits)
  constexpr static unsigned int commonXHits[6]       = {3, 3, 2, 1, 1, 1}; // a bit stanger
  constexpr static unsigned int commonRecoverHits[4] = {4, 3, 2, 2};       // 70%*(6-nHits)

protected:
  class compLHCbID {
  public:
    inline bool operator()( LHCb::LHCbID lv, LHCb::LHCbID rv ) const { return lv < rv; }
    inline bool operator()( const PrHit& lhs, LHCb::LHCbID rv ) const { return ( *this )( lhs.id(), rv ); }
    inline bool operator()( LHCb::LHCbID lv, const PrHit& rhs ) const { return ( *this )( lv, rhs.id() ); }
    inline bool operator()( const PrHit& lhs, const PrHit& rhs ) const { return ( *this )( lhs.id(), rhs.id() ); }
    inline bool operator()( const PrHit* lhs, LHCb::LHCbID rv ) const { return ( *this )( lhs->id(), rv ); }
    inline bool operator()( LHCb::LHCbID lv, const PrHit* rhs ) const { return ( *this )( lv, rhs->id() ); }
    inline bool operator()( const PrHit* lhs, const PrHit* rhs ) const { return ( *this )( lhs->id(), rhs->id() ); }
    inline bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const {
      return ( *this )( lhs.hit->id(), rhs.hit->id() );
    }
  };
};
#endif // PRHYBRIDSEEDING_H

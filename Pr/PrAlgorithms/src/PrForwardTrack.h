/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRFORWARDTRACK_H
#define PRFORWARDTRACK_H 1

// Include files
#include "Event/StateParameters.h"
#include "Event/Track_v2.h"
#include "PrKernel/PrHit.h"
#include <limits>

template <size_t N>
struct trackPars final {

  std::array<float, N> pars = {};

  trackPars() = default;
  trackPars( const std::array<float, N>& data ) : pars( data ){};

  void  set( const int i, const float v ) { pars[i] = v; };
  void  set( const std::array<float, N>& v ) { pars = v; };
  float get( const int i ) const { return pars[i]; };

  void add( const int i, const float v ) { pars[i] += v; };
  template <int S>
  void add( const std::array<float, S>& v ) {
    static_assert( N >= S, "wrong call to trackPars::add" );
    for ( uint i = 0; i < S; i++ ) { pars[i] += v[i]; }
  }

  trackPars& operator+=( const trackPars p ) {
    for ( uint i = 0; i < N; i++ ) { pars[i] += p.pars[i]; }
    return *this;
  }
};

/// Parameters of the velo track
struct VeloSeed {
  VeloSeed( float x0_, float y0_, float z0_, float tx_, float ty_, float qOverP_ )
      : x0{x0_}
      , y0{y0_}
      , z0{z0_}
      , tx{tx_}
      , ty{ty_}
      , qOverP{qOverP_}
      , tx2{tx_ * tx_}
      , ty2{ty_ * ty_}
      , slope2{tx_ * tx_ + ty * ty_} {}

  VeloSeed( const LHCb::State& state )
      : VeloSeed( state.x(), state.y(), state.z(), state.tx(), state.ty(), state.qOverP() ) {}

  float x0     = 0.f;
  float y0     = 0.f;
  float z0     = 0.f;
  float tx     = 0.f;
  float ty     = 0.f;
  float qOverP = 1.f;
  float tx2    = 0.f;
  float ty2    = 0.f;
  float slope2 = 0.f;

  // inline helper functions
  /// Predicted x position for a given z, using a straight line from Velo information
  inline float x( const float z ) const { return x0 + ( z - z0 ) * tx; }
  /// Predicted y position for a given z, using a straight line from Velo information
  inline float y( const float z ) const { return y0 + ( z - z0 ) * ty; }
};

/** @class PrForwardTrack PrForwardTrack.h
 *  This is the working class inside the T station pattern
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 *  @author Thomas Nikodem
 *  @date   2016-01-23
 */
class PrForwardTrack final {
  using Track = LHCb::Event::v2::Track;

public:
  /// Constructor with only a Velo track
  PrForwardTrack( int idx, float const x0, float const y0, float const z0, float const tx, float const ty,
                  float const qp )
      : m_track( idx )
      , m_seed{x0, y0, z0, tx, ty, qp}
      , m_valid( true )
      , m_xParams( {m_seed.x0 + ( m_zRef - m_seed.z0 ) * m_seed.tx, m_seed.tx, 0.f} )
      , m_yParams( {m_seed.y0 + ( m_zRef - m_seed.z0 ) * m_seed.ty, m_seed.ty, 0.f} ) {}

  int             track() const { return m_track; }
  const VeloSeed& seed() const { return m_seed; }

  void replaceHits( PrHits&& hits ) { m_hits = std::move( hits ); }

  /// Handling of hits: access, insertion
  PrHits&       hits() { return m_hits; }
  const PrHits& hits() const { return m_hits; }
  void          addHit( const PrHit& hit ) { m_hits.push_back( &hit ); }

  void addHits( PrHits& hits ) { m_hits.insert( m_hits.end(), hits.begin(), hits.end() ); }

  void setXParams( const trackPars<4>& xp ) { m_xParams = xp; }
  void addXParams( const trackPars<4>& xp ) { m_xParams += xp; }
  template <int N>
  void addXParams( const std::array<float, N>& pars ) {
    m_xParams.add<N>( pars );
  }
  const trackPars<4>& getXParams() const { return m_xParams; }

  void setYParams( const trackPars<3>& yp ) { m_yParams = yp; }
  void addYParams( const trackPars<3>& yp ) { m_yParams += yp; }
  template <int N>
  void addYParams( const std::array<float, N>& pars ) {
    m_yParams.add<N>( pars );
  }
  const trackPars<3>& getYParams() const { return m_yParams; }

  void setParams( const std::array<float, 7>& pars ) {
    m_xParams.set( {pars[0], pars[1], pars[2], pars[3]} );
    m_yParams.set( {pars[4], pars[5], pars[6]} );
  }

  /// Get the x position at a certain z position
  inline float x( const float z ) const {
    float dz = z - m_zRef;
    return m_xParams.get( 0 ) + dz * ( m_xParams.get( 1 ) + dz * ( m_xParams.get( 2 ) + dz * m_xParams.get( 3 ) ) );
  }
  /// Get the x slope at a certain z position
  inline float xSlope( const float z ) const {
    float dz = z - m_zRef;
    return m_xParams.get( 1 ) + dz * ( 2.f * m_xParams.get( 2 ) + 3.f * dz * m_xParams.get( 3 ) );
  }

  /// Get the y position at a certain z position
  inline float y( const float z ) const {
    float dz = z - m_zRef;
    return m_yParams.get( 0 ) + dz * ( m_yParams.get( 1 ) + dz * m_yParams.get( 2 ) );
  }
  /// Get the y slope at a certain z position
  inline float ySlope( const float z ) const {
    float dz = z - m_zRef;
    return m_yParams.get( 1 ) + dz * 2.f * m_yParams.get( 2 );
  }

  /// Get the x position at a certain z position, assuming the track is a straight line
  inline float xStraight( const float z ) const { return m_xParams.get( 0 ) + ( z - m_zRef ) * m_xParams.get( 1 ); }
  /// Get the y position at a certain z position, assuming the track is a straight line
  inline float yStraight( const float z ) const { return m_yParams.get( 0 ) + ( z - m_zRef ) * m_yParams.get( 1 ); }

  /// Calculate the y position of a hit given the parametrised track
  inline float yOnTrack( const PrHit& hit ) const {
    const float sly = ySlope( hit.z() );
    return hit.yOnTrack( y( hit.z() ) - sly * hit.z(), sly );
  }

  /// Calculate the distance between a hit and the parametrised track
  inline float distance( const PrHit& hit ) const {
    float z_Hit   = hit.z( y( hit.z() ) );
    float x_track = x( z_Hit );
    float y_track = y( z_Hit );
    return hit.distance( x_track, y_track );
  }
  inline float distanceFast( const PrHit& hit ) const {
    float z_Hit   = hit.z(); // ignore first itteraton!!
    float x_track = x( z_Hit );
    float y_track = y( z_Hit );
    return hit.distance( x_track, y_track );
  }
  inline float distanceXHit( const PrHit& hit ) const {
    // float z_Hit = hit.z(y(hit.z())); //ignore slope of modules in z direction. effect is small enough to ignore in
    // the beginning
    float x_track = x( hit.z() );
    return hit.distanceXHit( x_track );
  }

  /// Calculate the chi2 contribution of a single hit to the parametrised track
  float chi2( const PrHit& hit ) const {
    const float d = distance( hit );
    return d * d * hit.w();
  }
  float chi2Fast( const PrHit& hit ) const {
    const float d = distanceFast( hit );
    return d * d * hit.w();
  }
  float chi2XHit( const PrHit& hit ) const {
    const float d = distanceXHit( hit );
    return d * d * hit.w();
  }

  bool valid() const { return m_valid; }
  void setValid( const bool v ) { m_valid = v; }

  // void  setChi2( const float chi2, const int nDoF ) { m_chi2 = chi2; m_nDoF = nDoF; }
  float               nDoF() const { return m_chi2nDof.get( 1 ); }
  float               chi2PerDoF() const { return m_chi2nDof.get( 0 ) / m_chi2nDof.get( 1 ); }
  void                setChi2nDof( trackPars<2>& c ) { m_chi2nDof = c; }
  void                setChi2nDof( const std::array<float, 2>& c ) { m_chi2nDof.set( c ); }
  const trackPars<2>& getChi2nDof() const { return m_chi2nDof; }

  void  setQuality( const float q ) { m_quality = q; }
  float quality() const { return m_quality; }

  void  setQoP( const float qop ) { m_qop = qop; }
  float getQoP() const { return m_qop; }

  struct LowerByQuality {
    bool operator()( const PrForwardTrack& lhs, const PrForwardTrack& rhs ) const {
      return lhs.quality() < rhs.quality();
    }
  };

  static constexpr float m_zRef = 8520.f; // TODO do it better

private:
  static constexpr auto nan = std::numeric_limits<float>::quiet_NaN();
  int                   m_track{-1};
  VeloSeed              m_seed;
  PrHits                m_hits;
  bool                  m_valid = false;

  trackPars<4> m_xParams  = {{nan, nan, nan, nan}};
  trackPars<3> m_yParams  = {{nan, nan, nan}};
  trackPars<2> m_chi2nDof = {{nan, -1}};

  float m_quality = nan;

  // Charge(q) of particle divided by momentum (p) of particle
  float m_qop = nan;
};

typedef std::vector<PrForwardTrack> PrForwardTracks;

#endif // PRFORWARDTRACK_H

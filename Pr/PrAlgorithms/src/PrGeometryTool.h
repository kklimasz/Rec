/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRGEOMETRYTOOL_H
#define PRGEOMETRYTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "PrForwardTrack.h"
#include "PrHybridSeedTrack.h"

static const InterfaceID IID_PrGeometryTool( "PrGeometryTool", 1, 0 );

/** @class PrGeometryTool PrGeometryTool.h
 *  Holds the geometry information, namely the field parameterisation
 *
 *  @author Olivier Callot
 *  @date   2012-03-22
 *  @modified Thomas Nikodem
 */
class PrGeometryTool : public GaudiTool {
  using Track = LHCb::Event::v2::Track;

public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_PrGeometryTool; }

  /// Standard constructor
  PrGeometryTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  float qOverP( const Pr::Hybrid::SeedTrack& track ) const; ///< Momentum estimate for seed from HyrbidSeeding

  float qOverP( const Track& vTr, const Track& sTr ) const; ///< Momentum estimate for Match track

  float zMagnet( const PrForwardTrack& track ) const; ///< Initial value for the z at centre of magnet

  float zReference() const { return m_zReference; } ///< fixed z for all parameterizations

  Gaudi::TrackSymMatrix covariance( const float qOverp ) const; ///< Default (large) covariance matrix

  double magscalefactor() const { return m_magFieldSvc->signedRelativeCurrent(); };

private:
  ILHCbMagnetSvc*                       m_magFieldSvc;
  Gaudi::Property<std::array<float, 4>> m_zMagnetParams{this, "zMagnetParams", {5212.38, 406.609, -1102.35, -498.039}};
  Gaudi::Property<std::array<float, 6>> m_momentumParams{
      this, "momentumParams", {1.21014, 0.637339, -0.200292, 0.632298, 3.23793, -27.0259}};
  // ErrX = 2mm, ErrY = 20 mm, ErrSlX = 2 mrad, ErrSlY = 10 mrad, errQQoverP = 10% of qOverP
  Gaudi::Property<std::array<float, 5>> m_covarianceValues{this, "covarianceValues", {4.0, 400.0, 4.e-6, 1.e-4, 0.1}};
  Gaudi::Property<float>                m_zReference{this, "zReference", 8520. * Gaudi::Units::mm};
};
#endif // PRGEOMETRYTOOL_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRDEBUGTRACKINGLOSSES_H
#define PRDEBUGTRACKINGLOSSES_H 1

#include "Event/MCParticle.h"

#include "GaudiAlg/GaudiAlgorithm.h"

#include <vector>

namespace LHCb {
  class IParticlePropertySvc;
}

/** @class PrDebugTrackingLosses PrDebugTrackingLosses.h
 *  Debug which MCParticles are not reconstructed.
 *
 *  @author Olivier Callot
 *  @date   2009-03-26
 */
class PrDebugTrackingLosses : public GaudiAlgorithm {
public:
  /// Standard constructor
  PrDebugTrackingLosses( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PrDebugTrackingLosses(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  void printMCParticle( const LHCb::MCParticle* part );

private:
  mutable LHCb::IParticlePropertySvc* m_ppSvc; ///< Pointer to particle property service

  bool                          m_velo;
  bool                          m_forward;
  bool                          m_seed;
  bool                          m_clone;
  bool                          m_ghost;
  bool                          m_fromStrange;
  bool                          m_fromBeauty;
  double                        m_minMomentum;
  bool                          m_saveList;
  std::string                   m_veloName;
  std::string                   m_forwardName;
  std::string                   m_seedName;
  std::vector<std::vector<int>> m_badGuys;
  int                           m_eventNumber;
};
#endif // PRDEBUGTRACKINGLOSSES_H

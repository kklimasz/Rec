/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Event/MCEvent
#include "Event/MCHit.h"
#include "Event/MCTrackInfo.h"
// Event/TrackEvent
#include "Event/StateParameters.h"
#include "Event/Track.h"
// Event/LinkerEvent
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"

// Local
#include "PrCheatedVP.h"
#include "PrFitParams/PrFitTool.h"

using namespace LHCb;

DECLARE_COMPONENT( PrCheatedVP )
DECLARE_COMPONENT( PrCheatedVPMCHits )

namespace {
  LHCb::Tracks getTracks( const GaudiAlgorithm& alg, const MCTrackInfo& trackInfo, const IPrFitTool& fitTool,
                          const LHCb::MCParticles& particles,
                          const std::function<void( LHCb::Track&, std::vector<Gaudi::XYZPoint>&,
                                                    const LHCb::MCParticle*, LinkedFrom<LHCb::VPCluster, MCParticle>& )>
                              getPoints ) {
    LHCb::Tracks tracks;

    // Get the association table between MC particles and clusters.
    LinkedFrom<LHCb::VPCluster, MCParticle> link( alg.evtSvc(), alg.msgSvc(), LHCb::VPClusterLocation::Default );

    constexpr double zVelo = 0.;

    for ( const LHCb::MCParticle* const particle : particles ) {
      // Skip particles without track info.
      if ( 0 == trackInfo.fullInfo( particle ) ) continue;
      // Skip particles not linked to a VELO track.
      if ( !trackInfo.hasVelo( particle ) ) continue;
      // Skip electrons.
      if ( abs( particle->particleID().pid() ) == 11 ) continue;

      LHCb::Track* const           track = new LHCb::Track;
      std::vector<Gaudi::XYZPoint> points;
      getPoints( *track, points, particle, link );

      // Make a straight-line fit of the track.
      const auto xResult = fitTool.fitLine( points, IPrFitTool::XY::X, zVelo );
      const auto yResult = fitTool.fitLine( points, IPrFitTool::XY::Y, zVelo );
      if ( !xResult || !yResult ) {
        alg.err() << "Fit matrix is singular" << endmsg;
        continue;
      }

      const auto& [x0, x1] = *xResult;
      const auto& [y0, y1] = *yResult;

      LHCb::State state;
      state.setLocation( LHCb::State::Location::ClosestToBeam );
      state.setState( x0, y0, zVelo, x1, y1, 0. );
      track->addToStates( state );
      if ( 0 > particle->momentum().z() ) {
        track->setFlag( LHCb::Track::Flags::Backward, true );
        // Cut out backwards tracks.
        // delete track;
        // continue;
      }
      track->setType( LHCb::Track::Types::Velo );
      tracks.insert( track );
    }

    return tracks;
  }
} // namespace

//=============================================================================
// Constructor
//=============================================================================
PrCheatedVP::PrCheatedVP( const std::string& name, ISvcLocator* pSvcLocator )
    : PrCheatedVPBase( name, pSvcLocator, {KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}},
                       KeyValue{"Tracks", LHCb::TrackLocation::Velo} ) {}

PrCheatedVPMCHits::PrCheatedVPMCHits( const std::string& name, ISvcLocator* pSvcLocator )
    : PrCheatedVPBase(
          name, pSvcLocator,
          {KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}, KeyValue{"MCHits", LHCb::MCHitLocation::VP}},
          KeyValue{"Tracks", LHCb::TrackLocation::Velo} ) {}

//=============================================================================
// Initialization
//=============================================================================
template <bool useMCHits>
StatusCode PrCheatedVPBase<useMCHits>::initialize() {
  const auto sc = PrCheatedVPBase::Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  m_fitTool.retrieve();
  m_ft = m_fitTool.get();
  if ( m_ft == nullptr ) return StatusCode::FAILURE;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrCheatedVP::operator()( const LHCb::MCParticles& particles ) const {
  return getTracks( *this, make_MCTrackInfo( evtSvc(), msgSvc() ), *m_ft, particles,
                    []( LHCb::Track& track, std::vector<Gaudi::XYZPoint>& points,
                        const LHCb::MCParticle* const particle, LinkedFrom<LHCb::VPCluster, MCParticle>& link ) {
                      for ( const LHCb::VPCluster* cluster = link.first( particle ); cluster != nullptr;
                            cluster                        = link.next() ) {
                        track.addToLhcbIDs( LHCb::LHCbID( cluster->channelID() ) );
                        points.emplace_back( cluster->x(), cluster->y(), cluster->z() );
                      }
                    } );
}

LHCb::Tracks PrCheatedVPMCHits::operator()( const LHCb::MCParticles& particles, const LHCb::MCHits& hits ) const {
  return getTracks( *this, make_MCTrackInfo( evtSvc(), msgSvc() ), *m_ft, particles,
                    [&hits]( LHCb::Track& track, std::vector<Gaudi::XYZPoint>& points,
                             const LHCb::MCParticle* const particle, LinkedFrom<LHCb::VPCluster, MCParticle>& link ) {
                      for ( const auto id : link.keyRange( particle ) ) {
                        track.addToLhcbIDs( LHCb::LHCbID( LHCb::VPChannelID( id ) ) );
                      }
                      for ( const LHCb::MCHit* const hit : hits ) {
                        if ( hit->mcParticle() == particle ) { points.emplace_back( hit->midPoint() ); }
                      }
                    } );
}

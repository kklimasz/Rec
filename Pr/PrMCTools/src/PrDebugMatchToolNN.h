/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRDEBUGMATCHTOOLNN_H
#define PRDEBUGMATCHTOOLNN_H 1

// Include files
#include <string>

#include "Event/Track.h"
#include "GaudiAlg/GaudiTupleTool.h"

#include "PrKernel/IPrDebugMatchTool.h" // Interface

/** @class PrDebugMatchToolNN PrDebugMatchToolNN.h
 *
 * Debug tool for Matching algorithm
 *
 * @author Sevda Esen
 * @date   2017-02-21
 *	initial implementation
 */

struct PrDebugMatchToolNN : public extends<GaudiTupleTool, IPrDebugMatchTool> {

  using base_class::base_class;

  int matchMCPart( const LHCb::Track& velo, const LHCb::Track& seed ) const override;

  void fillTuple( const LHCb::Track& velo, const LHCb::Track& seed, const std::vector<float>& vars ) const override;
};

#endif // PRDEBUGMATCHTOOL_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "PrChecker.h"

#include "PrTrackCounter.h"
#include "PrUTCounter.h"

// Easier with typedefs to avoid strange syntax in Macros
typedef PrCheckerAlgorithm<PrTrackCounter> PrTrackChecker;
typedef PrCheckerAlgorithm<PrUTCounter>    PrUTHitChecker;

DECLARE_COMPONENT_WITH_ID( PrTrackChecker, "PrTrackChecker" )
DECLARE_COMPONENT_WITH_ID( PrUTHitChecker, "PrUTHitChecker" )

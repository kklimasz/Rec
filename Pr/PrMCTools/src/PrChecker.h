/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/Track.h"

#include "MCInterfaces/IMCReconstructible.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "LoKi/IMCHybridFactory.h"
#include "LoKi/MCParticles.h"
#include "LoKi/Primitives.h"

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/IHistoTool.h"
#include "GaudiKernel/ToolHandle.h"

/**
 *  Check the quality of the pattern recognition, by comparing to MC information
 *  Produces efficiency, ghost rate and clone rate numbers.
 *  Parameters:
 *   - [deprecated] Eta25Cut: Only consider particles with 2 < eta < 5? (default: false)
 *   - CheckNegEtaPlot: Check eta plotting range, plot negative values only if no eta25 cut was applied (default: false)
 *   - TriggerNumbers: Give numbers for p > 3GeV, pT > 500 MeV? (default: false)
 *     if selected long_fromB_P>3GeV_Pt>0.5GeV cut is added to each track container
 *   - VetoElectrons: Take electrons into account in numbers? (default: true)
 *   - WriteTexOutput: Writes the statistics table to a TeX file (default: false)
 *     which is dumped to the location specified in TexOutputFolder
 *   - MyCuts: selection cuts to be applied (default: empty)
 *   - WriteHistos: whether to plot histograms via IHistoTool. Values are -1 (default, no histograms), 1 (histograms), 2
 * (mote histograms : expectedHits, docaz, PVz, EtaP, EtaPhi, efficiency maps @z=9000mm XYZ9000 and @z=2485mm XYZ2485)
 *
 * This class is templated and is available in python with PrCounter and PrUTCounter as template arguments
 * The respective names to import are PrChecker and PrUTHitChecker
 *
 * Typical usage :
 *
 * @code
 *   from Configurables import PrChecker
 *   mychecker = PrChecker("PrCheckerVelo",
 *                          Title="Velo",
 *                          Tracks = "Rec/Track/Velo",
 *                          TriggerNumbers=True,
 *                          MyCuts = { "01_velo" : "isVelo",
 *                                     "02_long" : "isLong",
 *                                     "03_long>5GeV" : "isLong & over5" } ))
 *
 *   from Configurables import LoKi__Hybrid__MCTool
 *   myFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
 *   myFactory.Modules = [ "LoKiMC.decorators" ]
 *   mychecker.addTool( myFactory )
 *  @endcode
 *
 *  As a default selection cuts of old PrChecker are used. The following cuts are predefined:
 *  - is(Not)Long, is(Not)Velo, is(Not)Down, is(Not)Up, is(Not)UT, is(Not)Seed,
 *  - fromB, fromD, BOrDMother, fromKsFromB, strange
 *  - is(Not)Electron, eta25, over5, trigger
 *
 *  and LoKi syntax (LoKi::MCParticles) can be used for kinematical cuts, e.g. (MCPT> 2300), here the '()' are
 * essential.
 *
 *  NB: If you care about the implementation: The cut-strings are converted into two types of functors:
 *  - LoKi-type functors (hence all LoKi::MCParticles cuts work)
 *  - and custom-defined ones, mostly for type of reconstructibility and daughter-criteria (like 'isNotLong')
 *  where in the end all functors are evaluated on each MCParticle for each track container to define the
 * reconstructibility. If a MCParticle is actually reconstructed is checked. A large part of the code just deals with
 * the conversion of the strings into functors.
 *
 */

namespace {
  typedef std::vector<std::string> strings;
  const strings cutAliases{"isNotLong",  "isLong",      "isVelo",  "isNotVelo", "isDown",     "isNotDown",
                           "isUp",       "isNotUp",     "isUT",    "isNotUT",   "isSeed",     "isNotSeed",
                           "fromB",      "fromKsFromB", "strange", "fromD",     "isElectron", "isNotElectron",
                           "BOrDMother", "PairProd",    "isDecay", "fromHI",    "fromPV",     "muonHitsInAllStations"};
} // namespace

template <typename InternalCounter>
class PrCheckerAlgorithm : public Gaudi::Functional::Consumer<void( const LHCb::Track::Range&, const LHCb::MCParticles&,
                                                                    const LHCb::MCVertices&, const LHCb::MCProperty&,
                                                                    const LHCb::LinksByKey&, const LHCb::LinksByKey& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  PrCheckerAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"Tracks", ""}, KeyValue{"MCParticleInput", LHCb::MCParticleLocation::Default},
                   KeyValue{"MCVerticesInput", LHCb::MCVertexLocation::Default},
                   KeyValue{"MCPropertyInput", LHCb::MCPropertyLocation::TrackInfo}, KeyValue{"Links", ""},
                   KeyValue{"LinkTableLocation", "Link/Pr/LHCbID"}} ) {}

  /// Algorithm initialization
  StatusCode initialize() override;
  /// Algorithm execution
  void operator()( const LHCb::Track::Range&, const LHCb::MCParticles&, const LHCb::MCVertices&,
                   const LHCb::MCProperty&, const LHCb::LinksByKey&, const LHCb::LinksByKey& ) const override;
  /// Algorithm finalization
  StatusCode finalize() override;

private:
  // typedefs to make everything a bit more readable
  typedef std::map<std::string, std::string>  stringmap;
  typedef std::pair<std::string, std::string> stringpair;

  Gaudi::Property<std::string>  m_title{this, "Title", ""};
  Gaudi::Property<unsigned int> m_firstNVeloHits{this, "FirstNVeloHits", 3};
  Gaudi::Property<unsigned int> m_hitTypesToCheck{this, "HitTypesToCheck", 0};
  Gaudi::Property<std::string>  m_trackType{this, "TrackType", "TypeUnknown"};

  // The counters are not at all thread safe for the moment
  // So we protect their use by a mutex. Not optimal but MC checking
  // does not need to be absolutely fast
  mutable InternalCounter m_counter;
  mutable std::mutex      m_counterMutex;

  // -- histograming options
  Gaudi::Property<int> m_writeHistos{this, "WriteHistos", -1};

  Gaudi::Property<bool> m_eta25cut{this, "Eta25Cut", false}; // to be deprecated, cuts on track properties should be
                                                             // done in filters before the algorithm.
  Gaudi::Property<bool> m_checkNegEtaPlot{
      this, "CheckNegEtaPlot", false,
      "eta plotting range check : plot negative values only if no eta25 cut was applied"};
  Gaudi::Property<bool>  m_triggerNumbers{this, "TriggerNumbers", false};
  Gaudi::Property<bool>  m_vetoElectrons{this, "VetoElectrons", true};
  Gaudi::Property<bool>  m_xyPlots{this, "XYPlots", false};
  Gaudi::Property<float> m_ghostProbCut{this, "GhostProbCut", 1.0}; // to be deprecated, cuts on track properties should
                                                                    // be done in filters before the algorithm.

  Gaudi::Property<bool>        m_writetexfile{this, "WriteTexOutput", false};
  Gaudi::Property<std::string> m_texfilename{this, "TexOutputName", "efficiencies"};
  Gaudi::Property<std::string> m_texfolder{this, "TexOutputFolder", ""};

  enum recAs {
    isLong                = 1,
    isNotLong             = 2,
    isDown                = 3,
    isNotDown             = 4,
    isUp                  = 5,
    isNotUp               = 6,
    isVelo                = 7,
    isNotVelo             = 8,
    isUT                  = 9,
    isNotUT               = 10,
    isSeed                = 11,
    isNotSeed             = 12,
    strange               = 13,
    fromB                 = 15,
    fromD                 = 16,
    fromKsFromB           = 17,
    isElectron            = 18,
    isNotElectron         = 19,
    BOrDMother            = 20,
    PairProd              = 21,
    isDecay               = 22,
    fromHI                = 23,
    fromPV                = 24,
    muonHitsInAllStations = 25,
  };

  std::map<std::string, recAs> m_lookuptable = {{"isLong", isLong},
                                                {"isNotLong", isNotLong},
                                                {"isDown", isDown},
                                                {"isNotDown", isNotDown},
                                                {"isUp", isUp},
                                                {"isNotUp", isNotUp},
                                                {"isVelo", isVelo},
                                                {"isNotVelo", isNotVelo},
                                                {"isUT", isUT},
                                                {"isNotUT", isNotUT},
                                                {"isSeed", isSeed},
                                                {"isNotSeed", isNotSeed},
                                                {"strange", strange},
                                                {"fromB", fromB},
                                                {"fromD", fromD},
                                                {"fromKsFromB", fromKsFromB},
                                                {"isElectron", isElectron},
                                                {"isNotElectron", isNotElectron},
                                                {"BOrDMother", BOrDMother},
                                                {"PairProd", PairProd},
                                                {"isDecay", isDecay},
                                                {"fromHI", fromHI},
                                                {"fromPV", fromPV},
                                                {"muonHitsInAllStations", muonHitsInAllStations}};

  // convert strings to normal cuts ==> called m_otherCuts (without LoKi Hybrid factory)
  /**
   *  Predefined selection cuts: it converts strings to normal cuts, used by addOtherCuts
   */
  class isTrack {

  public:
    isTrack( const recAs kind ) { m_kind = kind; };
    /// Functor that checks if the MCParticle fulfills certain criteria, e.g. reco'ble as long track, B daughter, ...
    bool operator()( LHCb::MCParticle* mcp, MCTrackInfo* mcInfo, std::vector<LHCb::LHCbID> const& lhcbIds ) const {
      bool motherB = false;
      bool motherD = false;
      switch ( m_kind ) {
      case isLong:
        return mcInfo->hasVeloAndT( mcp );
      case isNotLong:
        return !mcInfo->hasVeloAndT( mcp );
      case isDown:
        return mcInfo->hasT( mcp ) && mcInfo->hasTT( mcp );
      case isNotDown:
        return !( mcInfo->hasT( mcp ) && mcInfo->hasTT( mcp ) );
      case isUp:
        return mcInfo->hasVelo( mcp ) && mcInfo->hasTT( mcp );
      case isNotUp:
        return !( mcInfo->hasVelo( mcp ) && mcInfo->hasTT( mcp ) );
      case isVelo:
        return mcInfo->hasVelo( mcp );
      case isNotVelo:
        return !mcInfo->hasVelo( mcp );
      case isSeed:
        return mcInfo->hasT( mcp );
      case isNotSeed:
        return !mcInfo->hasT( mcp );
      case isUT:
        return mcInfo->hasTT( mcp );
      case isNotUT:
        return !mcInfo->hasTT( mcp );
      case isElectron:
        return abs( mcp->particleID().pid() ) == 11;
      case isNotElectron:
        return abs( mcp->particleID().pid() ) != 11;
      case muonHitsInAllStations: {
        // Check that lhcbIds contains at least one muon hit in each muon station
        std::bitset<4> seen;
        for ( auto const& lhcbid : lhcbIds ) {
          if ( lhcbid.isMuon() ) { seen[lhcbid.muonID().station()] = true; }
        }
        return seen.all();
      }
      default:
        break;
      }

      if ( 0 != mcp->originVertex() ) {
        const LHCb::MCParticle* mother = mcp->originVertex()->mother();
        if ( 0 != mother ) {
          if ( 0 != mother->originVertex() ) {
            double rOrigin = mother->originVertex()->position().rho();
            if ( fabs( rOrigin ) < 5. ) {
              int pid = abs( mother->particleID().pid() );
              // -- MCParticle is coming from a strang particle
              if ( 130 == pid ||  // K0L
                   310 == pid ||  // K0S
                   3122 == pid || // Lambda
                   3222 == pid || // Sigma+
                   3212 == pid || // Sigma0
                   3112 == pid || // Sigma-
                   3322 == pid || // Xsi0
                   3312 == pid || // Xsi-
                   3334 == pid    // Omega-
              ) {
                if ( m_kind == strange ) return true;
              }
              // -- It's a Kshort from a b Hadron
              if ( 0 != mother->originVertex()->mother() ) {
                if ( 310 == pid && 2 == mcp->originVertex()->products().size() &&
                     mother->originVertex()->mother()->particleID().hasBottom() &&
                     ( mother->originVertex()->mother()->particleID().isMeson() ||
                       mother->originVertex()->mother()->particleID().isBaryon() ) ) {
                  if ( m_kind == fromKsFromB ) return true;
                }
              }
            }
          }
        }
        // -- It's a daughter of a B or D hadron
        while ( 0 != mother ) {
          if ( mother->particleID().hasBottom() &&
               ( mother->particleID().isMeson() || mother->particleID().isBaryon() ) )
            motherB = true;

          if ( mother->particleID().hasCharm() &&
               ( mother->particleID().isMeson() || mother->particleID().isBaryon() ) )
            motherD = true;

          mother = mother->originVertex()->mother();
        }
        if ( m_kind == fromD && motherD == true ) return m_kind == fromD;
        if ( m_kind == fromB && motherB == true ) return m_kind == fromB;

        if ( m_kind == BOrDMother && ( motherD || motherB ) ) return m_kind == BOrDMother;

        // -- It's from a decay, from gamma->ee pair production or from Hadronic Interaction.
        // -- isDecay includes both DecayVertex and OscillatedAndDecay
        if ( mcp->originVertex()->type() == LHCb::MCVertex::MCVertexType::DecayVertex ||
             mcp->originVertex()->type() == LHCb::MCVertex::MCVertexType::OscillatedAndDecay )
          return m_kind == isDecay;
        if ( mcp->originVertex()->type() == LHCb::MCVertex::MCVertexType::PairProduction ) return m_kind == PairProd;
        if ( mcp->originVertex()->type() == LHCb::MCVertex::MCVertexType::HadronicInteraction ) return m_kind == fromHI;
        if ( mcp->originVertex()->type() == LHCb::MCVertex::MCVertexType::ppCollision ) return m_kind == fromPV;
      }
      return false;
    }

  private:
    recAs m_kind;
  };

  /**
   *  Class that adds selection cuts defined in isTrack to cuts
   */
  class addOtherCuts {

  public:
    void addCut( isTrack cat ) { m_cuts.push_back( cat ); }

    /// Functor that evaluates all 'isTrack' cuts
    bool operator()( LHCb::MCParticle* mcp, MCTrackInfo* mcInfo, std::vector<LHCb::LHCbID> const& lhcbIds ) const {

      bool decision = true;

      for ( unsigned int i = 0; i < m_cuts.size(); ++i ) { decision = decision && m_cuts[i]( mcp, mcInfo, lhcbIds ); }

      return decision;
    }

  private:
    std::vector<isTrack> m_cuts;
  };

  // maps for each track container with {cut name,selection cut}
  Gaudi::Property<stringmap> m_map{this, "MyCuts", {}};

  /** @brief makes vector of second elements of DefaultCutMap --> needed as input for m_Cuts */
  strings getMyCut( stringmap myCutMap ) {
    strings dummy;
    for ( auto it : myCutMap ) { dummy.push_back( it.second ); }
    return dummy;
  }

  ToolHandle<IHistoTool>             m_histoTool{this, "HistoTool", "HistoTool/PrCheckerHistos"};
  ToolHandle<LoKi::IMCHybridFactory> m_factory{this, "LoKiFactory", "LoKi::Hybrid::MCTool/MCHybridFactory:PUBLIC"};
  ToolHandle<ITrackExtrapolator>     m_extrapolator{this, "TrackMasterExtrapolator", "TrackMasterExtrapolator"};
  std::vector<LoKi::Types::MCCut>    m_MCCuts;
  std::vector<addOtherCuts>          m_MCCuts2;
};

template <typename InternalCounter>
StatusCode PrCheckerAlgorithm<InternalCounter>::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  static const std::string histoDir = "Track/";
  if ( "" == histoTopDir() ) setHistoTopDir( histoDir );
  m_histoTool.retrieve(); // needs to be done for next line to work
  GaudiHistoTool* ghtool = dynamic_cast<GaudiHistoTool*>( m_histoTool.get() );

  // -- catch the possible failure of the dynamic cast
  if ( ghtool == NULL ) {
    error() << "Dynamic cast of Gaudi Histogramming Tool failed!" << endmsg;
    return StatusCode::FAILURE;
  }

  ghtool->setHistoDir( histoDir + name() );

  m_counter.setTitle( m_title.value() );
  m_counter.setFirstNVeloHits( m_firstNVeloHits );
  m_counter.setWriteHistos( m_writeHistos );
  m_counter.setHitTypesToCheck( m_hitTypesToCheck );
  m_counter.setTrackType( LHCb::Track::TypesToType( m_trackType ) );
  m_counter.setXYPlots( m_xyPlots );

  for ( auto pair : m_map ) {
    if ( m_checkNegEtaPlot ) {
      // define eta plotting range: plot negative values only if no eta25 cut was applied
      const std::string etaString( "eta25" );
      std::size_t       found = pair.second.find( etaString );
      if ( m_eta25cut == false && found == std::string::npos ) {
        m_counter.addSelection( pair.first, true, true );
      } else {
        m_counter.addSelection( pair.first, true );
      }
    } else {
      m_counter.addSelection( pair.first, true );
    }
  }

  m_counter.setUseEta25Cut( m_eta25cut );
  if ( m_eta25cut == true ) {
    warning() << "Property Eta25Cut to be deprecated. Please use dedicated algorithm to filter on track properties. "
                 "For example see in PrUpgradeChecking.py"
              << endmsg;
  }
  m_counter.setGhostProbCut( m_ghostProbCut );
  if ( m_ghostProbCut != 1.0 ) {
    warning() << "Property GhostProbCut to be deprecated. Please use dedicated algorithm to filter on track "
                 "properties. For example see in PrUpgradeChecking.py"
              << endmsg;
  }

  m_counter.setTriggerNumbers( m_triggerNumbers );
  if ( m_writetexfile.value() ) m_counter.setTeXName( m_texfolder, m_texfilename );

  // -- convert all strings into functors
  for ( std::string cutString : getMyCut( m_map ) ) { // loop over 2nd element of Cuts = strings of cuts

    m_MCCuts2.push_back( addOtherCuts() );

    // flag to circumvent veto of electrons for selections that only look at elctrons
    bool ExplicitlyKeepElectrons = cutString.find( "isElectron" ) != std::string::npos;
    // -- loop over cutAliases
    // -- extract them from cutString and replace with 'MCTRUE'
    for ( std::string alias : cutAliases ) {

      std::size_t found = cutString.find( alias );

      if ( found != std::string::npos ) {                           // if found then
        m_MCCuts2.back().addCut( isTrack( m_lookuptable[alias] ) ); // other components are already filled //add this
                                                                    // category of cuts to addOtherCuts()
        cutString.replace( found, alias.length(), "MCTRUE" ); // replace found at position found, with length of string
                                                              // to replace, replace it with string "" (Loki Cut)
      }
    }

    // -- Veto electrons or not
    if ( m_vetoElectrons == true && !ExplicitlyKeepElectrons ) {
      const std::string eString( "isNotElectron" );
      m_MCCuts2.back().addCut( isTrack( m_lookuptable[eString] ) );
      std::size_t found = cutString.find( eString );
      if ( found != std::string::npos ) { cutString.replace( found, eString.length(), "MCTRUE" ); }
    }

    // -- LoKi cuts: define aliases for better use
    const std::string etaString( "eta25" );
    std::size_t       found = cutString.find( etaString );
    if ( m_eta25cut == true && found == std::string::npos ) {
      cutString.append( " & eta25" );
      found = cutString.find( etaString );
    }
    if ( found != std::string::npos ) {
      cutString.replace( found, etaString.length(), "(MCETA > 2.0) & (MCETA < 5.0)" );
    }

    const std::string over5String( "over5" );
    found = cutString.find( over5String );
    if ( found != std::string::npos ) { cutString.replace( found, over5String.length(), "(MCP > 5000)" ); }

    const std::string triggerString( "trigger" );
    found = cutString.find( triggerString );
    if ( found != std::string::npos ) { cutString.replace( found, triggerString.length(), "(MCP>3000) & (MCPT>500)" ); }
    // ---------------------------------------------------------------------------------

    LoKi::Types::MCCut tmp = LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant( false ); //
    sc                     = m_factory->get( cutString, tmp );
    m_MCCuts.push_back( tmp );
    if ( sc.isFailure() ) { return Error( "Error from IMCHybridFactory", sc ); } // RETURN
  }

  return StatusCode::SUCCESS;
}

template <typename InternalCounter>
void PrCheckerAlgorithm<InternalCounter>::operator()( const LHCb::Track::Range& tracks,
                                                      const LHCb::MCParticles& mcParts, const LHCb::MCVertices& mcVert,
                                                      const LHCb::MCProperty& flags, const LHCb::LinksByKey& tr2McLink,
                                                      const LHCb::LinksByKey& mc2IdLink ) const {
  // in debug mode, check consistency of the inputs
  assert( tr2McLink.sourceClassID() == LHCb::Track::classID() &&
          "Incompatible link table in PrCheckerAlgorithm. Source should be Track" );
  assert( tr2McLink.targetClassID() == LHCb::MCParticle::classID() &&
          "Incompatible link table in PrCheckerAlgorithm. Target should be McParticle" );

  MCTrackInfo  trackInfo = {flags};
  unsigned int nPrim     = 0;
  for ( auto vertex : mcVert ) {
    if ( vertex->isPrimary() ) {
      int nbVisible = 0;
      for ( auto& part : mcParts ) {
        if ( part->primaryVertex() == vertex ) {
          if ( trackInfo.hasVelo( part ) ) nbVisible++;
        }
      }
      if ( nbVisible > 4 ) ++nPrim;
    }
  }

  m_counter.initEvent( m_histoTool.get(), m_extrapolator.get(), nPrim, tracks, tr2McLink );

  //== Build a table (vector of map) of Track -> weigth per MCParticle, indexed by MCParticle key.
  std::vector<std::map<const LHCb::Track*, double>> tracksForParticle;
  tr2McLink.applyToAllLinks( [&tracksForParticle, &tracks]( int trackKey, unsigned int mcPartKey, float weight ) {
    auto track = std::find_if( tracks.begin(), tracks.end(), [&trackKey]( auto t ) { return t->key() == trackKey; } );
    if ( track != tracks.end() ) {
      if ( tracksForParticle.size() <= mcPartKey ) { tracksForParticle.resize( mcPartKey + 1 ); }
      tracksForParticle[mcPartKey][*track] += weight;
    }
  } );

  //== Build a table (vector of vectors) of LHCbID per MCParticle, indexed by MCParticle key.
  std::vector<std::vector<LHCb::LHCbID>> idsForParticle;
  mc2IdLink.applyToAllLinks( [&idsForParticle]( unsigned int id, unsigned int mcPartKey, float ) {
    if ( idsForParticle.size() <= mcPartKey ) { idsForParticle.resize( mcPartKey + 1 ); }
    idsForParticle[mcPartKey].emplace_back( id );
  } );

  std::vector<LHCb::LHCbID>            noids;
  std::map<const LHCb::Track*, double> noTracksList;
  for ( const auto part : mcParts ) {
    if ( 0 == trackInfo.fullInfo( part ) ) continue;
    std::vector<bool> flags;
    // get the LHCbIDs that go with the MCParticle
    auto& ids = idsForParticle.size() > (unsigned int)part->key() ? idsForParticle[part->key()] : noids;
    // cuts
    for ( unsigned int i = 0; i < m_MCCuts.size(); ++i ) {
      flags.push_back( m_MCCuts[i]( part ) && m_MCCuts2[i]( part, &trackInfo, ids ) );
    }
    try {
      m_counter.countAndPlot( m_histoTool.get(), m_extrapolator.get(), part, flags, ids, nPrim,
                              tracksForParticle.size() > (unsigned int)part->key() ? tracksForParticle[part->key()]
                                                                                   : noTracksList );
    } catch ( const std::string& msg ) {
      Warning( msg ).ignore();
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "... Flag size " << flags.size() << " >  " << m_counter.title().size() << " declared selections"
                << endmsg;
      }
    }
  }
}

template <typename InternalCounter>
StatusCode PrCheckerAlgorithm<InternalCounter>::finalize() {
  info() << "Results" << endmsg;
  m_counter.printStatistics( info(), inputLocation<0>() );
  return GaudiHistoAlg::finalize(); // must be called after all other actions
}

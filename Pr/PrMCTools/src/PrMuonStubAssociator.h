/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRMUONSTUBASSOCIATOR_H
#define PRMUONSTUBASSOCIATOR_H 1

// Include files
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MCInterfaces/IMuonPad2MCTool.h"

/** @class TrackAssociator TrackAssociator.h
 *
 *  This algorithm computes the link between a Track and a MCParticle.
 *  The requirement is a match of both the Velo/VP and the T part of the
 *  Track. If there are not enough coordinates, the match is assumed so that
 *  a Velo only or a T only are matched properly.
 *  The required fraction of hits is a jobOption 'FractionOK', default 0.70.
 *
 *  Rewritten for the upgrade, handles all containers in one instance
 *
 *  @author Olivier Callot
 *  @date   2012-04-04
 */

class PrMuonStubAssociator : public GaudiAlgorithm {
public:
  // Standard constructor
  PrMuonStubAssociator( const std::string& name, ISvcLocator* pSvcLocator );

  // Destructor
  virtual ~PrMuonStubAssociator();

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_container{this, "Container", "Rec/Track/StandaloneMuonFitted"};
  Gaudi::Property<float>       m_matchThreshold{this, "MatchThreshold", 0.7};

  IMuonPad2MCTool* m_muonPad2MC = nullptr;
};

#endif // PRMUONSTUBASSOCIATOR_H

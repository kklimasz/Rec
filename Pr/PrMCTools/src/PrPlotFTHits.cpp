/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// Linker
#include "Linker/AllLinks.h"

#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
// local
#include "PrKernel/PrFTInfo.h"
#include "PrPlotFTHits.h"

#include "GaudiAlg/IHistoTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrPlotFTHits
//
// 2014-05-08 : Michel De Cian
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( PrPlotFTHits )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrPlotFTHits::PrPlotFTHits( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiHistoAlg( name, pSvcLocator ) {
  declareProperty( "FTHitsLocation", m_HitsInTES );
  declareProperty( "PlotStates", m_plotState = false );
  declareProperty( "ExcludeElectrons", m_excludeElectrons = true );
  declareProperty( "PlotHitEfficiency", m_plotHitEfficiency = true );
  declareProperty( "PlotAllFTHits", m_plotAllFTHits = true );
  declareProperty( "PlotFTHitsOnTrack", m_plotFTHitsOnTrack = true );
  declareProperty( "PlotTrackingEfficiency", m_plotTrackingEfficiency = true );
  declareProperty( "PlotMCHits", m_plotMCHits = true );
  declareProperty( "OnlyLongAndDownForMCHits", m_onlyLongDownForMCHits = true );
  declareProperty( "PlotOccupancy", m_plotOccupancy = false );
}
//=============================================================================
// Destructor
//=============================================================================
PrPlotFTHits::~PrPlotFTHits() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrPlotFTHits::initialize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  static const std::string histoDir = "Track/";
  if ( "" == histoTopDir() ) setHistoTopDir( histoDir );

  IHistoTool* htool = tool<IHistoTool>( "HistoTool", "PrPlotFTHitsHistos", this );
  m_ghtool          = dynamic_cast<GaudiHistoTool*>( htool );

  // -- catch the possible failure of the dynamic cast
  if ( m_ghtool == NULL ) {
    error() << "Dynamic cast of Gaudi Histogramming Tool failed!" << endmsg;
    return StatusCode::FAILURE;
  }

  m_extrapolator      = tool<ITrackExtrapolator>( "TrackMasterExtrapolator", this );
  m_idealStateCreator = tool<IIdealStateCreator>( "IdealStateCreator", "IdealStateCreator", this );
  m_ghtool->setHistoDir( "Track/PrPlotFTHits" );
  m_histoTool = htool;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrPlotFTHits::execute() {
  const auto m_hitHandler = m_HitsInTES.get();

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  if ( m_plotOccupancy ) plotOccupancy( *m_hitHandler );
  if ( m_plotHitEfficiency ) plotHitEfficiency( *m_hitHandler );
  if ( m_plotAllFTHits ) plotAllFTHits( *m_hitHandler );
  if ( m_plotFTHitsOnTrack ) plotFTHitsOnTrack( *m_hitHandler );
  if ( m_plotTrackingEfficiency ) plotTrackingEfficiency();
  if ( m_plotMCHits ) plotMCHits( *m_hitHandler );
  if ( m_plotState ) plotState();
  return StatusCode::SUCCESS;
}
void PrPlotFTHits::plotOccupancy( const PrFTHitHandler<PrHit>& FTHitHandler ) {
  m_ghtool->setHistoDir( "Track/PrPlotFTHits/Occupancy" );
  std::map<int, int>  m_Occupancy;
  const LHCb::MCHits* mcHitsNext = getIfExists<LHCb::MCHits>( "Next/" + LHCb::MCHitLocation::FT );
  const LHCb::MCHits* mcHitsPrev = getIfExists<LHCb::MCHits>( "Prev/" + LHCb::MCHitLocation::FT );

  bool noSpill = false;
  if ( !mcHitsNext && !mcHitsPrev ) {
    info() << "The sample has been digitized without spillover, Occupancy Plots for them are suppressed" << endmsg;
    noSpill = true;
  }
  //"/Event/Prev/MC/FT/Hits"
  LinkedFrom<LHCb::MCHit, LHCb::MCParticle> myMCHitLink( evtSvc(), msgSvc(),
                                                         LHCb::MCParticleLocation::Default + "2MC" + "FT" + "Hits" );

  LinkedTo<LHCb::MCParticle, LHCb::FTCluster> myClusterLink( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );
  LinkedTo<LHCb::MCHit, LHCb::FTCluster>      myFTCluster2MCHitLink( evtSvc(), msgSvc(),
                                                                LHCb::FTClusterLocation::Default + "2MCHits" );
  //"/Event/Prev/MC/FT/Hits"
  char              layerName[100];
  char              Title[100];
  std::vector<int>  nHits( 12, 0 );
  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  LHCb::MCVertices* mcVert = getIfExists<LHCb::MCVertices>( LHCb::MCVertexLocation::Default );
  if ( mcVert == nullptr ) {
    error() << "Could not find MCVertices at " << LHCb::MCParticleLocation::Default << endmsg;
  }
  unsigned int NbPrimaries = 0;
  for ( LHCb::MCVertices::iterator itV = mcVert->begin(); mcVert->end() != itV; ++itV ) {
    if ( ( *itV )->isPrimary() ) { NbPrimaries++; }
  }
  sprintf( Title, "Nb Primary vertices;Counts" );
  plot( NbPrimaries, "NbPrimary", Title, -0.5, 25.5, 25 );
  int                recoble       = 0;
  int                all           = 0;
  int                recoblewanted = 0;
  LHCb::MCParticles* mcParts       = getIfExists<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
  LHCb::MCParticle*  mcPart        = nullptr;
  for ( LHCb::MCParticles::const_iterator iPart = mcParts->begin(); iPart != mcParts->end(); ++iPart ) {
    mcPart         = ( *iPart );
    bool isRecoble = trackInfo.hasT( mcPart );
    bool recobleWanted =
        isRecoble && ( mcPart->originVertex()->isPrimary() ||
                       mcPart->originVertex()->isDecay() ); // really fast checker if mcParticle is a wanted one or not
    all++;
    if ( isRecoble ) recoble++;
    if ( recobleWanted ) recoblewanted++;
  }
  plot( all, "NbMCParticle_All", "Nb. MCParticle", 0., 10000., 200 );
  plot( recoble, "NbMCParticle_Recoble", "Nb. MCParticle", -0.5, 1000., 100 );
  plot( recoblewanted, "NbMCParticle_RecobleWanted", "Nb. MCParticle", -0.5, 1000.5, 100 );
  double recobleper       = (double)recoble / all;
  double recoblewantedper = (double)recoblewanted / all;
  plot( recoblewantedper, "NbMCParticle_RecobleWantedPerc", "Fraction Reconstructible", -0.5, 2, 200 );
  plot( recobleper, "NbMCParticle_RecoblePerc", "Fraction Reconstructible", -0.5, 2, 200 );
  for ( unsigned int zoneI = 0; zoneI < PrFTInfo::nbZones(); ++zoneI ) {
    int layer = zoneI / 2;
    sprintf( layerName, "Layer%i Occupancy", layer );
    for ( const PrHit& hit : FTHitHandler.hits( zoneI ) ) {
      nHits[layer]++;
      auto liteCluster = getLiteCluster( hit.id() );
      int  index       = liteCluster.channelID().uniqueSiPM() & 511;
      sprintf( layerName, "Layer%i/AllContributions", layer );
      sprintf( Title, "Sipm Index Layer %i;SiPm Id;Clusters/Sipm/Event", layer );
      m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
      bool fromMCParticleContrib = false;
      bool spillCluster          = false;
      bool NoiseCluster          = false;

      LHCb::MCParticle* mcPart0 = myClusterLink.first( hit.id().ftID() );

      LHCb::MCParticle* mcPart1 = mcPart0;
      double            maxw    = myClusterLink.weight();
      double            w       = maxw;

      while ( mcPart0 != nullptr ) {
        w = myClusterLink.weight();
        if ( w > maxw ) {
          maxw    = w;
          mcPart1 = mcPart;
        }
        mcPart0 = myClusterLink.next();
      }
      LHCb::MCHit* mcHit     = nullptr;
      LHCb::MCHit* mcHitHigh = myFTCluster2MCHitLink.first( hit.id().ftID() );
      double       maXweight = myFTCluster2MCHitLink.weight();
      double       weight    = maXweight;
      mcHit                  = mcHitHigh;
      while ( mcHitHigh != nullptr ) {
        weight = myFTCluster2MCHitLink.weight();
        if ( weight > maXweight ) {
          maXweight = weight;
          mcHit     = mcHitHigh;
        }
        mcHitHigh = myFTCluster2MCHitLink.next();
      }
      if ( !noSpill ) {
        if ( mcHit != nullptr && ( mcHit->parent() == mcHitsNext || mcHit->parent() == mcHitsPrev ) ) {
          spillCluster = true;
        }
      }
      if ( mcPart1 != nullptr ) { fromMCParticleContrib = true; }
      if ( mcHit == nullptr ) { NoiseCluster = true; }

      bool RecobleMCParticle = false;
      if ( fromMCParticleContrib && trackInfo.hasT( mcPart1 ) ) RecobleMCParticle = true;
      if ( RecobleMCParticle ) {
        sprintf( layerName, "Layer%i/FromRecobleMCParticle", layer );
        sprintf( Title, "Sipm Index Layer %i;Sipm ID;Clusters/Sipm/Event", layer );
        m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
        // 0.5,96.5,96);
        if ( mcPart->originVertex()->isPrimary() || mcPart->originVertex()->isDecay() ) {
          sprintf( layerName, "Layer%i/FromRecobleMCParticleWanted", layer );
          sprintf( Title, "Sipm Index Layer %i;Sipm ID;Clusters/Sipm/Event", layer );
          m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
        }
      }
      if ( fromMCParticleContrib ) {
        sprintf( layerName, "Layer%i/FromMCParticle", layer );
        sprintf( Title, "Sipm Index Layer %i;Sipm ID;Clusters/Sipm/Event", layer );
        m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
        // 0.5,96.5,96);
      }
      if ( NoiseCluster ) {
        sprintf( layerName, "Layer%i/FromNoise", layer );
        sprintf( Title, "Sipm Index Layer %i;Sipm ID;Clusters/Sipm/Event", layer );
        m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
        // 0.5,96.5,96);
      }
      if ( spillCluster && !noSpill ) {
        sprintf( layerName, "Layer%i/FromSpillover", layer );
        sprintf( Title, "Sipm Index Layer %i;Sipm ID;Clusters/Sipm/Event", layer );
        m_histoTool->plot1D( index, layerName, Title, 0.5, 512.5, 512 );
      }
    } // loop hits
  }   // loop zones
}

void PrPlotFTHits::plotState() {
  m_ghtool->setHistoDir( "Track/PrPlotFTHits/States" );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Seed );
  LinkedTo<LHCb::MCParticle, LHCb::Track> myForwardLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Forward );

  // -- get the tracks container

  LHCb::Tracks* forwardTracks = getIfExists<LHCb::Tracks>( LHCb::TrackLocation::Forward );
  LHCb::Tracks* seedTracks    = getIfExists<LHCb::Tracks>( LHCb::TrackLocation::Seed );

  if ( !forwardTracks ) info() << "No tracks found in: " << LHCb::TrackLocation::Forward << endmsg;
  if ( !seedTracks ) info() << "No tracks found in: " << LHCb::TrackLocation::Seed << endmsg;
  char stateZ[100];
  if ( seedTracks ) {

    for ( LHCb::Tracks::const_iterator iTrack = seedTracks->begin(); iTrack != seedTracks->end(); ++iTrack ) {
      const LHCb::Track* track  = *iTrack;
      LHCb::MCParticle*  mcPart = mySeedLink.first( track->key() );
      // std::vector<State*> TrackStates = track->states();
      if ( mcPart != nullptr ) {
        std::vector<LHCb::State*> TrackStates = track->states();
        if ( TrackStates.size() == 0 ) info() << "No states for track" << endmsg;
        // LHCb::State Idstate;
        LHCb::State* Trstate = nullptr;
        // loop trough the states of the track
        for ( std::vector<LHCb::State*>::iterator trstate = TrackStates.begin(); trstate != TrackStates.end();
              ++trstate ) {
          Trstate = ( *trstate );
          LHCb::State IdState;
          m_idealStateCreator->createState( mcPart, Trstate->z(), IdState );
          sprintf( stateZ, "Seeding/StateZ_%f/DeltaX", Trstate->z() );
          // do comparison here
          m_histoTool->plot1D( Trstate->x() - IdState.x(), stateZ, "Track x - IdState x;#Delta_{x} [mm]", -8., 8.,
                               300 );
          sprintf( stateZ, "Seeding/StateZ_%f/DeltaY", Trstate->z() );
          m_histoTool->plot1D( Trstate->y() - IdState.y(), stateZ, "Track y - IdState y;#Delta_{y} [mm]", -8., 8.,
                               300 );
          sprintf( stateZ, "Seeding/StateZ_%f/DeltaTx", Trstate->z() );
          m_histoTool->plot1D( 1000 * ( Trstate->tx() - IdState.tx() ), stateZ,
                               "Track tx(zState) - IdState tx(zState);#Delta_{tx} [mrad];Counts/0.03 mrad", -5., 5.,
                               300 );
          // m_histoTool->plot2D(1000*(Trstate->tx()-IdState.tx()),stateZ, "Trac
          sprintf( stateZ, "Seeding/StateZ_%f/DeltaTy", Trstate->z() );
          m_histoTool->plot1D( 1000 * ( Trstate->ty() - IdState.ty() ), stateZ,
                               "Track ty(zState) - IdState ty(zState);#Delta_{ty} [mrad];Counts/0.03 mrad", -5., 5.,
                               300 );
        }
      }
    }
  }

  if ( forwardTracks ) {
    for ( LHCb::Tracks::const_iterator iTrack = forwardTracks->begin(); iTrack != forwardTracks->end(); ++iTrack ) {
      const LHCb::Track* track  = *iTrack;
      LHCb::MCParticle*  mcPart = mySeedLink.first( track->key() );
      // std::vector<State*> TrackStates = track->states();
      if ( mcPart != nullptr ) {
        std::vector<LHCb::State*> TrackStates = track->states();
        if ( TrackStates.size() == 0 ) info() << "No states for track" << endmsg;
        // LHCb::State Idstate;
        LHCb::State* Trstate = nullptr;
        // loop trough the states of the track
        for ( std::vector<LHCb::State*>::iterator trstate = TrackStates.begin(); trstate != TrackStates.end();
              ++trstate ) {
          Trstate = ( *trstate );
          LHCb::State IdState;
          m_idealStateCreator->createState( mcPart, Trstate->z(), IdState );
          sprintf( stateZ, "Forward/StateZ_%f/DeltaX", Trstate->z() );
          // do comparison here
          m_histoTool->plot1D( Trstate->x() - IdState.x(), stateZ, "Track x - IdState x;#Delta_{x} [mm]", -8., 8.,
                               300 );
          m_histoTool->plot1D( Trstate->y() - IdState.y(), stateZ, "Track y - IdState y;#Delta_{y} [mm]", -8., 8.,
                               300 );
          sprintf( stateZ, "Forward/StateZ_%f/DeltaTx", Trstate->z() );
          m_histoTool->plot1D( 1000 * ( Trstate->tx() - IdState.tx() ), stateZ,
                               "Track tx(zState) - IdState tx(zState);#Delta_{tx} [mrad];Counts/0.03 mrad", -5., 5.,
                               300 );
          // m_histoTool->plot2D(1000*(Trstate->tx()-IdState.tx()),stateZ, "Trac
          sprintf( stateZ, "Forward/StateZ_%f/DeltaTy", Trstate->z() );
          m_histoTool->plot1D( 1000 * ( Trstate->ty() - IdState.ty() ), stateZ,
                               "Track ty(zState) - IdState ty(zState);#Delta_{ty} [mrad];Counts/0.03 mrad", -5., 5.,
                               300 );
        }
      }
    }
  }
}

//=============================================================================
//  Plot hit efficiency
//=============================================================================
void PrPlotFTHits::plotHitEfficiency( const PrFTHitHandler<PrHit>& FTHitHandler ) {
  m_ghtool->setHistoDir( "Track/PrPlotFTHits/HitEff" );
  // -- Link different track containers to MCParticles
  LinkedTo<LHCb::MCParticle, LHCb::Track> myForwardLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Forward );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Seed );
  LinkedTo<LHCb::MCParticle>              myClusterLink( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );
  const MCTrackInfo                       trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  // -----------------------------------------------------------------------------------
  // Plot hit efficiency as function of x and y
  // -----------------------------------------------------------------------------------
  for ( unsigned int zone = 0; zone < PrFTInfo::nbZones(); ++zone ) {
    char         layerNameRecoble[100];
    char         layerNameRecod[100];
    unsigned int layer = zone / 2; // merges two zones in a layer

    for ( const auto& iHit : FTHitHandler.hits( zone ) ) {
      // -- As the key of an FTCluster is its channelID, we can link LHCbID and MCParticle directly!
      // -- Caveat: Only take the first link. This might not be fully correct if high precision is needed.
      const LHCb::MCParticle* mcPart1 = myClusterLink.first( iHit.id().ftID() );
      if ( mcPart1 == nullptr ) continue;
      if ( m_excludeElectrons ) {
        if ( abs( mcPart1->particleID().pid() ) == 11 ) continue; // No electrons!
      }
      bool isLong = trackInfo.hasVeloAndT( mcPart1 );
      bool isSeed = trackInfo.hasT( mcPart1 );
      // -- We only care about tracks that have at least a T-station segment
      if ( !isLong && !isSeed ) continue;
      // -- Long tracks
      if ( isLong ) {
        sprintf( layerNameRecoble, "hitsInLayerReconstructibleLong%i", layer );
        sprintf( layerNameRecod, "hitsInLayerReconstructedLong%i", layer );
        LHCb::State state;
        m_idealStateCreator->createState( mcPart1, iHit.z(), state );

        // -- Plot all reconstructible hits that belong to this particle
        m_histoTool->plot2D( state.x(), state.y(), layerNameRecoble, "Reconstructible hit", -3000, 3000., -3000.,
                             3000.0, 100, 100 );
        std::vector<const LHCb::Track*> tracks = getTrack( iHit.id(), LHCb::TrackLocation::Forward );
        if ( !tracks.empty() ) {
          bool assoc = false;
          // -- If one of the tracks associated to this LHCbID is associated to the same MCParticle
          // -- as the LHCbID is associated, then it is associated...
          // -- (aka: if LHCbID->MCParticle == LHCbID->Track->MCParticle, then the hit was "efficient")
          for ( const LHCb::Track* track : tracks ) {
            LHCb::MCParticle* mcPart2 = myForwardLink.first( track->key() );
            if ( mcPart1 == mcPart2 ) {
              assoc = true;
              break;
            }
          }
          // -- Plot all reconstructed hits that belong to this particle
          if ( assoc )
            m_histoTool->plot2D( state.x(), state.y(), layerNameRecod, "Reconstructed hit", -3000, 3000., -3000.,
                                 3000.0, 100, 100 );
        }
      }
      // -- Seed tracks
      if ( isSeed ) {
        sprintf( layerNameRecoble, "hitsInLayerReconstructibleSeed%i", layer );
        sprintf( layerNameRecod, "hitsInLayerReconstructedSeed%i", layer );
        LHCb::State state;
        m_idealStateCreator->createState( mcPart1, iHit.z(), state );
        m_histoTool->plot2D( state.x(), state.y(), layerNameRecoble, "Reconstructible hit", -3000, 3000., -3000.,
                             3000.0, 100, 100 );
        std::vector<const LHCb::Track*> tracks = getTrack( iHit.id(), LHCb::TrackLocation::Seed );
        if ( !tracks.empty() ) {
          bool assoc = false;
          // -- If one of the tracks associated to this LHCbID is associated to the same MCParticle
          // -- as the LHCbID is associated, then it is associated...
          // -- (aka: if LHCbID->MCParticle == LHCbID->Track->MCParticle, then the hit was "efficient")
          for ( const LHCb::Track* track : tracks ) {
            LHCb::MCParticle* mcPart2 = mySeedLink.first( track->key() );
            if ( mcPart1 == mcPart2 ) {
              assoc = true;
              break;
            }
          }
          // -- Plot all reconstructed hits that belong to this particle
          if ( assoc )
            m_histoTool->plot2D( state.x(), state.y(), layerNameRecod, "Reconstructed hit", -3000, 3000., -3000.,
                                 3000.0, 100, 100 );
        }
      }
      // -- Seed tracks, but not long
      if ( isSeed && !isLong ) {
        sprintf( layerNameRecoble, "hitsInLayerReconstructibleSeedOnly%i", layer );
        sprintf( layerNameRecod, "hitsInLayerReconstructedSeedOnly%i", layer );
        LHCb::State state;
        m_idealStateCreator->createState( mcPart1, iHit.z(), state );
        m_histoTool->plot2D( state.x(), state.y(), layerNameRecoble, "Reconstructible hit", -3000, 3000., -3000.,
                             3000.0, 100, 100 );
        std::vector<const LHCb::Track*> tracks = getTrack( iHit.id(), LHCb::TrackLocation::Seed );
        if ( !tracks.empty() ) {
          bool assoc = false;
          // -- If one of the tracks associated to this LHCbID is associated to the same MCParticle
          // -- as the LHCbID is associated, then it is associated...
          // -- (aka: if LHCbID->MCParticle == LHCbID->Track->MCParticle, then the hit was "efficient")
          for ( const LHCb::Track* track : tracks ) {
            LHCb::MCParticle* mcPart2 = mySeedLink.first( track->key() );
            if ( mcPart1 == mcPart2 ) {
              assoc = true;
              break;
            }
          }
          // -- Plot all reconstructed hits that belong to this particle
          if ( assoc )
            m_histoTool->plot2D( state.x(), state.y(), layerNameRecod, "Reconstructed hit", -3000, 3000., -3000.,
                                 3000.0, 100, 100 );
        }
      }
    }
  }
}
//=============================================================================
//  Plot all FT hits (and the cluster size)
//=============================================================================
void PrPlotFTHits::plotAllFTHits( const PrFTHitHandler<PrHit>& FTHitHandler ) {

  m_ghtool->setHistoDir( "Track/PrPlotFTHits/AllFTHits" );

  char layerName[22];

  // ---------------------------------------------------------------
  // -- plot all FT hits for all zones (and their corresponding cluster size)
  // ---------------------------------------------------------------
  for ( unsigned int zone = 0; zone < PrFTInfo::nbZones(); ++zone ) {
    unsigned int layer = zone / 2;
    sprintf( layerName, "hitsInLayer%i", layer );
    for ( const auto& iHit : FTHitHandler.hits( zone ) ) {
      m_histoTool->plot2D( iHit.x(), 0.5 * ( iHit.yMin() + iHit.yMax() ), layerName, "hits in given layer", -3000,
                           3000., -3000., 3000.0, 100, 2 );
    }
  }
}
//=============================================================================
//  Plot FT hits on a truthmatched track
//=============================================================================
void PrPlotFTHits::plotFTHitsOnTrack( const PrFTHitHandler<PrHit>& FTHitHandler ) {

  m_ghtool->setHistoDir( "Track/PrPlotFTHits/FTHitsOnTrack" );

  // ----------------------------------------------------------------
  // -- plot FT hit on a given (truthmatched) track
  // ----------------------------------------------------------------
  LinkedTo<LHCb::MCParticle, LHCb::Track> myForwardLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Forward );
  LinkedTo<LHCb::MCParticle, LHCb::Track> mySeedLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Seed );

  LHCb::Tracks* forwardTracks = getIfExists<LHCb::Tracks>( LHCb::TrackLocation::Forward );
  LHCb::Tracks* seedTracks    = getIfExists<LHCb::Tracks>( LHCb::TrackLocation::Seed );

  if ( !forwardTracks ) info() << "No tracks found in: " << LHCb::TrackLocation::Forward << endmsg;
  if ( !seedTracks ) info() << "No tracks found in: " << LHCb::TrackLocation::Seed << endmsg;

  char layerName[48];

  // -- Forward tracks
  if ( forwardTracks ) {

    for ( const LHCb::Track* track : *forwardTracks ) {

      LHCb::MCParticle* mcPart = myForwardLink.first( track->key() );

      const auto& lhcbIDs = track->lhcbIDs();

      // --------------------------------------------------------------
      // -- truthmatched tracks
      if ( mcPart != nullptr ) {

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Forward truthmatched track has: " << lhcbIDs.size() << " hits in total" << endmsg;

        for ( const LHCb::LHCbID ID : lhcbIDs ) {

          if ( !( ID.isFT() ) ) continue;

          unsigned int zone = 0;
          const PrHit* hit  = getPrHit( ID, zone, FTHitHandler );
          if ( hit == nullptr ) continue;
          unsigned int layer = zone / 2;

          LHCb::State state = track->closestState( hit->z() );

          // -- Protect against nonphysical states
          if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;

          StatusCode sc = m_extrapolator->propagate( state, hit->z() );
          if ( !sc ) continue;

          sprintf( layerName, "hitsOnTruthMatchedForwardTrackInLayer%i", layer );
          m_histoTool->plot2D( hit->x(), state.y(), layerName, layerName, -3000, 3000., -3000., 3000.0, 100, 100 );
        }
        // --------------------------------------------------------------
        // -- ghost tracks
      } else {

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Forward ghost track has: " << lhcbIDs.size() << " hits in total" << endmsg;
        for ( const LHCb::LHCbID ID : lhcbIDs ) {
          if ( !( ID.isFT() ) ) continue;
          unsigned int zone = 0;
          const PrHit* hit  = getPrHit( ID, zone, FTHitHandler );
          if ( hit == nullptr ) continue;
          unsigned int layer = zone / 2;
          LHCb::State  state = track->closestState( hit->z() );
          // -- Protect against nonphysical states
          if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;
          StatusCode sc = m_extrapolator->propagate( state, hit->z() );
          if ( !sc ) continue;
          sprintf( layerName, "hitsOnGhostForwardTrackInLayer%i", layer );
          m_histoTool->plot2D( hit->x(), state.y(), layerName, layerName, -3000, 3000., -3000., 3000.0, 100, 100 );
        }
      }
    }
  }

  // -- Seed tracks
  if ( seedTracks ) {

    for ( LHCb::Tracks::const_iterator iTrack = seedTracks->begin(); iTrack != seedTracks->end(); ++iTrack ) {

      const LHCb::Track* track = *iTrack;

      LHCb::MCParticle* mcPart = mySeedLink.first( track->key() );

      const std::vector<LHCb::LHCbID> lhcbIDs = track->lhcbIDs();

      // --------------------------------------------------------------
      // -- truthmatched tracks
      if ( mcPart != nullptr ) { // truthmatched tracks

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Seed truthmatched track has: " << lhcbIDs.size() << " hits in total" << endmsg;

        for ( const LHCb::LHCbID ID : lhcbIDs ) {

          if ( !( ID.isFT() ) ) continue;

          unsigned int zone = 0;
          const PrHit* hit  = getPrHit( ID, zone, FTHitHandler );
          if ( hit == nullptr ) continue;
          unsigned int layer = zone / 2;
          LHCb::State  state = track->closestState( hit->z() );

          // -- Protect against nonphysical states
          if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;

          StatusCode sc = m_extrapolator->propagate( state, hit->z() );
          if ( !sc ) continue;

          sprintf( layerName, "hitsOnTruthMatchedSeedTrackInLayer%i", layer );
          m_histoTool->plot2D( hit->x(), state.y(), layerName, layerName, -3000, 3000., -3000., 3000.0, 100, 100 );
        }

        // --------------------------------------------------------------
        // -- ghost tracks
      } else {

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Seed ghost track has: " << lhcbIDs.size() << " hits in total" << endmsg;

        for ( const LHCb::LHCbID ID : lhcbIDs ) {

          if ( !( ID.isFT() ) ) continue;

          unsigned int zone = 0;
          const PrHit* hit  = getPrHit( ID, zone, FTHitHandler );
          if ( hit == nullptr ) continue;
          unsigned int layer = zone / 2;
          LHCb::State  state = track->closestState( hit->z() );

          // -- Protect against nonphysical states
          if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;

          StatusCode sc = m_extrapolator->propagate( state, hit->z() );
          if ( !sc ) continue;

          sprintf( layerName, "hitsOnGhostSeedTrackInLayer%i", layer );
          m_histoTool->plot2D( hit->x(), state.y(), layerName, layerName, -3000, 3000., -3000., 3000.0, 100, 100 );
        }
      }
    }
  }
}
//=============================================================================
//  Plot the tracking efficiency
//=============================================================================
void PrPlotFTHits::plotTrackingEfficiency() {

  m_ghtool->setHistoDir( "Track/PrPlotFTHits/TrackEff" );

  LinkedFrom<LHCb::Track, LHCb::MCParticle> myForwardLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Forward );
  LinkedFrom<LHCb::Track, LHCb::MCParticle> mySeedLink( evtSvc(), msgSvc(), LHCb::TrackLocation::Seed );

  // -----------------------------------------------------------------------------------
  // Plot efficiency as function of x and y (the z position is more or less arbitrarily chosen in the middle of the T
  // stations)
  // -----------------------------------------------------------------------------------

  LHCb::MCParticles* mcParts = getIfExists<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
  if ( msgLevel( MSG::ERROR ) && !mcParts )
    error() << "Could not find MCParticles at: " << LHCb::MCParticleLocation::Default << endmsg;

  const MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  for ( const LHCb::MCParticle* mcPart : *mcParts ) {

    if ( m_excludeElectrons ) {
      if ( abs( mcPart->particleID().pid() ) == 11 ) continue; // No electrons!
    }

    bool isLong = trackInfo.hasVeloAndT( mcPart );
    bool isSeed = trackInfo.hasT( mcPart );
    bool isDown = trackInfo.hasT( mcPart ) && trackInfo.hasTT( mcPart );

    if ( !isLong && !isSeed && !isDown ) continue;

    // -- Long tracks
    if ( isLong ) {

      LHCb::State state;
      m_idealStateCreator->createState( mcPart, 9000, state );

      // -- Protect against nonphysical states
      if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;

      m_histoTool->plot2D( state.x(), state.y(), "reconstructibleParticlesLong", "Reconstructible particles", -3000,
                           3000., -3000., 3000.0, 100, 100 );

      LHCb::Track* track = myForwardLink.first( mcPart );
      if ( track ) {
        if ( track->type() != LHCb::Track::Types::Long ) continue;
        m_histoTool->plot2D( state.x(), state.y(), "reconstructedParticlesLong", "Reconstructed particles", -3000,
                             3000., -3000., 3000.0, 100, 100 );
      }
    }
    // -- Seed tracks, but potentially also long
    if ( isSeed ) {

      LHCb::State state;
      m_idealStateCreator->createState( mcPart, 9000, state );
      // -- Protect against nonphysical states
      if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;
      m_histoTool->plot2D( state.x(), state.y(), "reconstructibleParticlesSeed", "Reconstructible particles", -3000,
                           3000., -3000., 3000.0, 100, 100 );
      LHCb::Track* track = mySeedLink.first( mcPart );
      if ( track ) {
        if ( track->type() != LHCb::Track::Types::Ttrack ) continue;
        m_histoTool->plot2D( state.x(), state.y(), "reconstructedParticlesSeed", "Reconstructed particles", -3000,
                             3000., -3000., 3000.0, 100, 100 );
      }
    }
    // -- Downstream tracks, but potentially also long
    if ( isDown ) {
      LHCb::State state;
      m_idealStateCreator->createState( mcPart, 9000, state );
      // -- Protect against nonphysical states
      if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;
      m_histoTool->plot2D( state.x(), state.y(), "reconstructibleParticlesDown", "Reconstructible particles Down",
                           -3000, 3000., -3000., 3000.0, 200, 200 );
      LHCb::Track* track = mySeedLink.first( mcPart );
      if ( track ) {
        if ( track->type() != LHCb::Track::Types::Ttrack ) continue;
        m_histoTool->plot2D( state.x(), state.y(), "reconstructedParticlesDown", "Reconstructed particles Down", -3000,
                             3000., -3000., 3000.0, 100, 100 );
      }
    }

    // -- Seed tracks, which are not long tracks
    if ( isSeed && !isLong ) {

      LHCb::State state;
      m_idealStateCreator->createState( mcPart, 9000, state );

      // -- Protect against nonphysical states
      if ( std::isnan( state.x() ) || std::isnan( state.y() ) ) continue;

      m_histoTool->plot2D( state.x(), state.y(), "reconstructibleParticlesSeedOnly", "Reconstructible particles", -3000,
                           3000., -3000., 3000.0, 100, 100 );

      LHCb::Track* track = mySeedLink.first( mcPart );
      if ( track ) {
        if ( track->type() != LHCb::Track::Types::Ttrack ) continue;
        m_histoTool->plot2D( state.x(), state.y(), "reconstructedParticlesSeedOnly", "Reconstructed particles", -3000,
                             3000., -3000., 3000.0, 100, 100 );
      }
    }
  }
}
//=============================================================================
//  Plot MC hits belonging to a cluster/hit
//=============================================================================
void PrPlotFTHits::plotMCHits( const PrFTHitHandler<PrHit>& FTHitHandler ) {

  m_ghtool->setHistoDir( "Track/PrPlotFTHits/MCHits" );

  // -- MC linking
  LinkedTo<LHCb::MCHit>      myMCHitLink( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default + "2MCHits" );
  LinkedTo<LHCb::MCParticle> myClusterLink( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );

  MCTrackInfo trackInfo = make_MCTrackInfo( evtSvc(), msgSvc() );

  for ( unsigned int i = 0; i < PrFTInfo::nbZones(); i++ ) {

    const int layer = i / 2;

    char layerName[100];
    sprintf( layerName, "Layer%i", layer );
    for ( const PrHit& hit : FTHitHandler.hits( i ) ) {
      const LHCb::MCParticle* mcPartHit = myClusterLink.first( hit.id().ftID() );

      // -- only care about MCHits that belong to long or downstream reconstructible tracks
      const bool isOK =
          trackInfo.hasT( mcPartHit ) && ( trackInfo.hasTT( mcPartHit ) || trackInfo.hasVelo( mcPartHit ) );
      if ( m_onlyLongDownForMCHits && !isOK ) continue;

      // -- Get the (first) MCHit matched to the MCParticle
      LHCb::MCHit* mcHit = myMCHitLink.first( hit.id().ftID() );

      while ( mcHit != nullptr ) {
        m_histoTool->plot2D( mcHit->midPoint().X(), mcHit->midPoint().Y(), layerName, "mc hits in layer", -3200, 3200,
                             -3200, 3200, 6400, 6400 );
        mcHit = myMCHitLink.next();
      }
    }
  }
}
//=============================================================================
//  Get the PrHit corresponding to the LHCbID
//=============================================================================
const PrHit* PrPlotFTHits::getPrHit( const LHCb::LHCbID id, unsigned int zone,
                                     const PrFTHitHandler<PrHit>& FTHitHandler ) const {

  for ( zone = 0; zone < PrFTInfo::nbZones(); ++zone ) {
    const auto& r = FTHitHandler.hits( zone );
    auto        i = std::find_if( r.begin(), r.end(), [id]( const auto& h ) { return h.id() == id; } );
    if ( i != r.end() ) return &*i;
  }
  return nullptr;
}
//=============================================================================
//  Get the Track(s) corresponding to the LHCbID
//=============================================================================
std::vector<const LHCb::Track*> PrPlotFTHits::getTrack( const LHCb::LHCbID id, const std::string location ) {
  const LHCb::Tracks* tracks = getIfExists<LHCb::Tracks>( location );
  if ( !tracks ) return {};
  std::vector<const LHCb::Track*> idTracks;
  std::copy_if(
      tracks->begin(), tracks->end(), std::back_inserter( idTracks ),
      [ids = LHCb::Track::LHCbIDContainer{id}]( const LHCb::Track* t ) { return t->containsLhcbIDs( ids ); } );
  return idTracks;
}
//=============================================================================
//  Get the LiteCluster associated to a LHCbID
//=============================================================================
LHCb::FTLiteCluster PrPlotFTHits::getLiteCluster( const LHCb::LHCbID id ) {
  LHCb::FTLiteCluster cluster;
  const auto*         clusters = m_clusters.get();
  if ( clusters == nullptr ) {
    error() << "Could not find FTLite clusters at: " << LHCb::FTLiteClusterLocation::Default << endmsg;
  }
  for ( const auto& it : clusters->range() ) {
    if ( it.channelID() == id.ftID() ) {
      cluster = it;
      break;
    }
  }
  return cluster;
}

//=============================================================================
//  Get the Cluster corresponding to the LHCbID
//=============================================================================
const LHCb::FTLiteCluster* PrPlotFTHits::getCluster( const LHCb::LHCbID id ) {
  if ( !id.isFT() ) return NULL;
  const auto*                clusters = m_clusters.get();
  const LHCb::FTLiteCluster* cluster  = nullptr;

  if ( clusters == nullptr && msgLevel( MSG::ERROR ) ) {
    error() << "Could not find FT clusters at: " << LHCb::FTClusterLocation::Default << endmsg;
  }
  for ( const auto& it : clusters->range() ) {
    if ( it.channelID() == id.ftID() ) {
      cluster = &it;
      break;
    }
  }

  return cluster;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrPlotFTHits::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;
  always() << "Ciao, for the occupancy plots, normalize the histograms for the amount of events processed" << endmsg;
  return GaudiHistoAlg::finalize(); // must be called after all other actions
}

//=============================================================================

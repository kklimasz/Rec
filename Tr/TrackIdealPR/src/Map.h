/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackMaps_H
#define _TrackMaps_H

#include <map>

#include "Event/Track.h"
#include "MCInterfaces/IMCReconstructible.h"

namespace TrackIdealPR {

  typedef std::map<IMCReconstructible::RecCategory, LHCb::Track::Types> CatMap;
  const CatMap&                                                         catToType();

} // namespace TrackIdealPR

#endif

/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Measurement.h"

#include "FTDet/DeFTMat.h"
#include "MuonDet/DeMuonChamber.h"
#include "OTDet/DeOTModule.h"
#include "STDet/DeSTSector.h"
#include "UTDet/DeUTSector.h"
#include "VPDet/DeVPSensor.h"
#include "VeloDet/DeVeloPhiType.h"
#include "VeloDet/DeVeloRType.h"

namespace LHCb {

  const DetectorElement* Measurement::detectorElement() const {
    return visit( []( const FT& ft ) -> const DetectorElement* { return ft.mat; },
                  []( const Muon& m ) -> const DetectorElement* { return m.chamber; },
                  []( const OT& ot ) -> const DetectorElement* { return ot.module; },
                  []( const ST& st ) -> const DetectorElement* { return st.sector; },
                  []( const UTLite& ut ) -> const DetectorElement* { return ut.sector; },
                  []( const VeloPhi& vp ) -> const DetectorElement* { return vp.sensor; },
                  []( const VeloR& vr ) -> const DetectorElement* { return vr.sensor; },
                  []( const VP& vp ) -> const DetectorElement* { return vp.sensor; } );
  }

  double LHCb::Measurement::OT::timeOfFlight() const {
    return deltaTimeOfFlight + module->strawReferenceTimeOfFlight( ottime.channel().straw() );
  }
  void LHCb::Measurement::OT::setTimeOfFlight( double tof ) {
    deltaTimeOfFlight = tof - module->strawReferenceTimeOfFlight( ottime.channel().straw() );
  }

  double LHCb::Measurement::OT::driftTime( double arclen ) const {
    double dist2strawbegin = trajectory->arclength( arclen, trajectory->endRange() );
    double dist2strawend   = trajectory->arclength( trajectory->beginRange(), arclen );
    double propTime        = dist2strawbegin / module->propagationVelocity();
    propTime += module->walkRelation().walk( dist2strawend );
    return ottime.calibratedTime() - deltaTimeOfFlight - propTime;
  }

  double LHCb::Measurement::OT::driftTimeFromY( double globalY ) const {
    double propTime = ( trajectory->endPoint().y() - globalY ) / module->propagationVelocityY();
    double dist2strawend =
        ( globalY - trajectory->beginPoint().y() ) * module->propagationVelocity() / module->propagationVelocityY();
    propTime += module->walkRelation().walk( dist2strawend );
    return ottime.calibratedTime() - deltaTimeOfFlight - propTime;
  }

  Measurement::OT::ValueWithError LHCb::Measurement::OT::driftRadiusWithError( double arclen ) const {
    auto r = module->driftRadiusWithError( driftTime( arclen ) );
    return {r.val, r.err};
  }

  Measurement::OT::ValueWithError LHCb::Measurement::OT::driftRadiusWithErrorFromY( double globalY ) const {
    auto r = module->driftRadiusWithError( driftTimeFromY( globalY ) );
    return {r.val, r.err};
  }

  double Measurement::OT::propagationTimeFromY( double globalY ) const {
    double propTime = ( trajectory->endPoint().y() - globalY ) / module->propagationVelocityY();
    double dist2strawend =
        ( globalY - trajectory->beginPoint().y() ) * module->propagationVelocity() / module->propagationVelocityY();
    return propTime + module->walkRelation().walk( dist2strawend );
  }

  double Measurement::VeloPhi::resolution( const Gaudi::XYZPoint& p, double errMeasure ) const {
    return ( p - sensor->globalOrigin() ).R() * errMeasure;
  }

} // namespace LHCb

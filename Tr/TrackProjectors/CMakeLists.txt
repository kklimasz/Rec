###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackProjectors
################################################################################
gaudi_subdir(TrackProjectors v3r4)

gaudi_depends_on_subdirs(Det/OTDet
                         Det/STDet
                         Det/UTDet
                         Det/VPDet
                         Det/VeloDet
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiAlg
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel
                         Tr/TrackVectorFit)

find_package(GSL)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TrackProjectors
                 src/*.cpp
                 INCLUDE_DIRS GSL Tr/TrackInterfaces
                 LINK_LIBRARIES GSL OTDetLib STDetLib UTDetLib VPDetLib VeloDetLib RecEvent TrackEvent GaudiAlgLib TrackFitEvent TrackKernel TrackVectorFit)


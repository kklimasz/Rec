/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from TrackEvent
#include "Event/Measurement.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackProjector.h"

// local
#include "TrackProjectorSelector.h"

using namespace Gaudi;
using namespace LHCb;

// Declaration of the Tool Factory
DECLARE_COMPONENT( TrackProjectorSelector )
//-----------------------------------------------------------------------------
/// Standard constructor, initializes variables
//-----------------------------------------------------------------------------
TrackProjectorSelector::TrackProjectorSelector( const std::string& type, const std::string& name,
                                                const IInterface* parent )
    : base_class( type, name, parent ) {
  // FIXME: as soon as the warnings in GaudiAlg on multiple tools are gone, we
  //       can remove the different names for the
  declareProperty( "VeloR",
                   m_projNames[Measurement::Type::VeloR] = "TrajProjector<TrajProj::Velo>/TrajVeloRProjector" );
  declareProperty( "VeloPhi",
                   m_projNames[Measurement::Type::VeloPhi] = "TrajProjector<TrajProj::Velo>/TrajVeloPhiProjector" );
  declareProperty( "VeloLiteR",
                   m_projNames[Measurement::Type::VeloLiteR] = "TrajProjector<TrajProj::Velo>/TrajVeloLiteRProjector" );
  declareProperty( "VeloLitePhi", m_projNames[Measurement::Type::VeloLitePhi] =
                                      "TrajProjector<TrajProj::Velo>/TrajVeloLitePhiProjector" );
  declareProperty( "VP", m_projNames[Measurement::Type::VP] = "TrajProjector<TrajProj::VP>/TrajVPProjector" );
  declareProperty( "TT", m_projNames[Measurement::Type::TT] = "TrajProjector<TrajProj::ST>/TrajTTProjector" );
  declareProperty( "UT", m_projNames[Measurement::Type::UT] = "TrajProjector<TrajProj::UT>/TrajUTProjector" );
  declareProperty( "IT", m_projNames[Measurement::Type::IT] = "TrajProjector<TrajProj::ST>/TrajITProjector" );
  declareProperty( "OT", m_projNames[Measurement::Type::OT] = "TrajOTProjector" );
  declareProperty( "FT", m_projNames[Measurement::Type::FT] = "TrajProjector<TrajProj::FT>/TrajFTProjector" );
  declareProperty( "Muon", m_projNames[Measurement::Type::Muon] = "TrajProjector<TrajProj::Muon>/TrajMuonProjector" );
  declareProperty( "TTLite",
                   m_projNames[Measurement::Type::TTLite] = "TrajProjector<TrajProj::ST>/TrajTTLiteProjector" );
  declareProperty( "UTLite",
                   m_projNames[Measurement::Type::UTLite] = "TrajProjector<TrajProj::UT>/TrajUTLiteProjector" );
  declareProperty( "ITLite",
                   m_projNames[Measurement::Type::ITLite] = "TrajProjector<TrajProj::ST>/TrajITLiteProjector" );
}

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------
StatusCode TrackProjectorSelector::initialize() {
  StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize!", sc );

  m_projectors.clear();
  std::for_each( m_projNames.begin(), m_projNames.end(), [&]( ProjectorNames::const_reference i ) {
    m_projectors.insert( i.first, this->tool<ITrackProjector>( i.second ) );
  } );
  if ( msgLevel( MSG::DEBUG ) ) {
    std::for_each( m_projNames.begin(), m_projNames.end(), [&]( ProjectorNames::const_reference i ) {
      debug() << " projector for " << i.first << " : " << i.second << endmsg;
    } );
  }
  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------
/// select the projector;
/// TODO: return an object which represents the binding of the measurement
///       and projector (taking care of any downcasting here, when creating
///       such an object)
//-----------------------------------------------------------------------------
ITrackProjector* TrackProjectorSelector::projector( const LHCb::Measurement& m ) const {
  auto i = m_projectors.find( m.type() );
  if ( i == m_projectors.end() || !i->second ) {
    Warning( "No projector in for measurement type " ).ignore();
    if ( msgLevel( MSG::DEBUG ) ) debug() << "No projector in for measurement type " << m.type() << "!";
    return nullptr;
  }
  return i->second;
}

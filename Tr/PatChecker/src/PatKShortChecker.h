/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATKSHORTCHECKER_H
#define PATKSHORTCHECKER_H 1

#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "Linker/LinkerTool.h"

#include "GaudiAlg/GaudiAlgorithm.h"

#include <string>

/** @class PatKShortChecker PatKShortChecker.h
 *  Monitor the KShort in an event
 *
 *  @author Olivier Callot
 *  @date   2002-11-23
 *  @adapt to A-Team framework 2007-08-20 SHM
 */

class PatKShortChecker : public GaudiAlgorithm {
public:
  /// Standard constructor
  PatKShortChecker( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PatKShortChecker(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

  typedef LinkerTool<LHCb::Track, LHCb::MCParticle> TrAsct;

protected:
  std::string trackType( const LHCb::Track* tr );

  bool isKChild( const LHCb::MCParticle* part );

private:
  std::string m_inputLocation;

  TrAsct* m_trToMCP;   ///< Truth associator
  TrAsct* m_seedToMCP; ///< Truth associator
  TrAsct* m_downToMCP; ///< Truth associator

  std::vector<int> m_counter;

  std::vector<double> m_cntSeed;
  std::vector<double> m_cntDown;
};

#endif // PATKSHORTCHECKER_H

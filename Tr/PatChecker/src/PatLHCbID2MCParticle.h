/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATLHCBID2MCPARTICLE_H
#define PATLHCBID2MCPARTICLE_H 1

#include "Event/MCParticle.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"
#include "TfKernel/IOTHitCreator.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"

/** @class PatLHCbID2MCParticle PatLHCbID2MCParticle.h
 *  Build the linker table relating LHCbID used in pattern to MCParticles
 *
 *  @author Olivier Callot
 *  @date   2005-06-01
 *  @update for A-Team framework 2007-08-20 SHM
 */

class PatLHCbID2MCParticle : public GaudiAlgorithm {
public:
  /// Standard constructor
  PatLHCbID2MCParticle( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  void addToList( const LHCb::MCParticle* part ) {
    auto it = std::find( m_partList.begin(), m_partList.end(), part );
    if ( it == m_partList.end() ) m_partList.push_back( part );
  }

private:
  std::string                          m_targetName;
  std::vector<const LHCb::MCParticle*> m_partList;
  ToolHandle<Tf::IOTHitCreator>        m_otHitCreator;
  bool                                 m_linkOT;
  bool                                 m_linkIT;
  bool                                 m_linkTT; // or UT when m_useUT=true
  bool                                 m_linkVELO;
  bool                                 m_useUT;
};
#endif // PATLHCBID2MCPARTICLE_H

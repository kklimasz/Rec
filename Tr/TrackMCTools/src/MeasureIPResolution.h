/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MEASUREIPRESOLUTION_H
#define MEASUREIPRESOLUTION_H 1

// Include files
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

namespace LHCb {
  class IParticlePropertySvc;
  class MCParticle;
} // namespace LHCb

/** @class MeasureIPResolution MeasureIPResolution.h
 *  Measure the IP resolution of Velo tracks
 *
 *  @author Olivier Callot
 *  @date   2010-10-05
 */
class MeasureIPResolution : public GaudiAlgorithm {
public:
  /// Standard constructor
  MeasureIPResolution( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~MeasureIPResolution(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
  void printMCParticle( const LHCb::MCParticle* part );

private:
  std::string                         m_containerName;
  mutable LHCb::IParticlePropertySvc* m_ppSvc; ///< Pointer to particle property service

  int    m_nTracks;
  double m_averX;
  double m_averY;
  int    m_nbInCore;
  double m_sumRInCore;
  double m_sumR2InCore;
  double m_sumIPS;
  double m_sumIPS2;
};
#endif // MEASUREIPRESOLUTION_H

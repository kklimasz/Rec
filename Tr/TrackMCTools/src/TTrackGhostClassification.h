/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TTrackGhostClassification_H
#define _TTrackGhostClassification_H

#include "Event/Track.h"
#include "TrackGhostClassificationBase.h"

namespace LHCb {
  class OTChannelID;
  class STChannelID;
} // namespace LHCb

class DeOTDetector;
class DeSTDetector;

class TTrackGhostClassification : public TrackGhostClassificationBase {

public:
  /// constructer
  TTrackGhostClassification( const std::string& type, const std::string& name, const IInterface* parent );

  /** destructer */
  virtual ~TTrackGhostClassification();

  /** initialize */
  StatusCode initialize() override;

private:
  DeOTDetector* m_oTracker;
  DeSTDetector* m_iTracker;

  bool stereoOT( const LHCb::OTChannelID& chan ) const;
  bool stereoIT( const LHCb::STChannelID& chan ) const;

  StatusCode specific( LHCbIDs::const_iterator& start, LHCbIDs::const_iterator& stop,
                       LHCb::GhostTrackInfo& tinfo ) const override;
};

#endif

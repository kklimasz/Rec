/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/GhostTrackInfo.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "Linker/LinkerTool.h"

#include "TMath.h"

// local
#include "UpgradeGhostIdNT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UpgradeGhostIdNT
//
//  To Generate ntuples for the ghost probability study
//
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( UpgradeGhostIdNT )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UpgradeGhostIdNT::UpgradeGhostIdNT( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTupleTool( type, name, parent ) {
  declareInterface<IGhostProbability>( this );
}

//=============================================================================
//

namespace {

  static const int largestTrackTypes = 1 + LHCb::Track::Ttrack;

}

StatusCode UpgradeGhostIdNT::initialize() {
  if ( !GaudiTupleTool::initialize() ) return StatusCode::FAILURE;

  m_ghostTool = tool<IGhostProbability>( "UpgradeGhostId", this );
  if ( largestTrackTypes <=
       std::max( LHCb::Track::Ttrack, std::max( std::max( LHCb::Track::Velo, LHCb::Track::Upstream ),
                                                std::max( LHCb::Track::Ttrack, LHCb::Track::Downstream ) ) ) )
    return Warning( "ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",
                    StatusCode::FAILURE );

  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode UpgradeGhostIdNT::execute( LHCb::Track& aTrack ) const {
  std::vector<float>       variables = m_ghostTool->netInputs( aTrack );
  std::vector<std::string> varnames  = m_ghostTool->variableNames( aTrack.type() );
  std::vector<std::string> varnames_new;
  varnames_new.clear();
  // varnames.push_back("offset");

  if ( m_ghostTool->execute( aTrack ).isFailure() ) { return StatusCode::SUCCESS; }

  LinkerTool<LHCb::Track, LHCb::MCParticle>* link =
      new LinkerTool<LHCb::Track, LHCb::MCParticle>( evtSvc(), "Rec/Track/Best" );
  const LinkerTool<LHCb::Track, LHCb::MCParticle>::DirectType* table = link->direct();
  LinkerTool<LHCb::Track, LHCb::MCParticle>::DirectType::Range range = table->relations( &aTrack );

  Tuples::Tuple tup = GaudiTupleTool::nTuple( "tracks", CLID_ColumnWiseTuple );

  if ( LHCb::Track::Long == aTrack.type() ) {
    for ( unsigned ivar = 0; ivar < varnames.size(); ivar++ ) {
      std::string tmp_var = varnames[ivar];
      if ( ivar != ( varnames.size() - 3 ) ) {
        varnames_new.push_back( tmp_var );
      } else {
        varnames_new.push_back( tmp_var );
        varnames_new.push_back( "TRACK_NDOF" );
      }
    }
  } else {
    for ( unsigned ivar = 0; ivar < varnames.size(); ivar++ ) {
      std::string tmp_var = varnames[ivar];
      varnames_new.push_back( tmp_var );
    }
  }
  if ( varnames_new.size() != variables.size() )
    fatal() << "ALARM  " << varnames_new.size() << " != " << variables.size() << "  " << endmsg;
  for ( unsigned i = 0; i < varnames_new.size(); ++i ) {
    tup->column( varnames_new[i].c_str(), variables[i] );
    // std::cout<< aTrack.type()<<"  "<<varnames_new[i].c_str()<<"  "<<variables[i]<<std::endl;
  }

  tup->column( "ghostprob", (float)aTrack.ghostProbability() );
  tup->column( "tracks_PP_TrackHistory", (float)aTrack.history() );
  tup->column( "tracks_TRACK_Type", (float)aTrack.type() );
  LHCb::GhostTrackInfo gInfo;
  // if (m_classification->info(aTrack,gInfo).isSuccess()) {
  //  tup->column( "ghostCat",gInfo.classification() );
  //}
  tup->column( "tracks_assoc", (float)(int)( !( range.empty() ) ) );
  tup->column( "mctruepid", (float)(int)( ( range.empty() ) ? 0 : ( range.begin()->to()->particleID().pid() ) ) );
  // std::cout<<"-------> "<<aTrack.ghostProbability()<<" type "<<aTrack.type()<<std::endl;

  tup->write();
  return StatusCode::SUCCESS;
}

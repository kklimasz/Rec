/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef _WIN32
#  pragma warning( disable : 4355 ) // This used in initializer list, needed for ToolHandles
#endif

// Include files
// -------------
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// from TrackEvent
#include "Event/Track.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackKernel/TrackFunctors.h"

// Range v3
#include "range/v3/version.hpp"
#include <range/v3/range_for.hpp>
#include <range/v3/view.hpp>
// upstream has renamed namespace ranges::view ranges::views
#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

//-----------------------------------------------------------------------------
// Implementation file for class : TrackEventFitter
//
// 2005-05-30 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

template <typename TrackListType>
class TrackEventFitter : public Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )> {
public:
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::declareProperty;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::msgLevel;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::info;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::debug;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::always;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::verbose;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::counter;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::inputLocation;
  using Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::outputLocation;

  /// Standard constructor
  TrackEventFitter( const std::string& name, ISvcLocator* pSvcLocator )
      : Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>(
            name, pSvcLocator, {"TracksInContainer", LHCb::TrackLocation::Default},
            {"TracksOutContainer", LHCb::TrackLocation::Default} )
      , m_tracksFitter( "TrackMasterFitter/Fitter", this ) {
    declareProperty( "Fitter", m_tracksFitter );
  };

  StatusCode   initialize() override;
  StatusCode   finalize() override;
  LHCb::Tracks operator()( const TrackListType& ) const override;

private:
  /// interface to tracks fitter tool
  ToolHandle<ITrackFitter> m_tracksFitter;

  Gaudi::Property<bool>     m_skipFailedFitAtInput{this, "SkipFailedFitAtInput", true};
  Gaudi::Property<double>   m_maxChi2DoF{this, "MaxChi2DoF", 9999999,
                                       "Max chi2 per track when output is a new container"};
  Gaudi::Property<unsigned> m_batchSize{this, "BatchSize", 50, "Size of batches"};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nTracksCount{this, "nTracks"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nFittedCount{this, "nFitted"};
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nBadInputCount{this, "nBadInput"};
};

DECLARE_COMPONENT_WITH_ID( TrackEventFitter<std::vector<LHCb::Track>>, "VectorOfTracksFitter" )
DECLARE_COMPONENT_WITH_ID( TrackEventFitter<LHCb::Tracks>, "TrackEventFitter" )
DECLARE_COMPONENT_WITH_ID( TrackEventFitter<LHCb::Track::Selection>, "SharedTrackEventFitter" )

//=============================================================================
// Initialization
//=============================================================================
template <typename TrackListType>
StatusCode TrackEventFitter<TrackListType>::initialize() {
  StatusCode sc =
      Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  m_tracksFitter.retrieve().ignore();

  // Print out the user-defined settings
  // -----------------------------------
  info() << inputLocation() << " -> " << outputLocation() << " using " << m_tracksFitter->name() << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================

template <typename TrackListType>
LHCb::Tracks TrackEventFitter<TrackListType>::operator()( const TrackListType& tracksCont ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  LHCb::Tracks retTracks;
  retTracks.reserve( tracksCont.size() );

  // Loop over the tracks and fit them
  // ---------------------------------
  unsigned int                              nFitFail  = 0;
  unsigned int                              nBadInput = 0;
  std::vector<std::unique_ptr<LHCb::Track>> tracks_to_fit;

  std::vector<LHCb::Track> tracks{};
  tracks.reserve( tracksCont.size() );
  std::for_each( std::begin( tracksCont ), std::end( tracksCont ), [&]( auto& track ) {
    if ( Gaudi::Functional::details::deref( track ).hasKey() ) {
      tracks.emplace_back( Gaudi::Functional::details::deref( track ),
                           Gaudi::Functional::details::deref( track ).key() );
    } else {
      tracks.emplace_back( Gaudi::Functional::details::deref( track ) );
    }
  } );

  std::vector<std::reference_wrapper<LHCb::Track>> fittingTracks;
  std::vector<double>                              qopBeforeVector;

  std::for_each( std::begin( tracks ), std::end( tracks ), [&]( LHCb::Track& track ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "#### Fitting Track # " << track.key() << " ####" << endmsg
              << "  # of states before fit:" << track.nStates() << endmsg;
      if ( track.nStates() > 0 ) {
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "  States are: " << endmsg;
          const std::vector<LHCb::State*>& allstates = track.states();
          for ( unsigned int it = 0; it < allstates.size(); it++ ) {
            verbose() << "  - z = " << allstates[it]->z() << endmsg
                      << "  - stateVector = " << allstates[it]->stateVector() << endmsg << "  - covariance = " << endmsg
                      << allstates[it]->covariance() << endmsg;
          }
        } else {
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "  First state vector = " << track.firstState().stateVector()
                    << " at z = " << track.firstState().z() << endmsg;
        }
      }
    }

    if ( track.nStates() == 0 || track.checkFlag( LHCb::Track::Flags::Invalid ) ||
         ( m_skipFailedFitAtInput.value() && track.fitStatus() == LHCb::Track::FitStatus::FitFailed ) ) {
      // don't put failures on the output container. this is how they want it in HLT.
      ++nBadInput;
    } else {
      fittingTracks.push_back( track );
      qopBeforeVector.push_back( track.firstState().qOverP() );
    }
  } );

  // Fit in batch
  RANGES_FOR( auto tracksChunk, ranges::views::chunk( fittingTracks, m_batchSize.value() ) ) {
    std::vector<std::reference_wrapper<LHCb::Track>> t = tracksChunk;
    m_tracksFitter->                                 operator()( t ).ignore();
  }

  for ( auto zipped : ranges::views::zip( fittingTracks, qopBeforeVector ) ) {
    LHCb::Track& track     = zipped.first;
    double&      qopBefore = zipped.second;

    if ( track.fitStatus() == LHCb::Track::FitStatus::Fitted ) {
      std::string prefix = Gaudi::Utils::toString( track.type() );
      if ( msgLevel( MSG::INFO ) ) {
        if ( msgLevel( MSG::DEBUG ) ) { debug() << "Fitted successfully track # " << track.key() << endmsg; }
        // Update counters
        if ( track.checkFlag( LHCb::Track::Flags::Backward ) ) prefix += "Backward";
        prefix += '.';
        if ( track.nDoF() > 0 ) {
          double chisqprob = track.probChi2();
          counter( prefix + "chisqprobSum" ) += chisqprob;
          counter( prefix + "badChisq" ) += ( chisqprob < 0.01 );
        }
        counter( prefix + "flipCharge" ) += ( qopBefore * track.firstState().qOverP() < 0 );
        counter( prefix + "numOutliers" ) += nMeasurementsRemoved( track );
      }
      // Add the track to the new Tracks container if it passes the chi2-cut.
      // -----------------------------------------
      if ( m_maxChi2DoF > track.chi2PerDoF() ) {
        if ( track.hasKey() ) {
          retTracks.add( new LHCb::Track( std::move( track ), track.key() ) );
        } else {
          retTracks.add( new LHCb::Track( std::move( track ) ) );
        }
      } else {
        counter( prefix + "RejectedChisqCut" ) += 1;
      }
    } else {
      ++nFitFail;
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Unable to fit the track # " << track.key() << endmsg; }
    }
  }

  // Update counters
  // ---------------
  if ( msgLevel( MSG::INFO ) ) {
    unsigned int nTracks = tracksCont.size();
    m_nTracksCount += nTracks;
    m_nFittedCount += ( nTracks - nFitFail - nBadInput );
    m_nBadInputCount += nBadInput;

    if ( msgLevel( MSG::DEBUG ) ) {
      if ( nFitFail == 0 )
        debug() << "All " << nTracks << " tracks fitted succesfully." << endmsg;
      else
        debug() << "Fitted successfully " << ( nTracks - nFitFail - nBadInput ) << " out of " << nTracks - nBadInput
                << endmsg;
    }
  }
  return retTracks;
}

//=============================================================================
//  Finalize
//=============================================================================
template <typename TrackListType>
StatusCode TrackEventFitter<TrackListType>::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  if ( msgLevel( MSG::INFO ) ) {
    float  perf    = 0.;
    double nTracks = m_nTracksCount.sum();
    if ( nTracks > 1e-3 ) perf = float( 100.0 * m_nFittedCount.sum() / nTracks );

    info() << "  Fitting performance   : " << format( " %7.2f %%", perf ) << endmsg;
  }

  m_tracksFitter.release().ignore();
  return Gaudi::Functional::Transformer<LHCb::Tracks( const TrackListType& )>::finalize(); // must be called after all
                                                                                           // other actions
}

//=============================================================================

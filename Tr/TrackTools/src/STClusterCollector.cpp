/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Kernel
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/PhysicalConstants.h"

#include "Kernel/ITrajPoca.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "GaudiKernel/IMagneticFieldSvc.h"

// Event
#include "Event/STCluster.h"
#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Kernel/LHCbID.h"
#include "Kernel/LineTraj.h"
#include "Kernel/Trajectory.h"
#include "TrackKernel/TrackTraj.h"
#include "TsaKernel/Line.h"

// Det
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"

#include "Kernel/ISTChannelIDSelector.h"
#include "STClusterCollector.h"

#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"

DECLARE_COMPONENT( STClusterCollector )

// trivial helpers to make code clearer...
namespace {
  typedef Gaudi::Matrix1x3 DualVector;

  DualVector dual( const Gaudi::XYZVector& v ) {
    DualVector d;
    v.GetCoordinates( d.Array() );
    return d;
  }

  double dot( const DualVector& a, const Gaudi::XYZVector& b ) {
    return a( 0, 0 ) * b.X() + a( 0, 1 ) * b.Y() + a( 0, 2 ) * b.Z();
  }
} // namespace

STClusterCollector::STClusterCollector( const std::string& type, const std::string& name, const IInterface* parent )
    : extends( type, name, parent ) {
  declareSTConfigProperty( "dataLocation", m_dataLocation, LHCb::STClusterLocation::TTClusters );
}

StatusCode STClusterCollector::initialize() {

  StatusCode sc = ST::ToolBase::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );

  incSvc()->addListener( this, IncidentType::BeginEvent );

  m_magFieldSvc = svc<IMagneticFieldSvc>( "MagneticFieldSvc" ); //, this );

  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorName, "Extrapolator", this );
  m_trajPoca     = tool<ITrajPoca>( "TrajPoca" );
  if ( m_selectorName != "ALL" ) m_selector = tool<ISTChannelIDSelector>( m_selectorType, m_selectorName );

  m_refZ = ( detType() == "IT" ? 750.0 : 250.0 ) * Gaudi::Units::cm;

  if ( m_magneticField )
    info() << "Magnetic field is set to ON" << endmsg;
  else
    info() << "Magnetic field is set to OFF" << endmsg;

  return StatusCode::SUCCESS;
}

void STClusterCollector::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) { m_configured = false; }
}

void STClusterCollector::initEvent() const {

  // retrieve clusters
  const LHCb::STClusters* clusterCont = get<LHCb::STClusters>( m_dataLocation );
  m_dataCont.clear();
  m_dataCont.reserve( clusterCont->size() );

  // Loop over the clusters and make a trajectory for each cluster
  // TODO: maybe put this functionality in STMeasurement and use projectors
  for ( const auto& clus : *clusterCont ) {
    const DeSTSector* aSector = tracker()->findSector( clus->channelID() );
    m_dataCont.emplace_back( clus, std::make_unique<LHCb::BrokenLineTrajectory>(
                                       aSector->trajectory( clus->channelID(), clus->interStripFraction() ) ) );
  }
}

StatusCode STClusterCollector::execute( const LHCb::Track& track, ISTClusterCollector::Hits& outputCont ) const {

  if ( m_configured == false ) {
    m_configured = true;
    initEvent();
  }

  std::unique_ptr<LHCb::Trajectory<double>> trackTraj;

  if ( m_magneticField ) {
    // If there is a mag field, use tracktraj
    trackTraj = std::make_unique<LHCb::TrackTraj>( track, m_magFieldSvc );
  } else {
    // else, a LineTraj is used
    LHCb::State aState;
    StatusCode  sc = m_extrapolator->propagate( track, m_refZ, aState );
    if ( sc.isFailure() ) { return Warning( "Failed to extrapolate state", StatusCode::SUCCESS, 1 ); }
    Gaudi::XYZVector slope( aState.slopes() );
    slope /= slope.z();
    trackTraj =
        std::make_unique<LHCb::LineTraj<double>>( aState.position(), slope, std::make_pair( -100000., 100000. ) );
  }

  StatusCode                sc;
  LHCb::Trajectory<double>* tmpTraj;
  double                    xMin, xMax, yMin, yMax, xTest, yTest, zTest, s1, s2;
  Gaudi::XYZVector          distance;

  for ( auto iter = m_dataCont.begin(); iter != m_dataCont.end(); ++iter ) {
    // check its not on the track
    if ( m_ignoreHitsOnTrack && track.isOnTrack( LHCb::LHCbID( ( iter->first )->channelID() ) ) ) { continue; }

    const DeSTSector* aSector = findSector( ( iter->first )->channelID() );

    // check we want this sector
    if ( !select( aSector->elementID() ) ) continue;

    // get the traj
    tmpTraj = ( iter->second ).get();

    //       info() << "From : " << tmpTraj -> beginPoint()
    // 	     << " to " << tmpTraj -> endPoint() << endmsg;

    // check that y is consistant....
    yMin = tmpTraj->beginPoint().y();
    yMax = tmpTraj->endPoint().y();
    if ( yMin > yMax ) std::swap( yMin, yMax );
    zTest = tmpTraj->beginPoint().z();
    yTest = ( trackTraj->position( zTest ) ).y();
    if ( yTest > yMax + m_yTol || yTest < yMin - m_yTol ) continue;

    // and and also the x range
    xMin = tmpTraj->beginPoint().x();
    xMax = tmpTraj->endPoint().x();
    if ( xMin > xMax ) std::swap( xMin, xMax );
    xTest = ( trackTraj->position( zTest ) ).x();
    // double xTest = xLine.value(zTest);
    if ( xTest > xMax + m_xTol || xTest < xMin - m_xTol ) continue;

    // poca....
    s1 = 0;
    s2 = tmpTraj->muEstimate( trackTraj->position( s1 ) );
    sc = m_trajPoca->minimize( *trackTraj, s1, ITrajPoca::RestrictRange{true}, *tmpTraj, s2,
                               ITrajPoca::RestrictRange{true}, distance, 0.5 * Gaudi::Units::mm );

    // Set up the vector onto which we project everything
    DualVector unit = dual( ( tmpTraj->direction( s2 ).Cross( trackTraj->direction( s1 ) ) ).Unit() );

    // Calculate the residual by projecting the distance onto unit
    const double residual = -dot( unit, distance );

    if ( fabs( residual ) < m_windowSize ) {
      ISTClusterCollector::Hit hitPair;
      hitPair.cluster  = iter->first;
      hitPair.residual = residual;
      outputCont.push_back( hitPair );
    }
  } // iter
  return StatusCode::SUCCESS;
}

bool STClusterCollector::select( const LHCb::STChannelID& chan ) const {
  return m_selector == nullptr ? true : m_selector->select( chan );
}

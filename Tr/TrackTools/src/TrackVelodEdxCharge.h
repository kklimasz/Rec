/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file TrackVelodEdxCharge.h
 *
 * Header file for tool TrackVelodEdxCharge
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 18/07/2006
 */
//-----------------------------------------------------------------------------

#ifndef TRACKTOOLS_TrackVelodEdxCharge_H
#define TRACKTOOLS_TrackVelodEdxCharge_H

// base class
#include "GaudiAlg/GaudiTool.h"

// Interfaces
#include "TrackInterfaces/ITrackVelodEdxCharge.h"

// Event
#include "DetDesc/Condition.h"
#include "Event/Measurement.h"
#include "Event/Track.h"

//-----------------------------------------------------------------------------
/** @class TrackVelodEdxCharge TrackVelodEdxCharge.h
 *
 *  Implementation of ITrackVelodEdxCharge
 *
 *  This tool takes as input a Track
 *  and gives as result the "estimated number of
 *  tracks which created its VELO track" using dE/dx
 *  information from the VELO.
 *
 *  This is mainly equal to one for standard track,
 *  and 2 for track (e+, e-) which comes from a photon
 *
 *  @author Richard Beneyton
 *  @date   21/05/2003
 *
 *  Updated for DC06
 *  @author Chris Jones
 *  @date 18/07/2006
 */
//-----------------------------------------------------------------------------

class TrackVelodEdxCharge : public extends<GaudiTool, ITrackVelodEdxCharge> {

public:
  /// Constructor
  using extends::extends;

  /// Tool initialisation
  StatusCode initialize() override;

  /// Finalize and print average track charge
  StatusCode finalize() override;

public:
  /// Returns the scaled truncated mean of the ADCs for Velo measurements to check for double tracks
  /// nTrk the number of estimated particles that contributed dEdx to this track in the VELO
  StatusCode nTracks( const LHCb::Track* track, double& nTks ) const override;

private: // definitions etc.
  /// call back to update dEdx conditions from DB
  StatusCode i_cachedEdx();

private:                                                                // data
  Gaudi::Property<double> m_Normalisation{this, "Normalisation", 47.1}; ///< Expected average charge for a single track
  Gaudi::Property<double> m_Ratio{this, "Ratio",
                                  0.6}; ///< Fraction of cluster considered (1-Ratio highest clusters are discarded)
  Gaudi::Property<bool>   m_useConditions{this, "UseConditions",
                                        true}; ///< use SIMCOND or LHCBCOND values instead of properties

  Condition* m_dEdx = nullptr; ///< Condition with dEdx content in SIMCOND/LHCBCOND

  mutable std::atomic_ullong  m_totalTracks  = {0};  ///< total tracks evaluated
  mutable std::atomic_ullong  m_veloTracks   = {0};  ///< total tracks with VELO part
  mutable std::atomic<double> m_sumEffective = {0.}; ///< effective number of VELO tracks
};

#endif // TRACKTOOLS_TrackVelodEdxCharge_H

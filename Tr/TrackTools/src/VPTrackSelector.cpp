/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file VPTrackSelector.cpp
 *
 *  Implementation file for reconstruction tool VPTrackSelector,
 *  based on VeloTrackSelector.
 *
 *  @author Christoph Hombach Christoph.Hombach@hep.manchester.ac.uk
 */

#include "TrackSelector.h"

class VPTrackSelector : public TrackSelector {

public:
  /// Constructor
  using TrackSelector::TrackSelector;

  /** Returns if the given track is selected or not
   *
   *  @param track Reference to the track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept( const LHCb::Track& track ) const override;

private:
  Gaudi::Property<size_t> m_minHitsASide{this, "MinHitsASide", 0};
  Gaudi::Property<size_t> m_minHitsCSide{this, "MinHitsCSide", 0};
  Gaudi::Property<size_t> m_minHits{this, "MinHits", 0};
};

DECLARE_COMPONENT( VPTrackSelector )

//-----------------------------------------------------------------------------

bool VPTrackSelector::accept( const LHCb::Track& track ) const {
  size_t numHits[3] = {0, 0, 0};
  for ( const LHCb::LHCbID lhcbid : track.lhcbIDs() ) {
    if ( !lhcbid.isVP() ) continue;
    const LHCb::VPChannelID vpid = lhcbid.vpID();
    const unsigned int      side = ( vpid.sensor() % 2 );
    ++numHits[side];
    ++numHits[2];
  }
  return numHits[0] >= m_minHitsASide && numHits[1] >= m_minHitsCSide && numHits[2] >= m_minHits &&
         TrackSelector::accept( track );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STSPILLOVERTOOL_H
#define STSPILLOVERTOOL_H 1

// Include files
// from Gaudi
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IAlgTool.h"
#include "TrackInterfaces/ISTSpilloverTool.h"
// ROOT
#include "TH2D.h"
#include "TString.h"

// forward declarations
namespace LHCb {
  class node;
}

/** @class STSpilloverTool STSpilloverTool.h
 *
 *
 *  @author Vincenzo Battista
 *  @date   2015-06-26
 *  Purpose: compute PDF(ADC,costheta|central)/PDF(ADC|spillover) ratio
 *
 */

class STSpilloverTool : public extends<GaudiTool, ISTSpilloverTool> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override; ///< Tool initialization

  /// Return PDF(ADC,costheta|central)/PDF(ADC|spillover) starting from a track node
  double pdfRatio( const LHCb::Node* node ) const override;

private:
  /// Configurables quantities
  Gaudi::Property<std::string>         m_detType{this, "DetType",
                                         "TT"}; ///< Detector type (deprecated: uses whole ST in any case)
  Gaudi::Property<std::vector<float>>  m_adcLowEdges{this, "ADCLowEdges", {0.0}};           ///< Array of ADC bins
  Gaudi::Property<std::vector<float>>  m_cosThetaLowEdges{this, "CosThetaLowEdges", {0.0}}; ///< Array of costheta bins
  Gaudi::Property<std::vector<double>> m_TTweightVals{this, "TTWeightVals", {0.0}};         ///< Array of TT weights
  Gaudi::Property<std::vector<double>> m_ITweightVals{this, "ITWeightVals", {0.0}};         ///< Array of IT weights
  Gaudi::Property<std::vector<int>>    m_weightBins{this, "WeightBins", {0}};               ///< Array of weight bins

  /// Pointers to wrap TH2D
  float* adcLowEdges      = nullptr;
  float* cosThetaLowEdges = nullptr;

  /// Weight map binning
  int adcBins;
  int cosThetaBins;

  /// Weight maps
  TH2D* TTWeightMap = nullptr;
  TH2D* ITWeightMap = nullptr;
};

#endif // STSPILLOVERTOOL_H

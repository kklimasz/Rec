/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "MuonSeeding.h"
#include "Event/VPCluster.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonSeeding
//
// 2010-09-14 : Michel De Cian
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonSeeding )

MuonSeeding::MuonSeeding( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "MuonRecTool", m_trackTool );
  declareProperty( "Fitter", m_trackFitter );
  declareProperty( "MuomMomTool", m_momentumTool );
  declareProperty( "Extrapolator", m_extrapolator );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MuonSeeding::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) {
    return sc; // error printed already by GaudiAlgorithm
  }

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "==> Initialize" << endmsg; }

  // -- Tracking tools
  m_trackTool.retrieve();
  m_extrapolator.retrieve();
  m_trackFitter.retrieve();
  m_momentumTool.retrieve();

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MuonSeeding::execute() {
  // Chrono chrono( chronoSvc(),name()+"::execute()" );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // -- This is where we assume the track came from
  std::vector<double> PVPos = {0.0, 0.0, 0.0}; // TODO take the VELO position?

  /*const LHCb::VPCluster* vp_cluster = nullptr;
  if ( true ) { // VP cluster 'hack'
    auto vp_clusters = getIfExists<LHCb::VPClusters>( LHCb::VPClusterLocation::Default );
    if ( !vp_clusters || vp_clusters->empty() ) {
      if ( msgLevel(MSG::DEBUG) ) {
        debug() << "No VP clusters, skipping..." << endmsg;
      }

      return StatusCode::SUCCESS;
    }

    // Find the VP cluster nearest the origin
    std::vector<std::pair<float,const LHCb::VPCluster*> > scores;
    for ( const auto& cluster : *vp_clusters ) {
      scores.emplace_back(
          std::sqrt( std::pow( cluster->x(), 2) + std::pow( cluster->y(), 2) + std::pow( cluster->z(), 2) ),
          cluster );
    }

    auto min_iter = std::min_element( scores.begin(), scores.end() );
    vp_cluster = min_iter->second;
  }*/

  const auto& muonTracks = m_trackTool->tracks();
  auto        tracks     = std::make_unique<LHCb::Tracks>();

  // -- Loop over all Muon Tracks
  for ( const auto& muonTrack : muonTracks ) {
    // -- Get the momentum of the muon track, make a new track
    auto track = m_momentumTool->recMomentum( const_cast<MuonTrack&>( muonTrack ) ); //@FIXME
    if ( !track ) { continue; }

    if ( m_fitTracks ) {
      // Try and improve the covariance information
      auto sc = m_trackFitter->operator()( *track );
      if ( sc.isFailure() ) {
        if ( msgLevel( MSG::WARNING ) ) { warning() << "Track fit failed" << endmsg; }

        continue;
      }
    }

    // -- Change q/p until it points to the origin (adapted from Wouter)
    LHCb::State veloState, muonState;
    auto        sc = iterateToPV( track.get(), muonState, veloState, PVPos, muonTrack.qOverP() );
    if ( sc.isFailure() ) {
      Warning( "==> Could not iterate to PV!", StatusCode::SUCCESS, 0 ).ignore();
      continue;
    }

    // -- Set Pattern Reco status and track type, finally fit the track
    track->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );
    track->setType( LHCb::Track::Types::Muon );

    if ( !m_fitTracks ) {
      track->clearStates(); // remove the state recMomentum created
    }

    track->addToStates( veloState );
    track->addToStates( muonState );
    // ---------------------------------------------------------------

    // -- Finally, insert the track into the container!
    tracks->insert( track.release() );
  }

  // ---------------------------------------------------------------

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Filling " << tracks->size() << " tracks in " << m_outputLoc << endmsg; }

  // Control flow can stop if we didn't make anything
  setFilterPassed( !tracks->empty() );

  // Put our output on the TES
  put( tracks.release(), m_outputLoc );

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Change the q/p till the track points to the PV (stolen from Wouter)
//=============================================================================
StatusCode MuonSeeding::iterateToPV( LHCb::Track* track, LHCb::State& muonState, LHCb::State& veloState,
                                     const std::vector<double>& PVPos, double qOverP ) {
  muonState = track->closestState( 15000 );

  double dXdQoP = 1e7;
  double deltaX = 100;
  muonState.setQOverP( qOverP );

  // Set the y slope based on the target position at ~the origin
  muonState.setTy( ( muonState.y() - PVPos[1] ) / ( muonState.z() - PVPos[2] ) );

  // Set the uncertainty on ty to just come from the y uncertainty from the muon stations
  auto cov    = muonState.covariance();
  cov( 3, 3 ) = std::pow( muonState.ty(), 2 ) * ( cov( 1, 1 ) / ( muonState.y() - PVPos[1] ) );
  muonState.setCovariance( cov );

  // -- Now call the extrapolator and iterate until we have the desired accuracy.
  double             tolerance = 0.5; // [mm]
  Gaudi::TrackMatrix jacobian;

  for ( int i = 0; i < 10 && std::abs( deltaX ) > tolerance; ++i ) {
    veloState = muonState;
    auto sc   = m_extrapolator->propagate( veloState, PVPos[2], &jacobian );
    if ( sc.isFailure() ) { return StatusCode::FAILURE; }
    dXdQoP          = jacobian( 0, 4 );
    deltaX          = -( veloState.x() - PVPos[0] );
    double deltaQoP = deltaX / dXdQoP;
    muonState.setQOverP( muonState.qOverP() + deltaQoP );
  }

  veloState.setLocation( LHCb::State::Location::ClosestToBeam );
  muonState.setLocation( LHCb::State::Location::Muon );

  return m_extrapolator->propagate( veloState, PVPos[2], &jacobian );
}

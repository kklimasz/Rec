/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTTTRACK_H
#define MUONTTTRACK_H 1

// Include files
// from Gaudi
#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/MuonTrack.h"

#include "Event/MCParticle.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "MCInterfaces/IMuonPad2MCTool.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"

#include "PatKernel/PatTTHit.h"
#include "TrackInterfaces/IAddTTClusterTool.h"

#include "Kernel/LHCbID.h"
#include "MCInterfaces/ILHCbIDsToMCParticles.h"

#include "GaudiKernel/Chrono.h"

/** @class MuonTTTrack MuonTTTrack.h
 *
 * \brief  Make a MuonTTTrack: Get muon standalone track, add TT hits, refit
 *
 * Parameters:
 * - ToolName: Name for the tool that makes muon standalone track.
 * - Extrapolator: Name for the track extrapolator.
 * - MC: To enable MC association.
 * - AddTTHits: Add TT Hits to the muon track.
 * - FillMuonStubInfo: Fill parameters of muon stub in info fields of track;
 * - Output: The location the tracks should be written to.
 * - MinNTTHits: Minimal number of TT hits that need to be added to save the track.
 *
 *  @author Michel De Cian
 *  @date   2010-09-20
 */

class MuonTTTrack final : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  // -- Methods
  StatusCode fillPVs( std::vector<double>& PVPos );
  void       fillMuonStubInfo( LHCb::Track& track, const MuonTrack& muTrack ) const;
  StatusCode iterateToPV( LHCb::Track* track, LHCb::State& finalState, std::vector<double> PVPos, double qOverP );
  const LHCb::MCParticle* assocMCParticle( const std::vector<const MuonHit*> muonHits );
  bool                    isIDOnTrack( LHCb::LHCbID id, LHCb::MCParticle* mcp );

  // -- Properties
  Gaudi::Property<std::string>  m_outputLoc{this, "Output", "Rec/" + name() + "/Tracks"};
  Gaudi::Property<std::string>  m_pvLoc{this, "PVLocation", LHCb::RecVertexLocation::Primary};
  Gaudi::Property<std::string>  m_extrapolatorName{this, "Extrapolator", "TrackMasterExtrapolator"};
  Gaudi::Property<bool>         m_MC{this, "MC", false};
  Gaudi::Property<bool>         m_addTTHits{this, "AddTTHits", true};
  Gaudi::Property<bool>         m_fillMuonStubInfo{this, "FillMuonStubInfo", false};
  Gaudi::Property<std::string>  m_trackToolName{this, "ToolName", "MuonNNetRec"};
  Gaudi::Property<unsigned int> m_minNumberTTHits{this, "MinNTTHits", 2};

  // -- Tools
  IAddTTClusterTool*     m_ttHitAddingTool    = nullptr;
  ILHCbIDsToMCParticles* m_lhcbid2mcparticles = nullptr;
  IMuonPad2MCTool*       m_muonPad2MC         = nullptr;
  IMuonTrackRec*         m_trackTool          = nullptr;
  IMuonTrackMomRec*      m_momentumTool       = nullptr;
  ITrackExtrapolator*    m_extrapolator       = nullptr;
  ITrackFitter*          m_trackFitter        = nullptr;
};
#endif // GETMUONTRACK_H

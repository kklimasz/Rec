/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINITFIT_H
#define TRACKINITFIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// from TrackInterfaces
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackStateInit.h"

/** @class TrackInitFit
 *
 *
 *  @author K Holubyev
 *  @date   2009-11-14
 */
class TrackInitFit final : public extends<GaudiTool, ITrackFitter> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  StatusCode operator()( LHCb::Track& track, const LHCb::Tr::PID& pid ) const override {
    // simply init
    StatusCode sc = m_initTrack->fit( track );
    if ( sc.isFailure() ) Warning( "TrackStateInitTool failed", sc, 0 ).ignore();

    // and fit
    return m_fitTrack->operator()( track, pid );
  }

  StatusCode operator()( std::vector<std::reference_wrapper<LHCb::Track>>& tracks,
                         const LHCb::Tr::PID&                              pid ) const override {
    for ( auto& track : tracks ) {
      StatusCode sc = m_initTrack->fit( track );
      if ( sc.isFailure() ) Warning( "TrackStateInitTool failed", sc, 0 ).ignore();
    }

    // Call entrypoint with vector of tracks
    return m_fitTrack->operator()( tracks, pid );
  }

private:
  Gaudi::Property<std::string> m_initToolName{this, "Init", "TrackStateInitTool"};
  Gaudi::Property<std::string> m_fitToolName{this, "Fit", "TrackMasterFitter"};

  ITrackStateInit* m_initTrack = nullptr;
  ITrackFitter*    m_fitTrack  = nullptr;
};
#endif // TRACKINITFIT_H

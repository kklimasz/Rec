/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _OTHitExpectation_H
#define _OTHitExpectation_H

/** @class OTHitExpectation THitExpectation.h
 *
 * Implementation of OTHitExpectation tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

#include "THitExpectation.h"

#include "Event/Track.h"
#include "TsaKernel/IOTExpectedHits.h"

#include <string>
#include <vector>

namespace LHCb {
  class LHCbID;
  class OTChannelID;
} // namespace LHCb

class DeOTDetector;
class IOTExpectedHits;

class OTHitExpectation : public THitExpectation {

public:
  /** constructer */
  using THitExpectation::THitExpectation;

  /** intialize */
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to inf
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  unsigned int nExpected( const LHCb::Track& aTrack ) const override;

  /** Returns number of hits expected
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return Info info including likelihood
   */
  IHitExpectation::Info expectation( const LHCb::Track& aTrack ) const override;

  /** Collect all the expected hits
   *
   * @param aTrack Reference to the Track to test
   * @param hits collected lhcbIDs
   *
   **/
  void collect( const LHCb::Track& aTrack, std::vector<LHCb::LHCbID>& ids ) const override;

private:
  typedef std::vector<Tf::Tsa::IOTExpectedHits::OTPair> OTPairs;

  LHCb::OTChannelID channelHint( const LHCb::OTChannelID& hintChan, const std::vector<LHCb::LHCbID>& ids ) const;

  DeOTDetector*                        m_otDet          = nullptr;
  Tf::Tsa::IOTExpectedHits*            m_expectedOTHits = nullptr;
  Gaudi::Property<std::vector<double>> m_likPar{this, "likPar", {0.261, 5.1, 11.87}};
};

#endif

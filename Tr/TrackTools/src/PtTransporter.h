/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PTTRANSPORTER_H
#define PTTRANSPORTER_H 1

#include "TrackInterfaces/IPtTransporter.h" // Interface

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include <string>

/** @class PtTransporter PtTransporter.h
 *
 * provide a fast way to calculate a pt estimate in the velo, given a
 * state after the magnet
 *
 * @author Manuel Tobias Schiller <schiller@phys.uni-heidelberg.de>
 * @date   2008-04-16
 */
class PtTransporter : public extends<GaudiTool, IPtTransporter> {
public:
  /// Standard constructor
  using extends::extends;

  double ptAtOrigin( double zref, double xref, double yref, double tx, double ty, double p ) const override;
  double ptAtOrigin( const LHCb::State& state ) const override;

private:
  Gaudi::Property<double> m_zMagnet{this, "zMagnet", 5300. * Gaudi::Units::mm};
};
#endif // PTTRANSPORTER_H

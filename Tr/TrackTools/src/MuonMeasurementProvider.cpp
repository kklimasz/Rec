/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MuonMeasurementProvider MuonMeasurementProvider.cpp
 *
 *  @author Wouter Hulsbergen + Stefania Vecchi
 *  @date   30/12/2005            18/12/2009
 */

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/IMeasurementProvider.h"

#include "Event/Measurement.h"
#include "Event/StateVector.h"
#include "Event/TrackParameters.h"
#include "Kernel/LineTraj.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/LHCbMath.h"
#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include <memory>

namespace {
  // to calculate the coordinate of the cluster
  struct pos_t {
    double pos;
    double dpos;
  };
  pos_t recomputePos( const std::vector<std::array<double, 2>>& data ) {
    int    np  = 0;
    double sum = 0., sum2 = 0.;
    for ( auto i = data.begin(); i != data.end(); ++i ) {
      // check that this position is not already the same of a previous pad
      if ( std::none_of( data.begin(), i, [ref = ( *i )[0], step = 0.5 * ( *i )[1]]( const std::array<double, 2>& x ) {
             return std::abs( x[0] - ref ) < step;
           } ) ) {
        np++;
        sum += ( *i )[0];
        sum2 += std::pow( ( *i )[0], 2 );
      }
    }
    double av = sum / np;
    return {av, np > 1 ? sqrt( ( sum2 - sum * av ) / ( np - 1 ) ) : data.back()[1]};
  }

  std::array<LHCb::Measurement, 2> makeMeasurements( LHCb::LHCbID id, const DeMuonDetector& det, Gaudi::XYZPoint p,
                                                     double dx, double dy ) {
    LHCb::MuonTileID muid = id.muonID();
    // they have promised to fix the const
    const DeMuonChamber* chamber =
        det.getChmbPtr( muid.station(), muid.region(),
                        const_cast<DeMuonDetector&>( det ).Tile2Chamber( muid ).front()->chamberNumber() );
    return {LHCb::Measurement( id, p.z(),
                               LHCb::LineTraj<double>{p, Gaudi::XYZVector{0, 1, 0}, std::pair{-dy, dy},
                                                      LHCb::Trajectory<double>::DirNormalized{true}},
                               2. * dx * LHCb::Math::inv_sqrt_12, chamber ),
            LHCb::Measurement( id, p.z(),
                               LHCb::LineTraj<double>{p, Gaudi::XYZVector{1, 0, 0}, std::pair{-dx, dx},
                                                      LHCb::Trajectory<double>::DirNormalized{true}},
                               2. * dy * LHCb::Math::inv_sqrt_12, chamber )};
  }
} // namespace

class MuonMeasurementProvider : public extends<GaudiTool, IMeasurementProvider> {
public:
  /// constructer
  using extends::extends;

  StatusCode initialize() override;

  void addToMeasurements( LHCb::span<LHCb::LHCbID> ids, std::vector<LHCb::Measurement>& measurements,
                          const LHCb::ZTrajectory<double>& reftraj ) const override;

  StatusCode load( LHCb::Track& ) const override {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg;
    return StatusCode::FAILURE;
  }

private:
  /// measurement for single hits
  std::array<LHCb::Measurement, 2> measurement( const LHCb::LHCbID& id ) const;
  /// measurement for clustrer of hits
  std::array<LHCb::Measurement, 2> measurement( LHCb::span<LHCb::LHCbID> ids ) const;

  // pointer to detector
  mutable DeMuonDetector* m_det = nullptr;
  Gaudi::Property<bool>   m_clusterize{this, "clusterize", false};
};

//=============================================================================
// Declare to tool factory
//=============================================================================

DECLARE_COMPONENT( MuonMeasurementProvider )

//-----------------------------------------------------------------------------
/// Initialize
//-----------------------------------------------------------------------------

StatusCode MuonMeasurementProvider::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize!", sc ); }

  // Retrieve the detector element
  m_det = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" );

  return sc;
}

//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------

std::array<LHCb::Measurement, 2> MuonMeasurementProvider::measurement( const LHCb::LHCbID& id ) const {
  if ( !id.isMuon() ) {
    error() << "Not a Muon measurement" << endmsg;
    throw GaudiException( "LHCbID provided is not a muon ID", __func__, StatusCode::FAILURE );
  }

  LHCb::MuonTileID muid = id.muonID();
  double           x, y, z, dx, dy, dz;
  StatusCode       sc = m_det->Tile2XYZ( muid, x, dx, y, dy, z, dz );
  if ( sc.isFailure() ) {
    Warning( "Failed to get x,y,z of tile ", sc ).ignore();
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Failed to get x,y,z of tile " << muid << endmsg;
    throw GaudiException( "Failed to get tile positions", __func__, StatusCode::FAILURE );
  }

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
    debug() << " Created muon measurement! " << muid << " x " << x << " y " << y << " z " << z << " dx " << dx << " dy "
            << dy << " dz " << dz << endmsg;

  return makeMeasurements( id, *m_det, Gaudi::XYZPoint{x, y, z}, dx, dy );
}
//-----------------------------------------------------------------------------
/// Create a measurement
//-----------------------------------------------------------------------------

std::array<LHCb::Measurement, 2> MuonMeasurementProvider::measurement( LHCb::span<LHCb::LHCbID> ids ) const {
  using std::pow;
  std::vector<std::array<double, 2>> padx;
  std::vector<std::array<double, 2>> pady;
  std::vector<std::array<double, 2>> padz;
  double                             hit_minx = 100000;
  double                             hit_maxx = -100000;
  double                             hit_miny = 100000;
  double                             hit_maxy = -100000;
  double                             hit_minz = 100000;
  double                             hit_maxz = -100000;

  padx.reserve( ids.size() );
  pady.reserve( ids.size() );
  padz.reserve( ids.size() );
  for ( auto id = ids.begin(); id != ids.end(); ++id ) {
    if ( !id->isMuon() ) {
      error() << "Not a Muon measurement" << endmsg;
    } else {
      LHCb::MuonTileID muid = id->muonID();
      double           x, y, z, dx, dy, dz;
      StatusCode       sc = m_det->Tile2XYZ( muid, x, dx, y, dy, z, dz );
      if ( sc.isFailure() ) {
        Warning( "Failed to get x,y,z of tile ", sc ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Failed to get x,y,z of tile " << muid << endmsg;
      } else {
        padx.push_back( {x, dx} );
        pady.push_back( {y, dy} );
        padz.push_back( {z, dz} );
        if ( ( x - dx ) < hit_minx ) hit_minx = x - dx;
        if ( ( x + dx ) > hit_maxx ) hit_maxx = x + dx;
        if ( ( y - dy ) < hit_miny ) hit_miny = y - dy;
        if ( ( y + dy ) > hit_maxy ) hit_maxy = y + dy;
        if ( ( z - dz ) < hit_minz ) hit_minz = z - dz;
        if ( ( z + dz ) > hit_maxz ) hit_maxz = z + dz;
      }
    }
  }

  auto [Cx, Cdx] = recomputePos( padx );
  auto [Cy, Cdy] = recomputePos( pady );
  auto [Cz, Cdz] = recomputePos( padz );

  LHCb::LHCbID Cid;
  double       min_dist2 = 100000000;
  for ( auto id = ids.begin(); id != ids.end(); ++id ) {
    if ( id->isMuon() ) {
      LHCb::MuonTileID muid = id->muonID();

      double     x, y, z, dx, dy, dz;
      StatusCode sc = m_det->Tile2XYZ( muid, x, dx, y, dy, z, dz );
      if ( sc.isSuccess() ) {
        double dist2 = pow( x - Cx, 2 ) + pow( y - Cy, 2 ) + pow( z - Cz, 2 );
        if ( min_dist2 > dist2 ) {
          min_dist2 = dist2;
          Cid       = *id; // choose the id closest to the cluster center
        }
      }
    }
  }
  if ( !Cid.muonID().isValid() ) {
    error() << " IMPOSSIBLE to Create muon measurement from a cluster of " << padx.size() << " hits ! " << Cid.muonID()
            << endmsg;
    error() << " x " << Cx << " +/- " << Cdx << " in the range [" << hit_minx << "," << hit_maxx << "] " << endmsg;
    error() << " y " << Cy << " +/- " << Cdy << " in the range [" << hit_miny << "," << hit_maxy << "] " << endmsg;
    error() << " z " << Cz << " +/- " << Cdz << " in the range [" << hit_minz << "," << hit_maxz << "] " << endmsg;
  }

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << " Created muon measurement from a cluster of " << padx.size() << " hits ! " << Cid.muonID() << endmsg;
    debug() << " x " << Cx << " +/- " << Cdx << " in the range [" << hit_minx << "," << hit_maxx << "] " << endmsg;
    debug() << " y " << Cy << " +/- " << Cdy << " in the range [" << hit_miny << "," << hit_maxy << "] " << endmsg;
    debug() << " z " << Cz << " +/- " << Cdz << " in the range [" << hit_minz << "," << hit_maxz << "] " << endmsg;
  }

  return makeMeasurements( Cid, *m_det, Gaudi::XYZPoint( Cx, Cy, Cz ), Cdx, Cdy );
}

//-----------------------------------------------------------------------------
/// Create measurements for list of LHCbIDs
//-----------------------------------------------------------------------------

void MuonMeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID>        ids,
                                                 std::vector<LHCb::Measurement>& measurements,
                                                 const LHCb::ZTrajectory<double>& ) const {

  if ( !m_clusterize ) {

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << " MuonMeasurementProvider makes measurements for each LHCbID" << endmsg;

    std::for_each( ids.begin(), ids.end(), [&]( const LHCb::LHCbID& id ) {
      auto m = measurement( id );
      measurements.push_back( m[0] );
      measurements.push_back( m[1] );
    } );

  } else {

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << " MuonMeasurementProvider makes measurements for each CLUSTER of LHCbIDs" << endmsg;

    auto first = ids.begin();
    auto last  = std::stable_partition( first, ids.end(), []( const LHCb::LHCbID& id ) { return id.isMuon(); } );

    for ( unsigned iMS = 0; iMS < 5; ++iMS ) {
      auto pivot = std::stable_partition(
          first, last, [&]( const LHCb::LHCbID& id ) { return id.isMuon() && iMS == id.muonID().station(); } );
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << " Station M" << iMS << " lhcbIDS " << std::distance( first, pivot ) << endmsg;
      if ( std::distance( first, pivot ) != 0 ) {
        auto m = measurement( make_span( first, pivot ) );
        measurements.push_back( m[0] );
        measurements.push_back( m[1] );
      }
      first = pivot;
    }
  }
}

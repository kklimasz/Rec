/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file TrackSelectorBase.cpp
 *
 *  Implementation file for reconstruction tool : TrackSelectorBase
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   30/12/2005
 */
//-----------------------------------------------------------------------------

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "TrackSelectorBase.h"

//-----------------------------------------------------------------------------
StatusCode TrackSelectorBase::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // initialise track type and alg selections
  m_selTypes.clear();
  m_selTypes.resize( MaxType, false );
  for ( const auto& tt : m_trTypes ) {
    if ( tt == "Velo" ) {
      m_selTypes[LHCb::Track::Types::Velo] = true;
    } else if ( tt == "VeloR" ) {
      m_selTypes[LHCb::Track::Types::VeloR] = true;
    } else if ( tt == "Long" ) {
      m_selTypes[LHCb::Track::Types::Long] = true;
    } else if ( tt == "Upstream" ) {
      m_selTypes[LHCb::Track::Types::Upstream] = true;
    } else if ( tt == "Downstream" ) {
      m_selTypes[LHCb::Track::Types::Downstream] = true;
    } else if ( tt == "Ttrack" ) {
      m_selTypes[LHCb::Track::Types::Ttrack] = true;
    } else if ( tt == "TT" ) {
      m_selTypes[LHCb::Track::Types::TT] = true;
    } else if ( tt == "Backward" ) {
      m_selTypes[Backward] = true;
    } else {
      return Error( "Unknown track type '" + tt + "'" );
    }
  }
  // Note, track types not selected above, will automatically NOT be selected

  return sc;
}

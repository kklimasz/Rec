/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UpgradeGhostId_H
#define UpgradeGhostId_H 1

// Include files
#include "Linker/LinkerTool.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataHandle.h"
#include "GaudiKernel/IIncidentListener.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVPExpectation.h"

class IClassifierReader;
namespace Track {
  class TabulatedFunction1D;
}

/** @class UpgradeGhostId UpgradeGhostId.h
 *
 *  @author Paul Seyfert
 *  @date   30-12-2014
 */
class UpgradeGhostId : public extends<GaudiTool, IGhostProbability> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode finalize() override;   ///< Algorithm initialization
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute( LHCb::Track& aTrack ) const override;
  StatusCode beginEvent() override { return StatusCode::SUCCESS; };
  /** reveal the variable names for a track type */
  std::vector<std::string> variableNames( LHCb::Track::Types type ) const override;

  /** reveal the variable values for a track */
  std::vector<float> netInputs( LHCb::Track& ) const override {
    fatal() << "UpgradeGhostId::netInputs is NOT IMPLEMENTED" << endmsg;
    return std::vector<float>();
  }

private:
  std::vector<std::unique_ptr<IClassifierReader>>          m_readers;
  std::vector<std::unique_ptr<Track::TabulatedFunction1D>> m_flatters;

  std::vector<IHitExpectation*>             m_Expectations;
  std::vector<std::string>                  m_inNames;
  Gaudi::Property<std::vector<std::string>> m_expectorNames{
      this, "Expectors", {"UTHitExpectation", "FTHitExpectation"}};

  DataObjectReadHandle<LHCb::VPLightClusters> m_vpClusters{this, "VPClusterLocation", LHCb::VPClusterLocation::Light,
                                                           "Location of VP light clusters"};
  DataObjectReadHandle<UT::HitHandler>        m_utClusters{this, "UTClusterLocation", UT::Info::HitLocation,
                                                    "Location of UT clusters"};
};

#endif // UpgradeGhostId_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TTHitExpectation_H
#define _TTHitExpectation_H

/** @class TTHitExpectation TTHitExpectation.h
 *
 * Implementation of TTHitExpectation tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "Kernel/ISTChannelIDSelector.h"
#include "LHCbMath/GeomFun.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TsaKernel/Line3D.h"
#include <string>

namespace LHCb {
  class StateVector;
  class STChannelID;
  class LHCbID;
} // namespace LHCb

class DeSTDetector;
class DeSTSensor;
struct ITrackExtrapolator;

class TTHitExpectation : public extends<GaudiTool, IHitExpectation> {

public:
  /** constructor */
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to inf
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  unsigned int nExpected( const LHCb::Track& aTrack ) const override;

  /** Returns number of hits expected
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return Info info including likelihood
   */
  IHitExpectation::Info expectation( const LHCb::Track& aTrack ) const override;

  /** Collect all the expected hits
   *
   * @param aTrack Reference to the Track to test
   * @param hits collected lhcbIDs
   *
   **/
  void collect( const LHCb::Track& aTrack, std::vector<LHCb::LHCbID>& ids ) const override;

private:
  void collectHits( std::vector<LHCb::STChannelID>& chans, LHCb::StateVector stateVec,
                    const unsigned int station ) const;

  bool insideSensor( const DeSTSensor* sensor, const Tf::Tsa::Line3D& line ) const;

  Gaudi::XYZPoint intersection( const Tf::Tsa::Line3D& line, const Gaudi::Plane3D& aPlane ) const;

  bool select( const LHCb::STChannelID& chan ) const;

  ITrackExtrapolator*   m_extrapolator = nullptr;
  ISTChannelIDSelector* m_selector     = nullptr;
  DeSTDetector*         m_ttDet        = nullptr;

  double m_zTTa;
  double m_zTTb;

  Gaudi::Property<std::string> m_extrapolatorName{this, "extrapolatorName", "TrackParabolicExtrapolator"};
  Gaudi::Property<std::string> m_selectorType{this, "SelectorType", "STSelectChannelIDByElement"};
  Gaudi::Property<std::string> m_selectorName{this, "SelectorName", "ALL"};
  Gaudi::Property<bool>        m_allStrips{this, "allStrips", false};
};

#include "STDet/DeSTSensor.h"

inline bool TTHitExpectation::insideSensor( const DeSTSensor* sensor, const Tf::Tsa::Line3D& line ) const {

  bool            isIn = false;
  Gaudi::XYZPoint point;
  double          mu;
  if ( Gaudi::Math::intersection( line, sensor->plane(), point, mu ) == true ) {
    isIn = sensor->globalInActive( point );
  }
  return isIn;
}

inline bool TTHitExpectation::select( const LHCb::STChannelID& chan ) const {
  return m_selector == 0 ? true : m_selector->select( chan );
}

#endif

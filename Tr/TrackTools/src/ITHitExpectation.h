/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _ITHitExpectation_H
#define _ITHitExpectation_H

/** @class ITHitExpectation ITHitExpectation.h
 *
 * Implementation of ITHitExpectation tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

#include "Event/Track.h"
#include "Kernel/ISTChannelIDSelector.h"
#include "THitExpectation.h"
#include "TsaKernel/IITExpectedHits.h"
#include <string>

namespace LHCb {
  class STChannelID;
  class LHCbID;
} // namespace LHCb

class DeSTDetector;

namespace Tf {
  namespace Tsa {
    class IITExpectedHits;
  }
} // namespace Tf

class ITHitExpectation final : public THitExpectation {

public:
  /** constructor */
  using THitExpectation::THitExpectation;

  /** intialize */
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to inf
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  unsigned int nExpected( const LHCb::Track& aTrack ) const override;

  /** Returns number of hits expected
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return Info info including likelihood
   */
  IHitExpectation::Info expectation( const LHCb::Track& aTrack ) const override;

  /** Collect all the expected hits
   *
   * @param aTrack Reference to the Track to test
   * @param hits collected lhcbIDs
   *
   **/
  void collect( const LHCb::Track& aTrack, std::vector<LHCb::LHCbID>& ids ) const override;

private:
  typedef std::vector<Tf::Tsa::IITExpectedHits::ITPair> ITPairs;

  bool select( const LHCb::STChannelID& chan ) const;

  int boxToSector( const LHCb::STChannelID& chan ) const;

  DeSTDetector*             m_itDet          = nullptr;
  Tf::Tsa::IITExpectedHits* m_expectedITHits = nullptr;
  ISTChannelIDSelector*     m_selector       = nullptr;

  Gaudi::Property<std::string> m_selectorType{this, "SelectorType", "STSelectChannelIDByElement"};
  Gaudi::Property<std::string> m_selectorName{this, "SelectorName", "ALL"};
  Gaudi::Property<bool>        m_allStrips{this, "allStrips", false};
};

inline bool ITHitExpectation::select( const LHCb::STChannelID& chan ) const {
  return m_selector == 0 ? true : m_selector->select( chan );
}

#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKTOOLS_TrackSelectorBase_H
#define TRACKTOOLS_TrackSelectorBase_H

//-----------------------------------------------------------------------------
/** @class TrackSelectorBase TrackSelectorBase.h
 *
 *  Common base class for track selectors
 *
 *  @author C. Jones  Christopher.Rob.Jones@cern.ch
 *
 *  @date   30/06/2009
 */
//-----------------------------------------------------------------------------

// Interface
#include "TrackInterfaces/ITrackSelector.h"

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/HashMap.h"

// Event model
#include "Event/Track.h"

class TrackSelectorBase : public extends<GaudiTool, ITrackSelector> {
public:
  /// constructer
  using extends::extends;

  /// Tool initialization
  StatusCode initialize() override;

protected:
  enum { Backward = 12, MaxType = 32 };

  // Check track type
  inline bool checkTrackType( const LHCb::Track& aTrack ) const {
    bool      OK   = true;
    const int type = ( aTrack.checkFlag( LHCb::Track::Flags::Backward ) ? int( Backward ) : int( aTrack.type() ) );
    if ( !m_selTypes[type] ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Track type " << aTrack.type() << " is rejected" << endmsg;
      OK = false;
    }
    return OK;
  }

private: // data
  /// Track types to accept
  typedef std::vector<std::string> TrackTypes;
  Gaudi::Property<TrackTypes>      m_trTypes{this,
                                        "TrackTypes",
                                        {"Velo", "VeloR", "Long", "Upstream", "Downstream", "Ttrack", "Backward",
                                         "TT"}}; ///< List of track types to select

  /// Mapping type linking track types to selection boolean
  std::vector<bool> m_selTypes; ///< Mapping linking track types to selection boolean
};

#endif // TRACKTOOLS_TrackSelectorBase_H

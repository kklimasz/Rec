/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// -------------
#include <cmath>

// from DetDesc
#include "DetDesc/Material.h"

// local
#include "StateSimpleBetheBlochEnergyCorrectionTool.h"

#include "LHCbMath/LHCbMath.h"

using namespace Gaudi::Units;

//-----------------------------------------------------------------------------
// Implementation file for class : StateSimpleBetheBlochEnergyCorrectionTool
//
// 2006-08-18 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( StateSimpleBetheBlochEnergyCorrectionTool )

//=============================================================================
// Correct a State for dE/dx energy losses
//=============================================================================
void StateSimpleBetheBlochEnergyCorrectionTool::correctState( LHCb::State& state, const Material* material,
                                                              std::any& /*cache*/, double wallThickness, bool upstream,
                                                              double ) const {
  double bbLoss = wallThickness * sqrt( 1. + std::pow( state.tx(), 2 ) + std::pow( state.ty(), 2 ) ) *
                  m_energyLossCorr * material->Z() * material->density() / material->A();
  bbLoss = std::min( m_maxEnergyLoss.value(), bbLoss );
  if ( !upstream ) bbLoss *= -1.;

  // apply correction - note for now only correct the state vector
  Gaudi::TrackVector& tX = state.stateVector();

  //  double minMomentumForEnergyCorrection = 10*Gaudi::Units::MeV;

  double qOverP        = 0.0;
  tX[4] < 0.0 ? qOverP = std::min( tX[4], -LHCb::Math::lowTolerance )
              : qOverP = std::max( tX[4], LHCb::Math::lowTolerance );

  double newP         = 0.0;
  qOverP < 0.0 ? newP = std::min( 1.0 / qOverP - bbLoss, -m_minMomentumAfterEnergyCorr.value() )
               : newP = std::max( 1.0 / qOverP + bbLoss, m_minMomentumAfterEnergyCorr.value() );

  tX[4] = 1.0 / newP;

  // correction on cov
  if ( m_useEnergyLossError && m_sqrtEError > 0 ) {
    // error on dE is proportional to the sqrt of dE:
    // double sigmadE = m_sqrtEError * std::sqrt(std::abs(bbLoss))
    double                 err2 = m_sqrtEError * m_sqrtEError * std::abs( bbLoss ) * tX[4] * tX[4];
    Gaudi::TrackSymMatrix& cov  = state.covariance();
    cov( 4, 4 ) += err2;
  }
}

//=============================================================================

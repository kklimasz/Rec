/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackKernel/TrackVertexUtils.h"

#ifdef __INTEL_COMPILER             // Disable ICC remark from ROOT GenVector classes
#  pragma warning( disable : 1572 ) // Floating-point equality and inequality comparisons are unreliable
#endif

#include <GaudiKernel/GenericVectorTypes.h>

namespace LHCb {
  namespace TrackVertexUtils {
    ///////////////////////////////////////////////////////////////////////////
    /// Return the distance between a track state and a point
    ///////////////////////////////////////////////////////////////////////////
    double doca( const LHCb::State& state, const Gaudi::XYZPoint& pos ) {
      double tx = state.tx();
      double ty = state.ty();
      double dz = pos.z() - state.z();
      double dx = state.x() + dz * tx - pos.x();
      double dy = state.y() + dz * ty - pos.y();
      return std::sqrt( ( dx * dx + dy * dy ) / ( 1.0 + tx * tx + ty * ty ) );
    }

    /////////////////////////////////////////////////////////////////////////
    /// Compute the chi2 and decaylength of a 'particle' with respect
    /// to a vertex. This should probably go into LHCb math.
    /////////////////////////////////////////////////////////////////////////
    inline Gaudi::Vector3 transform( const Gaudi::XYZVector& vec ) {
      return Gaudi::Vector3( vec.X(), vec.Y(), vec.Z() );
    }
    ReturnStatus computeChiSquare( const Gaudi::XYZPoint& pos, const Gaudi::XYZVector& vec,
                                   const Gaudi::SymMatrix6x6& cov6, const Gaudi::XYZPoint& motherpos,
                                   const Gaudi::SymMatrix3x3& mothercov, double& chi2, double& decaylength,
                                   double& decaylengtherr ) {
      // pos:  decay vertex of particle
      // vec:  direction or momentum of particle (does not need to be normalized)
      // cov6: corresponding covariance matrix

      // This calculation is basically a 1-iteration beamspot fit. The
      // constraint is
      //
      //    r = x - lambda p/|p| - xbs
      //
      // where x and p are the position of the decay vertex of the
      // candidate and its momentum, lambda is the decaylength and xbs
      // the position of the beamspot. The covariance in the constraint
      // is
      //
      //    V = Vbs + Vxx - a * Vxp - a Vxp^T + a^2 * Vpp
      //
      // where a=lambda/|p|^2. It needs an initial estimate for the
      // flightlength, for which we simply take the projection of deltaX
      // on the direction. We now minimize  the chisquare contribution
      //
      //     chi^2 = r^T V^{-1} r
      //
      // for lambda.

      Gaudi::Vector3 dx    = transform( pos - motherpos );
      double         p3mag = vec.R();
      Gaudi::Vector3 dir   = transform( vec.Unit() );

      Gaudi::SymMatrix3x3 W = mothercov;

      double a = ROOT::Math::Dot( dir, dx ) / p3mag;
      for ( size_t row = 0; row < 3; ++row )
        for ( size_t col = 0; col <= row; ++col )
          W( row, col ) +=
              cov6( row, col ) + a * a * cov6( row + 3, col + 3 ) - a * ( cov6( row + 3, col ) + cov6( col + 3, row ) );

      int OK = W.Invert();

      double halfdChi2dLam2 = ROOT::Math::Similarity( W, dir );
      decaylength           = ROOT::Math::Dot( dir, W * dx ) / halfdChi2dLam2;
      decaylengtherr        = std::sqrt( 1 / halfdChi2dLam2 );

      Gaudi::Vector3 res = dx - decaylength * dir;

      chi2 = ROOT::Math::Similarity( W, res );

      return OK ? Success : Failure;
    }

    namespace {
      inline double addToDerivatives( const LHCb::State& state, const Gaudi::XYZPoint& vertexpos,
                                      Gaudi::Vector3& halfDChi2DX, Gaudi::SymMatrix3x3& halfD2Chi2DX2 ) {
        // compute residual
        Gaudi::Vector2 res;
        double         dz = vertexpos.z() - state.z();
        res( 0 )          = state.x() + dz * state.tx() - vertexpos.x();
        res( 1 )          = state.y() + dz * state.ty() - vertexpos.y();
        // compute the weight matrix
        const auto&         trkcov = state.covariance();
        Gaudi::SymMatrix2x2 invcov = trkcov.Sub<Gaudi::SymMatrix2x2>( 0, 0 );
        // extrapolate to the vertex
        invcov( 0, 0 ) += dz * dz * trkcov( 2, 2 ) + 2 * dz * trkcov( 2, 0 );
        invcov( 1, 0 ) += dz * dz * trkcov( 3, 2 ) + dz * ( trkcov( 3, 0 ) + trkcov( 2, 1 ) );
        invcov( 1, 1 ) += dz * dz * trkcov( 3, 3 ) + 2 * dz * trkcov( 3, 1 );
        invcov.Invert();

        // I tried to make this faster by writing it out, but H does
        // not contain sufficiently manby zeroes. Better to
        // parallelize.
        ROOT::Math::SMatrix<double, 3, 2> H;
        H( 0, 0 ) = H( 1, 1 ) = 1;
        H( 2, 0 )             = -state.tx();
        H( 2, 1 )             = -state.ty();
        halfD2Chi2DX2 += ROOT::Math::Similarity( H, invcov );
        halfDChi2DX += ( H * invcov ) * res;
        // You could potentially save time by reusing HW. However, it
        // does not help enough. Perhaps better when parallelized.
        // ROOT::Math::SMatrix<double,3,2> HW = H*invcov ;
        // Gaudi::SymMatrix3x3 HWH ;
        // ROOT::Math::AssignSym::Evaluate( HWH, HW*ROOT::Math::Transpose(H) ) ;
        // halfD2Chi2DX2 += HWH ;
        // halfDChi2DX += HW * res ;
        return ROOT::Math::Similarity( res, invcov );
      }

      inline double solve( const Gaudi::Vector3& halfDChi2DX, const Gaudi::SymMatrix3x3& halfD2Chi2DX2,
                           Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov ) {
        vertexcov = halfD2Chi2DX2;
        vertexcov.InvertChol();
        Gaudi::Vector3 delta = vertexcov * halfDChi2DX;
        vertexpos.SetX( vertexpos.x() + delta( 0 ) );
        vertexpos.SetY( vertexpos.y() + delta( 1 ) );
        vertexpos.SetZ( vertexpos.z() + delta( 2 ) );
        return -1 * ROOT::Math::Dot( delta, halfDChi2DX );
      }
    } // namespace

    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexweight, Gaudi::SymMatrix3x3& vertexcov ) {
      // initialize the vertex position with the poca
      poca( stateA, stateB, vertexpos );
      vertexweight = Gaudi::SymMatrix3x3{};
      // add both tracks
      Gaudi::Vector3 halfDChi2DX{};
      double         chi2( 0 );
      chi2 += addToDerivatives( stateA, vertexpos, halfDChi2DX, vertexweight );
      chi2 += addToDerivatives( stateB, vertexpos, halfDChi2DX, vertexweight );
      // compute the vertex
      chi2 += solve( halfDChi2DX, vertexweight, vertexpos, vertexcov );
      return chi2;
    }

    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexcov ) {
      Gaudi::SymMatrix3x3 vertexweight;
      return vertex( stateA, stateB, vertexpos, vertexweight, vertexcov );
    }

    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                        Gaudi::SymMatrix3x3& vertexcov ) {
      // add the track
      Gaudi::Vector3 halfDChi2DX{}; // does default initializer initialize to zero? not for libEIGEN!
      double         chi2 = addToDerivatives( state, vertexpos, halfDChi2DX, vertexweight );
      // compute the vertex
      chi2 += solve( halfDChi2DX, vertexweight, vertexpos, vertexcov );
      return chi2;
    }

    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov ) {
      Gaudi::SymMatrix3x3 vertexweight = vertexcov;
      vertexweight.InvertChol();
      return addToVertex( state, vertexpos, vertexweight, vertexcov );
    }
  } // namespace TrackVertexUtils
} // namespace LHCb

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Vector {

      template <size_t W, bool checkNodeType>
      inline void update( fp_ptr_64_const rv, fp_ptr_64_const pm, fp_ptr_64_const rr, fp_ptr_64_const em, fp_ptr_64 us,
                          fp_ptr_64 uc, fp_ptr_64 chi2, const std::array<Sch::Item, W>& n ) {
        MathCommon<W>::template update<checkNodeType>( rv, pm, rr, em, us, uc, chi2, n );
      }

    } // namespace Vector

  } // namespace TrackVectorFit

} // namespace Tr

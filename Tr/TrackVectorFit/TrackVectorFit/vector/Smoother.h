/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Vector {

      template <size_t W>
      inline uint16_t smoother( fp_ptr_64_const s1, fp_ptr_64_const s2, fp_ptr_64_const c1, fp_ptr_64_const c2,
                                fp_ptr_64 ss, fp_ptr_64 sc ) {
        return MathCommon<W>::average( s1, c1, s2, c2, ss, sc );
      }

      template <size_t W, bool checkNodeType>
      inline void updateResiduals( fp_ptr_64_const rv, fp_ptr_64_const pm, fp_ptr_64_const rr, fp_ptr_64_const em,
                                   fp_ptr_64_const ss, fp_ptr_64_const sc, fp_ptr_64 res, fp_ptr_64 errRes,
                                   const std::array<Sch::Item, W>& n ) {
        MathCommon<W>::template updateResiduals<checkNodeType>( rv, pm, rr, em, ss, sc, res, errRes, n );
      }

    } // namespace Vector

  } // namespace TrackVectorFit

} // namespace Tr

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Scalar {

      template <bool F, bool B, class U = typename std::conditional<F == true and B == true, bool, void>::type>
      inline U smoother( Node& node );

      template <>
      inline void smoother<false, true>( Node& node ) {
        node.get<Op::Smooth, Op::StateVector>().setBasePointer( node.get<Op::Backward, Op::StateVector>() );
        node.get<Op::Smooth, Op::Covariance>().setBasePointer( node.get<Op::Backward, Op::Covariance>() );
      }

      template <>
      inline void smoother<true, false>( Node& node ) {
        node.get<Op::Smooth, Op::StateVector>().setBasePointer( node.get<Op::Forward, Op::StateVector>() );
        node.get<Op::Smooth, Op::Covariance>().setBasePointer( node.get<Op::Forward, Op::Covariance>() );
      }

      template <>
      inline bool smoother<true, true>( Node& node ) {
        return Math::average( node.get<Op::Forward, Op::StateVector>(), node.get<Op::Forward, Op::Covariance>(),
                              node.get<Op::Backward, Op::StateVector>(), node.get<Op::Backward, Op::Covariance>(),
                              node.get<Op::Smooth, Op::StateVector>(), node.get<Op::Smooth, Op::Covariance>() );
      }

      inline void updateResiduals( Node& node ) {
        TRACKVECTORFIT_PRECISION value{0.0}, error{0.0};
        if ( node.node().hasMeasurement() ) {
          const auto&              pm = node.get<Op::NodeParameters, Op::ProjectionMatrix>();
          TRACKVECTORFIT_PRECISION HCH;
          Math::similarity_5_1( node.get<Op::Smooth, Op::Covariance>(), pm, &HCH );

          const auto&                     rv   = node.get<Op::NodeParameters, Op::ReferenceVector>();
          const auto&                     ss   = node.get<Op::Smooth, Op::StateVector>();
          const TRACKVECTORFIT_PRECISION& em   = node.get<Op::NodeParameters, Op::ErrMeasure>();
          const TRACKVECTORFIT_PRECISION& sign = ( node.node().type() == LHCb::Node::Type::HitOnTrack ) ? -1 : 1;

          value =
              node.refResidual() + ( pm[0] * ( rv[0] - ss[0] ) + pm[1] * ( rv[1] - ss[1] ) + pm[2] * ( rv[2] - ss[2] ) +
                                     pm[3] * ( rv[3] - ss[3] ) + pm[4] * ( rv[4] - ss[4] ) );

          error = em * em + sign * HCH;
        }

        const auto v                            = Gaudi::Math::ValueWithError( value, error );
        node.get<Op::Smooth, Op::Residual>()    = v.value();
        node.get<Op::Smooth, Op::ErrResidual>() = v.error();
      }

    } // namespace Scalar

  } // namespace TrackVectorFit

} // namespace Tr

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../Types.h"
#include "Math.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Scalar {

      template <class D>
      inline void update( Node& node ) {
        if ( node.node().type() == LHCb::Node::Type::HitOnTrack ) {
          Math::update(
              node.get<D, Op::StateVector>(), node.get<D, Op::Covariance>(), node.get<D, Op::Chi2>(),
              node.get<Op::NodeParameters, Op::ReferenceVector>(), node.get<Op::NodeParameters, Op::ProjectionMatrix>(),
              node.get<Op::NodeParameters, Op::ReferenceResidual>(), node.get<Op::NodeParameters, Op::ErrMeasure>() );
        }
      }

    } // namespace Scalar

  } // namespace TrackVectorFit

} // namespace Tr

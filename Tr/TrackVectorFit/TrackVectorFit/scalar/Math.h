/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../MemView.h"
#include "../VectorConfiguration.h"
#include "LHCbMath/Similarity.h"

namespace Tr {

  namespace TrackVectorFit {

    namespace Scalar {

      struct Math {
        template <class T, class U>
        static inline void transportCovariance( const T& tm, const U& uc, Mem::View::TrackSymMatrix& pc ) {
          bool isLine = tm[4] == 0;
          if ( !isLine ) {
            similarity_5_5( tm, uc, pc );
          } else {
            pc.copy( uc );

            if ( tm[2] != 0 || tm[8] != 0 ) {
              pc[0] += 2 * uc[3] * tm[2] + uc[5] * tm[2] * tm[2];
              pc[3] += uc[5] * tm[2];
              pc[2] += 2 * uc[7] * tm[8] + uc[9] * tm[8] * tm[8];
              pc[7] += uc[9] * tm[8];
              pc[1] += uc[4] * tm[2] + uc[6] * tm[8] + uc[8] * tm[2] * tm[8];
              pc[4] += uc[8] * tm[8];
              pc[6] += uc[8] * tm[2];
            }
          }
        }

        static inline void transportCovariance( const Gaudi::TrackMatrix& F, const Gaudi::TrackSymMatrix& origin,
                                                Gaudi::TrackSymMatrix& target ) {
          bool isLine = F( 0, 4 ) == 0;
          if ( !isLine ) {
            // use vectorized similarity transform
            LHCb::Math::Similarity( F, origin, target );
          } else {
            target = origin;
            target( 0, 0 ) += 2 * origin( 2, 0 ) * F( 0, 2 ) + origin( 2, 2 ) * F( 0, 2 ) * F( 0, 2 );
            target( 2, 0 ) += origin( 2, 2 ) * F( 0, 2 );
            target( 1, 1 ) += 2 * origin( 3, 1 ) * F( 1, 3 ) + origin( 3, 3 ) * F( 1, 3 ) * F( 1, 3 );
            target( 3, 1 ) += origin( 3, 3 ) * F( 1, 3 );
            target( 1, 0 ) +=
                origin( 2, 1 ) * F( 0, 2 ) + origin( 3, 0 ) * F( 1, 3 ) + origin( 3, 2 ) * F( 0, 2 ) * F( 1, 3 );
            target( 2, 1 ) += origin( 3, 2 ) * F( 1, 3 );
            target( 3, 0 ) += origin( 3, 2 ) * F( 0, 2 );
          }
        }

        static inline void invertMatrix( const Gaudi::TrackMatrix& tm, Mem::View::TrackMatrix<25>& itm ) {
          const bool isLine = tm( 0, 4 ) == 0;
          if ( isLine ) {
            for ( unsigned i = 0; i < 25; ++i ) { itm[i] = tm.Array()[i]; }
            itm[2] = -itm[2];
            itm[8] = -itm[8];
          } else {
            // itm(0,0) = itm(1,1) = itm(4,4) = 1;
            // write
            //       ( 1  0 |  S00 S01 | U0 )
            //       ( 0  1 |  S10 S01 | U1 )
            // tm =  ( 0  0 |  T00 T01 | V0 )
            //       ( 0  0 |  T10 T11 | V1 )
            //       ( 0  0 |   0   0  | 1  )
            // then we have
            // Tinv = T^{-1}
            double det = tm( 2, 2 ) * tm( 3, 3 ) - tm( 2, 3 ) * tm( 3, 2 );

            // x*5 + y
            itm[12] = tm( 3, 3 ) / det;
            itm[18] = tm( 2, 2 ) / det;
            itm[13] = -tm( 2, 3 ) / det;
            itm[17] = -tm( 3, 2 ) / det;
            // Vinv = - T^-1 * V
            itm[14] = -itm[12] * tm( 2, 4 ) - itm[13] * tm( 3, 4 );
            itm[19] = -itm[17] * tm( 2, 4 ) - itm[18] * tm( 3, 4 );
            // Uinv = S * T^-1 * V - U = - S * Vinv - U
            itm[4] = -tm( 0, 4 ) - tm( 0, 2 ) * itm[14] - tm( 0, 3 ) * itm[19];
            itm[9] = -tm( 1, 4 ) - tm( 1, 2 ) * itm[14] - tm( 1, 3 ) * itm[19];
            // Sinv  = - S * T^{-1}
            itm[2] = -tm( 0, 2 ) * itm[12] - tm( 0, 3 ) * itm[17];
            itm[3] = -tm( 0, 2 ) * itm[13] - tm( 0, 3 ) * itm[18];
            itm[7] = -tm( 1, 2 ) * itm[12] - tm( 1, 3 ) * itm[17];
            itm[8] = -tm( 1, 2 ) * itm[13] - tm( 1, 3 ) * itm[18];

            // Rest of elements
            for ( unsigned i = 0; i < 5; ++i ) {
              itm[5 * i]     = tm.Array()[5 * i];
              itm[5 * i + 1] = tm.Array()[5 * i + 1];
            }
            itm[22] = tm.Array()[22];
            itm[23] = tm.Array()[23];
            itm[24] = tm.Array()[24];
          }
        }

        static inline void invertMatrix( const Gaudi::TrackMatrix& tm, Gaudi::TrackMatrix& itm ) {
          // it would save time if qw assume that Finv is either diagonal or filled with 0?
          itm         = tm;
          bool isLine = tm( 0, 4 ) == 0;
          if ( isLine ) {
            itm( 0, 2 ) = -tm( 0, 2 );
            itm( 1, 3 ) = -tm( 1, 3 );
          } else {
            // itm(0,0) = itm(1,1) = itm(4,4) = 1 ;
            // write
            //      ( 1  0 |  S00 S01 | U0 )
            //      ( 0  1 |  S10 S01 | U1 )
            // tm =  ( 0  0 |  T00 T01 | V0 )
            //      ( 0  0 |  T10 T11 | V1 )
            //      ( 0  0 |   0   0  | 1  )
            // then we have
            // Tinv = T^{-1}
            double det  = tm( 2, 2 ) * tm( 3, 3 ) - tm( 2, 3 ) * tm( 3, 2 );
            itm( 2, 2 ) = tm( 3, 3 ) / det;
            itm( 3, 3 ) = tm( 2, 2 ) / det;
            itm( 2, 3 ) = -tm( 2, 3 ) / det;
            itm( 3, 2 ) = -tm( 3, 2 ) / det;
            // Vinv = - T^-1 * V
            itm( 2, 4 ) = -itm( 2, 2 ) * tm( 2, 4 ) - itm( 2, 3 ) * tm( 3, 4 );
            itm( 3, 4 ) = -itm( 3, 2 ) * tm( 2, 4 ) - itm( 3, 3 ) * tm( 3, 4 );
            // Uinv = S * T^-1 * V - U = - S * Vinv - U
            itm( 0, 4 ) = -tm( 0, 4 ) - tm( 0, 2 ) * itm( 2, 4 ) - tm( 0, 3 ) * itm( 3, 4 );
            itm( 1, 4 ) = -tm( 1, 4 ) - tm( 1, 2 ) * itm( 2, 4 ) - tm( 1, 3 ) * itm( 3, 4 );
            // Sinv  = - S * T^{-1}
            itm( 0, 2 ) = -tm( 0, 2 ) * itm( 2, 2 ) - tm( 0, 3 ) * itm( 3, 2 );
            itm( 0, 3 ) = -tm( 0, 2 ) * itm( 2, 3 ) - tm( 0, 3 ) * itm( 3, 3 );
            itm( 1, 2 ) = -tm( 1, 2 ) * itm( 2, 2 ) - tm( 1, 3 ) * itm( 3, 2 );
            itm( 1, 3 ) = -tm( 1, 2 ) * itm( 2, 3 ) - tm( 1, 3 ) * itm( 3, 3 );
          }
        }

        static inline void similarity_5_1( const Mem::View::TrackSymMatrix& Ci, const Mem::View::TrackVector& Fi,
                                           TRACKVECTORFIT_PRECISION* ti ) {
          auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1] + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
          auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1] + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
          auto _2 = Ci[3] * Fi[0] + Ci[4] * Fi[1] + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
          auto _3 = Ci[6] * Fi[0] + Ci[7] * Fi[1] + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
          auto _4 = Ci[10] * Fi[0] + Ci[11] * Fi[1] + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];
          *ti     = Fi[0] * _0 + Fi[1] * _1 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;
        }

        template <class T, class U, class V>
        static inline void similarity_5_5( const U& tm, const T& uc, V& pc ) {
          auto _0 = uc[0] * tm[0] + uc[1] * tm[1] + uc[3] * tm[2] + uc[6] * tm[3] + uc[10] * tm[4];
          auto _1 = uc[1] * tm[0] + uc[2] * tm[1] + uc[4] * tm[2] + uc[7] * tm[3] + uc[11] * tm[4];
          auto _2 = uc[3] * tm[0] + uc[4] * tm[1] + uc[5] * tm[2] + uc[8] * tm[3] + uc[12] * tm[4];
          auto _3 = uc[6] * tm[0] + uc[7] * tm[1] + uc[8] * tm[2] + uc[9] * tm[3] + uc[13] * tm[4];
          auto _4 = uc[10] * tm[0] + uc[11] * tm[1] + uc[12] * tm[2] + uc[13] * tm[3] + uc[14] * tm[4];
          pc[0]   = tm[0] * _0 + tm[1] * _1 + tm[2] * _2 + tm[3] * _3 + tm[4] * _4;
          pc[1]   = tm[5] * _0 + tm[6] * _1 + tm[7] * _2 + tm[8] * _3 + tm[9] * _4;
          pc[3]   = tm[10] * _0 + tm[11] * _1 + tm[12] * _2 + tm[13] * _3 + tm[14] * _4;
          pc[6]   = tm[15] * _0 + tm[16] * _1 + tm[17] * _2 + tm[18] * _3 + tm[19] * _4;
          pc[10]  = tm[20] * _0 + tm[21] * _1 + tm[22] * _2 + tm[23] * _3 + tm[24] * _4;
          _0      = uc[0] * tm[5] + uc[1] * tm[6] + uc[3] * tm[7] + uc[6] * tm[8] + uc[10] * tm[9];
          _1      = uc[1] * tm[5] + uc[2] * tm[6] + uc[4] * tm[7] + uc[7] * tm[8] + uc[11] * tm[9];
          _2      = uc[3] * tm[5] + uc[4] * tm[6] + uc[5] * tm[7] + uc[8] * tm[8] + uc[12] * tm[9];
          _3      = uc[6] * tm[5] + uc[7] * tm[6] + uc[8] * tm[7] + uc[9] * tm[8] + uc[13] * tm[9];
          _4      = uc[10] * tm[5] + uc[11] * tm[6] + uc[12] * tm[7] + uc[13] * tm[8] + uc[14] * tm[9];
          pc[2]   = tm[5] * _0 + tm[6] * _1 + tm[7] * _2 + tm[8] * _3 + tm[9] * _4;
          pc[4]   = tm[10] * _0 + tm[11] * _1 + tm[12] * _2 + tm[13] * _3 + tm[14] * _4;
          pc[7]   = tm[15] * _0 + tm[16] * _1 + tm[17] * _2 + tm[18] * _3 + tm[19] * _4;
          pc[11]  = tm[20] * _0 + tm[21] * _1 + tm[22] * _2 + tm[23] * _3 + tm[24] * _4;
          _0      = uc[0] * tm[10] + uc[1] * tm[11] + uc[3] * tm[12] + uc[6] * tm[13] + uc[10] * tm[14];
          _1      = uc[1] * tm[10] + uc[2] * tm[11] + uc[4] * tm[12] + uc[7] * tm[13] + uc[11] * tm[14];
          _2      = uc[3] * tm[10] + uc[4] * tm[11] + uc[5] * tm[12] + uc[8] * tm[13] + uc[12] * tm[14];
          _3      = uc[6] * tm[10] + uc[7] * tm[11] + uc[8] * tm[12] + uc[9] * tm[13] + uc[13] * tm[14];
          _4      = uc[10] * tm[10] + uc[11] * tm[11] + uc[12] * tm[12] + uc[13] * tm[13] + uc[14] * tm[14];
          pc[5]   = tm[10] * _0 + tm[11] * _1 + tm[12] * _2 + tm[13] * _3 + tm[14] * _4;
          pc[8]   = tm[15] * _0 + tm[16] * _1 + tm[17] * _2 + tm[18] * _3 + tm[19] * _4;
          pc[12]  = tm[20] * _0 + tm[21] * _1 + tm[22] * _2 + tm[23] * _3 + tm[24] * _4;
          _0      = uc[0] * tm[15] + uc[1] * tm[16] + uc[3] * tm[17] + uc[6] * tm[18] + uc[10] * tm[19];
          _1      = uc[1] * tm[15] + uc[2] * tm[16] + uc[4] * tm[17] + uc[7] * tm[18] + uc[11] * tm[19];
          _2      = uc[3] * tm[15] + uc[4] * tm[16] + uc[5] * tm[17] + uc[8] * tm[18] + uc[12] * tm[19];
          _3      = uc[6] * tm[15] + uc[7] * tm[16] + uc[8] * tm[17] + uc[9] * tm[18] + uc[13] * tm[19];
          _4      = uc[10] * tm[15] + uc[11] * tm[16] + uc[12] * tm[17] + uc[13] * tm[18] + uc[14] * tm[19];
          pc[9]   = tm[15] * _0 + tm[16] * _1 + tm[17] * _2 + tm[18] * _3 + tm[19] * _4;
          pc[13]  = tm[20] * _0 + tm[21] * _1 + tm[22] * _2 + tm[23] * _3 + tm[24] * _4;
          _0      = uc[0] * tm[20] + uc[1] * tm[21] + uc[3] * tm[22] + uc[6] * tm[23] + uc[10] * tm[24];
          _1      = uc[1] * tm[20] + uc[2] * tm[21] + uc[4] * tm[22] + uc[7] * tm[23] + uc[11] * tm[24];
          _2      = uc[3] * tm[20] + uc[4] * tm[21] + uc[5] * tm[22] + uc[8] * tm[23] + uc[12] * tm[24];
          _3      = uc[6] * tm[20] + uc[7] * tm[21] + uc[8] * tm[22] + uc[9] * tm[23] + uc[13] * tm[24];
          _4      = uc[10] * tm[20] + uc[11] * tm[21] + uc[12] * tm[22] + uc[13] * tm[23] + uc[14] * tm[24];
          pc[14]  = tm[20] * _0 + tm[21] * _1 + tm[22] * _2 + tm[23] * _3 + tm[24] * _4;
        }

        static inline void update( Mem::View::TrackVector& X, Mem::View::TrackSymMatrix& C,
                                   TRACKVECTORFIT_PRECISION& chi2, const Mem::View::TrackVector& Xref,
                                   const Mem::View::TrackVector& H, const TRACKVECTORFIT_PRECISION& refResidual,
                                   const TRACKVECTORFIT_PRECISION& errorMeas ) {
          // The ugly code below makes the filter step about 20% faster
          // than SMatrix would do it.
          auto res = refResidual + H[0] * ( Xref[0] - X[0] ) + H[1] * ( Xref[1] - X[1] ) + H[2] * ( Xref[2] - X[2] ) +
                     H[3] * ( Xref[3] - X[3] ) + H[4] * ( Xref[4] - X[4] );
          TRACKVECTORFIT_PRECISION CHT[5] = {C[0] * H[0] + C[1] * H[1] + C[3] * H[2] + C[6] * H[3] + C[10] * H[4],
                                             C[1] * H[0] + C[2] * H[1] + C[4] * H[2] + C[7] * H[3] + C[11] * H[4],
                                             C[3] * H[0] + C[4] * H[1] + C[5] * H[2] + C[8] * H[3] + C[12] * H[4],
                                             C[6] * H[0] + C[7] * H[1] + C[8] * H[2] + C[9] * H[3] + C[13] * H[4],
                                             C[10] * H[0] + C[11] * H[1] + C[12] * H[2] + C[13] * H[3] + C[14] * H[4]};
          auto errorResInv = 1.0 / ( ( errorMeas * errorMeas ) + H[0] * CHT[0] + H[1] * CHT[1] + H[2] * CHT[2] +
                                     H[3] * CHT[3] + H[4] * CHT[4] );

          // update the state vector and cov matrix
          auto w = res * errorResInv;
          X[0] += CHT[0] * w;
          X[1] += CHT[1] * w;
          X[2] += CHT[2] * w;
          X[3] += CHT[3] * w;
          X[4] += CHT[4] * w;

          C[0] -= errorResInv * CHT[0] * CHT[0];
          C[1] -= errorResInv * CHT[1] * CHT[0];
          C[3] -= errorResInv * CHT[2] * CHT[0];
          C[6] -= errorResInv * CHT[3] * CHT[0];
          C[10] -= errorResInv * CHT[4] * CHT[0];

          C[2] -= errorResInv * CHT[1] * CHT[1];
          C[4] -= errorResInv * CHT[2] * CHT[1];
          C[7] -= errorResInv * CHT[3] * CHT[1];
          C[11] -= errorResInv * CHT[4] * CHT[1];

          C[5] -= errorResInv * CHT[2] * CHT[2];
          C[8] -= errorResInv * CHT[3] * CHT[2];
          C[12] -= errorResInv * CHT[4] * CHT[2];

          C[9] -= errorResInv * CHT[3] * CHT[3];
          C[13] -= errorResInv * CHT[4] * CHT[3];

          C[14] -= errorResInv * CHT[4] * CHT[4];

          chi2 = res * res * errorResInv;
        }

        static inline bool average( const Mem::View::TrackVector& X1, const Mem::View::TrackSymMatrix& C1,
                                    const Mem::View::TrackVector& X2, const Mem::View::TrackSymMatrix& C2,
                                    Mem::View::TrackVector& X, Mem::View::TrackSymMatrix& C ) {
          // compute the inverse of the covariance (i.e. weight) of the difference: R=(C1+C2)
          Gaudi::TrackSymMatrix invRM;
          auto                  invR = invRM.Array();
          for ( int i = 0; i < 15; ++i ) { invR[i] = C1[i] + C2[i]; }

          bool success = invRM.InvertChol();

          // compute the gain matrix

          // K <- C1*inverse(C1+C2) = C1*invR
          TRACKVECTORFIT_PRECISION K[25];
          K[0] = C1[0] * invR[0] + C1[1] * invR[1] + C1[3] * invR[3] + C1[6] * invR[6] + C1[10] * invR[10];
          K[1] = C1[0] * invR[1] + C1[1] * invR[2] + C1[3] * invR[4] + C1[6] * invR[7] + C1[10] * invR[11];
          K[2] = C1[0] * invR[3] + C1[1] * invR[4] + C1[3] * invR[5] + C1[6] * invR[8] + C1[10] * invR[12];
          K[3] = C1[0] * invR[6] + C1[1] * invR[7] + C1[3] * invR[8] + C1[6] * invR[9] + C1[10] * invR[13];
          K[4] = C1[0] * invR[10] + C1[1] * invR[11] + C1[3] * invR[12] + C1[6] * invR[13] + C1[10] * invR[14];

          K[5] = C1[1] * invR[0] + C1[2] * invR[1] + C1[4] * invR[3] + C1[7] * invR[6] + C1[11] * invR[10];
          K[6] = C1[1] * invR[1] + C1[2] * invR[2] + C1[4] * invR[4] + C1[7] * invR[7] + C1[11] * invR[11];
          K[7] = C1[1] * invR[3] + C1[2] * invR[4] + C1[4] * invR[5] + C1[7] * invR[8] + C1[11] * invR[12];
          K[8] = C1[1] * invR[6] + C1[2] * invR[7] + C1[4] * invR[8] + C1[7] * invR[9] + C1[11] * invR[13];
          K[9] = C1[1] * invR[10] + C1[2] * invR[11] + C1[4] * invR[12] + C1[7] * invR[13] + C1[11] * invR[14];

          K[10] = C1[3] * invR[0] + C1[4] * invR[1] + C1[5] * invR[3] + C1[8] * invR[6] + C1[12] * invR[10];
          K[11] = C1[3] * invR[1] + C1[4] * invR[2] + C1[5] * invR[4] + C1[8] * invR[7] + C1[12] * invR[11];
          K[12] = C1[3] * invR[3] + C1[4] * invR[4] + C1[5] * invR[5] + C1[8] * invR[8] + C1[12] * invR[12];
          K[13] = C1[3] * invR[6] + C1[4] * invR[7] + C1[5] * invR[8] + C1[8] * invR[9] + C1[12] * invR[13];
          K[14] = C1[3] * invR[10] + C1[4] * invR[11] + C1[5] * invR[12] + C1[8] * invR[13] + C1[12] * invR[14];

          K[15] = C1[6] * invR[0] + C1[7] * invR[1] + C1[8] * invR[3] + C1[9] * invR[6] + C1[13] * invR[10];
          K[16] = C1[6] * invR[1] + C1[7] * invR[2] + C1[8] * invR[4] + C1[9] * invR[7] + C1[13] * invR[11];
          K[17] = C1[6] * invR[3] + C1[7] * invR[4] + C1[8] * invR[5] + C1[9] * invR[8] + C1[13] * invR[12];
          K[18] = C1[6] * invR[6] + C1[7] * invR[7] + C1[8] * invR[8] + C1[9] * invR[9] + C1[13] * invR[13];
          K[19] = C1[6] * invR[10] + C1[7] * invR[11] + C1[8] * invR[12] + C1[9] * invR[13] + C1[13] * invR[14];

          K[20] = C1[10] * invR[0] + C1[11] * invR[1] + C1[12] * invR[3] + C1[13] * invR[6] + C1[14] * invR[10];
          K[21] = C1[10] * invR[1] + C1[11] * invR[2] + C1[12] * invR[4] + C1[13] * invR[7] + C1[14] * invR[11];
          K[22] = C1[10] * invR[3] + C1[11] * invR[4] + C1[12] * invR[5] + C1[13] * invR[8] + C1[14] * invR[12];
          K[23] = C1[10] * invR[6] + C1[11] * invR[7] + C1[12] * invR[8] + C1[13] * invR[9] + C1[14] * invR[13];
          K[24] = C1[10] * invR[10] + C1[11] * invR[11] + C1[12] * invR[12] + C1[13] * invR[13] + C1[14] * invR[14];

          // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
          TRACKVECTORFIT_PRECISION d[5]{X2[0] - X1[0], X2[1] - X1[1], X2[2] - X1[2], X2[3] - X1[3], X2[4] - X1[4]};
          X[0] = X1[0] + K[0] * d[0] + K[1] * d[1] + K[2] * d[2] + K[3] * d[3] + K[4] * d[4];
          X[1] = X1[1] + K[5] * d[0] + K[6] * d[1] + K[7] * d[2] + K[8] * d[3] + K[9] * d[4];
          X[2] = X1[2] + K[10] * d[0] + K[11] * d[1] + K[12] * d[2] + K[13] * d[3] + K[14] * d[4];
          X[3] = X1[3] + K[15] * d[0] + K[16] * d[1] + K[17] * d[2] + K[18] * d[3] + K[19] * d[4];
          X[4] = X1[4] + K[20] * d[0] + K[21] * d[1] + K[22] * d[2] + K[23] * d[3] + K[24] * d[4];

          // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
          C[0]  = K[0] * C2[0] + K[1] * C2[1] + K[2] * C2[3] + K[3] * C2[6] + K[4] * C2[10];
          C[1]  = K[5] * C2[0] + K[6] * C2[1] + K[7] * C2[3] + K[8] * C2[6] + K[9] * C2[10];
          C[3]  = K[10] * C2[0] + K[11] * C2[1] + K[12] * C2[3] + K[13] * C2[6] + K[14] * C2[10];
          C[6]  = K[15] * C2[0] + K[16] * C2[1] + K[17] * C2[3] + K[18] * C2[6] + K[19] * C2[10];
          C[10] = K[20] * C2[0] + K[21] * C2[1] + K[22] * C2[3] + K[23] * C2[6] + K[24] * C2[10];

          C[2]  = K[5] * C2[1] + K[6] * C2[2] + K[7] * C2[4] + K[8] * C2[7] + K[9] * C2[11];
          C[4]  = K[10] * C2[1] + K[11] * C2[2] + K[12] * C2[4] + K[13] * C2[7] + K[14] * C2[11];
          C[7]  = K[15] * C2[1] + K[16] * C2[2] + K[17] * C2[4] + K[18] * C2[7] + K[19] * C2[11];
          C[11] = K[20] * C2[1] + K[21] * C2[2] + K[22] * C2[4] + K[23] * C2[7] + K[24] * C2[11];

          C[5]  = K[10] * C2[3] + K[11] * C2[4] + K[12] * C2[5] + K[13] * C2[8] + K[14] * C2[12];
          C[8]  = K[15] * C2[3] + K[16] * C2[4] + K[17] * C2[5] + K[18] * C2[8] + K[19] * C2[12];
          C[12] = K[20] * C2[3] + K[21] * C2[4] + K[22] * C2[5] + K[23] * C2[8] + K[24] * C2[12];

          C[9]  = K[15] * C2[6] + K[16] * C2[7] + K[17] * C2[8] + K[18] * C2[9] + K[19] * C2[13];
          C[13] = K[20] * C2[6] + K[21] * C2[7] + K[22] * C2[8] + K[23] * C2[9] + K[24] * C2[13];

          C[14] = K[20] * C2[10] + K[21] * C2[11] + K[22] * C2[12] + K[23] * C2[13] + K[24] * C2[14];

          return success;
        }
      };

    } // namespace Scalar

  } // namespace TrackVectorFit

} // namespace Tr

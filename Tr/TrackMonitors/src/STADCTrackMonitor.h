/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STADCTRACKMONITOR_H
#define STADCTRACKMONITOR_H 1

// Include files

#include <map>
#include <string>

// from Gaudi
#include "TrackMonitorTupleBase.h"

// from Kernel
#include "Kernel/ISTReadoutTool.h"
#include "Kernel/STToolBase.h"

// STTELL1Event
#include "Event/STTELL1Data.h"
#include "Event/Track.h"

namespace LHCb {
  class State;
}
class DeVelo;
class DeOTDetector;
struct IHitExpectation;
struct IVeloExpectation;
struct ITrackExtraplator;
class DeSTDetector;

/** @class STADCTrackMonitor STADCTrackMonitor.h "TrackCheckers/TrackMonitor"
 *
 * Class for monitoring of hits by extrapolated tracks with NZS data
 *  @author Ch. Elsasser
 *  @date   17-6-2013
 */

class STADCTrackMonitor : public TrackMonitorTupleBase {

public:
  /** Standard construtor */
  STADCTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  void fillHistograms( const LHCb::Track& track, const std::string& type ) const;

  void findRefStates( const LHCb::Track& track, const LHCb::State*& firstMeasurementState,
                      const LHCb::State*& lastMeasurementState ) const;

  int GetADCValue( LHCb::STTELL1Data* board, unsigned int stripID, int nStripsBeetle, int summedADCValue, int nNeigh );

  LHCb::StateVector* GetTTHit( const LHCb::Track* track );
  LHCb::StateVector* GetITHit( const LHCb::Track* track );

  const DeVelo*       m_veloDet;
  const DeSTDetector* m_STDet;

  std::string m_detType;
  std::string m_layer;
  std::string m_rawDataLocCent;
  std::string m_odinLoc;

  int m_maxNeighbours;

  ISTReadoutTool* m_readoutTool;

  IVeloExpectation* m_veloExpectation;
  IHitExpectation*  m_ttExpectation;
  IHitExpectation*  m_itExpectation;
  IHitExpectation*  m_otExpectation;

  ITrackExtrapolator* m_extrapolator;

  typedef std::map<std::string, unsigned int> MultiplicityMap;
  MultiplicityMap                             m_multiplicityMap;

  std::map<std::string, unsigned int> m_layerMap;
};

#endif // STADCTRACKMONITOR_H

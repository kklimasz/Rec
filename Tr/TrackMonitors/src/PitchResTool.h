/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_PITCHRESTOOL_H
#define INCLUDE_PITCHRESTOOL_H 1

#include "Event/Track.h"
#include "TrackInterfaces/IPitchResTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "GaudiAlg/GaudiTool.h"

#include <string>
#include <utility>
#include <vector>

/** @class PitchResTool PitchResTool.h
 * tool to calculate pitch residuals for all suitable pairs of OT hits
 * on a track
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2008-06-05
 */
class PitchResTool : public extends<GaudiTool, IPitchResTool> {

public:
  /// Standard Constructor
  using extends::extends;

  /// return a vector of pairs of (layer, pitch residual) from the OT hits on the track
  std::vector<std::pair<LHCb::OTChannelID, double>> calcPitchResiduals( const LHCb::Track* track ) const override;

private:
  PublicToolHandle<ITrackExtrapolator> m_extrapolator{this, "TrackExtrapolatorName", "TrackMasterExtrapolator"};
};
#endif // INCLUDE_PITCHRESTOOL_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKMONITORTUPLEBASE_H
#define TRACKMONITORTUPLEBASE_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

#include <map>
#include <string>

// interfaces
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "Kernel/ITrajPoca.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "Event/Track.h"

/** @class TrackMonitorTupleBase TrackMonitorTupleBase.h "TrackCheckers/TrackMonitorTupleBase"
 *
 *  Base class for track monitoring: essentially a 'box' of common tools

 *  @author Ch. Elsasser
 *  @date   16-7-2013
 */

class TrackMonitorTupleBase : public GaudiTupleAlg {

public:
  /** Standard construtor */
  using GaudiTupleAlg::GaudiTupleAlg;

  /** Algorithm initialization */
  StatusCode initialize() override;

protected:
  /** Get a pointer to Magnetic field service
   *  @return field service
   */
  IMagneticFieldSvc* fieldSvc() const { return m_pIMF; }

  /** Get a pointer to the poca tool
   *  @return poca tool
   */
  const ITrajPoca* pocaTool() const { return m_poca.get(); }

  /** Get a pointer to the track extrapolator
   *  @return extrapolator
   */
  const ITrackExtrapolator* extrapolator() const { return m_extrapolator.get(); }

  /** Input track container location
   *  @return location
   */
  const std::string& inputContainer() const { return m_tracksInContainer; }

  /** Whether to split by algorithm
   *  @return splitByAlgorithm true or false
   */
  bool splitByAlgorithm() const { return m_splitByAlgorithm; }
  bool splitByType() const { return m_splitByType; }

  /** To avoid hard coding...
   *  @return all string
   */
  const std::string& all() const { return m_allString; }

protected:
  void setSplitByType( bool b ) { m_splitByType = b; }

  std::string histoDirName( const LHCb::Track& track ) const;

private:
  Gaudi::Property<std::string> m_tracksInContainer{this, "TracksInContainer",
                                                   LHCb::TrackLocation::Default}; ///< Input Tracks container location

  ToolHandle<ITrackExtrapolator> m_extrapolator{this, "Extrapolator",
                                                "TrackMasterExtrapolator"}; ///< Pointer to extrapolator
  PublicToolHandle<ITrajPoca>    m_poca{this, "TrajPoca", "TrajPoca"};      ///< Pointer to the ITrajPoca interface
  IMagneticFieldSvc*             m_pIMF = nullptr;                          ///< Pointer to the magn. field service

  Gaudi::Property<bool> m_splitByAlgorithm{this, "SplitByAlgorithm", false};
  Gaudi::Property<bool> m_splitByType{this, "SplitByType", true};
  std::string           m_allString = "ALL";
};

#endif // TRACKMONITORTUPLEBASE_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INCLUDE_HITEFFPLOTTER_H
#define INCLUDE_HITEFFPLOTTER_H 1

#include <array>
#include <bitset>
#include <string>
#include <vector>

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/Vector3DTypes.h"

#include "TrackExpectedHitsXYZTool.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

#include "Event/Track.h"

/** @class HitEffPlotter HitEffPlotter.h
 *
 * plot effective hit efficiencies for given track container as function of (x, y)
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2010-06-10
 */

class HitEffPlotter : public Gaudi::Functional::Consumer<void( const LHCb::Tracks& ),
                                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>>

{
public:
  /// Standard Constructor
  HitEffPlotter( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( const LHCb::Tracks& ) const override; ///< Algorithm event execution

private:
  PublicToolHandle<IVeloExpectation>         m_veloExpectation{this, "Velo", "VeloExpectation"};
  PublicToolHandle<IHitExpectation>          m_utExpectation{this, "UT", "UTHitExpectation"};
  PublicToolHandle<IHitExpectation>          m_ttExpectation{this, "TT", "TTHitExpectation"};
  PublicToolHandle<IHitExpectation>          m_itExpectation{this, "IT", "ITHitExpectation"};
  PublicToolHandle<IHitExpectation>          m_otExpectation{this, "OT", "OTHitExpectation"};
  PublicToolHandle<TrackExpectedHitsXYZTool> m_xyzExpectation{this, "XYZTool", "TrackExpectedHitsXYZTool"};

  Gaudi::Property<bool> m_useUT{this, "UseUT", false,
                                [=]( Property& ) {
                                  this->m_ttExpectation.setEnabled( !this->m_useUT );
                                  this->m_utExpectation.setEnabled( this->m_useUT );
                                },
                                Gaudi::Details::Property::ImmediatelyInvokeHandler{true}};

  /// plot hit efficiency numerator and denominator as function of (x, y)
  template <size_t N>
  void plot( std::string namepfx, std::string titlepfx, unsigned nxbins, const double xmin, const double xmax,
             unsigned nybins, const double ymin, const double ymax, const std::bitset<N>& expected,
             const std::bitset<N>& ontrack, const std::array<Gaudi::XYZVector, N>& points ) const;
};
#endif // INCLUDE_HITEFFPLOTTER_H

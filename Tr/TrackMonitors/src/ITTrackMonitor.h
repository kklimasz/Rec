/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ITTRACKMONITOR_H
#define ITTRACKMONITOR_H 1

// Include files

#include "Event/Track.h"
#include "TrackMonitorBase.h"

namespace LHCb {
  class LHCbID;
}

/** @class ITTrackMonitor ITTrackMonitor.h "TrackCheckers/ITTrackMonitor"
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class ITTrackMonitor : public TrackMonitorBase {

public:
  /** Standard construtor */
  ITTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  bool splitByITType() const;

  std::string ITCategory( const std::vector<LHCb::LHCbID>& ids ) const;

  void fillHistograms( const LHCb::Track& track, const std::string& type,
                       const std::vector<LHCb::LHCbID>& itIDs ) const;

  unsigned int histoBin( const LHCb::STChannelID& chan ) const;

  double ProjectedAngle() const;

  double m_refZ;
  double m_xMax;
  double m_yMax;

  unsigned int m_minNumITHits;
  bool         m_splitByITType;
  bool         m_plotsByLayer;
  bool         m_2DSummaryHist;
  bool         m_ProfileSummaryHist;

  std::string m_clusterLocation;

  bool m_plotsBySector; //< individual plots by sector
  bool m_hitsOnTrack;   //< plot only hits on tracks
};

inline bool ITTrackMonitor::splitByITType() const {
  return m_splitByITType;
  ;
}

#endif // TRACKMONITOR_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "TrackInterfaces/ITrackSelector.h"

// from Event
#include "Event/Node.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"
#include "TrackKernel/TrackFunctors.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToStream.h"

#include <map>

#include "Map.h"

using namespace LHCb;
using namespace Gaudi;

/** @class TrackMonitor TrackMonitor.h "TrackCheckers/TrackMonitor"
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackMonitorNT : public GaudiTupleAlg {

public:
  /** Standard construtor */
  TrackMonitorNT( const std::string& name, ISvcLocator* pSvcLocator );

  /** Destructor */
  virtual ~TrackMonitorNT();

  /** Algorithm execute */
  StatusCode execute() override;

  /** Algorithm initialize */
  StatusCode initialize() override;

private:
  ITrackSelector* selector( LHCb::Track::Types aType ) const {
    return m_selectors[m_splitByAlgorithm ? aType : LHCb::Track::Types::TypeUnknown];
  }

  void fillNtuple( const LHCb::Track* aTrack, const std::string& type );

private:
  std::string                                           m_tracksInContainer; ///< Input Tracks container location
  bool                                                  m_splitByAlgorithm;
  typedef std::map<LHCb::Track::Types, ITrackSelector*> Selectors;
  mutable Selectors                                     m_selectors;
  std::string                                           m_allString;
};

DECLARE_COMPONENT( TrackMonitorNT )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackMonitorNT::TrackMonitorNT( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiTupleAlg( name, pSvcLocator ), m_allString( "ALL" ) {
  declareProperty( "TracksInContainer", m_tracksInContainer = LHCb::TrackLocation::Default );
  declareProperty( "SplitByAlgorithm", m_splitByAlgorithm = false );
}

//=============================================================================
// Destructor
//=============================================================================
TrackMonitorNT::~TrackMonitorNT() {}

//=============================================================================
// Initialization. Check parameters
//=============================================================================
StatusCode TrackMonitorNT::initialize() {
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) { return sc; }

  if ( m_splitByAlgorithm ) {
    for ( const auto& map : TrackMonitorMaps::typeDescription() ) {
      // make a tool from this
      m_selectors[map.second] = tool<ITrackSelector>( "TrackSelector", map.first + "Selector" );
    }
  } else {
    m_selectors[LHCb::Track::Types::TypeUnknown] = tool<ITrackSelector>( "TrackSelector", m_allString + "Selector" );
  } // splitByType

  return StatusCode::SUCCESS;
}

//=============================================================================
// Execute
//=============================================================================
StatusCode TrackMonitorNT::execute() {
  // get the input data
  LHCb::Tracks* tracks = getIfExists<LHCb::Tracks>( m_tracksInContainer );
  if ( !tracks ) return Warning( m_tracksInContainer + " not found", StatusCode::SUCCESS, 0 );

  std::map<std::string, unsigned int> tMap;

  // # number of tracks
  plot( tracks->size(), 1, "# tracks", 0., 500., 50 );

  // histograms per track

  for ( const auto& iterT : *tracks ) {
    if ( /*selector((iterT)->type())->accept(*iterT) ==*/true ) {

      std::string type = ( m_splitByAlgorithm ? Gaudi::Utils::toString( iterT->history() ) : m_allString );
      ++tMap[type];
      fillNtuple( iterT, type );
    }
  } // iterT

  // fill counters....
  counter( "#Tracks" ) += tracks->size();
  for ( const auto& map : tMap ) counter( "#" + map.first ) += map.second;

  return StatusCode::SUCCESS;
}

void TrackMonitorNT::fillNtuple( const LHCb::Track* aTrack, const std::string& type ) {

  int                 maxVelo = 50;
  std::vector<double> Velo_res;
  std::vector<double> Velo_pull;
  std::vector<bool>   Velo_rType;
  std::vector<int>    Velo_sensor;

  int                 maxTT = 10;
  std::vector<double> TT_res;
  std::vector<double> TT_pull;
  std::vector<int>    TT_station;
  std::vector<int>    TT_layer;
  std::vector<int>    TT_region;
  std::vector<int>    TT_sector;

  int                 maxOT = 50;
  std::vector<double> OT_res;
  std::vector<double> OT_pull;
  std::vector<int>    OT_station;
  std::vector<int>    OT_layer;
  std::vector<int>    OT_quarter;
  std::vector<int>    OT_module;

  int                 maxIT = 50;
  std::vector<double> IT_res;
  std::vector<double> IT_pull;
  std::vector<int>    IT_station;
  std::vector<int>    IT_layer;
  std::vector<int>    IT_box;
  std::vector<int>    IT_sector;

  // Loop over the nodes to get the hits variables
  for ( const LHCb::Node* iNode : nodes( *aTrack ) ) {

    // Only loop on hits with measurement
    if ( iNode->hasMeasurement() ) continue;

    LHCb::LHCbID nodeID = iNode->measurement().lhcbID();

    iNode->measurement().visit(
        [&]( const LHCb::Measurement::IT& ) {
          auto theSTID = nodeID.stID();
          IT_res.push_back( iNode->unbiasedResidual() );
          IT_pull.push_back( iNode->unbiasedResidual() / iNode->errUnbiasedResidual() );
          IT_station.push_back( theSTID.station() );
          IT_layer.push_back( theSTID.layer() );
          IT_box.push_back( theSTID.detRegion() );
          IT_sector.push_back( theSTID.sector() );
        },
        [&]( const LHCb::Measurement::OT& ) {
          auto theOTID = nodeID.otID();
          OT_res.push_back( iNode->unbiasedResidual() );
          OT_pull.push_back( iNode->unbiasedResidual() / iNode->errUnbiasedResidual() );
          OT_station.push_back( theOTID.station() );
          OT_layer.push_back( theOTID.layer() );
          OT_quarter.push_back( theOTID.quarter() );
          OT_module.push_back( theOTID.module() );
        },
        [&]( const LHCb::Measurement::TT& ) {
          auto theTTID = nodeID.stID();
          TT_res.push_back( iNode->unbiasedResidual() );
          TT_pull.push_back( iNode->unbiasedResidual() / iNode->errUnbiasedResidual() );
          TT_station.push_back( theTTID.station() );
          TT_layer.push_back( theTTID.layer() );
          TT_region.push_back( theTTID.detRegion() );
          TT_sector.push_back( theTTID.sector() );
        },
        [&]( const LHCb::Measurement::VeloR& ) {
          auto theVeloID = nodeID.veloID();
          Velo_res.push_back( iNode->unbiasedResidual() );
          Velo_pull.push_back( iNode->unbiasedResidual() / iNode->errUnbiasedResidual() );
          Velo_sensor.push_back( theVeloID.sensor() );
          Velo_rType.push_back( theVeloID.isRType() );
        },
        [&]( const LHCb::Measurement::VeloPhi& ) {
          auto theVeloID = nodeID.veloID();
          Velo_res.push_back( iNode->unbiasedResidual() );
          Velo_pull.push_back( iNode->unbiasedResidual() / iNode->errUnbiasedResidual() );
          Velo_sensor.push_back( theVeloID.sensor() );
          Velo_rType.push_back( theVeloID.isRType() );
        },
        []( ... ) {} );
  }

  Tuple theTuple = nTuple( "HitResiduals/" + type + "/", "", CLID_ColumnWiseTuple );

  theTuple->farray( "Velo_res", Velo_res, "nVelo", maxVelo );
  theTuple->farray( "Velo_pull", Velo_pull, "nVelo", maxVelo );
  theTuple->farray( "Velo_sensor", Velo_sensor, "nVelo", maxVelo );
  theTuple->farray( "Velo_rType", Velo_rType, "nVelo", maxVelo );

  theTuple->farray( "TT_res", TT_res, "nTT", maxTT );
  theTuple->farray( "TT_pull", TT_pull, "nTT", maxTT );
  theTuple->farray( "TT_station", TT_station, "nTT", maxTT );
  theTuple->farray( "TT_layer", TT_layer, "nTT", maxTT );
  theTuple->farray( "TT_region", TT_region, "nTT", maxTT );
  theTuple->farray( "TT_sector", TT_sector, "nTT", maxTT );

  theTuple->farray( "OT_res", OT_res, "nOT", maxOT );
  theTuple->farray( "OT_pull", OT_pull, "nOT", maxOT );
  theTuple->farray( "OT_station", OT_station, "nOT", maxOT );
  theTuple->farray( "OT_layer", OT_layer, "nOT", maxOT );
  theTuple->farray( "OT_quarter", OT_quarter, "nOT", maxOT );
  theTuple->farray( "OT_module", OT_module, "nOT", maxOT );

  theTuple->farray( "IT_res", IT_res, "nIT", maxIT );
  theTuple->farray( "IT_pull", IT_pull, "nIT", maxIT );
  theTuple->farray( "IT_station", IT_station, "nIT", maxIT );
  theTuple->farray( "IT_layer", IT_layer, "nIT", maxIT );
  theTuple->farray( "IT_box", IT_box, "nIT", maxIT );
  theTuple->farray( "IT_sector", IT_sector, "nIT", maxIT );

  auto get_ei = []( int i ) { return [=]( const LHCb::Track& t ) { return t.info( i, 0 ); }; };
  theTuple->columns( *aTrack, std::pair{"p", []( const auto& t ) { return t.p() / Gaudi::Units::GeV; }},
                     std::pair{"probChi2", &LHCb::Track::probChi2}, std::pair{"chi2", &LHCb::Track::chi2},
                     std::pair{"ndof", &LHCb::Track::nDoF}, std::pair{"type", &LHCb::Track::type},
                     std::pair{"veloChi2", get_ei( LHCb::Track::AdditionalInfo::FitVeloChi2 )},
                     std::pair{"veloNdof", get_ei( LHCb::Track::AdditionalInfo::FitVeloNDoF )},
                     std::pair{"TChi2", get_ei( LHCb::Track::AdditionalInfo::FitTChi2 )},
                     std::pair{"TNdof", get_ei( LHCb::Track::AdditionalInfo::FitTNDoF )} );
  theTuple->columns( aTrack->firstState(), std::pair{"z", &LHCb::State::z}, std::pair{"x", &LHCb::State::x},
                     std::pair{"y", &LHCb::State::y}, std::pair{"tx", &LHCb::State::tx},
                     std::pair{"ty", &LHCb::State::ty}, std::pair{"qop", &LHCb::State::qOverP} );
  theTuple->columns(
      LHCb::HitPattern{aTrack->lhcbIDs()}, std::pair{"numVeloStations", &LHCb::HitPattern::numVeloStations},
      std::pair{"numVeloStationsOverlap", &LHCb::HitPattern::numVeloStationsOverlap},
      std::pair{"numITStationsOverlap", &LHCb::HitPattern::numITStationsOverlap},
      std::pair{"numITOTStationsOverlap", &LHCb::HitPattern::numITOTStationsOverlap},
      std::pair{"numVeloHoles", &LHCb::HitPattern::numVeloHoles}, std::pair{"numTHoles", &LHCb::HitPattern::numTHoles},
      std::pair{"numTLayers", &LHCb::HitPattern::numTLayers},
      std::pair{"numVeloStations", &LHCb::HitPattern::numVeloStations},
      std::pair{"numVeloClusters", []( const auto& hp ) { return hp.numVeloR() + hp.numVeloPhi(); }} );
  theTuple->write();
}

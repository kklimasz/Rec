/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef STCOORDINATES_H
#define STCOORDINATES_H 1

// Include files
#include "TrackMonitorBase.h"

class DeSTDetector;

/** @class STCoordinates STCoordinates.h
 *  ...
 *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-05-31
 */
class STCoordinates : public TrackMonitorBase {
public:
  /// Standard constructor
  STCoordinates( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  std::string   m_detType;
  DeSTDetector* m_tracker;
  bool          m_printsectorinfo;
  std::string   m_alignmenttag;
};
#endif // STCOORDINATES_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TTTRACKMONITOR_H
#define TTTRACKMONITOR_H 1

// Include files

#include "Event/Track.h"
#include "TrackMonitorBase.h"

namespace LHCb {
  class LHCbID;
}

/** @class TTTrackMonitor TTTrackMonitor.h "TrackCheckers/TTTrackMonitor"
 *
 * Class for TT track monitoring
 *  @author M. Needham.
 *  @date   16-1-2009
 */

class TTTrackMonitor : public TrackMonitorBase {

public:
  /** Standard construtor */
  TTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  void fillHistograms( const LHCb::Track& track, const std::vector<LHCb::LHCbID>& itIDs,
                       const std::string& type ) const;

  unsigned int histoBin( const LHCb::STChannelID& chan ) const;

  double ProjectedAngle() const;

  double m_refZ;
  double m_xMax;
  double m_yMax;

  unsigned int m_minNumTTHits;
  std::string  m_clusterLocation;

  bool m_plotsBySector; //< individual plots by sector
  bool m_hitsOnTrack;   //< plot only hits on tracks
  bool m_2DSummaryHist;
  bool m_ProfileSummaryHist;
};

#endif // TRACKMONITOR_H

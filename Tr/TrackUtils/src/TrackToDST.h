/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackToDST_H_
#define _TrackToDST_H_

/** @class TrackToDST TrackToDST.h
 *
 *  Fake a T seed from a long track
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <map>
#include <string>

#include "Event/State.h"

#include "Event/Track.h"

class TrackToDST : public GaudiAlgorithm {

public:
  // Constructors and destructor
  TrackToDST( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode execute() override;

private:
  typedef std::vector<LHCb::State::Location> SLocations;
  SLocations                                 m_veloStates;
  SLocations                                 m_longStates;
  SLocations                                 m_upstreamStates;
  SLocations                                 m_downstreamStates;
  SLocations                                 m_tStates;
  SLocations                                 m_muonStates;

  typedef std::vector<std::string> Strings;
  Strings                          m_veloStrings;
  Strings                          m_longStrings;
  Strings                          m_upstreamStrings;
  Strings                          m_downstreamStrings;
  Strings                          m_tStrings;
  Strings                          m_muonStrings;

  void cleanStates( LHCb::Track& aTrack, const SLocations& loc ) const;

  std::string m_inputLocation;
  bool        m_storeAllStates;
};

#endif

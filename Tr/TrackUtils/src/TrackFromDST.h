/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKFROMDST_H
#define TRACKFROMDST_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class TrackFromDST TrackFromDST.h
 *
 *  Algorithm to classify the tracks given as input according to
 *  their History / pattern recognition algorithms.
 *  Typically, this algorithm takes the tracks from the "best" container
 *  and remakes the original containers that were originally put together
 *  in the best container at the end of the tracking sequence in Brunel.
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-09-18
 */
class TrackFromDST : public GaudiAlgorithm {

public:
  /// Standard constructor
  TrackFromDST( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  // job options
  // -----------
  // input Track container path
  std::string m_tracksInContainer;
};
#endif // TRACKFROMDST_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the implementation of the TsaEff
// C++ code for 'LHCb Tracking package(s)'
//

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// track interfaces
#include "Event/State.h"

#include "TrackPrepareForFit.h"

using namespace LHCb;
using namespace Gaudi::Units;
using namespace Gaudi;

DECLARE_COMPONENT( TrackPrepareForFit )

TrackPrepareForFit::TrackPrepareForFit( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // constructor
  declareProperty( "inputLocation", m_inputLocation = TrackLocation::Velo );
  declareProperty( "ptVelo", m_ptVelo = 400. * MeV );
  declareProperty( "fixP", m_fixP = true );
  declareProperty( "curvValue", m_curvValue = 1.0 / ( 7.0 * GeV ) );
  declareProperty( "reverseCharge", m_reverseCharge = false );
}

StatusCode TrackPrepareForFit::execute() {

  // loop
  for ( auto track : *get<Tracks>( m_inputLocation ) ) {
    const auto& ids = track->lhcbIDs();
    auto        id  = std::find_if( begin( ids ), end( ids ), []( const LHCbID& id ) { return id.isVelo(); } );
    if ( id == end( ids ) ) { return Warning( "Setting can not set random q/p for non-velo track" ); }
    int firstStrip = id->veloID().strip();
    int charge     = ( firstStrip % 2 == 0 ? -1 : 1 );
    if ( m_reverseCharge ) charge *= -1;
    prepare( track, charge );
  }

  return StatusCode::SUCCESS;
}

void TrackPrepareForFit::prepare( Track* aTrack, const int charge ) const {

  // do what we have to do...
  State&       vState = aTrack->firstState();
  TrackVector& vec    = vState.stateVector();
  double       slope2 = std::max( vec( 2 ) * vec( 2 ) + vec( 3 ) * vec( 3 ), 1e-20 );
  double       curv   = ( m_fixP ? m_curvValue : charge * sqrt( slope2 ) / ( m_ptVelo * sqrt( 1. + slope2 ) ) );
  // set all the state a track has....
  for ( const auto& state : aTrack->states() ) {
    State* aState = const_cast<State*>( state );
    aState->setQOverP( curv );
    aState->setErrQOverP2( 1e-3 );
  }
}

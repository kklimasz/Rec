/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class VertexListFilter VertexListFilter.h
 *
 *  Algorithm to filter events in which a track list is not empty
 *
 *  @author W. Hulsbergen
 *  @date   2008
 */

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

// track interfaces
#include "Event/RecVertex.h"

class VertexListFilter final : public GaudiAlgorithm {
public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  Gaudi::Property<std::string> m_inputLocation{this, "InputLocation", LHCb::RecVertexLocation::Primary};
};

DECLARE_COMPONENT( VertexListFilter )

StatusCode VertexListFilter::execute() {
  LHCb::RecVertex::Range vertices = get<LHCb::RecVertex::Range>( m_inputLocation );
  setFilterPassed( !vertices.empty() );
  return StatusCode::SUCCESS;
}

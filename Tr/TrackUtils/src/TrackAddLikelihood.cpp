/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// track interfaces
#include "Event/Track.h"

#include "GaudiKernel/ToStream.h"
#include "TrackAddLikelihood.h"
#include "TrackInterfaces/ITrackFunctor.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackAddLikelihood )

TrackAddLikelihood::TrackAddLikelihood( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

StatusCode TrackAddLikelihood::initialize() {
  std::transform( m_types.begin(), m_types.end(), std::inserter( m_toolMap, m_toolMap.end() ), [&]( unsigned int t ) {
    Track::History type = Track::History( t );
    auto           name = Gaudi::Utils::toString( type ) + "_likTool";
    return std::make_pair( type, tool<ITrackFunctor>( m_likelihoodToolName.value(), name, this ) );
  } );
  return StatusCode::SUCCESS;
}

StatusCode TrackAddLikelihood::execute() {
  for ( const auto& track : *m_input.get() ) {
    unsigned int type = track->history();
    auto         iter = m_toolMap.find( type );
    if ( iter == m_toolMap.end() ) {
      Warning( "Likelihood not calculated: Unknown track type", StatusCode::SUCCESS ).ignore();
      track->setLikelihood( -999999. );
    } else {
      track->setLikelihood( ( *iter->second )( *track ) );
    }
  }
  return StatusCode::SUCCESS;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecVertex.h"
#include "Event/Track.h"

#include "MoveRecVertexTracks.h"

DECLARE_COMPONENT( MoveRecVertexTracks )

MoveRecVertexTracks::MoveRecVertexTracks( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  declareProperty( "VertexLocation", m_vertexLocation = LHCb::RecVertexLocation::Velo3D );
  declareProperty( "OutputLocation", m_outputLocation );
}

StatusCode MoveRecVertexTracks::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_outputLocation.empty() ) { return Error( "OutputLocation is not specified" ); }

  return sc;
}

StatusCode MoveRecVertexTracks::execute() {
  auto vertices = getIfExists<LHCb::RecVertices>( m_vertexLocation );
  if ( !vertices ) { return Error( "Container " + m_vertexLocation + " does not exist." ); }

  auto newTracks = new LHCb::Tracks{};
  put( newTracks, m_outputLocation );

  LHCb::RecVertex::TrackWithWeightVector newVertexTracks;

  for ( auto vertex : *vertices ) {
    auto nTracks = vertex->tracks().size();

    newVertexTracks.clear();
    newVertexTracks.reserve( nTracks );

    for ( size_t i = 0; i < nTracks; ++i ) {
      auto* newTrack = new LHCb::Track( *vertex->tracks()[i] );
      newTracks->insert( newTrack );
      newVertexTracks.emplace_back( newTrack, vertex->weights()[i] );
    }

    vertex->setTracksWithWeights( newVertexTracks );
  }

  return StatusCode::SUCCESS;
}

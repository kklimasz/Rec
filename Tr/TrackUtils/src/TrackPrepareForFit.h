/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackPrepareForFit_H_
#define _TrackPrepareForFit_H_

/** @class TrackPrepareForFit TrackPrepareForFit.h
 *
 *  Add p (from somewhere...) to a track...
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

class TrackPrepareForFit : public GaudiAlgorithm {

public:
  // Constructors and destructor
  TrackPrepareForFit( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;

private:
  void prepare( LHCb::Track* aTrack, const int i ) const;

  std::string m_inputLocation;
  double      m_ptVelo;
  double      m_curvValue;
  bool        m_fixP;
  bool        m_reverseCharge; ///< Reverse the VELO random charge assignment
};

#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _TrackComputeExpectedHits_H_
#define _TrackComputeExpectedHits_H_

/** @class TrackComputeExpectedHits TrackComputeExpectedHits.h
 *
 *
 *  @author S. Hansmann-Menzemer
 *  @date   20.07.2009
 */

#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

struct IHitExpectation;
struct IVeloExpectation;

class TrackComputeExpectedHits final : public GaudiAlgorithm {

public:
  // Constructors and destructor
  TrackComputeExpectedHits( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode execute() override;

private:
  std::string m_inputLocation;

  IVeloExpectation* m_veloExpectation = nullptr;
  IHitExpectation*  m_ttExpectation   = nullptr;
  IHitExpectation*  m_itExpectation   = nullptr;
  IHitExpectation*  m_otExpectation   = nullptr;
};

#endif

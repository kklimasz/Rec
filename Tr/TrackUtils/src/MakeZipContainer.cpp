/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/TrackSkin.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "SOAExtensions/ZipContainer.h"
#include "SOAExtensions/ZipSelection.h"
#include <vector>

template <typename I, typename O>
struct MakeZipContainer final
    : public Gaudi::Functional::MultiTransformer<std::tuple<O, Zipping::ExportedSelection<>>( const I& )> {
  using KeyValue =
      typename Gaudi::Functional::MultiTransformer<std::tuple<O, Zipping::ExportedSelection<>>( const I& )>::KeyValue;

  MakeZipContainer( const std::string& name, ISvcLocator* pSvcLocator )
      : Gaudi::Functional::MultiTransformer<std::tuple<O, Zipping::ExportedSelection<>>( const I& )>(
            name, pSvcLocator, KeyValue{"Input", ""}, {KeyValue{"OutputData", ""}, KeyValue{"OutputSelection", ""}} ) {}

  std::tuple<O, Zipping::ExportedSelection<>> operator()( const I& in ) const override {
    m_inputObjects += in.size();
    O    retval( SOA::make_soaview<LHCb::Event::v2::TrackSkin>( in ) );
    auto sel = Zipping::ExportedSelection<>{retval, Zipping::details::alwaysTrue};
    return {std::move( retval ), std::move( sel )};
  }

  mutable Gaudi::Accumulators::Counter<> m_inputObjects{this, "# input objects"};
};

using MakeZipContainer_Trackv2 =
    MakeZipContainer<std::vector<LHCb::Event::v2::Track>,
                     Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackSkin>>>;

DECLARE_COMPONENT_WITH_ID( MakeZipContainer_Trackv2, "MakeZipContainer__Track_v2" )

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTERMATCHTRACKS_H
#define FILTERMATCHTRACKS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class FilterMatchTracks FilterMatchTracks.h
 *  Fileter Match tracks identical to Forward tracks
 *
 *  @author Olivier Callot
 *  @date   2010-06-14
 */
class FilterMatchTracks : public GaudiAlgorithm {
public:
  /// Standard constructor
  FilterMatchTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  bool m_filter;
};
#endif // FILTERMATCHTRACKS_H

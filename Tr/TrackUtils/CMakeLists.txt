###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: TrackUtils
################################################################################
gaudi_subdir(TrackUtils v1r65)

gaudi_depends_on_subdirs(Det/CaloDet
                         Det/OTDet
                         Det/STDet
                         Event/HltEvent
                         Event/LinkerEvent
                         Event/TrackEvent
                         GaudiAlg
                         Kernel/HltInterfaces
                         Kernel/LHCbMath
                         Kernel/PartProp
                         Phys/SelKernel
                         Tf/TfKernel
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel
                         Tr/TrackVectorFit)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TrackUtils
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces Tf/TfKernel
                 LINK_LIBRARIES CaloDetLib OTDetLib STDetLib HltEvent LinkerEvent TrackEvent GaudiAlgLib HltInterfaces LHCbMathLib PartPropLib TrackFitEvent TrackKernel)

gaudi_add_unit_test(TestZipInfrastructure
                    tests/src/TestZipInfrastructure.cpp
                    LINK_LIBRARIES LHCbMathLib
                    TYPE Boost)
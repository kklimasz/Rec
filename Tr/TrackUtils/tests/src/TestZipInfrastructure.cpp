/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Make sure these are turned on for these tests, even if they are disabled
// elsewhere in the build
#define ZIPPING_SEMANTIC_CHECKS

#include "Event/PrFittedForwardTracks.h"
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrZip.h"
#include "Event/RecVertex_v2.h"
#include "Event/StateParameters.h"
#include "GaudiKernel/SerializeSTL.h"
#include "SelKernel/IterableVertexRelations.h"
#include "SelKernel/VertexRelation.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestZipInfrastructure
#include <boost/test/unit_test.hpp>

#include <iostream>
#include <random>

using Tracks   = LHCb::Pr::Fitted::Forward::Tracks;
using Vertices = std::vector<LHCb::Event::v2::RecVertex>;

Tracks make_tracks() {
  using dType = SIMDWrapper::scalar::types;
  using F     = dType::float_v;
  using I     = dType::int_v;
  std::mt19937                    gen( 42 ); // Random engine with fixed seed
  std::normal_distribution<float> xy_dist{0.f, 0.1f}, z_dist{0.f, 1.f}, txy_dist{0.f, 5e-3f};
  auto constexpr ntracks = 20;
  Tracks tracks{nullptr};
  for ( auto i = 0; i < ntracks; ++i ) {
    F       x{xy_dist( gen )}, y{xy_dist( gen )}, z{z_dist( gen )}, tx{txy_dist( gen )}, ty{txy_dist( gen )};
    Vec3<F> beam_pos{x, y, z}, beam_dir{tx, ty, 1.f},
        covX{1e-5f /* x_err**2 */, 0.f /* cov(x, tx) */, 1e-8 /* tx_err**2 */},
        covY{1e-5f /* y_err**2 */, 0.f /* cov(y, ty) */, 1e-8 /* ty_err**2 */};
    tracks.store_trackFT( i, I{0} );     // Ancestor index.
    tracks.store_QoP( i, F{1.f / 5e4} ); // q/p for q = +1, p = 50 GeV
    tracks.store_beamStatePos( i, beam_pos );
    tracks.store_beamStateDir( i, beam_dir );
    tracks.store_chi2( i, F{1.f} );    // chi2/dof
    tracks.store_chi2nDof( i, I{42} ); // chi2 dof
    tracks.store_covX( i, covX );
    tracks.store_covY( i, covY );
    ++tracks.size();
  }
  return tracks;
}

auto make_covmatrix( double diag = 1e-2 ) {
  Gaudi::SymMatrix3x3 cov;
  cov( 0, 0 ) = cov( 1, 1 ) = cov( 2, 2 ) = diag;
  return cov;
}

Vertices make_vertices() {
  Vertices vertices;
  auto constexpr nvertices = 3;
  std::mt19937                    gen( 24 ); // Random engine with fixed seed
  std::normal_distribution<float> xy_dist{0.f, 0.05f}, z_dist{0.f, 1.f};
  for ( auto i = 0; i < nvertices; ++i ) {
    auto x = xy_dist( gen ), y = xy_dist( gen ), z = z_dist( gen );
    vertices.emplace_back( Gaudi::XYZPoint{x, y, z}, make_covmatrix(), LHCb::Event::v2::Track::Chi2PerDoF{1.f, 42} );
  }
  return vertices;
}

BOOST_AUTO_TEST_CASE( test_dummy_tracks ) {
  auto tracks          = make_tracks();
  auto iterable_tracks = LHCb::Pr::make_zip( tracks );
  for ( auto const track_chunk : iterable_tracks ) { popcount( track_chunk.loop_mask() ); }
}

auto make_relations( Tracks const& tracks, Vertices const& vertices = make_vertices() ) {
  auto iterable_tracks = LHCb::Pr::make_zip( tracks );
  return Sel::calculateBestVertices( iterable_tracks, vertices );
}

BOOST_AUTO_TEST_CASE( test_making_vertex_relations ) { make_relations( make_tracks() ); }

BOOST_AUTO_TEST_CASE( test_zipping_relations ) {
  auto tracks    = make_tracks();
  auto relations = make_relations( tracks );
  auto zipped    = LHCb::Pr::make_zip( tracks, relations );
  for ( auto const& zipped_chunk : zipped ) {
    zipped_chunk.bestPV();
    zipped_chunk.closestToBeamState();
  }
}

BOOST_AUTO_TEST_CASE( test_using_zipped_relations ) {
  auto tracks    = make_tracks();
  auto vertices  = make_vertices();
  auto relations = make_relations( tracks, vertices );
  auto iterable  = LHCb::Pr::make_zip( tracks );
  auto zipped    = LHCb::Pr::make_zip( tracks, relations );
  // For the scalar case when masks are bools
  using Sel::Utils::all;
  for ( auto const& chunk : zipped ) {
    // This should use the zipped relations
    auto bestPVs = Sel::getBestPVRel( chunk, vertices );
    // This should calculate the same thing without using the zipped result
    auto calcrel = Sel::calculateBestVertex( chunk, vertices );
    // Check they're either the same or the elements are out of range
    BOOST_CHECK( all( ( bestPVs == calcrel ) || !chunk.loop_mask() ) );
  }
  // For the first chunk, explicitly check that we get the same results whether
  // or not the containers are zipped
  BOOST_CHECK( all( Sel::getBestPVRel( zipped[0], vertices ) == Sel::getBestPVRel( iterable[0], vertices ) ) );
}

BOOST_AUTO_TEST_CASE( filter_tracks_using_zip ) {
  auto tracks    = make_tracks();
  auto relations = make_relations( tracks );
  auto zipped    = LHCb::Pr::make_zip( tracks, relations );
  using dType    = decltype( zipped )::default_simd_t;
  // Produce some filtered tracks using information from both parts of the zip
  Tracks output{nullptr};
  for ( auto const& chunk : zipped ) {
    auto loop_mask = chunk.loop_mask();
    auto filt_mask = ( chunk.pt() > 400.f ) && ( chunk.bestPV().ipchi2() > 1.f );
    output.copy_back<dType>( tracks, chunk.offset(), loop_mask && filt_mask );
  }
}

BOOST_AUTO_TEST_CASE( new_struct_from_zip ) {
  constexpr bool print{false};
  auto           tracks    = make_tracks();
  auto           relations = make_relations( tracks );

  // Make an iterable (non-owning) zip
  auto zipped = LHCb::Pr::make_zip( tracks, relations );

  // Check we can iterate over it, and manually check the retention of the cut
  // that we're about to apply
  std::size_t passing_cut{0};
  for ( auto const& chunk : zipped ) {
    auto pt     = chunk.pt();
    auto ipchi2 = chunk.bestPV().ipchi2();
    auto index  = chunk.bestPV().index();
    passing_cut += popcount( ( pt > 400.f ) && ( ipchi2 > 1.f ) );
    if ( print ) { std::cout << "pt " << pt << " ipchi2 " << ipchi2 << " index " << index << std::endl; }
  }

  // Try and make a new structure containing the fields from both 'tracks' and
  // 'relations', applying some selection
  auto new_data =
      zipped.filter( []( auto const& chunk ) { return ( chunk.pt() > 400.f ) && ( chunk.bestPV().ipchi2() > 1.f ); } );

  // Check we retained the right number
  BOOST_CHECK( passing_cut == new_data.size() );

  // Make a new non-owning iterable view into this new structure
  auto new_iterable = LHCb::Pr::make_zip( new_data );

  // The handling of the merged data type returned by `filter` should be such
  // that the iterable version of it is the same type as the original zip.
  static_assert( std::is_same_v<decltype( new_iterable ), decltype( zipped )> );

  // Check we can form a loop over this one too [this is maybe redundant...]
  for ( auto const& chunk : new_iterable ) {
    auto pt     = chunk.pt();
    auto ipchi2 = chunk.bestPV().ipchi2();
    auto index  = chunk.bestPV().index();
    auto mask   = chunk.loop_mask();
    if ( print ) {
      std::cout << "pt " << pt << " ipchi2 " << ipchi2 << " index " << index << " mask " << mask << std::endl;
    }
  }

  // Try printing with a scalar loop too
  for ( auto const& chunk : new_iterable.with<SIMDWrapper::InstructionSet::Scalar>() ) {
    auto pt     = chunk.pt();
    auto ipchi2 = chunk.bestPV().ipchi2();
    auto index  = chunk.bestPV().index();
    if ( print ) { std::cout << "pt " << pt << " ipchi2 " << ipchi2 << " index " << index << std::endl; }
  }

  // Finally check we can use an 'unwrapped' scalar loop
  for ( auto const& chunk : new_iterable.unwrap() ) {
    auto pt = chunk.pt();
    // 'unwrapping' means that this should be a plain data type
    static_assert( std::is_same_v<decltype( pt ), float> );
  }
}

BOOST_AUTO_TEST_CASE( test_semantic_check ) {
  // Make two incompatible zip containers
  auto tracks1 = make_tracks();
  auto tracks2 = make_tracks();

  // The two track containers have different zip identifiers, so trying to zip
  // them together should throw. No compile error due to duplicate types as no
  // proxy or merged objects are created.
  BOOST_CHECK_THROW( LHCb::Pr::make_zip( tracks1, tracks2 ), GaudiException );

  // This would not compile (zipped = make_zip() return value above), as the
  // proxy type returned would inherit twice from the same base class. For the
  // same reason we cannot call .filter() on this zip.
  // zipped[0]; // This would not compile
}

BOOST_AUTO_TEST_CASE( test_size_check ) {
  // Make two compatible containers
  auto tracks    = make_tracks();
  auto relations = make_relations( tracks );

  // Change the size of one of them, so zipping them becomes invalid
  --tracks.size();

  // Check that we actually get an error
  BOOST_CHECK_THROW( LHCb::Pr::make_zip( tracks, relations ), GaudiException );
}
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PATLONGLIVEDPARAMS_H
#define PATLONGLIVEDPARAMS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Point3DTypes.h"

#include "FitTool.h"
#include "FwdParameters.h"

/** @class PatLongLivedParams PatLongLivedParams.h
 *  Parameterize the KShort/long lived tracks in PatLongLivedTracking
 *
 *  Algorithm to calculate the different parameters in PatLongLivedTracking, similar to KsFitParams it tries to fit
 * polynomial dependencies. Uses purely MC information from MCHits ie this will only run when they are available (XDIGI,
 * XDST)
 *  - NTupleName: Name of the output nTuple
 *  - ZTT1: z-position of reference point in TT
 *  - ZRef: z-position of reference plane in T-stations
 *  - zMagnetParams:  Initial parameters for calculation of z of 'kink-position' of magnet
 *  - momentumParams:  Initial paramters to calculate the momentum.
 *  - curvatureParams: Initial parameter to parametrize the quadratic correction in the TT track model
 *  - yParams: Initial parameter for the correction of the y slope
 *  - yParams2: Initial parameter for the correction of the y position.
 *  - seedLocation: Location of the seed track, to determine the resolution.
 *  - determineResolution: Determines the resolution of the parametrisations. This completely skips the part where the
 * parameters are derived, so correct parameters must be put in.
 *
 *  Note that only as many paramters are used for the calculation as are given as input, although internally more might
 * be defined.
 *
 *  For determining the parameters (but not the resolutions), nothing else from the Brunel sequence is needed when
 * running this code, a possible way to run it would be:
 *
 *  @code
 *  def doIt():
 *      from Configurables import PatLongLivedParams
 *      GaudiSequencer("BrunelSequencer").Members =  [ PatLongLivedParams("PatLongLivedParams", zMagnetParams = [
 * 0,0,0,0,0,0,0 ], momentumParams = [ 0,0,0 ], yParams = [ 0 ], yParams2 = [ 0 ], curvatureParams = [ 0 ]) ]
 *
 *  appendPostConfigAction( doIt )
 *  @endcode
 *
 *  @author Olivier Callot
 *  @author Michel De Cian
 *  @date   2016-08-29
 */

class PatLongLivedParams : public GaudiTupleAlg {
public:
  /// Standard constructor
  PatLongLivedParams( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~PatLongLivedParams(); ///< Destructor

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  void resolution();

  FitTool*    m_fitTool;
  std::string m_tupleName;
  double      m_zTT1;
  double      m_zRef;

  std::vector<double> m_zMagParams;
  std::vector<double> m_momParams;
  std::vector<double> m_curvatureParams;
  std::vector<double> m_yParams;
  std::vector<double> m_yParams2;

  FwdParameters m_zMagPar;
  FwdParameters m_momPar;
  FwdParameters m_curvature;
  FwdParameters m_yPar;
  FwdParameters m_yPar2;

  int m_nEvent;
  int m_nTrack;

  std::string m_inputLocationSeed;
  bool        m_resolution;
};
#endif // PATLONGLIVEDFITPARAMS_H

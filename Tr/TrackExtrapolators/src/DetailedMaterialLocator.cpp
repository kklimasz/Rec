/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/SystemOfUnits.h"

#include "DetDesc/IDetectorElement.h"
#include "DetDesc/Material.h"

#include "DetailedMaterialLocator.h"

DECLARE_COMPONENT( DetailedMaterialLocator )

StatusCode DetailedMaterialLocator::initialize() {
  StatusCode sc = MaterialLocatorBase::initialize();
  if ( !sc.isSuccess() ) return Error( "Failed to initialize base class", sc );

  // if it is zero, the transport services uses the 'standard' geometry
  auto mainvolume = !m_geometrypath.empty() ? getDet<IDetectorElement>( m_geometrypath ) : nullptr;
  m_geometry      = mainvolume ? mainvolume->geometry() : nullptr;

  return sc;
}

size_t DetailedMaterialLocator::intersect_r( const Gaudi::XYZPoint& start, const Gaudi::XYZVector& vect,
                                             ILVolume::Intersections& intersepts, std::any& accelCache ) const {
  // check if transport is within LHCb
  constexpr double m_25m = 25 * Gaudi::Units::m;
  size_t           rc    = 0;
  intersepts.clear();

  if ( std::abs( start.x() ) > m_25m || std::abs( start.y() ) > m_25m || std::abs( start.z() ) > m_25m ||
       std::abs( start.x() + vect.x() ) > m_25m || std::abs( start.y() + vect.y() ) > m_25m ||
       std::abs( start.z() + vect.z() ) > m_25m ) {
    Warning( "No transport possible since destination is outside LHCb " ).ignore();

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "No transport between z= " << start.z() << " and " << start.z() + vect.z()
              << ", since it reaches outside LHCb"
              << "start = " << start << " vect= " << vect << endmsg;
  } else {
    try {
      static const std::string chronotag = "DetailedMaterialLocator";
      chronoSvc()->chronoStart( chronotag );
      const double mintick = 0;
      const double maxtick = 1;
      rc = m_tSvc->intersections_r( start, vect, mintick, maxtick, intersepts, accelCache, m_minRadThickness,
                                    m_geometry );
      chronoSvc()->chronoStop( chronotag );
    } catch ( const GaudiException& exception ) {
      error() << "caught transportservice exception " << exception << std::endl
              << "propagating pos/vec: " << start << " / " << vect << endmsg;
      throw exception;
    }
  }
  return rc;
}

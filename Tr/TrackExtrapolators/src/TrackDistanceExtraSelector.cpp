/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtrapolator.h"

// local
#include "TrackDistanceExtraSelector.h"

DECLARE_COMPONENT( TrackDistanceExtraSelector )

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackDistanceExtraSelector::initialize() {
  // initialize
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );

  // request a short distance magnetic field extrapolator
  m_shortDistanceExtrapolator =
      tool<ITrackExtrapolator>( m_shortDistanceExtrapolatorType, "ShortDistanceExtrapolator", this );
  // request extrapolator for going short distances in magnetic field
  m_longDistanceExtrapolator =
      tool<ITrackExtrapolator>( m_longDistanceExtrapolatorType, "LongDistanceExtrapolator", this );

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    debug() << "Short distance extrapolator: " << m_shortDistanceExtrapolator->type() << endmsg;
    debug() << "Long distance extrapolator: " << m_longDistanceExtrapolator->type() << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//
//=============================================================================
const ITrackExtrapolator* TrackDistanceExtraSelector::select( double zStart, double zEnd ) const {
  return std::abs( zEnd - zStart ) < m_shortDist ? m_shortDistanceExtrapolator : m_longDistanceExtrapolator;
}

//=============================================================================

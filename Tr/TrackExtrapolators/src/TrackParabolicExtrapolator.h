/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKPARABOLICEXTRAPOLATOR_H
#define TRACKPARABOLICEXTRAPOLATOR_H 1

// Include files
#include "TrackFieldExtrapolatorBase.h"
#include <string>

// Forward declaration
class IMagneticFieldSvc;

/** @class TrackParabolicExtrapolator TrackParabolicExtrapolator.h \
 *         "TrackParabolicExtrapolator.h"
 *
 *  A TrackParabolicExtrapolator is a ITrackExtrapolator that does a transport
 *  using a parabolic expansion of the trajectory. It doesn't take into
 *  account Multiple Scattering.
 *
 *  @author Edwin Bos (added extrapolation methods)
 *  @date   05/07/2005
 *  @author Jose A. Hernando (13-03-2005)
 *  @author Matt Needham
 *  @date   22-04-2000
 */

class TrackParabolicExtrapolator : public TrackFieldExtrapolatorBase {

public:
  /// Constructor
  using TrackFieldExtrapolatorBase::propagate;
  using TrackFieldExtrapolatorBase::TrackFieldExtrapolatorBase;

  /// Propagate a state vector from zOld to zNew
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( Gaudi::TrackVector& stateVec, double zOld, double zNew, Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to the closest position to the specified point
  StatusCode propagate( LHCb::State& state, const Gaudi::XYZPoint& point,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

private:
  /// update transport matrix
  void updateTransportMatrix( const double dz, Gaudi::TrackVector& stateVec, Gaudi::TrackMatrix& transMat,
                              const Gaudi::XYZVector& B, double ax, double ay ) const;
};

#endif // TRACKPARABOLICEXTRAPOLATOR_H

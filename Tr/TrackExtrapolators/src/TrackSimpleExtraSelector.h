/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TrackSimpleExtraSelector_H
#define TrackSimpleExtraSelector_H 1

// Include files
#include "GaudiAlg/GaudiTool.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtraSelector.h"

#include <string>

/** @class TrackSimpleExtraSelector "TrackSimpleExtraSelector.h"
 *
 *  Simple selection of one extrapolator
 *
 */

class TrackSimpleExtraSelector : public extends<GaudiTool, ITrackExtraSelector> {

public:
  using base_class::base_class;

  StatusCode initialize() override;

  const ITrackExtrapolator* select( double zStart, double zEnd ) const override;

private:
  const ITrackExtrapolator*    m_extrapolator = nullptr;
  Gaudi::Property<std::string> m_extraName{this, "ExtrapolatorName", "TrackParabolicExtrapolator"};
};

#endif // TrackSimpleExtraSelector_H

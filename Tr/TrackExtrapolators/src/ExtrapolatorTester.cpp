/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolHandle.h"

#include "Event/StateVector.h"
#include "TrackFieldExtrapolatorBase.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

class ExtrapolatorTester : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;

  /** Algorithm execute */
  virtual StatusCode execute() override;

  /** Algorithm initialize */
  virtual StatusCode initialize() override;

private:
  // Gaudi::Property<std::string> m_refextrap{this,"ReferenceExtrapolator","TrackRungeKuttaExtrapolator"};
  ToolHandle<ITrackExtrapolator>            m_refextrap{this, "ReferenceExtrapolator", "TrackRungeKuttaExtrapolator"};
  Gaudi::Property<std::vector<std::string>> m_extraps{this, "Extrapolators"};
};

DECLARE_COMPONENT( ExtrapolatorTester )

// static __inline__ unsigned long GetCC( void ) {
//   unsigned a, d;
//   asm volatile( "rdtsc" : "=a"( a ), "=d"( d ) );
//   return ( (unsigned long)a ) | ( ( (unsigned long)d ) << 32 );
// }

StatusCode ExtrapolatorTester::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  // ToolHandle<ITrackExtrapolator> m_refextrap{this,"TrackRungeKuttaExtrapolator"};

  std::vector<const ITrackExtrapolator*> extraps;
  for ( const auto& m : m_extraps ) {
    // Tuple theTuple = nTuple( m , m , CLID_ColumnWiseTuple );
    ITrackExtrapolator* extrap = tool<ITrackExtrapolator>( m );
    size_t              ipos   = m.find_last_of( "/" );
    if ( ipos >= m.size() ) ipos = 0;
    extraps.push_back( extrap );
  }

  const double z1 = 770.;
  const double z2 = 7500.;

  const double qopmax = +0.0004; // 2.5 GeV
  const double qopmin = -0.0004;
  const double txmax  = +0.3;
  const double txmin  = -0.3;
  const double tymax  = +0.25;
  const double tymin  = -0.25;
  const int    nbins  = 11;

  const double dqop = ( qopmax - qopmin ) / ( nbins - 1 );
  const double dtx  = ( txmax - txmin ) / ( nbins - 1 );
  const double dty  = ( tymax - tymin ) / ( nbins - 1 );

  for ( int iqop = 0; iqop < nbins; ++iqop ) // grid in tx, ty, qop
    for ( int itx = 0; itx < nbins; ++itx )
      for ( int ity = 0; ity < nbins; ++ity ) {
        const double      qop = qopmin + iqop * dqop;
        const double      tx  = txmin + itx * dtx;
        const double      ty  = tymin + ity * dty;
        LHCb::StateVector origin;
        origin.setZ( z1 );
        origin.setY( 0 );
        origin.setX( 0 );
        origin.setQOverP( qop );
        origin.setTx( tx );
        origin.setTy( ty );

        // extrapolate the reference
        LHCb::StateVector  reftarget = origin;
        Gaudi::TrackMatrix refjacobian;
        m_refextrap->propagate( reftarget, z2, &refjacobian );

        // now do the same for the others
        for ( auto& extrap : extraps ) {

          LHCb::StateVector  target = origin;
          Gaudi::TrackMatrix jacobian;
          StatusCode         sc   = extrap->propagate( target, z2, &jacobian );
          LHCb::StateVector  back = target;
          extrap->propagate( back, z1 );
        }
      }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================

StatusCode ExtrapolatorTester::execute() { return StatusCode::SUCCESS; }

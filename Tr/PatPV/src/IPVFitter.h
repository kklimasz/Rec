/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IPVFITTER_H
#define IPVFITTER_H 1

// from STL
#include <vector>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/Point3DTypes.h"

namespace LHCb {
  class RecVertex;
}

struct IPVFitter : extend_interfaces<IAlgTool> {
  DeclareInterfaceID( IPVFitter, 2, 0 );
  virtual StatusCode fitVertex( const Gaudi::XYZPoint& seedPoint, const std::vector<const LHCb::Track*>& tracks,
                                LHCb::RecVertex& vtx, std::vector<const LHCb::Track*>& tracks2remove ) const = 0;
};
#endif // IPVFITTER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKCHECKERS_TRACKVELOTTCHECKER_H
#define TRACKCHECKERS_TRACKVELOTTCHECKER_H 1

#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "STDet/DeSTDetector.h"

#include "GaudiAlg/GaudiTupleAlg.h"

/** @class TrackVeloTTChecker TrackVeloTTChecker.h
 *
 *
 *  @author Eduardo Rodrigues (adaptations to new Track Event Model)
 *  @date   2005-11-04
 *  @author :  Yuehong Xie
 *  @date   :  15/10/2002
 *
 */

using namespace LHCb;

class TrackVeloTTChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  TrackVeloTTChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

  MCParticle* VeloTrackMCTruth( LHCb::Track* track );

private:
  int m_nEvents; ///< Number of events processed

  DeSTDetector* m_TTDet;
};
#endif // TRACKCHECKERS_TRACKVELOTTCHECKER_H

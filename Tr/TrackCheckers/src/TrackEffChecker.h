/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKEFFCHECKER_H
#define TRACKEFFCHECKER_H 1

#include "TrackCheckerBase.h"

#include "GaudiAlg/Consumer.h"

#include "Event/MCParticle.h"
#include "Event/Track.h"

/** @class TrackEffChecker TrackEffChecker.h
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackEffChecker : public Gaudi::Functional::Consumer<void( const LHCb::Tracks&, const LHCb::MCParticles&,
                                                                 const LHCb::LinksByKey&, const LHCb::LinksByKey& ),
                                                           Gaudi::Functional::Traits::BaseClass_t<TrackCheckerBase>> {

public:
  /** Standard construtor */
  TrackEffChecker( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm execute */
  void operator()( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey&,
                   const LHCb::LinksByKey& ) const override;

  /** Algorithm finalize */
  StatusCode finalize() override;

private:
  Gaudi::Property<bool> m_requireLong{this, "RequireLongTrack", false};

  void ghostInfo( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey&, unsigned int ) const;

  void effInfo( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey&,
                const std::vector<std::vector<int>>& ) const;

  void plots( const std::string& type, const LHCb::Track* track ) const;

  void plots( const std::string& type, const LHCb::MCParticle* part ) const;

  double weightedMeasurementSum( const LHCb::Track* aTrack ) const;

  mutable Gaudi::Accumulators::SummingCounter<> m_nTrackCounter{this, "nTrack"};
  mutable Gaudi::Accumulators::SummingCounter<> m_nGhostCounter{this, "nGhost"};
};

#endif // TRACKEFFCHECKER_H

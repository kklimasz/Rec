/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKCLONECHECKER_H
#define TRACKCLONECHECKER_H 1

#include "TrackCheckerBase.h"

#include "GaudiAlg/Consumer.h"

#include <map>

/** @class TrackCloneChecker TrackCloneChecker.h
 *
 *  Produce some simple plots for the Clone linker information
 *
 *  @author Chris Jones
 *  @date   2007-09-13
 */

class TrackCloneChecker : public Gaudi::Functional::Consumer<void( const LHCb::Tracks&, const LHCb::MCParticles&,
                                                                   const LHCb::LinksByKey&, const LHCb::LinksByKey& ),
                                                             Gaudi::Functional::Traits::BaseClass_t<TrackCheckerBase>> {

public:
  /// Standard constructor
  TrackCloneChecker( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm execute */
  void operator()( const LHCb::Tracks&, const LHCb::MCParticles&, const LHCb::LinksByKey&,
                   const LHCb::LinksByKey& ) const override;

  StatusCode finalize() override; ///< Algorithm finalize

private:
  /** @class TrackTally TrackCloneChecker.h
   *
   *  Counts track information for clones
   *
   *  @author Chris Jones
   *  @date   2007-09-13
   */
  struct TrackTally {
    unsigned long totalClones{0};
    unsigned long totalNonClones{0};
    unsigned long totalGhosts{0};
    unsigned long rejectedClones{0};
    unsigned long rejectedNonClones{0};
    unsigned long rejectedGhosts{0};
    /// Map for one tally object per track history type
    typedef std::map<LHCb::Track::History, TrackTally> Map;
  };

private:
  /// Get efficiency
  inline std::pair<double, double> getEff1( const double top, const double bot ) const {
    return std::pair<double, double>( ( bot > 0 ? 100.0 * top / bot : 0 ),
                                      ( bot > 0 ? 100.0 * sqrt( ( top / bot ) * ( 1. - top / bot ) / bot ) : 0 ) );
  }

  /// Get efficiency
  inline std::pair<double, double> getEff2( const double top, const double bot ) const {
    return std::pair<double, double>( ( bot > 0 ? top / bot : 0 ), ( bot > 0 ? sqrt( top ) / bot : 0 ) );
  }

private:
  /// Summary map XXXX This is not thread safe. Should be rewritten using Gaudi counters
  mutable TrackTally::Map m_trackMap;

  /// KL distance cut
  Gaudi::Property<double> m_klCut{this, "CloneCut", 5000};

  /// Event count
  mutable Gaudi::Accumulators::Counter<> m_nEvtsCounter{this, "Nb events"};
};

#endif // TRACKCLONECHECKER_H

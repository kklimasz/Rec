/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKOCCUPCHECKER_H
#define TRACKOCCUPCHECKER_H 1

// Include files
#include "TrackCheckerBase.h"

// for occupancy studies
#include "PatKernel/PatForwardHit.h"
#include "TfKernel/TStationHitManager.h"

#include "TfKernel/DefaultVeloPhiHitManager.h"
#include "TfKernel/DefaultVeloRHitManager.h"
#include "TfKernel/RegionID.h"

// detector stuff
#include "OTDet/DeOTDetector.h"
#include "STDet/DeSTDetector.h"
#include "VeloDet/DeVelo.h"

/** @class TrackOccupChecker TrackOccupChecker.h
 *
 * Class for occupancy measurements
 *  @author K. Holubyev.
 *  @date   5-10-2009
 */

class TrackOccupChecker : public TrackCheckerBase {

public:
  /** Standard constructor */
  TrackOccupChecker( const std::string& name, ISvcLocator* pSvcLocator );

  /** Destructor */
  virtual ~TrackOccupChecker();

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

  /** Algorithm finalize */
  StatusCode finalize() override;

private:
  void occupInfo();

  typedef Tf::TStationHitManager<PatForwardHit>::HitRange HitRange;
  Tf::TStationHitManager<PatForwardHit>*                  m_tHitManager;
  Tf::DefaultVeloRHitManager*                             m_rHitManager;
  Tf::DefaultVeloPhiHitManager*                           m_phiHitManager;

  static constexpr unsigned int m_nSta   = Tf::RegionID::OTIndex::kNStations;
  static constexpr unsigned int m_nLay   = Tf::RegionID::OTIndex::kNLayers;
  static constexpr unsigned int m_nRegOT = Tf::RegionID::OTIndex::kNRegions;
  static constexpr unsigned int m_nRegIT = Tf::RegionID::ITIndex::kNRegions;
  static constexpr unsigned int m_nReg   = m_nRegOT + m_nRegIT;

  // Detectors
  DeVelo*       m_Velo;
  DeSTDetector* m_IT;
  DeOTDetector* m_OT;

  unsigned int m_ITChannels;
  unsigned int m_OTChannels;

  inline bool isRegionOT( const unsigned reg ) const { return ( reg == 0 || reg == 1 ); }

  /// true if region reg is IT region
  inline bool isRegionIT( const unsigned reg ) const { return reg >= Tf::RegionID::OTIndex::kNRegions; }

  /// true if region reg is IT horizontal
  inline bool isRegionIThor( const unsigned reg ) const { return ( reg == 2 || reg == 3 ); }
  /// true if region reg is IT region
  inline bool isRegionITver( const unsigned reg ) const { return ( reg == 4 || reg == 5 ); }
};

#endif // TRACKOCCUPCHECKER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONNNETREC_H
#define MUONNNETREC_H 1

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonTrackRec.h" // Interface
#include "MuonInterfaces/MuonNeuron.h"
#include "MuonInterfaces/MuonTrack.h"
#include <string>
#include <vector>
class DeMuonDetector;
class IMuonHitDecode;
struct IMuonPadRec;
struct IMuonClusterRec;
class ISequencerTimerTool;
struct IMuonTrackMomRec;

/** @class MuonNNetRec MuonNNetRec.h
 *
 *
 *  @author Giovanni Passaleva
 *  @date   2008-04-11
 */
class MuonNNetRec : public GaudiTool, virtual public IMuonTrackRec, virtual public IIncidentListener {
public:
  /// Standard constructor
  MuonNNetRec( const std::string& type, const std::string& name, const IInterface* parent );

  ~MuonNNetRec() override; ///< Destructor
  // from GaudiTool
  StatusCode initialize() override;
  // from IIncidentListener
  void handle( const Incident& incident ) override;

  //------------------
  const std::vector<MuonHit>*   trackhits() const override;
  const std::vector<MuonTrack>& tracks() const override {
    if ( !m_recDone ) const_cast<MuonNNetRec*>( this )->muonNNetMon(); //@FIXME
    return m_tracks;
  }
  const std::vector<MuonNeuron*>* useneurons() override {
    if ( !m_recDone ) muonNNetMon();
    return (const std::vector<MuonNeuron*>*)( &m_useneurons );
  }
  const std::vector<MuonNeuron*>* allneurons() override {
    if ( !m_recDone ) muonNNetMon();
    return (const std::vector<MuonNeuron*>*)( &m_allneurons );
  }
  bool recOK() override { return ( m_recDone && m_recOK ); }
  bool tooManyHits() override { return m_tooManyHits; }
  bool clusteringOn() override { return ( m_clusterToolName != "MuonFakeClustering" ); }
  void setZref( double Zref ) override { MuonTrackRec::Zref = Zref; }
  void setPhysicsTiming( bool PhysTiming ) override { MuonTrackRec::PhysTiming = PhysTiming; }
  void setAssumeCosmics( bool AssumeCosmics ) override {
    MuonTrackRec::IsCosmic = AssumeCosmics;
    if ( AssumeCosmics ) MuonTrackRec::IsPhysics = false;
  }
  void setAssumePhysics( bool AssumePhysics ) override {
    MuonTrackRec::IsPhysics = AssumePhysics;
    if ( AssumePhysics ) MuonTrackRec::IsCosmic = false;
  }
  void       setSeedStation( int seedS ) override { m_seedStation = seedS; }
  void       setSkipStation( int skipS ) override { m_skipStation = skipS; }
  StatusCode copyToLHCbTracks() override;

private:
  IMuonHitDecode*   m_decTool      = nullptr;
  IMuonPadRec*      m_padTool      = nullptr;
  IMuonClusterRec*  m_clusterTool  = nullptr;
  IMuonTrackMomRec* m_momentumTool = nullptr;
  DeMuonDetector*   m_muonDetector = nullptr;

  bool m_recDone     = false;
  bool m_recOK       = false;
  bool m_tooManyHits = false;
  //  bool m_forceResetDAQ;

  std::vector<MuonNeuron*> m_useneurons;
  std::vector<MuonNeuron*> m_allneurons;
  std::vector<MuonTrack>   m_tracks;

  void clear();

  StatusCode muonNNetMon();
  StatusCode trackFit();

  // algorithm timing monitor
  ISequencerTimerTool* m_timer        = nullptr;
  int                  m_timeinitNet  = 0;
  int                  m_timeconvNet  = 0;
  int                  m_timefitTrack = 0;

  // job options
  Gaudi::Property<float>  m_aa{this, "PosScaleFactor", 10., "head-tail weight scale factor"};
  Gaudi::Property<float>  m_bb{this, "NegScaleFactorB", 1., "head-head weight scale factor"};
  Gaudi::Property<float>  m_cc{this, "NegScaleFactorC", 1., "tail-tail weight scale factor"};
  Gaudi::Property<float>  m_slamb{this, "SlamFactorB", 5., "penalty for TT connections"};
  Gaudi::Property<float>  m_slamc{this, "SlamFactorC", 5., "penalty for HH connections"};
  Gaudi::Property<double> m_xesp1{this, "ExponentXZ", 10., "exponent for (1-sin(thxz)) weight factor"};
  Gaudi::Property<double> m_xesp2{this, "ExponentYZ", 10., "exponent for (1-sin(thyz)) weight factor"};
  Gaudi::Property<float>  m_dd{this, "Stimulus", 0., "stimulation weight term"};
  Gaudi::Property<float>  m_temp{this, "Temperature", 1., "temperature"};
  Gaudi::Property<float>  m_dsum{this, "Convergence", 1e-5, "convergence factor"};
  Gaudi::Property<float>  m_scut{this, "NeuronThreshold", 0.7, "neuron activation cut"};
  Gaudi::Property<float>  m_acut{this, "DoubleKillAngCut", 0.1, "angular cut for double length neurons killing"};
  Gaudi::Property<int>    m_span_cut{this, "TrackSpanCut", 2, "cut on span for selected tracks"};
  Gaudi::Property<int>    m_firing_cut{this, "StationFiringCut", 2, "min # of stations firing in the track"};
  Gaudi::Property<int>    m_maxNeurons{this, "MaxNeurons", 3000, "max number of possible track segments"};
  Gaudi::Property<int>    m_maxIterations{this, "MaxIterations", 100, "max number of NN iterations"};

  Gaudi::Property<int> m_skipStation{this, "SkipStation", -1,
                                     "station to be skipped (e.g. for eff. study): 0-4; -1 = keep all stns (default)"};

  Gaudi::Property<bool> m_allowHoles{this, "AllowHoles", true,
                                     "if <true> (default) holes of 1 station are allowed in track reconstruction"};

  Gaudi::Property<bool> m_physicsTiming{
      this, "PhysicsTiming", true,
      "if true, we assume that timing is the final one (accounting for TOF from primary vx)"};

  Gaudi::Property<bool> m_assumeCosmics{
      this, "AssumeCosmics", true, "if true (default) we assume that tracks are of cosmic origin (can be backward)"};

  Gaudi::Property<bool> m_assumePhysics{this, "AssumePhysics", false,
                                        "if true we assume that tracks have the 'right' direction (pz>0)"};

  // TOOLS
  Gaudi::Property<std::string> m_decToolName{
      this, "DecodingTool", "MuonHitDecode",
      "name of decoding tool (MuonHitDecode for offline, MuonMonHitDecode for online monitoring)"};

  Gaudi::Property<std::string> m_padToolName{this, "PadRecTool", "MuonPadRec",
                                             "name of pad rec tool (MuonPadRec only option so far)"};

  Gaudi::Property<std::string> m_clusterToolName{this, "ClusterTool", "MuonFakeClustering", "name of clustering tool"};

  Gaudi::Property<bool>  m_XTalk{this, "AddXTalk", false};
  Gaudi::Property<float> m_XtalkRadius{this, "XtalkRadius", 1.5};

  /// LHCb tracks output location in TES
  Gaudi::Property<std::string> m_trackOutputLoc{this, "TracksOutputLocation", "Rec/Muon/Track"};

  // this is useless; only to comply with the virtual interface
  int m_seedStation = 0;
};
#endif // MUONNNETREC_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONPADFROMCOORD_H
#define MUONPADFROMCOORD_H 1
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonPadRec.h" // Interface
#include <map>

/** @class MuonPadFromCoord MuonPadFromCoord.h
 *  converts MuonCoord to MuonLogPad objects
 *
 *  @author G.Graziani
 *  @date   2014-03-11
 */

namespace LHCB {
  class MuonTileID;
}

class MuonPadFromCoord final : public extends<GaudiTool, IMuonPadRec, IIncidentListener> {
public:
  /// Standard constructor
  MuonPadFromCoord( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode                     buildLogicalPads( const std::vector<MuonLogHit>& myhits ) override;
  const std::vector<MuonLogPad>& pads() override;

  // from GaudiTool
  StatusCode initialize() override;
  StatusCode finalize() override;

  // from IIncidentListener
  void handle( const Incident& incident ) override;

private:
  std::vector<LHCb::MuonTileID> m_tiles;
  std::vector<MuonLogHit>       m_hits;
  std::vector<MuonLogPad>       m_pads;
  void                          clearPads();
  unsigned int                  m_stationOffset     = 0;
  bool                          m_padsReconstructed = false;
  DeMuonDetector*               m_muonDetector      = nullptr;
};
#endif // MUONPADFROMCOORD_H

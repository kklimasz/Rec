###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: MuonTrackRec
################################################################################
gaudi_subdir(MuonTrackRec v4r0)

gaudi_depends_on_subdirs(Det/MuonDet
                         GaudiAlg
                         Muon/MuonDAQ
                         Muon/MuonInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(MuonTrackRec
                 src/*
                 src/component/*.cpp
                 INCLUDE_DIRS Muon/MuonDAQ
                 LINK_LIBRARIES MuonDetLib GaudiAlgLib MuonInterfacesLib)


/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMUONREC_H
#define ALIGNMUONREC_H 1

#include "AlignMuonStationRec.h"
#include "AlignMuonTrack.h"

#include "Event/Track.h"
#include "Kernel/IMuonLayout.h"
#include "MuonDAQ/IMuonRawBuffer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/DeMuonRegion.h"
#include "MuonDet/MuonBasicGeometry.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"

#include "AIDA/IHistogram1D.h" //MB
#include "AIDA/IHistogram2D.h" //MB

/** @class AlignMuonRec AlignMuonRec.h
 *
 *
 *  @author Alessia Satta
 *  @date   2004-10-06
 */
class AlignMuonRec : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization
  StatusCode decodeBuffer();

  //  unsigned int readoutType(int partition, MuonLayout lay);
  LHCb::MuonTileID intercept( const LHCb::MuonTileID& stripX, const LHCb::MuonTileID& stripY );
  void             muonSearch();
  void             detectClone();
  void             strongCloneKiller();

  void insertMatch( int muon, LHCb::Track* track, float distance );
  void printOut();

  StatusCode createCoords();

private:
  IMuonRawBuffer*         m_muonBuffer    = nullptr;
  ITrackMomentumEstimate* m_fCalcMomentum = nullptr; // momentum tool  // Silvia July 07
  DeMuonDetector*         m_muonDetector  = nullptr;

  unsigned int m_nStation = 0;
  unsigned int m_nRegion  = 0;

  ISequencerTimerTool* m_timer         = nullptr;
  int                  m_timeLoad      = 0;
  int                  m_timePad       = 0;
  int                  m_timeMuon      = 0;
  int                  m_timeMuonStore = 0;

  std::vector<AlignMuonStationRec>               m_station;
  std::vector<std::pair<LHCb::MuonTileID, bool>> twelfthX[12];
  std::vector<std::pair<LHCb::MuonTileID, bool>> twelfthY[12];
  std::vector<AlignMuonTrack>                    m_muonTracks;
  const unsigned int                             m_maxMuonFound = 10;

  Gaudi::Property<bool>  m_measureTime{this, "MeasureTime", true};
  Gaudi::Property<bool>  m_cloneKiller{this, "CloneKiller", true};
  Gaudi::Property<bool>  m_Bfield{this, "BField", true};
  Gaudi::Property<bool>  m_m1Station{this, "M1Station", true};
  Gaudi::Property<float> m_matchChisq{this, "MatchChisq", 40.};

  Gaudi::Property<bool> m_decodingFromCoord{this, "DecodingFromCoord", true};

  Gaudi::Property<std::string> m_outputMuonTracksName{this, "OutputMuonTracksName",
                                                      "/Event/Rec/Muon/MuonsForAlignment"};

  Gaudi::Property<std::string> m_outputMuonTracksForAlignmentName{this, "OutputMuonTracksForAlignmentName",
                                                                  "/Event/Rec/Muon/MuonsTrackForAlignment"};

  Gaudi::Property<std::string> m_extrapolatorName{this, "Extrapolator", "TrackMasterExtrapolator"};

  ITrackExtrapolator* m_extrapolator = nullptr;

  Gaudi::Property<bool> m_Histo{this, "Histos", true};

  /// M5 pad size
  const double PADsizeX[4] = {31.6667, 63.0, 126.0, 252.0};
  const double PADsizeY[4] = {38.8585, 77.9582, 156.158, 312.557};

  // counters
  int m_countEvents       = 0;
  int m_countMuCandidates = 0;

  AIDA::IHistogram1D *m_Chi2match, *m_tx, *m_ty, *m_p, *m_states, *m_nMuonTrack, *m_nMuonTrackNoClone,
      *m_nMuonTrackMatch;
  AIDA::IHistogram2D *m_Mfirst, *m_hitM;
  AIDA::IHistogram2D *m_resx[5][4], *m_resy[5][4];
};
#endif // ALIGNMUONREC_H

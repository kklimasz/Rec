/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "MuonFastHWTool.h"

using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class : MuonFastHWTool
//
// 2003-04-16 : Alessia Satta
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( MuonFastHWTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonFastHWTool::MuonFastHWTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IMuonFastHWTool>( this );
}
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
StatusCode MuonFastHWTool::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( !sc ) return sc;
  debug() << " initialization " << endmsg;

  m_muonDetector = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" );

  /// get tile tool
  MuonBasicGeometry basegeometry( detSvc(), msgSvc() );
  m_stationNumber = basegeometry.getStations();
  m_regionNumber  = basegeometry.getRegions();

  long L1Number    = 0;
  long link_number = 0;
  long ODE_number  = 0;
  long ode_ch      = 0;
  int  add         = 0;
  for ( int station = 0; station < 5; station++ ) {
    for ( int region = 0; region < 4; region++ ) {
      int totone = 0;
      int tottwo = 0;
      totone     = 4 * m_layoutY[0][station * 4 + region] * m_layoutX[0][station * 4 + region];

      if ( layout[station * 4 + region] == 2 ) {
        tottwo = 4 * m_layoutY[1][station * 4 + region] * m_layoutX[1][station * 4 + region];
      }
      for ( int quadrant = 0; quadrant < 4; quadrant++ ) {

        m_ODENum[station * 16 + region * 4 + quadrant][0].resize( totone );
        m_synchNum[station * 16 + region * 4 + quadrant][0].resize( totone );
        if ( layout[station * 4 + region] == 2 ) {
          m_ODENum[station * 16 + region * 4 + quadrant][1].resize( tottwo );
          m_synchNum[station * 16 + region * 4 + quadrant][1].resize( tottwo );
        }
      }
    }
  }

  for ( int station = 0; station < 5; station++ ) {
    for ( int region = 0; region < 4; region++ ) {
      int num_lay = layout[station * 4 + region];
      for ( int ilay = 0; ilay < num_lay; ilay++ ) {
        MuonLayout lay( m_layoutX[ilay][station * 4 + region], m_layoutY[ilay][station * 4 + region] );

        for ( int quadrant = 0; quadrant < 4; quadrant++ ) {

          for ( int x = 0; x < m_layoutX[ilay][station * 4 + region]; x++ ) {
            for ( int y = 0; y < m_layoutY[ilay][station * 4 + region]; y++ ) {
              MuonTileID muontile1( station, lay, region, quadrant, x + m_layoutX[ilay][station * 4 + region], y );
              MuonTileID muontile2( station, lay, region, quadrant, x, y + m_layoutY[ilay][station * 4 + region] );
              MuonTileID muontile3( station, lay, region, quadrant, x + m_layoutX[ilay][station * 4 + region],
                                    y + m_layoutY[ilay][station * 4 + region] );

              L1Number    = 0;
              link_number = 0;
              ODE_number  = 0;
              ode_ch      = 0;

              m_muonDetector->getDAQInfo()->findHWNumber( muontile1, L1Number, link_number, ODE_number, ode_ch );

              add = y * m_layoutX[ilay][station * 4 + region] * 2 + x + m_layoutX[ilay][station * 4 + region];
              m_ODENum[station * 16 + region * 4 + quadrant][ilay][add]   = ODE_number;
              m_synchNum[station * 16 + region * 4 + quadrant][ilay][add] = ode_ch;

              L1Number    = 0;
              link_number = 0;
              ODE_number  = 0;
              ode_ch      = 0;

              m_muonDetector->getDAQInfo()->findHWNumber( muontile2, L1Number, link_number, ODE_number, ode_ch );
              add = ( y + m_layoutY[ilay][station * 4 + region] ) * m_layoutX[ilay][station * 4 + region] * 2 + x;
              m_ODENum[station * 16 + region * 4 + quadrant][ilay][add] = ODE_number;

              m_synchNum[station * 16 + region * 4 + quadrant][ilay][add] = ode_ch;

              m_muonDetector->getDAQInfo()->findHWNumber( muontile3, L1Number, link_number, ODE_number, ode_ch );

              add = ( y + m_layoutY[ilay][station * 4 + region] ) * m_layoutX[ilay][station * 4 + region] * 2 + x +
                    m_layoutX[ilay][station * 4 + region];
              m_ODENum[station * 16 + region * 4 + quadrant][ilay][add]   = ODE_number;
              m_synchNum[station * 16 + region * 4 + quadrant][ilay][add] = ode_ch;
            }
          }
        }
      }
    }
  }
  initFEB();
  return StatusCode::SUCCESS;
}

//=============================================================================
StatusCode MuonFastHWTool::GetHWName( LHCb::MuonTileID tile, int& ODENum, int& SynchOne, int& SynchTwo ) const {
  // check wheter tile is strips or cross of two strips

  if ( !tile.isValid() ) return StatusCode::FAILURE;

  int        reg      = tile.region();
  int        sta      = tile.station();
  int        quadrant = tile.quarter();
  MuonLayout tilelay  = tile.layout();
  ODENum              = -1;
  SynchOne            = -1;
  SynchTwo            = -1;

  if ( layout[sta * 4 + reg] == 1 ) {
    int add  = tile.nY() * m_layoutX[0][sta * 4 + reg] * 2 + tile.nX();
    ODENum   = m_ODENum[sta * 16 + reg * 4 + quadrant][0][add];
    SynchOne = m_synchNum[sta * 16 + reg * 4 + quadrant][0][add];

  } else {
    MuonLayout tmplayOne( m_layoutX[0][sta * 4 + reg], m_layoutY[0][sta * 4 + reg] );
    MuonLayout tmplayTwo( m_layoutX[1][sta * 4 + reg], m_layoutY[1][sta * 4 + reg] );

    if ( tilelay == tmplayOne || tilelay == tmplayTwo ) {
      int Numlay = -1;

      if ( tilelay == tmplayOne ) Numlay = 1;
      if ( tilelay == tmplayTwo ) Numlay = 2;
      int add  = tile.nY() * m_layoutX[Numlay - 1][sta * 4 + reg] * 2 + tile.nX();
      ODENum   = m_ODENum[sta * 16 + reg * 4 + quadrant][Numlay - 1][add];
      SynchOne = m_synchNum[sta * 16 + reg * 4 + quadrant][Numlay - 1][add];
    } else {
      for ( int i = 0; i < 2; i++ ) {

        MuonLayout Mylay( m_layoutX[i][sta * 4 + reg], m_layoutY[i][sta * 4 + reg] );
        MuonTileID tmp = tile.containerID( Mylay );
        int        add = tmp.nY() * m_layoutX[i][sta * 4 + reg] * 2 + tmp.nX();
        ODENum         = m_ODENum[sta * 16 + reg * 4 + quadrant][i][add];
        if ( i == 0 ) SynchOne = m_synchNum[sta * 16 + reg * 4 + quadrant][i][add];
        if ( i == 1 ) SynchTwo = m_synchNum[sta * 16 + reg * 4 + quadrant][i][add];
      }
    }
  }

  return StatusCode::SUCCESS;
}

void MuonFastHWTool::initFEB() {
  int station = -1;
  int region  = -1;

  std::vector<MuonLayout> myLayout;
  for ( station = 0; station < 5; station++ ) {
    for ( region = 0; region < 4; region++ ) {

      int NLogicalMap = m_muonDetector->getLogMapInRegion( station, region );

      unsigned int x1 = 0;
      unsigned int x2 = 0;
      unsigned int y1 = 0;
      unsigned int y2 = 0;
      x1              = m_muonDetector->getLayoutX( 0, station, region );
      y1              = m_muonDetector->getLayoutY( 0, station, region );
      verbose() << " station  reg lay " << station << " " << region << " " << x1 << " " << y1 << endmsg;

      if ( NLogicalMap == 2 ) {
        x2 = m_muonDetector->getLayoutX( 1, station, region );
        y2 = m_muonDetector->getLayoutY( 1, station, region );
        verbose() << " station  reg lay " << station << " " << region << " " << x2 << " " << y2 << endmsg;
      }

      if ( region == 1 ) {
        y1 = y1 / 2;
        y2 = y2 / 2;
      }
      if ( region == 2 ) {
        y1 = y1 / 4;
        y2 = y2 / 4;
      }
      if ( region == 3 ) {
        y1 = y1 / 8;
        y2 = y2 / 8;
        x1 = x1 / 2;
        x2 = x2 / 2;
      }
      if ( m_realLayout[station * 4 + region] == 1 && NLogicalMap == 1 ) {

        MuonLayout layoutOne( x1, y1 );
        MuonLayout layoutTwo( x2, y2 );
        m_padInChLayout.push_back( layoutOne );
        m_padInChLayout.push_back( layoutTwo );
        verbose() << "case 1 Layout " << layoutOne << endmsg;

      } else if ( m_realLayout[station * 4 + region] == 1 && NLogicalMap == 2 ) {

        MuonLayout layoutOne( x1, y2 );
        MuonLayout layoutTwo( x2, y1 );
        m_padInChLayout.push_back( layoutOne );
        m_padInChLayout.push_back( layoutTwo );
        verbose() << "case 2 Layout " << layoutOne << endmsg;
      } else {
        MuonLayout layoutOne( x1, y1 );
        MuonLayout layoutTwo( x2, y2 );
        m_padInChLayout.push_back( layoutOne );
        m_padInChLayout.push_back( layoutTwo );
        verbose() << "case 3 Layout " << layoutOne << " " << layoutTwo << endmsg;
      }
    }
  }

  for ( int sta = 0; sta < 5; sta++ ) {
    for ( int reg = 0; reg < 4; reg++ ) {
      int index = sta * 4 + reg;
      for ( int lay = 0; lay < m_realLayout[index]; lay++ ) {
        int xx = m_padInChLayout[index * 2 + lay].xGrid();
        int yy = m_padInChLayout[index * 2 + lay].yGrid();
        m_I2C[index][lay].resize( xx * yy );
        m_FEB[index][lay].resize( xx * yy );
      }
    }
  }

  // how many daisy chain  per chambers per layout

  // info()<<" step 2"<<endmsg;
  // fill list of pad in each I2C / FEb  per chamber type
  int index = -1;
  int lay   = -1;
  int d     = -1;
  int c     = -1;
  int qua   = 0;
  station   = -1;
  region    = -1;

  MuonTileID tileInC;

  int offsetX = 0;
  // M1R1

  station = 0;
  region  = 0;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );
  offsetX = m_padInChLayout[index * 2 + lay].xGrid();
  // info()<<" M1R1 "<<offsetX<<endmsg;

  d = 0;
  c = 0;
  setI2CFeb( 8, 12, 6, 8, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 4, 8, 6, 8, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 0, 4, 6, 8, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 4, 12, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 0, 4, 5, 6, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 8, 12, 4, 5, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 0, 8, 4, 5, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 0, 8, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 8, 12, 3, 4, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 0, 4, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 4, 12, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 0, 4, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 4, 8, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 5;
  setI2CFeb( 8, 12, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 0;
  setI2CFeb( 12, 16, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 1;
  setI2CFeb( 16, 20, 0, 2, offsetX, index, lay, d, c, tileInC );
  d = 2;
  c = 2;
  setI2CFeb( 20, 24, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 3;
  setI2CFeb( 12, 20, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 4;
  setI2CFeb( 20, 24, 2, 3, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 12, 16, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 5;
  setI2CFeb( 16, 24, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 0;
  setI2CFeb( 16, 24, 4, 5, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 1;
  setI2CFeb( 12, 16, 4, 5, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 20, 24, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 2;
  setI2CFeb( 12, 20, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 3;
  setI2CFeb( 20, 24, 6, 8, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 4;
  setI2CFeb( 16, 20, 6, 8, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 5;
  setI2CFeb( 12, 16, 6, 8, offsetX, index, lay, d, c, tileInC );

  // info()<<" step 3"<<endmsg;

  // M1R2

  station = 0;
  region  = 1;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  d = 0;
  c = 0;
  setI2CFeb( 0, 4, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 0, 4, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 0, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 0, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 4, 6, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 6, 8, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 8, 10, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 10, 12, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 12, 14, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 5;
  setI2CFeb( 14, 16, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 6;
  setI2CFeb( 16, 18, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 7;
  setI2CFeb( 18, 20, 0, 2, offsetX, index, lay, d, c, tileInC );

  // first daisy chain cardiac

  d = 2;
  c = 0;
  setI2CFeb( 20, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 1;
  setI2CFeb( 20, 24, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 2;
  setI2CFeb( 20, 24, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 2;
  c = 3;
  setI2CFeb( 20, 24, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 0;
  setI2CFeb( 18, 20, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 1;
  setI2CFeb( 16, 18, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 2;
  setI2CFeb( 14, 16, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 3;
  setI2CFeb( 12, 14, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 4;
  setI2CFeb( 10, 12, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 5;
  setI2CFeb( 8, 10, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 6;
  setI2CFeb( 6, 8, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 3;
  c = 7;
  setI2CFeb( 4, 6, 2, 4, offsetX, index, lay, d, c, tileInC );

  // M1R3

  station = 0;
  region  = 2;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  d = 0;
  c = 0;
  setI2CFeb( 0, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 4, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 8, 12, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 12, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 16, 20, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 20, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 20, 24, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 16, 20, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 12, 16, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 8, 12, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 4, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 5;
  setI2CFeb( 0, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  // M1R4
  station = 0;
  region  = 3;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;
  setI2CFeb( 0, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 4, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 8, 12, 0, 1, offsetX, index, lay, d, c, tileInC );

  // info()<<" finita M1 1"<<endmsg;

  // fill one by one the list of tiles for cardiac
  // M2R1
  station = 1;
  region  = 0;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  station = 1;
  region  = 0;
  lay     = 1;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  lay = 1;

  d = 0;
  c = 0;
  setI2CFeb( 0, 4, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 0, 4, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 0, 4, 4, 6, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 0, 4, 6, 8, offsetX, index, lay, d, c, tileInC );

  lay = 1;

  d = 1;
  c = 3;
  setI2CFeb( 4, 8, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 4, 8, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 4, 8, 4, 6, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 4, 8, 6, 8, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M2R2
  station = 1;
  region  = 1;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  // lay  1

  station = 1;
  region  = 1;
  lay     = 1;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  lay = 1;
  d   = 0;

  d = 0;
  c = 0;
  setI2CFeb( 0, 2, 0, 1, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 1;
  setI2CFeb( 0, 2, 1, 2, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 2;
  setI2CFeb( 0, 2, 2, 3, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 3;
  setI2CFeb( 0, 2, 3, 4, offsetX, index, lay, d, c, tileInC );

  lay = 1;
  d   = 1;

  d = 1;
  c = 3;
  setI2CFeb( 2, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 2, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 2, 4, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 2, 4, 3, 4, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M2R3
  station = 1;
  region  = 2;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  // second daisy chain cardiac
  d = 1;

  d = 1;
  c = 5;
  setI2CFeb( 0, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 8, 16, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 16, 24, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 24, 32, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 32, 40, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 40, 48, 1, 2, offsetX, index, lay, d, c, tileInC );

  // M2R4
  station = 1;
  region  = 3;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  // M3R1
  station = 2;
  region  = 0;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  station = 2;
  region  = 0;
  lay     = 1;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  lay = 1;

  d = 0;
  c = 0;
  setI2CFeb( 0, 4, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 0, 4, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 0, 4, 4, 6, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 0, 4, 6, 8, offsetX, index, lay, d, c, tileInC );

  lay = 1;

  d = 1;
  c = 3;
  setI2CFeb( 4, 8, 0, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 4, 8, 2, 4, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 4, 8, 4, 6, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 4, 8, 6, 8, offsetX, index, lay, d, c, tileInC );

  // M3R2
  station = 2;
  region  = 1;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  // lay  1

  station = 2;
  region  = 1;
  lay     = 1;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  lay = 1;
  d   = 0;

  d = 0;
  c = 0;
  setI2CFeb( 0, 2, 0, 1, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 1;
  setI2CFeb( 0, 2, 1, 2, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 2;
  setI2CFeb( 0, 2, 2, 3, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 3;
  setI2CFeb( 0, 2, 3, 4, offsetX, index, lay, d, c, tileInC );

  lay = 1;
  d   = 1;

  d = 1;
  c = 3;
  setI2CFeb( 2, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 2, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 2, 4, 2, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 2, 4, 3, 4, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M3R3
  station = 2;
  region  = 2;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac

  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 24, 32, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 32, 40, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 40, 48, 0, 1, offsetX, index, lay, d, c, tileInC );

  // second daisy chain cardiac
  d = 1;

  d = 1;
  c = 5;
  setI2CFeb( 0, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 8, 16, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 16, 24, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 24, 32, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 32, 40, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 40, 48, 1, 2, offsetX, index, lay, d, c, tileInC );

  // M3R4
  station = 2;
  region  = 3;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;
  setI2CFeb( 0, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 8, 16, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 16, 24, 0, 1, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M4R1
  station = 3;
  region  = 0;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 6, 0, 1, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 4, 6, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 0, 4, 1, 3, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 2, 3, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 0, 6, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 0, 6, 4, 5, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 4, 6, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 0, 4, 5, 7, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 4, 6, 6, 7, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 0, 6, 7, 8, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 6, 8, 6, 7, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 12, 7, 8, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 8, 12, 5, 7, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 6, 12, 4, 5, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 8, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 6, 8, 2, 3, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 12, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 8, 12, 1, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 5;
  setI2CFeb( 6, 12, 0, 1, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M4R2
  station = 3;
  region  = 1;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 2, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 1;
  setI2CFeb( 2, 4, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 0, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 6, 8, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 4;
  setI2CFeb( 8, 10, 0, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 10, 12, 0, 4, offsetX, index, lay, d, c, tileInC );
  // M4R3
  station = 3;
  region  = 2;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 4, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 8, 12, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 8, 12, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 4, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 0, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M4R4
  station = 3;
  region  = 3;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;
  setI2CFeb( 0, 2, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 2, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 0, 1, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M5R1
  station = 4;
  region  = 0;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 6, 0, 1, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 4, 6, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 0, 4, 1, 3, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 2, 3, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 0, 6, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 0, 6, 4, 5, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 4, 6, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 0, 4, 5, 7, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 4, 6, 6, 7, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 0, 6, 7, 8, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 0;
  setI2CFeb( 6, 8, 6, 7, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 12, 7, 8, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 1;
  setI2CFeb( 8, 12, 5, 7, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 2;
  setI2CFeb( 6, 12, 4, 5, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 8, 5, 6, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 3;
  setI2CFeb( 6, 8, 2, 3, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 12, 3, 4, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 4;
  setI2CFeb( 8, 12, 1, 3, offsetX, index, lay, d, c, tileInC );

  d = 1;
  c = 5;
  setI2CFeb( 6, 12, 0, 1, offsetX, index, lay, d, c, tileInC );
  setI2CFeb( 6, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M5R2
  station = 4;
  region  = 1;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 2, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 1;
  setI2CFeb( 2, 4, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 0, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 6, 8, 0, 4, offsetX, index, lay, d, c, tileInC );
  d = 0;
  c = 4;
  setI2CFeb( 8, 10, 0, 4, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 10, 12, 0, 4, offsetX, index, lay, d, c, tileInC );
  // M5R3
  station = 4;
  region  = 2;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;

  setI2CFeb( 0, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 4, 8, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 8, 12, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 3;
  setI2CFeb( 8, 12, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 4;
  setI2CFeb( 4, 8, 1, 2, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 5;
  setI2CFeb( 0, 4, 1, 2, offsetX, index, lay, d, c, tileInC );

  // fill one by one the list of tiles for cardiac
  // M5R4
  station = 4;
  region  = 3;
  lay     = 0;
  index   = station * 4 + region;
  tileInC.setStation( station );
  tileInC.setRegion( region );
  tileInC.setQuarter( qua );
  tileInC.setLayout( m_padInChLayout[index * 2 + lay] );

  offsetX = m_padInChLayout[index * 2 + lay].xGrid();

  // first daisy chain cardiac
  d = 0;
  c = 0;
  setI2CFeb( 0, 2, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 1;
  setI2CFeb( 2, 4, 0, 1, offsetX, index, lay, d, c, tileInC );

  d = 0;
  c = 2;
  setI2CFeb( 4, 6, 0, 1, offsetX, index, lay, d, c, tileInC );
}

int MuonFastHWTool::getPadChIndex( MuonTileID tile ) const {
  // tile is in chamber reference
  unsigned int localX = tile.nX();
  unsigned int localY = tile.nY();
  if ( localX >= tile.layout().xGrid() ) localX = localX - tile.layout().xGrid();
  if ( localY >= tile.layout().yGrid() ) localY = localY - tile.layout().yGrid();
  int index = localY * tile.layout().xGrid() + localX;
  return index;
}

void MuonFastHWTool::fillTileParameter( MuonTileID tile, int lay, int i2c, int feb ) {
  int index = tile.station() * 4 + tile.region();

  int padindex = getPadChIndex( tile );
  // info()<<" file tile "<<tile<<" "<<index<<" "<<padindex<<" " <<lay<<endmsg;

  m_I2C[index][lay][padindex] = i2c;
  m_FEB[index][lay][padindex] = feb;
}

StatusCode MuonFastHWTool::GetI2C( LHCb::MuonTileID tile, int& I2C ) const {
  int        result, index, lay;
  StatusCode sc = getGlobalIndex( tile, result, index, lay );
  if ( sc.isFailure() ) return sc;
  I2C = m_I2C[index][lay][result];
  return StatusCode::SUCCESS;
}

StatusCode MuonFastHWTool::GetFEBInCh( LHCb::MuonTileID tile, int& FEB ) const {
  int        result, index, lay;
  StatusCode sc = getGlobalIndex( tile, result, index, lay );
  if ( sc.isFailure() ) return sc;
  FEB = m_FEB[index][lay][result];
  return StatusCode::SUCCESS;
}

StatusCode MuonFastHWTool::GetFEBInI2C( LHCb::MuonTileID tile, int& FEB ) const {
  int        result, index, lay;
  StatusCode sc = getGlobalIndex( tile, result, index, lay );
  if ( sc.isFailure() ) return sc;

  FEB = m_FEB[index][lay][result];
  return StatusCode::SUCCESS;
}

StatusCode MuonFastHWTool::getGlobalIndex( MuonTileID tile, int& result, int& index, int& lay ) const {
  int region  = tile.region();
  int station = tile.station();
  index       = station * 4 + region;

  MuonLayout tilelay = tile.layout();
  MuonLayout myLayout( tilelay.xGrid() / m_chamberX[region], tilelay.yGrid() / m_chamberY[region] );
  lay = -1;

  // which layout if more than one???
  if ( m_realLayout[index] == 1 ) {
    if ( myLayout == m_padInChLayout[index * 2] ) lay = 0;
  }
  if ( m_realLayout[index] == 2 ) {
    if ( myLayout == m_padInChLayout[index * 2] ) lay = 0;
    if ( myLayout == m_padInChLayout[index * 2 + 1] ) lay = 1;
  }
  if ( lay < 0 ) return StatusCode::FAILURE;

  int localX = tile.nX() % ( m_padInChLayout[index * 2 + lay].xGrid() );
  int localY = tile.nY() % ( m_padInChLayout[index * 2 + lay].yGrid() );
  result     = localX + localY * m_padInChLayout[index * 2 + lay].xGrid();
  return StatusCode::SUCCESS;
}

LHCb::MuonTileID MuonFastHWTool::transformTile( int quarter, int chX, int chY, MuonTileID tile ) const {
  MuonTileID Outtile;
  int        station = tile.station();
  int        region  = tile.region();
  MuonLayout inLay   = tile.layout();
  MuonLayout outLay( inLay.xGrid() * m_chamberX[region], inLay.yGrid() * m_chamberY[region] );
  Outtile.setStation( station );
  Outtile.setRegion( region );
  Outtile.setLayout( outLay );
  Outtile.setQuarter( quarter );
  int Xlocal = tile.nX();
  int Ylocal = tile.nY();
  int XGlob  = Xlocal - inLay.xGrid() + chX * inLay.xGrid();
  int YGlob  = Ylocal + chY * inLay.yGrid();
  Outtile.setX( XGlob );
  Outtile.setY( YGlob );
  return Outtile;
}

void MuonFastHWTool::setI2CFeb( int xmin, int xmax, int ymin, int ymax, int offsetX, int index, int lay, int d, int c,
                                LHCb::MuonTileID tilePart ) {
  for ( int ii = xmin; ii < xmax; ii++ ) {
    for ( int j = ymin; j < ymax; j++ ) {
      tilePart.setX( ii + offsetX );
      tilePart.setY( j );
      m_padInCh[index][lay][d][c].push_back( tilePart );
      fillTileParameter( tilePart, lay, d, c );
    }
  }
}

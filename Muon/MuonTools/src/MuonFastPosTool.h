/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONFASTPOSTOOL_H
#define MUONFASTPOSTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"

// local
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonBasicGeometry.h"

/** @class MuonFastPosTool MuonFastPosTool.h
 *
 *  Convert an MuonTileID into an xyz position in the detector (with size)
 *  No abstract interface as I do not want to make more than one of these...
 *
 *  @author David Hutchcroft
 *  @date   07/03/2002
 */
class MuonFastPosTool : public extends<GaudiTool, IMuonFastPosTool> {
public:
  /// Standard constructor
  MuonFastPosTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
   * this ignores gaps: these can never be read out independently
   */
  StatusCode calcTilePos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay, double& z,
                          double& deltaz ) const override;

  StatusCode calcStripXPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override;

  StatusCode calcStripYPos( const LHCb::MuonTileID& tile, double& x, double& deltax, double& y, double& deltay,
                            double& z, double& deltaz ) const override;

private:
  // Number of stations
  int m_stationNumber = 0;
  // Number of regions
  int m_regionNumber = 0;

  // size of pads
  std::vector<float>           m_padSizeX;
  std::vector<float>           m_padSizeY;
  std::vector<Gaudi::XYZPoint> m_posPad[5];
  DeMuonDetector*              m_muonDetector = nullptr;
  unsigned int                 m_padGridX_internal[5];
  unsigned int                 m_padGridY_internal[5];
  // size of stripx

  std::vector<float>           m_stripXSizeX;
  std::vector<float>           m_stripXSizeY;
  std::vector<Gaudi::XYZPoint> m_posStripX[5];

  unsigned int m_stripXGridX_internal[20];
  unsigned int m_stripXGridY_internal[20];
  unsigned int m_stripXOffset[20];
  // size of stripy

  std::vector<float>           m_stripYSizeX;
  std::vector<float>           m_stripYSizeY;
  std::vector<Gaudi::XYZPoint> m_posStripY[5];

  unsigned int m_stripYGridX_internal[20];
  unsigned int m_stripYGridY_internal[20];
  unsigned int m_stripYOffset[20];
};
#endif // MUONFASTPOSTOOL_H

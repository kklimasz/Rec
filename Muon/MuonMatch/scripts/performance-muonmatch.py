#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Make performance measurements for the MuonMatch algorithms.
'''

__author__ = ['Miguel Ramos Pernas']
__email__ = ['miguel.ramos.pernas@cern.ch']

# Gaudi
from Configurables import Brunel, FTRawBankDecoder, GaudiSequencer, HistogramPersistencySvc, MuonMatchPerformance, MuonMatchVeloUTSoA, TrackSys
from GaudiPython import AppMgr
from Gaudi.Configuration import appendPostConfigAction
from GaudiKernel.SystemOfUnits import GeV, mm

from PRConfig import TestFileDB

# Python
import argparse


def postconfig():
    '''
    Brunel post-configuration actions.
    '''
    GaudiSequencer('RecoCALOSeq').Members = []
    GaudiSequencer('RecoCALOFUTURESeq').Members = []
    GaudiSequencer('RecoPROTOSeq').Members = []
    GaudiSequencer('RecoRICHSeq').Members = []
    GaudiSequencer('CheckRICHSeq').Members = []
    GaudiSequencer('RecoSUMMARYSeq').Members = []
    GaudiSequencer('LumiSeq').Members = []
    GaudiSequencer('MoniSeq').Members = []


def main(tfdb, printfreq, histos, nevts, ipcut, ptcut, fity, maxchi2ndof,
         set_qop, seq_type):
    '''
    Function to execute.
    '''
    HistogramPersistencySvc().OutputFile = histos

    # Configure TrackSys
    tracksys = TrackSys()
    tracksys.ConfigHLT1['VeloUTHLT1']['minPT'] = ptcut
    tracksys.ConfigHLT1['VeloUTHLT1']['IPCutVal'] = ipcut
    tracksys.ConfigHLT1['ForwardHLT1']['PreselectionPT'] = ptcut
    tracksys.ConfigHLT1['ForwardHLT1']['minPT'] = ptcut

    # General configuration for Brunel
    brunel = Brunel()
    brunel.DataType = 'Upgrade'
    brunel.Detectors = ['VP', 'UT', 'FT', 'Muon', 'Magnet', 'Tr']
    brunel.OutputType = 'NONE'
    brunel.DisableTiming = True
    brunel.RecoSequence = ['Decoding', 'TrFast']
    brunel.MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks']
    brunel.PrintFreq = printfreq
    brunel.EvtMax = nevts

    # Configuration that depends on the sequence to run
    if seq_type == 'muonmatch':

        MuonMatchVeloUTSoA().FitY = fity
        MuonMatchVeloUTSoA().SetQOverP = set_qop

        if maxchi2ndof is not None:
            MuonMatchVeloUTSoA().MaxChi2DoF = maxchi2ndof

        tracks_to_check = [
            'Velo', 'Upstream', 'MuonMatchVeloUT', 'ForwardFast'
        ]

        tracksys.TrackingSequence = ['TrMuonMatch']

        MuonMatchVeloUTSoA().SetQOverP = set_qop
    else:
        tracks_to_check = ['Velo', 'Upstream', 'ForwardFast']

        if set_qop:
            warnings.warn(
                'Specifying --set-qop has no effect if the sequence is "main"',
                Warning)
        if fity:
            warnings.warn(
                'Specifying --fity has no effect if the sequence is "main"',
                Warning)

    tracksys.TracksToConvert = tracks_to_check

    appendPostConfigAction(postconfig)

    # Build and append the checkers to the main sequence
    checkers = []
    for ttype in tracks_to_check:

        checker = MuonMatchPerformance('Perf' + ttype)
        checker.AcceptanceCuts = True
        checker.InputTracks = 'Rec/Track/Keyed/' + ttype
        checker.LinkerLocation = 'Link/Rec/Track/Keyed/' + ttype
        checker.SkipMotherPIDs = [+211, -211]

        checkers.append(checker)

    brunel.MainSequence += checkers

    # Configure Brunel for the input data
    files_proxy = TestFileDB.test_file_db[tfdb]
    files_proxy.run(configurable=brunel)

    brunel.InputType = files_proxy.qualifiers['Format']
    brunel.Simulation = files_proxy.qualifiers['Simulation']
    brunel.WithMC = True

    if not brunel.Simulation:
        raise ValueError('This script must run on a Monte Carlo sample')

    if brunel.InputType == 'XDIGI':
        brunel.InputType = 'DIGI'

    if tfdb == 'upgrade-magdown-sim09c-up02-minbias-xdigi':
        FTRawBankDecoder('createFTClusters').DecodingVersion = 2
    else:
        FTRawBankDecoder('createFTClusters').DecodingVersion = 6

    # Initialize the application manager
    gaudi = AppMgr()
    gaudi.initialize()
    gaudi.run(nevts)
    gaudi.stop()
    gaudi.finalize()


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument(
        '--tfdb',
        type=str,
        default='upgrade-magdown-sim09c-up02-minbias-xdigi',
        choices=('upgrade-magdown-sim09c-up02-minbias-xdigi',
                 'upgrade-magup-sim10-up01-34112106-xdigi',
                 'upgrade-magdown-sim10-up01-24142001-xdigi'),
        help='TestFileDB entry to process')
    parser.add_argument(
        '--nevts', type=int, default=-1, help='Number of events to process')
    parser.add_argument(
        '--printfreq',
        type=int,
        default=3000,
        help='Frequency to print events')
    parser.add_argument(
        '--histos',
        type=str,
        default='muonmatch-performance-Brunel-histos.root',
        help='Name of the ROOT output file storing the histograms')
    parser.add_argument(
        '--fity',
        action='store_true',
        help='Whether to run with MuonMatchVeloUTSoA().FitY = True')
    parser.add_argument('--maxchi2ndof', type=float, default=None,
                        help='Maximum chi2/ndof allowed for the combination '\
                        'of track and hits in the MuonMatchVeloUT algorithm')
    parser.add_argument(
        '--ipcut',
        type=float,
        default=0.5,
        help='Cut on the IP for tracks in the "High-IP" category')
    parser.add_argument(
        '--ptcut',
        type=float,
        default=250,
        help='Cut on the PT for tracks in the "High-PT" category')
    parser.add_argument(
        '--set-qop',
        action='store_true',
        help=
        'Whether to modify the momentum using the MuonMatchVeloUT algorithm or not'
    )
    parser.add_argument(
        '--seq-type',
        type=str,
        default='muonmatch',
        choices=('muonmatch', 'main'),
        help='Tracking sequence to run')

    args = parser.parse_args()

    main(**vars(args))

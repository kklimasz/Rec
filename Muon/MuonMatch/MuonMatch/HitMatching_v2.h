/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_HITMATCHING_H
#define MUONMATCH_HITMATCHING_H 1

// STL
#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>

// range
#include "range/v3/view/transform.hpp"

// LHCb
#include "Event/State.h"

// from MuonDAQ
#include "MuonDAQ/CommonMuonHit.h"

namespace MuonMatch {

  /** @class Hit HitMatching.h
   *  Store the values required for the "kink" method.
   *
   *  @author Miguel Ramos Pernas
   *  @date   2017-10-05
   */
  template <class value_type>
  struct Hit {

    /// Build a hit from a MuonID hit
    Hit( const CommonMuonHit& hit )
        : x( hit.x() )
        , sx2( hit.dx() * hit.dx() / 12. )
        , y( hit.y() )
        , sy2( hit.dy() * hit.dy() / 12. )
        , z( hit.z() ) {}

    /// Build a hit extrapolating the values from a state to the given point.
    Hit( const LHCb::State& state, value_type pz ) {

      const auto dz  = pz - state.z();
      const auto dz2 = dz * dz;

      x   = state.x() + dz * state.tx();
      sx2 = state.errX2() + dz2 * state.errTx2();
      y   = state.y() + dz * state.ty();
      sy2 = state.errY2() + dz2 * state.errTy2();
      z   = pz;
    }

    value_type x, sx2, y, sy2, z;
  };

  template <class value_type>
  using Hits = std::vector<Hit<value_type>>;

  // Define the policy to create a container of hits for matching. The first
  // hit is the magnet hit, while the second is the first hit matched in
  // the sequence.
  template <class value_type>
  inline Hits<value_type> createMatchingHitsVector( const Hit<value_type>& magnet_hit,
                                                    const Hit<value_type>& first_hit ) {

    Hits<value_type> hits{magnet_hit, first_hit};
    hits.reserve( 5 );

    return hits;
  }

  /** Calculate the chi2 of a container.
   *  As an input it takes an STL container of tuples with three values. The
   *  first must be the experimental value (observed), the second the
   *  expectation and the third must correspond to the variance.
   */
  template <class Container>
  constexpr auto chi2( const Container& points ) {

    return std::accumulate( std::begin( points ), std::end( points ), 0., []( const auto prev, const auto& point ) {
      const auto [ob, th, s2] = point;

      const auto d = ob - th;

      return prev + d * d / s2;
    } );
  }

  /** Calculate straight line fit using linear-least-squares.
   *  As an input it takes an STL container of tuples with three values. These
   *  correspond to the values of abscissa, ordinate, and the squared error on
   *  the ordinate.
   *  It returns: ordinate-intercept, slope, chi-square, degrees of freedom.
   *  Code inspired by numerical recipes, section 15.2
   */
  template <class Container>
  constexpr auto fit_linear( const Container& points ) {

    // Calculate some sums
    auto S = 0., Sz = 0., Sc = 0.;
    for ( const auto [z, c, s2] : points ) {

      const auto is2 = 1. / s2;

      S += is2;
      Sz += z * is2;
      Sc += c * is2;
    }
    const auto alpha = Sz / S;

    // Calculate the estimate for the slope
    auto b = 0., Stt = 0.;
    for ( const auto [z, c, s2] : points ) {

      const auto t_i = ( z - alpha );
      const auto ts2 = t_i / s2;

      Stt += t_i * ts2;

      b += c * ts2;
    }
    b /= Stt;

    const auto a = ( Sc - Sz * b ) / S;

    // Calculate the chi2
    const auto chisq = chi2( points | ranges::v3::view::transform( [&a, &b]( const auto& point ) {
                               const auto [z, c, s2] = point;

                               return std::make_tuple( c, a + b * z, s2 );
                             } ) );

    return std::make_tuple( a, b, chisq, points.size() - 2 );
  }

} // namespace MuonMatch

#endif // MUONMATCH_HITMATCHING_H

/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <tuple>

// range
#include "range/v3/view/transform.hpp"

// from MuonMatch
#include "MuonMatchVeloUTSoA.h"

using namespace MuonMatch;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonMatchVeloUTSoA )

namespace {
  using value_type    = MuonMatchVeloUTSoA::value_type;
  using hit_type      = MuonMatchVeloUTSoA::hit_type;
  using hits_type     = MuonMatchVeloUTSoA::hits_type;
  using window_bounds = MuonMatchVeloUTSoA::window_bounds;
} // namespace

//=============================================================================
//
std::optional<hit_type> MuonMatchVeloUTSoA::findHit( MuonChamber much, const LHCb::State& state,
                                                     const hit_type& magnet_hit, const MuonHitContainer& hit_cont,
                                                     const value_type slope ) const {

  // Get the station we are looking at.
  const auto& station = hit_cont.station( much );

  const auto [xMin, xMax, yMin, yMax] = this->stationWindow( state, station, magnet_hit, slope );

  // Look for the closest hit inside the search window
  std::optional<hit_type> closest;

  value_type min_dist2 = 0;

  for ( unsigned int r = 0; r < station.nRegions(); ++r ) {

    if ( !station.overlaps( r, xMin, xMax, yMin, yMax ) ) continue;

    for ( const auto& mhit : hit_cont.hits( xMin, xMax, much, r ) ) {

      if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

      const hit_type hit{mhit};

      const auto ymuon = yStraight( state, hit.z );

      const auto resx = magnet_hit.x + ( hit.z - magnet_hit.z ) * slope - hit.x;
      const auto resy = ymuon - hit.y;

      // Variable to define the closest hit
      const auto dist2 = ( resx * resx + resy * resy ) / ( hit.sx2 + hit.sy2 );

      if ( !closest || dist2 < min_dist2 ) {
        closest.emplace( hit );
        min_dist2 = dist2;
      }
    }
  }

  return closest;
}

//=============================================================================
//
window_bounds MuonMatchVeloUTSoA::firstStationWindow( const LHCb::State& state, const CommonMuonStation& station,
                                                      const hit_type& magnet_hit ) const {

  const auto& [xw, yw] = m_window[station.station()];

  // Calculate window in y. In y I just extrapolate in a straight line.
  const auto [yMin, yMax] = yStraightWindow( state, station.z(), yw );

  // Calculate window in x
  const auto dz = ( station.z() - magnet_hit.z );

  const auto mag    = m_fieldSvc->isDown() ? -1 : +1;
  const auto charge = track_charge( state );

  const auto t = state.tx();
  const auto d = this->dtx( state );

  const auto slope = charge * mag < 0 ? t + d : t - d;

  const auto xpos = magnet_hit.x + dz * slope;
  const auto xMin = xpos - xw;
  const auto xMax = xpos + xw;

  return {xMin, xMax, yMin, yMax};
}

//=============================================================================
/* Calculate the interception, slope, chi2 and degrees of fredom when no fit
 * is performed in y. This applies when we assume the velo extrapolation is
 * correct. Thus the residuals and chi2 can be calculated with respect to that.
 * Returns the position of the magnet and the slope in the velo in the y axis,
 * chi2 and degrees of freedom.
 */
template <class Container>
constexpr auto yParamsNoFit( const Container& hits, const hit_type& magnet_hit, const LHCb::State& state ) {

  const auto chisq = chi2( hits | ranges::v3::view::transform( [&state]( const hit_type& h ) {
                             return std::make_tuple( h.y, yStraight( state, h.z ), h.sy2 );
                           } ) );

  return std::make_tuple( magnet_hit.y, state.ty(), chisq, hits.size() );
}

//=============================================================================
//
std::tuple<value_type, value_type> MuonMatchVeloUTSoA::fitHits( const hits_type& hits, const hit_type& magnet_hit,
                                                                const LHCb::State& state ) const {
  value_type x_slope, x_chi2, x_ndof;
  std::tie( std::ignore, x_slope, x_chi2, x_ndof ) =
      fit_linear( hits | ranges::v3::view::transform( [&magnet_hit]( const hit_type& h ) {
                    return std::make_tuple( h.z - magnet_hit.z, h.x, h.sx2 );
                  } ) );

  value_type y_chi2, y_ndof;
  std::tie( std::ignore, std::ignore, y_chi2, y_ndof ) =
      m_fitY ? fit_linear( hits | ranges::v3::view::transform( [&magnet_hit]( const hit_type& h ) {
                             return std::make_tuple( h.z - magnet_hit.z, h.y, h.sy2 );
                           } ) )
             : yParamsNoFit( hits, magnet_hit, state );

  const auto chi2_ndof = ( x_chi2 + y_chi2 ) / ( x_ndof + y_ndof );

  return {x_slope, chi2_ndof};
}

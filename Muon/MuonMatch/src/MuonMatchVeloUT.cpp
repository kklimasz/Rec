/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <numeric>
#include <tuple>
#include <utility>

// range
#include "range/v3/view/transform.hpp"

// local
#include "MuonMatchVeloUT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMatchVeloUT
//
// 2010-12-02 : Roel Aaij, Vasileios Syropoulos
// 2017-10-05 : Miguel Ramos Pernas
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonMatchVeloUT )

// Some typedefs
namespace {

  using namespace MuonMatch;

  using value_type    = MuonMatchVeloUT::value_type;
  using window_bounds = MuonMatchVeloUT::window_bounds;
  using hit_type      = MuonMatchVeloUT::hit_type;
  using hits_type     = MuonMatchVeloUT::hits_type;

  struct Candidate final {
    Candidate( value_type _slope, value_type _p, value_type _chi2ndof ) : slope{_slope}, p{_p}, chi2ndof{_chi2ndof} {}

    /// Slope
    value_type slope;
    /// Momentum
    value_type p;
    /// Chi2 per degrees of freedom
    value_type chi2ndof;
  };
} // namespace

//=============================================================================
//
MuonMatchVeloUT::MuonMatchVeloUT( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"InputTracks", LHCb::TrackLocation::VeloUT},
                    KeyValue{"InputMuonHits", MuonHitContainerLocation::Default}},
                   KeyValue{"OutputTracks", LHCb::TrackLocation::Match} ) {}

//=============================================================================
//
std::optional<Track> MuonMatchVeloUT::match( const Track& seed, const MuonHitContainer& hit_cont ) const {

  if ( !seed.hasUT() ) {
    error() << "Input track has no UT information (location: " << this->inputLocation<0>() << ")" << endmsg;
    return {};
  }

  // Define the track type, if its momentum is too low, stop processing
  const TrackType tt = trackTypeFromMomentum( seed.p() );

  if ( tt == VeryLowP ) return {};

  // Get the state at the Velo
  const LHCb::State* state = seed.stateAt( LHCb::State::Location::EndVelo );
  if ( !state ) {
    error() << "Input track has no EndVelo state (location: " << this->inputLocation<0>() << ")" << endmsg;
    return {};
  }

  // Parametrization of the z-position of the magnet’s focal plane as a function
  // of the direction of the velo track "tx2"
  const auto magnet_hit = this->magnetFocalPlaneFromTx2( *state );

  // To store the best candidate in "SetQOverP" mode
  std::optional<Candidate> best;

  // Function to build the output object of this function from the seed track
  auto build_output = [&seed]() {
    std::optional<Track> output{seed};
    output->addToAncestors( seed );
    output->setHistory( Track::History::MuonMatchVeloUT );

    return output;
  };

  // The iteration is done over all the possible initial stations. Only the best
  // is kept in case "SetQOverP" is true.
  for ( const auto ist : this->firstMuCh( tt ) ) {

    const CommonMuonStation& first = hit_cont.station( ist );

    const auto [xMin, xMax, yMin, yMax] = this->firstStationWindow( seed, *state, first, magnet_hit );

    for ( unsigned int r = 0; r < first.nRegions(); ++r ) {

      if ( !first.overlaps( r, xMin, xMax, yMin, yMax ) ) continue;

      for ( const auto& mhit : hit_cont.hits( xMin, xMax, ist, r ) ) {

        if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

        // Create the first proxy for the hits in the muon stations
        const hit_type hit{mhit};

        // Estimate the new slope of the track
        const auto slope = ( hit.x - magnet_hit.x ) / ( hit.z - magnet_hit.z );

        auto hits = createMatchingHitsVector( magnet_hit, hit );

        // Add hits to the seed using the muon chambers designated for the
        // given track type. Break the iteration if no hits are found
        // in one of the chambers.
        const auto muchs   = this->afterKickMuChs( tt );
        const bool is_good = std::all_of( std::cbegin( muchs ), std::cend( muchs ), [&]( const auto& much ) {
          auto h = this->findHit( much, *state, magnet_hit, hit_cont, slope );

          if ( h ) {
            hits.emplace_back( std::move( *h ) );
            return true;
          } else
            return false;
        } );

        if ( !muchs.size() || !is_good ) continue;

        // The candidate is fitted if it is a "good" candidate
        const auto& [fit_slope, p, chi2_ndof] = this->fitHits( hits, magnet_hit, *state );

        if ( m_setQOverP ) {
          // Set q/p mode, we have to find the "best" candidate
          if ( !best ) {
            // "best" candidate is not defined, check if the incoming
            // passes the threshold
            if ( chi2_ndof < m_maxChi2DoF ) best.emplace( fit_slope, p, chi2_ndof );
          } else if ( chi2_ndof < best->chi2ndof )
            // The candidate has a smaller chi2/ndof than the "best". Set
            // as new "best".
            best.emplace( fit_slope, p, chi2_ndof );
        } else {
          // Do not set q/p
          if ( chi2_ndof < m_maxChi2DoF ) {
            // A candidate is found satisfying the requirements, it is
            // directly returned
            return build_output();
          }
        }
      }
    }
  }

  if ( best ) {
    //
    // Running with SetQOverP = True
    //
    auto output = build_output();

    const auto down = m_fieldSvc->isDown() ? -1 : +1;
    const auto q    = down * ( ( best->slope < state->tx() ) - ( best->slope > state->tx() ) );

    output->setQOverPInAllStates( q / best->p );

    return output;
  } else
    return {};
}

//=============================================================================
//
std::optional<hit_type> MuonMatchVeloUT::findHit( const MuonChamber& much, const LHCb::State& state,
                                                  const hit_type& magnet_hit, const MuonHitContainer& hit_cont,
                                                  const value_type slope ) const {

  // Get the station we are looking at.
  const CommonMuonStation& station = hit_cont.station( much );

  const auto [xMin, xMax, yMin, yMax] = this->stationWindow( state, station, magnet_hit, slope );

  // Look for the closest hit inside the search window
  std::optional<hit_type> closest;

  value_type min_dist2 = 0;

  for ( unsigned int r = 0; r < station.nRegions(); ++r ) {

    if ( !station.overlaps( r, xMin, xMax, yMin, yMax ) ) continue;

    for ( const auto& mhit : hit_cont.hits( xMin, xMax, much, r ) ) {

      if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

      const hit_type hit{mhit};

      const auto ymuon = yStraight( state, hit.z );

      const auto resx = magnet_hit.x + ( hit.z - magnet_hit.z ) * slope - hit.x;
      const auto resy = ymuon - hit.y;

      // Variable to define the closest hit
      const auto dist2 = ( resx * resx + resy * resy ) / ( hit.sx2 + hit.sy2 );

      if ( !closest || dist2 < min_dist2 ) {
        closest.emplace( hit );
        min_dist2 = dist2;
      }
    }
  }

  return closest;
}

//=============================================================================
//
inline window_bounds MuonMatchVeloUT::firstStationWindow( const Track& track, const LHCb::State& state,
                                                          const CommonMuonStation& station,
                                                          const hit_type&          magnet_hit ) const {

  const auto& [xw, yw] = m_window[station.station()];

  // Calculate window in y. In y I just extrapolate in a straight line.
  const auto [yMin, yMax] = yStraightWindow( state, station.z(), yw );

  // Calculate window in x
  const auto dz = ( station.z() - magnet_hit.z );

  const auto mag    = m_fieldSvc->isDown() ? -1 : +1;
  const auto charge = track.charge();

  const auto t = state.tx();
  const auto d = this->dtx( track );

  const auto slope = charge * mag < 0 ? t + d : t - d;

  const auto xpos = magnet_hit.x + dz * slope;
  const auto xMin = xpos - xw;
  const auto xMax = xpos + xw;

  return {xMin, xMax, yMin, yMax};
}

//=============================================================================
/* Calculate the interception, slope, chi2 and degrees of fredom when no fit
 * is performed in y. This applies when we assume the velo extrapolation is
 * correct. Thus the residuals and chi2 can be calculated with respect to that.
 * Returns the position of the magnet and the slope in the velo in the y axis,
 * chi2 and degrees of freedom.
 */
template <class Container>
constexpr auto yParamsNoFit( const Container& hits, const hit_type& magnet_hit, const LHCb::State& state ) {

  const auto chisq = chi2( hits | ranges::v3::view::transform( [&state]( const hit_type& h ) {
                             return std::make_tuple( h.y, yStraight( state, h.z ), h.sy2 );
                           } ) );

  return std::make_tuple( magnet_hit.y, state.ty(), chisq, hits.size() );
}

//=============================================================================
//
std::tuple<value_type, value_type, value_type>
MuonMatchVeloUT::fitHits( const hits_type& hits, const hit_type& magnet_hit, const LHCb::State& state ) const {
  value_type x_slope, x_chi2, x_ndof;
  std::tie( std::ignore, x_slope, x_chi2, x_ndof ) =
      fit_linear( hits | ranges::v3::view::transform( [&magnet_hit]( const hit_type& h ) {
                    return std::make_tuple( h.z - magnet_hit.z, h.x, h.sx2 );
                  } ) );

  value_type y_chi2, y_ndof;
  std::tie( std::ignore, std::ignore, y_chi2, y_ndof ) =
      m_fitY ? fit_linear( hits | ranges::v3::view::transform( [&magnet_hit]( const hit_type& h ) {
                             return std::make_tuple( h.z - magnet_hit.z, h.y, h.sy2 );
                           } ) )
             : yParamsNoFit( hits, magnet_hit, state );

  const auto p = this->momentum( x_slope - state.tx() );

  const auto chi2_ndof = ( x_chi2 + y_chi2 ) / ( x_ndof + y_ndof );

  return {x_slope, p, chi2_ndof};
}

//=============================================================================
//
inline window_bounds MuonMatchVeloUT::stationWindow( const LHCb::State& state, const CommonMuonStation& station,
                                                     const hit_type& magnet_hit, const value_type slope ) const {

  const auto& [xRange, yRange] = m_window[station.station()];

  const auto yMuon = yStraight( state, station.z() );

  const auto yMin = yMuon - yRange;
  const auto yMax = yMuon + yRange;

  const auto xMuon = ( station.z() - magnet_hit.z ) * slope + magnet_hit.x;

  const auto xMin = xMuon - xRange;
  const auto xMax = xMuon + xRange;

  return {xMin, xMax, yMin, yMax};
}

/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCHVELOUTSOA_H
#define MUONMATCHVELOUTSOA_H 1

// STL
#include <algorithm>
#include <numeric>
#include <string>
#include <tuple>
#include <utility>

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// from LHCb
#include "Kernel/ILHCbMagnetSvc.h"

// from MuonDAQ
#include "MuonDAQ/MuonHitContainer.h"

// from MuonDet
#include "MuonDet/DeMuonDetector.h"

// from MuonMatch
#include "MuonMatch/HitMatching_v2.h"
#include "MuonMatch/Tracks_v2.h"

using namespace MuonMatch;

/** @class MuonMatchVeloUTSoA MuonMatchVeloUTSoA.h
 *
 *  @author Roel Aaij, Vasileios Syropoulos
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 *
 *  @author Arthur Hennequin
 *  @desc   Switch to SoA
 *  @date   2019-06-06
 */

class MuonMatchVeloUTSoA : public Gaudi::Functional::Transformer<TracksUT( const TracksUT&, const MuonHitContainer& )> {

public:
  using value_type    = double;
  using hit_type      = Hit<value_type>;
  using hits_type     = Hits<value_type>;
  using window_bounds = std::tuple<value_type, value_type, value_type, value_type>;

  /// Definition of the track types
  enum TrackType { VeryLowP, LowP, MediumP, HighP };

  /// Definition of the muon chamber indices.
  enum MuonChamber { M2, M3, M4, M5, NumberOfStations };

  /// Sequence of muon chambers
  template <MuonChamber... Chambers>
  struct MuChSeq {};

  /// Stores the slope and the chi2 over degrees of freedom after a fit
  struct Candidate final {

    Candidate( value_type _slope, value_type _chi2ndof ) : slope{_slope}, chi2ndof{_chi2ndof} {}

    /// Slope
    value_type slope;
    /// Chi2 per degrees of freedom
    value_type chi2ndof;
  };

  MuonMatchVeloUTSoA( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            {KeyValue{"InputTracks", "Rec/Track/UT"}, KeyValue{"InputMuonHits", MuonHitContainerLocation::Default}},
            KeyValue{"OutputTracks", LHCb::TrackLocation::Match} ) {}

  /// Initialize the instance
  StatusCode initialize() override {
    // Initialize transformer
    auto sc = Transformer::initialize();
    if ( sc.isFailure() ) return sc;

    // Initialize magnetic field service
    m_fieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

    // Check the number of muon stations in the detector description
    DeMuonDetector* mudet = getDet<DeMuonDetector>( DeMuonLocation::Default );
    if ( mudet->stations() < NumberOfStations ) {
      fatal() << "Algorithm expects at least four muon stations" << endmsg;
      return StatusCode::FAILURE;
    } else if ( mudet->stations() > NumberOfStations )
      warning() << "Algorithm is optimized to work with four muon stations" << endmsg;

    return StatusCode::SUCCESS;
  }

  /// Gaudi-functional method
  TracksUT operator()( const TracksUT& seeds, const MuonHitContainer& hit_cont ) const override {
    m_seedCount += seeds.size();

    size_t nhits = 0;
    for ( unsigned int s = M2; s < NumberOfStations; ++s ) {

      const auto& station = hit_cont.station( s );

      for ( unsigned int r = 0; r < station.nRegions(); ++r ) nhits += station.hits( r ).size();
    }

    m_hitCount += nhits;

    TracksUT matched( seeds.getVeloAncestors() );

    for ( int t = 0; t < seeds.size(); t += simd::size ) {
      auto state = get_state( seeds, t );
      auto mask  = this->matchByTrackType( state, hit_cont );

      matched.copy_back<simd>( seeds, t, mask );

      if ( m_setQOverP ) matched.compressstore_stateQoP<simd::float_v>( matched.size() - 1, mask, state.qOverP() );
    }

    m_matchCount += matched.size();

    return matched;
  }

private:
  /// Magnetic field service
  ILHCbMagnetSvc* m_fieldSvc = nullptr;

  /// Counter for the number of input muon hits
  mutable Gaudi::Accumulators::AveragingCounter<> m_hitCount{this, "#hits"};

  /// Counter for the number of input seeds
  mutable Gaudi::Accumulators::AveragingCounter<> m_seedCount{this, "#seeds"};

  /// Counter for the matched tracks
  mutable Gaudi::Accumulators::AveragingCounter<> m_matchCount{this, "#matched"};

  // Values to use in the "kick" method
  Gaudi::Property<value_type> m_kickOffset{this, "KickOffset", 338.92 * Gaudi::Units::MeV};
  Gaudi::Property<value_type> m_kickScale{this, "KickScale", 1218.62 * Gaudi::Units::MeV};

  // Whether to set the value of q/p of the track after matching
  Gaudi::Property<bool> m_setQOverP{this, "SetQOverP", false};

  // Properties of the magnet focal plane
  Gaudi::Property<value_type> m_za{this, "MagnetPlaneParA", +5.331 * Gaudi::Units::m};
  Gaudi::Property<value_type> m_zb{this, "MagnetPlaneParB", -0.958 * Gaudi::Units::m};

  // Definition of the search windows for each muon station
  Gaudi::Property<std::array<std::pair<value_type, value_type>, NumberOfStations>> m_window{
      this,
      "SearchWindows",
      {std::pair<value_type, value_type>{500 * Gaudi::Units::mm, 400 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{600 * Gaudi::Units::mm, 500 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{700 * Gaudi::Units::mm, 600 * Gaudi::Units::mm},
       std::pair<value_type, value_type>{800 * Gaudi::Units::mm, 700 * Gaudi::Units::mm}}};

  // Values for the momentum bounds defining the track types
  Gaudi::Property<std::array<value_type, 3>> m_momentumBounds{
      this,
      "MomentumBounds",
      {2.5 * Gaudi::Units::GeV, 7 * Gaudi::Units::GeV, 12 * Gaudi::Units::GeV}}; // 3, 6 and 10 GeV/c for offline muons

  // General properties
  Gaudi::Property<value_type> m_maxChi2DoF{this, "MaxChi2DoF", 20};
  Gaudi::Property<bool>       m_fitY{this, "FitY", false};

  /// Get the first estimation of the magnet focal plane position from "tx2".
  inline hit_type magnetFocalPlaneFromTx2( const LHCb::State& state ) const {
    const auto z = m_za + m_zb * state.tx() * state.tx();

    return hit_type{state, z};
  }

  /// Calculate the slope using the minimum momentum
  inline auto dtx( const LHCb::State& state ) const { return m_kickScale / ( state.p() - m_kickOffset ); }

  /// Add hits to the given candidate and return whether it can be fitted
  std::optional<hit_type> findHit( MuonChamber much, const LHCb::State& state, const hit_type& magnet_hit,
                                   const MuonHitContainer& hit_cont, const value_type slope ) const;

  /** Calculate the window to search in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  window_bounds firstStationWindow( const LHCb::State& state, const CommonMuonStation& station,
                                    const hit_type& hit ) const;

  /// Fit the given hits, returning the slope of the fitted track, momentum and chi2/ndof
  std::tuple<value_type, value_type> fitHits( const hits_type& hits, const hit_type& magnetHit,
                                              const LHCb::State& state ) const;

  /// Match the given tracks to the seed state
  template <MuonChamber... First, MuonChamber... Last>
  bool match( LHCb::State& state, const MuonHitContainer& hit_cont, MuChSeq<First...>&&,
              MuChSeq<Last...>&& last ) const;

  /** Match tracks to muon hits switching by momentum range.
   *
   * This function defines which stations the algorithm has to look at for each
   * track type.
   */
  bool matchByTrackType( LHCb::State& state, const MuonHitContainer& hit_cont ) const {
    // Define the track type, if its momentum is too low, stop processing
    const TrackType tt = trackTypeFromMomentum( state.p() );

    switch ( tt ) {

    case ( LowP ):
      return this->match( state, hit_cont, MuChSeq<M3>{}, MuChSeq<M2>{} );
    case ( MediumP ):
      return this->match( state, hit_cont, MuChSeq<M5, M4>{}, MuChSeq<M3, M2>{} );
    case ( HighP ):
      return this->match( state, hit_cont, MuChSeq<M5>{}, MuChSeq<M4, M3, M2>{} );
    default: // VeryLowP
      return false;
    };
  }

  /// Get the momentum given the slope in "x"
  inline auto momentum( value_type dtx ) const { return m_kickScale / std::fabs( dtx ) + m_kickOffset; }

  template <MuonChamber... Last>
  std::optional<Candidate> searchInLastChambers( const LHCb::State& state, const MuonHitContainer& hit_cont,
                                                 const hit_type& magnet_hit, MuonChamber first,
                                                 const MuChSeq<Last...>& ) const;

  /** Calculate the window to search in the given station.
   *  Not to be applied in the first station.
   *  Returns "xmin", "xmax", "ymin", "ymax".
   */
  inline window_bounds stationWindow( const LHCb::State& state, const CommonMuonStation& station,
                                      const hit_type& magnet_hit, const value_type slope ) const {

    const auto [xRange, yRange] = m_window[station.station()];

    const auto yMuon = yStraight( state, station.z() );

    const auto yMin = yMuon - yRange;
    const auto yMax = yMuon + yRange;

    const auto xMuon = ( station.z() - magnet_hit.z ) * slope + magnet_hit.x;

    const auto xMin = xMuon - xRange;
    const auto xMax = xMuon + xRange;

    return {xMin, xMax, yMin, yMax};
  }

  /// Return the track type associated to a given momentum
  template <class value>
  inline TrackType trackTypeFromMomentum( value p ) const {

    if ( p < m_momentumBounds[0] )
      return VeryLowP;
    else if ( p < m_momentumBounds[1] )
      return LowP;
    else if ( p < m_momentumBounds[2] )
      return MediumP;
    else
      return HighP;
  }
};

namespace {
  using Candidate   = MuonMatchVeloUTSoA::Candidate;
  using MuonChamber = MuonMatchVeloUTSoA::MuonChamber;
} // namespace

//=============================================================================
//
template <MuonChamber... First, MuonChamber... Last>
bool MuonMatchVeloUTSoA::match( LHCb::State& state, const MuonHitContainer& hit_cont, MuChSeq<First...>&&,
                                MuChSeq<Last...>&& last ) const {

  // Parametrization of the z-position of the magnet’s focal plane as a function
  // of the direction of the velo track "tx2"
  const auto magnet_hit = this->magnetFocalPlaneFromTx2( state );

  // The iteration is done over all the possible initial stations. Only the best
  // is kept in case "SetQOverP" is true.
  std::optional<Candidate> best;

  ( ( best = searchInLastChambers( state, hit_cont, magnet_hit, First, last ) ) || ... );

  if ( best && m_setQOverP ) {

    const auto down = m_fieldSvc->isDown() ? -1 : +1;
    const auto dtx  = best->slope - state.tx();
    const auto q    = down * ( ( dtx < 0 ) - ( dtx > 0 ) );

    const auto p = this->momentum( dtx );

    state.setQOverP( q / p );
  }

  return best ? true : false;
}

//=============================================================================
//
template <MuonChamber... Last>
std::optional<Candidate> MuonMatchVeloUTSoA::searchInLastChambers( const LHCb::State&      state,
                                                                   const MuonHitContainer& hit_cont,
                                                                   const hit_type& magnet_hit, MuonChamber first,
                                                                   const MuChSeq<Last...>& ) const {

  const CommonMuonStation& first_station = hit_cont.station( first );

  const auto [xMin, xMax, yMin, yMax] = this->firstStationWindow( state, first_station, magnet_hit );

  std::optional<Candidate> cand;

  for ( unsigned int r = 0; r < first_station.nRegions(); ++r ) {

    if ( !first_station.overlaps( r, xMin, xMax, yMin, yMax ) ) continue;

    for ( const auto& mhit : hit_cont.hits( xMin, xMax, first, r ) ) {

      if ( mhit.y() > yMax || mhit.y() < yMin ) continue;

      // Create the first proxy for the hits in the muon stations
      const hit_type hit{mhit};

      // Estimate the new slope of the track
      const auto slope = ( hit.x - magnet_hit.x ) / ( hit.z - magnet_hit.z );

      auto hits = createMatchingHitsVector( magnet_hit, hit );

      // Add hits to the seed using the muon chambers designated for the
      // given track type. Break the iteration if no hits are found
      // in one of the chambers.
      auto check = [&]( auto much ) {
        auto h = this->findHit( much, state, magnet_hit, hit_cont, slope );

        if ( h ) {
          hits.emplace_back( std::move( *h ) );
          return true;
        }

        return false;
      };

      const auto is_good = ( check( Last ) && ... );

      if ( !is_good ) continue;

      // The candidate is fitted if it is a "good" candidate
      const auto [fit_slope, chi2_ndof] = this->fitHits( hits, magnet_hit, state );

      if ( chi2_ndof < m_maxChi2DoF ) {

        if ( !cand ) {
          // "best" candidate is not defined, check if the incoming passes the threshold
          cand.emplace( fit_slope, chi2_ndof );

          if ( !m_setQOverP )
            // If it is not running in SetQOverP = True, it must stop since it found a good candidate
            return cand;
        } else if ( chi2_ndof < cand->chi2ndof )
          // The candidate has a smaller chi2/ndof than the "best". Set as new "best".
          cand.emplace( fit_slope, chi2_ndof );
      }
    }
  }

  return cand;
}

#endif // MUONMATCHVELOUTSOA_H

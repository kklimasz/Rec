###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: MuonInterfaces
################################################################################
gaudi_subdir(MuonInterfaces v2r0)

gaudi_depends_on_subdirs(Det/MuonDet
                         Event/DAQEvent
                         Event/RecEvent
                         GaudiKernel
                         Kernel/LHCbKernel
                         Muon/MuonKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(MuonInterfacesLib
                  src/Lib/*.cpp
                  PUBLIC_HEADERS MuonInterfaces
                  LINK_LIBRARIES MuonDetLib DAQEventLib RecEvent GaudiKernel LHCbKernel MuonKernelLib)

gaudi_add_dictionary(MuonInterfaces
                     dict/MuonInterfacesDict.h
                     dict/MuonInterfacesDict.xml
                     LINK_LIBRARIES MuonDetLib DAQEventLib RecEvent GaudiKernel LHCbKernel MuonKernelLib MuonInterfacesLib
                     OPTIONS "-U__MINGW32__")


/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONINTERFACES_IMUONTRACKMOMREC_H
#define MUONINTERFACES_IMUONTRACKMOMREC_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from LHCb
#include "Event/Track.h"

class MuonTrack;

/** @class IMuonTrackMomRec IMuonTrackMomRec.h MuonInterfaces/IMuonTrackMomRec.h
 *
 *
 *  @author Giacomo Graziani
 *  @date   2010-02-10
 */
struct IMuonTrackMomRec : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IMuonTrackMomRec, 3, 0 );

  virtual std::unique_ptr<LHCb::Track> recMomentum( MuonTrack& track ) const = 0;
  virtual double                       getBdl() const                        = 0;
  virtual double                       getZcenter() const                    = 0;
};
#endif // MUONINTERFACES_IMUONTRACKMOMREC_H

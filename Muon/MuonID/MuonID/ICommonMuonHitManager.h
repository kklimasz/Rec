/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ICOMMONMUONHITMANAGER_H_
#define ICOMMONMUONHITMANAGER_H_

#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"

struct ICommonMuonHitManager : extend_interfaces<IAlgTool, IIncidentListener> {
  DeclareInterfaceID( ICommonMuonHitManager, 2, 0 );

  virtual CommonMuonHitRange hits( float xmin, unsigned int station, unsigned int region )             = 0;
  virtual CommonMuonHitRange hits( float xmin, float xmax, unsigned int station, unsigned int region ) = 0;

  virtual unsigned int nRegions( unsigned int station ) const = 0;

  virtual const CommonMuonStation& station( unsigned int id ) const = 0;
};

#endif // ICOMMONMUONHITMANAGER_H_

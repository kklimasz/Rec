/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHI2MUIDTOOL_H
#define CHI2MUIDTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "ICLTool.h"
#include "IGetArrival.h"
#include "IIsMuonCandidateC.h"
#include "ImuIDTool.h"

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "MuonDet/MuonBasicGeometry.h"

#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"

#include "SmartMuonMeasProvider.h"

/** @class Chi2MuIDTool Chi2MuIDTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */
class Chi2MuIDTool : public extends1<GaudiTool, ImuIDTool> {
public:
  /// Standard constructor
  Chi2MuIDTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode isGoodSeed( const LHCb::Track& seed ) override;

  StatusCode muonCandidate( const LHCb::Track& seed, LHCb::Track& muTrack,
                            const std::vector<LHCb::LHCbID> ids_init = std::vector<LHCb::LHCbID>() ) override;

  StatusCode muonQuality( LHCb::Track& muTrack, double& Quality ) override;

  StatusCode muonArrival( const LHCb::Track& muTrack, double& Arrival ) const override;

  StatusCode muonDLL( const LHCb::Track& muTrack, const double& Quality, double& CLQuality, const double& Arrival,
                      double& CLArrival, double& DLL ) const override;

  StatusCode muonID( const LHCb::Track& seed, LHCb::Track& muTrack, double& Quality, double& CLQuality, double& Arrival,
                     double& CLArrival, double& DLL,
                     const std::vector<LHCb::LHCbID> ids_init = std::vector<LHCb::LHCbID>() ) override;

  bool isTrackInAcceptance( const LHCb::Track& seed ) override;

  StatusCode findTrackRegions( const LHCb::Track& muTrack, std::vector<int>& trackRegion ) override;

protected:
  std::vector<double>      m_zstations;
  std::vector<LHCb::State> m_states;
  IMeasurementProvider*    m_measProvider = nullptr;
  SmartMuonMeasProvider*   m_muonProvider = nullptr;
  StatusCode               makeStates( const LHCb::Track& seed );

  /// Momentum ranges: different treatement of M4/M5 in each
  Gaudi::Property<std::vector<double>> m_MomentumCuts{
      this, "MomentumCuts", {}, "Different depths of stations considered in different momentum ranges"};

  Gaudi::Property<float> m_PreSelMomentum{this, "PreSelMomentum", 3000.,
                                          "Preselection momentum (no attempt to ID below this)"};

private:
  bool       isTrackInsideStation( int istation ) const;
  void       addLHCbIDsToMuTrack( LHCb::Track& muTrack, double mom ) const;
  StatusCode search( const LHCb::Track& seed, LHCb::Track& muTrack );

  ITrackFitter*       m_fitter       = nullptr;
  ITrackExtrapolator* m_extrapolator = nullptr;

  IIsMuonCandidateC* m_IsMuonTool = nullptr;
  ICLTool*           m_CLQuality  = nullptr;
  IGetArrival*       m_GetArrival = nullptr;

  DeMuonDetector* m_mudet = nullptr;

  Gaudi::Property<float> m_nsigmas{this, "NSigmas", 5., "Number of sigmas for FOI"};
  double                 m_nsigmasUsed = 0.;
  Gaudi::Property<int>   m_discrval{
      this, "DiscrValue", 1,
      "Discrimination value to select hits in FOI: (1) chi2, (2) chi2x, (3) chi2y, (4) distance, (5) distx, (6) disty"};

  int m_NStation = 0;

  Gaudi::Property<bool> m_useBkg{this, "UseBkg", false, "Use background and return clratio?"};

  LHCb::State* m_mySeedState = nullptr;

  Gaudi::Property<float> m_2hits{this, "MinMomSt3", 3500, "Min momentum to accept hits in stations 3 and 4"};
  Gaudi::Property<float> m_3hits{this, "MinMomSt4", 4500, "Min momentum to accept hits in station 4"};
  Gaudi::Property<float> m_chi2cut{this, "Chi2Cut", 0., "Use chi2cut when selecting candidates?"};

  bool m_arrivalCuts = false;

  bool m_applyIsMuon = false;

  std::vector<double> m_MomRangeIsMuon;

  std::vector<int> m_stations_acceptance;

  // GP check if M1 exists
  int m_isM1defined = 0;
  //
};

#endif // MUIDTOOL_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <algorithm>

// local
#include "NShared.h"

//-----------------------------------------------------------------------------
// Implementation file for class : NShared
//
// 2011-03-17 : Xabier Cid Vidal
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( NShared )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
NShared::NShared( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareInterface<INShared>( this );
}

// from ChargedProtoParticleAddMuonInfo
StatusCode NShared::mapMuonPIDs() {
  // empty the map
  m_muonMap.clear();

  // Do we have any MuonPID results
  if ( !exist<LHCb::MuonPIDs>( m_MuonPIDsPath ) ) {
    Warning( "No MuonPIDs at '" + m_MuonPIDsPath + "' -> ProtoParticles will not be changed.", StatusCode::SUCCESS, 1 )
        .ignore();
    return StatusCode::FAILURE;
  }

  // yes, so load them
  const LHCb::MuonPIDs* muonpids = get<LHCb::MuonPIDs>( m_MuonPIDsPath );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Successfully loaded " << muonpids->size() << " MuonPIDs from " << m_MuonPIDsPath << endmsg;
  }

  // refresh the reverse mapping
  std::transform( muonpids->begin(), muonpids->end(), std::inserter( m_muonMap, m_muonMap.end() ),
                  []( LHCb::MuonPID* mpid ) { return std::make_pair( mpid->idTrack(), mpid ); } );

  return StatusCode::SUCCESS;
}

// from ChargedProtoParticleAddMuonInfo
LHCb::MuonPID* NShared::mupidFromTrack( const LHCb::Track& mother ) const {
  // Does this track have a MUON PID result ?
  auto iM = m_muonMap.find( &mother );
  return m_muonMap.end() != iM ? iM->second : nullptr;
}

StatusCode NShared::getNShared( const LHCb::Track& mother, int& nshared ) {

  // if m_muonmap not loaded, do so
  if ( !m_muonMap.size() ) {
    StatusCode sc = mapMuonPIDs();
    if ( sc.isFailure() ) return sc;
  }

  const LHCb::MuonPID* mymupid = mupidFromTrack( mother );
  if ( !mymupid ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "No MuonPID found when computing NShared" << endmsg;
    return StatusCode::SUCCESS;
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: MuonPID retrieved " << ( *mymupid ) << endmsg;

  // if not a muon candidate do nothing
  if ( !mymupid->IsMuonLoose() ) {
    nshared = 100;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: Track with IsMuonLoose false" << endmsg;
    return StatusCode::SUCCESS;
  }

  const LHCb::Track* mymutrack = mymupid->muonTrack();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: MuonTrack retrieved " << ( *mymutrack ) << endmsg;
  nshared      = 0;
  double dist1 = mymutrack->info( 305, -1. );
  if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: dist1=" << dist1 << endmsg;

  // if distance badly calculated: bad track!
  if ( dist1 < 0 ) {
    nshared = 100;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: Track with wrong distance stored" << endmsg;
    return StatusCode::SUCCESS;
  }

  LHCb::Tracks* trTracks    = get<LHCb::Tracks>( m_TracksPath );
  int           comp_tracks = 0;

  for ( auto iTrack = trTracks->begin(); iTrack != trTracks->end(); iTrack++ ) {
    if ( ( *iTrack )->checkFlag( LHCb::Track::Flags::Clone ) ) continue;
    if ( mother.checkType( LHCb::Track::Types::Long ) && ( !( *iTrack )->checkType( LHCb::Track::Types::Long ) ) )
      continue;
    if ( mother.checkType( LHCb::Track::Types::Ttrack ) && ( !( *iTrack )->checkType( LHCb::Track::Types::Ttrack ) ) )
      continue;
    if ( mother.checkType( LHCb::Track::Types::Downstream ) &&
         ( !( *iTrack )->checkType( LHCb::Track::Types::Downstream ) ) )
      continue;

    comp_tracks++;

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "getNShared: comp_tracks=" << comp_tracks << endmsg;
      debug() << "iTrack=" << ( *( *iTrack ) ) << endmsg;
    }

    // if not muon candidate or same candidate: nothing to compare!
    const LHCb::MuonPID* mupid = mupidFromTrack( *( *iTrack ) );
    if ( !mupid ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "No MuonPID found when computing NShared" << endmsg;
      continue;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: mupid=" << ( *mupid ) << endmsg;
    if ( !mupid->IsMuonLoose() ) continue;
    if ( mupid == mymupid ) continue;

    if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: comparable track" << endmsg;
    const LHCb::Track* mutrack = mupid->muonTrack();

    // see if there are shared hits between the muonIDs
    if ( !compareHits( *mymutrack, *mutrack ) ) continue;

    if ( msgLevel( MSG::DEBUG ) ) debug() << "getNShared: tracks share a hit!" << endmsg;
    double dist2 = mutrack->info( 305, -1. );
    // if distance badly calculated: nothing to compare!
    if ( dist2 < 0 ) continue;

    // the muonID which gets the number of shared hits is the one
    // which has the biggest dist
    if ( dist2 < dist1 ) nshared += 1;
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "nShared=  " << nshared << endmsg;

  return StatusCode::SUCCESS;
}

void NShared::fillNShared() {
  StatusCode sc = mapMuonPIDs();
  if ( sc.isFailure() ) return;

  for ( const auto& trk : *get<LHCb::Tracks>( m_TracksPath ) ) {
    // in the clone killed output we want only
    // unique && (matched || forward || downstream)
    if ( !trk->checkFlag( LHCb::Track::Flags::Clone ) &&
         ( trk->checkType( LHCb::Track::Types::Long ) ||
           ( trk->checkType( LHCb::Track::Types::Ttrack ) && m_useTtrack ) ||
           trk->checkType( LHCb::Track::Types::Downstream ) ) ) {

      int nshared = -1;
      sc          = getNShared( *trk, nshared );
      if ( UNLIKELY( sc.isFailure() ) ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Error getting Nshared for track " << ( *trk ) << endmsg;
      } else {
        LHCb::MuonPID* pMuid = mupidFromTrack( *trk );
        pMuid->setNShared( nshared );
      }
    }
  }
}

bool NShared::compareHits( const LHCb::Track& muTrack1, const LHCb::Track& muTrack2 ) {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "comparing_hits=" << endmsg;
  const auto& ids1 = muTrack1.lhcbIDs();
  const auto& ids2 = muTrack2.lhcbIDs();
  // TODO: use the fact that ids1 and ids2 are both ordered, so we can do this
  //      in linear time instead of quadratic...
  return std::any_of( ids1.begin(), ids1.end(), [&]( const LHCb::LHCbID& id1 ) {
    return id1.isMuon() && id1.muonID().station() != 0 &&
           std::any_of( ids2.begin(), ids2.end(), [&]( const LHCb::LHCbID& id2 ) {
             return id2.isMuon() && id2.muonID().station() != 0 && id1.channelID() == id2.channelID();
           } );
  } );
}

//=============================================================================

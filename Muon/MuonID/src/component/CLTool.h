/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CLTOOL_H
#define CLTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "ICLTool.h" // Interface
#include "Interpolator.h"

/** @class CLTool CLTool.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */
class CLTool : public extends<GaudiTool, ICLTool> {
public:
  /// Standard constructor
  CLTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode cl( double value, double& cls, double& clb, double& clr, double mom = 0., int region = 0 ) const override;

  StatusCode finalize() override {
    m_init.ignore();
    return GaudiTool::finalize();
  } // tool finalization

private:
  double minRange() const override { return m_minMomentum; }
  double maxRange() const override { return 1000000.; }

  enum class sb { sig, bkg };

  StatusCode findMomRange( const double& mom, int& p_r, sb sig_bkg ) const;
  double     valFromUnif( double value, double mom, int p_r, sb sig_bkg ) const;

  StatusCode getClValues( sb sig_bkg );

  Gaudi::Property<std::string> m_leftRight{this, "LeftRight", {}, "Decide wether to integrate to right or left"};

  Gaudi::Property<bool> m_overlap{this, "Overlap", true, "Decide if you apply interpolation between momentum bins"};

  Gaudi::Property<bool> m_applyLast{this, "applyOvlapLast", false,
                                    "Apply interpolation after center of last mom bin. May induce negative values!"};

  int m_nrange       = 0;
  int m_nrangeNmuons = 0;

  Gaudi::Property<std::vector<double>> m_range{this, "Range", {-1.}, "Get momentum bins for signal distributions"};

  Gaudi::Property<std::vector<double>> m_rangeNmuons{
      this, "RangeNmuons", {-1.}, "Get momentum bins for background distributions"};

  std::vector<double> m_mombinsCenter;
  std::vector<double> m_mombinsCenterNmuons;
  std::vector<double> m_yvals;

  int    m_nvals       = 0;
  double m_minMomentum = 0.;

  Gaudi::Property<float> m_lbinCenter{this, "lbinCenter", 0., "Last mom bin for signal: set center"};

  Gaudi::Property<float> m_lbinCenterNmuons{this, "lbinCenterNmuons", 0., "Last mom bin for background: set center"};

  Gaudi::Property<std::vector<double>> m_signal{this, "Signal", {}, "Get distributions for signal"};

  Gaudi::Property<std::vector<double>> m_bkg{this, "Bkg", {}, "Get distributions for background"};

  std::vector<std::vector<double>> m_vsignal;
  std::vector<std::vector<double>> m_vbkg;

  std::vector<Uniformer> m_unifsignal;
  std::vector<Uniformer> m_unifbkg;

  StatusCode m_init = StatusCode::FAILURE;
};
#endif // CLTOOL_H

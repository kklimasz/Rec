/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GETARRIVAL_H
#define GETARRIVAL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "IGetArrival.h" // Interface
#include "Interpolator.h"

/** @class GetArrival GetArrival.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */

class GetArrival : public GaudiTool, virtual public IGetArrival {
public:
  /// Standard constructor
  GetArrival( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode getStationsFromTrack( const LHCb::Track& mutrack, std::vector<int>& sts, const bool opt = true );

  double findProbAllStations( std::vector<int> sts, const double mom );

  double findProbStation( const int st, const double mom );

  std::vector<int> getStationsFromTrack( const LHCb::Track& mutrack );

  StatusCode getArrivalFromTrack( const LHCb::Track& mutrack, double& parr ) override;

  bool stInStations( const int myst, const std::vector<int>& stations );

  double probTypeSt( const double p, const std::vector<int>& type_st );

  int countArray( const std::vector<int>& type_st, const int up_to, const int el );

  double probTypeStStation( const double p, const std::vector<int>& type_st, const int station );

  StatusCode clArrival( const LHCb::Track& muTrack, double& clarr ) override;

  StatusCode clArrival( const double p, const std::vector<int>& type_st, double& clarr ) override;

  StatusCode finalize() override {
    m_init.ignore();
    return GaudiTool::finalize();
  } // tool finalization

protected:
  Gaudi::Property<bool> m_removeSmallest{this, "removeSmallest", false,
                                         "remove smallest amongst all probabilities for 4 stations"};

  Gaudi::Property<bool> m_useFunct{this, "useFunct", false, "use fitted functions instead of histograms"};

  StatusCode m_init;

  Gaudi::Property<std::vector<double>> m_alpha{this, "alpha", {-1.}, "parameter alpha in fitted curves"};
  Gaudi::Property<std::vector<double>> m_beta{this, "beta", {-1.}, "parameter beta in fitted curves"};
  Gaudi::Property<std::vector<double>> m_probs{this, "probs", {}, "probs corresponding to moms in each station"};
  Gaudi::Property<std::vector<double>> m_moms{this, "moms", {-1.}, "moms for all 4 stations"};

  Gaudi::Property<float> m_eff{this, "eff", .99, "eff of muon chambers"};
  Gaudi::Property<int>   m_minhits{this, "MinHits", 2, "number of min hits for cls arr"};

  std::vector<std::vector<double>> m_vprobs;

  std::vector<Uniformer> m_functprobs;

  std::vector<std::vector<int>> m_types_st;

private:
};
#endif // CLTOOL_H

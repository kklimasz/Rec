/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of PrepareMuonHits.
 * Transformer supposed to take muon coord as input and
 * muon hits as output
 **/
#include "boost/container/static_vector.hpp"
#include "boost/numeric/conversion/cast.hpp"
#include <algorithm>

#include "PrepareMuonHits.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrepareMuonHits )

namespace {
  // X region boundaries
  // Define the regions, the last ones are increased a little to make sure
  // everything is caught.
  constexpr std::array<std::array<double, 8>, 5> s_xRegions{
      {{-3940 * Gaudi::Units::mm, -1920 * Gaudi::Units::mm, -960 * Gaudi::Units::mm, -480 * Gaudi::Units::mm,
        480 * Gaudi::Units::mm, 960 * Gaudi::Units::mm, 1920 * Gaudi::Units::mm, 3940 * Gaudi::Units::mm},
       {-4900 * Gaudi::Units::mm, -2400 * Gaudi::Units::mm, -1200 * Gaudi::Units::mm, -600 * Gaudi::Units::mm,
        600 * Gaudi::Units::mm, 1200 * Gaudi::Units::mm, 2400 * Gaudi::Units::mm, 4900 * Gaudi::Units::mm},
       {-5252 * Gaudi::Units::mm, -2576 * Gaudi::Units::mm, -1288 * Gaudi::Units::mm, -644 * Gaudi::Units::mm,
        644 * Gaudi::Units::mm, 1288 * Gaudi::Units::mm, 2576 * Gaudi::Units::mm, 5252 * Gaudi::Units::mm},
       {-5668 * Gaudi::Units::mm, -2784 * Gaudi::Units::mm, -1392 * Gaudi::Units::mm, -696 * Gaudi::Units::mm,
        696 * Gaudi::Units::mm, 1392 * Gaudi::Units::mm, 2784 * Gaudi::Units::mm, 5668 * Gaudi::Units::mm},
       {-6052 * Gaudi::Units::mm, -2976 * Gaudi::Units::mm, -1488 * Gaudi::Units::mm, -744 * Gaudi::Units::mm,
        744 * Gaudi::Units::mm, 1488 * Gaudi::Units::mm, 2976 * Gaudi::Units::mm, 6052 * Gaudi::Units::mm}}};
} // namespace

PrepareMuonHits::PrepareMuonHits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"CoordLocation", LHCb::MuonCoordLocation::MuonCoords},
                   KeyValue{"Output", MuonHitHandlerLocation::Default} ) {}

//=============================================================================
// Initialization
//=============================================================================

StatusCode PrepareMuonHits::initialize() {
  StatusCode sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  m_muonDet = getDet<DeMuonDetector>( DeMuonLocation::Default );

  m_nStations = boost::numeric_cast<size_t>( m_muonDet->stations() );
  assert( m_nStations <= 5 );

  // Number of regions should be 5 for Run II and 4 or 5 for upgrade
  if ( s_xRegions.size() != 5 && s_xRegions.size() != m_nStations ) {
    return Error( std::string{"XRegions defined for "} + std::to_string( s_xRegions.size() ) + " stations, should be " +
                      ( m_nStations == 4 ? "5 (M1 will be ignored) or 4." : "5." ),
                  StatusCode::FAILURE );
  }
  for ( size_t i = 0; i < s_xRegions.size(); ++i ) {
    const auto& bounds = s_xRegions[i];
    if ( bounds.size() <= 1 || ( bounds.size() % 2 != 0 ) ) {
      return Error( "Number of X regions for station " + std::to_string( i ) + " should be even and at least 2",
                    StatusCode::FAILURE );
    }
  }
  return sc;
}

MuonHitHandler PrepareMuonHits::operator()( const LHCb::MuonCoords& muonCoords ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "==> Execute" << endmsg;
    debug() << "Detector version used: " << m_muonDet->version() << endmsg;
  }
  boost::container::static_vector<int, 5> nCoords( m_nStations, 0 );
  for ( const auto* coord : muonCoords ) { ++nCoords[coord->key().station()]; }
  boost::container::static_vector<CommonMuonHits, 5> hits( m_nStations );
  for ( unsigned int station = 0; station < m_nStations; ++station ) { hits[station].reserve( nCoords[station] ); }
  auto badTile = m_badTile.buffer();
  for ( const auto* coord : muonCoords ) {
    unsigned int station = coord->key().station();
    double       x = 0., dx = 0., y = 0., dy = 0., z = 0., dz = 0.;
    StatusCode   sc = m_muonDet->Tile2XYZ( coord->key(), x, dx, y, dy, z, dz );
    badTile += sc.isFailure();
    if ( sc.isFailure() ) continue;
    hits[station].emplace_back( coord->key(), x, dx, y, dy, z, dz, coord->uncrossed(), coord->digitTDC1(),
                                coord->digitTDC1() - coord->digitTDC2() );
  }
  std::array<CommonMuonStation, 5> stations;
  for ( unsigned int station = 0; station < m_nStations; ++station ) {
    // create a MuonHitHandler to be returned and stored in the TES
    // the s_xRegions property has all regions starting from M1.
    // For the upgrade nStations == 4, and M1 has been removed, so use
    // 1-5, for Run II use 0-5
    auto region       = s_xRegions.size() - m_nStations + station;
    stations[station] = CommonMuonStation{m_muonDet, station, s_xRegions[region], std::move( hits[station] )};
  }
  return MuonHitHandler{std::move( stations )};
}

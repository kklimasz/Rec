/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of MuonIDAlgLite.
 *
 * 2015-06-01: Kevin Dungs, Ricardo Vazquez Gomez
 * Updated to functional:
 * 2017-01-26: Holger Stevens, Martin Bieker, Julian Surmann
 * 2017-??-??: Roel Aaij
 * 2017-12-12: Nikita Kazeev
 */
#include "MuonIDAlgLite.h"

DECLARE_COMPONENT( MuonIDAlgLite )

/** Load the CommonMuonTool.
 */
MuonIDAlgLite::MuonIDAlgLite( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        // Inputs
                        {KeyValue{"TracksLocations", LHCb::TrackLocation::Default},
                         KeyValue{"HitHandler", MuonHitHandlerLocation::Default}},
                        // Outputs
                        {KeyValue{"MuonIDLocation", LHCb::MuonPIDLocation::Default},
                         KeyValue{"MuonTrackLocation", LHCb::TrackLocation::Muon}} ) {}

StatusCode MuonIDAlgLite::initialize() {
  auto sc = MultiTransformer::initialize();
  if ( sc.isFailure() ) return sc;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  muonTool_     = tool<ICommonMuonTool>( "CommonMuonTool" );
  DLLTool_      = tool<DLLMuonTool>( "DLLMuonTool" );
  makeMuonTool_ = tool<MakeMuonTool>( "MakeMuonTool" );
  MVATool_      = tool<IMVATool>( "MVATool" );
  return StatusCode::SUCCESS;
}

/** Iterates over all tracks in the current event and performs muon id on them.
 * Resulting PID objects as well as muon tracks are stored on the TES.
 */
std::tuple<LHCb::MuonPIDs, LHCb::Tracks> MuonIDAlgLite::operator()( const LHCb::Tracks&   tracks,
                                                                    const MuonHitHandler& hitHandler ) const {
  LHCb::MuonPIDs                                muPids;
  LHCb::Tracks                                  muTracks;
  std::map<LHCb::MuonPID*, float>               m_muonDistMap;
  std::map<LHCb::MuonPID*, CommonConstMuonHits> m_muonMap;

  muPids.setVersion( 1 ); // TODO: still valid? do we need a new version?

  std::vector<int>   isMuonIndices;
  std::vector<float> batchMVAFeaturesStorage;

  // counter buffers
  auto momCutCount     = m_momCutCount.buffer();
  auto preselCount     = m_preselCount.buffer();
  auto goodCount       = m_goodCount.buffer();
  auto pidsCount       = m_pidsCount.buffer();
  auto acceptanceCount = m_acceptanceCount.buffer();
  auto isLooseCount    = m_isLooseCount.buffer();
  auto isMuonCount     = m_isMuonCount.buffer();
  auto isTightCount    = m_isTightCount.buffer();

  // Iterate over all tracks and do the offline identification for each of them
  for ( const auto newTrackPtr : tracks ) {
    const auto& track = *newTrackPtr;
    if ( !isGoodOfflineTrack( track ) ) { continue; }
    goodCount++;
    auto muPid = std::make_unique<LHCb::MuonPID>(); // TODO: Put to where the values are set after the
                                                    // muonMap thing has been figured out...
    muPid->setIDTrack( newTrackPtr );
    muPid->setIsMuon( false );
    muPid->setIsMuonLoose( false );
    muPid->setIsMuonTight( false );
    muPid->setNShared( 0 );
    muPid->setPreSelMomentum( false );
    muPid->setInAcceptance( true );
    muPid->setMuonLLMu( -10000. );
    muPid->setMuonLLBg( -10000. );
    muPid->setChi2Corr( 0 );
    muPid->setMuonMVA1( -999. );
    muPid->setMuonMVA2( -999. );
    muPid->setMuonMVA3( -999. );
    muPid->setMuonMVA4( -999. );

    double probMu = 0.00000000001, probNonMu = 0.00000000001;
    double Dsquare = -1;

    float       trackP        = track.p();
    bool        preSel        = muonTool_->preSelection( trackP );
    const auto& state         = track.closestState( 9450.0 ); // state closest to M1
    const auto  extrapolation = muonTool_->extrapolateTrack( state );
    if ( preSel ) {
      muPid->setPreSelMomentum( 1 );
      ++momCutCount;
    }
    // Store the muonPIDs only for tracks passing InAcceptance
    if ( !muonTool_->inAcceptance( extrapolation ) ) { continue; }
    muPid->setInAcceptance( 1 );
    ++acceptanceCount;

    CommonConstMuonHits hits, hitsTight;
    if ( preSel ) {
      preselCount++;

      ICommonMuonTool::MuonTrackOccupancies occupancies, occupanciesTight;
      std::tie( hits, occupancies )           = muonTool_->hitsAndOccupancies( trackP, extrapolation, hitHandler );
      std::tie( hitsTight, occupanciesTight ) = muonTool_->extractCrossed( hits );
      const auto isMuon                       = muonTool_->isMuon( occupancies, trackP );
      const auto isMuonTight                  = muonTool_->isMuon( occupanciesTight, trackP );
      const auto isMuonLoose                  = muonTool_->isMuonLoose( occupancies, trackP );
      muPid->setIsMuon( isMuon );
      muPid->setIsMuonLoose( isMuonLoose );
      muPid->setIsMuonTight( isMuonTight );
      if ( isMuon ) { isMuonCount++; }
      if ( isMuonTight ) { isTightCount++; }
      if ( isMuonLoose ) { isLooseCount++; }

      if ( isMuonLoose and runDLL_ ) {
        // TODO: This has to go
        if ( m_dllFlag == 4 ) {
          std::tie( probMu, probNonMu, Dsquare ) =
              DLLTool_->calcMuonLL_tanhdist_landau( trackP, extrapolation, hits, occupancies );
        } else if ( m_dllFlag == 5 ) {
          std::tie( probMu, probNonMu, Dsquare ) =
              DLLTool_->calcMuonLL_tanhdist( trackP, extrapolation, hits, occupancies );
        } else {
          Error( "Invalid DLL flag" ).ignore(); // TODO: seriously this is just a quick fix,
          // this whole flag thing has to go
        }

        muPid->setMuonLLMu( log( probMu ) );
        muPid->setMuonLLBg( log( probNonMu ) );

        // TODO(kazeevn) check return value
        DLLTool_->calcNShared( muPid.get(), &muPids, hits, extrapolation, &m_muonDistMap, &m_muonMap );
      }
    }
    int this_index = muPids.insert( muPid.get() ); // ownership goes here...
    pidsCount++;

    // make the muon track only for tracks passing IsMuonLoose
    if ( muPid->IsMuonLoose() ) {
      auto muTrack = makeMuonTool_->makeMuonTrack( muPid.get(), hits, extrapolation );

      // Insert in muonTrack
      muTrack->setType( LHCb::Track::Types::Muon );
      muTrack->setHistory( LHCb::Track::History::MuonID );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::MuonMomentumPreSel, muPid->PreSelMomentum() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::MuonInAcceptance, muPid->InAcceptance() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::IsMuonLoose, muPid->IsMuonLoose() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::IsMuon, muPid->IsMuon() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::IsMuonTight, muPid->IsMuonTight() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::MuonDist2, Dsquare );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::MuonDLL, muPid->MuonLLMu() - muPid->MuonLLBg() );
      muTrack->addInfo( LHCb::Track::AdditionalInfo::MuonNShared, muPid->nShared() );
      // muTrack->addInfo(LHCb::Track::AdditionalInfo::MuonChi2perDoF, muTrack->chi2PerDoF());
      muPid->setMuonTrack( muTrack );
      muTracks.insert( muTrack );

      // Only compute the MVA for tracks passing isMuon, as the BDT has been trained with this cut
      // This follows the same logic as the ProbNN which is not computed
      // for tracks with IsMuonLoose == 1 and IsMuon == 0
      if ( runMVA_ && muPid->IsMuon() ) {
        if ( MVATool_->getRunCatBoostBatch() ) {
          isMuonIndices.push_back( this_index );
          // TODO(kazeevn) test whether it will be more efficient to allocate in
          // full in the beginning
          const size_t these_features_start = batchMVAFeaturesStorage.size();
          batchMVAFeaturesStorage.resize( batchMVAFeaturesStorage.size() + MVATool_->getNFeatures() );
          LHCb::span<float> this_features( batchMVAFeaturesStorage.data() + these_features_start,
                                           batchMVAFeaturesStorage.data() + batchMVAFeaturesStorage.size() );
          MVATool_->compute_features( *muTrack, hits, this_features );
          if ( MVATool_->getRunTMVA() ) {
            muPid->setMuonMVA1( MVATool_->calcTMVAonFeatures( this_features ) );
            m_muonMVA1Stat += muPid->muonMVA1();
          }
        } else {
          // compute the MVA variable
          IMVATool::MVAResponses mva = MVATool_->calcBDT( *muTrack, hits );
          muPid->setMuonMVA2( mva.CatBoost );
          m_muonMVA2Stat += mva.CatBoost;
          if ( MVATool_->getRunTMVA() ) {
            muPid->setMuonMVA1( mva.TMVA );
            m_muonMVA1Stat += mva.TMVA;
          }
        }
      }
    }
    muPid.release(); // ownership goes with muPids
  }
  if ( runMVA_ && MVATool_->getRunCatBoostBatch() ) {
    const std::vector<double> predictions = MVATool_->calcBDTBatch( batchMVAFeaturesStorage );
    for ( size_t track_index_index = 0; track_index_index < isMuonIndices.size(); ++track_index_index ) {
      muPids( isMuonIndices[track_index_index] )->setMuonMVA2( predictions[track_index_index] );
      m_muonMVA2Stat += predictions[track_index_index];
    }
  }
  return std::make_tuple( std::move( muPids ), std::move( muTracks ) );
}

/** Offline requirement for a good track. The track mustn't be a clone. Also it
 * should either be a long track or a downstream track or in case TT tracks are
 * supposed to be used a TT track.
 */
bool MuonIDAlgLite::isGoodOfflineTrack( const LHCb::Track& track ) const {
  return !track.checkFlag( LHCb::Track::Flags::Clone ) &&
         ( track.checkType( LHCb::Track::Types::Long ) || track.checkType( LHCb::Track::Types::Downstream ) ||
           ( useTTrack_ && track.checkType( LHCb::Track::Types::Ttrack ) ) );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of CommonMuonTool
 *
 * 2015-01-06: Kevin Dungs
 */
#include "CommonMuonTool.h"

#include <algorithm>
#include <array>
#include <memory>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

#include "DetDesc/Condition.h"
#include "Event/MuonPID.h"
#include "MuonDet/DeMuonDetector.h"
#include "vdt/exp.h"

DECLARE_COMPONENT( CommonMuonTool )

/** C'tor
 * Also declares an interface to that the tool can be obtained in Gaudi via
 * tool<...>(...)
 */
CommonMuonTool::CommonMuonTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ), m_foiFactor{1.0} {}

/** Initializes the tool.
 * Loads helpers and extracts detector information and variables from the
 * CondDB.
 */
StatusCode CommonMuonTool::initialize() {
  auto sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  m_muonDet          = getDet<DeMuonDetector>( DeMuonLocation::Default );
  m_stationsCount    = m_muonDet->stations();
  m_regionsCount     = m_muonDet->regions() / m_stationsCount;
  m_firstUsedStation = m_stationsCount - num_active_stations;

  updMgrSvc()->registerCondition( this, m_muonDet->geometry(), &CommonMuonTool::updateGeometry );

  // Prepare to load information from conditions data base
  Condition* condFoiFactor      = nullptr;
  Condition* condPreSelMomentum = nullptr;
  Condition* condFoiParametersX = nullptr;
  Condition* condFoiParametersY = nullptr;
  Condition* condMomentumCuts   = nullptr;
  registerCondition<CommonMuonTool>( "Conditions/ParticleID/Muon/FOIfactor", condFoiFactor );
  registerCondition<CommonMuonTool>( "Conditions/ParticleID/Muon/PreSelMomentum", condPreSelMomentum );
  registerCondition<CommonMuonTool>( "Conditions/ParticleID/Muon/XFOIParameters", condFoiParametersX );
  registerCondition<CommonMuonTool>( "Conditions/ParticleID/Muon/YFOIParameters", condFoiParametersY );
  registerCondition<CommonMuonTool>( "Conditions/ParticleID/Muon/MomentumCuts", condMomentumCuts );
  // Update conditions
  sc = runUpdate();
  if ( sc.isFailure() ) { return Error( "Could not update conditions from the CondDB.", sc ); }
  // Extract parameters
  m_foiFactor      = condFoiFactor->param<double>( "FOIfactor" );
  m_preSelMomentum = condPreSelMomentum->param<double>( "PreSelMomentum" );
  m_foiParamX[0]   = condFoiParametersX->paramVect<double>( "XFOIParameters1" );
  m_foiParamX[1]   = condFoiParametersX->paramVect<double>( "XFOIParameters2" );
  m_foiParamX[2]   = condFoiParametersX->paramVect<double>( "XFOIParameters3" );
  m_foiParamY[0]   = condFoiParametersY->paramVect<double>( "YFOIParameters1" );
  m_foiParamY[1]   = condFoiParametersY->paramVect<double>( "YFOIParameters2" );
  m_foiParamY[2]   = condFoiParametersY->paramVect<double>( "YFOIParameters3" );

  // TODO(kazeevn, raaij) fix the upgrade CondDB and remove the hack
  // Remove parameters we don't need, in particular in case of the upgrade
  // geometry, the first set of nRegions parameters
  for ( auto params : {std::ref( m_foiParamX ), std::ref( m_foiParamY )} ) {
    for ( auto& v : params.get() ) {
      if ( v.size() != m_stationsCount * m_regionsCount ) {
        warning() << "CondDB {X,Y}FOIParameters member size is " << v.size() << ", geometry expects "
                  << m_stationsCount * m_regionsCount << endmsg;
        v.erase( v.begin(), v.begin() + ( 5 - m_stationsCount ) * m_regionsCount );
      }
    }
  }

  m_momentumCuts = condMomentumCuts->paramVect<double>( "MomentumCuts" );

  return sc;
}

/** Project a given track into the muon stations.
 * Returns a std::vector that contains the coordinates for each station.
 */
ICommonMuonTool::MuonTrackExtrapolation CommonMuonTool::extrapolateTrack( const LHCb::State& state ) const {
  MuonTrackExtrapolation extrapolation;

  // Project the state into the muon stations
  // Linear extrapolation equivalent to TrackLinearExtrapolator
  for ( unsigned station = 0; station != m_stationsCount; ++station ) {
    // x(z') = x(z) + (dx/dz * (z' - z))
    extrapolation.emplace_back( state.x() + state.tx() * ( m_stationZ[station] - state.z() ),
                                state.y() + state.ty() * ( m_stationZ[station] - state.z() ) );
  }

  return extrapolation;
}

/** Require minimum momentum of track.
 */
bool CommonMuonTool::preSelection( const float p ) const noexcept { return p > m_preSelMomentum; }

/** Check whether track extrapolation is within detector acceptance.
 */
bool CommonMuonTool::inAcceptance( const ICommonMuonTool::MuonTrackExtrapolation& extrapolation ) const noexcept {
  using xy_t  = std::pair<float, float>;
  auto abs_lt = []( const xy_t& p1, const xy_t& p2 ) {
    return std::abs( p1.first ) < p2.first && std::abs( p1.second ) < p2.second;
  };

  // Outer acceptance
  if ( !abs_lt( extrapolation[0], m_regionOuter[0] ) ||
       !abs_lt( extrapolation[m_stationsCount - 1], m_regionOuter[m_stationsCount - 1] ) ) {
    // Outside M1 - M5 region
    return false;
  }

  // Inner acceptance
  if ( abs_lt( extrapolation[0], m_regionInner[0] ) ||
       abs_lt( extrapolation[m_stationsCount - 1], m_regionInner[m_stationsCount - 1] ) ) {
    // Inside M1 - M5 chamber hole
    return false;
  }

  return true;
}

/** Extracts hits within the FoI for a given track and its extrapolation. Also
 * gives the occupancies of the muon stations. That information is used in
 * isMuon.
 */
std::tuple<CommonConstMuonHits, ICommonMuonTool::MuonTrackOccupancies>
CommonMuonTool::hitsAndOccupancies( const float p, const ICommonMuonTool::MuonTrackExtrapolation& extrapolation,
                                    const MuonHitHandler& hitHandler ) const {
  using xy_t = std::pair<float, float>;

  /* Define an inline callable that makes it easier to check whether a hit is
   * within a given window. */
  class IsInWindow {
  public:
    // Constructor takes parameters by value and then moves them into place.
    IsInWindow( xy_t center, xy_t foi0, xy_t foi1, xy_t foi2, xy_t foi3, double sf )
        : center_{std::move( center )}
        , foi_{{std::move( foi0 ), std::move( foi1 ), std::move( foi2 ), std::move( foi3 )}} {
      std::transform( begin( foi_ ), end( foi_ ), begin( foi_ ), [sf]( const xy_t& p ) -> xy_t {
        return {p.first * sf, p.second * sf};
      } );
    }
    bool operator()( const CommonMuonHit& hit ) const {
      auto region = hit.tile().region();
      assert( region < 4 );
      return ( fabs( hit.x() - center_.first ) < hit.dx() * foi_[region].first ) &&
             ( fabs( hit.y() - center_.second ) < hit.dy() * foi_[region].second );
    }

  private:
    xy_t                center_;
    std::array<xy_t, 4> foi_;
  };

  auto makeIsInWindow = [&]( unsigned station ) {
    return IsInWindow{extrapolation[station], foi( station, 0, p ), foi( station, 1, p ),
                      foi( station, 2, p ),   foi( station, 3, p ), m_foiFactor};
  };

  CommonConstMuonHits  hits;
  MuonTrackOccupancies occupancies( m_stationsCount, 0 );

  // For Run I,II start at 1 because M1 does not matter for IsMuon
  // For the Upgrade starts at 0, as M1 does not exist anymore
  for ( unsigned s = m_stationsCount - num_active_stations; s != m_stationsCount; ++s ) {
    auto        predicate = makeIsInWindow( s );
    const auto& station   = hitHandler.station( s );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "max number of regions = " << station.nRegions() << endmsg;
    for ( unsigned r = 0; r != station.nRegions(); ++r ) {
      auto hr = hitHandler.hits( -m_regionOuter[s].first, s, r );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "REGION = " << r << endmsg;
        std::for_each( begin( hr ), end( hr ), [&]( const CommonMuonHit& hit ) {
          debug() << "Hit st = " << s << ", reg = " << r << ", position X = " << hit.x() << ", position Y = " << hit.y()
                  << endmsg;
          if ( predicate( hit ) ) {
            debug() << "###Hit in FOI. st = " << s << ", reg = " << r << ", position X = " << hit.x()
                    << ", position Y = " << hit.y() << endmsg;
          }
        } );
      }

      std::for_each( begin( hr ), end( hr ), [&]( const CommonMuonHit& hit ) {
        if ( predicate( hit ) ) hits.push_back( &hit );
      } );
    }
    occupancies[s] = hits.size();
  }
  // Turn cumulative occupancies into occupancies per station
  std::adjacent_difference( std::begin( occupancies ), std::end( occupancies ), std::begin( occupancies ) );

  return std::make_tuple( hits, occupancies );
}

/** Given a container of hits that are sorted by station this function extracts
 * all of the hits that are crossed and calculates the corresponding
 * occupancies.
 * The stations,regions M1RX, M4R1, M5R1 are made out by pads, and hence
 * they behave as crossed hits always. These regions yield mapInRegion == 1,
 * the others yield mapInRegion == 2.
 * The hits can be separated as:
 * 1) uncrossed == 0 && mapInRegion == 1 (crossed hit)
 * 2) uncrossed == 0 && mapInRegion == 2 (crossed hit)
 * 3) uncrossed == 1 && mapInRegion == 1 (crossed hit, due to mapInRegion == 1)
 * 4) uncrossed == 1 && mapInRegion == 2 (uncross hit)
 * For the crossed hits we want to select 1), 2) and 3)
 */
auto CommonMuonTool::extractCrossed( const CommonConstMuonHits& hits ) const noexcept
    -> std::tuple<CommonConstMuonHits, ICommonMuonTool::MuonTrackOccupancies> {
  auto res = CommonConstMuonHits{};
  res.reserve( hits.size() );
  std::copy_if( std::begin( hits ), std::end( hits ), std::back_inserter( res ), [&]( const CommonMuonHit* hit ) {
    return !hit->uncrossed() || m_muonDet->mapInRegion( hit->tile().station(), hit->tile().region() ) == 1;
  } );
  res.shrink_to_fit();
  // Recalculate occupancies - makes use of the fact that hits are ordered by
  // station
  MuonTrackOccupancies occupancies( m_stationsCount, 0 );
  auto                 ppl = std::begin( res );
  auto                 ppr = std::begin( res );
  // For the upgrade start at 0 as there is no M1.
  // For Run I,II start at 1 as M1 does not enter in computations.
  for ( unsigned s = m_stationsCount - num_active_stations; s != m_stationsCount; ++s ) {
    ppr            = std::partition_point( ppl, std::end( res ),
                                [s]( const CommonMuonHit* hit ) { return hit->tile().station() == s; } );
    occupancies[s] = std::distance( ppl, ppr );
    ppl            = ppr;
  }
  return std::make_tuple( res, occupancies );
}

/** Checks 'isMuon' given the occupancies corresponding to a track and the
 * track's momentum. The requirement differs in bins of p.
 */
bool CommonMuonTool::isMuon( const ICommonMuonTool::MuonTrackOccupancies& occupancies, float p ) const noexcept {
  if ( p < m_preSelMomentum ) return false;
  auto has = [&]( unsigned station ) { return occupancies[m_firstUsedStation + station] != 0; };
  if ( !has( 0 ) || !has( 1 ) ) return false;
  if ( p < m_momentumCuts[0] ) return true;
  if ( p < m_momentumCuts[1] ) return has( 2 ) || has( 3 );
  return has( 2 ) && has( 3 );
}

/** Checks 'isMuonLoose' given the occupancies corresponding to a track and the
 * track's momentum. The requirement differs in bins of p.
 *
 * The magic numbers 3.5 GeV and 4.2 GeV are the results of study by Helder in
 * Aug 2011.
 */
bool CommonMuonTool::isMuonLoose( const ICommonMuonTool::MuonTrackOccupancies& occupancies, float p ) const noexcept {
  const float pr1 = m_preSelMomentum, pr2 = m_momentumCuts[0], magic1 = 3500, magic2 = 4200;
  auto        has  = [&]( unsigned station ) { return occupancies[m_firstUsedStation + station] != 0; };
  const bool  has2 = has( 2 ) && p > magic1, has3 = has( 3 ) && p > magic2;

  if ( p < pr1 ) return false;
  if ( p < pr2 ) {
    // Require at least two hits in M2, M3, (M4 if p > magic1).
    auto v = std::array{has( 0 ), has( 1 ), has2};
    return std::count( std::begin( v ), std::end( v ), true ) > 1;
  }
  // Require at least three hits in M2, M3, (M4 if p > magic1), (M5 if p >
  // magic2).
  auto v = std::array{has( 0 ), has( 1 ), has2, has3};
  return std::count( std::begin( v ), std::end( v ), true ) > 2;
}

std::pair<float, float> CommonMuonTool::foi( unsigned int station, unsigned int region, float p ) const noexcept {
  return i_foi( station, region, p );
}

/** Helper function that returns the x, y coordinates of the FoI for a
 * given station and region and a track's momentum.
 */
std::pair<float, float> CommonMuonTool::i_foi( unsigned int station, unsigned int region, float p ) const noexcept {
  auto i = station * m_regionsCount + region;
  // TODO(kazeevn) More magic numbers(
  if ( p < 1000000 ) {
    const auto pGev                = p / Gaudi::Units::GeV;
    auto       ellipticalFoiWindow = [&]( const auto& fp ) {
      //      return fp[0][i] + fp[1][i] * vdt::fast_exp(-fp[2][i] * pGev);
      return fp[0][i] + fp[1][i] * std::exp( -fp[2][i] * pGev ); // from VTune is faster than vdt::fast_exp
    };
    return {ellipticalFoiWindow( m_foiParamX ), ellipticalFoiWindow( m_foiParamY )};
  }
  return {m_foiParamX[0][i], m_foiParamY[0][i]};
}

unsigned int CommonMuonTool::getFirstUsedStation() const noexcept { return m_firstUsedStation; }

StatusCode CommonMuonTool::updateGeometry() {
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Station positions: Inner{X,Y}, Outer{X,Y}, Z :" << endmsg; }

  // Update the cached DeMuon geometry (should be done by the detector element..)
  m_muonDet->fillGeoArray();

  // Load station information from detector description
  m_regionInner.clear();
  m_regionOuter.clear();
  m_stationZ.clear();

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Station positions: Inner{X,Y}, Outer{X,Y}, Z :" << endmsg; }
  for ( int s = 0; s != m_muonDet->stations(); ++s ) {
    m_regionInner.emplace_back( m_muonDet->getInnerX( s ), m_muonDet->getInnerY( s ) );
    m_regionOuter.emplace_back( m_muonDet->getOuterX( s ), m_muonDet->getOuterY( s ) );
    m_stationZ.push_back( m_muonDet->getStationZ( s ) );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << m_muonDet->getInnerX( s ) << ", " << m_muonDet->getInnerY( s ) << ", " << m_muonDet->getOuterX( s )
              << ", " << m_muonDet->getOuterY( s ) << ", " << m_muonDet->getStationZ( s ) << endmsg;
    }
  }

  return StatusCode::SUCCESS;
}

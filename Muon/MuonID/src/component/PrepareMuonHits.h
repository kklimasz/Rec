/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PREPAREMUONHITS_H
#define PREPAREMUONHITS_H

#include <vector>

#include "Event/MuonCoord.h"
#include "GaudiAlg/Transformer.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/CommonMuonStation.h"
#include "MuonID/MuonHitHandler.h"

/** @class Preparemuonhits Preparemuonhits.h
 *  Used to be CommonMuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class PrepareMuonHits final : public Gaudi::Functional::Transformer<MuonHitHandler( const LHCb::MuonCoords& coords )> {
public:
  PrepareMuonHits( const std::string& name, ISvcLocator* pSvcLocator );

  MuonHitHandler operator()( const LHCb::MuonCoords& coords ) const override;

  StatusCode initialize() override;

private:
  // Data members
  DeMuonDetector* m_muonDet = nullptr;
  size_t          m_nStations;

  mutable Gaudi::Accumulators::BinomialCounter<> m_badTile{this, "Impossible MuonTileID"};
};
#endif // PREPAREMUONHITS_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of MuonIDHlt1Alg.
 */
//#include "MuonIDHlt1Alg.h"

#include "Event/MuonPID_v2.h"

#include "Event/PrIterableForwardTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/PrZip.h"

#include "Event/TrackSkin.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipAlgorithms.h"
#include "SOAExtensions/ZipContainer.h"

#include "MuonDAQ/MuonHitContainer.h"
#include "MuonID/IMuonMatchTool.h"

#include "DetDesc/Condition.h"
#include "MuonDet/DeMuonDetector.h"

#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

#include <string>

namespace {

  using v2_Tracks_Zip = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackSkin>>;
  using MuonPID       = LHCb::Event::v2::MuonPID;
  using v2_MuonPIDs   = Zipping::ZipContainer<SOA::Container<std::vector, LHCb::Event::v2::MuonID>>;
  using MuonTrackExtrapolation = std::array<std::pair<float, float>, 4>;
  using MuonTrackOccupancies   = std::array<unsigned, 4>;
} // namespace

template <typename Tracks, typename MuonPIDs>
class MuonIDHlt1Alg final : public Gaudi::Functional::Transformer<MuonPIDs( const Tracks&, const MuonHitContainer& )> {
  using base_class_t = Gaudi::Functional::Transformer<MuonPIDs( const Tracks&, const MuonHitContainer& )>;
  using base_class_t::debug;
  using base_class_t::msgLevel;
  using base_class_t::warning;
  using typename base_class_t::KeyValue;

public:
  MuonIDHlt1Alg( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t( name, pSvcLocator,
                      // Inputs
                      {KeyValue{"InputTracks", "/Event/Rec/Track/Forward"}, // LHCb::TrackLocation::Default},
                       KeyValue{"InputMuonHits", MuonHitContainerLocation::Default}},
                      // Output
                      KeyValue{"OutputMuonPID", LHCb::Event::v2::MuonPIDLocation::Default} ) {}

  StatusCode initialize() override {
    auto sc = base_class_t::initialize();

    m_muonDet       = this->template getDet<DeMuonDetector>( DeMuonLocation::Default );
    m_stationsCount = m_muonDet->stations();
    m_regionsCount  = m_muonDet->regions() / m_stationsCount;

    auto scupdate = updateGeometry();
    if ( scupdate.isFailure() ) return scupdate;

    this->updMgrSvc()->registerCondition( this, m_muonDet->geometry(), &MuonIDHlt1Alg::updateGeometry );

    // Prepare to load information from conditions data base
    Condition* condFoiFactor      = nullptr;
    Condition* condPreSelMomentum = nullptr;
    Condition* condFoiParametersX = nullptr;
    Condition* condFoiParametersY = nullptr;
    Condition* condMomentumCuts   = nullptr;
    this->template registerCondition<MuonIDHlt1Alg>( "Conditions/ParticleID/Muon/FOIfactor", condFoiFactor );
    this->template registerCondition<MuonIDHlt1Alg>( "Conditions/ParticleID/Muon/PreSelMomentum", condPreSelMomentum );
    this->template registerCondition<MuonIDHlt1Alg>( "Conditions/ParticleID/Muon/XFOIParameters", condFoiParametersX );
    this->template registerCondition<MuonIDHlt1Alg>( "Conditions/ParticleID/Muon/YFOIParameters", condFoiParametersY );
    this->template registerCondition<MuonIDHlt1Alg>( "Conditions/ParticleID/Muon/MomentumCuts", condMomentumCuts );
    // Update conditions
    sc = this->runUpdate();
    if ( sc.isFailure() ) { return this->Error( "Could not update conditions from the CondDB.", sc ); }
    // Extract parameters
    m_foiFactor      = condFoiFactor->param<double>( "FOIfactor" );
    m_preSelMomentum = condPreSelMomentum->param<double>( "PreSelMomentum" );
    m_foiParamX[0]   = condFoiParametersX->paramVect<double>( "XFOIParameters1" );
    m_foiParamX[1]   = condFoiParametersX->paramVect<double>( "XFOIParameters2" );
    m_foiParamX[2]   = condFoiParametersX->paramVect<double>( "XFOIParameters3" );
    m_foiParamY[0]   = condFoiParametersY->paramVect<double>( "YFOIParameters1" );
    m_foiParamY[1]   = condFoiParametersY->paramVect<double>( "YFOIParameters2" );
    m_foiParamY[2]   = condFoiParametersY->paramVect<double>( "YFOIParameters3" );

    // TODO(kazeevn, raaij) fix the upgrade CondDB and remove the hack
    // Remove parameters we don't need, in particular in case of the upgrade
    // geometry, the first set of nRegions parameters
    for ( auto params : {std::ref( m_foiParamX ), std::ref( m_foiParamY )} ) {
      for ( auto& v : params.get() ) {
        if ( v.size() != m_stationsCount * m_regionsCount ) {
          warning() << "CondDB {X,Y}FOIParameters member size is " << v.size() << ", geometry expects "
                    << m_stationsCount * m_regionsCount << endmsg;
          v.erase( v.begin(), v.begin() + ( 5 - m_stationsCount ) * m_regionsCount );
        }
      }
    }

    m_momentumCuts = condMomentumCuts->paramVect<double>( "MomentumCuts" );

    if ( sc.isFailure() ) return sc;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
    return StatusCode::SUCCESS;
  }

  /** Iterates over all tracks in the current event and performs muon id on them.
   * Resulting PID objects are stored on the TES.
   */
  MuonPIDs operator()( const Tracks& tracks, const MuonHitContainer& hitContainer ) const override {
    // counter buffers
    auto momCutCount     = m_momCutCount.buffer();
    auto goodCount       = m_goodCount.buffer();
    auto acceptanceCount = m_acceptanceCount.buffer();
    auto isMuonCount     = m_isMuonCount.buffer();
    auto isTightCount    = m_isTightCount.buffer();

    auto muonIDlambda = [this, &momCutCount, &goodCount, &acceptanceCount, &isMuonCount, &isTightCount,
                         &hitContainer]( auto const track ) {
      ++goodCount;
      // auto muPid = std::make_unique<MuonPID>(); // TODO: Put to where the values are set after the
      // muonMap thing has been figured out...
      MuonPID muPid;
      muPid.setIsMuon( false );
      muPid.setIsMuonTight( false );
      muPid.setPreSelMomentum( false );
      // muPid.setMuonLLMu(-10000.);
      // muPid.setMuonLLBg(-10000.);
      muPid.setChi2Corr( -1. );

      auto trackmom = track.p();

      bool preSel = trackmom > m_preSelMomentum ? 1 : 0;
      if ( !preSel ) {
        muPid.setPreSelMomentum( false );
        return muPid;
      }
      muPid.setPreSelMomentum( true );
      ++momCutCount;

      MuonTrackExtrapolation extrapolation = extrapolateTrack( track.endScifiState() );

      // Store the muonPIDs only for tracks passing InAcceptance
      if ( !inAcceptance( extrapolation ) ) {
        muPid.setInAcceptance( false );
        return muPid;
      }
      muPid.setInAcceptance( 1 );
      ++acceptanceCount;

      // decide the ordering to look for hits
      // The correct order to look should be momentum dependent
      // M3, M2                  3<p<6 GeV
      // (M4 or M5) and M3, M2   6<p<10 GeV
      // M5, M4, M3, M2          p>10 GeV
      std::array<unsigned, 4> ordering;
      if ( trackmom >= m_lowMomentumThres ) {
        ordering = {3, 2, 1, 0};
      } else {
        ordering = {1, 0, 3, 2};
      }

      // CommonConstMuonHits hits, hitsTight;

      // MuonTrackOccupancies occupancies, occupanciesTight;
      auto [hits, occupancies]           = hitsAndOccupancies( trackmom, extrapolation, hitContainer, ordering );
      auto [hitsTight, occupanciesTight] = extractCrossed( hits, ordering );
      bool isM                           = isMuon( occupancies, trackmom );
      bool isMTight                      = isMuon( occupanciesTight, trackmom );
      muPid.setIsMuon( isM );
      muPid.setIsMuonTight( isMTight );
      if ( isM ) ++isMuonCount;
      if ( isMTight ) ++isTightCount;

      // int this_index = muPids.emplace_back(muPid.get());  //ownership goes here...
      /*
      // make the muon track only for tracks passing IsMuonLoose
      if (muPid.IsMuonLoose()) {
      CommonConstMuonHits::iterator ih;
      if ((hits.size()>0) && m_ComputeChi2) {
      // put match information in IMuonMatchTool format
      std::vector<TrackMuMatch> matches;
      for (ih = hits.begin(); ih != hits.end(); ih++) {
      const std::pair<float, float>& trackxy = extrapolation[(*ih)->station()];
      matches.push_back( std::tuple(*ih, 0., trackxy.first, trackxy.second ) );
      }
      // get correlated chi2 algorithm
      auto [ chi2, ndof ] = matchTool->run(trackmom, &matches);
      muPid.setChi2Corr(chi2/ndof);
      }
      }
      */
      return muPid;
    };
    if constexpr ( std::is_same_v<MuonPIDs, LHCb::Pr::Muon::PIDs> ) {
      auto iterable_tracks_scalar = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar, true>( tracks );
      return Zipping::transform<LHCb::Pr::Muon::PIDs>( iterable_tracks_scalar, muonIDlambda );
    } else {
      return Zipping::transform<LHCb::Event::v2::MuonID, std::vector>( tracks, muonIDlambda );
    }
  }

private:
  std::pair<float, float> foi( unsigned int station, unsigned int region, float p ) const {

    /** Helper function that returns the x, y coordinates of the FoI for a
     * given station and region and a track's momentum.
     */
    auto i = station * m_regionsCount + region;
    if ( p < 1000000.f ) { // limit to 1 TeV momentum tracks
      const auto pGev                = p / Gaudi::Units::GeV;
      auto       ellipticalFoiWindow = [&]( const auto& fp ) {
        return fp[0][i] + fp[1][i] * std::exp( -fp[2][i] * pGev ); // from VTune is faster than vdt::fast_exp
      };
      return {ellipticalFoiWindow( m_foiParamX ), ellipticalFoiWindow( m_foiParamY )};
    }
    return {m_foiParamX[0][i], m_foiParamY[0][i]};
  }

  /** Given a container of hits that are sorted by station this function extracts
   * all of the hits that are crossed and calculates the corresponding
   * occupancies.
   * The stations,regions M1RX, M4R1, M5R1 are made out by pads, and hence
   * they behave as crossed hits always. These regions yield mapInRegion == 1,
   * the others yield mapInRegion == 2.
   * The hits can be separated as:
   * 1) uncrossed == 0 && mapInRegion == 1 (crossed hit)
   * 2) uncrossed == 0 && mapInRegion == 2 (crossed hit)
   * 3) uncrossed == 1 && mapInRegion == 1 (crossed hit, due to mapInRegion == 1)
   * 4) uncrossed == 1 && mapInRegion == 2 (uncross hit)
   * For the crossed hits we want to select 1), 2) and 3)
   */
  std::tuple<CommonConstMuonHits, MuonTrackOccupancies> extractCrossed( const CommonConstMuonHits&    hits,
                                                                        const std::array<unsigned, 4> ordering ) const {
    auto res = CommonConstMuonHits{};
    res.reserve( hits.size() );
    std::copy_if( std::begin( hits ), std::end( hits ), std::back_inserter( res ), [&]( const CommonMuonHit* hit ) {
      return !hit->uncrossed() || m_muonDet->mapInRegion( hit->station(), hit->region() ) == 1;
    } );
    res.shrink_to_fit();
    // Recalculate occupancies - makes use of the fact that hits are ordered by
    // station
    MuonTrackOccupancies occupancies{0, 0, 0, 0};
    auto                 ppl = std::begin( res );
    auto                 ppr = std::begin( res );
    for ( unsigned it = 0; it != ordering.size(); ++it ) {
      unsigned s = ordering[it];
      ppr =
          std::partition_point( ppl, std::end( res ), [s]( const CommonMuonHit* hit ) { return hit->station() == s; } );
      occupancies[s] = std::distance( ppl, ppr );
      ppl            = ppr;
    }
    return std::tuple( res, occupancies );
  }

  /** Checks 'isMuon' given the occupancies corresponding to a track and the
   * track's momentum. The requirement differs in bins of p.
   */
  bool isMuon( const MuonTrackOccupancies& occupancies, float p ) const {
    if ( p < m_preSelMomentum ) return false;
    auto has = [&]( unsigned station ) { return occupancies[station] != 0; };
    if ( !has( 0 ) || !has( 1 ) ) return false;
    if ( p < m_momentumCuts[0] ) return true;
    if ( p < m_momentumCuts[1] ) return has( 2 ) || has( 3 );
    return has( 2 ) && has( 3 );
  }

  /** Check whether track extrapolation is within detector acceptance.
   */
  bool inAcceptance( const MuonTrackExtrapolation& extrapolation ) const {
    using xy_t  = std::pair<float, float>;
    auto abs_lt = []( const xy_t& p1, const xy_t& p2 ) {
      return std::abs( p1.first ) < p2.first && std::abs( p1.second ) < p2.second;
    };

    // Outer acceptance
    if ( !abs_lt( extrapolation[0], m_regionOuter[0] ) ||
         !abs_lt( extrapolation[m_stationsCount - 1], m_regionOuter[m_stationsCount - 1] ) ) {
      // Outside M2 - M5 region
      return false;
    }

    // Inner acceptance
    if ( abs_lt( extrapolation[0], m_regionInner[0] ) ||
         abs_lt( extrapolation[m_stationsCount - 1], m_regionInner[m_stationsCount - 1] ) ) {
      // Inside M2 - M5 chamber hole
      return false;
    }

    return true;
  }

  /** Project a given track into the muon stations.
   */
  template <typename State>
  MuonTrackExtrapolation extrapolateTrack( State const& state ) const {
    MuonTrackExtrapolation extrapolation;

    // Project the state into the muon stations
    // Linear extrapolation equivalent to TrackLinearExtrapolator
    for ( unsigned station = 0; station != m_stationsCount; ++station ) {
      // x(z') = x(z) + (dx/dz * (z' - z))
      extrapolation[station] = std::make_pair( state.x() + state.tx() * ( m_stationZ[station] - state.z() ),
                                               state.y() + state.ty() * ( m_stationZ[station] - state.z() ) );
    }

    return extrapolation;
  }

  /** Extracts hits within the FoI for a given track and its extrapolation. Also
   * gives the occupancies of the muon stations. That information is used in
   * isMuon.
   */
  std::tuple<CommonConstMuonHits, MuonTrackOccupancies>
  hitsAndOccupancies( const float p, const MuonTrackExtrapolation& extrapolation, const MuonHitContainer& hitContainer,
                      const std::array<unsigned, 4> ordering ) const {
    using xy_t = std::pair<float, float>;

    /* Define an inline callable that makes it easier to check whether a hit is
     * within a given window. */
    class IsInWindow {
    public:
      // Constructor takes parameters by value and then moves them into place.
      IsInWindow( xy_t center, xy_t foi0, xy_t foi1, xy_t foi2, xy_t foi3, double sf )
          : center_{std::move( center )}
          , foi_{{std::move( foi0 ), std::move( foi1 ), std::move( foi2 ), std::move( foi3 )}} {
        std::transform( begin( foi_ ), end( foi_ ), begin( foi_ ), [sf]( const xy_t& p ) -> xy_t {
          return {p.first * sf, p.second * sf};
        } );
      }
      bool operator()( const CommonMuonHit& hit ) const {
        auto region = hit.region();
        assert( region < 4 );
        bool passed = false;
        if ( fabs( hit.x() - center_.first ) < hit.dx() * foi_[region].first ) {
          if ( fabs( hit.y() - center_.second ) < hit.dy() * foi_[region].second ) { passed = true; }
        }
        return passed;
      }

    private:
      xy_t                center_;
      std::array<xy_t, 4> foi_;
    };

    auto makeIsInWindow = [&]( unsigned station ) {
      return IsInWindow{extrapolation[station], foi( station, 0, p ), foi( station, 1, p ),
                        foi( station, 2, p ),   foi( station, 3, p ), m_foiFactor};
    };

    CommonConstMuonHits  hits;
    MuonTrackOccupancies occupancies{0, 0, 0, 0};

    // For Run I,II start at 1 because M1 does not matter for IsMuon
    // For the Upgrade starts at 0, as M1 does not exist anymore
    unsigned previousOccupancy = 0;
    for ( unsigned it = 0; it != ordering.size(); ++it ) {
      unsigned    s              = ordering[it];
      unsigned    currentStation = s;
      auto        predicate      = makeIsInWindow( s );
      const auto& station        = hitContainer.station( s );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "max number of regions = " << station.nRegions() << endmsg;
      for ( unsigned r = 0; r != station.nRegions(); ++r ) {
        auto hr = hitContainer.hits( -m_regionOuter[s].first, s, r );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "REGION = " << r << endmsg;
          std::for_each( begin( hr ), end( hr ), [&]( const CommonMuonHit& hit ) {
            debug() << "Hit st = " << s << ", reg = " << r << ", position X = " << hit.x()
                    << ", position Y = " << hit.y() << endmsg;
            if ( predicate( hit ) ) {
              debug() << "###Hit in FOI. st = " << s << ", reg = " << r << ", position X = " << hit.x()
                      << ", position Y = " << hit.y() << endmsg;
            }
          } );
        }

        std::for_each( begin( hr ), end( hr ), [&]( const CommonMuonHit& hit ) {
          if ( predicate( hit ) ) hits.push_back( &hit );
        } );
      }
      previousOccupancy =
          occupancies[ordering[0]] + occupancies[ordering[1]] + occupancies[ordering[2]] + occupancies[ordering[3]];
      occupancies[s] = hits.size() - previousOccupancy;

      // evaluate the occupancies from less to more occupied to avoid
      // useless loops through stations
      if ( currentStation < 2 and occupancies[currentStation] == 0 ) break;
      if ( currentStation == 2 and occupancies[currentStation] == 0 and occupancies[currentStation + 1] == 0 and
           p > m_lowMomentumThres and p < m_highMomentumThres )
        break;
      if ( p > m_highMomentumThres and occupancies[currentStation] == 0 ) break;
    }

    return std::tuple( hits, occupancies );
  }

  StatusCode updateGeometry() {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Station positions: Inner{X,Y}, Outer{X,Y}, Z :" << endmsg; }

    // Update the cached DeMuon geometry (should be done by the detector element..)
    m_muonDet->fillGeoArray();

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Station positions: Inner{X,Y}, Outer{X,Y}, Z :" << endmsg; }
    for ( int s = 0; s != m_muonDet->stations(); ++s ) {
      m_regionInner[s] = std::make_pair( m_muonDet->getInnerX( s ), m_muonDet->getInnerY( s ) );
      m_regionOuter[s] = std::make_pair( m_muonDet->getOuterX( s ), m_muonDet->getOuterY( s ) );
      m_stationZ[s]    = m_muonDet->getStationZ( s );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << m_muonDet->getInnerX( s ) << ", " << m_muonDet->getInnerY( s ) << ", " << m_muonDet->getOuterX( s )
                << ", " << m_muonDet->getOuterY( s ) << ", " << m_muonDet->getStationZ( s ) << endmsg;
      }
    }

    return StatusCode::SUCCESS;
  }

private:
  DeMuonDetector*                    m_muonDet        = nullptr; // non-owning
  double                             m_foiFactor      = 1.;
  double                             m_preSelMomentum = 0.;
  std::array<float, 4>               m_stationZ;
  MuonTrackExtrapolation             m_regionInner, m_regionOuter;
  std::array<std::vector<double>, 3> m_foiParamX, m_foiParamY;
  std::vector<double>                m_momentumCuts;
  size_t                             m_stationsCount     = 0;
  size_t                             m_regionsCount      = 0;
  float                              m_lowMomentumThres  = 6000.f;
  float                              m_highMomentumThres = 10000.f;

  // Members
  //  ToolHandle<IMuonMatchTool> matchTool = {"MuonChi2MatchToolUpgrade"};

  mutable Gaudi::Accumulators::Counter<> m_momCutCount{this, "nMomentumCut"};
  mutable Gaudi::Accumulators::Counter<> m_goodCount{this, "nGoodOffline"};
  mutable Gaudi::Accumulators::Counter<> m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<> m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<> m_isTightCount{this, "nIsMuonTight"};

  Gaudi::Property<bool> useTTrack_{this, "useTTrack", false};
  Gaudi::Property<bool> m_ComputeChi2{this, "ComputeChi2", true};
};

typedef MuonIDHlt1Alg<v2_Tracks_Zip, v2_MuonPIDs> MuonIDHlt1Alg_v2;
DECLARE_COMPONENT_WITH_ID( MuonIDHlt1Alg_v2, "MuonIDHlt1Alg" )

typedef MuonIDHlt1Alg<LHCb::Pr::Forward::Tracks, LHCb::Pr::Muon::PIDs> MuonIDHlt1Alg_pr;
DECLARE_COMPONENT_WITH_ID( MuonIDHlt1Alg_pr, "MuonIDHlt1AlgPr" )

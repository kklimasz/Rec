/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef INSHARED_H
#define INSHARED_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

/** @class INShared INShared.h
 *
 *
 *  @author Xabier Cid Vidal
 *  @date   2011-03-17
 */
struct INShared : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( INShared, 2, 0 );
  virtual StatusCode getNShared( const LHCb::Track& muTrack, int& nshared ) = 0;
  virtual void       fillNShared()                                          = 0;
};
#endif // INSHARED_H

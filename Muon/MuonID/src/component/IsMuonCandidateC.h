/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ISMUONCANDIDATEC_H
#define ISMUONCANDIDATEC_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "IIsMuonCandidateC.h" // Interface

/** @class Ismuoncandidatec Ismuoncandidatec.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2009-03-12
 */
class IsMuonCandidateC : public GaudiTool, public IIsMuonCandidateC {
public:
  /// Standard constructor
  IsMuonCandidateC( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  bool       IsMuonCandidate( const LHCb::Track& muTrack ) override;
  bool       IsMuonCandidate( const std::vector<LHCb::LHCbID>& LHCbIDs, const double& mom ) override;

  bool stInStations( const int myst, const std::vector<int>& stations );
  bool IsMuonSimple( const std::vector<int>& stations );
  bool IsMuon( const std::vector<int>& stations, double p );
  bool IsMuonLoose( const std::vector<int>& stations, double p );

protected:
  Gaudi::Property<std::vector<float>> m_MomRangeIsMuon{
      this, "MomRangeIsMuon", {3000., 6000., 10000.}, "Mom Range for IsMuon"};

  Gaudi::Property<int> m_ismopt{this, "IsMuonOpt", 1, "IsMuon used: 1=IsMuonCandidate, 2=IsMuonLoose, 3=IsMuon"};

  Gaudi::Property<int> m_minhits{this, "MinHits", 2, "Min number of hits for IsMuonSimple"};
};
#endif // ISMUONCANDIDATEC_H

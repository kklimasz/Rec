/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "model_generated.h"

#include <string>
#include <vector>

namespace NCatboostStandalone {
  enum class EPredictionType { RawValue, Probability, Class };

  // TODO: Currently this class contains only pointwise apply interface
  // Should reuse formula evaluator from libs/model folder to get unified codebase
  class TZeroCopyEvaluator {
  public:
    TZeroCopyEvaluator() = default;

    TZeroCopyEvaluator( const NCatBoostFbs::TModelCore* core );

    double Apply( const std::vector<float>& features, EPredictionType predictionType ) const;

    void SetModelPtr( const NCatBoostFbs::TModelCore* core );

    int GetFloatFeatureCount() const { return FloatFeatureCount; }

  private:
    const NCatBoostFbs::TObliviousTrees* ObliviousTrees     = nullptr;
    size_t                               BinaryFeatureCount = 0;
    int                                  FloatFeatureCount  = 0;
  };

  class TOwningEvaluator : public TZeroCopyEvaluator {
    TOwningEvaluator() = delete;

  public:
    TOwningEvaluator( const std::string& modelFile );
    TOwningEvaluator( const std::vector<unsigned char>& modelBlob );
    TOwningEvaluator( std::vector<unsigned char>&& modelBlob );

  private:
    void InitEvaluator();

  private:
    std::vector<unsigned char> ModelBlob;
  };
} // namespace NCatboostStandalone

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONIDALGLITE_H
#define MUONIDALGLITE_H

#include <string>

#include "Event/MuonPID.h"
#include "Event/Track.h"

#include "GaudiAlg/Transformer.h"

#include "DLLMuonTool.h"
#include "MakeMuonTool.h"
#include "MuonID/ICommonMuonTool.h"
#include "MuonID/IMVATool.h"
#include "MuonID/MuonHitHandler.h"

/** @class MuonIDAlgLite MuonIDAlgLite.h
 * A lightweight version of MuonIDAlg that uses the CommonMuonTool.
 *
 * @author Kevin Dungs
 * @date 2015-01-06
 */
class MuonIDAlgLite final : public Gaudi::Functional::MultiTransformer<std::tuple<LHCb::MuonPIDs, LHCb::Tracks>(
                                const LHCb::Tracks&, const MuonHitHandler& )> {
public:
  /// Standard constructor
  MuonIDAlgLite( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode                               initialize() override;
  std::tuple<LHCb::MuonPIDs, LHCb::Tracks> operator()( const LHCb::Tracks&   tracks,
                                                       const MuonHitHandler& hitHandler ) const override;

  /* Number of MUON stations to use. In Run II there are 5 stations, 4
  used in the software. In Run III there will be 4 stations. So in
  foreseeable future you want this = 4. Unfortunately, the 4 stations are
  hardcoded into some algorithms so this should be a const.
  */
  static const unsigned int num_active_stations = 4;

private:
  // Helper functions
  bool isGoodOfflineTrack( const LHCb::Track& ) const;

  LHCb::Track makeMuonTrack( const LHCb::MuonPID& ) const;

  // TODO: This has to go
  //--------------------
  // flag to use DLL:
  //--------------------
  // 4 -- binned tanh(distance) with closest hit(Muon) + integral of Landau
  // fit(NonMuon).
  // 5 -- binned tanh(distance) with closest hit(Muon) + integral. New tunning
  // with runI data
  //-----------------------------
  Gaudi::Property<int> m_dllFlag{this, "DLL_flag", 4};

  // Members
  ICommonMuonTool* muonTool_     = nullptr;
  DLLMuonTool*     DLLTool_      = nullptr;
  MakeMuonTool*    makeMuonTool_ = nullptr;
  IMVATool*        MVATool_      = nullptr;

  mutable Gaudi::Accumulators::Counter<>           m_momCutCount{this, "nMomentumCut"};
  mutable Gaudi::Accumulators::Counter<>           m_preselCount{this, "nPreSelTrack"};
  mutable Gaudi::Accumulators::Counter<>           m_goodCount{this, "nGoodOffline"};
  mutable Gaudi::Accumulators::Counter<>           m_pidsCount{this, "nMuonPIDs"};
  mutable Gaudi::Accumulators::Counter<>           m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<>           m_isLooseCount{this, "nIsMuonLoose"};
  mutable Gaudi::Accumulators::Counter<>           m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<>           m_isTightCount{this, "nIsMuonTight"};
  mutable Gaudi::Accumulators::StatCounter<double> m_muonMVA1Stat{this, "muonMVA1Stat"};
  mutable Gaudi::Accumulators::StatCounter<float>  m_muonMVA2Stat{this, "muonMVA2Stat"};

  Gaudi::Property<bool> useTTrack_{this, "useTTrack", false};
  Gaudi::Property<bool> runDLL_{this, "runDLL", true};
  Gaudi::Property<bool> runMVA_{this, "runMVA", true};
};

#endif // MUONIDALGLITE_H

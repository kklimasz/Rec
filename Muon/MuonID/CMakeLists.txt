###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: MuonID
################################################################################
LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}") 
gaudi_subdir(MuonID v10r1)

gaudi_depends_on_subdirs(Det/MuonDet
                         Event/RecEvent
                         Event/TrackEvent
                         Muon/MuonInterfaces
                         Muon/MuonTrackRec
                         Tr/TrackInterfaces
                         Tr/TrackFitEvent
                         Tr/TrackKernel
                         GaudiAlg)

find_package(Boost)
find_package(ROOT COMPONENTS Hist Gpad Graf Matrix)
find_path(CPP_GSL_INCLUDE_DIR NAMES gsl/span)
find_package(VDT)
find_package(catboost)
find_package(flatbuffers)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${CPP_GSL_INCLUDE_DIR}
                           ${flatbuffers_INCLUDE_DIRS} ${catboost_INCLUDE_DIRS})

gaudi_add_library(MuonIDLib
                 src/lib/*.cpp
                 PUBLIC_HEADERS MuonID
                 INCLUDE_DIRS ROOT Tr/TrackInterfaces VDT
                 LINK_LIBRARIES ROOT MuonDetLib RecEvent TrackEvent TrackKernel)

gaudi_add_module(MuonID
                 src/component/*.cpp
                 INCLUDE_DIRS ROOT Tr/TrackInterfaces VDT
                 LINK_LIBRARIES ROOT MuonDetLib RecEvent TrackEvent TrackKernel MuonInterfacesLib TrackFitEvent GaudiAlgLib MuonIDLib catboost flatbuffers MuonDAQLib)


gaudi_add_dictionary(MuonID
                     dict/MuonIDDict.h
                     dict/MuonID.xml
                     LINK_LIBRARIES TrackEvent TrackKernel
                     OPTIONS " -U__MINGW32__ ")

gaudi_install_python_modules()

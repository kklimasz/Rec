/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
// #include "Event/CaloFutureHypo.h"
#include "CaloFuturePi0Ntp.h"
#include "CaloFutureMoniUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"

namespace {
  /// hack to allow for tools with non-const interfaces...
  template <typename IFace>
  IFace* fixup( const ToolHandle<IFace>& iface ) {
    return &const_cast<IFace&>( *iface );
  }
} // namespace
// =============================================================================

DECLARE_COMPONENT( CaloFuturePi0Ntp )

namespace {
  // Return new 4D LorentzVector with first-3 spatial components normalized to 1
  // and the last component equal to 1. The space norm ==1, the 4dnorm==0.
  Gaudi::LorentzVector normalize( Gaudi::XYZPoint v3 ) {
    return Gaudi::LorentzVector{v3.X() / v3.R(), v3.Y() / v3.R(), v3.Z() / v3.R(), 1.};
  }
} // namespace

// =============================================================================

CaloFuturePi0Ntp::CaloFuturePi0Ntp( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default},
                 KeyValue{"Locations", LHCb::CaloHypoLocation::Photons}, KeyValue{"VertexLoc", ""}} ) {}

// =============================================================================

StatusCode CaloFuturePi0Ntp::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  if ( sc.isFailure() ) return sc;

  // retrieve & configure tools
  m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

  // set vertex location
  auto vertLoc = inputLocation<2>(); // <-- Careful with the index, 'VertexLoc' is 4th.
  if ( vertLoc.empty() ) {
    vertLoc = ( m_usePV3D ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary );
    updateHandleLocation( *this, "VertexLoc", vertLoc );
  }

  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloFuturePi0Ntp::operator()( const ODIN& odin, const Hypos& hypos, const Vertices& verts ) const {

  // Collect global info (in pre-Functional implementation, some are optionals)
  m_odin->getTime();
  const auto run   = odin.runNumber();
  const auto evt   = (double)odin.eventNumber();
  const auto tty   = odin.triggerType();
  const auto nVert = verts.size();
  if ( m_counterStat->isQuiet() )
    counter( "#PV=" + Gaudi::Utils::toString( nVert ) + " [" + inputLocation<2>() + "]" ) += 1;

  // Start looping over hypos
  bool ok = false;
  for ( auto g1 = hypos.begin(); hypos.end() != g1; ++g1 ) {
    const auto p1 = *g1;
    if ( p1 == nullptr ) continue;

    LHCb::CaloMomentum mt1( p1 );
    if ( !inRange( m_ppt, mt1.pt() ) ) continue;

    const auto v1     = momentum( p1 );
    const auto point1 = position3d( p1 );
    const auto cl1    = firstCluster( p1 );
    if ( cl1 == 0 ) continue; // SmartRef not overload with nullptr yet

    const auto id1  = cl1->seed();
    const auto pos1 = position3d( cl1 );
    const auto c1   = position4d( cl1 );
    const auto cc1  = normalize( pos1 ) * cl1->e();
    const auto ccc1 = normalize( point1 ) * cl1->e();

    // loop over the second photon
    for ( auto g2 = g1 + 1; hypos.end() != g2; ++g2 ) {
      const auto p2 = *g2;
      if ( p2 == nullptr ) continue;

      LHCb::CaloMomentum mt2( p2 );
      if ( !inRange( m_ppt, mt2.pt() ) ) continue;

      const auto v2     = momentum( p2 );
      const auto point2 = position3d( p2 );
      const auto pi0    = v1 + v2;

      // background shape from (x,y)->(-x,-y) symmetrized g2
      Gaudi::LorentzVector v2Sym( v2 );
      v2Sym.SetPx( -v2.Px() );
      v2Sym.SetPy( -v2.Py() );
      const auto      bkg = v1 + v2Sym;
      Gaudi::XYZPoint p2Sym( -p2->position()->x(), -p2->position()->y(), p2->position()->z() );

      bool isBkg = inRange( m_e, bkg.e() ) && inRange( m_pt, bkg.pt() ) && inRange( m_mass, bkg.mass() );
      bool isPi0 = inRange( m_e, pi0.e() ) && inRange( m_pt, pi0.pt() ) && inRange( m_mass, pi0.mass() );

      if ( !isPi0 && !isBkg ) continue;

      // Get cellIDs
      const auto cl2 = firstCluster( p2 );
      const auto id2 = cl2->seed();
      if ( cl2 == 0 ) continue; // SmartRef not overload with nullptr yet

      // isolation criteria
      const auto vec     = point2 - point1;
      const auto vecSym  = p2Sym - point1;
      const auto cSize   = std::max( m_calo->cellSize( id1 ), m_calo->cellSize( id2 ) );
      const auto isol    = ( cSize > 0 ) ? vec.Rho() / cSize : 0;
      const auto isolSym = ( cSize > 0 ) ? vecSym.Rho() / cSize : 0;

      isPi0 = isPi0 && inRange( m_isol, isol );
      isBkg = isBkg && inRange( m_isol, isolSym );

      if ( ( isPi0 ) || ( m_bkg && isBkg ) ) {

        const auto pi0m = isPi0 ? pi0.mass() : 0.;
        const auto bkgm = isBkg ? bkg.mass() : 0.;
        const auto typ  = (isPi0)*1 + (isBkg)*2;
        const auto c2   = position4d( cl2 );
        const auto pos2 = position3d( cl2 );
        const auto cc2  = normalize( pos2 ) * cl2->e();
        const auto ccc2 = normalize( point2 ) * cl2->e();

        // cluster mass
        const auto cc     = cc1 + cc2;
        const auto ccc    = ccc1 + ccc2;
        const auto ccmas  = ( isPi0 ) ? cc.mass() : 0;
        const auto cccmas = ( isPi0 ) ? ccc.mass() : 0;

        if ( m_counterStat->isQuiet() )
          counter( "candidates for #PV=" + Gaudi::Utils::toString( nVert ) + " [" + inputLocation<2>() + "]" ) += 1;

        // Write tuple on-demand
        if ( m_tuple ) {
          auto ntp = nTuple( 500, "pi0_tupling", CLID_ColumnWiseTuple );
          ntp->column( "p1", v1 );
          ntp->column( "p2", v2 );
          ntp->column( "r1", point1 );
          ntp->column( "r2", point2 );
          ntp->column( "id1", id1.index() );
          ntp->column( "id2", id2.index() );
          ntp->column( "cl1", c1 );
          ntp->column( "cl2", c2 );
          ntp->column( "mass", pi0m );
          ntp->column( "type", typ );
          ntp->column( "p", pi0 );
          ntp->column( "clmass", ccmas );
          ntp->column( "clEmass", cccmas );
          if ( m_bkg ) ntp->column( "bkg", bkgm );
          // odin info
          ntp->column( "run", run );
          ntp->column( "event", (double)evt );
          ntp->column( "triggertype", tty );
          // #vertices
          ntp->column( "Nvertices", nVert );
          ok = ntp->write().isSuccess();
        }
        if ( !isPi0 ) continue;
        // histograms for tuning
        if ( m_histo ) {
          hTuning( "Cluster", ccc1, id1, ccc2, id2, nVert );
          hTuning( "Corrected", v1, id1, v2, id2, nVert );
        }
        if ( m_trend ) {
          std::string sNpv = "PV" + Gaudi::Utils::toString( nVert ) + "/";
          std::string sRun = "r" + Gaudi::Utils::toString( run ) + "/";
          std::string base = "Trend/";
          plot1D( pi0m, base + "allPV/allRun/mass", "di-photon mass spectrum for all run & allPV", m_hMin, m_hMax,
                  m_hBin );
          plot1D( pi0m, base + "allPV/" + sRun + "mass", "di-photon mass spectrum for all PV & run = " + sRun, m_hMin,
                  m_hMax, m_hBin );
          if ( id1.area() == id2.area() ) {
            const auto sarea = id1.areaName();
            plot1D( pi0m, base + "allPV/" + sRun + sarea, "di-photon mass spectrum for all PV & run = " + sRun, m_hMin,
                    m_hMax, m_hBin );
          }
          plot1D( pi0m, base + sNpv + sRun + "mass", "di-photon mass spectrum for PV=" + sNpv + " (run = " + sRun + ")",
                  m_hMin, m_hMax, m_hBin );
          plot1D( pi0m, base + sNpv + "allRun/mass", "di-photon mass spectrum for PV=" + sNpv + " (all run)", m_hMin,
                  m_hMax, m_hBin );
        }
      }
    }
  }
  if ( ok && m_counterStat->isQuiet() )
    counter( "Events in tuple" ) += 1;
  else
    Warning( "Error with ntupling", StatusCode::SUCCESS ).ignore();
  return;
}

//==============================================================================

void CaloFuturePi0Ntp::hTuning( std::string base, const Gaudi::LorentzVector c1, const LHCb::CaloCellID id1,
                                const Gaudi::LorentzVector c2, const LHCb::CaloCellID id2, int nVert ) const {

  std::string s1    = id1.areaName();
  std::string s2    = id2.areaName();
  std::string sarea = ( id1.area() > id2.area() ) ? s1 + s2 : s2 + s1;
  // log(E) bins
  int ble1 = int( ( log( c1.e() ) / m_leBin ) );
  int ble2 = int( ( log( c2.e() ) / m_leBin ) );
  // Et bins
  int bet1 = int( c1.Pt() / m_etBin );
  int bet2 = int( c2.Pt() / m_etBin );
  // theta bins
  int bth1 = int( c1.Theta() / m_thBin / pow( 2., (double)2 - id1.area() ) );
  int bth2 = int( c2.Theta() / m_thBin / pow( 2., (double)2 - id2.area() ) );

  const auto di = c1 + c2;

  // pi0->gg versus nPV
  std::string sVert = "PV" + Gaudi::Utils::toString( nVert );
  plot1D( di.mass(), base + "/" + sVert + "/all", base + "/all  #PV=" + Gaudi::Utils::toString( nVert ), m_hMin, m_hMax,
          m_hBin );
  plot1D( di.mass(), base + "/" + sVert + "/all", base + "/all  #PV=" + Gaudi::Utils::toString( nVert ), m_hMin, m_hMax,
          m_hBin );
  const double Y1 = m_calo->cellCenter( id1 ).Y();
  const double Y2 = m_calo->cellCenter( id2 ).Y();
  if ( fabs( Y1 ) < 300 && fabs( Y2 ) < 300 ) {
    plot1D( di.mass(), base + "/" + sVert + "/band", base + "/band  #PV=" + Gaudi::Utils::toString( nVert ), m_hMin,
            m_hMax, m_hBin );
    plot1D( di.mass(), base + "/" + sVert + "/band", base + "/band  #PV=" + Gaudi::Utils::toString( nVert ), m_hMin,
            m_hMax, m_hBin );
  }

  // highPt selection
  if ( di.Pt() > 2000 && c1.Pt() > 800 && c2.Pt() > 800 )
    plot1D( di.mass(), base + "/all_highPt_sel1", base + "/all highPt  sel1 spd == 0", m_hMin, m_hMax, m_hBin );
  if ( ( ( c1.Pt() > 1050 && c2.Pt() > 250 ) || ( c2.Pt() > 1050 && c1.Pt() > 250 ) ) )
    plot1D( di.mass(), base + "/all_highPt_sel2", base + "/all highPt sel2 spd == 0", m_hMin, m_hMax, m_hBin );

  plot1D( di.mass(), base + "/all", base + "/all", m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), base + "/" + sarea + "/all", base + "/" + sarea + "/all", m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), base + "/" + sarea + "/all", base + "/" + sarea + "/all", m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), base + "/" + sarea + "/all", base + "/" + sarea + "/all", m_hMin, m_hMax, m_hBin );

  std::string u  = base + "/" + sarea + "/Energy/all";
  std::string u1 = base + "/" + sarea + "/" + "/Energy/b" + Gaudi::Utils::toString( (double)ble1 * m_leBin );
  std::string u2 = base + "/" + sarea + "/" + "/Energy/b" + Gaudi::Utils::toString( (double)ble2 * m_leBin );
  plot1D( di.mass(), u1, u1, m_hMin, m_hMax, m_hBin );
  if ( u2 != u1 ) plot1D( di.mass(), u2, u2, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), u, u, m_hMin, m_hMax, m_hBin );

  std::string tu  = base + "/" + sarea + "/EcalTuning/Energy/";
  std::string bin = Gaudi::Utils::toString( (double)ble1 * m_leBin );
  plot1D( di.mass(), tu + bin, tu + bin, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), tu + "all", tu + "all", m_hMin, m_hMax, m_hBin );

  std::string uu  = base + "/" + sarea + "/" + "/Pt/all";
  std::string uu1 = base + "/" + sarea +
                    "/"
                    "/Pt/b" +
                    Gaudi::Utils::toString( (double)bet1 * m_etBin );
  std::string uu2 = base + "/" + sarea + "/" + "/Pt/b" + Gaudi::Utils::toString( (double)bet2 * m_etBin );
  plot1D( di.mass(), uu1, uu1, m_hMin, m_hMax, m_hBin );
  if ( uu2 != uu1 ) plot1D( di.mass(), uu2, uu2, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), uu, uu, m_hMin, m_hMax, m_hBin );
  tu  = base + "/" + sarea + "/EcalTuning/Pt/";
  bin = Gaudi::Utils::toString( (double)bet1 * m_etBin );
  plot1D( di.mass(), tu + bin, tu + bin, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), tu + "all", tu + "all", m_hMin, m_hMax, m_hBin );

  std::string uuu  = base + "/" + sarea + "/Theta/all";
  std::string uuu1 = base + "/" + sarea + "/Theta/b" +
                     Gaudi::Utils::toString( (double)bth1 * m_thBin * pow( 2., (double)2 - id1.area() ) );
  std::string uuu2 = base + "/" + sarea + "/Theta/b" +
                     Gaudi::Utils::toString( (double)bth2 * m_thBin * pow( 2., (double)2 - id2.area() ) );
  plot1D( di.mass(), uuu1, uuu1, m_hMin, m_hMax, m_hBin );
  if ( uu2 != uu1 ) plot1D( di.mass(), uuu2, uuu2, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), uuu, uuu, m_hMin, m_hMax, m_hBin );
  tu  = base + "/" + sarea + "/EcalTuning/Theta/";
  bin = Gaudi::Utils::toString( (double)bth1 * m_thBin * pow( 2., (double)2 - id1.area() ) );
  plot1D( di.mass(), tu + bin, tu + bin, m_hMin, m_hMax, m_hBin );
  plot1D( di.mass(), tu + "all", tu + "all", m_hMin, m_hMax, m_hBin );
}

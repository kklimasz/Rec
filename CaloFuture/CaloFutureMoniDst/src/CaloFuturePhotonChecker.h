/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREPHOTONCHECKER_H
#define CALOFUTUREPHOTONCHECKER_H 1

#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "LHCbMath/Line.h"
#include "Relations/Relation.h"
#include "Relations/RelationWeighted.h"

#include "GaudiAlg/Consumer.h"

// Aliases
using Input   = LHCb::CaloHypo::Container;
using MCPs    = LHCb::MCParticles;
using Table   = Relations::RelationWeighted<LHCb::CaloCluster, LHCb::Track, float>;
using MCTable = Relations::RelationWeighted<LHCb::CaloCluster, LHCb::MCParticle, float>;
using IDTable = Relations::Relation<LHCb::CaloHypo, float>;
using Line    = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>;

// =============================================================================

/** @class CaloFuturePhotonChecker CaloFuturePhotonChecker.h
 *
 *  Photon Selection Monitoring
 *  (LHCb 2004-03)
 *
 *  @author Frederic Machefert frederic.machefert@in2p3.fr
 *  @date   2004-15-04
 */

class CaloFuturePhotonChecker final
    : public Gaudi::Functional::Consumer<void( const Input&, const MCPs&, const Table&, const MCTable&,
                                               const IDTable& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  // GaudiAlgorithm
  StatusCode initialize() override;
  StatusCode finalize() override;

  // Gaudi::Functional
  void operator()( const Input&, const MCPs&, const Table&, const MCTable&, const IDTable& ) const override;

  CaloFuturePhotonChecker( const std::string& name, ISvcLocator* pSvc );

protected:
  std::vector<AIDA::IHistogram2D*> defHisto( const unsigned int, const double, const double, const unsigned int,
                                             const std::string );

  bool isMergedPi0( const LHCb::MCParticle* ) const;

private:
  // FutureCounters
  mutable unsigned int m_nEvents     = 0;
  mutable unsigned int m_nCandidates = 0;
  mutable unsigned int m_nPhotons    = 0;
  mutable unsigned int m_nMCPhotons  = 0;
  mutable unsigned int m_nWrongs     = 0;

  // more counters
  mutable std::vector<double> m_mc_g;
  mutable std::vector<double> m_rec_bkg;
  mutable std::vector<double> m_rec_sig;
  mutable double              m_lh_mcg;
  mutable std::vector<double> m_lh_recsig;
  mutable std::vector<double> m_lh_recbkg;
  mutable double              m_lh_mcg_conv;
  mutable std::vector<double> m_lh_recsig_conv;
  mutable std::vector<double> m_lh_recsig_spd;
  mutable std::vector<double> m_lh_recbkg_spd;
  mutable double              m_lh_mcg_noconv;
  mutable std::vector<double> m_lh_recsig_noconv;
  mutable std::vector<double> m_lh_recsig_nospd;
  mutable std::vector<double> m_lh_recbkg_nospd;

  // Detector Information
  DeCalorimeter* m_ecal = nullptr;
  DeCalorimeter* m_spd  = nullptr;
  DeCalorimeter* m_prs  = nullptr;

  Gaudi::Plane3D m_ecalPlane;
  Gaudi::Plane3D m_prsPlane;
  Gaudi::Plane3D m_spdPlane;

  double m_zConv = 0.;

  // Tools
  std::string m_IDTableName = LHCb::CaloFutureIdLocation::PhotonID;

  // Particle Properties
  std::string      m_gammaName{"gamma"};
  LHCb::ParticleID m_gammaID{0};
  std::string      m_pi0Name{"pi0"};
  LHCb::ParticleID m_pi0ID{0};

  // histogramming related variables
  Gaudi::Property<bool>                m_pdf{this, "Pdf", false};
  Gaudi::Property<std::vector<double>> m_prsbin{this, "EPrsBin", {50, 0., 200.}};
  Gaudi::Property<std::vector<double>> m_chi2bin{this, "Chi2Bin", {26, 0., 104.}};
  Gaudi::Property<std::vector<double>> m_seedbin{this, "SeedBin", {50, 0., 1.}};

  // Signal/background definitions
  Gaudi::Property<float> m_etmin{this, "Etmin", 200. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_dr{this, "Dr", -1.};
  Gaudi::Property<float> m_dz{this, "Dz", -1.};
  Gaudi::Property<float> m_de{this, "DE", 0.25};
  Gaudi::Property<float> m_mergedDist{this, "MergedDist", 1.5};

  // histograms

  AIDA::IHistogram1D* m_lhSig    = nullptr;
  AIDA::IHistogram1D* m_lhSigSpd = nullptr;
  AIDA::IHistogram1D* m_lhBkg    = nullptr;
  AIDA::IHistogram1D* m_lhBkgSpd = nullptr;

  Gaudi::Property<int> m_nbinlh{this, "LhNBin", 20};
  AIDA::IHistogram2D*  m_effpur       = nullptr;
  AIDA::IHistogram2D*  m_effpur_spd   = nullptr;
  AIDA::IHistogram2D*  m_effpur_nospd = nullptr;

  Gaudi::Property<int>   m_nbinpt{this, "PtNBin", 20};
  Gaudi::Property<float> m_lhcut{this, "LhCut", 0.3};
  Gaudi::Property<float> m_ptmin{this, "PtMinHisto", 0. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_ptmax{this, "PtMaxHisto", 10. * Gaudi::Units::GeV};
  AIDA::IHistogram1D*    m_efficiency = nullptr;
  AIDA::IHistogram1D*    m_purity     = nullptr;

  std::vector<AIDA::IHistogram2D*> m_signalEPrs2D;
  std::vector<AIDA::IHistogram2D*> m_backgrEPrs2D;
  std::vector<AIDA::IHistogram2D*> m_signalChi22D;
  std::vector<AIDA::IHistogram2D*> m_backgrChi22D;
  std::vector<AIDA::IHistogram2D*> m_signalSeed2D;
  std::vector<AIDA::IHistogram2D*> m_backgrSeed2D;

  std::vector<AIDA::IHistogram2D*> m_signalEPrsSpd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrEPrsSpd2D;
  std::vector<AIDA::IHistogram2D*> m_signalChi2Spd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrChi2Spd2D;
  std::vector<AIDA::IHistogram2D*> m_signalSeedSpd2D;
  std::vector<AIDA::IHistogram2D*> m_backgrSeedSpd2D;
};

#endif // CALOFUTUREPHOTONCHECKER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureElectronNtp.h"
#include "CaloFutureMoniUtils.h"

namespace {

  /// hack to allow for tools with non-const interfaces...
  template <typename Mutex>
  class fixup_t {
    std::lock_guard<Mutex> m_l;

  public:
    fixup_t( Mutex& m ) : m_l{m} {}

    template <typename IFace>
    IFace& operator()( const ToolHandle<IFace>& iface ) {
      return const_cast<IFace&>( *iface );
    }
  };
} // namespace
//------------------------------------------------------------------------------
// Implementation file for class : CaloFutureElectronNtp
//
// 2009-12-11 : Olivier Deschamps
//------------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloFutureElectronNtp )

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloFutureElectronNtp::CaloFutureElectronNtp( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default},
                 KeyValue{"InputContainer", LHCb::ProtoParticleLocation::Charged},
                 KeyValue{"VertexLoc", LHCb::RecVertexLocation::Primary}} ) {}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloFutureElectronNtp::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  if ( !m_tracks.empty() )
    info() << "Will only look at track type(s) = " << m_tracks.value() << endmsg;
  else
    info() << "Will look at any track type" << endmsg;

  // Get, retrieve, configure tools
  m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  if ( !( m_toSpd.retrieve() && m_toPrs.retrieve() ) ) {
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }

  // set vertex location
  m_vertLoc = inputLocation<2>(); // <-- Careful with the index, 'VertexLoc' is 3rd.
  // use fallback if null (forced by user)
  if ( m_vertLoc.empty() ) m_vertLoc = m_usePV3D ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary;
  updateHandleLocation( *this, "VertexLoc", m_vertLoc );

  return StatusCode::SUCCESS;
}

//==============================================================================

// Warning, not actually a const member function, as it modifies the internal
// state of m_caloElectron. Use with care.
// Note, extra flag count to count at the specific place. Because this method
// is used on both first-electron, and second-electron, the counting should only
// be enabled in the first one only.
bool CaloFutureElectronNtp::set_and_validate( LHCb::Calo::Interfaces::IElectron& caloElectron,
                                              const LHCb::ProtoParticle& proto, bool count ) const {
  // abort if fail to set
  if ( !caloElectron.set( proto ) ) return false;
  // counting
  if ( count && m_counterStat->isQuiet() ) counter( "proto electron" ) += 1;
  // abort if no hypo obtained
  const auto hypo = caloElectron.electron();
  if ( !hypo ) return false;
  // abort if null track
  const auto track = proto.track();
  if ( !track ) return false;
  // abort if not matching track whitelist
  if ( !m_tracks.empty() ) {
    const auto ttype = proto.track()->type();
    if ( !std::any_of( m_tracks.begin(), m_tracks.end(), [ttype]( int itype ) { return itype == ttype; } ) ) {
      return false;
    }
  }
  // Abort if poor energy
  LHCb::CaloMomentum mmt( hypo );
  const double       e  = mmt.e();
  const double       et = mmt.pt();
  if ( !inRange( m_et, et ) ) return false;
  if ( !inRange( m_e, e ) ) return false;
  const double ePrs = proto.info( LHCb::ProtoParticle::additionalInfo::CaloPrsE, 0. );
  if ( !inRange( m_prs, ePrs ) ) return false;
  const double eOp = caloElectron.eOverP();
  if ( !inRange( m_eop, eOp ) ) return false;
  // finally
  if ( count && m_counterStat->isQuiet() ) counter( "Selected electron" ) += 1;
  return true;
}

// Calculated the squared of the invar mass of two tracks
// The result can be negative.
double CaloFutureElectronNtp::invar_mass_squared( const LHCb::Track* t1, const LHCb::Track* t2 ) const {
  auto st1 = t1->firstState();
  auto st2 = t2->firstState();
  auto sc  = m_extrapolator->propagate( st1, 0. );
  if ( sc.isFailure() ) Warning( "Propagation 1 failed" ).ignore();
  sc = m_extrapolator->propagate( st2, 0. );
  if ( sc.isFailure() ) Warning( "Propagation 2 failed" ).ignore();
  const auto p1 = st1.momentum();
  const auto p2 = st2.momentum();
  double     m2 = p1.R() * p2.R();
  m2 -= p1.X() * p2.X();
  m2 -= p1.Y() * p2.Y();
  m2 -= p1.Z() * p2.Z();
  m2 *= 2;
  return m2;
}

//==============================================================================
// Main execution
//==============================================================================

void CaloFutureElectronNtp::operator()( const ODIN& odin, const Protos& protos, const Vertices& verts ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // GET ODIN INFO
  m_odin->getTime();
  ulonglong evt = odin.eventNumber();
  int       run = odin.runNumber();
  int       tty = odin.triggerType();

  // FIXME: a number of private tools is NOT thread safe. So we 'just' brute-force it, and take a lock here...
  fixup_t fixup{m_badtoolmutex};

  // Loop over each protoparticle
  for ( auto p = protos.begin(); protos.end() != p; ++p ) {
    const auto proto = *p;
    if ( !proto ) continue;
    if ( !set_and_validate( fixup( m_caloElectron ), *proto, true ) ) continue;

    // Retrieve info for tuple
    const auto ePrs         = proto->info( LHCb::ProtoParticle::additionalInfo::CaloPrsE, 0. );
    const auto track1       = proto->track();
    const auto hypo         = m_caloElectron->electron();
    const auto iSpd         = (int)fixup( m_toSpd ).multiplicity( *hypo, CaloCellCode::CaloIndex::SpdCalo );
    const auto track1mmt    = momentum( track1 );
    const auto caloCluster  = firstCluster( hypo );
    const auto caloCellId   = caloCluster->seed();
    const auto caloState    = m_caloElectron->caloState();
    const auto caloStateMmt = momentum( caloState );
    const auto brem         = m_caloElectron->bremstrahlung();
    const auto bremCellId   = firstCluster( brem )->seed();
    const auto bremMmt      = momentum( m_caloElectron->bremCaloFutureMomentum() );
    const auto eOp          = m_caloElectron->eOverP();
    const auto e            = LHCb::CaloMomentum( hypo ).e();

    // dielectron filter
    double mas = 999999.;
    if ( m_pairing ) {
      for ( auto pp = p + 1; protos.end() != pp; ++pp ) {
        const auto proto2 = *pp;
        if ( !proto2 ) continue;
        if ( !set_and_validate( fixup( m_caloElectron ), *proto2 ) ) continue;
        // abort if same pair (shouldn't happen)
        const auto hypo2 = m_caloElectron->electron();
        if ( hypo == hypo2 ) continue;
        // Need opposite charge
        const auto t1 = proto->track();
        const auto t2 = proto2->track();
        if ( -1 != t1->charge() * t2->charge() ) continue;
        // compute mass
        const auto m2 = invar_mass_squared( t1, t2 );
        if ( m2 > 0 ) {
          const auto m = sqrt( m2 );
          if ( m < mas ) mas = m;
        }
      }
    }
    if ( mas > 0 && mas < 100 && m_counterStat->isQuiet() ) counter( "Selected (di)electron" ) += 1;

    // proto info
    if ( m_tuple ) {
      auto ntp = nTuple( 500, "e_tupling", CLID_ColumnWiseTuple );
      ntp->column( "Spd", iSpd );
      ntp->column( "Prs", ePrs );
      ntp->column( "TrackMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 9999. ) );
      ntp->column( "ElecMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 9999. ) );
      ntp->column( "BremMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 9999. ) );
      ntp->column( "TrajectoryL", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 9999. ) );
      ntp->column( "VeloCharge", proto->info( LHCb::ProtoParticle::additionalInfo::VeloCharge, -1. ) );
      ntp->column( "DLLe", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLe, 0. ) );
      ntp->column( "RichDLLe", proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLe, 0. ) );
      // hypo info
      ntp->column( "EoP", eOp );
      ntp->column( "HypoE", e );
      ntp->column( "HypoR", position3d( hypo ) );
      ntp->column( "HypoTheta", position3d( hypo ).theta() );
      // track info
      ntp->column( "TrackP", track1mmt );
      ntp->column( "TrackR", position3d( caloState ) );
      ntp->column( "caloState", caloStateMmt );
      ntp->column( "incidence", caloStateMmt.theta() );
      ntp->column( "incidenceX", atan2( caloStateMmt.X(), caloStateMmt.Z() ) );
      ntp->column( "incidenceY", atan2( caloStateMmt.Y(), caloStateMmt.Z() ) );
      ntp->column( "trackType", track1->type() );
      ntp->column( "trackProb", track1->probChi2() );
      // cluster info
      ntp->column( "id", caloCellId.index() );
      ntp->column( "ClusterE", caloCluster->e() );
      ntp->column( "ClusterR", position3d( caloCluster ) );
      // brem info
      ntp->column( "BremId", bremCellId.index() );
      ntp->column( "BremP", bremMmt );
      // odin info
      ntp->column( "run", run );
      ntp->column( "event", (double)evt );
      ntp->column( "triggertype", tty );
      if ( m_pairing ) ntp->column( "MinMee", mas );
      ntp->write();
    }

    // histogramming / channel
    const auto trmatch = proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 9999. );
    if ( m_histo && iSpd != 0 && trmatch < 25 ) {
      fillH( eOp, track1mmt, caloCellId );
      if ( m_pairing && mas > 0. && mas < 100. ) fillH( eOp, track1mmt, caloCellId, "conversion/" );
    }

    // vertices
    const auto nVert = verts.size();
    if ( m_counterStat->isQuiet() ) counter( "#PV=" + Gaudi::Utils::toString( nVert ) + " [" + m_vertLoc + "]" ) += 1;

    if ( m_trend ) {
      std::string sNpv = "PV" + Gaudi::Utils::toString( nVert ) + "/";
      std::string sRun = "r" + Gaudi::Utils::toString( run ) + "/";
      std::string base = "Trend/";
      plot1D( eOp, base + "allPV/allRun/eOp", "e/p spectrum for all run & allPV", 0., 2.5, 250 );
      plot1D( eOp, base + "allPV/" + sRun + "eOp", "e/p spectrum for run = " + sRun, 0., 2.5, 250 );
      plot1D( eOp, base + sNpv + sRun + "eOp", "e/p spectrum for PV=" + sNpv + " (run = " + sRun + ")", 0., 2.5, 250 );
      plot1D( eOp, base + sNpv + "allRun/eOp", "e/p spectrum for PV=" + sNpv + " (all run)", 0., 2.5, 250 );
    }
  }
}
//=============================================================================

void CaloFutureElectronNtp::fillH( double eOp, Gaudi::LorentzVector t, LHCb::CaloCellID id, std::string hat ) const {
  if ( !m_histo ) return;
  std::string zone = id.areaName();
  plot1D( eOp, hat + "all/eOp", "all/eOp", 0., 3., 300 );
  plot1D( eOp, hat + zone + "/eOp", zone + "/eOp", 0., 3., 300 );
  if ( m_splitFEBs ) {
    std::ostringstream sid;
    int                feb = m_calo->cardNumber( id );
    sid << hat << "crate" << format( "%02i", m_calo->cardCrate( feb ) ) << "/"
        << "feb" << format( "%02i", m_calo->cardSlot( feb ) ) << "/"
        << Gaudi::Utils::toString( m_calo->cardColumn( id ) + nColCaloCard * m_calo->cardRow( id ) );
    plot1D( eOp, sid.str(), sid.str(), 0., 3., 300 );
  }
  if ( m_splitE && t.P() < 200. * Gaudi::Units::GeV ) { // put a limit at 200 GeV
    int                ebin = ( t.P() / 5000. ) * 5;    // 1 GeV binning
    std::ostringstream sid;
    sid << "/E_" << ebin << "/eOp";
    if ( m_counterStat->isQuiet() ) counter( sid.str() + "[<p>]" ) += t.P();
    plot1D( eOp, hat + zone + sid.str(), zone + sid.str(), 0., 3., 300 );
    plot1D( eOp, hat + "all" + sid.str(), "all" + sid.str(), 0., 3., 300 );
  }
}

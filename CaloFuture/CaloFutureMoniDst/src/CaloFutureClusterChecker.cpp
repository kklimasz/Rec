/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloFutureMoniAlg.h"
#include "Event/CaloCluster.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/Consumer.h"
#include "Relations/RelationWeighted1D.h"

// =============================================================================

/** @class CaloFutureClusterChecker CaloFutureClusterChecker.cpp
 *
 *  The algorithm for trivial monitoring of MCTruthing of
 *  "CaloClusters" with Tracks.
 *  It produces 5 histograms:
 *
 *  <ol>
 *  <li> Total Link              distribution               </li>
 *  <li> Link multiplicity       distribution               </li>
 *  <li> Minimal Weight          distribution               </li>
 *  <li> Maximal Weight          distribution               </li>
 *  <li>         Weight          distribution               </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input    = LHCb::RelationWeighted1D<LHCb::CaloCluster, LHCb::MCParticle, float>;
using Clusters = LHCb::CaloCluster::Container;

class CaloFutureClusterChecker final
    : public Gaudi::Functional::Consumer<void( const Input&, const Clusters&, const Clusters& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const Clusters&, const Clusters& ) const override;

  CaloFutureClusterChecker( const std::string& name, ISvcLocator* pSvcLocator );
};

// =============================================================================

DECLARE_COMPONENT( CaloFutureClusterChecker )

// =============================================================================

CaloFutureClusterChecker::CaloFutureClusterChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", "Relations/" + LHCb::CaloClusterLocation::Default},
                 KeyValue{"InputClusters", LHCb::CaloClusterLocation::Ecal},
                 KeyValue{"InputSplitClusters", LHCb::CaloClusterLocation::EcalSplit}} ) {}

// =============================================================================

StatusCode CaloFutureClusterChecker::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  hBook1( "1", "log10(#Links+1)  '" + inputLocation() + "'", 0, 5 );
  hBook1( "2", "Rels/Cluster     '" + inputLocation() + "'", 0, 50 );
  hBook1( "3", "Minimal weight/e '" + inputLocation() + "'", 0, 2.0 );
  hBook1( "4", "Maximal weight/e '" + inputLocation() + "'", 0, 2.0 );
  hBook1( "5", "        Weight/e '" + inputLocation() + "'", 0, 2.0 );
  if ( m_split ) {
    Warning( "No area spliting allowed for CaloFutureClusterChecker" ).ignore();
    m_split = false;
  }
  return StatusCode::SUCCESS;
}

// =============================================================================

void CaloFutureClusterChecker::operator()( const Input& table, const Clusters& clusters1,
                                           const Clusters& clusters2 ) const {
  // produce histos ?
  if ( !produceHistos() ) return;

  // total number of links
  hFill1( "1", log10( table.relations().size() + 1. ) );

  // loop over clusters
  auto filler = [&table, this]( auto& clusters ) {
    for ( const auto& cluster : clusters ) {
      const auto& range = table.relations( cluster );
      // number of relations per cluster
      hFill1( "2", range.size() );
      if ( range.empty() ) continue;
      // cluster energy
      const double e = cluster->e();
      // minimal weight
      hFill1( "3", range.front().weight() / e );
      // maximal weight
      hFill1( "4", range.back().weight() / e );
      // all weights
      for ( const auto& relation : range ) { hFill1( "5", relation.weight() / e ); }
    } // end of loop over clusters
  };

  filler( clusters1 );
  filler( clusters2 );

  return;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureInterfaces/CaloFutureMatch3D.h"
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "CaloFutureTrackMatch.h"
#include "CaloMatchUtils.h"
#include "Event/CaloPosition.h"
#include "Event/Track.h"
#include "GaudiKernel/GaudiException.h"
#include <tuple>
#include <variant>

// ============================================================================
/** @file
 *  Implementation file for class CaloFutureElectronMatch
 *  @date 2006-05-29
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
namespace LHCb::Calo {
  class ElectronMatch final : public virtual Interfaces::ITrackMatch, public TrackMatch {
  public:
    ElectronMatch( const std::string& type, const std::string& name, const IInterface* parent );

    /// initialize the tool
    StatusCode initialize() override;

    /** the main matching method
     *  @see ICaloFutureTrackMatch
     *  @param calo_obj "calorimeter" object (position)
     *  @param track_obj tracking object (track)
     *  @param old_match_results match results from last iteration step
     */
    Interfaces::ITrackMatch::MatchResults
    match( const CaloPosition& calo_obj, const Track& track_obj,
           const Interfaces::ITrackMatch::MatchResults& old_match_results ) const override;

  private:
    Gaudi::Plane3D m_showerMax;
  };

  // ============================================================================

  ElectronMatch::ElectronMatch( const std::string& type, const std::string& name, const IInterface* parent )
      : TrackMatch( type, name, parent ) {
    declareInterface<Interfaces::ITrackMatch>( this );
    _setProperty( "Calorimeter", DeCalorimeterLocation::Ecal );
    _setProperty( "Tolerance", "5" ); // 5 millimeters
  }

  StatusCode ElectronMatch::initialize() {
    StatusCode sc = TrackMatch::initialize();
    if ( sc.isFailure() ) { return sc; }
    m_showerMax = calo()->plane( CaloPlane::ShowerMax );
    return StatusCode::SUCCESS;
  }

  Interfaces::ITrackMatch::MatchResults
  ElectronMatch::match( const CaloPosition& calo_obj, const Track& track_obj,
                        const Interfaces::ITrackMatch::MatchResults& old_match_results ) const {
    Interfaces::ITrackMatch::MatchResults match_result;
    match_result.chi2_value = bad();
    match_result.plane      = old_match_results.plane;
    match_result.state      = old_match_results.state;

    Match3D calo_match;

    if ( old_match_results.is_new_calo_obj ) {
      calo_match = getMatch3D( calo_obj );
      if ( !calo_match ) {
        if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): Error from fill(3D) -- " << endmsg; }
        /** According to old code all calls for this one calo objecte will be canceled.
           We send here signal: stop calling match for this calo object and load next one
        **/
        match_result.skip_this_calo = true;
        return match_result;
      }

      // find the proper plane in detector
      const CaloPosition::Center& par = calo_obj.center();
      const Gaudi::XYZPoint       point( par( 0 ), par( 1 ), calo_obj.z() );
      if ( tolerance() < match_result.plane.Distance( point ) ) { match_result.plane = calo()->plane( point ); }
    } else {
      calo_match = std::get<Match3D>( old_match_results.matrix );
    }

    match_result.matrix = calo_match;

    // getting the correct state
    const State* ptr_state = TrackTool::state( track_obj, State::Location::ECalShowerMax );

    if ( !ptr_state ) {
      StatusCode sc = propagate( track_obj, m_showerMax, match_result.state );
      if ( sc.isFailure() ) {
        if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): failure from propagate (1) " << endmsg; }
        return match_result;
      }

      match_result.state.setLocation( State::Location::ECalShowerMax );
      // We want to use additional state, but without modification of original one - so we have to copy it and use here.
      Track track_with_additional_state( track_obj );
      track_with_additional_state.addToStates( match_result.state );
      ptr_state = TrackTool::state( track_with_additional_state, State::Location::ECalShowerMax );
    }

    // check the validity of the state
    if ( tolerance() < ::fabs( match_result.plane.Distance( ptr_state->position() ) ) ) {
      match_result.state = *ptr_state;
      StatusCode sc      = propagate( match_result.state, match_result.plane );
      if ( sc.isFailure() ) {
        if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): failure from propagate (2) " << endmsg; }
        return match_result;
      }

      ptr_state = &match_result.state;
    }

    Assert( ptr_state != nullptr, "LHCb::State* points to NULL!" );

    auto track_match = getTrackMatch3D( *ptr_state );
    if ( !track_match ) {
      if ( msgLevel( MSG::WARNING ) ) { warning() << "match(): error for fill(3D)" << endmsg; }
      /** According to old code this only means that matching failed so we cant calculate in proper way a chi2 value
       **/
      return match_result;
    }

    // make a real evaluation
    match_result.match_successful = true;
    match_result.chi2_value       = TrackMatch::chi2( calo_match, track_match );
    return match_result;
  }
} // namespace LHCb::Calo
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::ElectronMatch, "CaloFutureElectronMatch" )

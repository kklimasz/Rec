/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrack2IDAlg.h"
#include <type_traits>

// ============================================================================

namespace LHCb::Calo {
  // ============================================================================
  /// Standard protected constructor
  // ============================================================================

  Track2IDAlg::Track2IDAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     {KeyValue{"Inputs", LHCb::CaloFutureAlgUtils::TrackLocations().front()}, KeyValue{"Filter", ""},
                      KeyValue{"Digits", ""}},
                     KeyValue{"Output", ""} ) {
    setProperty( "StatPrint", "false" ).ignore();
    // track types:
    setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                                              LHCb::Track::Types::Ttrack ) )
        .ignore();
  }

  // ============================================================================
  /// standard algorithm execution
  // ============================================================================
  Table Track2IDAlg::operator()( const LHCb::Tracks& tracks, const Filter& filter,
                                 const LHCb::CaloDigits& digits ) const {
    static_assert( std::is_base_of_v<LHCb::CaloFuture2Track::ITrEvalTable, Table>,
                   "Table must inherit from ITrEvalTable" );

    Table table( 100 );

    // loop over all tracks
    for ( const auto& track : tracks ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        continue;
      }
      // use filter ?
      const auto r = filter.relations( track );
      // no positive information? skip!
      if ( r.empty() || !r.front().to() ) { continue; } // CONTINUE
      auto value = m_tool->process( *track, digits );
      // make a relations (fast, efficient, call for i_sort is mandatory!)
      if ( value ) table.i_push( track, value.value() ); // NB! i_push
    }
    // MANDATORY after i_push! // NB! i_sort
    table.i_sort();

    m_nTracks += tracks.size();
    auto links = table.i_relations();
    m_nLinks += links.size();
    // for( const auto& link : links) m_nEnergy += link.to() / Gaudi::Units::GeV;
    auto c = m_nEnergy.buffer();
    for ( const auto& link : links ) c += link.to() / Gaudi::Units::GeV;

    return table;
  }
} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Track2IDAlg, "CaloFutureTrack2IDAlg" )

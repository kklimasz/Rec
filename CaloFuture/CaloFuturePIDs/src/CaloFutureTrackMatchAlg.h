/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURETRACKMATCHALG_H
#define CALOFUTURETRACKMATCHALG_H 1

// Include files
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloFutureTrackMatchAlg CaloFutureTrackMatchAlg.h
 *
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-16
 */

namespace LHCb::Calo {
  using Filter = LHCb::Relation1D<LHCb::Track, bool>;

  template <typename TABLE, typename CALOFUTURETYPES>
  class TrackMatchAlg
      : public Gaudi::Functional::Transformer<TABLE( const LHCb::Tracks&, const CALOFUTURETYPES&, const Filter& )> {
    static_assert( std::is_base_of_v<LHCb::CaloFuture2Track::ITrAccTable, Filter>,
                   "Filter must inherit from ITrAccTable" );

  public:
    using base_type =
        Gaudi::Functional::Transformer<TABLE( const LHCb::Tracks&, const CALOFUTURETYPES&, const Filter& )>;
    using KeyValue = typename base_type::KeyValue;
    using base_type::debug;
    using base_type::msgLevel;
    using base_type::setProperty;

    // standard constructor
    TrackMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // standard execution
    TABLE operator()( const LHCb::Tracks& tracks, const CALOFUTURETYPES& calos, const Filter& filter ) const override;

  protected:
    Gaudi::Property<float>            m_threshold{this, "Threshold", 10000., "threshold"};
    Gaudi::Property<int>              m_tablesize{this, "TableSize", 100, "table size"};
    Gaudi::Property<std::vector<int>> m_type{this, "AcceptedType", {}, "Accepted tracks types"};

    // the tool for matching
    const ToolHandle<Interfaces::ITrackMatch> m_tool{this, "Tool", "<NOT DEFINED>"};

    // a bit of statistics
    mutable Gaudi::Accumulators::StatCounter<>      m_nMatchFailure{this, "#match failure"};
    mutable Gaudi::Accumulators::StatCounter<>      m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>      m_nTracks{this, "#good tracks"};
    mutable Gaudi::Accumulators::StatCounter<>      m_nCalos{this, "#total calos"};
    mutable Gaudi::Accumulators::StatCounter<>      m_nOverflow{this, "#above threshold"};
    mutable Gaudi::Accumulators::StatCounter<float> m_chi2{this, "#chi2"};

  private:
    const LHCb::CaloPosition* position( const LHCb::CaloCluster* c ) const { return &c->position(); }
    const LHCb::CaloPosition* position( const LHCb::CaloHypo* c ) const { return c->position(); }
  };
} // namespace LHCb::Calo
// ============================================================================
#endif // CALOFUTURETRACKMATCHALG_H

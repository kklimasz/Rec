/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class FutureEcalPIDmuAlg  FutureEcalPIDmuAlg.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================
namespace LHCb::Calo {
  class EcalPIDmuAlg final : public ID2DLL {
  public:
    EcalPIDmuAlg( const std::string& name, ISvcLocator* pSvc ) : ID2DLL( name, pSvc ) {
      using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

      updateHandleLocation( *this, "Input", CaloFutureIdLocation( "EcalE" ) );
      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "EcalPIDmu" ) );

      setProperty( "nVlong", Gaudi::Utils::toString( 5 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nVdown", Gaudi::Utils::toString( 5 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nVTtrack", Gaudi::Utils::toString( 5 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMlong", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMdown", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMTtrack", Gaudi::Utils::toString( 25 * Gaudi::Units::GeV ) ).ignore();

      setProperty( "HistogramL", "DLL_Long" ).ignore();
      setProperty( "HistogramD", "DLL_Downstream" ).ignore();
      setProperty( "HistogramT", "DLL_Ttrack" ).ignore();
      setProperty( "ConditionName", "Conditions/ParticleID/Calo/EcalPIDmu" ).ignore();

      setProperty( "HistogramL_THS", "CaloFuturePIDs/CALO/ECALPIDM/h3" ).ignore();
      setProperty( "HistogramD_THS", "CaloFuturePIDs/CALO/ECALPIDM/h5" ).ignore();
      setProperty( "HistogramT_THS", "CaloFuturePIDs/CALO/ECALPIDM/h6" ).ignore();

      setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                                LHCb::Track::Types::Downstream ) )
          .ignore();
    };
  };
} // namespace LHCb::Calo
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::EcalPIDmuAlg, "FutureEcalPIDmuAlg" )

// ============================================================================

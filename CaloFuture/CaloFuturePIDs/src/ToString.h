/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef CALOFUTUREPIDS_TOSTRING_H
#define CALOFUTUREPIDS_TOSTRING_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include "GaudiKernel/ToStream.h"
#include <sstream>
#include <vector>
// ============================================================================
namespace Gaudi {
  namespace Utils {
    inline std::string toString( const std::string& o ) { return "\"" + o + "\""; }
  } // end of namespace Utils
} // end of namespace Gaudi
// ============================================================================

// ============================================================================
// The END
// ============================================================================
#endif // CALOFUTUREPIDS_TOSTRING_H

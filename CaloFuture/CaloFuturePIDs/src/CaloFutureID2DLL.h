/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREPIDS_CALOFUTUREID2DLL_H
#define CALOFUTUREPIDS_CALOFUTUREID2DLL_H 1

// Include files
#include "AIDA/IHistogram2D.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/Condition.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/VectorMap.h"
#include "Relations/IRelation.h"
#include "Relations/Relation1D.h"
#include "TH2D.h"
#include "ToString.h"
#include "ToVector.h"
#include <cmath>

class ITHistSvc;

// ============================================================================
/** @class CaloFutureID2DLL CaloFutureID2DLL.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-18
 */
// ============================================================================
namespace LHCb::Calo {
  using Table = Relation1D<Track, float>;

  class ID2DLL : public Gaudi::Functional::Transformer<Table( const Table& )> {
  public:
    ID2DLL( const std::string& name, ISvcLocator* pSvc );

    StatusCode initialize() override;
    Table      operator()( const Table& input ) const override;

    /// callback function invoked by the UpdateManagerSvc
    StatusCode i_updateDLL();
    StatusCode dumpDLLsToFile();

  protected:
    inline double dLL( const double p, const double v, const Track::Types t ) const;

  private:
    StatusCode initializeWithCondDB();
    StatusCode initializeWithoutCondDB();

  protected:
    // histogram title
    Gaudi::Property<std::string> m_title_lt{this, "HistogramL", "", "histogram title long"};
    Gaudi::Property<std::string> m_title_dt{this, "HistogramD", "", "histogram title down"};
    Gaudi::Property<std::string> m_title_tt{this, "HistogramT", "", "histogram title TTrack"};
    Gaudi::Property<std::string> m_title_ut{this, "HistogramU", "", "histogram title upstr"};
    Gaudi::Property<std::string> m_title_vt{this, "HistogramV", "", "histogram title velo"};

    Gaudi::Property<std::string> m_title_lt_ths{this, "HistogramL_THS", "",
                                                "histogram title long if DLLs are read from a root file via THS"};
    Gaudi::Property<std::string> m_title_dt_ths{this, "HistogramD_THS", "",
                                                "histogram title down if DLLs are read from a root file via THS"};
    Gaudi::Property<std::string> m_title_tt_ths{this, "HistogramT_THS", "",
                                                "histogram title TTrack if DLLs are read from a root file via THS"};
    Gaudi::Property<std::string> m_title_ut_ths{this, "HistogramU_THS", "",
                                                "histogram title upstr if DLLs are read from a root file via THS"};
    Gaudi::Property<std::string> m_title_vt_ths{this, "HistogramV_THS", "",
                                                "histogram title velo if DLLs are read from a root file via THS"};

    Gaudi::Property<float> m_pScale_lt{this, "nMlong", -1, "scale for mom long"};
    Gaudi::Property<float> m_pScale_dt{this, "nMdown", -1, "scale for mom down"};
    Gaudi::Property<float> m_pScale_tt{this, "nMTtrack", -1, "scale for mom TT"};
    Gaudi::Property<float> m_pScale_ut{this, "nMupstr", -1, "scale for val upstr"};
    Gaudi::Property<float> m_pScale_vt{this, "nMvelo", -1, "scale for val velo"};

    Gaudi::Property<float> m_vScale_lt{this, "nVlong", -1, "scale for val long"};
    Gaudi::Property<float> m_vScale_dt{this, "nVdown", -1, "scale for val down"};
    Gaudi::Property<float> m_vScale_tt{this, "nVTtrack", -1, "scale for val TT"};
    Gaudi::Property<float> m_vScale_ut{this, "nVupstr", -1, "scale for val upstr"};
    Gaudi::Property<float> m_vScale_vt{this, "nVvelo", -1, "scale for val velo"};

    Gaudi::Property<std::vector<int>> m_type{this, "AcceptedType", {}, "Accepted tracks types"};

  private:
    const TH2D* m_histo_lt = nullptr;
    const TH2D* m_histo_dt = nullptr;
    const TH2D* m_histo_tt = nullptr;
    const TH2D* m_histo_ut = nullptr;
    const TH2D* m_histo_vt = nullptr;

    const ToolHandle<IFutureCounterLevel> counterStat{"FutureCounterLevel"};
    Gaudi::Property<bool>                 m_useCondDB{this, "UseCondDB", true,
                                      "if true - use CondDB, otherwise get the DLLs via THS from a root file"};

    Gaudi::Property<std::string> m_conditionName{this, "ConditionName"};
    Condition*                   m_cond = nullptr;
  };

  // ============================================================================

  inline double ID2DLL::dLL( const double p, const double v, const Track::Types t ) const {
    const TH2D* histo  = nullptr;
    double      pScale = -1.;
    double      vScale = -1.;

    switch ( t ) {
    case Track::Types::Long:
      histo  = m_histo_lt;
      pScale = m_pScale_lt;
      vScale = m_vScale_lt;
      break;
    case Track::Types::Downstream:
      histo  = m_histo_dt;
      pScale = m_pScale_dt;
      vScale = m_vScale_dt;
      break;
    case Track::Types::Upstream:
      histo  = m_histo_ut;
      pScale = m_pScale_ut;
      vScale = m_vScale_ut;
      break;
    case Track::Types::Velo:
      histo  = m_histo_vt;
      pScale = m_pScale_vt;
      vScale = m_vScale_vt;
      break;
    case Track::Types::Ttrack:
      histo  = m_histo_tt;
      pScale = m_pScale_tt;
      vScale = m_vScale_tt;
      break;
    default:
      Error( "Invalid track type, return 0" ).ignore();
      return 0;
    }

    if ( 0 == histo ) {
      Error( "Histogram is not specified return 0" ).ignore();
      return 0;
    }

    const double _x = ::tanh( p / pScale );
    const double _y = ::tanh( v / vScale );
    const int    ii = const_cast<TH2D*>( histo )->FindBin( _x, _y );
    return histo->GetBinContent( ii );
  }
} // namespace LHCb::Calo
// ============================================================================
#endif // CALOFUTUREID2DLL_H

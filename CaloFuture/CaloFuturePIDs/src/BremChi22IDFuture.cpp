/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class BremChi22IDFuture BremChi22IDFuture.cpp
 *  The preconfigured instance of class CaloFutureChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
namespace LHCb::Calo {
  using TABLEI = RelationWeighted2D<CaloHypo, Track, float>;
  using TABLEO = Relation1D<Track, float>;

  struct BremChi22ID final : public Chi22ID<TABLEI, TABLEO> {
    static_assert( std::is_base_of_v<CaloFuture2Track::IHypoTrTable2D, TABLEI>,
                   "TABLEI must inherit from IHypoTrTable2D" );
    BremChi22ID( const std::string& name, ISvcLocator* pSvc ) : Chi22ID<TABLEI, TABLEO>( name, pSvc ) {
      using CaloFutureAlgUtils::CaloFutureIdLocation;
      updateHandleLocation( *this, "Input", CaloFutureIdLocation( "BremMatch" ) );
      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "BremChi2" ) );
      // @todo it must be in agrement with "Threshold" for BremMatchAlgFuture
      setProperty( "CutOff", "10000" ).ignore();
      // track types:
      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( Track::Types::Velo, Track::Types::Long, Track::Types::Upstream ) )
          .ignore();
    };
  };
} // namespace LHCb::Calo
// ============================================================================
// DECLARE_COMPONENT( LHCb::Calo::BremChi22ID)
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::BremChi22ID, "BremChi22IDFuture" ) // for backwards compatibility

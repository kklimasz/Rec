/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureEnergyForTrack.h"

// ============================================================================
/** @class FutureHcalEnergyForTrack
 *  The concrete preconfigured insatnce for CaloFutureEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
namespace LHCb::Calo {
  struct HcalEnergyForTrack final : public EnergyForTrack {
    HcalEnergyForTrack( const std::string& type, const std::string& name, const IInterface* parent )
        : EnergyForTrack( type, name, parent ) {
      setProperty( "Calorimeter", DeCalorimeterLocation::Hcal ).ignore();
      setProperty( "MorePlanes", 5 ).ignore();
      setProperty( "AddNeigbours", 0 ).ignore();
      setProperty( "Tolerance", 15.0 * Gaudi::Units::mm ).ignore();
    }
  };
} // namespace LHCb::Calo

// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::HcalEnergyForTrack, "FutureHcalEnergyForTrack" )

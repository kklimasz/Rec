/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREUTILS_CALOFUTURE_CALOFUTURETRACKTOOL_H
#define CALOFUTUREUTILS_CALOFUTURE_CALOFUTURETRACKTOOL_H 1

// Include files
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/TrackFunctor.h"
#include "Event/TrackUse.h"
#include "GaudiAlg/GaudiTool.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include <algorithm>
#include <string>
#include <vector>

//==============================================================================

namespace LHCb::Calo {
  /** @class CaloFuture::CaloFutureTrackTool CaloFutureTrackTool.h
   *
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-05-28
   */
  class TrackTool : public GaudiTool {
  public:
    using TrackTypes = std::vector<LHCb::Track::Types>;
    // type of line in 3D
    using Line = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>;

    /// initialize the tool
    StatusCode initialize() override;

    // Internal version. Raise warning if failure
    void _setProperty( const std::string& p, const std::string& v );

  protected:
    /// standard constructor
    TrackTool( const std::string& type, const std::string& name, const IInterface* parent );

    /// Propagate track to a given 3D-place
    StatusCode propagate( const LHCb::Track& track, const Gaudi::Plane3D& plane, LHCb::State& state,
                          const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const;

    /// Propagate state to a given 3D-place
    StatusCode propagate( LHCb::State& state, const Gaudi::Plane3D& plane,
                          const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const;

    /// Propagate state to a given Z
    StatusCode propagate( LHCb::State& state, const double z, const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const;

    /// construct the straight line from the state
    inline Line line( const LHCb::State& state ) const { return Line( state.position(), state.slopes() ); };

    /** get  a pointer to the satte for the given track at given location
     *  it shodul be faster then double usage of
     *  LHCb::Track::hasStateAt ( location )  and LHCb::stateAt ( location )
     *  In addition it scans the list of states fro the end -
     *  it is good for calorimeter
     */
    inline const LHCb::State* state( const LHCb::Track& track, const LHCb::State::Location loc ) const;

    /// check if the track to be used @see TrackUse
    bool use( const LHCb::Track* track ) const;

    /// print the short infomration about track flags
    inline MsgStream& print( MsgStream& stream, const LHCb::Track* track ) const;

    /// print the short infomration about track flags
    inline MsgStream& print( const LHCb::Track* track, const MSG::Level level = MSG::INFO ) const;

    double               tolerance() const { return m_tolerance; }
    const std::string&   detectorName() const { return m_detectorName; }
    const DeCalorimeter* calo() const { return m_calo; }

  private:
    // extrapolator
    ToolHandle<ITrackExtrapolator> m_extrapolator{this, "Extrapolator", "TrackRungeKuttaExtrapolator/Regular",
                                                  "Runge Kutta extrapolator"};

    // fast extrapolator
    ToolHandle<ITrackExtrapolator> m_fastExtrapolator{this, "FastExtrapolator", "TrackLinearExtrapolator/Linear",
                                                      "Linear extrapolator"};

    Gaudi::Property<float> m_fastZ{this, "zForFastExtrapolator", 10.0 * Gaudi::Units::meter,
                                   "z-position of 'linear' extrapolation"};

    Gaudi::Property<float> m_tolerance{this, "Tolerance", 2.0 * Gaudi::Units::mm, "plane extrapolation tolerance"};

    Gaudi::Property<float> m_cosTolerance{this, "CosTolerance", ::cos( 0.1 * Gaudi::Units::mrad ),
                                          "plane extrapolation angular tolerance"};

    Gaudi::Property<unsigned int> m_maxIter{this, "MaxPlaneIterations", 5, "maximal number of iterations"};

    Gaudi::Property<std::string> m_detectorName{this, "Calorimeter"};

    // detector element
    const DeCalorimeter* m_calo = nullptr;

    // track selector
    TrackUse m_use{*this};
  };

  // ============================================================================
  // print the short infomration about track flags
  // ============================================================================

  inline MsgStream& TrackTool::print( MsgStream& stream, const LHCb::Track* track ) const {
    return stream.isActive() ? m_use.print( stream, track ) : stream;
  }

  // ============================================================================
  // print the short infomration about track flags
  // ============================================================================

  inline MsgStream& TrackTool::print( const LHCb::Track* track, const MSG::Level level ) const {
    return print( msgStream( level ), track );
  }

  // ============================================================================
  // get  a pointer to the state for the given track at given location
  // ============================================================================

  inline const LHCb::State* TrackTool::state( const LHCb::Track& track, const LHCb::State::Location loc ) const {
    const auto& states = track.states();
    // loop in reverse order: for calo should be a bit more efficient
    auto found =
        std::find_if( states.rbegin(), states.rend(), [&]( const LHCb::State* s ) { return s->checkLocation( loc ); } );
    //
    return found != states.rend() ? *found : nullptr; // RETURN
  }
} // namespace LHCb::Calo

// ============================================================================
#endif // CALOFUTUREUTILS_CALOFUTURE_CALOFUTURETRACKTOOL_H

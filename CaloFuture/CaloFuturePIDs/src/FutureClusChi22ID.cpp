/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureChi22ID.h"
#include "ToVector.h"

// ============================================================================
namespace LHCb::Calo {
  using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
  using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

  struct ClusChi22ID final : public Chi22ID<TABLEI, TABLEO> {
    static_assert( std::is_base_of_v<LHCb::CaloFuture2Track::IClusTrTable2D, TABLEI>,
                   "TABLEI must inherit from IClusTrTable2D" );

    ClusChi22ID( const std::string& name, ISvcLocator* pSvc ) : Chi22ID<TABLEI, TABLEO>( name, pSvc ) {
      using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
      updateHandleLocation( *this, "Input", CaloFutureIdLocation( "ClusterMatch" ) );
      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "ClusChi2" ) );
      // @todo it must be in agrement with "Threshold" for PhotonMatchAlg
      setProperty( "CutOff", "1000" ).ignore(); //
      // track types:
      setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                                LHCb::Track::Types::Downstream ) )
          .ignore();
    };
  };
} // namespace LHCb::Calo
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::ClusChi22ID, "FutureClusChi22ID" )

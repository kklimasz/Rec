/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFuturePhotonIdAlg.h"
#include "DetDesc/HistoParam.h"
#include <GaudiUtils/Aida2ROOT.h>
#include <math.h>

//------------------------------------------------------------------------------
// Implementation file for class : CaloFuturePhotonIdAlg
//
// 2010-02-27 : Olivier Deschamps
//------------------------------------------------------------------------------
namespace {
  static const auto s_histo_path = "CaloFutureNeutralPIDs/PhotonID/";
}

DECLARE_COMPONENT( CaloFuturePhotonIdAlg )

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloFuturePhotonIdAlg::CaloFuturePhotonIdAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : MergingTransformer( name, pSvcLocator, {"Inputs", {}}, {"Output", {}} ) {
  // TODO : split PID estimator (Prs-Spd-Ecal(Chi2, seed/cluster) + add
  // estimator ( E4/E9, Hcal/Ecal, shower-shape,  ...)
  // and let NeutralProtoParticle to combine ...
}

//==============================================================================
// Initialization
//==============================================================================
StatusCode CaloFuturePhotonIdAlg::initialize() {
  StatusCode sc = MergingTransformer::initialize();
  if ( sc.isFailure() ) return sc;

  // Retrieve tools
  m_counterStat.retrieve();
  sc = m_estimator.retrieve();
  if ( sc.isFailure() ) return sc;

  // Report
  info() << "PhotonId : " << ( m_dlnL ? "Delta Log Likelihood calculation." : "Likelihood estimator." ) << endmsg;
  // Initialize histogram access
  if ( m_useCondDB && !existDet<DataObject>( detSvc(), m_conditionName ) ) {
    warning() << "Initialise: Condition '" << m_conditionName << "' not found -- switch to reading the DLLs from THS!"
              << endmsg;
    m_useCondDB = false;
  }

  sc = m_useCondDB ? initializeWithCondDB() : initializeWithoutCondDB();

  using LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation;
  using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

  // Update inputs to this default if null
  if ( getProperty( "Inputs" ).toString() == "[  ]" ) {
    std::string_view loc =
        ( m_type == "MergedID" ? "MergedPi0s"                                                           //
                               : m_type == "PhotonID" ? "Photons"                                       //
                                                      : m_type == "PhotonFromMergedID" ? "SplitPhotons" //
                                                                                       : "" );
    updateHandleLocations( *this, "Inputs", {CaloFutureHypoLocation( loc )} );
  }
  // Update output to this default if null
  if ( getProperty( "Output" ).toString().empty() ) {
    updateHandleLocation( *this, "Output", CaloFutureIdLocation( m_type.value() ) );
  }

  // Warning : the algorithm settings overwrite the caloHypo2CaloFuture default
  // settings
  auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
  h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
  h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
  h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();
  m_estimator->_setProperty( "SkipNeutralID", "true" ).ignore(); // avoid loop

  return sc;
}

//==============================================================================

Table CaloFuturePhotonIdAlg::operator()( const HyposList& hyposlist ) const {

  // create the table
  auto table = Table( 200 );

  // fill the table
  int n = 0;
  for ( const auto hypos : hyposlist ) {
    // Retrieve location on TES
    const auto& loc = inputLocation( n++ );
    // Skip if null
    if ( !hypos ) {
      if ( m_counterStat->isQuiet() ) counter( "Empty location " ) += 1;
      continue;
    }
    if ( m_counterStat->isVerbose() ) counter( "#Hypos in " + loc ) += hypos->size();

    // Loop over hypos
    for ( const auto hypo : *hypos ) {
      if ( !hypo ) {
        if ( m_counterStat->isQuiet() ) counter( "hypo points to NULL in " + loc ) += 1;
        continue;
      }
      const double l = ( m_isRunnable ? likelihood( hypo ) : -999. );
      if ( m_counterStat->isVerbose() ) counter( "likelihood" ) += l;
      table.i_push( hypo, l );
    }
  }
  table.i_sort();

  // statistics
  if ( m_counterStat->isQuiet() ) {
    const auto inputs = getProperty( "Inputs" ).toString();
    const auto output = getProperty( "Output" ).toString();
    counter( inputs + " ==> " + output ) += table.i_relations().size();
  }

  return table;
}

//==============================================================================

double CaloFuturePhotonIdAlg::likelihood( const LHCb::CaloHypo* hypo ) const {
  // Get the relevant information - basic checks
  if ( !hypo ) {
    Warning( "CaloHypo points to NULL", StatusCode::SUCCESS ).ignore();
    if ( m_counterStat->isQuiet() ) counter( "Null hypo" ) += 1;
    return -999.;
  }
  // parameter evaluation
  auto params = getParams( *hypo );
  if ( !params ) return -999.;

  // get chi2
  double chi2 =
      ( m_tracking ? m_estimator->data( *hypo, LHCb::Calo::Enum::DataType::ClusterMatch ).value_or( +999. ) : -999. );
  // evaluate
  return evalLikelihood( *params, chi2 ).value_or( -999. );
}

//==============================================================================

std::optional<CaloFuturePhotonIdAlg::Params> CaloFuturePhotonIdAlg::getParams( const LHCb::CaloHypo& hypo ) const {
  using namespace LHCb::Calo::Enum;

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
  auto p = Params{.energy = m_estimator->data( hypo, DataType::HypoE ).value_or( Default ),
                  .et     = m_estimator->data( hypo, DataType::HypoEt ).value_or( Default ),
                  .eSeed  = m_estimator->data( hypo, DataType::E1Hypo ).value_or( Default ),
                  .nSpd   = (int)m_estimator->data( hypo, DataType::ToSpdM ).value_or( Default ),
                  .ePrs   = m_estimator->data( hypo, DataType::ToPrsE ).value_or( Default ),
                  .area   = LHCb::CaloCellID( m_estimator->data( hypo, DataType::CellID ).value_or( Default ) ).area()};
#pragma GCC diagnostic pop

  if ( !m_estimator->status() ) return std::nullopt;
  return p;
}

//==============================================================================

std::optional<double> CaloFuturePhotonIdAlg::evalLikelihood( CaloFuturePhotonIdAlg::Params p, double chi2 ) const {

  std::optional<double> estimator;
  constexpr double      epsilon = 1.e-20;

  double signal = epsilon;
  double backgr = epsilon;

  if ( p.nSpd == 0 ) {
    signal = CaloFuturePhotonIdAlg::dLL( p.energy, p.ePrs, chi2, p.eSeed, CaloFuturePhotonIdAlg::SIGNAL, p.area );
    if ( m_isRunnable ) {
      backgr = CaloFuturePhotonIdAlg::dLL( p.energy, p.ePrs, chi2, p.eSeed, CaloFuturePhotonIdAlg::BACKGROUND, p.area );
    }
  } else {
    signal = CaloFuturePhotonIdAlg::dLL( p.energy, p.ePrs, chi2, p.eSeed, CaloFuturePhotonIdAlg::SIGNAL_SPD, p.area );
    if ( m_isRunnable ) {
      backgr =
          CaloFuturePhotonIdAlg::dLL( p.energy, p.ePrs, chi2, p.eSeed, CaloFuturePhotonIdAlg::BACKGROUND_SPD, p.area );
    }
  }

  if ( m_isRunnable ) {
    estimator = ( m_dlnL ? ( log( std::max( signal, epsilon ) ) - log( std::max( backgr, epsilon ) ) )
                         : ( ( signal + backgr > 0 ) ? signal / ( signal + backgr ) : -1. ) );
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Photon Candidate :" << endmsg;
    debug() << " -E         =" << p.energy << endmsg;
    debug() << " -Et        =" << p.et << endmsg;
    debug() << " -#Spd hits =" << p.nSpd << endmsg;
    debug() << " -EPrs      =" << p.ePrs << endmsg;
    debug() << " -Chi2      =" << chi2 << endmsg;
    debug() << " -ESeed     =" << p.eSeed << endmsg;
    debug() << "      => " << ( m_dlnL ? "DlnL     " : "estimator" ) << " = " << estimator.value_or( -999. ) << endmsg;
  }

  return estimator;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::initializeWithCondDB() {
  info() << "init with CondDB, m_conditionName = " << m_conditionName.value() << endmsg;
  try {
    registerCondition( m_conditionName, m_cond, &CaloFuturePhotonIdAlg::i_updateDLL );
  } catch ( GaudiException& e ) {
    fatal() << e << endmsg;
    m_isRunnable = false;
    return StatusCode::FAILURE;
  }
  StatusCode sc = runUpdate(); // load the conditions
  return sc;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::initializeWithoutCondDB() {
  info() << "init w/o CondDB, pdf histos path is " << s_histo_path << endmsg;

  m_Sig_EPrs_10 = locateHistoOnDisk( m_title_Sig_EPrs_10 );
  m_Sig_EPrs_11 = locateHistoOnDisk( m_title_Sig_EPrs_11 );
  m_Sig_EPrs_12 = locateHistoOnDisk( m_title_Sig_EPrs_12 );
  m_Sig_EPrs_15 = locateHistoOnDisk( m_title_Sig_EPrs_15 );
  m_Sig_EPrs_16 = locateHistoOnDisk( m_title_Sig_EPrs_16 );
  m_Sig_EPrs_17 = locateHistoOnDisk( m_title_Sig_EPrs_17 );

  m_Sig_Chi2_20 = locateHistoOnDisk( m_title_Sig_Chi2_20 );
  m_Sig_Chi2_21 = locateHistoOnDisk( m_title_Sig_Chi2_21 );
  m_Sig_Chi2_22 = locateHistoOnDisk( m_title_Sig_Chi2_22 );
  m_Sig_Chi2_25 = locateHistoOnDisk( m_title_Sig_Chi2_25 );
  m_Sig_Chi2_26 = locateHistoOnDisk( m_title_Sig_Chi2_26 );
  m_Sig_Chi2_27 = locateHistoOnDisk( m_title_Sig_Chi2_27 );

  m_Sig_Seed_30 = locateHistoOnDisk( m_title_Sig_Seed_30 );
  m_Sig_Seed_31 = locateHistoOnDisk( m_title_Sig_Seed_31 );
  m_Sig_Seed_32 = locateHistoOnDisk( m_title_Sig_Seed_32 );
  m_Sig_Seed_35 = locateHistoOnDisk( m_title_Sig_Seed_35 );
  m_Sig_Seed_36 = locateHistoOnDisk( m_title_Sig_Seed_36 );
  m_Sig_Seed_37 = locateHistoOnDisk( m_title_Sig_Seed_37 );

  m_Bkg_EPrs_110 = locateHistoOnDisk( m_title_Bkg_EPrs_110 );
  m_Bkg_EPrs_111 = locateHistoOnDisk( m_title_Bkg_EPrs_111 );
  m_Bkg_EPrs_112 = locateHistoOnDisk( m_title_Bkg_EPrs_112 );
  m_Bkg_EPrs_115 = locateHistoOnDisk( m_title_Bkg_EPrs_115 );
  m_Bkg_EPrs_116 = locateHistoOnDisk( m_title_Bkg_EPrs_116 );
  m_Bkg_EPrs_117 = locateHistoOnDisk( m_title_Bkg_EPrs_117 );

  m_Bkg_Chi2_120 = locateHistoOnDisk( m_title_Bkg_Chi2_120 );
  m_Bkg_Chi2_121 = locateHistoOnDisk( m_title_Bkg_Chi2_121 );
  m_Bkg_Chi2_122 = locateHistoOnDisk( m_title_Bkg_Chi2_122 );
  m_Bkg_Chi2_125 = locateHistoOnDisk( m_title_Bkg_Chi2_125 );
  m_Bkg_Chi2_126 = locateHistoOnDisk( m_title_Bkg_Chi2_126 );
  m_Bkg_Chi2_127 = locateHistoOnDisk( m_title_Bkg_Chi2_127 );

  m_Bkg_Seed_130 = locateHistoOnDisk( m_title_Bkg_Seed_130 );
  m_Bkg_Seed_131 = locateHistoOnDisk( m_title_Bkg_Seed_131 );
  m_Bkg_Seed_132 = locateHistoOnDisk( m_title_Bkg_Seed_132 );
  m_Bkg_Seed_135 = locateHistoOnDisk( m_title_Bkg_Seed_135 );
  m_Bkg_Seed_136 = locateHistoOnDisk( m_title_Bkg_Seed_136 );
  m_Bkg_Seed_137 = locateHistoOnDisk( m_title_Bkg_Seed_137 );

  return StatusCode::SUCCESS;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::i_updateDLL() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "i_updateDLL() called" << endmsg;
  if ( !m_cond ) return StatusCode::FAILURE;

  try {
    m_Sig_EPrs_10 = locateHistoOnDB( m_title_Sig_EPrs_10 );
    m_Sig_EPrs_11 = locateHistoOnDB( m_title_Sig_EPrs_11 );
    m_Sig_EPrs_12 = locateHistoOnDB( m_title_Sig_EPrs_12 );
    m_Sig_EPrs_15 = locateHistoOnDB( m_title_Sig_EPrs_15 );
    m_Sig_EPrs_16 = locateHistoOnDB( m_title_Sig_EPrs_16 );
    m_Sig_EPrs_17 = locateHistoOnDB( m_title_Sig_EPrs_17 );

    m_Sig_Chi2_20 = locateHistoOnDB( m_title_Sig_Chi2_20 );
    m_Sig_Chi2_21 = locateHistoOnDB( m_title_Sig_Chi2_21 );
    m_Sig_Chi2_22 = locateHistoOnDB( m_title_Sig_Chi2_22 );
    m_Sig_Chi2_25 = locateHistoOnDB( m_title_Sig_Chi2_25 );
    m_Sig_Chi2_26 = locateHistoOnDB( m_title_Sig_Chi2_26 );
    m_Sig_Chi2_27 = locateHistoOnDB( m_title_Sig_Chi2_27 );

    m_Sig_Seed_30 = locateHistoOnDB( m_title_Sig_Seed_30 );
    m_Sig_Seed_31 = locateHistoOnDB( m_title_Sig_Seed_31 );
    m_Sig_Seed_32 = locateHistoOnDB( m_title_Sig_Seed_32 );
    m_Sig_Seed_35 = locateHistoOnDB( m_title_Sig_Seed_35 );
    m_Sig_Seed_36 = locateHistoOnDB( m_title_Sig_Seed_36 );
    m_Sig_Seed_37 = locateHistoOnDB( m_title_Sig_Seed_37 );

    m_Bkg_EPrs_110 = locateHistoOnDB( m_title_Bkg_EPrs_110 );
    m_Bkg_EPrs_111 = locateHistoOnDB( m_title_Bkg_EPrs_111 );
    m_Bkg_EPrs_112 = locateHistoOnDB( m_title_Bkg_EPrs_112 );
    m_Bkg_EPrs_115 = locateHistoOnDB( m_title_Bkg_EPrs_115 );
    m_Bkg_EPrs_116 = locateHistoOnDB( m_title_Bkg_EPrs_116 );
    m_Bkg_EPrs_117 = locateHistoOnDB( m_title_Bkg_EPrs_117 );

    m_Bkg_Chi2_120 = locateHistoOnDB( m_title_Bkg_Chi2_120 );
    m_Bkg_Chi2_121 = locateHistoOnDB( m_title_Bkg_Chi2_121 );
    m_Bkg_Chi2_122 = locateHistoOnDB( m_title_Bkg_Chi2_122 );
    m_Bkg_Chi2_125 = locateHistoOnDB( m_title_Bkg_Chi2_125 );
    m_Bkg_Chi2_126 = locateHistoOnDB( m_title_Bkg_Chi2_126 );
    m_Bkg_Chi2_127 = locateHistoOnDB( m_title_Bkg_Chi2_127 );

    m_Bkg_Seed_130 = locateHistoOnDB( m_title_Bkg_Seed_130 );
    m_Bkg_Seed_131 = locateHistoOnDB( m_title_Bkg_Seed_131 );
    m_Bkg_Seed_132 = locateHistoOnDB( m_title_Bkg_Seed_132 );
    m_Bkg_Seed_135 = locateHistoOnDB( m_title_Bkg_Seed_135 );
    m_Bkg_Seed_136 = locateHistoOnDB( m_title_Bkg_Seed_136 );
    m_Bkg_Seed_137 = locateHistoOnDB( m_title_Bkg_Seed_137 );
  } catch ( GaudiException& exc ) {
    fatal() << "DLL update failed! msg ='" << exc << "'" << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//==============================================================================

TH2D* CaloFuturePhotonIdAlg::locateHistoOnDisk( std::string_view histoname ) const {
  TH2D* histo = nullptr;
  if ( !histoname.empty() ) {
    AIDA::IHistogram2D* aida =
        get<AIDA::IHistogram2D>( histoSvc(), s_histo_path + std::string{histoname} ); // TODO: C++20: use std::format
    if ( !aida ) {
      warning() << "Could not find AIDA::IHistogram2D* " << s_histo_path + histoname << "." << endmsg;
      m_isRunnable = false;
      return nullptr;
    }
    histo = Gaudi::Utils::Aida2ROOT::aida2root( aida );
  }
  return histo;
}

//==============================================================================

TH2D* CaloFuturePhotonIdAlg::locateHistoOnDB( std::string_view histoname ) const {
  return !histoname.empty() ? reinterpret_cast<TH2D*>( &m_cond->param<DetDesc::Params::Histo2D>(
                                  std::string{histoname} ) ) // TODO: make param accept string_view
                            : nullptr;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloFutureAcceptance.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/Track.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/CaloCellCode.h"

// ============================================================================
/** @file
 *  Implementation file for class InCaloFutureAcceptance
 *  @see InAccpetance
 *
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================
namespace LHCb::Calo {

  // ============================================================================
  //  Standard constructor, initializes variables
  // ============================================================================

  InAcceptance::InAcceptance( const std::string& type, const std::string& name, const IInterface* parent )
      : TrackTool( type, name, parent ) {
    declareInterface<IInAcceptance>( this );
  }

  // ============================================================================
  // initialization @see IAlgTool
  // ============================================================================

  StatusCode InAcceptance::initialize() {
    StatusCode sc = TrackTool::initialize();
    if ( sc.isFailure() ) { return sc; }
    // check the detector
    if ( calo() == nullptr ) { return Error( "Invalid Detector" ); }
    // select the plane
    const int check_calorimeter_id = CaloCellCode::CaloNumFromName( detectorName() );
    switch ( check_calorimeter_id ) {
    case 0:
      // SPD
      m_loc   = State::Location::Spd;
      m_plane = calo()->plane( CaloPlane::Middle );
      break; // BREAK
    case 1:
      // PRS
      m_loc   = State::Location::Prs;
      m_plane = calo()->plane( CaloPlane::Middle );
      break; // BREAK
    case 2:
      // ECAL
      m_loc   = State::Location::ECalShowerMax;
      m_plane = calo()->plane( CaloPlane::ShowerMax );
      break; // BREAK
    case 3:
      // HCAL
      m_loc   = State::Location::MidHCal;
      m_plane = calo()->plane( CaloPlane::Middle );
      break; // BREAK
    default:
      return Error( "Invalid calorimeter TYPE! '" + detectorName() + "'" );
    }
    //
    if ( propsPrint() || msgLevel( MSG::DEBUG ) ) {
      info() << "State to be used for aceptance check is '" << m_loc << "'" << endmsg;
    }
    return StatusCode::SUCCESS;
  }

  // ============================================================================
  // check the track is in acceptance of the given calorimeter
  // ============================================================================

  bool InAcceptance::inAcceptance( const Track* track ) const {
    // check the goodness of the tarck
    if ( !use( track ) ) { return false; } // RETURN

    // find the appropriate state
    auto state_ptr = TrackTool::state( *track, m_loc );
    if ( state_ptr == nullptr ) {
      LHCb::State state;
      // if there is no proper state - add it into the track!
      StatusCode sc = propagate( *track, plane(), state );
      if ( sc.isFailure() ) {
        Error( "failure from propagate()", sc ).ignore();
        return false; // RETURN
      }
      state.setLocation( m_loc );
      const_cast<Track*>( track )->addToStates( state );
      return ok( state.position() );
    } else {
      // check the point
      return ok( state_ptr->position() );
    }
  }

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::InAcceptance, "InCaloFutureAcceptance" )
// ============================================================================

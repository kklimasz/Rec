/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackTool.h"
#include "TrackKernel/TrackFunctors.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloFuture::CaloFutureTrackTool
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-05-28
 */

namespace LHCb::Calo {
  // ============================================================================
  /// standard constructor
  // ============================================================================
  TrackTool::TrackTool( const std::string& type, // ?
                        const std::string& name, const IInterface* parent )
      : GaudiTool( type, name, parent ) {
    _setProperty( "CheckTracks", "false" );
  }

  //==============================================================================
  /// initialize the tool
  //==============================================================================

  StatusCode TrackTool::initialize() {
    StatusCode sc = GaudiTool::initialize();
    if ( sc.isFailure() ) { return sc; }
    //
    if ( propsPrint() || msgLevel( MSG::DEBUG ) || m_use.check() ) { info() << m_use << endmsg; };
    //
    if ( !m_detectorName.empty() ) {
      m_calo = getDet<DeCalorimeter>( detectorName() );
    } else {
      Warning( "empty detector name!" ).ignore();
    }

    return StatusCode::SUCCESS;
  }

  //==============================================================================

  void TrackTool::_setProperty( const std::string& p, const std::string& v ) {
    StatusCode sc = setProperty( p, v );
    if ( !sc ) { warning() << " setting Property " << p << " to " << v << " FAILED" << endmsg; }
  }

  // ============================================================================
  // Propagate track to a given 3D-place
  // ============================================================================

  StatusCode TrackTool::propagate( const LHCb::Track& track, const Gaudi::Plane3D& plane, LHCb::State& state,
                                   const LHCb::Tr::PID pid ) const {
    state = closestState( track, plane );
    if ( std::abs( plane.Distance( state.position() ) ) < tolerance() ) { return StatusCode::SUCCESS; }
    return propagate( state, plane, pid );
  }

  // ============================================================================
  // Propagate state to a given 3D-place
  // ============================================================================

  StatusCode TrackTool::propagate( LHCb::State& state, const Gaudi::Plane3D& plane, const LHCb::Tr::PID pid ) const {
    // check the plane: if it is "almost Z=const"-plane
    const Gaudi::XYZVector& normal = plane.Normal();
    if ( m_cosTolerance < std::abs( normal.Z() ) ) {
      // use the standard method
      const double Z = -1 * plane.HesseDistance() / normal.Z();
      return propagate( state, Z, pid );
    }
    Gaudi::XYZPoint point;
    for ( unsigned int iter = 0; iter < m_maxIter; ++iter ) {
      const double distance = std::abs( plane.Distance( state.position() ) );
      if ( distance < m_tolerance ) { return StatusCode::SUCCESS; } // RETURN
      double mu = 0.0;
      if ( !Gaudi::Math::intersection( line( state ), plane, point, mu ) ) {
        return Warning( "propagate: line does not cross the place" );
      } // RETURN
      StatusCode sc = propagate( state, point.Z(), pid );
      if ( sc.isFailure() ) { return Warning( "propagate: failure from propagate", sc ); } // RETURN
    }
    return Warning( "propagate: no convergency has been reached" );
  }

  // ============================================================================
  // Propagate state to a given Z
  // ============================================================================

  StatusCode TrackTool::propagate( LHCb::State& state, const double z, const LHCb::Tr::PID pid ) const {
    if ( std::max( state.z(), z ) < m_fastZ ) { // use the regular extrapolator
      return m_extrapolator->propagate( state, z, pid );
    } else if ( std::min( state.z(), z ) > m_fastZ ) { // use the fast (linear) extrapolator
      return m_fastExtrapolator->propagate( state, z, pid );
    }
    // use the pair of extrapolators
    StatusCode sc1;
    StatusCode sc2;
    if ( state.z() < z ) {
      sc1 = m_extrapolator->propagate( state, m_fastZ, pid );
      sc2 = m_fastExtrapolator->propagate( state, z, pid );
    } else {
      sc2 = m_fastExtrapolator->propagate( state, m_fastZ, pid );
      sc1 = m_extrapolator->propagate( state, z, pid );
    }

    StatusCode  sc = StatusCode::SUCCESS;
    std::string errMsg;
    if ( sc2.isFailure() ) {
      errMsg = "Error from FastExtrapolator";
      sc     = sc2;
    }
    if ( sc1.isFailure() ) {
      errMsg = "Error from extrapolator";
      sc     = sc1;
    }
    //
    return sc.isFailure() ? Warning( errMsg, sc ) : sc;
  }

  bool TrackTool::use( const LHCb::Track* track ) const { return m_use( track ); }
} // namespace LHCb::Calo

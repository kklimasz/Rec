/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREUTILS_CALOFUTURETRACKMATCH_H
#define CALOFUTUREUTILS_CALOFUTURETRACKMATCH_H 1

// Include files
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureInterfaces/CaloFutureMatch3D.h"
#include "CaloFutureTrackTool.h"
#include "DetDesc/Condition.h"
#include "Event/CaloPosition.h"
#include "Event/State.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include <optional>

// ============================================================================
/** @class CaloFutureTrackMatch CaloFutureTrackMatch.h
 *  Description of the use of the track-cluster matching X-correction
 *  parameters for electrons and positrons.
 *
 *  The X-correction part of the code has been designed with the kind help from
 *  Vanya BELYAEV and Dmitry GOLUBKOV.
 *
 *  Common form of the X-correction between the X-position of the state
 *  of track and barycentre of the CaloHypo object is:
 *  dX = a0*p + a1 + a2/p + a3/p^2 + ...
 *  where p - momentum of the track in GeV/$c$,
 *  parameter vectors for each zone of the ECAL
 *  m_alphaN<Area>[3] = {a0, a1, a2, a3, ...}
 *  for e- MagUp and e+ MagDown: (q*polarity) < 0
 *  and
 *  m_alphaP<Area>[3] = {a0, a1, a2, a3, ...}
 *  for e- MagDown and e+ MagUp: (q*polarity) > 0
 *
 *  By default the X-correction parameters are read from the CondDB
 *  path '/dd/Conditions/ParticleID/Calo/ElectronXCorrection'.
 *
 *  In the case when the CondDB is switched off or reading
 *  of the X-correction parameters from the CondDB is disabled
 *  the X-correction is not implemented and all X-correction parameters
 *  are equal to zero.
 *
 *  Usage of the X-correction parameters in Bender scripts:
 *  def configure ( ) :
 *      """
 *      Job configuration
 *      """
 *      ...
 *      from Configurables import CaloFutureElectronMatch
 *      my_tool = CaloFutureElectronMatch()
 *      ## disable use of the CondDB to apply correction coefficients
 *      ## from the Options
 *      my_tool.ConditionName = ""
 *      ## Set the X-correction parameters for the Outer zone
 *      ## of the ECAL in case (q*polarity) < 0
 *      my_tool.AlphaNOut = [ 0.0, -18.92, 83.46, -292.4 ]
 *
 *  For more info see talk by O. Stenyakin
 *  at 2014/02/20 Moscow student meeting
 *  https://indico.cern.ch/event/302695/
 *  or
 *  at 2014/01/24 CaloFuture Objects meeting
 *  https://indico.cern.ch/event/296617/
 *
 *  @author Oleg STENYAKIN oleg.stenyakin@cern.ch
 *  @date   2014-03-03
 */
// ============================================================================

namespace LHCb::Calo {
  class TrackMatch : public TrackTool {
  public:
    /// initialization
    StatusCode initialize() override;
    StatusCode i_updateAlpha();

  protected:
    /// Standard constructor
    using TrackTool::TrackTool;

    Match3D getTrackMatch3D( const LHCb::State& s ) const;

    bool updateCaloPos( const LHCb::CaloPosition* p1, const LHCb::CaloPosition* p2 );

    double chi2( const Match2D& m1, const Match2D& m2 ) const;

    double chi2( const Match3D& m1, const Match3D& m2 ) const;

    const std::vector<double>& selectAlpha( unsigned int area, const bool qpolarity ) const;

  protected:
    double bad() const;

    Condition*                   m_cond;
    Gaudi::Property<std::string> m_conditionName{this, "ConditionName",
                                                 "/dd/Conditions/ParticleID/Calo/ElectronXCorrection",
                                                 "set this property to an empty string to disable the use of CondDB"};

    ILHCbMagnetSvc* m_magFieldSvc = nullptr;

  private:
    Gaudi::Property<double> m_bad{this, "BadValue", 1.e+10, "bad value for chi2"};

    Gaudi::Property<std::vector<double>> m_alphaPOut{
        this, "AlphaPOut", {}, "electron X-correction params for (q*polarity) > 0 Outer"};

    Gaudi::Property<std::vector<double>> m_alphaNOut{
        this, "AlphaNOut", {}, "electron X-correction params for (q*polarity) < 0 Outer"};

    Gaudi::Property<std::vector<double>> m_alphaPMid{
        this, "AlphaPMid", {}, "electron X-correction params for (q*polarity) > 0 Middle"};

    Gaudi::Property<std::vector<double>> m_alphaNMid{
        this, "AlphaNMid", {}, "electron X-correction params for (q*polarity) < 0 Middle"};

    Gaudi::Property<std::vector<double>> m_alphaPInn{
        this, "AlphaPInn", {}, "electron X-correction params for (q*polarity) > 0 Inner"};

    Gaudi::Property<std::vector<double>> m_alphaNInn{
        this, "AlphaNInn", {}, "electron X-correction params for (q*polarity) < 0 Inner"};
  };
} // namespace LHCb::Calo

// ============================================================================
#endif // CALOFUTUREUTILS_CALOFUTURETRACKMATCH_H

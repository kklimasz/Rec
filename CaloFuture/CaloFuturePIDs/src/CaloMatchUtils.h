/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALO_MATCH_UTILS_H
#define CALO_MATCH_UTILS_H

namespace LHCb {
  class State;
  class CaloPosition;
} // namespace LHCb

namespace LHCb::Calo {
  class Match2D;
  class Match3D;

  Match2D getMatch2D( const LHCb::State& s );

  Match2D getMatch2D( const LHCb::CaloPosition& c );
  Match3D getMatch3D( const LHCb::CaloPosition& c );

  Match2D getBremMatch2D( const LHCb::CaloPosition& c );

} // namespace LHCb::Calo
#endif

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatch.h"
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureInterfaces/CaloFutureMatch3D.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloFutureTrackMatch
 *
 *  Properties of the class:
 *  BadValue - bad value of the chi2,
 *  ConditionName - string with the path to the file containing e- and e+ X-correction condition parameters,
 *  AlphaPOut - track-cluster matching X-correction parameters for Outer zone of the ECAL in case (q*polarity) > 0
 *  AlphaNOut - track-cluster matching X-correction parameters for Outer zone of the ECAL in case (q*polarity) < 0
 *  AlphaPMid - track-cluster matching X-correction parameters for Middle zone of the ECAL in case (q*polarity) > 0
 *  AlphaNMid - track-cluster matching X-correction parameters for Middle zone of the ECAL in case (q*polarity) < 0
 *  AlphaPInn - track-cluster matching X-correction parameters for Inner zone of the ECAL in case (q*polarity) > 0
 *  AlphaNInn - track-cluster matching X-correction parameters for Inner zone of the ECAL in case (q*polarity) < 0
 *
 *  @author Oleg STENYAKIN  oleg.stenyakin@cern.ch
 *  @date   2014-03-03
 *
 */
namespace {
  constexpr inline double s_chi2_error_value = 99999999.;
}

namespace LHCb::Calo {

  // ============================================================================

  StatusCode TrackMatch::i_updateAlpha() {
    // allow a user to disable the CondDB and reset the x-corrections with
    // setProperty on the fly (if ever needed)
    if ( m_conditionName.empty() ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "attempt to update X-correction parameters by UpdMgrSvc while CondDB access disabled" << endmsg;
      return StatusCode::SUCCESS;
    }

    // safety protection against SIGSEGV
    if ( !m_cond ) {
      fatal() << "CaloFutureTrackMatch::i_updateAlpha m_cond == 0" << endmsg;
      return StatusCode::FAILURE;
    }

    try {
      m_alphaPOut.value() = m_cond->paramAsDoubleVect( "alphaPOut" );
      m_alphaNOut.value() = m_cond->paramAsDoubleVect( "alphaNOut" );
      m_alphaPMid.value() = m_cond->paramAsDoubleVect( "alphaPMid" );
      m_alphaNMid.value() = m_cond->paramAsDoubleVect( "alphaNMid" );
      m_alphaPInn.value() = m_cond->paramAsDoubleVect( "alphaPInn" );
      m_alphaNInn.value() = m_cond->paramAsDoubleVect( "alphaNInn" );
    } catch ( GaudiException& exc ) {
      fatal() << "X-correction update failed! msg ='" << exc << "'" << endmsg;
      return StatusCode::FAILURE;
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "CondDB update of CaloFutureTrackMatch X-correction parameters with '" << m_conditionName << "':"
              << "\nalphaPOut: " << Gaudi::Utils::toString( m_alphaPOut.value() )
              << " alphaNOut: " << Gaudi::Utils::toString( m_alphaNOut.value() )
              << "\nalphaPMid: " << Gaudi::Utils::toString( m_alphaPMid.value() )
              << " alphaNMid: " << Gaudi::Utils::toString( m_alphaNMid.value() )
              << "\nalphaPInn: " << Gaudi::Utils::toString( m_alphaPInn.value() )
              << " alphaNInn: " << Gaudi::Utils::toString( m_alphaNInn.value() ) << endmsg;

    return StatusCode::SUCCESS;
  }

  bool TrackMatch::updateCaloPos( const LHCb::CaloPosition* p1, const LHCb::CaloPosition* p2 ) { return p1 != p2; }

  double TrackMatch::chi2( const Match2D& m1, const Match2D& m2 ) const {
    if ( !m1 || !m2 ) {
      Warning( "chi2(): invalid data are detected - return bad chi2" ).ignore();
      return s_chi2_error_value;
    }

    // evaluate the overall covariance matrix
    Match2D::Matrix cov = m1.matrix() + m2.matrix();

    if ( !cov.Invert() ) {
      Warning( "chi2(): can not invert the matrix - return bad chi2" ).ignore();
      return s_chi2_error_value;
    }

    // get the weighted and mean parameters
    // Note: Cannot use `auto` here
    Match2D::Vector pw = m1.matrix() * m1.params() + m2.matrix() * m2.params();
    Match2D::Vector pm = cov * pw;

    // evaluate chi2
    return ROOT::Math::Similarity( pm - m1.params(), m1.matrix() ) +
           ROOT::Math::Similarity( pm - m2.params(), m2.matrix() );
  }

  double TrackMatch::chi2( const Match3D& m1, const Match3D& m2 ) const {
    if ( !m1 || !m2 ) {
      Warning( "chi2(): invalid data are detected - return bad chi2: " ).ignore();
      return s_chi2_error_value;
    }

    // evaluate the overall covariance matrix
    Match3D::Matrix cov = m1.matrix() + m2.matrix();

    if ( !cov.Invert() ) {
      Warning( "chi2(): can not invert the matrix - return bad chi2" ).ignore();
      return s_chi2_error_value;
    }

    // get the weighted and mean parameters
    // Note: Cannot use `auto` here
    Match3D::Vector pw = m1.matrix() * m1.params() + m2.matrix() * m2.params();
    Match3D::Vector pm = cov * pw;

    // evaluate chi2
    return ROOT::Math::Similarity( pm - m1.params(), m1.matrix() ) +
           ROOT::Math::Similarity( pm - m2.params(), m2.matrix() );
  }

  double TrackMatch::bad() const { return m_bad; }

  // =============================================================================

  StatusCode TrackMatch::initialize() {
    StatusCode sc = TrackTool::initialize();

    if ( sc.isFailure() ) { return sc; }

    if ( !existDet<DataObject>( detSvc(), m_conditionName ) ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Condition '" << m_conditionName
                << "' not found -- switch off the use of the CondDB for CaloFutureTrackMatch!" << endmsg;
      m_conditionName.clear();
    }

    if ( !m_conditionName.empty() ) {
      registerCondition( m_conditionName, m_cond, &TrackMatch::i_updateAlpha );
      sc = runUpdate(); // ask UpdateManagerSvc to load the condition w/o waiting for the next BeginEvent incident
    } else if ( msgLevel( MSG::DEBUG ) ) {
      debug()
          << "ConditionName empty -- reading of the CaloFutureTrackMatch X-correction parameters from the CondDB has "
             "been disabled!"
          << endmsg;
    }

    m_magFieldSvc = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );

    return StatusCode::SUCCESS;
  }

  // ============================================================================

  // get 3D-information from State
  Match3D TrackMatch::getTrackMatch3D( const LHCb::State& s ) const {
    const auto   par = s.stateVector();
    const double q   = 0 < par( 4 ) ? 1. : -1.;                      // charge
    const double mom = ::fabs( 1.0 / par( 4 ) / Gaudi::Units::GeV ); // momentum in GeV

    // find calo area corresponding to the input LHCb::State &s;
    const auto   cell = calo()->Cell_( s.position() ); // cell parameters (null if point is outside the Calorimeter)
    unsigned int area = 4;                             // initialize with some invalid area number
    if ( cell )                                        // protection against tracks pointing outside the Calorimeter
    {
      area = cell->cellID().area(); // 0:Outer, 1:Middle, 2:Inner
    } else // roughly assign the area around the beam hole to the Inner, everything outside Calo -- to the Outer
    {
      area = ( fabs( s.position().x() ) < 2. * Gaudi::Units::m && fabs( s.position().y() ) < 2. * Gaudi::Units::m )
                 ? 2
                 : 0; // |x,y| < > 2m
    }

    const float polarity  = m_magFieldSvc->isDown() ? -1 : +1;
    const bool  qpolarity = ( q * polarity > 0 );

    const std::vector<double>& alpha = selectAlpha( area, qpolarity );

    Match3D::Vector vector;
    Match3D::Matrix matrix;
    vector( 0 ) = par( 0 );
    // now add the correction series dX = a0*p + a1 + a2/p + a3/p^2 + ...
    if ( !alpha.empty() ) {
      double mmm = mom;                                  // p, 1, 1/p, 1/p^2, ...
      double inv = fabs( par( 4 ) * Gaudi::Units::GeV ); // abs(1/p) in GeV

      for ( const auto it : alpha ) {
        vector( 0 ) += it * mmm;
        mmm *= inv; // [1/p]^k, k=-1, 0, ... size(alpha)-2
      }
    }

    vector( 1 )      = par( 1 );
    vector( 2 )      = ::fabs( 1.0 / par( 4 ) );       /// @todo check it!
    const double f   = -1.0 * q / par( 4 ) / par( 4 ); // d(p)/d(Q/p)
    const auto   cov = s.covariance();
    matrix( 0, 0 )   = cov( 0, 0 );         // (x,x)
    matrix( 0, 1 )   = cov( 0, 1 );         // (x,y)
    matrix( 1, 1 )   = cov( 1, 1 );         // (y,y)
    matrix( 0, 2 )   = f * cov( 0, 4 );     // (x,p)
    matrix( 1, 2 )   = f * cov( 1, 4 );     // (y,p)
    matrix( 2, 2 )   = f * cov( 4, 4 ) * f; // (p,p)

    Match3D match( vector, matrix );

    if ( !match ) return {};
    return match;
  }

  const std::vector<double>& TrackMatch::selectAlpha( unsigned int area, const bool qpolarity ) const {
    switch ( area ) // symbolic names only declaread as "the private part" of namespace CaloCellCode in CaloCellCode.cpp
    {
    case 0: // Outer  ECAL
      return qpolarity ? m_alphaPOut.value() : m_alphaNOut.value();
    case 1: // Middle ECAL
      return qpolarity ? m_alphaPMid.value() : m_alphaNMid.value();
    case 2: // Inner  ECAL
      return qpolarity ? m_alphaPInn.value() : m_alphaNInn.value();
    default:
      throw GaudiException( "requested impossible Calo area", __func__, StatusCode::FAILURE );
    }
  }
} // namespace LHCb::Calo

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// include files
#include "InCaloFutureAcceptanceAlg.h"
#include "Relations/Relation1D.h"
#include <type_traits>

// ============================================================================
/** @file
 *  Implementation file for class InCaloFutureAcceptanceAlg
 *  @Date 2006-06-17
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

namespace LHCb::Calo {
  // ============================================================================
  /// Standard protected constructor
  // ============================================================================

  InAcceptanceAlg::InAcceptanceAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, KeyValue{"Inputs", ""}, KeyValue{"Output", ""} ) {
    updateHandleLocation( *this, "Inputs", CaloFutureAlgUtils::TrackLocations().front() );
  }

  // ============================================================================
  // algorithm execution
  // ============================================================================

  Table InAcceptanceAlg::operator()( const Tracks& tracks ) const {
    // a trivial check
    Assert( m_tool, "InAcceptance-tool  is invalid!" );

    if ( tracks.empty() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "No tracks retrieved from container" << endmsg;
    }

    Table table( 100 );

    size_t nAccept = 0;
    // loop over all tracks in the container
    for ( const auto& track : tracks ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        continue;
      }

      if ( track->checkFlag( LHCb::Track::Flags::Backward ) ) { continue; }

      const bool result = m_tool->inAcceptance( track );
      // fill the relation table
      table.i_push( track, result ); // ATTENTION: i-push is used
      if ( result ) { ++nAccept; }
    }
    // MANDATORY: i_sort after i_push
    table.i_sort();

    // a bit of statistics
    m_nTracks += tracks.size();
    m_nAccept += nAccept;
    m_nLinks += table.i_relations().size();

    return table;
  }
} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::InAcceptanceAlg, "InCaloFutureAcceptanceAlg" )

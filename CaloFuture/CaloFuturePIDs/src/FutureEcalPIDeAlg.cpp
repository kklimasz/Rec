/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class FutureEcalPIDeAlg  FutureEcalPIDeAlg.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================
namespace LHCb::Calo {
  class EcalPIDeAlg final : public ID2DLL {
  public:
    EcalPIDeAlg( const std::string& name, ISvcLocator* pSvc ) : ID2DLL( name, pSvc ) {
      using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

      updateHandleLocation( *this, "Input", CaloFutureIdLocation( "EcalChi2" ) );
      updateHandleLocation( *this, "Output", CaloFutureIdLocation( "EcalPIDe" ) );

      setProperty( "nVlong", Gaudi::Utils::toString( 2500 ) ).ignore();
      setProperty( "nVdown", Gaudi::Utils::toString( 2500 ) ).ignore();
      setProperty( "nVTtrack", Gaudi::Utils::toString( 2500 ) ).ignore();
      setProperty( "nMlong", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMdown", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) ).ignore();
      setProperty( "nMTtrack", Gaudi::Utils::toString( 100 * Gaudi::Units::GeV ) ).ignore();

      setProperty( "HistogramL", "DLL_Long" ).ignore();
      setProperty( "HistogramD", "DLL_Downstream" ).ignore();
      setProperty( "HistogramT", "DLL_Ttrack" ).ignore();
      setProperty( "ConditionName", "Conditions/ParticleID/Calo/EcalPIDe" ).ignore();

      setProperty( "HistogramL_THS", "CaloFuturePIDs/CALO/ECALPIDE/h3" ).ignore();
      setProperty( "HistogramD_THS", "CaloFuturePIDs/CALO/ECALPIDE/h5" ).ignore();
      setProperty( "HistogramT_THS", "CaloFuturePIDs/CALO/ECALPIDE/h6" ).ignore();

      setProperty( "AcceptedType", Gaudi::Utils::toString<int>( LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                                                LHCb::Track::Types::Downstream ) )
          .ignore();
    };
  };
} // namespace LHCb::Calo
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::EcalPIDeAlg, "FutureEcalPIDeAlg" )

// ============================================================================

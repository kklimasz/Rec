#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## The major building blocks of CaloFuturerimeter Reconstruction
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
The major building blocks of CaloFuturerimeter Reconstruction
"""

# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
__all__ = ('clustersReco', 'photonsReco', 'mergedPi0sReco')
# =============================================================================
from Gaudi.Configuration import *

from Configurables import GaudiSequencer

from GaudiKernel.SystemOfUnits import MeV, GeV

from CaloKernel.ConfUtils import (prntCmp, addAlgs, setTheProperty, onDemand,
                                  caloOnDemand)

import logging
_log = logging.getLogger('CaloFutureReco')


# =============================================================================
## prepare the digits for the recontruction
def digitsFutureReco(createADC):
    """
    Prepare the digits for the recontruction
    """

    from CaloFutureDAQ.CaloFutureDigits import caloDigits

    _log.warning(
        'CaloFutureReco.digitsFutureReco is deprecated, use CaloFutureDigitConf instead'
    )

    return caloDigits(createADC)


## ============================================================================
## define the recontruction of Ecal clusters
def clusterFutureReco(enableRecoOnDemand,
                      clusterPt=0.,
                      fastReco=False,
                      external='',
                      makeTag=False,
                      masksE=[],
                      masksP=[],
                      mergedPi0Pt=2 * GeV):
    """
    Define the recontruction of Ecal Clusters
    """

    from Configurables import (FutureCellularAutomatonAlg,
                               CaloFutureShowerOverlap,
                               CaloFutureClusterCovarianceAlg)

    ## Define the sequencer
    seq = GaudiSequencer("ClusterFutureReco")

    seq.Members[:] = []

    clust = FutureCellularAutomatonAlg("FutureEcalClust")
    share = CaloFutureShowerOverlap("FutureEcalShare")
    covar = CaloFutureClusterCovarianceAlg("FutureEcalCovar")

    if masksE != []:
        share.EnergyTags = masksE
        covar.EnergyTags = masksE
    if masksP != []:
        share.PositionTags = masksP
        covar.PositionTags = masksP

    if external != '':  # use non-default clusters
        share.InputData = external
        covar.InputData = external
        if makeTag != '':  # make the non-default clusters
            seq.Members += [clust]
            clust.OutputData = external
    else:
        seq.Members += [clust]  # make default clusters

    if clusterPt > 0:
        clust.ETcut = clusterPt
        clust.withET = True

    seq.Members += [share, covar]

    ## setup onDemand for SplitClusters
    if enableRecoOnDemand:
        ## DG,20181105: temprorarily always use clusterOnly = False and run full CaloFutureMergedPi0 algo
        ## (CreateSplitClustersOnly flag ignored after conversion to Gaudi::Functional, and
        ##  the full algorithm is run creating all the 3 outputs: split-clusters, pi0 and split-photon CaloHypos)
        splitSeq = mergedPi0FutureReco(enableRecoOnDemand, False, False, False,
                                       mergedPi0Pt, False, '', masksE, masksP)

    ## printout
    _log.debug('Configure Ecal Cluster Reco Seq : %s' % (seq.name()))
    if enableRecoOnDemand:
        _log.info('ClusterFutureReco onDemand')

    ##
    return seq


# ============================================================================
## define the reconstruction of Single Photons and Electrons
def classifyClusPhotonElec(enableRecoOnDemand,
                           useTracks=True,
                           trackLocation='',
                           neutralID=True,
                           photonEt=50. * MeV,
                           electronPt=50. * MeV,
                           fastReco=False,
                           external='',
                           matchTrTypes=[]):
    """
    Define reconstruction of Single Photons and Electrons
    """
    from Configurables import ClassifyPhotonElectronAlg
    from Configurables import (CaloFutureECorrection, CaloFutureSCorrection,
                               CaloFutureLCorrection)

    ## build the sequence
    seq = GaudiSequencer("ClassifyClusters")
    seq.Members = []  # is this needed?

    # 1/ trackMatch from CaloFuturePIDs
    # chi2 tables needed for classification
    # FIX ME: make dependency explicit
    if useTracks:  # false to run calo standalone
        from CaloFuturePIDs.PIDs import trackMatch
        tm = trackMatch(enableRecoOnDemand, trackLocation, matchTrTypes,
                        fastReco, external)
        addAlgs(seq, tm)

    # 2/ classify clusters in Single Photon and Electron hypotheses
    alg = ClassifyPhotonElectronAlg("ClassifyPhotonElectron")

    if external != '':
        alg.InputData = external

    alg.MinDigits = 2  # skip single-cell clusters
    alg.PhotonMinEt = photonEt
    alg.ElectrMinEt = electronPt
    alg.PhotonMinChi2 = 4
    alg.ElectrMaxChi2 = 25

    ## correction tools : E/S/L
    ecorrph = CaloFutureECorrection('ECorrectionPhoton')
    scorrph = CaloFutureSCorrection('SCorrectionPhoton')
    lcorrph = CaloFutureLCorrection('LCorrectionPhoton')

    ecorrph.ConditionName = 'Conditions/Reco/Calo/PhotonECorrection'
    scorrph.ConditionName = 'Conditions/Reco/Calo/PhotonSCorrection'
    lcorrph.ConditionName = 'Conditions/Reco/Calo/PhotonLCorrection'

    ecorrel = CaloFutureECorrection('ECorrectionElectron')
    scorrel = CaloFutureSCorrection('SCorrectionElectron')
    lcorrel = CaloFutureLCorrection('LCorrectionElectron')

    ecorrel.ConditionName = 'Conditions/Reco/Calo/ElectronECorrection'
    scorrel.ConditionName = 'Conditions/Reco/Calo/ElectronSCorrection'
    lcorrel.ConditionName = 'Conditions/Reco/Calo/ElectronLCorrection'

    # tool configuration via condDB only (remove default configuration)
    alg.PhotonCorrectionTools = [ecorrph, scorrph, lcorrph]
    alg.ElectronCorrectionTools = [ecorrel, scorrel, lcorrel]

    # update the sequence
    addAlgs(seq, alg)

    # 3/ PhotonID
    from CaloFuturePIDs.PIDs import PhotonID
    if neutralID:
        pID = PhotonID(enableRecoOnDemand, useTracks)
        addAlgs(seq, pID)

    ## printout
    _log.debug('Configure Classify Clusters Seq : %s' % seq.name())
    if enableRecoOnDemand:
        _log.info('ClassifyClusters onDemand')

    return seq


# =============================================================================
## define the reconstruction of Merged Pi0s Hypos
def mergedPi0FutureReco(enableRecoOnDemand,
                        clusterOnly=False,
                        neutralID=True,
                        useTracks=True,
                        mergedPi0Pt=2 * GeV,
                        fastReco=False,
                        external='',
                        masksE=[],
                        masksP=[]):
    """
    Define the recontruction of Merged Pi0s
    """

    from Configurables import CaloFutureMergedPi0  # CaloFutureMergedPi0Alg is obsolete (replaced by CaloFutureMergedPi0)
    from Configurables import CaloFutureHypoAlg

    from Configurables import (CaloFutureECorrection, CaloFutureSCorrection,
                               CaloFutureLCorrection)

    ## DG,20181105: temporary stub related to disabling 'CaloFutureMergedPi0.CreateSplitClustersOnly = True' flag
    ## The code below related to 'clusterOnly = True' is disabled but not removed b/c it might be useful in a future remake.
    ## For the record: if the previous functionality allowing possibility to create two separate sub-sequences
    ##  - sseq(with reduced version of the pi0 algo, producing only split-clusters) and
    ##  - seq (with the full version of the pi0 algo, producing split-clusters, merged pi0 and split-photon CaloHypos)
    ## is necessary, then a possible solution could be to split the CaloFutureMergedPi0 into two separate algorithms,
    ## which would perhaps also need changing from the default keys used to store CaloClusters in KeyedContainer to the seed cellID.
    ## (To allow fast location of the original un-split cluster by the seed cell, stored in the first split-cluster in each pair
    ## of split-clustes, necessary in order to fill properly the list of related clusters in the pi0 and split-photon CaloHypos.)
    ## In the previous implementation with the 2 sub-sequences, the part of the CaloMergedPi0 algorithm, which created the
    ## split-clusters, was run twice by both instances of the algo, and the resulting split-clusters were overwritten on TES
    ## by the second instance, which looks sub-optimal and also does not seem to match well the Gaudi::Functional approach.
    if clusterOnly:
        _log.warning(
            'mergedPi0FutureReco.clusterOnly is deprecated - will be ignored!')
        clusterOnly = False

    # build the sequences
    seq = GaudiSequencer('MergedPi0FutureReco')
    sseq = GaudiSequencer('SplitClusterFutureReco')

    ## Merged Pi0
    if clusterOnly:
        pi0 = CaloFutureMergedPi0('SplitClustersRec')
        pi0.CreateSplitClustersOnly = True
        sseq.Members = [pi0]
    else:
        pi0 = CaloFutureMergedPi0('MergedPi0RecFuture')

    if masksE != []:
        pi0.EnergyTags = masksE
    if masksP != []:
        pi0.PositionTags = masksP

    if external != '':
        pi0.InputData = external

    pi0.EtCut = mergedPi0Pt

    if clusterOnly:
        return sseq

    ## MergedPi0 sequence

    ## 2/ SplitPhotons
    #    splitg = CaloFutureHypoAlg('PhotonFromMergedRec')
    #    splitg.HypoType = "SplitPhotons";
    #    splitg.PropertiesPrint = False

    ## Pi0 correction tools
    pi0.addTool(CaloFutureECorrection, 'ECorrectionFuture')
    pi0.addTool(CaloFutureSCorrection, 'SCorrectionFuture')
    pi0.addTool(CaloFutureLCorrection, 'LCorrectionFuture')

    # tool configuration via condDB only (remove default configuration)
    pi0.PhotonTools = [
        pi0.ECorrectionFuture, pi0.SCorrectionFuture, pi0.LCorrectionFuture
    ]

    seq.Members = [pi0]

    ## 3/ (PhotonFrom)MergedID
    if neutralID:
        from CaloFuturePIDs.PIDs import (MergedID, PhotonFromMergedID)
        idSeq = GaudiSequencer("MergedIDSeq")
        mID = MergedID(enableRecoOnDemand, useTracks)
        pmID = PhotonFromMergedID(enableRecoOnDemand, useTracks)
        idSeq.Members = [mID, pmID]
        addAlgs(seq, idSeq)

    ## printout
    _log.debug('Configure MergedPi0 Reco Seq : %s' % (seq.name()))
    if enableRecoOnDemand:
        _log.info('MergedPi0FutureReco onDemand')

    return seq


# =============================================================================
if '__main__' == __name__:
    print __doc__

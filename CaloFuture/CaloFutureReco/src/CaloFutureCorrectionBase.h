/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURECORRECTIONBASE_H
#define CALOFUTURECORRECTIONBASE_H 1

// Include files
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "DetDesc/Condition.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/CaloCellID.h"
#include "Relations/IRelationWeighted2D.h"

// VDT
#include "vdt/vdtMath.h"

static const InterfaceID IID_CaloFutureCorrectionBase( "CaloFutureCorrectionBase", 1,
                                                       0 ); // TODO: probably change IF version (?)

/** @class CaloFutureCorrectionBase CaloFutureCorrectionBase.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-05-07
 */

namespace CaloFutureCorrectionUtils {
  class DigitFromCaloFuture {
  public:
    explicit DigitFromCaloFuture( const int calo ) : m_calo( calo ){};
    explicit DigitFromCaloFuture( const std::string& calo )
        : DigitFromCaloFuture( CaloCellCode::CaloNumFromName( calo ) ){};
    inline bool operator()( const LHCb::CaloDigit* digit ) const {
      return digit && ( ( (int)digit->cellID().calo() ) == m_calo );
    };
    DigitFromCaloFuture() = delete;

  private:
    int m_calo{0};
  };
} // namespace CaloFutureCorrectionUtils
// DO NOT CHANGE THE FUNCTION ORDER FOR BACKWARD COMPATIBILITY WITH EXISTING CONDITIONS DB
namespace CaloFutureCorrection {
  enum Function {
    InversPolynomial     = 0,
    Polynomial           = 1,
    ExpPolynomial        = 2,
    ReciprocalPolynomial = 3,
    Sigmoid              = 4,
    Sshape               = 5,
    ShowerProfile        = 6,
    SshapeMod            = 7,
    Sinusoidal           = 8,
    ParamList            = 9,  // simple parameter access (by area)
    GlobalParamList      = 10, // simple parameter access (ind. of area)
    Unknown                    // MUST be the last item
  };
  using ParamVector = std::vector<double>;
  using Parameters  = std::pair<CaloFutureCorrection::Function, ParamVector>;
  enum Type {
    // NB: numbering is not continuous due to removed PRS and SPD/Converted_photons -related parameters for Run 1-2
    // CondDB compatibility
    // E-Correction parameters
    alphaG = 0,             // global alpha factor #0
    alphaE,                 // alpha(E)    #1
    alphaB,                 // alpha(Bary) #2
    alphaX,                 // alpha(Dx)   #3
    alphaY,                 // alpha(Dy)   #4
    globalT = 13,           // global(DeltaTheta) function of incidence angle #13
    offsetT,                // offset(DeltaTheta) function of incidence angle #14
    offset,                 // offset( sinTheta ) energy (or ET ) offset #15
    ClusterCovariance = 17, // parameters for cluster covariance estimation #17
    // L-Correction parameters
    gamma0, // #18
    delta0, // #19
    gammaP, // Prs-related L-correction (non-zero at ePrs = 0) #20
    deltaP, // Prs-related L-correction (non-sero at ePrs = 0) #21
    // S-correction parameters
    shapeX,    // #22
    shapeY,    // #23
    residual,  // #24
    residualX, // #25
    residualY, // #26
    asymP,     // #27
    asymM,     // #28
    angularX,  // #29
    angularY,  // #30
    // ShowerShape profile
    profile, // #31
    // Cluster masking
    EnergyMask = 33, // #33
    PositionMask,    // #34
    lastType         // MUST BE THE LAST LINE
  };

  constexpr int            nT           = lastType + 1;
  constexpr int            nF           = Unknown + 1;
  inline const std::string typeName[nT] = {"alphaG",
                                           "alphaE",
                                           "alphaB",
                                           "alphaX",
                                           "alphaY",
                                           "RESERVED(alphaP)",
                                           "RESERVED(beta)",
                                           "RESERVED(betaP)",
                                           "RESERVED(betaPR)",
                                           "RESERVED(betaC)",
                                           "RESERVED(betaCP)",
                                           "RESERVED(betaCPR)" // E-corrections
                                           ,
                                           "RESERVED(globalC)",
                                           "globalT",
                                           "offsetT",
                                           "offset",
                                           "RESERVED(offsetC)",
                                           "ClusterCovariance",
                                           "gamma0",
                                           "delta0",
                                           "gammaP",
                                           "deltaP" // L-Corrections
                                           ,
                                           "shapeX",
                                           "shapeY",
                                           "residual",
                                           "residualX",
                                           "residualY",
                                           "asymP",
                                           "asymM",
                                           "angularX",
                                           "angularY" // S-Corrections
                                           ,
                                           "profile",
                                           "RESERVED(profileC)" // Profile shape
                                           ,
                                           "EnergyMask",
                                           "PositionMask",
                                           "Unknown"};

  inline const std::string funcName[nF] = {"InversPolynomial", "Polynomial", "ExpPolynomial",   "ReciprocalPolynomial",
                                           "Sigmoid",          "Sshape",     "ShowerProfile",   "SshapeMod",
                                           "Sinusoidal",       "ParamList",  "GlobalParamList", "Unknown"};
} // namespace CaloFutureCorrection

class CaloFutureCorrectionBase : public GaudiTool {

public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_CaloFutureCorrectionBase; }

  /// Standard constructor
  CaloFutureCorrectionBase( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode finalize() override;   ///< Algorithm finalization

  void               setOrigin( Gaudi::XYZPoint origin ) { m_origin = origin; }
  virtual StatusCode updParams();

  StatusCode setConditionParams( const std::string& cond,
                                 bool               force = false ) { // force = true : forcing access via condDB only
    if ( cond != m_conditionName ) m_conditionName = cond;

    // get parameters from options  :
    if ( !m_useCondDB && !force ) return setOptParams();

    // get from DB if exists :
    if ( !existDet<DataObject>( m_conditionName ) ) {
      if ( force ) {
        if ( m_conditionName != "none" )
          info() << "Condition '" << m_conditionName.value() << "' has not been found " << endmsg;
        return StatusCode::SUCCESS;
      } else {
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " Condition '" << m_conditionName.value() << "' has not found -- try options parameters !"
                  << endmsg;
        return setOptParams();
      }
    }
    return setDBParams();
  }

  // accessors
  CaloFutureCorrection::Parameters         getParams( const CaloFutureCorrection::Type type,
                                                      const LHCb::CaloCellID           id = LHCb::CaloCellID() ) const;
  inline CaloFutureCorrection::ParamVector getParamVector( const CaloFutureCorrection::Type type,
                                                           const LHCb::CaloCellID id = LHCb::CaloCellID() ) const {
    return getParams( type, id ).second;
  }
  double getParameter( CaloFutureCorrection::Type type, unsigned int i, const LHCb::CaloCellID id = LHCb::CaloCellID(),
                       double def = 0. ) const {
    const auto  params = getParams( type, id );
    const auto& data   = params.second;
    return ( i < data.size() ) ? data[i] : def;
  }
  /// calculate analytic derivative for a given function type in cell id at a given point var with default value def
  double getCorrectionDerivative( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id, double var = 0.,
                                  double def = 0. ) const;
  /// propagate cov.m. cov0 according to Jacobian jac: cov1 = (jac * cov * jac^T), see comments in
  /// CaloFutureECorrection.cpp and CaloFutureSCorrection.cpp
  // void recalculate_cov(const TMatrixD &jac, const TMatrixDSym &cov0, TMatrixDSym &cov1) const;

  double getCorrection( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id, double var = 0.,
                        double def = 1. ) const;

protected:
  Gaudi::Property<std::string>              m_conditionName{this, "ConditionName", "none"};
  Gaudi::Property<std::vector<std::string>> m_corrections{this, "Corrections", {"All"}};
  //
  typedef std::vector<LHCb::CaloHypo::Hypothesis> Hypotheses;
  typedef std::vector<int>                        Hypotheses_;
  Hypotheses                                      m_hypos;
  Gaudi::Property<Hypotheses_>                    m_hypos_{this,
                                        "Hypotheses",
                                        {
                                            (int)LHCb::CaloHypo::Hypothesis::Photon,
                                            (int)LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                                            (int)LHCb::CaloHypo::Hypothesis::EmCharged,
                                        },
                                        "acceptable hypotheses"};

  LHCb::ClusterFunctors::ClusterArea           m_area;
  LHCb::ClusterFunctors::ClusterFromCaloFuture m_calo{DeCalorimeterLocation::Ecal};
  std::string                                  m_detData{DeCalorimeterLocation::Ecal};
  const DeCalorimeter*                         m_det = nullptr;
  Gaudi::XYZPoint                              m_origin;
  Gaudi::Property<bool>                        m_correctCovariance{this, "CorrectCovariance", true};

  template <typename TYPE>
  static TYPE myexp( const TYPE x ) {
    return vdt::fast_exp( x );
  }

  template <typename TYPE>
  static TYPE mylog( const TYPE x ) {
    return vdt::fast_log( x );
  }

  template <typename TYPE>
  static TYPE myatan( const TYPE x ) {
    return vdt::fast_atan( x );
  }

  template <typename TYPE>
  static TYPE myatan2( const TYPE x, const TYPE y ) {
    return vdt::fast_atan2( x, y );
  }

  template <typename TYPE>
  static TYPE mycos( const TYPE x ) {
    return vdt::fast_cos( x );
  }

  template <typename TYPE>
  static TYPE mysin( const TYPE x ) {
    return vdt::fast_sin( x );
  }

  template <typename TYPE>
  static TYPE mytanh( const TYPE x ) {
    const auto y = myexp( -2.0 * x );
    return ( 1.0 - y ) / ( 1.0 + y );
  }

  template <typename TYPE>
  static TYPE mysinh( const TYPE x ) {
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) - y );
  }

  template <typename TYPE>
  static TYPE mycosh( const TYPE x ) {
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) + y );
  }

private:
  inline CaloFutureCorrection::Type stringToCorrectionType( const std::string& type ) {
    for ( int i = 0; i < CaloFutureCorrection::nT; ++i ) {
      if ( CaloFutureCorrection::typeName[i] == type ) return static_cast<CaloFutureCorrection::Type>( i );
    }
    return static_cast<CaloFutureCorrection::Type>( CaloFutureCorrection::lastType );
  }

  LHCb::Calo::Interfaces::IElectron* m_caloElectron = nullptr;

  bool accept( const std::string& name ) {
    return std::any_of( m_corrections.begin(), m_corrections.end(),
                        [&]( const auto& i ) { return i == name || i == "All"; } );
  }

  StatusCode setOptParams();
  StatusCode setDBParams();
  void       checkParams();

  class Params {
  public:
    Params() = default;
    Params( const std::string& t, const CaloFutureCorrection::ParamVector& v ) : active( true ), type( t ), data( v ) {}

  public:
    void clear() {
      active = false;
      data.clear();
    }

  public:
    bool                              active{false};
    std::string                       type;
    CaloFutureCorrection::ParamVector data;

  public:
    typedef std::vector<Params> Vector;
  };

  /// Cache counters, as looking them up as
  ///    counter( CaloFutureCorrection::typeName[ type ] + " correction processing (" + areaName + ")" )
  /// requires the creation of a temporary string every time, which in turn
  /// involves (eventually) a call to 'new' and 'delete'  -- and the above was the source of 10% (!)
  /// of the # of calls to 'new' and 'delete' in the HLT!!!! (FYI: there are, on average, 1K calls
  /// per event to this method!!!)
  ///
  /// On top of that, this also speeds up the actual search for the correct counter,
  /// by making it a two-step process, and the first step is a direct lookup
  /// instead of a 'find'.
  ///
  /// in the end, this change alone speeds up the total HLT by about 1%...
  inline StatEntity& kounter( const CaloFutureCorrection::Type type, const std::string& areaName ) const {
    assert( type < CaloFutureCorrection::lastType + 1 );
    auto a = m_counters[type].find( areaName );
    if ( UNLIKELY( a == std::end( m_counters[type] ) ) ) {
      const auto name = CaloFutureCorrection::typeName[type] + " correction processing (" + areaName + ")";
      auto       r    = m_counters[type].insert( areaName, &counter( name ) );
      assert( r.second );
      a = r.first;
    }
    assert( a->second );
    return *( a->second );
  }

private:
  Params::Vector m_params{CaloFutureCorrection::nT};

  Gaudi::Property<std::map<std::string, std::vector<double>>> m_optParams{this, "Parameters"};

  mutable std::array<GaudiUtils::VectorMap<std::string, StatEntity*>, CaloFutureCorrection::lastType + 1> m_counters;
  Condition*                   m_cond = nullptr;
  Gaudi::Property<std::string> m_cmLoc{this, "ClusterMatchLocation"};
  Gaudi::Property<bool>        m_useCondDB{this, "UseCondDB", true};

protected:
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CALOFUTURECORRECTIONBASE_H

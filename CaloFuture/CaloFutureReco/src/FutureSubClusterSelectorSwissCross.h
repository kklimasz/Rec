/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_SUBCLUSTERSELECTORSwissCross_H
#define CALOFUTURERECO_SUBCLUSTERSELECTORSwissCross_H 1
// Include files
// CaloFutureTools
#include "FutureSubClusterSelectorBase.h"
// CaloFutureUtils
#include "CaloFutureUtils/CellSwissCross.h"

class FutureSubClusterSelectorSwissCross : public FutureSubClusterSelectorBase {
public:
  StatusCode initialize() override;

  StatusCode tag( LHCb::CaloCluster& cluster ) const override;
  StatusCode untag( LHCb::CaloCluster& cluster ) const override;

  FutureSubClusterSelectorSwissCross( const std::string& type, const std::string& name, const IInterface* parent );

private:
  CellSwissCross m_matrix;
};

#endif

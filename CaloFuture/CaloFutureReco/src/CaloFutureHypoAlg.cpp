/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// CaloFutureInterfaces
// ============================================================================
#include "ICaloFutureHypoTool.h"
// ============================================================================
// CaloFutureEvent/Event
// ============================================================================
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloHypo.h"
// ============================================================================
// local
// ============================================================================
#include "CaloFutureHypoAlg.h"
// ============================================================================
/** @file CaloFutureHypoAlg.cpp
 *
 *  Template implementation file for class : CaloFutureHypoAlg
 *  @see CaloFutureHypoAlg
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 02/09/2002
 */
// ============================================================================
DECLARE_COMPONENT( CaloFutureHypoAlg )
// ============================================================================
/*  Standard constructor
 *  @param   name   algorithm name
 *  @param   svcloc pointer to service locator
 */
// ============================================================================
CaloFutureHypoAlg::CaloFutureHypoAlg( const std::string& name, ISvcLocator* svcloc ) : GaudiAlgorithm( name, svcloc ) {
  setProperty( "PropertiesPrint", true );
}
// ============================================================================
/*  standard algorithm initialization
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return Error( "Could not initialize the base class CaloFutureAlgorithm", sc ); }

  if ( m_inputData.empty() && !m_type.empty() )
    m_inputData = LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( m_type.value() );

  if ( m_inputData.empty() ) { return Error( "Empty 'InptuData'" ); }
  //
  for ( auto& it : m_names ) {
    auto* t = tool<LHCb::Calo::Interfaces::IProcessHypos>( it, this );
    if ( !t ) { return Error( "Could not locate the tool!" ); }
    m_tools.push_back( t );
  }
  //
  if ( m_tools.empty() ) { Warning( "Empty list of tools" ).ignore(); }
  //
  counterStat = tool<IFutureCounterLevel>( "FutureCounterLevel" );
  return StatusCode::SUCCESS;
}
// ============================================================================
/*  standard algorithm finalization
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::finalize() {
  // clear the container
  m_tools.clear();
  /// finalize the base class
  return GaudiAlgorithm::finalize();
}
// ============================================================================
/*  standard algorithm execution
 *  @see CaloFutureAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *  @return status code
 */
// ============================================================================
StatusCode CaloFutureHypoAlg::execute() {

  LHCb::CaloHypos* hypos = get<LHCb::CaloHypos>( m_inputData );
  if ( !hypos ) { return StatusCode::FAILURE; }

  for ( auto* hypo : *hypos ) {
    if ( !hypo ) { continue; }
    // loop over all tools
    StatusCode sc =
        std::accumulate( m_tools.begin(), m_tools.end(), StatusCode{StatusCode::SUCCESS},
                         [&]( StatusCode sc, auto* tool ) { return sc.isSuccess() ? tool->process( *hypo ) : sc; } );
    if ( sc.isFailure() ) {
      Error( "Error from the Tool! skip hypo!", sc ).ignore();
      continue;
    }
  }

  if ( counterStat->isQuiet() ) counter( "#Hypos from '" + m_inputData + "'" ) += hypos->size();
  //
  return StatusCode::SUCCESS;
}

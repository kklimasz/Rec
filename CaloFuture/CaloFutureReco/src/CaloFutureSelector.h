/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURESELECTOR_H
#define CALOFUTURERECO_CALOFUTURESELECTOR_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"

#include "ICaloFutureClusterSelector.h"
// forward declaratiosn
struct ICaloFutureLikelihood; // from CaloFutureInterfaces ;

/** @class CaloFutureSelector CaloFutureSelector.h
 *
 *  Concrete cluster-selector tool, to select CaloFutureClusters
 *  which satisfy the given calorimeter hypothesis
 *  Tool in turn uses the tool with ICaloFutureLikelihood
 *  interaface to calculate the likelihood.
 *  @see ICaloFutureClusterSelector
 *  @see ICaloFutureLikelyhood
 *  @see CaloCluster
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   31/03/2002
 */
class CaloFutureSelector : public extends<GaudiTool, ICaloFutureClusterSelector> {
public:
  /** "select"/"preselect" method
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::CaloCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::CaloCluster* cluster ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

  /** Standard constructor
   */
  using extends::extends;

private:
  // type of Likelyhood tool to be used
  Gaudi::Property<std::string> m_lhType{this, "LikelihoodType", "", "type of Likelyhood tool to be used"};

  // name of Likelyhood tool to be used
  Gaudi::Property<std::string> m_lhName{this, "LikelihoodName", "", "name of Likelyhood tool to be used"};

  // Likelyhood tool to be used
  ICaloFutureLikelihood* m_likelihood = nullptr;

  // cut on likelyhood
  Gaudi::Property<double> m_cut{this, "LikelihoodCut", 1.e+50, "cut on likelyhood"};
};

// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESELECTOR_H

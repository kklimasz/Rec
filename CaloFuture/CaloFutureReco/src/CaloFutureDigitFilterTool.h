/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREDIGITFILTERTOOL_H
#define CALOFUTUREDIGITFILTERTOOL_H 1

class StatusCode;
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureCellIDAsProperty.h"
#include "Event/CaloDigit.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "ICaloFutureDigitFilterTool.h" // Interface
#include "Kernel/CaloCellID.h"

#include <memory>
#include <optional>
#include <unordered_map>

namespace {
  inline constexpr auto Default = static_cast<CaloCellCode::CaloIndex>( -1 );
}

/** @class CaloFutureDigitFilterTool CaloFutureDigitFilterTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-12-13
 */
class CaloFutureDigitFilterTool final : public extends<GaudiTool, ICaloFutureDigitFilterTool, IIncidentListener> {
public:
  /// Standard constructor
  CaloFutureDigitFilterTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode finalize() override;   ///< Algorithm finalization

  int getScale() override;
  int method( CaloCellCode::CaloIndex det ) override {
    if ( !m_calo || det != m_calo->index() ) { setDet( det ); }
    return m_scalingMethod;
  }
  double offset( LHCb::CaloCellID id ) override;

private:
  unsigned int nVertices();
  StatusCode   caloUpdate(); /// Triggered by calo updates
  bool         setDet( const CaloCellCode::CaloIndex det );
  double       getOffset( LHCb::CaloCellID id, int scale ) const;
  void         handle( const Incident& /* inc */ ) override {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "IIncident Svc reset" << endmsg;
    m_scale.reset();
  }

  using Offsets = std::map<LHCb::CaloCellID, double>;
  const Offsets& offsets() const noexcept { return *m_offsets; }
  const Offsets& offsetsRMS() const noexcept { return *m_offsetsRMS; }
  struct CondMaps {
    Offsets offsets, offsetsRMS;
  };
  const CondMaps& createMaps( DeCalorimeter* calo, bool regUpdate = true );
  void            setMaps( const CondMaps& maps ) const {
    m_offsets    = &maps.offsets;
    m_offsetsRMS = &maps.offsetsRMS;
  }

private:
  Gaudi::Property<int>         m_scalingMethod{this, "ScalingMethod", 0}; // 0 : SpdMult ; 1 = nPV  (+10 for clusters)
  Gaudi::Property<bool>        m_useCondDB{this, "UseCondDB", true};
  Gaudi::Property<int>         m_scalingBin{this, "ScalingBin", 50};
  Gaudi::Property<double>      m_scalingMin{this, "ScalingMin", 150.};
  Gaudi::Property<std::string> m_vertLoc{this, "VertexLoc", ""};
  Gaudi::Property<bool>        m_usePV3D{this, "UsePV3D", false};
  Gaudi::Property<std::map<CaloCellCode::CaloIndex, int>> m_maskMap{
      this, "MaskMap", {std::pair{Default, CaloCellQuality::OfflineMask}}};

  CondMaps              m_ecalMaps;
  CondMaps              m_hcalMaps;
  CondMaps              m_prsMaps;
  static const CondMaps m_nullMaps;

  std::map<DeCalorimeter*, std::unique_ptr<CondMaps>> m_offsetMap;
  using OffsetsPtr                = Offsets const*;
  mutable OffsetsPtr m_offsets    = nullptr;
  mutable OffsetsPtr m_offsetsRMS = nullptr;

  DeCalorimeter* m_calo = nullptr;

  std::optional<int> m_scale;
};

#endif // CALOFUTUREDIGITFILTERTOOL_H

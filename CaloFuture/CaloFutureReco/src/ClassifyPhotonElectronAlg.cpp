/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#include "ClassifyPhotonElectronAlg.h"

// ============================================================================
/** @file
 *
 *  Implementation file for class: ClassifyPhotonElectronAlg
 *  The implementation is partially based on previous
 *  SinglePhotonAlg and ElectronAlg codes.
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */
// ============================================================================

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Algorithm::ClassifyPhotonElectronAlg, "ClassifyPhotonElectronAlg" )

namespace LHCb::Calo::Algorithm {

  namespace {
    bool apply( const ToolHandleArray<Interfaces::IProcessHypos>& c, CaloHypo& hypo,
                std::optional<std::reference_wrapper<const CaloFuture2Track::IClusTrTable::Range>> matching_tracks =
                    std::nullopt ) {
      return std::all_of( std::begin( c ), std::end( c ),
                          [&]( const auto& elem ) { return elem->correct( hypo, matching_tracks ).isSuccess(); } );
    }
  } // namespace

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  ClassifyPhotonElectronAlg::ClassifyPhotonElectronAlg( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          // Inputs
                          {
                              KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                              KeyValue( "InputClusters", {CaloClusterLocation::Ecal} ),
                              KeyValue( "InputTable", {CaloFutureIdLocation::ClusterMatch} ),
                          },
                          // Outputs
                          {KeyValue( "OutputPhotons", {CaloHypoLocation::Photons} ),
                           KeyValue( "OutputElectrons", {CaloHypoLocation::Electrons} )} ) {}

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  std::tuple<CaloHypos, CaloHypos> ClassifyPhotonElectronAlg::
                                   operator()( const DeCalorimeter& calo, const CaloClusters& clusters, const TrackMatchTable& table ) const {
    // output containers
    auto result                = std::tuple<CaloHypos, CaloHypos>{};
    auto& [photons, electrons] = result;
    photons.reserve( clusters.size() );
    electrons.reserve( clusters.size() );

    // used in the loop
    auto eT = CaloDataFunctor::EnergyTransverse{&calo};

    // loop on clusters
    for ( const auto* cl : clusters ) {
      int m = cl->entries().size();
      // debug info
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "*Variables and cut values:" << endmsg;
        debug() << " - e: " << cl->e() << " " << m_ecut << endmsg;
        debug() << " - eT:" << eT( cl ) << " " << m_eTcut << endmsg;
        debug() << " - pt:" << CaloMomentum( cl ).pt() << " " << m_photonEtCut << endmsg;
        debug() << " - m: " << m << " " << m_minDigits << " " << m_maxDigits << endmsg;
        debug() << " - chi2 cut: " << m_photonChi2Cut << endmsg;
      }

      // check common cluster requirements
      if ( ( cl->e() > m_ecut ) && ( eT( cl ) > m_eTcut ) && ( m > m_minDigits ) && ( m < m_maxDigits ) ) {

        const LHCb::CaloFuture2Track::IClusTrTable::Range& matching_tracks_photonHypo =
            table.relations( cl, m_photonChi2Cut, false );
        // 1. check if cluster fullfills photon hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_photonEtCut ) && matching_tracks_photonHypo.empty() ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies photon e, et, digits, pt and chi2 requirements" << endmsg;
          }

          // get photon hypo and apply corrections
          auto photon_hypo = std::make_unique<CaloHypo>();
          photon_hypo->setHypothesis( CaloHypo::Hypothesis::Photon );
          photon_hypo->addToClusters( cl );
          photon_hypo->setPosition( std::make_unique<CaloPosition>(
              cl->position() ) ); // setPosition takes a std::unique_ptr<CaloPosition> as arg, while cl->position()
                                  // returns a CaloPosition by ref
          bool corr_ok = apply( m_correc_photon, *photon_hypo );
          if ( UNLIKELY( !corr_ok ) ) ++m_errApply;

          // debug
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " - pt hypo: " << CaloMomentum( photon_hypo.get() ).pt() << endmsg;
            debug() << " - pt cut: " << m_eTcut << endmsg;
          }

          // check pt after all corrections
          if ( corr_ok && ( CaloMomentum( photon_hypo.get() ).pt() >= m_eTcut ) ) {
            photons.insert( photon_hypo.release() );
            if ( msgLevel( MSG::DEBUG ) ) debug() << "PASS!" << endmsg;
          }
        }

        const LHCb::CaloFuture2Track::IClusTrTable::Range& matching_tracks_electronHypo =
            table.relations( cl, m_electrChi2Cut, false );

        // 2. check if cluster fullfills electron hypo requirements
        if ( ( CaloMomentum( cl ).pt() >= m_electrEtCut ) && !matching_tracks_electronHypo.empty() ) {
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " --> Cluster satisfies electron e, et, digits, pt and chi2 requirements" << endmsg;
          }

          // get electron hypo and apply corrections

          auto electr_hypo = std::make_unique<CaloHypo>();
          electr_hypo->setHypothesis( CaloHypo::Hypothesis::EmCharged );
          electr_hypo->addToClusters( cl );
          electr_hypo->setPosition( std::make_unique<CaloPosition>( cl->position() ) );
          bool corr_ok = apply( m_correc_electr, *electr_hypo, matching_tracks_electronHypo );
          if ( UNLIKELY( !corr_ok ) ) ++m_errApply;

          // debug
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << " - pt hypo: " << CaloMomentum( electr_hypo.get() ).pt() << endmsg;
            debug() << " - pt cut: " << m_eTcut << endmsg;
          }

          if ( corr_ok && CaloMomentum( electr_hypo.get() ).pt() >= m_electrEtCut ) {
            electrons.insert( electr_hypo.release() );
            if ( msgLevel( MSG::DEBUG ) ) debug() << "PASS!" << endmsg;
          }
        }
      }
    }

    // debug info
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " # of created Photon Hypos is " << photons.size() << endmsg;
      debug() << " # of created Electron Hypos is " << electrons.size() << endmsg;
    }
    // counters
    m_counterPhotons += photons.size();
    m_counterElectrs += electrons.size();

    return result;
  }

} // namespace LHCb::Calo::Algorithm

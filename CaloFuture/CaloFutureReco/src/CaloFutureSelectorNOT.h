/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURESELECTORNOT_H
#define CALOFUTURERECO_CALOFUTURESELECTORNOT_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureClusterSelector.h"
/** @class CaloFutureSelectorNOT CaloFutureSelectorNOT.h
 *
 *  Helper concrete tool for selection of calocluster objects
 *  This selector selects the cluster if
 *  none  of its daughter selector select it!
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   27/04/2002
 */
class CaloFutureSelectorNOT final : public extends<GaudiTool, ICaloFutureClusterSelector> {
public:
  using extends::extends;

  /** "select"/"preselect" method
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::CaloCluster* cluster ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::CaloCluster* cluster ) const override;

  StatusCode initialize() override;
  StatusCode finalize() override;

private:
  Gaudi::Property<std::vector<std::string>> m_selectorsTypeNames{this, "SelectorTools"};
  std::vector<ICaloFutureClusterSelector*>  m_selectors;
  mutable Gaudi::Accumulators::Counter<>    m_counter{this, "selected clusters"};
};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESELECTORNOT_H

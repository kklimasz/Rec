/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CALOFUTURESCORRECTION_H
#define CALOFUTURERECO_CALOFUTURESCORRECTION_H 1
// Include files
// Include files
#include "CaloFutureCorrectionBase.h"
#include "GaudiKernel/Counters.h"
#include "ICaloFutureHypoTool.h"
#include <string>

/** @class CaloFutureSCorrection CaloFutureSCorrection.h
 *
 *
 *   @author Deschamps Olivier
 *  @date   2003-03-10
 */

class CaloFutureSCorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {
public:
  typedef std::reference_wrapper<const LHCb::CaloFuture2Track::IClusTrTable::Range>
             const_ref_range_type; // this makes it possible to be used by std::optional for matching_tracks
  StatusCode correct( LHCb::span<LHCb::CaloHypo* const>   hypos,
                      std::optional<const_ref_range_type> matching_tracks = std::nullopt )
      const override; // default value needed in this suntax for calling process(hypo) without the second argument
  StatusCode process( LHCb::span<LHCb::CaloHypo* const> hypos ) const override { return correct( hypos ); }

public:
  StatusCode initialize() override;
  StatusCode finalize() override;

  CaloFutureSCorrection( const std::string& type, const std::string& name, const IInterface* parent );

private:
  /// input variables calculated once in process() and passed to all calcSCorrection() calls
  //
  struct SCorrInputParams {
    LHCb::CaloCellID cellID;
    Gaudi::XYZPoint  seedPos;
    double           z;
  };

  /// Jacobian elements returned from calcSCorrection() to process()
  struct SCorrOutputParams {
    double dXhy_dXcl;
    double dYhy_dYcl;
  };

  /// calculate corrected CaloHypo position depending on CaloCluster position
  void calcSCorrection( double xBar, double yBar, double& xCor, double& yCor, const struct SCorrInputParams& params,
                        struct SCorrOutputParams* results = nullptr ) const;

  /// numeric partial derivatives w.r.t. X and Y, necessary to check after any change to the S-corrections
  void debugDerivativesCalculation( const double& xBar, const double& yBar, const double& xCor, const double& yCor,
                                    const double& dXhy_dXcl, const double& dYhy_dYcl,
                                    const struct SCorrInputParams& params ) const;

private:
  using IncCounter    = Gaudi::Accumulators::Counter<>;
  using SCounter      = Gaudi::Accumulators::StatCounter<float>;
  using MapOfCounters = std::map<std::string, SCounter>;

  mutable IncCounter m_counterSkipNegativeEnergyCorrection{this, "Skip negative energy correction"};
  mutable SCounter   m_counterDeltaX{this, "Delta(X)"};
  mutable SCounter   m_counterDeltaY{this, "Delta(Y)"};
};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESCORRECTION_H

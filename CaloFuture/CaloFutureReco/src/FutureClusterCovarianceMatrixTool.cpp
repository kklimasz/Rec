/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "FutureClusterCovarianceMatrixTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloCluster.h"

// ============================================================================
/** @file
 *
 *  Implementation file for class FutureClusterCovarianceMatrixTool
 *
 *  @date 02/11/2001
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  modified 02/07/2014 by O. Deschamps
 */
// ============================================================================

DECLARE_COMPONENT( FutureClusterCovarianceMatrixTool )

// ============================================================================
/** Standard constructor
 *  @param type tool type (useless)
 *  @param name tool name
 *  @param parent pointer to parent object (service, algorithm or tool)
 */
// ============================================================================
FutureClusterCovarianceMatrixTool::FutureClusterCovarianceMatrixTool( const std::string& type, const std::string& name,
                                                                      const IInterface* parent )
    : extends( type, name, parent ) {

  // set default configuration as a function of detector
  using namespace CaloFutureCovariance;
  m_detData            = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name );
  std::string caloName = LHCb::CaloFutureAlgUtils::CaloFutureNameFromAlg( name );
  m_conditionName      = "Conditions/Reco/Calo/" + caloName + "Covariance";

  // get parameters from parent property when defined
  ParameterProperty p( "CovarianceParameters", m_parameters.value() );
  if ( const IProperty* prop = dynamic_cast<const IProperty*>( parent );
       prop && prop->getProperty( &p ).isSuccess() && !p.value().empty() ) {
    m_parameters = p.value();
    m_useDB      = false; // parent default settings win !
  }

  // apply local parameters if not defined in parent algorithm
  auto push_if_not_present = [&]( const auto& key, auto value ) {
    if ( m_parameters.find( ParameterName[key] ) == m_parameters.end() )
      m_parameters[ParameterName[key]].push_back( value );
  };

  const auto ecal_default = {std::pair{Stochastic, 0.10},      std::pair{GainError, 0.01},
                             std::pair{IncoherentNoise, 1.20}, std::pair{CoherentNoise, 0.30},
                             std::pair{ConstantE, 0.},         std::pair{ConstantX, 0.},
                             std::pair{ConstantY, 0.}};

  const auto hcal_default = {std::pair{Stochastic, 0.70},      std::pair{GainError, 0.10},
                             std::pair{IncoherentNoise, 1.20}, std::pair{CoherentNoise, 0.30},
                             std::pair{ConstantE, 0.},         std::pair{ConstantX, 0.},
                             std::pair{ConstantY, 0.}};

  if ( caloName == "Ecal" ) {
    for ( const auto& [key, value] : ecal_default ) push_if_not_present( key, value );
  } else if ( caloName == "Hcal" ) {
    for ( const auto& [key, value] : hcal_default ) push_if_not_present( key, value );
  }
}

//==============================================================================

StatusCode FutureClusterCovarianceMatrixTool::getParamsFromOptions() {
  m_source_is_db.clear();
  unsigned int nareas = m_det->numberOfAreas();
  for ( auto& [_, pars] : m_parameters ) {
    if ( pars.size() == 1 ) pars = std::vector<double>( nareas, pars[0] );
    if ( pars.size() != nareas ) return Error( "Parameters must be set for each calo area", StatusCode::FAILURE );
  }
  // check all expected parameters are defined
  using namespace CaloFutureCovariance;
  for ( unsigned int index = 0; index < CaloFutureCovariance::Last; ++index ) {
    if ( m_parameters.find( ParameterName[index] ) == m_parameters.end() )
      return Error( "No default value for parameter '" + ParameterName[index] + "'", StatusCode::FAILURE );
    m_source_is_db[index] = false;
  }
  return StatusCode::SUCCESS;
}

//------
StatusCode FutureClusterCovarianceMatrixTool::getParamsFromDB() {

  unsigned int nareas = m_det->numberOfAreas();
  // overwrite m_parameters using DB value
  if ( !m_useDB ) return StatusCode::SUCCESS;
  m_source_is_db.clear();
  using namespace CaloFutureCovariance;
  ParameterMap parameters;
  for ( unsigned int area = 0; area < nareas; ++area ) {     // loop over calo area
    const LHCb::CaloCellID id( m_det->index(), area, 0, 0 ); // fake cell
    const auto&            params = m_dbAccessor->getParamVector( CaloFutureCorrection::ClusterCovariance, id );
    if ( params.size() > CaloFutureCovariance::Last )
      Warning( "Parameters vector exceeds the number of known parameters - only " + Gaudi::Utils::toString( Last ) +
                   " parameters will be applied",
               StatusCode::SUCCESS )
          .ignore();
    for ( unsigned int index = 0; index < CaloFutureCovariance::Last; ++index ) {
      if ( index < params.size() ) {
        parameters[ParameterName[index]].push_back( params[index] );
        m_source_is_db[index] = true;
      } else {
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << "Parameter '" << ParameterName[index] << "' not found in DB - use default options value" << endmsg;
        if ( m_parameters.find( ParameterName[index] ) == m_parameters.end() )
          return Error( "No default value for parameter '" + ParameterName[index] + "'", StatusCode::FAILURE );
        parameters[ParameterName[index]].push_back( m_parameters[ParameterName[index]][area] );
        m_source_is_db[index] = false;
      }
    }
  }
  m_parameters = parameters;
  return m_parameters.empty() ? StatusCode::FAILURE // no parameters set
                              : StatusCode::SUCCESS;
}

//-------
void FutureClusterCovarianceMatrixTool::setEstimatorParams( bool init ) {

  // update DB parameters
  if ( !init && !m_useDB ) return; // estimator setting via options :  at initialization only
  if ( m_useDB && getParamsFromDB().isFailure() ) {
    Error( "Failed updating the covariance parameters from DB", StatusCode::FAILURE ).ignore(); // update DB parameters
    return; // failed to update parameters from DB
  }
  using namespace CaloFutureCovariance;
  m_estimator.setStochastic( m_parameters[ParameterName[Stochastic]] );
  m_estimator.setGainError( m_parameters[ParameterName[GainError]] );
  m_estimator.setIncoherentNoise( m_parameters[ParameterName[IncoherentNoise]] );
  m_estimator.setCoherentNoise( m_parameters[ParameterName[CoherentNoise]] );
  m_estimator.setConstantE( m_parameters[ParameterName[ConstantE]] );
  m_estimator.setConstantX( m_parameters[ParameterName[ConstantX]] );
  m_estimator.setConstantY( m_parameters[ParameterName[ConstantY]] );

  ++m_parUpdate;
}

//---------
StatusCode FutureClusterCovarianceMatrixTool::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) { return Error( "Could not initialize the base class!" ); }

  // get detector
  m_det = getDet<DeCalorimeter>( m_detData );

  // set DB accessor
  m_dbAccessor.retrieve();
  if ( m_useDB && ( m_conditionName.empty() || m_dbAccessor->setConditionParams( m_conditionName, true ).isFailure() ) )
    return Error( "Cannot access DB", StatusCode::FAILURE );

  // always set default parameters from options (will be updated by DB if requested)
  sc = getParamsFromOptions();

  // check the parameters consistency
  for ( const auto& [name, _] : m_parameters ) {
    bool ok =
        std::any_of( std::begin( CaloFutureCovariance::ParameterName ), std::end( CaloFutureCovariance::ParameterName ),
                     [name = std::cref( name )]( const auto& n ) { return n == name.get(); } );
    if ( !ok ) return Error( "Parameter type '" + name + "' is unknown", StatusCode::FAILURE );
  }

  if ( m_useDB ) {
    registerCondition( m_conditionName.value(), &FutureClusterCovarianceMatrixTool::setEstimatorParams_wrapper );
  }

  // configure estimator (possibly from DB if requested)
  m_estimator.setDetector( m_det );
  setEstimatorParams( true ); // force initialization
  info() << " Has initialized with parameters: " << endmsg << " \t 'Detector'         = '" << m_detData.value() << "'"
         << endmsg << " \t ==  Parameters for covariance estimation ==" << endmsg;
  using namespace CaloFutureCovariance;
  for ( unsigned int index = 0; index < Last; ++index ) {
    info() << ParameterName[index] << " \t : " << m_parameters[ParameterName[index]] << " " << ParameterUnit[index]
           << "\t : from " << ( m_source_is_db[index] ? "Covariance DB" : "options" ) << endmsg;
  }
  return sc;
}

// ============================================================================
StatusCode FutureClusterCovarianceMatrixTool::operator()( LHCb::CaloCluster& cluster ) const {
  /// check the argument
  if ( !m_estimator.detector() )
    return Error( "DeCalorimeter* points to NULL!" ); // TODO: consider moving to initialize
  /// apply the estimator
  return m_estimator( &cluster );
}
// ============================================================================

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===================================================q=========================
#ifndef CALOFUTURERECO_CALOFUTURESELECTNEUTRALCLUSTERWITHTRACKS_H
#define CALOFUTURERECO_CALOFUTURESELECTNEUTRALCLUSTERWITHTRACKS_H 1

#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/Track.h"
#include "ICaloFutureClusterSelector.h"
#include "Relations/IRelationWeighted.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"

#include <string>

/** @class CaloFutureSelectNeutralClusterWithTracks CaloFutureSelectNeutralClusterWithTracks.h
 *
 *  Concrete tool, which select the Neutral clusters
 *  (photon candidates) looking through the relation table
 *  of 2D-matched CaloClusters and Tracks
 *
 *  Cluster is considered to be "selected"
 *  if there are no reconstructed tracks with
 *  chi2 value for 2D-matching under the threshold value
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   26/04/2002
 */
class CaloFutureSelectNeutralClusterWithTracks : public extends<GaudiTool, ICaloFutureClusterSelector> {
public:
  /** @brief "select"  method
   *
   *  Cluster is considered to be "selected"
   *  if there are no reconstructed tracks with
   *  chi2 value for 2D-matching under the threshold value
   *
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( const LHCb::CaloCluster* cluster ) const override;

  /** @brief "select"  method (functor interface)
   *
   *  Cluster is considered to be "selected"
   *  if there are no reconstructed tracks with
   *  chi2 value for 2D-matching under the threshold value
   *
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( const LHCb::CaloCluster* cluster ) const override;

  /** Standard constructor
   */
  using extends::extends;

private:
  Gaudi::Property<std::string>           m_tableLocation{this, "Table",
                                               LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )};
  Gaudi::Property<float>                 m_chi2cut{this, "MinChi2", -100};
  mutable Gaudi::Accumulators::Counter<> m_counter{this, "selected clusters"};
};
#endif // CALOFUTURERECO_CALOFUTURESELECTNEUTRALCLUSTERWITHTRACKS_H

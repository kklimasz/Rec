/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERECO_CLUSTERSPREADTOOL_H
#define CALOFUTURERECO_CLUSTERSPREADTOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Property.h"
// ============================================================================
// CaloFutureInterfaces
// ============================================================================
#include "ICaloFutureClusterTool.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// CaloFutureUtil
// ============================================================================
#include "CaloFutureUtils/SpreadEstimator.h"
// ============================================================================
/** @class FutureClusterSpreadTool FutureClusterSpreadTool.h
 *
 *  Concrete tool for estimation of the
 *  effective cluster size ("spread")
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   22/11/2001
 */
// ============================================================================
class FutureClusterSpreadTool : public extends<GaudiTool, ICaloFutureClusterTool> {
public:
  // ==========================================================================
  /** standard initialization method
   *  @return status code
   */
  StatusCode initialize() override;
  // ==========================================================================
  /** standard finalization method
   *  @return status code
   */
  StatusCode finalize() override;
  // ==========================================================================
  /** The main processing method (functor interface)
   *  @param cluster pointer to CaloCluster object to be processed
   *  @return status code
   */
  StatusCode operator()( LHCb::CaloCluster& cluster ) const override;
  // ==========================================================================
  /** Standard constructor
   *  @param type tool type (useless)
   *  @param name tool name
   *  @param parent pointer to parent object (service, algorithm or tool)
   */
  FutureClusterSpreadTool( const std::string& type, const std::string& name, const IInterface* parent );
  // ==========================================================================
private:
  // ==========================================================================
  SpreadEstimator              m_estimator;
  Gaudi::Property<std::string> m_detData{this, "Detector"};
  const DeCalorimeter*         m_det = nullptr;
  // ==========================================================================
};
// ============================================================================
// The End
// ============================================================================
#endif // CALOFUTURERECO_CLUSTERSPREADTOOL_H

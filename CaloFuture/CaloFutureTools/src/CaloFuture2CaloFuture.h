/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURE2CALOFUTURE_H
#define CALOFUTURE2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "GaudiAlg/GaudiTool.h"
/** @class CaloFuture2CaloFuture CaloFuture2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-05-29
 */

namespace {
  inline std::vector<const LHCb::CaloDigit*> addCell( const LHCb::CaloCellID& id, const LHCb::CaloDigits& input,
                                                      std::vector<const LHCb::CaloDigit*>&& output ) {
    if ( output.end() !=
         std::find_if( output.begin(), output.end(), [id]( const auto* d ) { return d->cellID() == id; } ) ) {
      if ( auto* digit = input.object( id ); digit ) output.push_back( digit );
    }
    return std::move( output );
  }
} // namespace

class CaloFuture2CaloFuture : public GaudiTool {
public:
  /// Standard constructor
  using GaudiTool::GaudiTool;

  StatusCode initialize() override;

protected:
  // CaloCellIDs
  std::vector<const LHCb::CaloDigit*> cellIDs( const LHCb::CaloCluster& fromCluster, CaloCellCode::CaloIndex toCalo,
                                               const LHCb::CaloDigits& digits ) const;
  std::vector<const LHCb::CaloDigit*> cellIDs_( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCalo,
                                                const LHCb::CaloDigits&               digits,
                                                std::vector<const LHCb::CaloDigit*>&& container ) const;

  // Calo Maps
  template <typename T>
  class Map {
    std::array<T, 2> content = {};

  public:
    Map() = default;
    template <typename Args>
    Map( Args&& ecal, Args&& hcal )
        : content{std::make_from_tuple<T>( std::forward<Args>( ecal ) ),
                  std::make_from_tuple<T>( std::forward<Args>( hcal ) )} {}
    T& operator[]( CaloCellCode::CaloIndex idx ) {
      switch ( idx ) {
      case CaloCellCode::CaloIndex::EcalCalo:
        return content[0];
      case CaloCellCode::CaloIndex::HcalCalo:
        return content[1];
      default:
        throw std::out_of_range( "CaloFuture2CaloFuture: invalid calorimeter requested" );
      }
    }
    const T& operator[]( CaloCellCode::CaloIndex idx ) const {
      switch ( idx ) {
      case CaloCellCode::CaloIndex::EcalCalo:
        return content[0];
      case CaloCellCode::CaloIndex::HcalCalo:
        return content[1];
      default:
        throw std::out_of_range( "CaloFuture2CaloFuture: invalid calorimeter requested" );
      }
    }
  };

  Map<const DeCalorimeter*> m_det;
  Map<Gaudi::Plane3D>       m_plane;

private:
  Map<double>           m_refSize;
  Gaudi::Property<bool> m_geo{this, "IdealGeometry", true};
};
#endif // CALOFUTURE2CALOFUTURE_H

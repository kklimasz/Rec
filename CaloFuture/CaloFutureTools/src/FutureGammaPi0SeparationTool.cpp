/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include <cmath>

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
// from Event
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/RecHeader.h"

// local
#include "FutureGammaPi0SeparationTool.h"

using namespace LHCb;
using namespace Gaudi::Units;
//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0SeparationTool
//
// 2019-03-28 : Miriam Calvo Gomez
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::GammaPi0Separation, "FutureGammaPi0SeparationTool" )
namespace LHCb::Calo {

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode GammaPi0Separation::initialize() {

    StatusCode sc = base_class::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;          // error printed already by GaudiAlgorithm

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

    /// Retrieve geometry of detector
    m_ecal = getDetIfExists<DeCalorimeter>( DeCalorimeterLocation::Ecal );

    // TMVA discriminant
    static const std::vector<std::string> inputVars = {"isPhfr2",   "isPhfr2r4", "abs(isPhasym)",
                                                       "isPhkappa", "isPhEseed", "isPhE2"};

    m_reader0 = std::make_unique<ReadMLPOuter>( inputVars );
    m_reader1 = std::make_unique<ReadMLPMiddle>( inputVars );
    m_reader2 = std::make_unique<ReadMLPInner>( inputVars );

    return StatusCode::SUCCESS;
  }

  //=============================================================================
  //  Finalize
  //=============================================================================
  StatusCode GammaPi0Separation::finalize() {

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Finalize" << endmsg;

    m_reader0.reset();
    m_reader1.reset();
    m_reader2.reset();

    return base_class::finalize(); // must be executed last
  }

  //=============================================================================
  // Main execution
  //=============================================================================

  std::optional<double> GammaPi0Separation::isPhoton( const LHCb::CaloHypo& hypo ) {

    // clear all data
    m_data.clear();

    if ( !m_ecal ) return {};
    if ( LHCb::CaloMomentum( &hypo ).pt() < m_minPt ) return {};

    double fr2    = 0;
    double fasym  = 0;
    double fkappa = 0;
    double fr2r4  = 0;
    double Eseed  = 0;
    double E2     = 0;
    double Ecl    = 0;
    int    area   = 0;

    // evaluate the NN inputs
    bool ecalV = ClusterVariables( hypo, fr2, fasym, fkappa, fr2r4, Ecl, Eseed, E2, area );

    if ( !ecalV ) return {};
    // return NN output
    return photonDiscriminant( area, fr2, fr2r4, fasym, fkappa, Eseed, E2 );
  }

  bool GammaPi0Separation::ClusterVariables( const LHCb::CaloHypo& hypo, double& fr2, double& fasym, double& fkappa,
                                             double& fr2r4, double& etot, double& Eseed, double& E2, int& area ) {
    m_data.clear();

    const LHCb::CaloCluster* cluster =
        LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo ); // OD 2014/05 - change to Split Or Main  cluster
    if ( !cluster ) return false;

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Inside ClusterVariables ------" << endmsg;

    const auto cxx = cluster->position().spread()( 0, 0 );
    const auto cyy = cluster->position().spread()( 1, 1 );
    const auto cxy = cluster->position().spread()( 0, 1 );
    fr2            = cxx + cyy;
    fasym          = ( cxx > 0 && cyy > 0 ? cxy / std::sqrt( cxx * cyy ) : 0 );
    const auto arg = ( fr2 > 0. ? 1. - 4. * ( cxx * cyy - cxy * cxy ) / ( fr2 * fr2 ) : 0. );
    fkappa         = arg > 0. ? std::sqrt( arg ) : 0.;

    const auto xmean = cluster->position().x();
    const auto ymean = cluster->position().y();

    // OD : WARNING cluster->e() is cluster-shape dependent (3x3 / 2x2 ...) RE-EVALUATE E3x3 instead for back.
    // compatibility etot = cluster->e(); //same as position.e
    etot = 0.;

    const Gaudi::XYZPoint position( xmean, ymean, cluster->position().z() );

    double r4      = 0.;
    area           = -1;
    int    ncells  = 0;
    double secondE = 0.;

    const auto& entries = cluster->entries();
    for ( const auto& entry : entries ) {
      const LHCb::CaloDigit* digit = entry.digit();
      if ( !digit ) { continue; }
      const auto energy = digit->e() * entry.fraction();

      if ( abs( (int)digit->cellID().col() - (int)cluster->seed().col() ) <= 1 &&
           abs( (int)digit->cellID().row() - (int)cluster->seed().row() ) <= 1 &&
           digit->cellID().area() == cluster->seed().area() ) {
        etot += energy;
      }

      if ( entry.status() & LHCb::CaloDigitStatus::SeedCell ) {
        area  = digit->cellID().area();
        Eseed = energy;
      } else {
        if ( energy > secondE ) { secondE = energy; }
      }

      const auto& pos = m_ecal->cellCenter( digit->cellID() );
      const auto  x   = pos.x();
      const auto  y   = pos.y();

      if ( energy <= 0 ) { continue; }
      const double weight = energy > 0.0 ? energy : 0.0;

      auto rr = std::pow( x - xmean, 2 ) + std::pow( y - ymean, 2 );
      if ( entries.size() <= 1 || rr < 1.e-10 ) { rr = 0; } // to avoid huge unphysical value due to machine precision
      r4 += weight * rr * rr;

      ncells++;
    } // loop cluster cells

    if ( etot > 0. ) {
      r4 /= etot;
      fr2r4 = ( r4 != 0 ) ? ( r4 - fr2 * fr2 ) / r4 : 0.;
      E2    = ( secondE + Eseed ) / etot;
      Eseed = Eseed / etot;
    } else {
      // should never happen
      r4    = 0;
      fr2r4 = 0.;
      E2    = 0.;
      Eseed = 0.;
    }

    m_data[Enum::DataType::isPhotonEcl]   = etot;
    m_data[Enum::DataType::isPhotonFr2]   = fr2;
    m_data[Enum::DataType::isPhotonFr2r4] = fr2r4;
    m_data[Enum::DataType::isPhotonAsym]  = fasym;
    m_data[Enum::DataType::isPhotonKappa] = fkappa;
    m_data[Enum::DataType::isPhotonEseed] = Eseed;
    m_data[Enum::DataType::isPhotonE2]    = E2;
    return true;
  }

  std::optional<double> GammaPi0Separation::photonDiscriminant( int area, double r2, double r2r4, double asym,
                                                                double kappa, double Eseed, double E2 ) const {
    std::vector<double> input = {r2,    r2r4, std::abs( asym ), kappa,
                                 Eseed, // already divided by Ecl
                                 E2};   // means (e2+eseed)/ecl

    switch ( area ) {
    case 0:
      return m_reader0->GetMvaValue( input );
    case 1:
      return m_reader1->GetMvaValue( input );
    case 2:
      return m_reader2->GetMvaValue( input );
    default:
      return {};
    }
  }
} // namespace LHCb::Calo

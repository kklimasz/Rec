/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTURERELATIONSGETTER_H
#define CALOFUTURERELATIONSGETTER_H 1

// Include files
// from Gaudi
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h" // Interface
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
// from LHCb
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Relations/Relation2D.h"

#include "Relations/RelationWeighted2D.h"

/** @class CaloFutureRelationsGetter CaloFutureRelationsGetter.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2013-10-04
 */
class CaloFutureRelationsGetter : public extends<GaudiTool, ICaloFutureRelationsGetter, IIncidentListener> {
public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;
  StatusCode finalize() override;
  void       handle( const Incident& ) override {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "IIncident Svc reset" << endmsg;
    clean();
  }

  // getters
  const LHCb::CaloFuture2Track::ITrHypoTable2D* getTrHypoTable2D( std::string location ) const override;

private:
  LHCb::RelationWeighted2D<LHCb::Track, LHCb::CaloHypo, float>   m_trHypo;
  std::map<std::string, LHCb::CaloFuture2Track::IHypoTrTable2D*> m_hypoTr;
  void                                                           clean();
};
#endif // CALOFUTURERELATIONSGETTER_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FUTURECOUNTERLEVEL_H
#define FUTURECOUNTERLEVEL_H 1

// Include files
// from Gaudi
#include "CaloFutureInterfaces/IFutureCounterLevel.h" // Interface
#include "GaudiAlg/GaudiTool.h"

/** @class FutureCounterLevel FutureCounterLevel.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2016-08-13
 */
class FutureCounterLevel final : public extends<GaudiTool, IFutureCounterLevel> {
public:
  /// Standard constructor
  FutureCounterLevel( const std::string& type, const std::string& name, const IInterface* parent );

  bool isQuiet() const override { return m_isQuiet; };
  bool isVerbose() const override { return m_isVerbose; };
  bool isLevel( int l ) const override { return m_clevel >= l; };
  int  level() const override { return m_clevel; };

private:
  Gaudi::Property<int> m_clevel{this, "SetLevel", 1, "quiet mode is the default"};
  bool                 m_isQuiet   = true;
  bool                 m_isVerbose = false;
};
#endif // FUTURECOUNTERLEVEL_H
